/**
 * 
 */
package com.evampsaanga.azerfon.controllers;

import java.io.IOException;
import java.sql.SQLException;

import javax.jms.JMSException;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONObject;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.evampsaanga.azerfon.business.AppMenuBusiness;
import com.evampsaanga.azerfon.common.messagesmappings.GetMessagesMappings;
import com.evampsaanga.azerfon.common.utilities.Constants;
import com.evampsaanga.azerfon.common.utilities.Transactions;
import com.evampsaanga.azerfon.common.utilities.Utilities;
import com.evampsaanga.azerfon.common.utilities.Validator;
import com.evampsaanga.azerfon.models.menues.appmenu.AppMenuRequest;
import com.evampsaanga.azerfon.models.menues.appmenuV2.AppMenuResponse;

/**
 * @author Evamp & Saanga
 *
 */
@RestController
@RequestMapping("/menus")
public class AppMenuController {

	Logger logger = Logger.getLogger(AppMenuController.class);

	@RequestMapping(value = "/getappmenu", method = RequestMethod.POST, headers = Constants.CONTENT_TYPE_DEFAULT_VALUE)
	public AppMenuResponse getAppMenu(@RequestBody String data,
			@RequestHeader(value = Constants.TOKEN_KEY, defaultValue = Constants.TOKEN_DEFAULT_VALUE) String token,
			@RequestHeader(value = Constants.LANGUAGE_KEY, defaultValue = Constants.LANGUAGE_DEFAULT_VALUE) String lang,
			@RequestHeader(value = Constants.USER_AGENT_KEY, defaultValue = Constants.USER_AGENT_DEFAULT_VALUE) String userAgent,
			@RequestHeader(value = Constants.SUBSCRIBER_TYPE_KEY, defaultValue = Constants.SUBSCRIBER_TYPE_DEFAULT_VALUE) String userType,
			@RequestHeader(value = Constants.TARIFF_TYPE_KEY, defaultValue = Constants.TARIFF_TYPE_DEFAULT_VALUE) String tariffType,
			HttpServletRequest servletRequest) throws ClassNotFoundException, SQLException, JsonParseException,
			JsonMappingException, IOException, JMSException {
		ObjectMapper mapper = new ObjectMapper();
		JSONObject jsonObject = new JSONObject();
		AppMenuBusiness appMenuBusiness = new AppMenuBusiness();
		AppMenuRequest appMenuRequest = new AppMenuRequest();
		AppMenuResponse appMenuResponse = new AppMenuResponse();
		String requestTime = Utilities.getReportDateTime();

		String TRANSACTION_NAME = Transactions.APP_MENU_TRANSACTION_NAME + " CONTROLLER";
		try {
			//data = jsonObject.toString();
			String msisdn = Utilities.getValueFromJSON(Utilities.splitToken(Utilities.decodeString(token)).toString(),
					Constants.MSISDN_KEY);

			// Populating report object before processing business logic.
			appMenuResponse.setLogsReport(Utilities.preBusinessReportLoggingData(token, msisdn, TRANSACTION_NAME,
					servletRequest.getRemoteAddr(), userAgent, userType, tariffType, data, lang,
					appMenuResponse.getLogsReport()));

			data = Utilities.addAttributesToRequest(data, msisdn, servletRequest, lang, userAgent);

			Utilities.printInfoLog(msisdn + "-Request Landed in-" + TRANSACTION_NAME, logger);
			Utilities.printInfoLog(msisdn + "-Request Data-" + data, logger);

			appMenuRequest = mapper.readValue(data, AppMenuRequest.class);
			String requestValidationStatus = Validator.validateRequest(msisdn, appMenuRequest);

			if (requestValidationStatus.equalsIgnoreCase(Constants.VALIDATION_SUCCESS_CODE)) {

				appMenuResponse = appMenuBusiness.getAppMenuBusiness(msisdn, appMenuRequest, appMenuResponse);

			} else {
				appMenuResponse.setCallStatus(Constants.Call_Status_False);
				appMenuResponse.setResultCode(Constants.VALIDATION_FAILURE_CODE);
				appMenuResponse.setResultDesc(requestValidationStatus);
			}

			appMenuResponse.getLogsReport().setRequestTime(requestTime);
			appMenuResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
			appMenuResponse.getLogsReport().setResponse(mapper.writeValueAsString(appMenuResponse));
			appMenuResponse.getLogsReport().setResponseCode(appMenuResponse.getResultCode());

			// Sending report log into queue.
			Utilities.prepareLogReportForQueue(appMenuResponse.getLogsReport());

			Utilities.printInfoLog(msisdn + "-Response Returned from-" + TRANSACTION_NAME + "-"
					+ mapper.writeValueAsString(appMenuResponse), logger);
		} catch (Exception e) {
			Utilities.printErrorLog(Utilities.convertStackTraceToString(e), logger);
			appMenuResponse.setCallStatus(Constants.Call_Status_False);
			appMenuResponse.setResultCode(Constants.EXCEPTION_CODE);
			appMenuResponse.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("unexpected.error", lang));

			appMenuResponse.getLogsReport().setResponseCode(appMenuResponse.getResultCode());
			appMenuResponse.getLogsReport().setRequestTime(requestTime);
			appMenuResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
			appMenuResponse.getLogsReport().setResponse(mapper.writeValueAsString(appMenuResponse));

			// Sending report log into queue.
			Utilities.prepareLogReportForQueue(appMenuResponse.getLogsReport());

		}
		return appMenuResponse;
	}
}
