/**
 * 
 */
package com.evampsaanga.azerfon.controllers;

import java.io.IOException;
import java.sql.SQLException;

import javax.jms.JMSException;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONObject;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.evampsaanga.azerfon.business.HomePageBusiness;
import com.evampsaanga.azerfon.common.messagesmappings.GetMessagesMappings;
import com.evampsaanga.azerfon.common.utilities.Constants;
import com.evampsaanga.azerfon.common.utilities.Transactions;
import com.evampsaanga.azerfon.common.utilities.Utilities;
import com.evampsaanga.azerfon.common.utilities.Validator;
import com.evampsaanga.azerfon.models.homepageservices.HomePageRequest;
import com.evampsaanga.azerfon.models.homepageservices.HomePageResponse;

/**
 * @author Evamp & Saanga
 *
 */

@RestController
@RequestMapping("/homepageservices")
public class HomePageController {

    Logger logger = Logger.getLogger(HomePageController.class);

    @RequestMapping(value = "/gethomepage", method = RequestMethod.POST, headers = Constants.CONTENT_TYPE_DEFAULT_VALUE)
    public HomePageResponse getHomePage(@RequestBody String data,
	    @RequestHeader(value = Constants.TOKEN_KEY, defaultValue = Constants.TOKEN_DEFAULT_VALUE) String token,
	    @RequestHeader(value = Constants.LANGUAGE_KEY, defaultValue = Constants.LANGUAGE_DEFAULT_VALUE) String lang,
	    @RequestHeader(value = Constants.USER_AGENT_KEY, defaultValue = Constants.USER_AGENT_DEFAULT_VALUE) String userAgent,
	    @RequestHeader(value = Constants.SUBSCRIBER_TYPE_KEY, defaultValue = Constants.SUBSCRIBER_TYPE_DEFAULT_VALUE) String userType,
	    @RequestHeader(value = Constants.TARIFF_TYPE_KEY, defaultValue = Constants.TARIFF_TYPE_DEFAULT_VALUE) String tariffType,
	    HttpServletRequest servletRequest) throws ClassNotFoundException, SQLException, JsonParseException,
	    JsonMappingException, IOException, JMSException {

	ObjectMapper mapper = new ObjectMapper();

	HomePageBusiness homePageBusiness = new HomePageBusiness();
	HomePageRequest homePageRequest = new HomePageRequest();
	HomePageResponse homePageResponse = new HomePageResponse();

	String TRANSACTION_NAME = Transactions.HOME_PAGE_TRANSACTION_NAME + " CONTROLLER";

	String requestTime = Utilities.getReportDateTime();
	try {

	    String msisdn = Utilities.getValueFromJSON(Utilities.splitToken(Utilities.decodeString(token)).toString(),
		    Constants.MSISDN_KEY);

	    // Populating report object before processing business logic.
	    homePageResponse.setLogsReport(Utilities.preBusinessReportLoggingData(token, msisdn, TRANSACTION_NAME,
		    servletRequest.getRemoteAddr(), userAgent, userType, tariffType, data, lang,
		    homePageResponse.getLogsReport()));

	    data = Utilities.addAttributesToRequest(data, msisdn, servletRequest, lang, userAgent);

	    Utilities.printInfoLog(msisdn + "-Request Landed in-" + TRANSACTION_NAME, logger);
	    Utilities.printInfoLog(msisdn + "-Request Data-" + data, logger);

	    homePageRequest = mapper.readValue(data, HomePageRequest.class);
	    JSONObject jsonObject = new JSONObject(data);
	    homePageRequest.setCustomerType(jsonObject.getString("subscriberType"));
	    String requestValidationStatus = Validator.validateRequest(msisdn, homePageRequest);

	    if (requestValidationStatus.equalsIgnoreCase(Constants.VALIDATION_SUCCESS_CODE)) {

		homePageResponse = homePageBusiness.getHomePageBusiness(msisdn, homePageRequest, homePageResponse);
	    } else {
		homePageResponse.setCallStatus(Constants.Call_Status_False);
		homePageResponse.setResultCode(Constants.VALIDATION_FAILURE_CODE);
		homePageResponse.setResultDesc(requestValidationStatus);
	    }

	    homePageResponse.getLogsReport().setResponseCode(homePageResponse.getResultCode());
	    homePageResponse.getLogsReport().setRequestTime(requestTime);
	    homePageResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
	    homePageResponse.getLogsReport().setResponse(mapper.writeValueAsString(homePageResponse));

	    // Sending report log into queue.
	    Utilities.prepareLogReportForQueue(homePageResponse.getLogsReport());

	    Utilities.printInfoLog(msisdn + "-Response Returned from-" + TRANSACTION_NAME + "-"
		    + mapper.writeValueAsString(homePageResponse), logger);
	} catch (Exception e) {
	    Utilities.printErrorLog(Utilities.convertStackTraceToString(e), logger);
	    homePageResponse.setCallStatus(Constants.Call_Status_False);
	    homePageResponse.setResultCode(Constants.EXCEPTION_CODE);
	    homePageResponse.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("unexpected.error", lang));

	    homePageResponse.getLogsReport().setResponseCode(homePageResponse.getResultCode());
	    homePageResponse.getLogsReport().setRequestTime(requestTime);
	    homePageResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
	    homePageResponse.getLogsReport().setResponse(mapper.writeValueAsString(homePageResponse));

	    // Sending report log into queue.
	    Utilities.prepareLogReportForQueue(homePageResponse.getLogsReport());

	}
	return homePageResponse;
    }

}
