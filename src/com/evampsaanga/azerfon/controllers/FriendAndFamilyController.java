/**
 * 
 */
package com.evampsaanga.azerfon.controllers;

import java.io.IOException;
import java.sql.SQLException;

import javax.jms.JMSException;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.evampsaanga.azerfon.business.FriendAndFamilyBusiness;
import com.evampsaanga.azerfon.common.messagesmappings.GetMessagesMappings;
import com.evampsaanga.azerfon.common.utilities.Constants;
import com.evampsaanga.azerfon.common.utilities.Transactions;
import com.evampsaanga.azerfon.common.utilities.Utilities;
import com.evampsaanga.azerfon.common.utilities.Validator;
import com.evampsaanga.azerfon.models.friendandfamily.addfnf.AddFriendAndFamilyRequest;
import com.evampsaanga.azerfon.models.friendandfamily.addfnf.AddFriendAndFamilyResponse;
import com.evampsaanga.azerfon.models.friendandfamily.deletefnf.DeleteFriendAndFamilyRequest;
import com.evampsaanga.azerfon.models.friendandfamily.deletefnf.DeleteFriendAndFamilyResponse;
import com.evampsaanga.azerfon.models.friendandfamily.getfnf.GetFriendAndFamilyRequest;
import com.evampsaanga.azerfon.models.friendandfamily.getfnf.GetFriendAndFamilyResponse;
import com.evampsaanga.azerfon.models.friendandfamily.updatefnf.UpdateFriendAndFamilyRequest;
import com.evampsaanga.azerfon.models.friendandfamily.updatefnf.UpdateFriendAndFamilyResponse;

/**
 * @author Evamp & Saanga
 *
 */
@RestController
@RequestMapping("/fnf")
public class FriendAndFamilyController {

	Logger logger = Logger.getLogger(FriendAndFamilyController.class);

	@RequestMapping(value = "/getfnf", method = RequestMethod.POST, headers = Constants.CONTENT_TYPE_DEFAULT_VALUE)
	public GetFriendAndFamilyResponse getFnF(@RequestBody String data,
			@RequestHeader(value = Constants.TOKEN_KEY, defaultValue = Constants.TOKEN_DEFAULT_VALUE) String token,
			@RequestHeader(value = Constants.LANGUAGE_KEY, defaultValue = Constants.LANGUAGE_DEFAULT_VALUE) String lang,
			@RequestHeader(value = Constants.USER_AGENT_KEY, defaultValue = Constants.USER_AGENT_DEFAULT_VALUE) String userAgent,
			@RequestHeader(value = Constants.SUBSCRIBER_TYPE_KEY, defaultValue = Constants.SUBSCRIBER_TYPE_DEFAULT_VALUE) String userType,
			@RequestHeader(value = Constants.TARIFF_TYPE_KEY, defaultValue = Constants.TARIFF_TYPE_DEFAULT_VALUE) String tariffType,
			HttpServletRequest servletRequest) throws ClassNotFoundException, SQLException, JsonParseException,
			JsonMappingException, IOException, JMSException {
		ObjectMapper mapper = new ObjectMapper();
		FriendAndFamilyBusiness friendAndFamilyBusiness = new FriendAndFamilyBusiness();
		GetFriendAndFamilyRequest friendAndFamilyRequest = new GetFriendAndFamilyRequest();
		GetFriendAndFamilyResponse friendAndFamilyResponse = new GetFriendAndFamilyResponse();

		String requestTime = Utilities.getReportDateTime();

		String TRANSACTION_NAME = Transactions.GET_FNF_TRANSACTION_NAME + " CONTROLLER";
		try {

			String msisdn = Utilities.getValueFromJSON(Utilities.splitToken(Utilities.decodeString(token)).toString(),
					Constants.MSISDN_KEY);

			// Populating report object before processing business logic.
			friendAndFamilyResponse.setLogsReport(Utilities.preBusinessReportLoggingData(token, msisdn,
					TRANSACTION_NAME, servletRequest.getRemoteAddr(), userAgent, userType, tariffType, data, lang,
					friendAndFamilyResponse.getLogsReport()));

			data = Utilities.addAttributesToRequest(data, msisdn, servletRequest, lang, userAgent);

			Utilities.printInfoLog(msisdn + "-Request Landed in-" + TRANSACTION_NAME, logger);
			Utilities.printInfoLog(msisdn + "-Request Data-" + data, logger);

			friendAndFamilyRequest = mapper.readValue(data, GetFriendAndFamilyRequest.class);

			String requestValidationStatus = Validator.validateRequest(msisdn, friendAndFamilyRequest);

			if (requestValidationStatus.equalsIgnoreCase(Constants.VALIDATION_SUCCESS_CODE)) {

				friendAndFamilyResponse = friendAndFamilyBusiness.getFnf(msisdn, friendAndFamilyRequest,
						friendAndFamilyResponse);

			} else {
				friendAndFamilyResponse.setCallStatus(Constants.Call_Status_False);
				friendAndFamilyResponse.setResultCode(Constants.VALIDATION_FAILURE_CODE);
				friendAndFamilyResponse.setResultDesc(requestValidationStatus);
			}

			friendAndFamilyResponse.getLogsReport().setResponseCode(friendAndFamilyResponse.getResultCode());
			friendAndFamilyResponse.getLogsReport().setRequestTime(requestTime);
			friendAndFamilyResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
			friendAndFamilyResponse.getLogsReport().setResponse(mapper.writeValueAsString(friendAndFamilyResponse));

			// Sending report log into queue.
			Utilities.prepareLogReportForQueue(friendAndFamilyResponse.getLogsReport());

			Utilities.printInfoLog(msisdn + "-Response Returned from-" + TRANSACTION_NAME + "-"
					+ mapper.writeValueAsString(friendAndFamilyResponse), logger);
		} catch (Exception e) {
			Utilities.printErrorLog(Utilities.convertStackTraceToString(e), logger);
			friendAndFamilyResponse.setCallStatus(Constants.Call_Status_False);
			friendAndFamilyResponse.setResultCode(Constants.EXCEPTION_CODE);
			friendAndFamilyResponse
					.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("unexpected.error", lang));

			friendAndFamilyResponse.getLogsReport().setResponseCode(friendAndFamilyResponse.getResultCode());
			friendAndFamilyResponse.getLogsReport().setRequestTime(requestTime);
			friendAndFamilyResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
			friendAndFamilyResponse.getLogsReport().setResponse(mapper.writeValueAsString(friendAndFamilyResponse));

			// Sending report log into queue.
			Utilities.prepareLogReportForQueue(friendAndFamilyResponse.getLogsReport());

		}
		return friendAndFamilyResponse;
	}

	@RequestMapping(value = "/addfnf", method = RequestMethod.POST, headers = Constants.CONTENT_TYPE_DEFAULT_VALUE)
	public AddFriendAndFamilyResponse addFnF(@RequestBody String data,
			@RequestHeader(value = Constants.TOKEN_KEY, defaultValue = Constants.TOKEN_DEFAULT_VALUE) String token,
			@RequestHeader(value = Constants.LANGUAGE_KEY, defaultValue = Constants.LANGUAGE_DEFAULT_VALUE) String lang,
			@RequestHeader(value = Constants.USER_AGENT_KEY, defaultValue = Constants.USER_AGENT_DEFAULT_VALUE) String userAgent,
			@RequestHeader(value = Constants.SUBSCRIBER_TYPE_KEY, defaultValue = Constants.SUBSCRIBER_TYPE_DEFAULT_VALUE) String userType,
			@RequestHeader(value = Constants.TARIFF_TYPE_KEY, defaultValue = Constants.TARIFF_TYPE_DEFAULT_VALUE) String tariffType,
			HttpServletRequest servletRequest) throws ClassNotFoundException, SQLException, JsonParseException,
			JsonMappingException, IOException, JMSException {
		ObjectMapper mapper = new ObjectMapper();

		FriendAndFamilyBusiness friendAndFamilyBusiness = new FriendAndFamilyBusiness();
		AddFriendAndFamilyRequest addFriendAndFamilyRequest = new AddFriendAndFamilyRequest();
		AddFriendAndFamilyResponse addFriendAndFamilyResponse = new AddFriendAndFamilyResponse();

		String requestTime = Utilities.getReportDateTime();

		String TRANSACTION_NAME = Transactions.ADD_FNF_TRANSACTION_NAME + " CONTROLLER";
		try {
			String msisdn = Utilities.getValueFromJSON(Utilities.splitToken(Utilities.decodeString(token)).toString(),
					Constants.MSISDN_KEY);

			// Populating report object before processing business logic.
			addFriendAndFamilyResponse.setLogsReport(Utilities.preBusinessReportLoggingData(token, msisdn,
					TRANSACTION_NAME, servletRequest.getRemoteAddr(), userAgent, userType, tariffType, data, lang,
					addFriendAndFamilyResponse.getLogsReport()));

			data = Utilities.addAttributesToRequest(data, msisdn, servletRequest, lang, userAgent);

			Utilities.printInfoLog(msisdn + "-Request Landed in-" + TRANSACTION_NAME, logger);
			Utilities.printInfoLog(msisdn + "-Request Data-" + data, logger);

			addFriendAndFamilyRequest = mapper.readValue(data, AddFriendAndFamilyRequest.class);

			String requestValidationStatus = Validator.validateRequest(msisdn, addFriendAndFamilyRequest);

			if (requestValidationStatus.equalsIgnoreCase(Constants.VALIDATION_SUCCESS_CODE)) {

				addFriendAndFamilyResponse = friendAndFamilyBusiness.addFnf(msisdn, addFriendAndFamilyRequest,
						addFriendAndFamilyResponse);

			} else {
				addFriendAndFamilyResponse.setCallStatus(Constants.Call_Status_False);
				addFriendAndFamilyResponse.setResultCode(Constants.VALIDATION_FAILURE_CODE);
				addFriendAndFamilyResponse.setResultDesc(requestValidationStatus);
			}

			addFriendAndFamilyResponse.getLogsReport().setResponseCode(addFriendAndFamilyResponse.getResultCode());
			addFriendAndFamilyResponse.getLogsReport().setRequestTime(requestTime);
			addFriendAndFamilyResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
			addFriendAndFamilyResponse.getLogsReport()
					.setResponse(mapper.writeValueAsString(addFriendAndFamilyResponse));

			// Sending report log into queue.
			Utilities.prepareLogReportForQueue(addFriendAndFamilyResponse.getLogsReport());

			Utilities.printInfoLog(msisdn + "-Response Returned from-" + TRANSACTION_NAME + "-"
					+ mapper.writeValueAsString(addFriendAndFamilyResponse), logger);
		} catch (Exception e) {
			Utilities.printErrorLog(Utilities.convertStackTraceToString(e), logger);
			addFriendAndFamilyResponse.setCallStatus(Constants.Call_Status_False);
			addFriendAndFamilyResponse.setResultCode(Constants.EXCEPTION_CODE);
			addFriendAndFamilyResponse
					.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("unexpected.error", lang));

			addFriendAndFamilyResponse.getLogsReport().setResponseCode(addFriendAndFamilyResponse.getResultCode());
			addFriendAndFamilyResponse.getLogsReport().setRequestTime(requestTime);
			addFriendAndFamilyResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
			addFriendAndFamilyResponse.getLogsReport()
					.setResponse(mapper.writeValueAsString(addFriendAndFamilyResponse));

			// Sending report log into queue.
			Utilities.prepareLogReportForQueue(addFriendAndFamilyResponse.getLogsReport());

		}
		return addFriendAndFamilyResponse;
	}

	@RequestMapping(value = "/deletefnf", method = RequestMethod.POST, headers = Constants.CONTENT_TYPE_DEFAULT_VALUE)
	public DeleteFriendAndFamilyResponse deleteFnF(@RequestBody String data,
			@RequestHeader(value = Constants.TOKEN_KEY, defaultValue = Constants.TOKEN_DEFAULT_VALUE) String token,
			@RequestHeader(value = Constants.LANGUAGE_KEY, defaultValue = Constants.LANGUAGE_DEFAULT_VALUE) String lang,
			@RequestHeader(value = Constants.USER_AGENT_KEY, defaultValue = Constants.USER_AGENT_DEFAULT_VALUE) String userAgent,
			@RequestHeader(value = Constants.SUBSCRIBER_TYPE_KEY, defaultValue = Constants.SUBSCRIBER_TYPE_DEFAULT_VALUE) String userType,
			@RequestHeader(value = Constants.TARIFF_TYPE_KEY, defaultValue = Constants.TARIFF_TYPE_DEFAULT_VALUE) String tariffType,
			HttpServletRequest servletRequest) throws ClassNotFoundException, SQLException, JsonParseException,
			JsonMappingException, IOException, JMSException {
		ObjectMapper mapper = new ObjectMapper();

		FriendAndFamilyBusiness friendAndFamilyBusiness = new FriendAndFamilyBusiness();
		DeleteFriendAndFamilyRequest deleteFriendAndFamilyRequest = new DeleteFriendAndFamilyRequest();
		DeleteFriendAndFamilyResponse deleteFriendAndFamilyResponse = new DeleteFriendAndFamilyResponse();

		String requestTime = Utilities.getReportDateTime();

		String TRANSACTION_NAME = Transactions.DELETE_FNF_TRANSACTION_NAME + " CONTROLLER";
		try {
			String msisdn = Utilities.getValueFromJSON(Utilities.splitToken(Utilities.decodeString(token)).toString(),
					Constants.MSISDN_KEY);

			// Populating report object before processing business logic.
			deleteFriendAndFamilyResponse.setLogsReport(Utilities.preBusinessReportLoggingData(token, msisdn,
					TRANSACTION_NAME, servletRequest.getRemoteAddr(), userAgent, userType, tariffType, data, lang,
					deleteFriendAndFamilyResponse.getLogsReport()));

			data = Utilities.addAttributesToRequest(data, msisdn, servletRequest, lang, userAgent);

			Utilities.printInfoLog(msisdn + "-Request Landed in-" + TRANSACTION_NAME, logger);
			Utilities.printInfoLog(msisdn + "-Request Data-" + data, logger);

			deleteFriendAndFamilyRequest = mapper.readValue(data, DeleteFriendAndFamilyRequest.class);

			String requestValidationStatus = Validator.validateRequest(msisdn, deleteFriendAndFamilyRequest);

			if (requestValidationStatus.equalsIgnoreCase(Constants.VALIDATION_SUCCESS_CODE)) {

				deleteFriendAndFamilyResponse = friendAndFamilyBusiness.deleteFnf(msisdn, deleteFriendAndFamilyRequest,
						deleteFriendAndFamilyResponse);

			} else {
				deleteFriendAndFamilyResponse.setCallStatus(Constants.Call_Status_False);
				deleteFriendAndFamilyResponse.setResultCode(Constants.VALIDATION_FAILURE_CODE);
				deleteFriendAndFamilyResponse.setResultDesc(requestValidationStatus);
			}

			deleteFriendAndFamilyResponse.getLogsReport()
					.setResponseCode(deleteFriendAndFamilyResponse.getResultCode());
			deleteFriendAndFamilyResponse.getLogsReport().setRequestTime(requestTime);
			deleteFriendAndFamilyResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
			deleteFriendAndFamilyResponse.getLogsReport()
					.setResponse(mapper.writeValueAsString(deleteFriendAndFamilyResponse));

			// Sending report log into queue.
			Utilities.prepareLogReportForQueue(deleteFriendAndFamilyResponse.getLogsReport());

			Utilities.printInfoLog(msisdn + "-Response Returned from-" + TRANSACTION_NAME + "-"
					+ mapper.writeValueAsString(deleteFriendAndFamilyResponse), logger);
		} catch (Exception e) {
			Utilities.printErrorLog(Utilities.convertStackTraceToString(e), logger);
			deleteFriendAndFamilyResponse.setCallStatus(Constants.Call_Status_False);
			deleteFriendAndFamilyResponse.setResultCode(Constants.EXCEPTION_CODE);
			deleteFriendAndFamilyResponse
					.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("unexpected.error", lang));

			deleteFriendAndFamilyResponse.getLogsReport()
					.setResponseCode(deleteFriendAndFamilyResponse.getResultCode());
			deleteFriendAndFamilyResponse.getLogsReport().setRequestTime(requestTime);
			deleteFriendAndFamilyResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
			deleteFriendAndFamilyResponse.getLogsReport()
					.setResponse(mapper.writeValueAsString(deleteFriendAndFamilyResponse));

			// Sending report log into queue.
			Utilities.prepareLogReportForQueue(deleteFriendAndFamilyResponse.getLogsReport());

		}
		return deleteFriendAndFamilyResponse;
	}

	@RequestMapping(value = "/updatefnf", method = RequestMethod.POST, headers = Constants.CONTENT_TYPE_DEFAULT_VALUE)
	public UpdateFriendAndFamilyResponse updateFnF(@RequestBody String data,
			@RequestHeader(value = Constants.TOKEN_KEY, defaultValue = Constants.TOKEN_DEFAULT_VALUE) String token,
			@RequestHeader(value = Constants.LANGUAGE_KEY, defaultValue = Constants.LANGUAGE_DEFAULT_VALUE) String lang,
			@RequestHeader(value = Constants.USER_AGENT_KEY, defaultValue = Constants.USER_AGENT_DEFAULT_VALUE) String userAgent,
			@RequestHeader(value = Constants.SUBSCRIBER_TYPE_KEY, defaultValue = Constants.SUBSCRIBER_TYPE_DEFAULT_VALUE) String userType,
			@RequestHeader(value = Constants.TARIFF_TYPE_KEY, defaultValue = Constants.TARIFF_TYPE_DEFAULT_VALUE) String tariffType,
			HttpServletRequest servletRequest) throws ClassNotFoundException, SQLException, JsonParseException,
			JsonMappingException, IOException, JMSException {
		ObjectMapper mapper = new ObjectMapper();

		FriendAndFamilyBusiness friendAndFamilyBusiness = new FriendAndFamilyBusiness();
		UpdateFriendAndFamilyRequest updateFriendAndFamilyRequest = new UpdateFriendAndFamilyRequest();
		UpdateFriendAndFamilyResponse updateFriendAndFamilyResponse = new UpdateFriendAndFamilyResponse();

		String requestTime = Utilities.getReportDateTime();

		String TRANSACTION_NAME = Transactions.UPDATE_FNF_TRANSACTION_NAME + " CONTROLLER";
		try {
			String msisdn = Utilities.getValueFromJSON(Utilities.splitToken(Utilities.decodeString(token)).toString(),
					Constants.MSISDN_KEY);

			// Populating report object before processing business logic.
			updateFriendAndFamilyResponse.setLogsReport(Utilities.preBusinessReportLoggingData(token, msisdn,
					TRANSACTION_NAME, servletRequest.getRemoteAddr(), userAgent, userType, tariffType, data, lang,
					updateFriendAndFamilyResponse.getLogsReport()));

			data = Utilities.addAttributesToRequest(data, msisdn, servletRequest, lang, userAgent);

			Utilities.printInfoLog(msisdn + "-Request Landed in-" + TRANSACTION_NAME, logger);
			Utilities.printInfoLog(msisdn + "-Request Data-" + data, logger);

			updateFriendAndFamilyRequest = mapper.readValue(data, UpdateFriendAndFamilyRequest.class);

			String requestValidationStatus = Validator.validateRequest(msisdn, updateFriendAndFamilyRequest);

			if (requestValidationStatus.equalsIgnoreCase(Constants.VALIDATION_SUCCESS_CODE)) {

				updateFriendAndFamilyResponse = friendAndFamilyBusiness.updateFnf(msisdn, updateFriendAndFamilyRequest,
						updateFriendAndFamilyResponse);

			} else {
				updateFriendAndFamilyResponse.setCallStatus(Constants.Call_Status_False);
				updateFriendAndFamilyResponse.setResultCode(Constants.VALIDATION_FAILURE_CODE);
				updateFriendAndFamilyResponse.setResultDesc(requestValidationStatus);
			}

			updateFriendAndFamilyResponse.getLogsReport()
					.setResponseCode(updateFriendAndFamilyResponse.getResultCode());
			updateFriendAndFamilyResponse.getLogsReport().setRequestTime(requestTime);
			updateFriendAndFamilyResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
			updateFriendAndFamilyResponse.getLogsReport()
					.setResponse(mapper.writeValueAsString(updateFriendAndFamilyResponse));

			// Sending report log into queue.
			Utilities.prepareLogReportForQueue(updateFriendAndFamilyResponse.getLogsReport());

			Utilities.printInfoLog(msisdn + "-Response Returned from-" + TRANSACTION_NAME + "-"
					+ mapper.writeValueAsString(updateFriendAndFamilyResponse), logger);
		} catch (Exception e) {
			Utilities.printErrorLog(Utilities.convertStackTraceToString(e), logger);
			updateFriendAndFamilyResponse.setCallStatus(Constants.Call_Status_False);
			updateFriendAndFamilyResponse.setResultCode(Constants.EXCEPTION_CODE);
			updateFriendAndFamilyResponse
					.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("unexpected.error", lang));

			updateFriendAndFamilyResponse.getLogsReport()
					.setResponseCode(updateFriendAndFamilyResponse.getResultCode());
			updateFriendAndFamilyResponse.getLogsReport().setRequestTime(requestTime);
			updateFriendAndFamilyResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
			updateFriendAndFamilyResponse.getLogsReport()
					.setResponse(mapper.writeValueAsString(updateFriendAndFamilyResponse));

			// Sending report log into queue.
			Utilities.prepareLogReportForQueue(updateFriendAndFamilyResponse.getLogsReport());

		}
		return updateFriendAndFamilyResponse;
	}
}
