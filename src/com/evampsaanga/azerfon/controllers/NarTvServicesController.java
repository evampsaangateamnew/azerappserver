/**
 * 
 */
package com.evampsaanga.azerfon.controllers;

import java.io.IOException;
import java.sql.SQLException;

import javax.jms.JMSException;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.evampsaanga.azerfon.business.NarTvServicesBusiness;
import com.evampsaanga.azerfon.common.messagesmappings.GetMessagesMappings;
import com.evampsaanga.azerfon.common.utilities.Constants;
import com.evampsaanga.azerfon.common.utilities.Transactions;
import com.evampsaanga.azerfon.common.utilities.Utilities;
import com.evampsaanga.azerfon.common.utilities.Validator;
import com.evampsaanga.azerfon.models.nartvservices.getsubscriptions.GetSubscriptionsRequest;
import com.evampsaanga.azerfon.models.nartvservices.getsubscriptions.GetSubscriptionsResponse;
import com.evampsaanga.azerfon.models.nartvservices.migration.MigrationRequest;
import com.evampsaanga.azerfon.models.nartvservices.migration.MigrationResponse;

/**
 * @author Evamp & Saanga
 * 
 */

@RestController
@RequestMapping("/nartvservices")
public class NarTvServicesController {

	Logger logger = Logger.getLogger(NarTvServicesController.class);

	@RequestMapping(value = "/getsubscriptions", method = RequestMethod.POST, headers = Constants.CONTENT_TYPE_DEFAULT_VALUE)
	public GetSubscriptionsResponse getSubscriptions(@RequestBody String data,
			@RequestHeader(value = Constants.TOKEN_KEY, defaultValue = Constants.TOKEN_DEFAULT_VALUE) String token,
			@RequestHeader(value = Constants.LANGUAGE_KEY, defaultValue = Constants.LANGUAGE_DEFAULT_VALUE) String lang,
			@RequestHeader(value = Constants.USER_AGENT_KEY, defaultValue = Constants.USER_AGENT_DEFAULT_VALUE) String userAgent,
			@RequestHeader(value = Constants.SUBSCRIBER_TYPE_KEY, defaultValue = Constants.SUBSCRIBER_TYPE_DEFAULT_VALUE) String userType,
			@RequestHeader(value = Constants.TARIFF_TYPE_KEY, defaultValue = Constants.TARIFF_TYPE_DEFAULT_VALUE) String tariffType,
			HttpServletRequest servletRequest) throws ClassNotFoundException, SQLException, JsonParseException,
			JsonMappingException, IOException, JMSException {

		ObjectMapper mapper = new ObjectMapper();
		NarTvServicesBusiness narTvServicesBusiness = new NarTvServicesBusiness();
		GetSubscriptionsRequest getSubscriptionsRequest = new GetSubscriptionsRequest();
		GetSubscriptionsResponse getSubscriptionsResponse = new GetSubscriptionsResponse();

		String TRANSACTION_NAME = Transactions.NAR_TV_GET_SUBSCRIPTIONS + " CONTROLLER";
		String requestTime = Utilities.getReportDateTime();
		try {

			String msisdn = Utilities.getValueFromJSON(Utilities.splitToken(Utilities.decodeString(token)).toString(),
					Constants.MSISDN_KEY);

			// Populating report object before processing business logic.
			getSubscriptionsResponse.setLogsReport(Utilities.preBusinessReportLoggingData(token, msisdn,
					TRANSACTION_NAME, servletRequest.getRemoteAddr(), userAgent, userType, tariffType, data, lang,
					getSubscriptionsResponse.getLogsReport()));

			data = Utilities.addAttributesToRequest(data, msisdn, servletRequest, lang, userAgent);

			Utilities.printInfoLog(msisdn + "-Request Landed in-" + TRANSACTION_NAME, logger);
			Utilities.printInfoLog(msisdn + "-Request Data-" + data, logger);

			getSubscriptionsRequest = mapper.readValue(data, GetSubscriptionsRequest.class);

			String requestValidationStatus = Validator.validateRequest(msisdn, getSubscriptionsRequest);

			if (requestValidationStatus.equalsIgnoreCase(Constants.VALIDATION_SUCCESS_CODE)) {

				getSubscriptionsResponse = narTvServicesBusiness.getSubscriptions(msisdn, getSubscriptionsRequest,
						getSubscriptionsResponse);
			} else {

				getSubscriptionsResponse.setCallStatus(Constants.Call_Status_False);
				getSubscriptionsResponse.setResultCode(Constants.VALIDATION_FAILURE_CODE);
				getSubscriptionsResponse.setResultDesc(requestValidationStatus);
			}

			getSubscriptionsResponse.getLogsReport().setResponseCode(getSubscriptionsResponse.getResultCode());
			getSubscriptionsResponse.getLogsReport().setRequestTime(requestTime);
			getSubscriptionsResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
			getSubscriptionsResponse.getLogsReport().setResponse(mapper.writeValueAsString(getSubscriptionsResponse));

			// Sending report log into queue.
			Utilities.prepareLogReportForQueue(getSubscriptionsResponse.getLogsReport());

			Utilities.printInfoLog(msisdn + "-Response Returned from-" + TRANSACTION_NAME + "-"
					+ mapper.writeValueAsString(getSubscriptionsResponse), logger);

		} catch (Exception e) {

			Utilities.printErrorLog(Utilities.convertStackTraceToString(e), logger);
			getSubscriptionsResponse.setCallStatus(Constants.Call_Status_False);

			getSubscriptionsResponse.setResultCode(Constants.EXCEPTION_CODE);
			getSubscriptionsResponse
					.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("unexpected.error", lang));

			getSubscriptionsResponse.getLogsReport().setResponseCode(getSubscriptionsResponse.getResultCode());
			getSubscriptionsResponse.getLogsReport().setRequestTime(requestTime);
			getSubscriptionsResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
			getSubscriptionsResponse.getLogsReport().setResponse(mapper.writeValueAsString(getSubscriptionsResponse));

			// Sending report log into queue.
			Utilities.prepareLogReportForQueue(getSubscriptionsResponse.getLogsReport());
		}
		return getSubscriptionsResponse;
	}

	@RequestMapping(value = "/migration", method = RequestMethod.POST, headers = Constants.CONTENT_TYPE_DEFAULT_VALUE)
	public MigrationResponse migration(@RequestBody String data,
			@RequestHeader(value = Constants.TOKEN_KEY, defaultValue = Constants.TOKEN_DEFAULT_VALUE) String token,
			@RequestHeader(value = Constants.LANGUAGE_KEY, defaultValue = Constants.LANGUAGE_DEFAULT_VALUE) String lang,
			@RequestHeader(value = Constants.USER_AGENT_KEY, defaultValue = Constants.USER_AGENT_DEFAULT_VALUE) String userAgent,
			@RequestHeader(value = Constants.SUBSCRIBER_TYPE_KEY, defaultValue = Constants.SUBSCRIBER_TYPE_DEFAULT_VALUE) String userType,
			@RequestHeader(value = Constants.TARIFF_TYPE_KEY, defaultValue = Constants.TARIFF_TYPE_DEFAULT_VALUE) String tariffType,
			HttpServletRequest servletRequest) throws ClassNotFoundException, SQLException, JsonParseException,
			JsonMappingException, IOException, JMSException {

		ObjectMapper mapper = new ObjectMapper();
		MigrationRequest migrationRequest = new MigrationRequest();
		MigrationResponse migrationResponse = new MigrationResponse();
		NarTvServicesBusiness narTvServicesBusiness = new NarTvServicesBusiness();

		String TRANSACTION_NAME = Transactions.NAR_TV_MIGRATE + " CONTROLLER";
		String requestTime = Utilities.getReportDateTime();
		try {

			String msisdn = Utilities.getValueFromJSON(Utilities.splitToken(Utilities.decodeString(token)).toString(),
					Constants.MSISDN_KEY);

			// Populating report object before processing business logic.
			migrationResponse.setLogsReport(Utilities.preBusinessReportLoggingData(token, msisdn, TRANSACTION_NAME,
					servletRequest.getRemoteAddr(), userAgent, userType, tariffType, data, lang,
					migrationResponse.getLogsReport()));

			data = Utilities.addAttributesToRequest(data, msisdn, servletRequest, lang, userAgent);

			Utilities.printInfoLog(msisdn + "-Request Landed in-" + TRANSACTION_NAME, logger);
			Utilities.printInfoLog(msisdn + "-Request Data-" + data, logger);

			migrationRequest = mapper.readValue(data, MigrationRequest.class);

			String requestValidationStatus = Validator.validateRequest(msisdn, migrationRequest);

			if (requestValidationStatus.equalsIgnoreCase(Constants.VALIDATION_SUCCESS_CODE)) {

				migrationResponse = narTvServicesBusiness.migrationBusiness(msisdn, migrationRequest,
						migrationResponse);
			} else {

				migrationResponse.setCallStatus(Constants.Call_Status_False);
				migrationResponse.setResultCode(Constants.VALIDATION_FAILURE_CODE);
				migrationResponse.setResultDesc(requestValidationStatus);
			}

			migrationResponse.getLogsReport().setResponseCode(migrationResponse.getResultCode());
			migrationResponse.getLogsReport().setRequestTime(requestTime);
			migrationResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
			migrationResponse.getLogsReport().setResponse(mapper.writeValueAsString(migrationResponse));

			// Sending report log into queue.
			Utilities.prepareLogReportForQueue(migrationResponse.getLogsReport());

			Utilities.printInfoLog(msisdn + "-Response Returned from-" + TRANSACTION_NAME + "-"
					+ mapper.writeValueAsString(migrationResponse), logger);

		} catch (Exception e) {

			Utilities.printErrorLog(Utilities.convertStackTraceToString(e), logger);
			migrationResponse.setCallStatus(Constants.Call_Status_False);

			migrationResponse.setResultCode(Constants.EXCEPTION_CODE);
			migrationResponse.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("unexpected.error", lang));

			migrationResponse.getLogsReport().setResponseCode(migrationResponse.getResultCode());
			migrationResponse.getLogsReport().setRequestTime(requestTime);
			migrationResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
			migrationResponse.getLogsReport().setResponse(mapper.writeValueAsString(migrationResponse));

			// Sending report log into queue.
			Utilities.prepareLogReportForQueue(migrationResponse.getLogsReport());

		}
		return migrationResponse;
	}
}