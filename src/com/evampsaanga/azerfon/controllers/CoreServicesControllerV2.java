/**
 * 
 */
package com.evampsaanga.azerfon.controllers;

import java.io.IOException;
import java.sql.SQLException;

import javax.jms.JMSException;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONObject;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.evampsaanga.azerfon.business.CoreServicesBusiness;
import com.evampsaanga.azerfon.common.messagesmappings.GetMessagesMappings;
import com.evampsaanga.azerfon.common.utilities.Constants;
import com.evampsaanga.azerfon.common.utilities.Transactions;
import com.evampsaanga.azerfon.common.utilities.Utilities;
import com.evampsaanga.azerfon.common.utilities.Validator;
import com.evampsaanga.azerfon.models.coreservices.getcoreservices.GetCoreServicesRequest;
import com.evampsaanga.azerfon.models.coreservices.getcoreservices.GetCoreServicesRequestV2;
import com.evampsaanga.azerfon.models.coreservices.getcoreservices.GetCoreServicesResponse;
import com.evampsaanga.azerfon.models.coreservices.processcoreservices.ProcessCoreServicesRequest;
import com.evampsaanga.azerfon.models.coreservices.processcoreservices.ProcessCoreServicesRequestV2;
import com.evampsaanga.azerfon.models.coreservices.processcoreservices.ProcessCoreServicesResponse;
import com.evampsaanga.azerfon.models.quickservices.sendfreesms.SendFreeSMSResponseV2;

/**
 * @author Evamp & Saanga
 *
 */
@RestController
@RequestMapping("/coreservicesV2")
public class CoreServicesControllerV2 {

	Logger logger = Logger.getLogger(CoreServicesControllerV2.class);

	@RequestMapping(value = "/getcoreservices", method = RequestMethod.POST, headers = Constants.CONTENT_TYPE_DEFAULT_VALUE)
	public GetCoreServicesResponse getcoreservices(@RequestBody String data,
			@RequestHeader(value = Constants.MSISDN_KEY, defaultValue = Constants.MSISDN_DEFAULT_VALUE) String msisdn,
			@RequestHeader(value = Constants.LANGUAGE_KEY, defaultValue = Constants.LANGUAGE_DEFAULT_VALUE) String lang,
			@RequestHeader(value = Constants.USER_AGENT_KEY, defaultValue = Constants.USER_AGENT_DEFAULT_VALUE) String userAgent,
			@RequestHeader(value = Constants.SUBSCRIBER_TYPE_KEY, defaultValue = Constants.SUBSCRIBER_TYPE_DEFAULT_VALUE) String userType,
			@RequestHeader(value = Constants.TARIFF_TYPE_KEY, defaultValue = Constants.TARIFF_TYPE_DEFAULT_VALUE) String tariffType,
			@RequestHeader(value = Constants.APP_PHASE_KEY, defaultValue = Constants.APP_PHASE_DEFAULT_VALUE) String isFromB2B,
			HttpServletRequest servletRequest) throws ClassNotFoundException, SQLException, JsonParseException,
			JsonMappingException, IOException, JMSException {
		ObjectMapper mapper = new ObjectMapper();

		CoreServicesBusiness coreServicesBusiness = new CoreServicesBusiness();
		GetCoreServicesRequest getCoreServicesRequest = new GetCoreServicesRequest();
		GetCoreServicesResponse getCoreServicesResponse = new GetCoreServicesResponse();

		String requestTime = Utilities.getReportDateTime();

		String TRANSACTION_NAME = Transactions.GET_CORE_SERVICES_TRANSACTION_NAME + " CONTROLLER";
		try {

			/*
			 * String msisdn = Utilities.getValueFromJSON(Utilities.splitToken(Utilities.
			 * decodeString(token)).toString(), Constants.MSISDN_KEY);
			 */

			// Populating report object before processing business logic.
			getCoreServicesResponse.setLogsReport(
					Utilities.preBusinessReportLoggingData("", msisdn, TRANSACTION_NAME, servletRequest.getRemoteAddr(),
							userAgent, userType, tariffType, data, lang, getCoreServicesResponse.getLogsReport()));

			data = Utilities.addAttributesToRequest(data, msisdn, servletRequest, lang, userAgent);
			Utilities.printInfoLog(msisdn + "-Request Landed in-" + TRANSACTION_NAME, logger);
			Utilities.printInfoLog(msisdn + "-Request Data-" + data, logger);
			// checking if has individual msisdn
			JSONObject jObject = new JSONObject(data);

				getCoreServicesRequest = mapper.readValue(data, GetCoreServicesRequest.class);
//				getCoreServicesRequest.setBrand(tariffType);
//				getCoreServicesRequest.setUserType(userType);
//				getCoreServicesRequest.setIsFrom("B2B");
//				getCoreServicesRequest.setIsB2B(isFromB2B);
				if (isFromB2B.equalsIgnoreCase("true")) {
					getCoreServicesRequest.setIsB2B("1");
				} else {
					getCoreServicesRequest.setIsB2B("0");
				}

				Utilities.printInfoLog(msisdn + "-CHecking IsB2B-" + getCoreServicesRequest.getIsB2B(), logger);
				String requestValidationStatus = Validator.validateRequest(msisdn, getCoreServicesRequest);

				if (requestValidationStatus.equalsIgnoreCase(Constants.VALIDATION_SUCCESS_CODE)) {

					getCoreServicesResponse = coreServicesBusiness.getCoreServicesBusiness(msisdn,
							getCoreServicesRequest, getCoreServicesResponse, lang);

				} else {
					getCoreServicesResponse.setCallStatus(Constants.Call_Status_False);
					getCoreServicesResponse.setResultCode(Constants.VALIDATION_FAILURE_CODE);
					getCoreServicesResponse.setResultDesc(requestValidationStatus);
				}
			getCoreServicesResponse.getLogsReport().setRequestTime(requestTime);
			getCoreServicesResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
			getCoreServicesResponse.getLogsReport().setResponse(mapper.writeValueAsString(getCoreServicesResponse));
			getCoreServicesResponse.getLogsReport().setResponseCode(getCoreServicesResponse.getResultCode());

			// Sending report log into queue.
			getCoreServicesResponse.getLogsReport().setAppVersion(Constants.APP_VERSION_PHASE2);
			Utilities.prepareLogReportForQueue(getCoreServicesResponse.getLogsReport());

			Utilities.printInfoLog(msisdn + "-Response Returned from-" + TRANSACTION_NAME + "-"
					+ mapper.writeValueAsString(getCoreServicesResponse), logger);
		} catch (Exception e) {
			Utilities.printErrorLog(Utilities.convertStackTraceToString(e), logger);
			getCoreServicesResponse.setCallStatus(Constants.Call_Status_False);
			getCoreServicesResponse.setResultCode(Constants.EXCEPTION_CODE);
			getCoreServicesResponse
					.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("unexpected.error", lang));

			getCoreServicesResponse.getLogsReport().setResponseCode(getCoreServicesResponse.getResultCode());
			getCoreServicesResponse.getLogsReport().setRequestTime(requestTime);
			getCoreServicesResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
			getCoreServicesResponse.getLogsReport().setResponse(mapper.writeValueAsString(getCoreServicesResponse));

			// Sending report log into queue.
			getCoreServicesResponse.getLogsReport().setAppVersion(Constants.APP_VERSION_PHASE2);
			Utilities.prepareLogReportForQueue(getCoreServicesResponse.getLogsReport());

		}
		return getCoreServicesResponse;
	}

	@RequestMapping(value = "/getindividualcoreservices", method = RequestMethod.POST, headers = Constants.CONTENT_TYPE_DEFAULT_VALUE)
	public GetCoreServicesResponse getindividualcoreservices(@RequestBody String data,
			@RequestHeader(value = Constants.MSISDN_KEY, defaultValue = Constants.MSISDN_DEFAULT_VALUE) String msisdn,
			@RequestHeader(value = Constants.LANGUAGE_KEY, defaultValue = Constants.LANGUAGE_DEFAULT_VALUE) String lang,
			@RequestHeader(value = Constants.USER_AGENT_KEY, defaultValue = Constants.USER_AGENT_DEFAULT_VALUE) String userAgent,
			@RequestHeader(value = Constants.SUBSCRIBER_TYPE_KEY, defaultValue = Constants.SUBSCRIBER_TYPE_DEFAULT_VALUE) String userType,
			@RequestHeader(value = Constants.TARIFF_TYPE_KEY, defaultValue = Constants.TARIFF_TYPE_DEFAULT_VALUE) String tariffType,
			@RequestHeader(value = Constants.APP_PHASE_KEY, defaultValue = Constants.APP_PHASE_DEFAULT_VALUE) String isFromB2B,
			HttpServletRequest servletRequest) throws ClassNotFoundException, SQLException, JsonParseException,
			JsonMappingException, IOException, JMSException {
		ObjectMapper mapper = new ObjectMapper();

		CoreServicesBusiness coreServicesBusiness = new CoreServicesBusiness();
		GetCoreServicesRequest getCoreServicesRequest = new GetCoreServicesRequest();
		GetCoreServicesResponse getCoreServicesResponse = new GetCoreServicesResponse();

		String requestTime = Utilities.getReportDateTime();

		String TRANSACTION_NAME = Transactions.GET_CORE_SERVICES_TRANSACTION_NAME + " CONTROLLER";
		try {

			/*
			 * String msisdn = Utilities.getValueFromJSON(Utilities.splitToken(Utilities.
			 * decodeString(token)).toString(), Constants.MSISDN_KEY);
			 */

			// Populating report object before processing business logic.
			getCoreServicesResponse.setLogsReport(
					Utilities.preBusinessReportLoggingData("", msisdn, TRANSACTION_NAME, servletRequest.getRemoteAddr(),
							userAgent, userType, tariffType, data, lang, getCoreServicesResponse.getLogsReport()));

			data = Utilities.addAttributesToRequest(data, msisdn, servletRequest, lang, userAgent);
			Utilities.printInfoLog(msisdn + "-Request Landed in-" + TRANSACTION_NAME, logger);
			Utilities.printInfoLog(msisdn + "-Request Data-" + data, logger);

			getCoreServicesRequest = mapper.readValue(data, GetCoreServicesRequest.class);
//				getCoreServicesRequest.setBrand(tariffType);
//				getCoreServicesRequest.setUserType(userType);
//				getCoreServicesRequest.setIsFrom("B2B");
//				getCoreServicesRequest.setIsB2B(isFromB2B);
			if (isFromB2B.equalsIgnoreCase("true")) {
				getCoreServicesRequest.setIsB2B("true");
				getCoreServicesRequest.setIsFrom("B2B");
				getCoreServicesRequest.setAccountType("");
				getCoreServicesRequest.setGroupType("");
				
			} else {
				getCoreServicesRequest.setIsB2B("0");
			}

			Utilities.printInfoLog(msisdn + "-CHecking IsB2B-" + getCoreServicesRequest.getIsB2B(), logger);
			String requestValidationStatus = Validator.validateRequest(msisdn, getCoreServicesRequest);

			if (requestValidationStatus.equalsIgnoreCase(Constants.VALIDATION_SUCCESS_CODE)) {

				getCoreServicesResponse = coreServicesBusiness.getCoreServicesBusiness(msisdn, getCoreServicesRequest,
						getCoreServicesResponse, lang);

			} else {
				getCoreServicesResponse.setCallStatus(Constants.Call_Status_False);
				getCoreServicesResponse.setResultCode(Constants.VALIDATION_FAILURE_CODE);
				getCoreServicesResponse.setResultDesc(requestValidationStatus);
			}
			getCoreServicesResponse.getLogsReport().setRequestTime(requestTime);
			getCoreServicesResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
			getCoreServicesResponse.getLogsReport().setResponse(mapper.writeValueAsString(getCoreServicesResponse));
			getCoreServicesResponse.getLogsReport().setResponseCode(getCoreServicesResponse.getResultCode());

			// Sending report log into queue.
			getCoreServicesResponse.getLogsReport().setAppVersion(Constants.APP_VERSION_PHASE2);
			Utilities.prepareLogReportForQueue(getCoreServicesResponse.getLogsReport());

			Utilities.printInfoLog(msisdn + "-Response Returned from-" + TRANSACTION_NAME + "-"
					+ mapper.writeValueAsString(getCoreServicesResponse), logger);
		} catch (Exception e) {
			Utilities.printErrorLog(Utilities.convertStackTraceToString(e), logger);
			getCoreServicesResponse.setCallStatus(Constants.Call_Status_False);
			getCoreServicesResponse.setResultCode(Constants.EXCEPTION_CODE);
			getCoreServicesResponse
					.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("unexpected.error", lang));

			getCoreServicesResponse.getLogsReport().setResponseCode(getCoreServicesResponse.getResultCode());
			getCoreServicesResponse.getLogsReport().setRequestTime(requestTime);
			getCoreServicesResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
			getCoreServicesResponse.getLogsReport().setResponse(mapper.writeValueAsString(getCoreServicesResponse));

			// Sending report log into queue.
			getCoreServicesResponse.getLogsReport().setAppVersion(Constants.APP_VERSION_PHASE2);
			Utilities.prepareLogReportForQueue(getCoreServicesResponse.getLogsReport());

		}
		return getCoreServicesResponse;
	}

	@RequestMapping(value = "/getcoreservicesbulk", method = RequestMethod.POST, headers = Constants.CONTENT_TYPE_DEFAULT_VALUE)
	public GetCoreServicesResponse getcoreservicesbulk(@RequestBody String data,
			@RequestHeader(value = Constants.MSISDN_KEY, defaultValue = Constants.MSISDN_DEFAULT_VALUE) String msisdn,
			@RequestHeader(value = Constants.LANGUAGE_KEY, defaultValue = Constants.LANGUAGE_DEFAULT_VALUE) String lang,
			@RequestHeader(value = Constants.USER_AGENT_KEY, defaultValue = Constants.USER_AGENT_DEFAULT_VALUE) String userAgent,
			@RequestHeader(value = Constants.SUBSCRIBER_TYPE_KEY, defaultValue = Constants.SUBSCRIBER_TYPE_DEFAULT_VALUE) String userType,
			@RequestHeader(value = Constants.TARIFF_TYPE_KEY, defaultValue = Constants.TARIFF_TYPE_DEFAULT_VALUE) String tariffType,
			@RequestHeader(value = Constants.APP_PHASE_KEY, defaultValue = Constants.APP_PHASE_DEFAULT_VALUE) String isFromB2B,
			HttpServletRequest servletRequest) throws ClassNotFoundException, SQLException, JsonParseException,
			JsonMappingException, IOException, JMSException {
		ObjectMapper mapper = new ObjectMapper();

		CoreServicesBusiness coreServicesBusiness = new CoreServicesBusiness();
		GetCoreServicesRequest getCoreServicesRequest = new GetCoreServicesRequest();
		GetCoreServicesResponse getCoreServicesResponse = new GetCoreServicesResponse();

		String requestTime = Utilities.getReportDateTime();

		String TRANSACTION_NAME = Transactions.GET_CORE_SERVICES_TRANSACTION_NAME + " CONTROLLER";
		try {

			/*
			 * String msisdn = Utilities.getValueFromJSON(Utilities.splitToken(Utilities.
			 * decodeString(token)).toString(), Constants.MSISDN_KEY);
			 */

			// Populating report object before processing business logic.
			getCoreServicesResponse.setLogsReport(
					Utilities.preBusinessReportLoggingData("", msisdn, TRANSACTION_NAME, servletRequest.getRemoteAddr(),
							userAgent, userType, tariffType, data, lang, getCoreServicesResponse.getLogsReport()));

			data = Utilities.addAttributesToRequest(data, msisdn, servletRequest, lang, userAgent);

			Utilities.printInfoLog(msisdn + "-Request Landed in-" + TRANSACTION_NAME, logger);
			Utilities.printInfoLog(msisdn + "-Request Data-" + data, logger);
			if (Utilities.hasJSONKey(data, "accountType")) {

				getCoreServicesRequest = mapper.readValue(data, GetCoreServicesRequest.class);
				getCoreServicesRequest.setBrand(tariffType);
				getCoreServicesRequest.setUserType(userType);
				getCoreServicesRequest.setIsFrom("B2B");
				getCoreServicesRequest.setIsB2B(isFromB2B);
				String requestValidationStatus = Validator.validateRequest(msisdn, getCoreServicesRequest);

				if (requestValidationStatus.equalsIgnoreCase(Constants.VALIDATION_SUCCESS_CODE)) {

					getCoreServicesResponse = coreServicesBusiness.getCoreServicesBusiness(msisdn + "-bulk",
							getCoreServicesRequest, getCoreServicesResponse, lang);

				} else {
					getCoreServicesResponse.setCallStatus(Constants.Call_Status_False);
					getCoreServicesResponse.setResultCode(Constants.VALIDATION_FAILURE_CODE);
					getCoreServicesResponse.setResultDesc(requestValidationStatus);
				}
			} else {
				getCoreServicesResponse.setCallStatus(Constants.Call_Status_False);
				getCoreServicesResponse.setResultCode(Constants.API_FAILURE_CODE);
				getCoreServicesResponse.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle(
						"appNewVersion.feature.", Utilities.getValueFromJSON(data, "lang")));
			}
			getCoreServicesResponse.getLogsReport().setRequestTime(requestTime);
			getCoreServicesResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
			getCoreServicesResponse.getLogsReport().setResponse(mapper.writeValueAsString(getCoreServicesResponse));
			getCoreServicesResponse.getLogsReport().setResponseCode(getCoreServicesResponse.getResultCode());

			// Sending report log into queue.
			getCoreServicesResponse.getLogsReport().setAppVersion(Constants.APP_VERSION_PHASE2);
			Utilities.prepareLogReportForQueue(getCoreServicesResponse.getLogsReport());

			Utilities.printInfoLog(msisdn + "-Response Returned from-" + TRANSACTION_NAME + "-"
					+ mapper.writeValueAsString(getCoreServicesResponse), logger);
		} catch (Exception e) {
			Utilities.printErrorLog(Utilities.convertStackTraceToString(e), logger);
			getCoreServicesResponse.setCallStatus(Constants.Call_Status_False);
			getCoreServicesResponse.setResultCode(Constants.EXCEPTION_CODE);
			getCoreServicesResponse
					.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("unexpected.error", lang));

			getCoreServicesResponse.getLogsReport().setResponseCode(getCoreServicesResponse.getResultCode());
			getCoreServicesResponse.getLogsReport().setRequestTime(requestTime);
			getCoreServicesResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
			getCoreServicesResponse.getLogsReport().setResponse(mapper.writeValueAsString(getCoreServicesResponse));

			// Sending report log into queue.
			getCoreServicesResponse.getLogsReport().setAppVersion(Constants.APP_VERSION_PHASE2);
			Utilities.prepareLogReportForQueue(getCoreServicesResponse.getLogsReport());

		}
		return getCoreServicesResponse;
	}

	@RequestMapping(value = "/processcoreservices", method = RequestMethod.POST, headers = Constants.CONTENT_TYPE_DEFAULT_VALUE)
	public ProcessCoreServicesResponse processCoreServices(@RequestBody String data,
			@RequestHeader(value = Constants.MSISDN_KEY, defaultValue = Constants.MSISDN_DEFAULT_VALUE) String msisdn,
			@RequestHeader(value = Constants.LANGUAGE_KEY, defaultValue = Constants.LANGUAGE_DEFAULT_VALUE) String lang,
			@RequestHeader(value = Constants.USER_AGENT_KEY, defaultValue = Constants.USER_AGENT_DEFAULT_VALUE) String userAgent,
			@RequestHeader(value = Constants.SUBSCRIBER_TYPE_KEY, defaultValue = Constants.SUBSCRIBER_TYPE_DEFAULT_VALUE) String userType,
			@RequestHeader(value = Constants.TARIFF_TYPE_KEY, defaultValue = Constants.TARIFF_TYPE_DEFAULT_VALUE) String tariffType,
			@RequestHeader(value = Constants.APP_PHASE_KEY, defaultValue = Constants.APP_PHASE_DEFAULT_VALUE) String isFromB2B,
			HttpServletRequest servletRequest) throws ClassNotFoundException, SQLException, JsonParseException,
			JsonMappingException, IOException, JMSException {
		ObjectMapper mapper = new ObjectMapper();
		CoreServicesBusiness coreServicesBusiness = new CoreServicesBusiness();
		ProcessCoreServicesRequest processCoreServicesRequest = new ProcessCoreServicesRequest();
		ProcessCoreServicesResponse processCoreServicesResponse = new ProcessCoreServicesResponse();

		String requestTime = Utilities.getReportDateTime();

		String TRANSACTION_NAME = Transactions.MANIPULATE_CORE_SERVICES_TRANSACTION_NAME + " CONTROLLER";
		try {
			/*
			 * String msisdn = Utilities.getValueFromJSON(Utilities.splitToken(Utilities.
			 * decodeString(token)).toString(), Constants.MSISDN_KEY);
			 */

			// Populating report object before processing business logic.
			processCoreServicesResponse.setLogsReport(
					Utilities.preBusinessReportLoggingData("", msisdn, TRANSACTION_NAME, servletRequest.getRemoteAddr(),
							userAgent, userType, tariffType, data, lang, processCoreServicesResponse.getLogsReport()));

			data = Utilities.addAttributesToRequest(data, msisdn, servletRequest, lang, userAgent);

			Utilities.printInfoLog(msisdn + "-Request Landed in-" + TRANSACTION_NAME, logger);
			Utilities.printInfoLog(msisdn + "-Request Data-" + data, logger);
			Utilities.printInfoLog(msisdn + "-BRAND-" + tariffType, logger);
			Utilities.printInfoLog(msisdn + "-USER TYPE-" + userType, logger);

			JSONObject jObject = new JSONObject(data);
			if (jObject.has("individualMsisdn")) {
				jObject.remove("msisdn");
				jObject.put("msisdn", jObject.get("individualMsisdn"));
				jObject.remove("individualMsisdn");
			}
			if (Utilities.hasJSONKey(data, "accountType")) {
				processCoreServicesRequest = mapper.readValue(jObject.toString(), ProcessCoreServicesRequest.class);
				processCoreServicesRequest.setBrand(tariffType);
				processCoreServicesRequest.setUserType(userType);
				// Specific params for report log
				processCoreServicesResponse.getLogsReport()
						.setCallForwardNumber(processCoreServicesRequest.getNumber());
				String requestValidationStatus = Validator.validateRequest(msisdn, processCoreServicesRequest);

				if (requestValidationStatus.equalsIgnoreCase(Constants.VALIDATION_SUCCESS_CODE)) {

					processCoreServicesResponse = coreServicesBusiness.processCoreServicesBusiness(msisdn,
							processCoreServicesRequest, processCoreServicesResponse);

				} else {
					processCoreServicesResponse.setCallStatus(Constants.Call_Status_False);
					processCoreServicesResponse.setResultCode(Constants.VALIDATION_FAILURE_CODE);
					processCoreServicesResponse.setResultDesc(requestValidationStatus);
				}
			} else {
				processCoreServicesResponse.setCallStatus(Constants.Call_Status_False);
				processCoreServicesResponse.setResultCode(Constants.API_FAILURE_CODE);
				processCoreServicesResponse.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle(
						"appNewVersion.feature.", Utilities.getValueFromJSON(data, "lang")));
			}
			processCoreServicesResponse.getLogsReport().setRequestTime(requestTime);
			processCoreServicesResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
			processCoreServicesResponse.getLogsReport()
					.setResponse(mapper.writeValueAsString(processCoreServicesResponse));
			processCoreServicesResponse.getLogsReport().setResponseCode(processCoreServicesResponse.getResultCode());

			// Sending report log into queue.
			processCoreServicesResponse.getLogsReport().setAppVersion(Constants.APP_VERSION_PHASE2);
			Utilities.prepareLogReportForQueue(processCoreServicesResponse.getLogsReport());

			Utilities.printInfoLog(msisdn + "-Response Returned from-" + TRANSACTION_NAME + "-"
					+ mapper.writeValueAsString(processCoreServicesResponse), logger);
		} catch (Exception e) {
			Utilities.printErrorLog(Utilities.convertStackTraceToString(e), logger);
			processCoreServicesResponse.setCallStatus(Constants.Call_Status_False);
			processCoreServicesResponse.setResultCode(Constants.EXCEPTION_CODE);
			processCoreServicesResponse
					.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("unexpected.error", lang));

			processCoreServicesResponse.getLogsReport().setResponseCode(processCoreServicesResponse.getResultCode());
			processCoreServicesResponse.getLogsReport().setRequestTime(requestTime);
			processCoreServicesResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
			processCoreServicesResponse.getLogsReport()
					.setResponse(mapper.writeValueAsString(processCoreServicesResponse));

			// Sending report log into queue.
			processCoreServicesResponse.getLogsReport().setAppVersion(Constants.APP_VERSION_PHASE2);
			Utilities.prepareLogReportForQueue(processCoreServicesResponse.getLogsReport());

		}
		return processCoreServicesResponse;
	}

	@RequestMapping(value = "/processcoreservicesbulk", method = RequestMethod.POST, headers = Constants.CONTENT_TYPE_DEFAULT_VALUE)
	public SendFreeSMSResponseV2 processCoreServicesbulk(@RequestBody String data,
			@RequestHeader(value = Constants.MSISDN_KEY, defaultValue = Constants.MSISDN_DEFAULT_VALUE) String msisdn,
			@RequestHeader(value = Constants.LANGUAGE_KEY, defaultValue = Constants.LANGUAGE_DEFAULT_VALUE) String lang,
			@RequestHeader(value = Constants.USER_AGENT_KEY, defaultValue = Constants.USER_AGENT_DEFAULT_VALUE) String userAgent,
			@RequestHeader(value = Constants.SUBSCRIBER_TYPE_KEY, defaultValue = Constants.SUBSCRIBER_TYPE_DEFAULT_VALUE) String userType,
			@RequestHeader(value = Constants.TARIFF_TYPE_KEY, defaultValue = Constants.TARIFF_TYPE_DEFAULT_VALUE) String tariffType,
			@RequestHeader(value = Constants.APP_PHASE_KEY, defaultValue = Constants.APP_PHASE_DEFAULT_VALUE) String isFromB2B,
			HttpServletRequest servletRequest) throws ClassNotFoundException, SQLException, JsonParseException,
			JsonMappingException, IOException, JMSException {
		ObjectMapper mapper = new ObjectMapper();
		CoreServicesBusiness coreServicesBusiness = new CoreServicesBusiness();
		ProcessCoreServicesRequestV2 processCoreServicesRequest = new ProcessCoreServicesRequestV2();
		SendFreeSMSResponseV2 processCoreServicesResponse = new SendFreeSMSResponseV2();
		Utilities.printInfoLog(msisdn + "-Landed in processCreServicesBulk with Request Data-" + data, logger);
		String requestTime = Utilities.getReportDateTime();

		String TRANSACTION_NAME = Transactions.MANIPULATE_BULK_CORE_SERVICES_TRANSACTION_NAME + " CONTROLLER";
		try {
			/*
			 * String msisdn = Utilities.getValueFromJSON(Utilities.splitToken(Utilities.
			 * decodeString(token)).toString(), Constants.MSISDN_KEY);
			 */

			// Populating report object before processing business logic.
			processCoreServicesResponse.setLogsReport(
					Utilities.preBusinessReportLoggingData("", msisdn, TRANSACTION_NAME, servletRequest.getRemoteAddr(),
							userAgent, userType, tariffType, data, lang, processCoreServicesResponse.getLogsReport()));

			data = Utilities.addAttributesToRequest(data, msisdn, servletRequest, lang, userAgent);

			Utilities.printInfoLog(msisdn + "-Request Landed in-" + TRANSACTION_NAME, logger);
			Utilities.printInfoLog(msisdn + "-Request Data-" + data, logger);
			if (Utilities.hasJSONKey(data, "users")) {
				// Specific params for report log
				processCoreServicesRequest = mapper.readValue(data, ProcessCoreServicesRequestV2.class);
				String requestValidationStatus = Validator.validateRequest(msisdn, processCoreServicesRequest);

				if (requestValidationStatus.equalsIgnoreCase(Constants.VALIDATION_SUCCESS_CODE)) {
					processCoreServicesRequest.setIsB2B(isFromB2B);
					processCoreServicesResponse = coreServicesBusiness.processCoreServicesBusiness(msisdn,
							processCoreServicesRequest, processCoreServicesResponse);

				} else {
					processCoreServicesResponse.setCallStatus(Constants.Call_Status_False);
					processCoreServicesResponse.setResultCode(Constants.VALIDATION_FAILURE_CODE);
					processCoreServicesResponse.setResultDesc(requestValidationStatus);
				}
			} else {
				processCoreServicesResponse.setCallStatus(Constants.Call_Status_False);
				processCoreServicesResponse.setResultCode(Constants.API_FAILURE_CODE);
				processCoreServicesResponse.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle(
						"appNewVersion.feature.", Utilities.getValueFromJSON(data, "lang")));
			}
			processCoreServicesResponse.getLogsReport().setRequestTime(requestTime);
			processCoreServicesResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
			processCoreServicesResponse.getLogsReport()
					.setResponse(mapper.writeValueAsString(processCoreServicesResponse));
			processCoreServicesResponse.getLogsReport().setResponseCode(processCoreServicesResponse.getResultCode());

			// Sending report log into queue.
			processCoreServicesResponse.getLogsReport().setAppVersion(Constants.APP_VERSION_PHASE2);
			Utilities.prepareLogReportForQueue(processCoreServicesResponse.getLogsReport());

			Utilities.printInfoLog(msisdn + "-Response Returned from-" + TRANSACTION_NAME + "-"
					+ mapper.writeValueAsString(processCoreServicesResponse), logger);
		} catch (Exception e) {
			Utilities.printErrorLog(Utilities.convertStackTraceToString(e), logger);
			processCoreServicesResponse.setCallStatus(Constants.Call_Status_False);
			processCoreServicesResponse.setResultCode(Constants.EXCEPTION_CODE);
			processCoreServicesResponse
					.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("unexpected.error", lang));

			processCoreServicesResponse.getLogsReport().setResponseCode(processCoreServicesResponse.getResultCode());
			processCoreServicesResponse.getLogsReport().setRequestTime(requestTime);
			processCoreServicesResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
			processCoreServicesResponse.getLogsReport()
					.setResponse(mapper.writeValueAsString(processCoreServicesResponse));

			// Sending report log into queue.
			processCoreServicesResponse.getLogsReport().setAppVersion(Constants.APP_VERSION_PHASE2);
			Utilities.prepareLogReportForQueue(processCoreServicesResponse.getLogsReport());

		}
		return processCoreServicesResponse;
	}

}
