/**
 * 
 */
package com.evampsaanga.azerfon.controllers;

import java.io.IOException;
import java.net.SocketException;
import java.sql.SQLException;
import java.util.Date;

import javax.jms.JMSException;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.evampsaanga.azerfon.business.TariffBusiness;
import com.evampsaanga.azerfon.common.messagesmappings.GetMessagesMappings;
import com.evampsaanga.azerfon.common.utilities.Constants;
import com.evampsaanga.azerfon.common.utilities.Transactions;
import com.evampsaanga.azerfon.common.utilities.Utilities;
import com.evampsaanga.azerfon.common.utilities.Validator;
import com.evampsaanga.azerfon.models.tariffdetails.TariffRequestV2;
import com.evampsaanga.azerfon.models.tariffdetails.changetariff.ChangeTariffRequestBulk;
import com.evampsaanga.azerfon.models.tariffdetails.changetariff.ChangeTariffResponseBulk;
import com.evampsaanga.azerfon.models.tariffdetails.cug.CloseUserGroupRequest;
import com.evampsaanga.azerfon.models.tariffdetails.cug.CloseUserGroupResponseBase;
import com.evampsaanga.azerfon.models.tariffdetails.cug.CloseUsersGroupResponse;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * @author Evamp & Saanga
 *
 */

@RestController
@RequestMapping("/tariffservicesV2")
public class TariffsControllerV2 {

	Logger logger = Logger.getLogger(TariffsControllerV2.class);

	@RequestMapping(value = "/gettariffdetails", method = RequestMethod.POST, headers = Constants.CONTENT_TYPE_DEFAULT_VALUE)
	public com.evampsaanga.azerfon.models.tariffdetails.TariffResponse getTariffDetails(@RequestBody String data,
			@RequestHeader(value = Constants.MSISDN_KEY, defaultValue = Constants.MSISDN_DEFAULT_VALUE) String msisdn,
			@RequestHeader(value = Constants.LANGUAGE_KEY, defaultValue = Constants.LANGUAGE_DEFAULT_VALUE) String lang,
			@RequestHeader(value = Constants.USER_AGENT_KEY, defaultValue = Constants.USER_AGENT_DEFAULT_VALUE) String userAgent,
			@RequestHeader(value = Constants.SUBSCRIBER_TYPE_KEY, defaultValue = Constants.SUBSCRIBER_TYPE_DEFAULT_VALUE) String userType,
			@RequestHeader(value = Constants.TARIFF_TYPE_KEY, defaultValue = Constants.TARIFF_TYPE_DEFAULT_VALUE) String tariffType,
			@RequestHeader(value = Constants.APP_PHASE_KEY, defaultValue = Constants.APP_PHASE_DEFAULT_VALUE) String isFromB2B,
			HttpServletRequest servletRequest) throws ClassNotFoundException, SQLException, JsonParseException,
			JsonMappingException, IOException, JMSException {

		ObjectMapper mapper = new ObjectMapper();
		TariffRequestV2 tariffRequest = new TariffRequestV2();
		com.evampsaanga.azerfon.models.tariffdetails.TariffResponse tariffResponse = new com.evampsaanga.azerfon.models.tariffdetails.TariffResponse();
		TariffBusiness tariffBusiness = new TariffBusiness();

		String TRANSACTION_NAME = Transactions.TARRIF_DETAILS_TRANSACTION_NAME + " CONTROLLER";
		String requestTime = Utilities.getReportDateTime();
		try {

			/*
			 * String msisdn =
			 * Utilities.getValueFromJSON(Utilities.splitToken(Utilities.
			 * decodeString(token)).toString(), Constants.MSISDN_KEY);
			 */

			// Populating report object before processing business logic.
			tariffResponse.setLogsReport(
					Utilities.preBusinessReportLoggingData("", msisdn, TRANSACTION_NAME, servletRequest.getRemoteAddr(),
							userAgent, userType, tariffType, data, lang, tariffResponse.getLogsReport()));
			data = Utilities.addAttributesToRequest(data, msisdn, servletRequest, lang, userAgent);
			Utilities.printInfoLog(msisdn + "-Request Landed in- TariffsControllerV2" + TRANSACTION_NAME, logger);
			Utilities.printInfoLog(msisdn + "-Request Data TariffsControllerV2-" + data, logger);

			tariffRequest = mapper.readValue(data, TariffRequestV2.class);
			/*
			 * // Store ID value is same as of Language ID.
			 * tariffRequest.setStoreId(tariffRequest.getLang());
			 */

			String requestValidationStatus = Validator.validateRequest(msisdn, tariffRequest);
			if (requestValidationStatus.equalsIgnoreCase(Constants.VALIDATION_SUCCESS_CODE)) {
				tariffRequest.setIsB2B(isFromB2B);
				tariffResponse = tariffBusiness.getTariffDetailsBusinessV2(msisdn, tariffRequest, tariffResponse);
				tariffResponse.getLogsReport().setResponseCode(tariffResponse.getResultCode());
				tariffResponse.getLogsReport().setRequestTime(requestTime);
				tariffResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
				tariffResponse.getLogsReport().setResponse(mapper.writeValueAsString(tariffResponse));

				// Sending report log into queue.
				tariffResponse.getLogsReport().setAppVersion(Constants.APP_VERSION_PHASE2);
				Utilities.prepareLogReportForQueue(tariffResponse.getLogsReport());
			} else {
				tariffResponse.setCallStatus(Constants.Call_Status_False);
				tariffResponse.setResultCode(Constants.VALIDATION_FAILURE_CODE);
				tariffResponse.setResultDesc(requestValidationStatus);
				tariffResponse.getLogsReport().setResponseCode(tariffResponse.getResultCode());
				tariffResponse.getLogsReport().setRequestTime(requestTime);
				tariffResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
				tariffResponse.getLogsReport().setResponse(mapper.writeValueAsString(tariffResponse));

				// Sending report log into queue.
				tariffResponse.getLogsReport().setAppVersion(Constants.APP_VERSION_PHASE2);
				Utilities.prepareLogReportForQueue(tariffResponse.getLogsReport());
			}
		} catch (Exception e) {
			logger.error("ERROR:", e);
			// Utilities.printErrorLog(Utilities.convertStackTraceToString(e),
			// logger);
			tariffResponse.setCallStatus(Constants.Call_Status_False);
			tariffResponse.setResultCode(Constants.EXCEPTION_CODE);
			// tariffResponse.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("unexpected.error",
			// lang));

			tariffResponse.getLogsReport().setResponseCode(tariffResponse.getResultCode());
			tariffResponse.getLogsReport().setRequestTime(requestTime);
			tariffResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
			tariffResponse.getLogsReport().setResponse(mapper.writeValueAsString(tariffResponse));

			// Sending report log into queue.
			tariffResponse.getLogsReport().setAppVersion(Constants.APP_VERSION_PHASE2);
			Utilities.prepareLogReportForQueue(tariffResponse.getLogsReport());

		}
		return tariffResponse;
	}

	/*private void sortTariffResponse(TariffResponse tariffResponse) {
		TariffResponseData tariffResponseData = tariffResponse.getData();
		if (tariffResponseData != null) {
			// Sort prepaid tariff
			if (tariffResponseData.getPrepaid() != null) {
				// Sort prepaid cin tariff
				if (tariffResponseData.getPrepaid().getCin() != null
						&& tariffResponseData.getPrepaid().getCin().size() > 0) {
					Collections.sort(tariffResponseData.getPrepaid().getCin(), new Comparator<Cin>() {
						@Override
						public int compare(Cin o1, Cin o2) {

							if (o1.getHeader() != null && o2.getHeader() != null
									&& o1.getHeader().getSortOrder() != null && o2.getHeader().getSortOrder() != null) {
								return o1.getHeader().getSortOrder().compareTo(o2.getHeader().getSortOrder());
							} else {
								return Constants.MAX_INT;
							}
						}
					});
				}
				// Sort prepaid klass tariff
				if (tariffResponseData.getPrepaid().getKlass() != null
						&& tariffResponseData.getPrepaid().getKlass().size() > 0) {
					Collections.sort(tariffResponseData.getPrepaid().getKlass(), new Comparator<Klass>() {
						@Override
						public int compare(Klass o1, Klass o2) {
							if (o1.getHeader() != null && o2.getHeader() != null
									&& o1.getHeader().getSortOrder() != null && o2.getHeader().getSortOrder() != null) {
								return o1.getHeader().getSortOrder().compareTo(o2.getHeader().getSortOrder());
							} else {
								return Constants.MAX_INT;
							}
						}
					});
				}
			}

			if (tariffResponseData.getPostpaid() != null) {
				// Sort postpaid Corporate tariff
				if (tariffResponseData.getPostpaid().getCorporate() != null
						&& tariffResponseData.getPostpaid().getCorporate().size() > 0) {
					Collections.sort(tariffResponseData.getPostpaid().getCorporate(), new Comparator<Corporate>() {
						@Override
						public int compare(Corporate o1, Corporate o2) {
							if (o1.getHeader() != null && o2.getHeader() != null
									&& o1.getHeader().getSortOrder() != null && o2.getHeader().getSortOrder() != null) {
								return o1.getHeader().getSortOrder().compareTo(o2.getHeader().getSortOrder());
							} else {
								return Constants.MAX_INT;
							}
						}
					});
				}

				// Sort postpaid Individual tariff
				if (tariffResponseData.getPostpaid().getIndividual() != null
						&& tariffResponseData.getPostpaid().getIndividual().size() > 0) {
					Collections.sort(tariffResponseData.getPostpaid().getIndividual(), new Comparator<Individual>() {
						@Override
						public int compare(Individual o1, Individual o2) {
							if (o1.getHeader() != null && o2.getHeader() != null
									&& o1.getHeader().getSortOrder() != null && o2.getHeader().getSortOrder() != null) {
								return o1.getHeader().getSortOrder().compareTo(o2.getHeader().getSortOrder());
							} else {
								return Constants.MAX_INT;
							}
						}
					});
				}

				// Sort postpaid Klass tariff
				if (tariffResponseData.getPostpaid().getKlassPostpaid() != null
						&& tariffResponseData.getPostpaid().getKlassPostpaid().size() > 0) {
					Collections.sort(tariffResponseData.getPostpaid().getKlassPostpaid(), new Comparator<Klass>() {
						@Override
						public int compare(Klass o1, Klass o2) {
							if (o1.getHeader() != null && o2.getHeader() != null
									&& o1.getHeader().getSortOrder() != null && o2.getHeader().getSortOrder() != null) {
								return o1.getHeader().getSortOrder().compareTo(o2.getHeader().getSortOrder());
							} else {
								return Constants.MAX_INT;
							}
						}
					});
				}
			}
		}
	}
*/
	
	@RequestMapping(value = "/changetariffbulk", method = RequestMethod.POST, headers = Constants.CONTENT_TYPE_DEFAULT_VALUE)
	public ChangeTariffResponseBulk changeTariff(@RequestBody String data,
			@RequestHeader(value = Constants.TOKEN_KEY, defaultValue = Constants.TOKEN_DEFAULT_VALUE) String token,
			@RequestHeader(value = Constants.LANGUAGE_KEY, defaultValue = Constants.LANGUAGE_DEFAULT_VALUE) String lang,
			@RequestHeader(value = Constants.USER_AGENT_KEY, defaultValue = Constants.USER_AGENT_DEFAULT_VALUE) String userAgent,
			@RequestHeader(value = Constants.SUBSCRIBER_TYPE_KEY, defaultValue = Constants.SUBSCRIBER_TYPE_DEFAULT_VALUE) String userType,
			@RequestHeader(value = Constants.TARIFF_TYPE_KEY, defaultValue = Constants.TARIFF_TYPE_DEFAULT_VALUE) String tariffType,
			@RequestHeader(value = Constants.APP_PHASE_KEY, defaultValue = Constants.APP_PHASE_DEFAULT_VALUE) String isFromB2B,
			HttpServletRequest servletRequest) throws ClassNotFoundException, SQLException, JsonParseException,
			JsonMappingException, IOException, JMSException {
		ObjectMapper mapper = new ObjectMapper();
		TariffBusiness tariffBusiness = new TariffBusiness();
		ChangeTariffRequestBulk changeTariffRequest = new ChangeTariffRequestBulk();
		ChangeTariffResponseBulk changeTariffResponse = new ChangeTariffResponseBulk();

		String TRANSACTION_NAME = Transactions.CHANGE_TARRIF_TRANSACTION_NAME + " CONTROLLER";
		String requestTime = Utilities.getReportDateTime();
		try {
			String msisdn = Utilities.getValueFromJSON(Utilities.splitToken(Utilities.decodeString(token)).toString(),
					Constants.MSISDN_KEY);

			// Populating report object before processing business logic.
			changeTariffResponse.setLogsReport(Utilities.preBusinessReportLoggingData(token, msisdn, TRANSACTION_NAME,
					servletRequest.getRemoteAddr(), userAgent, userType, tariffType, data, lang,
					changeTariffResponse.getLogsReport()));

			data = Utilities.addAttributesToRequest(data, msisdn, servletRequest, lang, userAgent);

			Utilities.printInfoLog(msisdn + "-Request Landed in-" + TRANSACTION_NAME, logger);
			Utilities.printInfoLog(msisdn + "-Request Data-" + data, logger);

			changeTariffRequest = mapper.readValue(data, ChangeTariffRequestBulk.class);

			// Logging specific params
	

			String requestValidationStatus = Validator.validateRequest(msisdn, changeTariffRequest);

			if (requestValidationStatus.equalsIgnoreCase(Constants.VALIDATION_SUCCESS_CODE)) {
				Utilities.printInfoLog(msisdn + "-AFTER VALIDATION Request Data-" + requestValidationStatus, logger);
				changeTariffRequest.setIsB2B(isFromB2B);
				changeTariffResponse = tariffBusiness.changeTariffBulkBusiness(msisdn, changeTariffRequest,
						changeTariffResponse);
			} else {
				changeTariffResponse.setCallStatus(Constants.Call_Status_False);
				changeTariffResponse.setResultCode(Constants.VALIDATION_FAILURE_CODE);
				changeTariffResponse.setResultDesc(requestValidationStatus);
			}

			changeTariffResponse.getLogsReport().setResponseCode(changeTariffResponse.getResultCode());
			changeTariffResponse.getLogsReport().setRequestTime(requestTime);
			changeTariffResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
			changeTariffResponse.getLogsReport().setResponse(mapper.writeValueAsString(changeTariffResponse));

			// Sending report log into queue.
			changeTariffResponse.getLogsReport().setAppVersion(Constants.APP_VERSION_PHASE2);
			Utilities.prepareLogReportForQueue(changeTariffResponse.getLogsReport());

			Utilities.printInfoLog(msisdn + "-Response Returned from-" + TRANSACTION_NAME + "-"
					+ mapper.writeValueAsString(changeTariffResponse), logger);
		} catch (Exception e) {
			Utilities.printErrorLog(Utilities.convertStackTraceToString(e), logger);
			changeTariffResponse.setCallStatus(Constants.Call_Status_False);
			changeTariffResponse.setResultCode(Constants.EXCEPTION_CODE);
			changeTariffResponse
					.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("unexpected.error", lang));

			changeTariffResponse.getLogsReport().setResponseCode(changeTariffResponse.getResultCode());
			changeTariffResponse.getLogsReport().setRequestTime(requestTime);
			changeTariffResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
			changeTariffResponse.getLogsReport().setResponse(mapper.writeValueAsString(changeTariffResponse));

			// Sending report log into queue.
			changeTariffResponse.getLogsReport().setAppVersion(Constants.APP_VERSION_PHASE2);
			Utilities.prepareLogReportForQueue(changeTariffResponse.getLogsReport());

		}
		return changeTariffResponse;
	}
	
	
	
	 @RequestMapping(value = "/getcugdata", method = RequestMethod.POST, headers = Constants.CONTENT_TYPE_DEFAULT_VALUE)
	    public CloseUserGroupResponseBase getCloseUserGroupData(@RequestBody String data,
	            @RequestHeader(value = Constants.TOKEN_KEY, defaultValue = Constants.TOKEN_DEFAULT_VALUE) String token,
	            @RequestHeader(value = Constants.LANGUAGE_KEY, defaultValue = Constants.LANGUAGE_DEFAULT_VALUE) String lang,
	            @RequestHeader(value = Constants.USER_AGENT_KEY, defaultValue = Constants.USER_AGENT_DEFAULT_VALUE) String UserAgent,
	            @RequestHeader(value = Constants.DEVICE_ID_KEY, defaultValue = Constants.DEVICE_ID_DEFAULT_VALUE) String deviceID,
	            @RequestHeader(value = Constants.SUBSCRIBER_TYPE_KEY, defaultValue = Constants.SUBSCRIBER_TYPE_DEFAULT_VALUE) String userType,
	            @RequestHeader(value = Constants.TARIFF_TYPE_KEY, defaultValue = Constants.TARIFF_TYPE_DEFAULT_VALUE) String tariffType,
	            @RequestHeader(value = Constants.APP_PHASE_KEY, defaultValue = Constants.APP_PHASE_DEFAULT_VALUE) String isFromB2B,
	            HttpServletRequest servletRequest) throws SocketException, SQLException {
	        logger.info("E&S -- Request Land Time " + new Date());
	        ObjectMapper mapper = new ObjectMapper();
	        CloseUsersGroupResponse usersGroupResponse = new CloseUsersGroupResponse();
	        CloseUserGroupResponseBase userGroupResponseBase = new CloseUserGroupResponseBase();
	        CloseUserGroupRequest userGroupRequest = new CloseUserGroupRequest();
	        TariffBusiness tariffControllerBusiness = new TariffBusiness();
	        String TRANSACTION_NAME = Transactions.CLOSE_USER_GROUP_DATA + " CONTROLLER";
	 
	        String requestTime = Utilities.getReportDateTime();
	        try {
	 
	            String msisdn = Utilities.getValueFromJSON(Utilities.splitToken(Utilities.decodeString(token)).toString(),
	                    Constants.MSISDN_KEY);
	 
	            // Populating report object before processing business logic.
	            userGroupResponseBase.setLogsReport(
	                    Utilities.preBusinessReportLoggingData("", msisdn, TRANSACTION_NAME, servletRequest.getRemoteAddr(),
	                            UserAgent, "", "", data, lang, userGroupResponseBase.getLogsReport()));
	 
	            data = Utilities.addAttributesToRequest(data, msisdn, servletRequest, lang, UserAgent);
	            // String iP = "\"iP\": \"127.0.0.1\"";
	            Utilities.printInfoLog(msisdn + "-Request Landed in-" + TRANSACTION_NAME, logger);
	            Utilities.printInfoLog(msisdn + "-Request Data-" + data, logger);
	 
	            userGroupRequest = mapper.readValue(data, CloseUserGroupRequest.class);
	            userGroupRequest.setIsB2B(isFromB2B);
	            userGroupResponseBase = tariffControllerBusiness.getcloseUserGroupData(isFromB2B, msisdn, deviceID,
	                    userGroupRequest, usersGroupResponse);
	 
	        } catch (Exception e) {
	            logger.error("Error", e);
	            userGroupResponseBase.setCallStatus(Constants.Call_Status_False);
	            userGroupResponseBase.setResultCode(Constants.EXCEPTION_CODE);
	            userGroupResponseBase.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("unexpected.error", lang));
	        }
	 
	        return userGroupResponseBase;
	 
	    }
	
}
