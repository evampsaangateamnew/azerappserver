/**
 * 
 */
package com.evampsaanga.azerfon.controllers;

import java.io.IOException;
import java.sql.SQLException;

import javax.jms.JMSException;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.evampsaanga.azerfon.business.CoreServicesBusiness;
import com.evampsaanga.azerfon.common.messagesmappings.GetMessagesMappings;
import com.evampsaanga.azerfon.common.utilities.Constants;
import com.evampsaanga.azerfon.common.utilities.Transactions;
import com.evampsaanga.azerfon.common.utilities.Utilities;
import com.evampsaanga.azerfon.common.utilities.Validator;
import com.evampsaanga.azerfon.models.coreservices.getcoreservices.GetCoreServicesRequest;
import com.evampsaanga.azerfon.models.coreservices.getcoreservices.GetCoreServicesResponse;
import com.evampsaanga.azerfon.models.coreservices.getpaygstatus.GetPayGStatusRequest;
import com.evampsaanga.azerfon.models.coreservices.getpaygstatus.GetPayGStatusResponse;
import com.evampsaanga.azerfon.models.coreservices.processcoreservices.ProcessCoreServicesRequest;
import com.evampsaanga.azerfon.models.coreservices.processcoreservices.ProcessCoreServicesResponse;

/**
 * @author Evamp & Saanga
 *
 */
@RestController
@RequestMapping("/coreservices")
public class CoreServicesController {

	Logger logger = Logger.getLogger(CoreServicesController.class);

	@RequestMapping(value = "/getcoreservices", method = RequestMethod.POST, headers = Constants.CONTENT_TYPE_DEFAULT_VALUE)
	public GetCoreServicesResponse getcoreservices(@RequestBody String data,
			@RequestHeader(value = Constants.TOKEN_KEY, defaultValue = Constants.TOKEN_DEFAULT_VALUE) String token,
			@RequestHeader(value = Constants.LANGUAGE_KEY, defaultValue = Constants.LANGUAGE_DEFAULT_VALUE) String lang,
			@RequestHeader(value = Constants.USER_AGENT_KEY, defaultValue = Constants.USER_AGENT_DEFAULT_VALUE) String userAgent,
			@RequestHeader(value = Constants.SUBSCRIBER_TYPE_KEY, defaultValue = Constants.SUBSCRIBER_TYPE_DEFAULT_VALUE) String userType,
			@RequestHeader(value = Constants.TARIFF_TYPE_KEY, defaultValue = Constants.TARIFF_TYPE_DEFAULT_VALUE) String tariffType,
			HttpServletRequest servletRequest) throws ClassNotFoundException, SQLException, JsonParseException,
			JsonMappingException, IOException, JMSException {
		ObjectMapper mapper = new ObjectMapper();

		CoreServicesBusiness coreServicesBusiness = new CoreServicesBusiness();
		GetCoreServicesRequest getCoreServicesRequest = new GetCoreServicesRequest();
		GetCoreServicesResponse getCoreServicesResponse = new GetCoreServicesResponse();

		String requestTime = Utilities.getReportDateTime();

		String TRANSACTION_NAME = Transactions.GET_CORE_SERVICES_TRANSACTION_NAME + " CONTROLLER";
		try {

			String msisdn = Utilities.getValueFromJSON(Utilities.splitToken(Utilities.decodeString(token)).toString(),
					Constants.MSISDN_KEY);

			// Populating report object before processing business logic.
			getCoreServicesResponse.setLogsReport(Utilities.preBusinessReportLoggingData(token, msisdn,
					TRANSACTION_NAME, servletRequest.getRemoteAddr(), userAgent, userType, tariffType, data, lang,
					getCoreServicesResponse.getLogsReport()));

			data = Utilities.addAttributesToRequest(data, msisdn, servletRequest, lang, userAgent);

			Utilities.printInfoLog(msisdn + "-Request Landed in-" + TRANSACTION_NAME, logger);
			Utilities.printInfoLog(msisdn + "-Request Data-" + data, logger);
			if (Utilities.hasJSONKey(data, "accountType")) {

				getCoreServicesRequest = mapper.readValue(data, GetCoreServicesRequest.class);
				getCoreServicesRequest.setBrand(tariffType);
				getCoreServicesRequest.setUserType(userType);

				String requestValidationStatus = Validator.validateRequest(msisdn, getCoreServicesRequest);

				if (requestValidationStatus.equalsIgnoreCase(Constants.VALIDATION_SUCCESS_CODE)) {

					getCoreServicesResponse = coreServicesBusiness.getCoreServicesBusiness(msisdn,
							getCoreServicesRequest, getCoreServicesResponse, lang);

				} else {
					getCoreServicesResponse.setCallStatus(Constants.Call_Status_False);
					getCoreServicesResponse.setResultCode(Constants.VALIDATION_FAILURE_CODE);
					getCoreServicesResponse.setResultDesc(requestValidationStatus);
				}
			} else {
				getCoreServicesResponse.setCallStatus(Constants.Call_Status_False);
				getCoreServicesResponse.setResultCode(Constants.API_FAILURE_CODE);
				getCoreServicesResponse.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle(
						"appNewVersion.feature.", Utilities.getValueFromJSON(data, "lang")));
			}
			getCoreServicesResponse.getLogsReport().setRequestTime(requestTime);
			getCoreServicesResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
			getCoreServicesResponse.getLogsReport().setResponse(mapper.writeValueAsString(getCoreServicesResponse));
			getCoreServicesResponse.getLogsReport().setResponseCode(getCoreServicesResponse.getResultCode());

			// Sending report log into queue.
			Utilities.prepareLogReportForQueue(getCoreServicesResponse.getLogsReport());

			Utilities.printInfoLog(msisdn + "-Response Returned from-" + TRANSACTION_NAME + "-"
					+ mapper.writeValueAsString(getCoreServicesResponse), logger);
		} catch (Exception e) {
			Utilities.printErrorLog(Utilities.convertStackTraceToString(e), logger);
			getCoreServicesResponse.setCallStatus(Constants.Call_Status_False);
			getCoreServicesResponse.setResultCode(Constants.EXCEPTION_CODE);
			getCoreServicesResponse
					.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("unexpected.error", lang));

			getCoreServicesResponse.getLogsReport().setResponseCode(getCoreServicesResponse.getResultCode());
			getCoreServicesResponse.getLogsReport().setRequestTime(requestTime);
			getCoreServicesResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
			getCoreServicesResponse.getLogsReport().setResponse(mapper.writeValueAsString(getCoreServicesResponse));

			// Sending report log into queue.
			Utilities.prepareLogReportForQueue(getCoreServicesResponse.getLogsReport());

		}
		return getCoreServicesResponse;
	}

	@RequestMapping(value = "/processcoreservices", method = RequestMethod.POST, headers = Constants.CONTENT_TYPE_DEFAULT_VALUE)
	public ProcessCoreServicesResponse processCoreServices(@RequestBody String data,
			@RequestHeader(value = Constants.TOKEN_KEY, defaultValue = Constants.TOKEN_DEFAULT_VALUE) String token,
			@RequestHeader(value = Constants.LANGUAGE_KEY, defaultValue = Constants.LANGUAGE_DEFAULT_VALUE) String lang,
			@RequestHeader(value = Constants.USER_AGENT_KEY, defaultValue = Constants.USER_AGENT_DEFAULT_VALUE) String userAgent,
			@RequestHeader(value = Constants.SUBSCRIBER_TYPE_KEY, defaultValue = Constants.SUBSCRIBER_TYPE_DEFAULT_VALUE) String userType,
			@RequestHeader(value = Constants.TARIFF_TYPE_KEY, defaultValue = Constants.TARIFF_TYPE_DEFAULT_VALUE) String tariffType,
			HttpServletRequest servletRequest) throws ClassNotFoundException, SQLException, JsonParseException,
			JsonMappingException, IOException, JMSException {
		ObjectMapper mapper = new ObjectMapper();
		CoreServicesBusiness coreServicesBusiness = new CoreServicesBusiness();
		ProcessCoreServicesRequest processCoreServicesRequest = new ProcessCoreServicesRequest();
		ProcessCoreServicesResponse processCoreServicesResponse = new ProcessCoreServicesResponse();

		String requestTime = Utilities.getReportDateTime();

		String TRANSACTION_NAME = Transactions.MANIPULATE_CORE_SERVICES_TRANSACTION_NAME + " CONTROLLER";
		try {
			String msisdn = Utilities.getValueFromJSON(Utilities.splitToken(Utilities.decodeString(token)).toString(),
					Constants.MSISDN_KEY);

			// Populating report object before processing business logic.
			processCoreServicesResponse.setLogsReport(Utilities.preBusinessReportLoggingData(token, msisdn,
					TRANSACTION_NAME, servletRequest.getRemoteAddr(), userAgent, userType, tariffType, data, lang,
					processCoreServicesResponse.getLogsReport()));

			data = Utilities.addAttributesToRequest(data, msisdn, servletRequest, lang, userAgent);

			Utilities.printInfoLog(msisdn + "-Request Landed in-" + TRANSACTION_NAME, logger);
			Utilities.printInfoLog(msisdn + "-Request Data-" + data, logger);
			Utilities.printInfoLog(msisdn + "-BRAND-" + tariffType, logger);
			Utilities.printInfoLog(msisdn + "-USER TYPE-" + userType, logger);
			if (Utilities.hasJSONKey(data, "accountType")) {
				processCoreServicesRequest = mapper.readValue(data, ProcessCoreServicesRequest.class);
				processCoreServicesRequest.setBrand(tariffType);
				processCoreServicesRequest.setUserType(userType);
				// Specific params for report log
				processCoreServicesResponse.getLogsReport()
						.setCallForwardNumber(processCoreServicesRequest.getNumber());
				String requestValidationStatus = Validator.validateRequest(msisdn, processCoreServicesRequest);

				if (requestValidationStatus.equalsIgnoreCase(Constants.VALIDATION_SUCCESS_CODE)) {

					processCoreServicesResponse = coreServicesBusiness.processCoreServicesBusiness(msisdn,
							processCoreServicesRequest, processCoreServicesResponse);

				} else {
					processCoreServicesResponse.setCallStatus(Constants.Call_Status_False);
					processCoreServicesResponse.setResultCode(Constants.VALIDATION_FAILURE_CODE);
					processCoreServicesResponse.setResultDesc(requestValidationStatus);
				}
			} else {
				processCoreServicesResponse.setCallStatus(Constants.Call_Status_False);
				processCoreServicesResponse.setResultCode(Constants.API_FAILURE_CODE);
				processCoreServicesResponse.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle(
						"appNewVersion.feature.", Utilities.getValueFromJSON(data, "lang")));
			}
			processCoreServicesResponse.getLogsReport().setRequestTime(requestTime);
			processCoreServicesResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
			processCoreServicesResponse.getLogsReport()
					.setResponse(mapper.writeValueAsString(processCoreServicesResponse));
			processCoreServicesResponse.getLogsReport().setResponseCode(processCoreServicesResponse.getResultCode());

			// Sending report log into queue.
			Utilities.prepareLogReportForQueue(processCoreServicesResponse.getLogsReport());

			Utilities.printInfoLog(msisdn + "-Response Returned from-" + TRANSACTION_NAME + "-"
					+ mapper.writeValueAsString(processCoreServicesResponse), logger);
		} catch (Exception e) {
			Utilities.printErrorLog(Utilities.convertStackTraceToString(e), logger);
			processCoreServicesResponse.setCallStatus(Constants.Call_Status_False);
			processCoreServicesResponse.setResultCode(Constants.EXCEPTION_CODE);
			processCoreServicesResponse
					.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("unexpected.error", lang));

			processCoreServicesResponse.getLogsReport().setResponseCode(processCoreServicesResponse.getResultCode());
			processCoreServicesResponse.getLogsReport().setRequestTime(requestTime);
			processCoreServicesResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
			processCoreServicesResponse.getLogsReport()
					.setResponse(mapper.writeValueAsString(processCoreServicesResponse));

			// Sending report log into queue.
			Utilities.prepareLogReportForQueue(processCoreServicesResponse.getLogsReport());

		}
		return processCoreServicesResponse;
	}
	
	@RequestMapping(value = "/getpaygstatus", method = RequestMethod.POST, headers = Constants.CONTENT_TYPE_DEFAULT_VALUE)
	public GetPayGStatusResponse getpaygstatus(
			@RequestHeader(value = Constants.TOKEN_KEY, defaultValue = Constants.TOKEN_DEFAULT_VALUE) String token,
			@RequestHeader(value = Constants.LANGUAGE_KEY, defaultValue = Constants.LANGUAGE_DEFAULT_VALUE) String lang,
			@RequestHeader(value = Constants.USER_AGENT_KEY, defaultValue = Constants.USER_AGENT_DEFAULT_VALUE) String userAgent,
			@RequestHeader(value = Constants.SUBSCRIBER_TYPE_KEY, defaultValue = Constants.SUBSCRIBER_TYPE_DEFAULT_VALUE) String userType,
			@RequestHeader(value = Constants.TARIFF_TYPE_KEY, defaultValue = Constants.TARIFF_TYPE_DEFAULT_VALUE) String tariffType,
			HttpServletRequest servletRequest) throws JsonParseException, JsonMappingException, IOException,
			JMSException, ClassNotFoundException, SQLException {

		ObjectMapper mapper = new ObjectMapper();
		String data = "{}";

		GetPayGStatusRequest getPayGStatusRequest = new GetPayGStatusRequest();
		GetPayGStatusResponse getPayGStatusResponse = new GetPayGStatusResponse();

		String TRANSACTION_NAME = Transactions.GET_PAYG_STATUS_TRANSACTION_NAME + " CONTROLLER";
		String requestTime = Utilities.getReportDateTime();

		CoreServicesBusiness coreServicesBusiness = new CoreServicesBusiness();

		try {
			String msisdn = Utilities.getValueFromJSON(Utilities.splitToken(Utilities.decodeString(token)).toString(),
					Constants.MSISDN_KEY);
//comment
			// Populating report object before processing business logic.
			getPayGStatusResponse.setLogsReport(Utilities.preBusinessReportLoggingData(token, msisdn, TRANSACTION_NAME,
					servletRequest.getRemoteAddr(), userAgent, userType, tariffType, data, lang,
					getPayGStatusResponse.getLogsReport()));

			String deviceID = servletRequest.getHeader("deviceID");

			data = Utilities.addAttributesToRequest(data, msisdn, servletRequest, lang, userAgent);

			Utilities.printInfoLog(msisdn + "-Request Landed in-" + TRANSACTION_NAME, logger);
			Utilities.printInfoLog(msisdn + "-Request Data-" + data, logger);

			getPayGStatusRequest = mapper.readValue(data, GetPayGStatusRequest.class);

			String requestValidationStatus = Validator.validateRequest(msisdn, getPayGStatusRequest);

			if (requestValidationStatus.equalsIgnoreCase(Constants.VALIDATION_SUCCESS_CODE)) {
				getPayGStatusResponse = coreServicesBusiness.getGetPayGStatusBusiness(msisdn, deviceID, getPayGStatusRequest,
						getPayGStatusResponse);
			} else {
				getPayGStatusResponse.setCallStatus(Constants.Call_Status_False);
				getPayGStatusResponse.setResultCode(Constants.VALIDATION_FAILURE_CODE);
				getPayGStatusResponse.setResultDesc(requestValidationStatus);
			}

			getPayGStatusResponse.getLogsReport().setResponseCode(getPayGStatusResponse.getResultCode());
			getPayGStatusResponse.getLogsReport().setRequestTime(requestTime);
			getPayGStatusResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
			getPayGStatusResponse.getLogsReport().setResponse(mapper.writeValueAsString(getPayGStatusResponse));

			// Sending report log into queue.
			Utilities.prepareLogReportForQueue(getPayGStatusResponse.getLogsReport());

			Utilities.printInfoLog(msisdn + "-Response Returned from-" + TRANSACTION_NAME + "-"
					+ mapper.writeValueAsString(getPayGStatusResponse), logger);
		} catch (Exception e) {
			Utilities.printErrorLog(Utilities.convertStackTraceToString(e), logger);
			getPayGStatusResponse.setCallStatus(Constants.Call_Status_False);

			getPayGStatusResponse.setResultCode(Constants.EXCEPTION_CODE);
			getPayGStatusResponse
					.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("unexpected.error", lang));

			getPayGStatusResponse.getLogsReport().setResponseCode(getPayGStatusResponse.getResultCode());
			getPayGStatusResponse.getLogsReport().setRequestTime(requestTime);
			getPayGStatusResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
			getPayGStatusResponse.getLogsReport().setResponse(mapper.writeValueAsString(getPayGStatusResponse));

			// Sending report log into queue.
			Utilities.prepareLogReportForQueue(getPayGStatusResponse.getLogsReport());
		}
		return getPayGStatusResponse;

	}

}
