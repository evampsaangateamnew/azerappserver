package com.evampsaanga.azerfon.controllers;

import java.io.IOException;
import java.sql.SQLException;

import javax.jms.JMSException;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.evampsaanga.azerfon.business.GetReportsBusiness;
import com.evampsaanga.azerfon.common.messagesmappings.GetMessagesMappings;
import com.evampsaanga.azerfon.common.utilities.Constants;
import com.evampsaanga.azerfon.common.utilities.Utilities;
import com.evampsaanga.azerfon.common.utilities.Validator;
import com.evampsaanga.azerfon.models.getreports.GetReportsRequest;
import com.evampsaanga.azerfon.models.getreports.GetReportsResponse;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@RestController
@RequestMapping("/notification")
public class GetReportsController {
	Logger logger = Logger.getLogger(GetReportsController.class);

	@RequestMapping(value = "/getreports", method = RequestMethod.POST, headers = Constants.CONTENT_TYPE_DEFAULT_VALUE)
	public GetReportsResponse refreshCache(@RequestBody String data,
			@RequestHeader(value = "credentials") String credentials, HttpServletRequest servletRequest)
			throws ClassNotFoundException, SQLException, JsonParseException, JsonMappingException, IOException,
			JMSException {

		ObjectMapper mapper = new ObjectMapper();
		GetReportsBusiness getReportsBusiness = new GetReportsBusiness();
		GetReportsRequest getReportsRequest = new GetReportsRequest();
		GetReportsResponse getReportsResponse = new GetReportsResponse();

		if (credentials.equalsIgnoreCase(Constants.CREDENTIALS_FOR_INTERNAL_CALLS)) {

			String msisdn = "Not Applicable";
			String lang = "3";

			String requestTime = Utilities.getReportDateTime();

			String TRANSACTION_NAME = "GET REPORTS";

			try {

				Utilities.printInfoLog(TRANSACTION_NAME + "-Request Landed in-" + TRANSACTION_NAME, logger);
				Utilities.printInfoLog(TRANSACTION_NAME + "-Request Data-" + data, logger);

				getReportsRequest = mapper.readValue(data, GetReportsRequest.class);

				String requestValidationStatus = Validator.validateRequest(msisdn, getReportsRequest);

				if (requestValidationStatus.equalsIgnoreCase(Constants.VALIDATION_SUCCESS_CODE)) {

					getReportsResponse = getReportsBusiness.getReports(getReportsRequest, getReportsResponse);

				} else {

					getReportsResponse.setResultCode(Constants.VALIDATION_FAILURE_CODE);
					getReportsResponse.setResultDesc(requestValidationStatus);
				}

				getReportsResponse.getLogsReport().setRequestTime(requestTime);
				getReportsResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
				getReportsResponse.getLogsReport().setResponse(mapper.writeValueAsString(getReportsResponse));
				getReportsResponse.getLogsReport().setResponseCode(getReportsResponse.getResultCode());

				// Sending report log into queue.
				Utilities.prepareLogReportForQueue(getReportsResponse.getLogsReport());

				Utilities.printInfoLog(msisdn + "-Response Returned from-" + TRANSACTION_NAME + "-"
						+ mapper.writeValueAsString(getReportsResponse), logger);

			} catch (Exception e) {

				Utilities.printErrorLog(Utilities.convertStackTraceToString(e), logger);

				getReportsResponse.setCallStatus(Constants.Call_Status_False);
				getReportsResponse.setResultCode(Constants.EXCEPTION_CODE);
				getReportsResponse
						.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("unexpected.error", lang));

				getReportsResponse.getLogsReport().setResponseCode(getReportsResponse.getResultCode());
				getReportsResponse.getLogsReport().setRequestTime(requestTime);
				getReportsResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
				getReportsResponse.getLogsReport().setResponse(mapper.writeValueAsString(getReportsResponse));

				// Sending report log into queue.
				Utilities.prepareLogReportForQueue(getReportsResponse.getLogsReport());

			}
		} else {

			getReportsResponse.setCallStatus(Constants.Call_Status_False);
			getReportsResponse.setResultCode(Constants.UNAUTHORIZED_ACCESS);
			getReportsResponse.setResultDesc("UNAUTHORIZED ACCESS.");

		}
		return getReportsResponse;
	}
}
