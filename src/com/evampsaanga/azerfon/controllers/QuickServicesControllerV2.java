/**
 * 
 */
package com.evampsaanga.azerfon.controllers;

import java.io.IOException;
import java.sql.SQLException;

import javax.jms.JMSException;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONObject;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.evampsaanga.azerfon.business.QuickServicesBusiness;
import com.evampsaanga.azerfon.common.messagesmappings.GetMessagesMappings;
import com.evampsaanga.azerfon.common.utilities.Constants;
import com.evampsaanga.azerfon.common.utilities.Transactions;
import com.evampsaanga.azerfon.common.utilities.Utilities;
import com.evampsaanga.azerfon.common.utilities.Validator;
import com.evampsaanga.azerfon.models.broadcastsms.BroadcastSMSRequest;
import com.evampsaanga.azerfon.models.quickservices.getfreesmsstatus.GetFreeSMSStatusRequest;
import com.evampsaanga.azerfon.models.quickservices.getfreesmsstatus.GetFreeSMSStatusResponse;
import com.evampsaanga.azerfon.models.quickservices.sendfreesms.SendFreeSMSResponseV2;

/**
 * @author Evamp & Saanga
 *
 */
@RestController
@RequestMapping("/quickservicesV2")
public class QuickServicesControllerV2 {

	Logger logger = Logger.getLogger(QuickServicesControllerV2.class);

	@RequestMapping(value = "/broadcastsms", method = RequestMethod.POST, headers = Constants.CONTENT_TYPE_DEFAULT_VALUE)
	public SendFreeSMSResponseV2 sendFreeSMS(@RequestBody String data,
			@RequestHeader(value = Constants.MSISDN_KEY, defaultValue = Constants.MSISDN_DEFAULT_VALUE) String msisdn,
			@RequestHeader(value = Constants.LANGUAGE_KEY, defaultValue = Constants.LANGUAGE_DEFAULT_VALUE) String lang,
			@RequestHeader(value = Constants.USER_AGENT_KEY, defaultValue = Constants.USER_AGENT_DEFAULT_VALUE) String userAgent,
			@RequestHeader(value = Constants.SUBSCRIBER_TYPE_KEY, defaultValue = Constants.SUBSCRIBER_TYPE_DEFAULT_VALUE) String userType,
			@RequestHeader(value = Constants.TARIFF_TYPE_KEY, defaultValue = Constants.TARIFF_TYPE_DEFAULT_VALUE) String tariffType,
			@RequestHeader(value = Constants.APP_PHASE_KEY, defaultValue = Constants.APP_PHASE_DEFAULT_VALUE) String isFromB2B,
			HttpServletRequest servletRequest) throws ClassNotFoundException, SQLException, JsonParseException,
			JsonMappingException, IOException, JMSException {
		ObjectMapper mapper = new ObjectMapper();
		QuickServicesBusiness quickServicesBusiness = new QuickServicesBusiness();
		BroadcastSMSRequest sendFreeSMSRequest = new BroadcastSMSRequest();
		SendFreeSMSResponseV2 freeSMSResponse = new SendFreeSMSResponseV2();
		String requestTime = Utilities.getReportDateTime();

		String TRANSACTION_NAME = Transactions.SEND_FREE_SMS + " CONTROLLER";
		try {
			/*
			 * String msisdn =
			 * Utilities.getValueFromJSON(Utilities.splitToken(Utilities.
			 * decodeString(token)).toString(), Constants.MSISDN_KEY);
			 */

			// Populating report object before processing business logic.
			freeSMSResponse.setLogsReport(
					Utilities.preBusinessReportLoggingData("", msisdn, TRANSACTION_NAME, servletRequest.getRemoteAddr(),
							userAgent, userType, tariffType, data, lang, freeSMSResponse.getLogsReport()));

			data = Utilities.addAttributesToRequest(data, msisdn, servletRequest, lang, userAgent);

			Utilities.printInfoLog(msisdn + "-Request Landed in-" + TRANSACTION_NAME, logger);
			Utilities.printInfoLog(msisdn + "-Request Data-" + data, logger);
			if (!Utilities.hasJSONKey(data, "accountType")) {
				sendFreeSMSRequest = mapper.readValue(data, BroadcastSMSRequest.class);

				// Logging specific params in report.
				freeSMSResponse.getLogsReport().setReceiverMsisdn(sendFreeSMSRequest.getRecieverMsisdn());

				String requestValidationStatus = Validator.validateRequest(msisdn, sendFreeSMSRequest);
				Utilities.printInfoLog("<<<<< Validation status is: >>>>>" + requestValidationStatus, logger);
				if (requestValidationStatus.equalsIgnoreCase(Constants.VALIDATION_SUCCESS_CODE)) {
					sendFreeSMSRequest.setIsB2B(isFromB2B);
					if (msisdn.equalsIgnoreCase(sendFreeSMSRequest.getRecieverMsisdn())) {
						freeSMSResponse.setCallStatus(Constants.Call_Status_False);
						freeSMSResponse.setResultCode(Constants.API_FAILURE_CODE);
						freeSMSResponse.setResultDesc(
								Utilities.getErrorMessageFromFile("free.sms", sendFreeSMSRequest.getLang(), "01"));
					} else {
						freeSMSResponse = quickServicesBusiness.sendFreeSMSBusinessV2(msisdn, sendFreeSMSRequest,
								freeSMSResponse);
					}
				} else {
					freeSMSResponse.setCallStatus(Constants.Call_Status_False);
					freeSMSResponse.setResultCode(Constants.VALIDATION_FAILURE_CODE);
					freeSMSResponse.setResultDesc(requestValidationStatus);
				}
			} else {
				freeSMSResponse.setCallStatus(Constants.Call_Status_False);
				freeSMSResponse.setResultCode(Constants.API_FAILURE_CODE);
				freeSMSResponse.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("appNewVersion.feature.",
						Utilities.getValueFromJSON(data, "lang")));
			}
			freeSMSResponse.getLogsReport().setResponseCode(freeSMSResponse.getResultCode());
			freeSMSResponse.getLogsReport().setRequestTime(requestTime);
			freeSMSResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
			freeSMSResponse.getLogsReport().setResponse(mapper.writeValueAsString(freeSMSResponse));

			// Sending report log into queue.
			freeSMSResponse.getLogsReport().setAppVersion(Constants.APP_VERSION_PHASE2);
			Utilities.prepareLogReportForQueue(freeSMSResponse.getLogsReport());

			Utilities.printInfoLog(msisdn + "-Response Returned from-" + TRANSACTION_NAME + "-"
					+ mapper.writeValueAsString(freeSMSResponse), logger);
		} catch (Exception e) {
			Utilities.printErrorLog(Utilities.convertStackTraceToString(e), logger);
			freeSMSResponse.setCallStatus(Constants.Call_Status_False);
			freeSMSResponse.setResultCode(Constants.EXCEPTION_CODE);
			freeSMSResponse.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("unexpected.error", lang));

			freeSMSResponse.getLogsReport().setResponseCode(freeSMSResponse.getResultCode());
			freeSMSResponse.getLogsReport().setRequestTime(requestTime);
			freeSMSResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
			freeSMSResponse.getLogsReport().setResponse(mapper.writeValueAsString(freeSMSResponse));

			// Sending report log into queue.
			freeSMSResponse.getLogsReport().setAppVersion(Constants.APP_VERSION_PHASE2);
			Utilities.prepareLogReportForQueue(freeSMSResponse.getLogsReport());

		}
		return freeSMSResponse;
	}

	@RequestMapping(value = "/getfreesmsstatus", method = RequestMethod.POST, headers = Constants.CONTENT_TYPE_DEFAULT_VALUE)
	public GetFreeSMSStatusResponse getFreeSMSStatus(@RequestBody String data,
			@RequestHeader(value = Constants.MSISDN_KEY, defaultValue = Constants.MSISDN_DEFAULT_VALUE) String msisdn,
			@RequestHeader(value = Constants.LANGUAGE_KEY, defaultValue = Constants.LANGUAGE_DEFAULT_VALUE) String lang,
			@RequestHeader(value = Constants.USER_AGENT_KEY, defaultValue = Constants.USER_AGENT_DEFAULT_VALUE) String userAgent,
			@RequestHeader(value = Constants.SUBSCRIBER_TYPE_KEY, defaultValue = Constants.SUBSCRIBER_TYPE_DEFAULT_VALUE) String userType,
			@RequestHeader(value = Constants.TARIFF_TYPE_KEY, defaultValue = Constants.TARIFF_TYPE_DEFAULT_VALUE) String tariffType,
			@RequestHeader(value = Constants.APP_PHASE_KEY, defaultValue = Constants.APP_PHASE_DEFAULT_VALUE) String isFromB2B,
			HttpServletRequest servletRequest) throws ClassNotFoundException, SQLException, JsonParseException,
			JsonMappingException, IOException, JMSException {
		ObjectMapper mapper = new ObjectMapper();
		JSONObject jsonObject = new JSONObject();
		QuickServicesBusiness quickServicesBusiness = new QuickServicesBusiness();
		GetFreeSMSStatusRequest freeSMSStatusRequest = new GetFreeSMSStatusRequest();
		GetFreeSMSStatusResponse freeSMSStatusResponse = new GetFreeSMSStatusResponse();
		String requestTime = Utilities.getReportDateTime();

		String TRANSACTION_NAME = Transactions.SEND_SMS_STATUS + " CONTROLLER";
		try {
			data = jsonObject.toString();
			/*
			 * String msisdn =
			 * Utilities.getValueFromJSON(Utilities.splitToken(Utilities.
			 * decodeString(token)).toString(), Constants.MSISDN_KEY);
			 */

			// Populating report object before processing business logic.
			freeSMSStatusResponse.setLogsReport(
					Utilities.preBusinessReportLoggingData("", msisdn, TRANSACTION_NAME, servletRequest.getRemoteAddr(),
							userAgent, userType, tariffType, data, lang, freeSMSStatusResponse.getLogsReport()));

			data = Utilities.addAttributesToRequest(data, msisdn, servletRequest, lang, userAgent);

			Utilities.printInfoLog(msisdn + "-Request Landed in-" + TRANSACTION_NAME, logger);
			Utilities.printInfoLog(msisdn + "-Request Data-" + data, logger);

			freeSMSStatusRequest = mapper.readValue(data, GetFreeSMSStatusRequest.class);

			String requestValidationStatus = Validator.validateRequest(msisdn, freeSMSStatusRequest);

			if (requestValidationStatus.equalsIgnoreCase(Constants.VALIDATION_SUCCESS_CODE)) {
				freeSMSStatusRequest.setIsB2B(isFromB2B);
				freeSMSStatusResponse = quickServicesBusiness.getFreeSMSStatusBusiness(msisdn, freeSMSStatusRequest,
						freeSMSStatusResponse);

			} else {
				freeSMSStatusResponse.setCallStatus(Constants.Call_Status_False);
				freeSMSStatusResponse.setResultCode(Constants.VALIDATION_FAILURE_CODE);
				freeSMSStatusResponse.setResultDesc(requestValidationStatus);
			}

			freeSMSStatusResponse.getLogsReport().setResponseCode(freeSMSStatusResponse.getResultCode());
			freeSMSStatusResponse.getLogsReport().setRequestTime(requestTime);
			freeSMSStatusResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
			freeSMSStatusResponse.getLogsReport().setResponse(mapper.writeValueAsString(freeSMSStatusResponse));

			// Sending report log into queue.
			freeSMSStatusResponse.getLogsReport().setAppVersion(Constants.APP_VERSION_PHASE2);
			Utilities.prepareLogReportForQueue(freeSMSStatusResponse.getLogsReport());

			Utilities.printInfoLog(msisdn + "-Response Returned from-" + TRANSACTION_NAME + "-"
					+ mapper.writeValueAsString(freeSMSStatusResponse), logger);
		} catch (Exception e) {
			Utilities.printErrorLog(Utilities.convertStackTraceToString(e), logger);
			freeSMSStatusResponse.setCallStatus(Constants.Call_Status_False);
			freeSMSStatusResponse.setResultCode(Constants.EXCEPTION_CODE);
			freeSMSStatusResponse
					.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("unexpected.error", lang));

			freeSMSStatusResponse.getLogsReport().setResponseCode(freeSMSStatusResponse.getResultCode());
			freeSMSStatusResponse.getLogsReport().setRequestTime(requestTime);
			freeSMSStatusResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
			freeSMSStatusResponse.getLogsReport().setResponse(mapper.writeValueAsString(freeSMSStatusResponse));

			// Sending report log into queue.
			freeSMSStatusResponse.getLogsReport().setAppVersion(Constants.APP_VERSION_PHASE2);
			Utilities.prepareLogReportForQueue(freeSMSStatusResponse.getLogsReport());

		}
		return freeSMSStatusResponse;
	}
}
