/**
 * 
 */
package com.evampsaanga.azerfon.controllers;

import java.io.IOException;
import java.sql.SQLException;

import javax.jms.JMSException;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.evampsaanga.azerfon.business.TriggersBusiness;
import com.evampsaanga.azerfon.common.messagesmappings.GetMessagesMappings;
import com.evampsaanga.azerfon.common.utilities.Constants;
import com.evampsaanga.azerfon.common.utilities.Utilities;
import com.evampsaanga.azerfon.common.utilities.Validator;
import com.evampsaanga.azerfon.models.triggers.TriggersRequest;
import com.evampsaanga.azerfon.models.triggers.TriggersResponse;

/**
 * @author Evamp & Saanga
 *
 */
@RestController
@RequestMapping("/trigger")
public class TriggersController {

	Logger logger = Logger.getLogger(TriggersController.class);

	@RequestMapping(value = "/refreshcache", method = RequestMethod.POST, headers = Constants.CONTENT_TYPE_DEFAULT_VALUE)
	public TriggersResponse refreshCache(@RequestBody String data,
			@RequestHeader(value = "credentials") String credentials, HttpServletRequest servletRequest)
			throws ClassNotFoundException, SQLException, JsonParseException, JsonMappingException, IOException,
			JMSException {

		ObjectMapper mapper = new ObjectMapper();
		TriggersBusiness triggersBusiness = new TriggersBusiness();
		TriggersRequest triggersRequest = new TriggersRequest();
		TriggersResponse triggersResponse = new TriggersResponse();

		if (credentials.equalsIgnoreCase(Constants.CREDENTIALS_FOR_INTERNAL_CALLS)) {

			String msisdn = "Not Applicable";
			String lang = "3";

			String requestTime = Utilities.getReportDateTime();

			String TRANSACTION_NAME = "TRIGGER";

			try {

				Utilities.printInfoLog(TRANSACTION_NAME + "-Request Landed in-" + TRANSACTION_NAME, logger);
				Utilities.printInfoLog(TRANSACTION_NAME + "-Request Data-" + data, logger);

				triggersRequest = mapper.readValue(data, TriggersRequest.class);

				String requestValidationStatus = Validator.validateRequest(msisdn, triggersRequest);

				if (requestValidationStatus.equalsIgnoreCase(Constants.VALIDATION_SUCCESS_CODE)) {

					triggersResponse = triggersBusiness.refreshCacheBusiness(triggersRequest, triggersResponse);

				} else {

					triggersResponse.setResultCode(Constants.VALIDATION_FAILURE_CODE);
					triggersResponse.setResultDesc(requestValidationStatus);
				}

				triggersResponse.getLogsReport().setRequestTime(requestTime);
				triggersResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
				triggersResponse.getLogsReport().setResponse(mapper.writeValueAsString(triggersResponse));
				triggersResponse.getLogsReport().setResponseCode(triggersResponse.getResultCode());

				// Sending report log into queue.
				Utilities.prepareLogReportForQueue(triggersResponse.getLogsReport());

				Utilities.printInfoLog(msisdn + "-Response Returned from-" + TRANSACTION_NAME + "-"
						+ mapper.writeValueAsString(triggersResponse), logger);

			} catch (Exception e) {

				Utilities.printErrorLog(Utilities.convertStackTraceToString(e), logger);

				triggersResponse.setCallStatus(Constants.Call_Status_False);
				triggersResponse.setResultCode(Constants.EXCEPTION_CODE);
				triggersResponse
						.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("unexpected.error", lang));

				triggersResponse.getLogsReport().setResponseCode(triggersResponse.getResultCode());
				triggersResponse.getLogsReport().setRequestTime(requestTime);
				triggersResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
				triggersResponse.getLogsReport().setResponse(mapper.writeValueAsString(triggersResponse));

				// Sending report log into queue.
				Utilities.prepareLogReportForQueue(triggersResponse.getLogsReport());

			}
		} else {

			triggersResponse.setCallStatus(Constants.Call_Status_False);
			triggersResponse.setResultCode(Constants.UNAUTHORIZED_ACCESS);
			triggersResponse.setResultDesc("UNAUTHORIZED ACCESS.");

		}
		return triggersResponse;
	}

}
