/**
 * 
 */
package com.evampsaanga.azerfon.controllers;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Collections;
import java.util.Comparator;

import javax.jms.JMSException;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.evampsaanga.azerfon.business.TariffBusiness;
import com.evampsaanga.azerfon.common.messagesmappings.GetMessagesMappings;
import com.evampsaanga.azerfon.common.utilities.Constants;
import com.evampsaanga.azerfon.common.utilities.Transactions;
import com.evampsaanga.azerfon.common.utilities.Utilities;
import com.evampsaanga.azerfon.common.utilities.Validator;
import com.evampsaanga.azerfon.models.tariffdetails.TariffRequest;
import com.evampsaanga.azerfon.models.tariffdetails.TariffResponse;
import com.evampsaanga.azerfon.models.tariffdetails.TariffResponseData;
import com.evampsaanga.azerfon.models.tariffdetails.changetariff.ChangeTariffRequest;
import com.evampsaanga.azerfon.models.tariffdetails.changetariff.ChangeTariffResponse;
import com.evampsaanga.azerfon.models.tariffdetails.postpaid.corporate.Corporate;
import com.evampsaanga.azerfon.models.tariffdetails.postpaid.individual.Individual;
import com.evampsaanga.azerfon.models.tariffdetails.prepaid.cin.Cin;
import com.evampsaanga.azerfon.models.tariffdetails.prepaid.cin.description.Classification;
import com.evampsaanga.azerfon.models.tariffdetails.prepaid.klass.Klass;

/**
 * @author Evamp & Saanga
 *
 */

@RestController
@RequestMapping("/tariffservices")
public class TariffsController {

	Logger logger = Logger.getLogger(TariffsController.class);

	@RequestMapping(value = "/gettariffdetails", method = RequestMethod.POST, headers = Constants.CONTENT_TYPE_DEFAULT_VALUE)
	public com.evampsaanga.azerfon.models.tariffdetails.TariffResponse getTariffDetails(@RequestBody String data,
			@RequestHeader(value = Constants.TOKEN_KEY, defaultValue = Constants.TOKEN_DEFAULT_VALUE) String token,
			@RequestHeader(value = Constants.LANGUAGE_KEY, defaultValue = Constants.LANGUAGE_DEFAULT_VALUE) String lang,
			@RequestHeader(value = Constants.USER_AGENT_KEY, defaultValue = Constants.USER_AGENT_DEFAULT_VALUE) String userAgent,
			@RequestHeader(value = Constants.SUBSCRIBER_TYPE_KEY, defaultValue = Constants.SUBSCRIBER_TYPE_DEFAULT_VALUE) String userType,
			@RequestHeader(value = Constants.TARIFF_TYPE_KEY, defaultValue = Constants.TARIFF_TYPE_DEFAULT_VALUE) String tariffType,
			HttpServletRequest servletRequest) throws ClassNotFoundException, SQLException, JsonParseException,
			JsonMappingException, IOException, JMSException {

		ObjectMapper mapper = new ObjectMapper();
		TariffRequest tariffRequest = new TariffRequest();
		com.evampsaanga.azerfon.models.tariffdetails.TariffResponse tariffResponse = new com.evampsaanga.azerfon.models.tariffdetails.TariffResponse();
		TariffBusiness tariffBusiness = new TariffBusiness();

		String TRANSACTION_NAME = Transactions.TARRIF_DETAILS_TRANSACTION_NAME + " CONTROLLER";
		String requestTime = Utilities.getReportDateTime();
		try {

			String msisdn = Utilities.getValueFromJSON(Utilities.splitToken(Utilities.decodeString(token)).toString(),
					Constants.MSISDN_KEY);

			// Populating report object before processing business logic.
			tariffResponse.setLogsReport(Utilities.preBusinessReportLoggingData(token, msisdn, TRANSACTION_NAME,
					servletRequest.getRemoteAddr(), userAgent, userType, tariffType, data, lang,
					tariffResponse.getLogsReport()));
			data = Utilities.addAttributesToRequest(data, msisdn, servletRequest, lang, userAgent);
			Utilities.printInfoLog(msisdn + "-Request Landed in-" + TRANSACTION_NAME, logger);
			Utilities.printInfoLog(msisdn + "-Request Data-" + data, logger);

			tariffRequest = mapper.readValue(data, TariffRequest.class);
			// Store ID value is same as of Language ID.
			tariffRequest.setStoreId(tariffRequest.getLang());

			String requestValidationStatus = Validator.validateRequest(msisdn, tariffRequest);
			if (requestValidationStatus.equalsIgnoreCase(Constants.VALIDATION_SUCCESS_CODE)) {

				tariffResponse = tariffBusiness.getTariffDetailsBusiness(msisdn, tariffRequest, tariffResponse);

			} else {
				tariffResponse.setCallStatus(Constants.Call_Status_False);
				tariffResponse.setResultCode(Constants.VALIDATION_FAILURE_CODE);
				tariffResponse.setResultDesc(requestValidationStatus);
			}

			tariffResponse.getLogsReport().setResponseCode(tariffResponse.getResultCode());
			tariffResponse.getLogsReport().setRequestTime(requestTime);
			tariffResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
			tariffResponse.getLogsReport().setResponse(mapper.writeValueAsString(tariffResponse));

			// Sending report log into queue.
			Utilities.prepareLogReportForQueue(tariffResponse.getLogsReport());

			Utilities.printInfoLog(msisdn + "-Response Returned from-" + TRANSACTION_NAME + "-"
					+ mapper.writeValueAsString(tariffResponse), logger);
		} catch (Exception e) {
			Utilities.printErrorLog(Utilities.convertStackTraceToString(e), logger);
			tariffResponse.setCallStatus(Constants.Call_Status_False);
			tariffResponse.setResultCode(Constants.EXCEPTION_CODE);
			tariffResponse.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("unexpected.error", lang));

			tariffResponse.getLogsReport().setResponseCode(tariffResponse.getResultCode());
			tariffResponse.getLogsReport().setRequestTime(requestTime);
			tariffResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
			tariffResponse.getLogsReport().setResponse(mapper.writeValueAsString(tariffResponse));

			// Sending report log into queue.
			Utilities.prepareLogReportForQueue(tariffResponse.getLogsReport());

		}
		return tariffResponse;
	}

	/*private void sortTariffResponse(TariffResponse tariffResponse) {
		TariffResponseData tariffResponseData = tariffResponse.getData();
		if (tariffResponseData != null) {
			// Sort prepaid tariff
			if (tariffResponseData.getPrepaid() != null) {
				// Sort prepaid cin tariff
				if (tariffResponseData.getPrepaid().getCin() != null
						&& tariffResponseData.getPrepaid().getCin().size() > 0) {
					Collections.sort(tariffResponseData.getPrepaid().getCin(), new Comparator<Cin>() {
						@Override
						public int compare(Cin o1, Cin o2) {

							if (o1.getHeader() != null && o2.getHeader() != null
									&& o1.getHeader().getSortOrder() != null && o2.getHeader().getSortOrder() != null) {
								return o1.getHeader().getSortOrder().compareTo(o2.getHeader().getSortOrder());
							} else {
								return Constants.MAX_INT;
							}
						}
					});
				}
				// Sort prepaid klass tariff
				if (tariffResponseData.getPrepaid().getKlass() != null
						&& tariffResponseData.getPrepaid().getKlass().size() > 0) {
					Collections.sort(tariffResponseData.getPrepaid().getKlass(), new Comparator<Klass>() {
						@Override
						public int compare(Klass o1, Klass o2) {
							if (o1.getHeader() != null && o2.getHeader() != null
									&& o1.getHeader().getSortOrder() != null && o2.getHeader().getSortOrder() != null) {
								return o1.getHeader().getSortOrder().compareTo(o2.getHeader().getSortOrder());
							} else {
								return Constants.MAX_INT;
							}
						}
					});
				}
			}

			if (tariffResponseData.getPostpaid() != null) {
				// Sort postpaid Corporate tariff
				if (tariffResponseData.getPostpaid().getCorporate() != null
						&& tariffResponseData.getPostpaid().getCorporate().size() > 0) {
					Collections.sort(tariffResponseData.getPostpaid().getCorporate(), new Comparator<Corporate>() {
						@Override
						public int compare(Corporate o1, Corporate o2) {
							if (o1.getHeader() != null && o2.getHeader() != null
									&& o1.getHeader().getSortOrder() != null && o2.getHeader().getSortOrder() != null) {
								return o1.getHeader().getSortOrder().compareTo(o2.getHeader().getSortOrder());
							} else {
								return Constants.MAX_INT;
							}
						}
					});
				}

				// Sort postpaid Individual tariff
				if (tariffResponseData.getPostpaid().getIndividual() != null
						&& tariffResponseData.getPostpaid().getIndividual().size() > 0) {
					Collections.sort(tariffResponseData.getPostpaid().getIndividual(), new Comparator<Individual>() {
						@Override
						public int compare(Individual o1, Individual o2) {
							if (o1.getHeader() != null && o2.getHeader() != null
									&& o1.getHeader().getSortOrder() != null && o2.getHeader().getSortOrder() != null) {
								return o1.getHeader().getSortOrder().compareTo(o2.getHeader().getSortOrder());
							} else {
								return Constants.MAX_INT;
							}
						}
					});
				}

				// Sort postpaid Klass tariff
				if (tariffResponseData.getPostpaid().getKlassPostpaid() != null
						&& tariffResponseData.getPostpaid().getKlassPostpaid().size() > 0) {
					Collections.sort(tariffResponseData.getPostpaid().getKlassPostpaid(), new Comparator<Klass>() {
						@Override
						public int compare(Klass o1, Klass o2) {
							if (o1.getHeader() != null && o2.getHeader() != null
									&& o1.getHeader().getSortOrder() != null && o2.getHeader().getSortOrder() != null) {
								return o1.getHeader().getSortOrder().compareTo(o2.getHeader().getSortOrder());
							} else {
								return Constants.MAX_INT;
							}
						}
					});
				}
			}
		}
	}*/

	@RequestMapping(value = "/changetariff", method = RequestMethod.POST, headers = Constants.CONTENT_TYPE_DEFAULT_VALUE)
	public ChangeTariffResponse changeTariff(@RequestBody String data,
			@RequestHeader(value = Constants.TOKEN_KEY, defaultValue = Constants.TOKEN_DEFAULT_VALUE) String token,
			@RequestHeader(value = Constants.LANGUAGE_KEY, defaultValue = Constants.LANGUAGE_DEFAULT_VALUE) String lang,
			@RequestHeader(value = Constants.USER_AGENT_KEY, defaultValue = Constants.USER_AGENT_DEFAULT_VALUE) String userAgent,
			@RequestHeader(value = Constants.SUBSCRIBER_TYPE_KEY, defaultValue = Constants.SUBSCRIBER_TYPE_DEFAULT_VALUE) String userType,
			@RequestHeader(value = Constants.TARIFF_TYPE_KEY, defaultValue = Constants.TARIFF_TYPE_DEFAULT_VALUE) String tariffType,
			HttpServletRequest servletRequest) throws ClassNotFoundException, SQLException, JsonParseException,
			JsonMappingException, IOException, JMSException {
		ObjectMapper mapper = new ObjectMapper();
		TariffBusiness tariffBusiness = new TariffBusiness();
		ChangeTariffRequest changeTariffRequest = new ChangeTariffRequest();
		ChangeTariffResponse changeTariffResponse = new ChangeTariffResponse();

		String TRANSACTION_NAME = Transactions.CHANGE_TARRIF_TRANSACTION_NAME + " CONTROLLER";
		String requestTime = Utilities.getReportDateTime();
		try {
			String msisdn = Utilities.getValueFromJSON(Utilities.splitToken(Utilities.decodeString(token)).toString(),
					Constants.MSISDN_KEY);

			// Populating report object before processing business logic.
			changeTariffResponse.setLogsReport(Utilities.preBusinessReportLoggingData(token, msisdn, TRANSACTION_NAME,
					servletRequest.getRemoteAddr(), userAgent, userType, tariffType, data, lang,
					changeTariffResponse.getLogsReport()));

			data = Utilities.addAttributesToRequest(data, msisdn, servletRequest, lang, userAgent);

			Utilities.printInfoLog(msisdn + "-Request Landed in-" + TRANSACTION_NAME, logger);
			Utilities.printInfoLog(msisdn + "-Request Data-" + data, logger);

			changeTariffRequest = mapper.readValue(data, ChangeTariffRequest.class);

			// Logging specific params
			changeTariffResponse.getLogsReport().setPrimaryOfferingName(changeTariffRequest.getTariffName());
			changeTariffResponse.getLogsReport().setTariffOfferingId(changeTariffRequest.getOfferingId());

			String requestValidationStatus = Validator.validateRequest(msisdn, changeTariffRequest);

			if (requestValidationStatus.equalsIgnoreCase(Constants.VALIDATION_SUCCESS_CODE)) {

				changeTariffResponse = tariffBusiness.changeTariffBusiness(msisdn, changeTariffRequest,
						changeTariffResponse);
			} else {
				changeTariffResponse.setCallStatus(Constants.Call_Status_False);
				changeTariffResponse.setResultCode(Constants.VALIDATION_FAILURE_CODE);
				changeTariffResponse.setResultDesc(requestValidationStatus);
			}

			changeTariffResponse.getLogsReport().setResponseCode(changeTariffResponse.getResultCode());
			changeTariffResponse.getLogsReport().setRequestTime(requestTime);
			changeTariffResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
			changeTariffResponse.getLogsReport().setResponse(mapper.writeValueAsString(changeTariffResponse));

			// Sending report log into queue.
			Utilities.prepareLogReportForQueue(changeTariffResponse.getLogsReport());

			Utilities.printInfoLog(msisdn + "-Response Returned from-" + TRANSACTION_NAME + "-"
					+ mapper.writeValueAsString(changeTariffResponse), logger);
		} catch (Exception e) {
			Utilities.printErrorLog(Utilities.convertStackTraceToString(e), logger);
			changeTariffResponse.setCallStatus(Constants.Call_Status_False);
			changeTariffResponse.setResultCode(Constants.EXCEPTION_CODE);
			changeTariffResponse
					.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("unexpected.error", lang));

			changeTariffResponse.getLogsReport().setResponseCode(changeTariffResponse.getResultCode());
			changeTariffResponse.getLogsReport().setRequestTime(requestTime);
			changeTariffResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
			changeTariffResponse.getLogsReport().setResponse(mapper.writeValueAsString(changeTariffResponse));

			// Sending report log into queue.
			Utilities.prepareLogReportForQueue(changeTariffResponse.getLogsReport());

		}
		return changeTariffResponse;
	}
}
