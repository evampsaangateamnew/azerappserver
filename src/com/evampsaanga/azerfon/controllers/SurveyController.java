package com.evampsaanga.azerfon.controllers;

import java.io.IOException;
import java.sql.SQLException;
import javax.jms.JMSException;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONObject;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.evampsaanga.azerfon.common.messagesmappings.GetMessagesMappings;
import com.evampsaanga.azerfon.common.utilities.Constants;
import com.evampsaanga.azerfon.common.utilities.Transactions;
import com.evampsaanga.azerfon.common.utilities.Utilities;
import com.evampsaanga.azerfon.common.utilities.Validator;
import com.evampsaanga.azerfon.models.survey.GetSurveyRequest;
import com.evampsaanga.azerfon.models.survey.GetSurveyResponse;
import com.evampsaanga.azerfon.models.survey.SaveSurveyRequest;
import com.evampsaanga.azerfon.models.survey.SaveSurveyResponse;
import com.evampsaanga.azerfon.business.SurveyBusiness;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@RestController
@RequestMapping("/survey")
public class SurveyController {

	Logger logger = Logger.getLogger(SurveyController.class);
	SurveyBusiness surveyBusiness = new SurveyBusiness();

	@RequestMapping(value = "/getsurveys", method = RequestMethod.POST, headers = Constants.CONTENT_TYPE_DEFAULT_VALUE)
	public GetSurveyResponse getSurvey(
			@RequestHeader(value = Constants.TOKEN_KEY, defaultValue = Constants.TOKEN_DEFAULT_VALUE) String token,
			@RequestHeader(value = Constants.LANGUAGE_KEY, defaultValue = Constants.LANGUAGE_DEFAULT_VALUE) String lang,
			@RequestHeader(value = Constants.USER_AGENT_KEY, defaultValue = Constants.USER_AGENT_DEFAULT_VALUE) String userAgent,
			@RequestHeader(value = Constants.SUBSCRIBER_TYPE_KEY, defaultValue = Constants.SUBSCRIBER_TYPE_DEFAULT_VALUE) String userType,
			@RequestHeader(value = Constants.TARIFF_TYPE_KEY, defaultValue = Constants.TARIFF_TYPE_DEFAULT_VALUE) String tariffType,
			HttpServletRequest servletRequest) throws ClassNotFoundException, SQLException, JsonParseException,
			JsonMappingException, IOException, JMSException {
		String data = "{}";
		ObjectMapper mapper = new ObjectMapper();
		JSONObject jsonObject = new JSONObject();

		GetSurveyRequest getSurveyRequest = new GetSurveyRequest();
		GetSurveyResponse getSurveyResponse = new GetSurveyResponse();
		String requestTime = Utilities.getReportDateTime();

		String TRANSACTION_NAME = Transactions.GET_SURVEY_TRANSACTION_NAME + " CONTROLLER";
		try {
			data = jsonObject.toString();
			String msisdn = Utilities.getValueFromJSON(Utilities.splitToken(Utilities.decodeString(token)).toString(),
					Constants.MSISDN_KEY);

			// Populating report object before processing business logic.
			getSurveyResponse.setLogsReport(Utilities.preBusinessReportLoggingData(token, msisdn, TRANSACTION_NAME,
					servletRequest.getRemoteAddr(), userAgent, userType, tariffType, data, lang,
					getSurveyResponse.getLogsReport()));

			data = Utilities.addAttributesToRequest(data, msisdn, servletRequest, lang, userAgent/* , "false" */);

			Utilities.printInfoLog(msisdn + "-Request Landed in " + TRANSACTION_NAME, logger);
			Utilities.printInfoLog(msisdn + "-Request Data-" + data, logger);

			getSurveyRequest = mapper.readValue(data, GetSurveyRequest.class);
			String requestValidationStatus = Validator.validateRequest(msisdn, getSurveyRequest);

			if (requestValidationStatus.equalsIgnoreCase(Constants.VALIDATION_SUCCESS_CODE)) {

				getSurveyResponse = surveyBusiness.getSurveyBusiness(msisdn, getSurveyRequest, getSurveyResponse);

			} else {
				getSurveyResponse.setCallStatus(Constants.Call_Status_False);
				getSurveyResponse.setResultCode(Constants.VALIDATION_FAILURE_CODE);
				getSurveyResponse.setResultDesc(requestValidationStatus);
			}

			getSurveyResponse.getLogsReport().setRequestTime(requestTime);
			getSurveyResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
			getSurveyResponse.getLogsReport().setResponse(mapper.writeValueAsString(getSurveyResponse));
			getSurveyResponse.getLogsReport().setResponseCode(getSurveyResponse.getResultCode());

			// Sending report log into queue.
			Utilities.prepareLogReportForQueue(getSurveyResponse.getLogsReport());

			Utilities.printInfoLog(msisdn + "-Response Returned from-" + TRANSACTION_NAME + "-"
					+ mapper.writeValueAsString(getSurveyResponse), logger);
		} catch (Exception e) {
			Utilities.printErrorLog(Utilities.convertStackTraceToString(e), logger);
			getSurveyResponse.setCallStatus(Constants.Call_Status_False);
			getSurveyResponse.setResultCode(Constants.EXCEPTION_CODE);
			getSurveyResponse.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("unexpected.error", lang));

			getSurveyResponse.getLogsReport().setResponseCode(getSurveyResponse.getResultCode());
			getSurveyResponse.getLogsReport().setRequestTime(requestTime);
			getSurveyResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
			getSurveyResponse.getLogsReport().setResponse(mapper.writeValueAsString(getSurveyResponse));

			// Sending report log into queue.
			Utilities.prepareLogReportForQueue(getSurveyResponse.getLogsReport());

		}
		return getSurveyResponse;
	}

	@RequestMapping(value = "/savesurvey", method = RequestMethod.POST, headers = Constants.CONTENT_TYPE_DEFAULT_VALUE)
	public SaveSurveyResponse saveSurvey(@RequestBody String data,
			@RequestHeader(value = Constants.TOKEN_KEY, defaultValue = Constants.TOKEN_DEFAULT_VALUE) String token,
			@RequestHeader(value = Constants.LANGUAGE_KEY, defaultValue = Constants.LANGUAGE_DEFAULT_VALUE) String lang,
			@RequestHeader(value = Constants.USER_AGENT_KEY, defaultValue = Constants.USER_AGENT_DEFAULT_VALUE) String userAgent,
			@RequestHeader(value = Constants.SUBSCRIBER_TYPE_KEY, defaultValue = Constants.SUBSCRIBER_TYPE_DEFAULT_VALUE) String userType,
			@RequestHeader(value = Constants.TARIFF_TYPE_KEY, defaultValue = Constants.TARIFF_TYPE_DEFAULT_VALUE) String tariffType,
			HttpServletRequest servletRequest) throws ClassNotFoundException, SQLException, JsonParseException,
			JsonMappingException, IOException, JMSException {

		ObjectMapper mapper = new ObjectMapper();

		SaveSurveyRequest saveSurveyRequest = new SaveSurveyRequest();
		SaveSurveyResponse saveSurveyResponse = new SaveSurveyResponse();
		String requestTime = Utilities.getReportDateTime();

		String TRANSACTION_NAME = Transactions.SAVE_SURVEY_TRANSACTION_NAME + " CONTROLLER";
		try {
			String msisdn = Utilities.getValueFromJSON(Utilities.splitToken(Utilities.decodeString(token)).toString(),
					Constants.MSISDN_KEY);

			// Populating report object before processing business logic.
			saveSurveyResponse.setLogsReport(Utilities.preBusinessReportLoggingData(token, msisdn, TRANSACTION_NAME,
					servletRequest.getRemoteAddr(), userAgent, userType, tariffType, data, lang,
					saveSurveyResponse.getLogsReport()));

			data = Utilities.addAttributesToRequest(data, msisdn, servletRequest, lang, userAgent/* , "false" */);

			Utilities.printInfoLog(msisdn + "-Request Landed in " + TRANSACTION_NAME, logger);
			Utilities.printInfoLog(msisdn + "-Request Data-" + data, logger);

			saveSurveyRequest = mapper.readValue(data, SaveSurveyRequest.class);
			String requestValidationStatus = Validator.validateRequest(msisdn, saveSurveyRequest);

			if (requestValidationStatus.equalsIgnoreCase(Constants.VALIDATION_SUCCESS_CODE)) {

				saveSurveyResponse = surveyBusiness.saveSurveyBusiness(msisdn, saveSurveyRequest, saveSurveyResponse);

			} else {
				saveSurveyResponse.setCallStatus(Constants.Call_Status_False);
				saveSurveyResponse.setResultCode(Constants.VALIDATION_FAILURE_CODE);
				saveSurveyResponse.setResultDesc(requestValidationStatus);
			}

			saveSurveyResponse.getLogsReport().setRequestTime(requestTime);
			saveSurveyResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
			saveSurveyResponse.getLogsReport().setResponse(mapper.writeValueAsString(saveSurveyResponse));
			saveSurveyResponse.getLogsReport().setResponseCode(saveSurveyResponse.getResultCode());

			// Sending report log into queue.
			Utilities.prepareLogReportForQueue(saveSurveyResponse.getLogsReport());

			Utilities.printInfoLog(msisdn + "-Response Returned from-" + TRANSACTION_NAME + "-"
					+ mapper.writeValueAsString(saveSurveyResponse), logger);
		} catch (Exception e) {
			Utilities.printErrorLog(Utilities.convertStackTraceToString(e), logger);
			saveSurveyResponse.setCallStatus(Constants.Call_Status_False);
			saveSurveyResponse.setResultCode(Constants.EXCEPTION_CODE);
			saveSurveyResponse
					.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("unexpected.error", lang));

			saveSurveyResponse.getLogsReport().setResponseCode(saveSurveyResponse.getResultCode());
			saveSurveyResponse.getLogsReport().setRequestTime(requestTime);
			saveSurveyResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
			saveSurveyResponse.getLogsReport().setResponse(mapper.writeValueAsString(saveSurveyResponse));

			// Sending report log into queue.
			Utilities.prepareLogReportForQueue(saveSurveyResponse.getLogsReport());

		}
		return saveSurveyResponse;
	}
}
