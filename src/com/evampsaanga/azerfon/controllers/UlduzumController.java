package com.evampsaanga.azerfon.controllers;

import java.io.IOException;
import java.sql.SQLException;

import javax.jms.JMSException;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.evampsaanga.azerfon.business.UlduzumBusiness;
import com.evampsaanga.azerfon.common.messagesmappings.GetMessagesMappings;
import com.evampsaanga.azerfon.common.utilities.Constants;
import com.evampsaanga.azerfon.common.utilities.Transactions;
import com.evampsaanga.azerfon.common.utilities.Utilities;
import com.evampsaanga.azerfon.common.utilities.Validator;
import com.evampsaanga.azerfon.models.ulduzum.GetCodesResponse;
import com.evampsaanga.azerfon.models.ulduzum.GetMerchantsRequest;
import com.evampsaanga.azerfon.models.ulduzum.GetMerchantsResponse;
import com.evampsaanga.azerfon.models.ulduzum.GetUnusedCodesResponse;
import com.evampsaanga.azerfon.models.ulduzum.GetUsageHistoryRequest;
import com.evampsaanga.azerfon.models.ulduzum.GetUsageHistoryResponse;
import com.evampsaanga.azerfon.models.ulduzum.GetUsageTotalsResponse;
import com.evampsaanga.azerfon.models.ulduzum.UlduzumGetCategoriesResponse;
import com.evampsaanga.azerfon.models.ulduzum.UlduzumRequest;

@RestController
@RequestMapping("/ulduzum")
public class UlduzumController {
	Logger logger = Logger.getLogger(UlduzumController.class);

	@RequestMapping(value = "/getcategories", method = RequestMethod.POST, headers = Constants.CONTENT_TYPE_DEFAULT_VALUE)
	public UlduzumGetCategoriesResponse getCategories(
			@RequestBody String data,
			@RequestHeader(value = Constants.MSISDN_KEY, defaultValue = Constants.MSISDN_DEFAULT_VALUE) String msisdn,
			@RequestHeader(value = Constants.LANGUAGE_KEY, defaultValue = Constants.LANGUAGE_DEFAULT_VALUE) String lang,
			@RequestHeader(value = Constants.USER_AGENT_KEY, defaultValue = Constants.USER_AGENT_DEFAULT_VALUE) String userAgent,
			@RequestHeader(value = Constants.SUBSCRIBER_TYPE_KEY, defaultValue = Constants.SUBSCRIBER_TYPE_DEFAULT_VALUE) String userType,
			@RequestHeader(value = Constants.TARIFF_TYPE_KEY, defaultValue = Constants.TARIFF_TYPE_DEFAULT_VALUE) String tariffType,

			HttpServletRequest servletRequest) throws ClassNotFoundException,
			SQLException, JsonParseException, JsonMappingException,
			IOException, JMSException {
		String TRANSACTION_NAME = Transactions.ULDUZUM_GET_CATEGORIES_TRANSACTION_NAME
				+ " CONTROLLER";

		ObjectMapper mapper = new ObjectMapper();
		UlduzumBusiness ulduzumBusiness = new UlduzumBusiness();
		UlduzumRequest request = new UlduzumRequest();
		UlduzumGetCategoriesResponse response = new UlduzumGetCategoriesResponse();
		String requestTime = Utilities.getReportDateTime();
		Utilities.printInfoLog(msisdn + "-Request Landed in-"
				+ TRANSACTION_NAME, logger);
		Utilities.printInfoLog(msisdn + "-Request Data-" + "", logger);

		try {
			response.setLogsReport(Utilities.preBusinessReportLoggingData("",
					msisdn, TRANSACTION_NAME, servletRequest.getRemoteAddr(),
					userAgent, userType, tariffType, "", lang,
					response.getLogsReport()));
			data = Utilities.addAttributesToRequest(data, msisdn,
					servletRequest, lang, userAgent);
			/*
			 * request.setChannel(userAgent);
			 * 
			 * 
			 * request.setMsisdn(msisdn);
			 * request.setiP(servletRequest.getRemoteAddr());
			 * request.setLang(lang);
			 */
			request = mapper.readValue(data, UlduzumRequest.class);

			String requestValidationStatus = Validator.validateRequest(msisdn,
					request);
			logger.info("<<<<  Validation Status >>>>:"
					+ requestValidationStatus + " -- " + msisdn);

			if (msisdn.equals("Unknow Msisdn") || msisdn == null) {
				requestValidationStatus = "Unknow Msisdn";
			}
			if (requestValidationStatus
					.equalsIgnoreCase(Constants.VALIDATION_SUCCESS_CODE)) {

				response = ulduzumBusiness.getCategories(msisdn, request,
						response);

			} else {
				response.setCallStatus(Constants.Call_Status_False);
				response.setResultCode(Constants.VALIDATION_FAILURE_CODE);
				response.setResultDesc(requestValidationStatus);
			}

			response.getLogsReport().setRequestTime(requestTime);
			response.getLogsReport().setResponseTime(
					Utilities.getReportDateTime());
			response.getLogsReport().setResponse(
					mapper.writeValueAsString(response));
			response.getLogsReport().setResponseCode(response.getResultCode());

			// Sending report log into queue.
			/*
			 * response.getLogsReport().setAppVersion(Constants.APP_VERSION_PHASE2
			 * );
			 */
			Utilities.prepareLogReportForQueue(response.getLogsReport());

			Utilities
					.printInfoLog(
							msisdn + "-Response Returned from-"
									+ TRANSACTION_NAME + "-"
									+ mapper.writeValueAsString(response),
							logger);
		} catch (Exception e) {
			Utilities.printErrorLog(Utilities.convertStackTraceToString(e),
					logger);
			response.setCallStatus(Constants.Call_Status_False);
			response.setResultCode(Constants.EXCEPTION_CODE);
			response.setResultDesc(GetMessagesMappings
					.getMessageFromResourceBundle("unexpected.error", lang));

			response.getLogsReport().setResponseCode(response.getResultCode());
			response.getLogsReport().setRequestTime(requestTime);
			response.getLogsReport().setResponseTime(
					Utilities.getReportDateTime());
			response.getLogsReport().setResponse(
					mapper.writeValueAsString(response));

			// Sending report log into queue.
			response.getLogsReport()
					.setAppVersion(Constants.APP_VERSION_PHASE2);
			Utilities.prepareLogReportForQueue(response.getLogsReport());
		}
		return response;
	}

	@RequestMapping(value = "/getmerchants", method = RequestMethod.POST, headers = Constants.CONTENT_TYPE_DEFAULT_VALUE)
	public GetMerchantsResponse getMerchants(
			@RequestBody String data,
			@RequestHeader(value = Constants.MSISDN_KEY, defaultValue = Constants.MSISDN_DEFAULT_VALUE) String msisdn,
			@RequestHeader(value = Constants.LANGUAGE_KEY, defaultValue = Constants.LANGUAGE_DEFAULT_VALUE) String lang,
			@RequestHeader(value = Constants.USER_AGENT_KEY, defaultValue = Constants.USER_AGENT_DEFAULT_VALUE) String userAgent,
			@RequestHeader(value = Constants.SUBSCRIBER_TYPE_KEY, defaultValue = Constants.SUBSCRIBER_TYPE_DEFAULT_VALUE) String userType,
			@RequestHeader(value = Constants.TARIFF_TYPE_KEY, defaultValue = Constants.TARIFF_TYPE_DEFAULT_VALUE) String tariffType,

			HttpServletRequest servletRequest) throws ClassNotFoundException,
			SQLException, JsonParseException, JsonMappingException,
			IOException, JMSException {
		String TRANSACTION_NAME = Transactions.ULDUZUM_GET_MERCHANTS_TRANSACTION_NAME
				+ " CONTROLLER";

		ObjectMapper mapper = new ObjectMapper();
		UlduzumBusiness ulduzumBusiness = new UlduzumBusiness();
		GetMerchantsRequest request = new GetMerchantsRequest();
		GetMerchantsResponse response = new GetMerchantsResponse();
		String requestTime = Utilities.getReportDateTime();

		Utilities.printInfoLog(msisdn + "-Request Landed in-"
				+ TRANSACTION_NAME, logger);
		Utilities.printInfoLog(msisdn + "-Request Data-" + "", logger);

		try {
			response.setLogsReport(Utilities.preBusinessReportLoggingData("",
					msisdn, TRANSACTION_NAME, servletRequest.getRemoteAddr(),
					userAgent, userType, tariffType, "", lang,
					response.getLogsReport()));
			data = Utilities.addAttributesToRequest(data, msisdn,
					servletRequest, lang, userAgent);
			/*
			 * request.setChannel(userAgent);
			 * 
			 * 
			 * request.setMsisdn(msisdn);
			 * request.setiP(servletRequest.getRemoteAddr());
			 * request.setLang(lang);
			 */
			request = mapper.readValue(data, GetMerchantsRequest.class);

			String requestValidationStatus = Validator.validateRequest(msisdn,
					request);
			logger.info("<<<<  Validation Status >>>>:"
					+ requestValidationStatus + " -- " + msisdn);

			if (msisdn.equals("Unknow Msisdn") || msisdn == null) {
				requestValidationStatus = "Unknow Msisdn";
			}
			if (requestValidationStatus
					.equalsIgnoreCase(Constants.VALIDATION_SUCCESS_CODE)) {

				response = ulduzumBusiness.getMerchants(msisdn, request,
						response);

			} else {
				response.setCallStatus(Constants.Call_Status_False);
				response.setResultCode(Constants.VALIDATION_FAILURE_CODE);
				response.setResultDesc(requestValidationStatus);
			}
			

			response.getLogsReport().setRequestTime(requestTime);
			response.getLogsReport().setResponseTime(
					Utilities.getReportDateTime());
			response.getLogsReport().setResponse(
					mapper.writeValueAsString(response));
			response.getLogsReport().setResponseCode(response.getResultCode());

			// Sending report log into queue.
			/*
			 * response.getLogsReport().setAppVersion(Constants.APP_VERSION_PHASE2
			 * );
			 */
			Utilities.prepareLogReportForQueue(response.getLogsReport());

			Utilities
					.printInfoLog(
							msisdn + "-Response Returned from-"
									+ TRANSACTION_NAME + "-"
									+ mapper.writeValueAsString(response),
							logger);
		} catch (Exception e) {
			Utilities.printErrorLog(Utilities.convertStackTraceToString(e),
					logger);
			response.setCallStatus(Constants.Call_Status_False);
			response.setResultCode(Constants.EXCEPTION_CODE);
			response.setResultDesc(GetMessagesMappings
					.getMessageFromResourceBundle("unexpected.error", lang));

			response.getLogsReport().setResponseCode(response.getResultCode());
			response.getLogsReport().setRequestTime(requestTime);
			response.getLogsReport().setResponseTime(
					Utilities.getReportDateTime());
			response.getLogsReport().setResponse(
					mapper.writeValueAsString(response));

			// Sending report log into queue.
			response.getLogsReport()
					.setAppVersion(Constants.APP_VERSION_PHASE2);
			Utilities.prepareLogReportForQueue(response.getLogsReport());
		}
		return response;
	}
	@RequestMapping(value = "/getusagetotals", method = RequestMethod.POST, headers = Constants.CONTENT_TYPE_DEFAULT_VALUE)
	public GetUsageTotalsResponse getUsageTotals(
			@RequestBody String data,
			@RequestHeader(value = Constants.MSISDN_KEY, defaultValue = Constants.MSISDN_DEFAULT_VALUE) String msisdn,
			@RequestHeader(value = Constants.LANGUAGE_KEY, defaultValue = Constants.LANGUAGE_DEFAULT_VALUE) String lang,
			@RequestHeader(value = Constants.USER_AGENT_KEY, defaultValue = Constants.USER_AGENT_DEFAULT_VALUE) String userAgent,
			@RequestHeader(value = Constants.SUBSCRIBER_TYPE_KEY, defaultValue = Constants.SUBSCRIBER_TYPE_DEFAULT_VALUE) String userType,
			@RequestHeader(value = Constants.TARIFF_TYPE_KEY, defaultValue = Constants.TARIFF_TYPE_DEFAULT_VALUE) String tariffType,

			HttpServletRequest servletRequest) throws ClassNotFoundException,
			SQLException, JsonParseException, JsonMappingException,
			IOException, JMSException {
		String TRANSACTION_NAME = Transactions.ULDUZUM_GET_USAGE_TOTALS_TRANSACTION_NAME
				+ " CONTROLLER";

		ObjectMapper mapper = new ObjectMapper();
		UlduzumBusiness ulduzumBusiness = new UlduzumBusiness();
		UlduzumRequest request = new UlduzumRequest();
		GetUsageTotalsResponse response = new GetUsageTotalsResponse();
		String requestTime = Utilities.getReportDateTime();

		Utilities.printInfoLog(msisdn + "-Request Landed in-"
				+ TRANSACTION_NAME, logger);
		Utilities.printInfoLog(msisdn + "-Request Data-" + "", logger);

		try {
			response.setLogsReport(Utilities.preBusinessReportLoggingData("",
					msisdn, TRANSACTION_NAME, servletRequest.getRemoteAddr(),
					userAgent, userType, tariffType, "", lang,
					response.getLogsReport()));
			data = Utilities.addAttributesToRequest(data, msisdn,
					servletRequest, lang, userAgent);
			/*
			 * request.setChannel(userAgent);
			 * 
			 * 
			 * request.setMsisdn(msisdn);
			 * request.setiP(servletRequest.getRemoteAddr());
			 * request.setLang(lang);
			 */
			request = mapper.readValue(data, UlduzumRequest.class);

			String requestValidationStatus = Validator.validateRequest(msisdn,
					request);
			logger.info("<<<<  Validation Status >>>>:"
					+ requestValidationStatus + " -- " + msisdn);

			if (msisdn.equals("Unknow Msisdn") || msisdn == null) {
				requestValidationStatus = "Unknow Msisdn";
			}
			if (requestValidationStatus
					.equalsIgnoreCase(Constants.VALIDATION_SUCCESS_CODE)) {

				response = ulduzumBusiness.getUsageTotals(msisdn, request,response);

			} else {
				response.setCallStatus(Constants.Call_Status_False);
				response.setResultCode(Constants.VALIDATION_FAILURE_CODE);
				response.setResultDesc(requestValidationStatus);
			}
			

			response.getLogsReport().setRequestTime(requestTime);
			response.getLogsReport().setResponseTime(
					Utilities.getReportDateTime());
			response.getLogsReport().setResponse(
					mapper.writeValueAsString(response));
			response.getLogsReport().setResponseCode(response.getResultCode());

			// Sending report log into queue.
			/*
			 * response.getLogsReport().setAppVersion(Constants.APP_VERSION_PHASE2
			 * );
			 */
			Utilities.prepareLogReportForQueue(response.getLogsReport());

			Utilities
					.printInfoLog(
							msisdn + "-Response Returned from-"
									+ TRANSACTION_NAME + "-"
									+ mapper.writeValueAsString(response),
							logger);
		} catch (Exception e) {
			Utilities.printErrorLog(Utilities.convertStackTraceToString(e),
					logger);
			response.setCallStatus(Constants.Call_Status_False);
			response.setResultCode(Constants.EXCEPTION_CODE);
			response.setResultDesc(GetMessagesMappings
					.getMessageFromResourceBundle("unexpected.error", lang));

			response.getLogsReport().setResponseCode(response.getResultCode());
			response.getLogsReport().setRequestTime(requestTime);
			response.getLogsReport().setResponseTime(
					Utilities.getReportDateTime());
			response.getLogsReport().setResponse(
					mapper.writeValueAsString(response));

			// Sending report log into queue.
			response.getLogsReport()
					.setAppVersion(Constants.APP_VERSION_PHASE2);
			Utilities.prepareLogReportForQueue(response.getLogsReport());
		}
		return response;
	}
	
	//start of ulduzum getUnsusedCodes API in Controller
	@RequestMapping(value = "/getunusedcodes", method = RequestMethod.POST, headers = Constants.CONTENT_TYPE_DEFAULT_VALUE)
	public GetUnusedCodesResponse getUnusedCodes(
			@RequestBody String data,
			@RequestHeader(value = Constants.MSISDN_KEY, defaultValue = Constants.MSISDN_DEFAULT_VALUE) String msisdn,
			@RequestHeader(value = Constants.LANGUAGE_KEY, defaultValue = Constants.LANGUAGE_DEFAULT_VALUE) String lang,
			@RequestHeader(value = Constants.USER_AGENT_KEY, defaultValue = Constants.USER_AGENT_DEFAULT_VALUE) String userAgent,
			@RequestHeader(value = Constants.SUBSCRIBER_TYPE_KEY, defaultValue = Constants.SUBSCRIBER_TYPE_DEFAULT_VALUE) String userType,
			@RequestHeader(value = Constants.TARIFF_TYPE_KEY, defaultValue = Constants.TARIFF_TYPE_DEFAULT_VALUE) String tariffType,

			HttpServletRequest servletRequest) throws ClassNotFoundException,
			SQLException, JsonParseException, JsonMappingException,
			IOException, JMSException {
		String TRANSACTION_NAME = Transactions.ULDUZUM_GET_UNUSED_CODES_TRANSACTION_NAME
				+ " CONTROLLER";

		ObjectMapper mapper = new ObjectMapper();
		UlduzumBusiness ulduzumBusiness = new UlduzumBusiness();
		UlduzumRequest request = new UlduzumRequest();
		GetUnusedCodesResponse response = new GetUnusedCodesResponse();
		String requestTime = Utilities.getReportDateTime();

		Utilities.printInfoLog(msisdn + "-Request Landed in-"
				+ TRANSACTION_NAME, logger);
		Utilities.printInfoLog(msisdn + "-Request Data-" + "", logger);

		try {
			response.setLogsReport(Utilities.preBusinessReportLoggingData("",
					msisdn, TRANSACTION_NAME, servletRequest.getRemoteAddr(),
					userAgent, userType, tariffType, "", lang,
					response.getLogsReport()));
			data = Utilities.addAttributesToRequest(data, msisdn,
					servletRequest, lang, userAgent);
			/*
			 * request.setChannel(userAgent);
			 * 
			 * 
			 * request.setMsisdn(msisdn);
			 * request.setiP(servletRequest.getRemoteAddr());
			 * request.setLang(lang);
			 */
			request = mapper.readValue(data, UlduzumRequest.class);

			String requestValidationStatus = Validator.validateRequest(msisdn,
					request);
			logger.info("<<<<  Validation Status >>>>:"
					+ requestValidationStatus + " -- " + msisdn);

			if (msisdn.equals("Unknow Msisdn") || msisdn == null) {
				requestValidationStatus = "Unknow Msisdn";
			}
			if (requestValidationStatus
					.equalsIgnoreCase(Constants.VALIDATION_SUCCESS_CODE)) {

				response = ulduzumBusiness.getUnsedCodes(msisdn, request,response);

			} else {
				response.setCallStatus(Constants.Call_Status_False);
				response.setResultCode(Constants.VALIDATION_FAILURE_CODE);
				response.setResultDesc(requestValidationStatus);
			}
			

			response.getLogsReport().setRequestTime(requestTime);
			response.getLogsReport().setResponseTime(
					Utilities.getReportDateTime());
			response.getLogsReport().setResponse(
					mapper.writeValueAsString(response));
			response.getLogsReport().setResponseCode(response.getResultCode());

			// Sending report log into queue.
			/*
			 * response.getLogsReport().setAppVersion(Constants.APP_VERSION_PHASE2
			 * );
			 */
			Utilities.prepareLogReportForQueue(response.getLogsReport());

			Utilities
					.printInfoLog(
							msisdn + "-Response Returned from-"
									+ TRANSACTION_NAME + "-"
									+ mapper.writeValueAsString(response),
							logger);
		} catch (Exception e) {
			Utilities.printErrorLog(Utilities.convertStackTraceToString(e),
					logger);
			response.setCallStatus(Constants.Call_Status_False);
			response.setResultCode(Constants.EXCEPTION_CODE);
			response.setResultDesc(GetMessagesMappings
					.getMessageFromResourceBundle("unexpected.error", lang));

			response.getLogsReport().setResponseCode(response.getResultCode());
			response.getLogsReport().setRequestTime(requestTime);
			response.getLogsReport().setResponseTime(
					Utilities.getReportDateTime());
			response.getLogsReport().setResponse(
					mapper.writeValueAsString(response));

			// Sending report log into queue.
			response.getLogsReport()
					.setAppVersion(Constants.APP_VERSION_PHASE2);
			Utilities.prepareLogReportForQueue(response.getLogsReport());
		}
		return response;
	}
	//END of ulduzum getUnsusedCodes API in Controller\\
	
	//start of ulduzum getUsageHistory API in Controller
	@RequestMapping(value = "/getusagehistory", method = RequestMethod.POST, headers = Constants.CONTENT_TYPE_DEFAULT_VALUE)
	public GetUsageHistoryResponse getUsagehistory(
			@RequestBody String data,
			@RequestHeader(value = Constants.MSISDN_KEY, defaultValue = Constants.MSISDN_DEFAULT_VALUE) String msisdn,
			@RequestHeader(value = Constants.LANGUAGE_KEY, defaultValue = Constants.LANGUAGE_DEFAULT_VALUE) String lang,
			@RequestHeader(value = Constants.USER_AGENT_KEY, defaultValue = Constants.USER_AGENT_DEFAULT_VALUE) String userAgent,
			@RequestHeader(value = Constants.SUBSCRIBER_TYPE_KEY, defaultValue = Constants.SUBSCRIBER_TYPE_DEFAULT_VALUE) String userType,
			@RequestHeader(value = Constants.TARIFF_TYPE_KEY, defaultValue = Constants.TARIFF_TYPE_DEFAULT_VALUE) String tariffType,

			HttpServletRequest servletRequest) throws ClassNotFoundException,
			SQLException, JsonParseException, JsonMappingException,
			IOException, JMSException {
		String TRANSACTION_NAME = Transactions.ULDUZUM_GET_USAGE_HISTORY_TRANSACTION_NAME
				+ " CONTROLLER";

		ObjectMapper mapper = new ObjectMapper();
		UlduzumBusiness ulduzumBusiness = new UlduzumBusiness();
		GetUsageHistoryRequest request = new GetUsageHistoryRequest();
		GetUsageHistoryResponse response = new GetUsageHistoryResponse();
		String requestTime = Utilities.getReportDateTime();

		Utilities.printInfoLog(msisdn + "-Request Landed in-"
				+ TRANSACTION_NAME, logger);
		Utilities.printInfoLog(msisdn + "-Request Data-" + "", logger);

		try {
			response.setLogsReport(Utilities.preBusinessReportLoggingData("",
					msisdn, TRANSACTION_NAME, servletRequest.getRemoteAddr(),
					userAgent, userType, tariffType, "", lang,
					response.getLogsReport()));
			data = Utilities.addAttributesToRequest(data, msisdn,
					servletRequest, lang, userAgent);
			/*
			 * request.setChannel(userAgent);
			 * 
			 * 
			 * request.setMsisdn(msisdn);
			 * request.setiP(servletRequest.getRemoteAddr());
			 * request.setLang(lang);
			 */
			request = mapper.readValue(data, GetUsageHistoryRequest.class);

			String requestValidationStatus = Validator.validateRequest(msisdn,
					request);
			logger.info("<<<<  Validation Status >>>>:"
					+ requestValidationStatus + " -- " + msisdn);

			if (msisdn.equals("Unknow Msisdn") || msisdn == null) {
				requestValidationStatus = "Unknow Msisdn";
			}
			if (requestValidationStatus
					.equalsIgnoreCase(Constants.VALIDATION_SUCCESS_CODE)) {

				response = ulduzumBusiness.getUsageHistory(msisdn, request,response);

			} else {
				response.setCallStatus(Constants.Call_Status_False);
				response.setResultCode(Constants.VALIDATION_FAILURE_CODE);
				response.setResultDesc(requestValidationStatus);
			}
			

			response.getLogsReport().setRequestTime(requestTime);
			response.getLogsReport().setResponseTime(
					Utilities.getReportDateTime());
			response.getLogsReport().setResponse(
					mapper.writeValueAsString(response));
			response.getLogsReport().setResponseCode(response.getResultCode());

			// Sending report log into queue.
			/*
			 * response.getLogsReport().setAppVersion(Constants.APP_VERSION_PHASE2
			 * );
			 */
			Utilities.prepareLogReportForQueue(response.getLogsReport());

			Utilities
					.printInfoLog(
							msisdn + "-Response Returned from-"
									+ TRANSACTION_NAME + "-"
									+ mapper.writeValueAsString(response),
							logger);
		} catch (Exception e) {
			Utilities.printErrorLog(Utilities.convertStackTraceToString(e),
					logger);
			response.setCallStatus(Constants.Call_Status_False);
			response.setResultCode(Constants.EXCEPTION_CODE);
			response.setResultDesc(GetMessagesMappings
					.getMessageFromResourceBundle("unexpected.error", lang));

			response.getLogsReport().setResponseCode(response.getResultCode());
			response.getLogsReport().setRequestTime(requestTime);
			response.getLogsReport().setResponseTime(
					Utilities.getReportDateTime());
			response.getLogsReport().setResponse(
					mapper.writeValueAsString(response));

			// Sending report log into queue.
			response.getLogsReport()
					.setAppVersion(Constants.APP_VERSION_PHASE2);
			Utilities.prepareLogReportForQueue(response.getLogsReport());
		}
		return response;
	}
	//END of ulduzum getUsagehistory API in Controller
	
	
	//start of ulduzum getCodes API in Controller
	@RequestMapping(value = "/getcodes", method = RequestMethod.POST, headers = Constants.CONTENT_TYPE_DEFAULT_VALUE)
	public GetCodesResponse getCodes(
			@RequestBody String data,
			@RequestHeader(value = Constants.MSISDN_KEY, defaultValue = Constants.MSISDN_DEFAULT_VALUE) String msisdn,
			@RequestHeader(value = Constants.LANGUAGE_KEY, defaultValue = Constants.LANGUAGE_DEFAULT_VALUE) String lang,
			@RequestHeader(value = Constants.USER_AGENT_KEY, defaultValue = Constants.USER_AGENT_DEFAULT_VALUE) String userAgent,
			@RequestHeader(value = Constants.SUBSCRIBER_TYPE_KEY, defaultValue = Constants.SUBSCRIBER_TYPE_DEFAULT_VALUE) String userType,
			@RequestHeader(value = Constants.TARIFF_TYPE_KEY, defaultValue = Constants.TARIFF_TYPE_DEFAULT_VALUE) String tariffType,

			HttpServletRequest servletRequest) throws ClassNotFoundException,
			SQLException, JsonParseException, JsonMappingException,
			IOException, JMSException {
		String TRANSACTION_NAME = Transactions.ULDUZUM_GET_CODES
				+ " CONTROLLER";

		ObjectMapper mapper = new ObjectMapper();
		UlduzumBusiness ulduzumBusiness = new UlduzumBusiness();
		UlduzumRequest request = new UlduzumRequest();
		GetCodesResponse response = new GetCodesResponse();
		String requestTime = Utilities.getReportDateTime();

		Utilities.printInfoLog(msisdn + "-Request Landed in-"
				+ TRANSACTION_NAME, logger);
		Utilities.printInfoLog(msisdn + "-Request Data-" + "", logger);

		try {
			response.setLogsReport(Utilities.preBusinessReportLoggingData("",
					msisdn, TRANSACTION_NAME, servletRequest.getRemoteAddr(),
					userAgent, userType, tariffType, "", lang,
					response.getLogsReport()));
			data = Utilities.addAttributesToRequest(data, msisdn,
					servletRequest, lang, userAgent);
			/*
			 * request.setChannel(userAgent);
			 * 
			 * 
			 * request.setMsisdn(msisdn);
			 * request.setiP(servletRequest.getRemoteAddr());
			 * request.setLang(lang);
			 */
			request = mapper.readValue(data, UlduzumRequest.class);

			String requestValidationStatus = Validator.validateRequest(msisdn,
					request);
			logger.info("<<<<  Validation Status >>>>:"
					+ requestValidationStatus + " -- " + msisdn);

			if (msisdn.equals("Unknow Msisdn") || msisdn == null) {
				requestValidationStatus = "Unknow Msisdn";
			}
			if (requestValidationStatus
					.equalsIgnoreCase(Constants.VALIDATION_SUCCESS_CODE)) {

				response = ulduzumBusiness.getCodes(msisdn, request,response);

			} else {
				response.setCallStatus(Constants.Call_Status_False);
				response.setResultCode(Constants.VALIDATION_FAILURE_CODE);
				response.setResultDesc(requestValidationStatus);
			}
			

			response.getLogsReport().setRequestTime(requestTime);
			response.getLogsReport().setResponseTime(
					Utilities.getReportDateTime());
			response.getLogsReport().setResponse(
					mapper.writeValueAsString(response));
			response.getLogsReport().setResponseCode(response.getResultCode());

			// Sending report log into queue.
			/*
			 * response.getLogsReport().setAppVersion(Constants.APP_VERSION_PHASE2
			 * );
			 */
			Utilities.prepareLogReportForQueue(response.getLogsReport());

			Utilities
					.printInfoLog(
							msisdn + "-Response Returned from-"
									+ TRANSACTION_NAME + "-"
									+ mapper.writeValueAsString(response),
							logger);
		} catch (Exception e) {
			Utilities.printErrorLog(Utilities.convertStackTraceToString(e),
					logger);
			response.setCallStatus(Constants.Call_Status_False);
			response.setResultCode(Constants.EXCEPTION_CODE);
			response.setResultDesc(GetMessagesMappings
					.getMessageFromResourceBundle("unexpected.error", lang));

			response.getLogsReport().setResponseCode(response.getResultCode());
			response.getLogsReport().setRequestTime(requestTime);
			response.getLogsReport().setResponseTime(
					Utilities.getReportDateTime());
			response.getLogsReport().setResponse(
					mapper.writeValueAsString(response));

			// Sending report log into queue.
			response.getLogsReport()
					.setAppVersion(Constants.APP_VERSION_PHASE2);
			Utilities.prepareLogReportForQueue(response.getLogsReport());
		}
		return response;
	}
	//END of ulduzum getCodes API in Controller
	
	
	
}
