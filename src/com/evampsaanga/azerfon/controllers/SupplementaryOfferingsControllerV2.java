/**
 * 
 */
package com.evampsaanga.azerfon.controllers;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.jms.JMSException;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONObject;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.evampsaanga.azerfon.business.SupplementaryOfferingsBusiness;
import com.evampsaanga.azerfon.common.messagesmappings.GetMessagesMappings;
import com.evampsaanga.azerfon.common.utilities.Constants;
import com.evampsaanga.azerfon.common.utilities.Transactions;
import com.evampsaanga.azerfon.common.utilities.Utilities;
import com.evampsaanga.azerfon.common.utilities.Validator;
import com.evampsaanga.azerfon.models.supplementaryofferings.changesupplementaryoffering.ChangeSupplementaryOfferingsRequest;
import com.evampsaanga.azerfon.models.supplementaryofferings.changesupplementaryoffering.ChangeSupplementaryOfferingsRequestBulk;
import com.evampsaanga.azerfon.models.supplementaryofferings.changesupplementaryoffering.ChangeSupplementaryOfferingsResponse;
import com.evampsaanga.azerfon.models.supplementaryofferings.changesupplementaryoffering.ChangeSupplementaryOfferingsResponsebulk;
import com.evampsaanga.azerfon.models.supplementaryofferings.getsupplementaryofferings.SupplementaryOfferingsByIDsRequest;
import com.evampsaanga.azerfon.models.supplementaryofferings.getsupplementaryofferings.SupplementaryOfferingsRequest;
import com.evampsaanga.azerfon.models.supplementaryofferings.getsupplementaryofferings.SupplementaryOfferingsResponse;
import com.evampsaanga.azerfon.models.supplementaryofferings.getsupplementaryofferings.commonofferings.SupplementryOfferingsData;

/**
 * @author Evamp & Saanga
 *
 */
@RestController
@RequestMapping("/supplementaryofferingsV2")
public class SupplementaryOfferingsControllerV2 {

	Logger logger = Logger.getLogger(SupplementaryOfferingsControllerV2.class);

	@RequestMapping(value = "/getsupplementaryofferings", method = RequestMethod.POST, headers = Constants.CONTENT_TYPE_DEFAULT_VALUE)
	public SupplementaryOfferingsResponse getSupplementryOfferings(@RequestBody String data,
			@RequestHeader(value = Constants.MSISDN_KEY, defaultValue = Constants.MSISDN_DEFAULT_VALUE) String msisdn,
			@RequestHeader(value = Constants.LANGUAGE_KEY, defaultValue = Constants.LANGUAGE_DEFAULT_VALUE) String lang,
			@RequestHeader(value = Constants.USER_AGENT_KEY, defaultValue = Constants.USER_AGENT_DEFAULT_VALUE) String userAgent,
			@RequestHeader(value = Constants.SUBSCRIBER_TYPE_KEY, defaultValue = Constants.SUBSCRIBER_TYPE_DEFAULT_VALUE) String userType,
			@RequestHeader(value = Constants.TARIFF_TYPE_KEY, defaultValue = Constants.TARIFF_TYPE_DEFAULT_VALUE) String tariffType,
			@RequestHeader(value = Constants.APP_PHASE_KEY, defaultValue = Constants.APP_PHASE_DEFAULT_VALUE) String isFromB2B,
			HttpServletRequest servletRequest) throws ClassNotFoundException, SQLException, IOException, JMSException {
		ObjectMapper mapper = new ObjectMapper();

		SupplementaryOfferingsBusiness supplementaryOfferingsBusiness = new SupplementaryOfferingsBusiness();
		SupplementaryOfferingsRequest supplementaryOfferingsRequest = new SupplementaryOfferingsRequest();
		SupplementaryOfferingsResponse supplementaryOfferingsResponse = new SupplementaryOfferingsResponse();

		String TRANSACTION_NAME = Transactions.SUPPLEMENTARY_SERVICES_TRANSACTION_NAME + " CONTROLLER";
		String requestTime = Utilities.getReportDateTime();
		try {
			/*
			 * String msisdn =
			 * Utilities.getValueFromJSON(Utilities.splitToken(Utilities.
			 * decodeString(token)).toString(), Constants.MSISDN_KEY);
			 */

			// Populating report object before processing business logic.
			supplementaryOfferingsResponse.setLogsReport(Utilities.preBusinessReportLoggingData("", msisdn,
					TRANSACTION_NAME, servletRequest.getRemoteAddr(), userAgent, userType, tariffType, data, lang,
					supplementaryOfferingsResponse.getLogsReport()));

			data = Utilities.addAttributesToRequest(data, msisdn, servletRequest, lang, userAgent);

			Utilities.printInfoLog(msisdn + "-Request Landed in-" + TRANSACTION_NAME, logger);
			Utilities.printInfoLog(msisdn + "-Request Data-" + data, logger);

			supplementaryOfferingsRequest = mapper.readValue(data, SupplementaryOfferingsRequest.class);
			String requestValidationStatus = Validator.validateRequest(msisdn, supplementaryOfferingsRequest);

			if (requestValidationStatus.equalsIgnoreCase(Constants.VALIDATION_SUCCESS_CODE)) {
				
				supplementaryOfferingsRequest.setIsB2B(isFromB2B);
				supplementaryOfferingsResponse = supplementaryOfferingsBusiness.getSupplementaryOfferings(msisdn,
						supplementaryOfferingsRequest, supplementaryOfferingsResponse);
				sortSupplementaryOfferings(supplementaryOfferingsResponse);
			} else {
				supplementaryOfferingsResponse.setCallStatus(Constants.Call_Status_False);
				supplementaryOfferingsResponse.setResultCode(Constants.VALIDATION_FAILURE_CODE);
				supplementaryOfferingsResponse.setResultDesc(requestValidationStatus);
			}

			supplementaryOfferingsResponse.getLogsReport()
					.setResponseCode(supplementaryOfferingsResponse.getResultCode());
			supplementaryOfferingsResponse.getLogsReport().setRequestTime(requestTime);
			supplementaryOfferingsResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
			supplementaryOfferingsResponse.getLogsReport()
					.setResponse(mapper.writeValueAsString(supplementaryOfferingsResponse));

			// Sending report log into queue.
			supplementaryOfferingsResponse.getLogsReport().setAppVersion(Constants.APP_VERSION_PHASE2);
			Utilities.prepareLogReportForQueue(supplementaryOfferingsResponse.getLogsReport());

			Utilities.printInfoLog(msisdn + "-Response Returned from-" + TRANSACTION_NAME + "-"
					+ mapper.writeValueAsString(supplementaryOfferingsResponse), logger);
		} catch (Exception e) {

			e.printStackTrace();

			Utilities.printErrorLog(Utilities.convertStackTraceToString(e), logger);
			supplementaryOfferingsResponse.setCallStatus(Constants.Call_Status_False);
			supplementaryOfferingsResponse.setResultCode(Constants.EXCEPTION_CODE);
			supplementaryOfferingsResponse
					.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("unexpected.error", lang));
			supplementaryOfferingsResponse.getLogsReport()
					.setResponseCode(supplementaryOfferingsResponse.getResultCode());
			supplementaryOfferingsResponse.getLogsReport().setRequestTime(requestTime);
			supplementaryOfferingsResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
			supplementaryOfferingsResponse.getLogsReport()
					.setResponse(mapper.writeValueAsString(supplementaryOfferingsResponse));

			// Sending report log into queue.
			supplementaryOfferingsResponse.getLogsReport().setAppVersion(Constants.APP_VERSION_PHASE2);
			Utilities.prepareLogReportForQueue(supplementaryOfferingsResponse.getLogsReport());

		}
		return supplementaryOfferingsResponse;
	}

	@RequestMapping(value = "/getsupplementaryofferingsV2", method = RequestMethod.POST, headers = Constants.CONTENT_TYPE_DEFAULT_VALUE)
	public SupplementaryOfferingsResponse getSupplementryOfferingsV2(@RequestBody String data,
			@RequestHeader(value = Constants.MSISDN_KEY, defaultValue = Constants.MSISDN_DEFAULT_VALUE) String msisdn,
			@RequestHeader(value = Constants.LANGUAGE_KEY, defaultValue = Constants.LANGUAGE_DEFAULT_VALUE) String lang,
			@RequestHeader(value = Constants.USER_AGENT_KEY, defaultValue = Constants.USER_AGENT_DEFAULT_VALUE) String userAgent,
			@RequestHeader(value = Constants.SUBSCRIBER_TYPE_KEY, defaultValue = Constants.SUBSCRIBER_TYPE_DEFAULT_VALUE) String userType,
			@RequestHeader(value = Constants.TARIFF_TYPE_KEY, defaultValue = Constants.TARIFF_TYPE_DEFAULT_VALUE) String tariffType,
			@RequestHeader(value = Constants.APP_PHASE_KEY, defaultValue = Constants.APP_PHASE_DEFAULT_VALUE) String isFromB2B,
			HttpServletRequest servletRequest) throws ClassNotFoundException, SQLException, IOException, JMSException {
		ObjectMapper mapper = new ObjectMapper();

		SupplementaryOfferingsBusiness supplementaryOfferingsBusiness = new SupplementaryOfferingsBusiness();
		SupplementaryOfferingsByIDsRequest supplementaryOfferingsRequest = new SupplementaryOfferingsByIDsRequest();
		SupplementaryOfferingsResponse supplementaryOfferingsResponse = new SupplementaryOfferingsResponse();

		String TRANSACTION_NAME = Transactions.SUPPLEMENTARY_SERVICES_TRANSACTION_NAME + " V2 CONTROLLER";
		String requestTime = Utilities.getReportDateTime();
		try {
			JSONObject jObject = new JSONObject(data);
			List<String> offeringIds = Arrays.asList(jObject.getString("offeringIds").split(","));
			// populating request
			//data = Utilities.addAttributesToRequest(data, msisdn, servletRequest, lang, userAgent);
			logger.info("<<<<<<<< Request Packet >>>>>>>>" + data);
			supplementaryOfferingsRequest.setOfferingIds(offeringIds);
			supplementaryOfferingsRequest.setChannel(userAgent);
			supplementaryOfferingsRequest.setiP(servletRequest.getRemoteAddr());
			supplementaryOfferingsRequest.setLang(lang);
			supplementaryOfferingsRequest.setMsisdn(msisdn);
			//supplementaryOfferingsRequest = mapper.readValue(data, SupplementaryOfferingsByIDsRequest.class);
			// Populating report object before processing business logic.
			supplementaryOfferingsResponse.setLogsReport(Utilities.preBusinessReportLoggingData("", msisdn,
					TRANSACTION_NAME, servletRequest.getRemoteAddr(), userAgent, userType, tariffType, data, lang,
					supplementaryOfferingsResponse.getLogsReport()));

			Utilities.printInfoLog(msisdn + "-Request Landed in-" + TRANSACTION_NAME, logger);
			Utilities.printInfoLog(msisdn + "-Request Data-" + data, logger);

			String requestValidationStatus = Validator.validateRequest(msisdn, supplementaryOfferingsRequest);

			if (requestValidationStatus.equalsIgnoreCase(Constants.VALIDATION_SUCCESS_CODE)) {
				
				supplementaryOfferingsRequest.setIsB2B(isFromB2B);
				supplementaryOfferingsResponse = supplementaryOfferingsBusiness.getSupplementaryOfferingsByIDsV2(msisdn,
						supplementaryOfferingsRequest, supplementaryOfferingsResponse);
			} else {
				supplementaryOfferingsResponse.setCallStatus(Constants.Call_Status_False);
				supplementaryOfferingsResponse.setResultCode(Constants.VALIDATION_FAILURE_CODE);
				supplementaryOfferingsResponse.setResultDesc(requestValidationStatus);
			}

			supplementaryOfferingsResponse.getLogsReport()
					.setResponseCode(supplementaryOfferingsResponse.getResultCode());
			supplementaryOfferingsResponse.getLogsReport().setRequestTime(requestTime);
			supplementaryOfferingsResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
			supplementaryOfferingsResponse.getLogsReport()
					.setResponse(mapper.writeValueAsString(supplementaryOfferingsResponse));

			// Sending report log into queue.
			supplementaryOfferingsResponse.getLogsReport().setAppVersion(Constants.APP_VERSION_PHASE2);
			Utilities.prepareLogReportForQueue(supplementaryOfferingsResponse.getLogsReport());

			Utilities.printInfoLog(msisdn + "-Response Returned from-" + TRANSACTION_NAME + "-"
					+ mapper.writeValueAsString(supplementaryOfferingsResponse), logger);
		} catch (Exception e) {

			e.printStackTrace();

			Utilities.printErrorLog(Utilities.convertStackTraceToString(e), logger);
			supplementaryOfferingsResponse.setCallStatus(Constants.Call_Status_False);
			supplementaryOfferingsResponse.setResultCode(Constants.EXCEPTION_CODE);
			supplementaryOfferingsResponse
					.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("unexpected.error", lang));
			supplementaryOfferingsResponse.getLogsReport()
					.setResponseCode(supplementaryOfferingsResponse.getResultCode());
			supplementaryOfferingsResponse.getLogsReport().setRequestTime(requestTime);
			supplementaryOfferingsResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
			supplementaryOfferingsResponse.getLogsReport()
					.setResponse(mapper.writeValueAsString(supplementaryOfferingsResponse));

			// Sending report log into queue.
			supplementaryOfferingsResponse.getLogsReport().setAppVersion(Constants.APP_VERSION_PHASE2);
			Utilities.prepareLogReportForQueue(supplementaryOfferingsResponse.getLogsReport());

		}
		return supplementaryOfferingsResponse;
	}

	private void sortSupplementaryOfferings(SupplementaryOfferingsResponse supplementaryOfferingsResponse) {

		if (supplementaryOfferingsResponse != null && supplementaryOfferingsResponse.getData() != null) {

			// Sorting Internet offers
			if (supplementaryOfferingsResponse.getData().getInternet() != null) {
				List<SupplementryOfferingsData> offers = supplementaryOfferingsResponse.getData().getInternet()
						.getOffers();
				if (offers != null && offers.size() > 0) {
					Collections.sort(offers, new Comparator<SupplementryOfferingsData>() {
						@Override
						public int compare(SupplementryOfferingsData o1, SupplementryOfferingsData o2) {
							if (o1.getHeader() != null && o2.getHeader() != null
									&& o1.getHeader().getSortOrder() != null && o2.getHeader().getSortOrder() != null) {
								return o1.getHeader().getSortOrder().compareTo(o2.getHeader().getSortOrder());
							} else {
								return Constants.MAX_INT;
							}
						}
					});
				}
			}

			// Sorting Compaign offers
			if (supplementaryOfferingsResponse.getData().getCampaign() != null) {
				List<SupplementryOfferingsData> offers = supplementaryOfferingsResponse.getData().getCampaign()
						.getOffers();
				if (offers != null && offers.size() > 0) {
					Collections.sort(offers, new Comparator<SupplementryOfferingsData>() {
						@Override
						public int compare(SupplementryOfferingsData o1, SupplementryOfferingsData o2) {
							if (o1.getHeader() != null && o2.getHeader() != null
									&& o1.getHeader().getSortOrder() != null && o2.getHeader().getSortOrder() != null) {
								return o1.getHeader().getSortOrder().compareTo(o2.getHeader().getSortOrder());
							} else {
								return Constants.MAX_INT;
							}
						}
					});
				}
			}

			// Sorting SMS offers
			if (supplementaryOfferingsResponse.getData().getSms() != null) {
				List<SupplementryOfferingsData> offers = supplementaryOfferingsResponse.getData().getSms().getOffers();
				if (offers != null && offers.size() > 0) {
					Collections.sort(offers, new Comparator<SupplementryOfferingsData>() {
						@Override
						public int compare(SupplementryOfferingsData o1, SupplementryOfferingsData o2) {
							if (o1.getHeader() != null && o2.getHeader() != null
									&& o1.getHeader().getSortOrder() != null && o2.getHeader().getSortOrder() != null) {
								return o1.getHeader().getSortOrder().compareTo(o2.getHeader().getSortOrder());
							} else {
								return Constants.MAX_INT;
							}
						}
					});
				}
			}

			// Sorting SMS offers
			if (supplementaryOfferingsResponse.getData().getCall() != null) {
				List<SupplementryOfferingsData> offers = supplementaryOfferingsResponse.getData().getCall().getOffers();
				if (offers != null && offers.size() > 0) {
					Collections.sort(offers, new Comparator<SupplementryOfferingsData>() {
						@Override
						public int compare(SupplementryOfferingsData o1, SupplementryOfferingsData o2) {
							if (o1.getHeader() != null && o2.getHeader() != null
									&& o1.getHeader().getSortOrder() != null && o2.getHeader().getSortOrder() != null) {
								return o1.getHeader().getSortOrder().compareTo(o2.getHeader().getSortOrder());
							} else {
								return Constants.MAX_INT;
							}
						}
					});
				}
			}

			// Sorting TM offers
			if (supplementaryOfferingsResponse.getData().getTm() != null) {
				List<SupplementryOfferingsData> offers = supplementaryOfferingsResponse.getData().getTm().getOffers();
				if (offers != null && offers.size() > 0) {
					Collections.sort(offers, new Comparator<SupplementryOfferingsData>() {
						@Override
						public int compare(SupplementryOfferingsData o1, SupplementryOfferingsData o2) {
							if (o1.getHeader() != null && o2.getHeader() != null
									&& o1.getHeader().getSortOrder() != null && o2.getHeader().getSortOrder() != null) {
								return o1.getHeader().getSortOrder().compareTo(o2.getHeader().getSortOrder());
							} else {
								return Constants.MAX_INT;
							}
						}
					});
				}
			}

			// Sorting Hybrid offers
			if (supplementaryOfferingsResponse.getData().getHybrid() != null) {
				List<SupplementryOfferingsData> offers = supplementaryOfferingsResponse.getData().getHybrid()
						.getOffers();
				if (offers != null && offers.size() > 0) {
					Collections.sort(offers, new Comparator<SupplementryOfferingsData>() {
						@Override
						public int compare(SupplementryOfferingsData o1, SupplementryOfferingsData o2) {
							if (o1.getHeader() != null && o2.getHeader() != null
									&& o1.getHeader().getSortOrder() != null && o2.getHeader().getSortOrder() != null) {
								return o1.getHeader().getSortOrder().compareTo(o2.getHeader().getSortOrder());
							} else {
								return Constants.MAX_INT;
							}
						}
					});
				}
			}

			// Sorting Roaming offers
			if (supplementaryOfferingsResponse.getData().getRoaming() != null) {
				List<SupplementryOfferingsData> offers = supplementaryOfferingsResponse.getData().getRoaming()
						.getOffers();
				if (offers != null && offers.size() > 0) {
					Collections.sort(offers, new Comparator<SupplementryOfferingsData>() {
						@Override
						public int compare(SupplementryOfferingsData o1, SupplementryOfferingsData o2) {
							if (o1.getHeader() != null && o2.getHeader() != null
									&& o1.getHeader().getSortOrder() != null && o2.getHeader().getSortOrder() != null) {
								return o1.getHeader().getSortOrder().compareTo(o2.getHeader().getSortOrder());
							} else {
								return Constants.MAX_INT;
							}
						}
					});
				}
			}

		}
	}

	@RequestMapping(value = "/changesupplementaryoffering", method = RequestMethod.POST, headers = Constants.CONTENT_TYPE_DEFAULT_VALUE)
	public ChangeSupplementaryOfferingsResponse changeSupplementaryOffering(@RequestBody String data,
			@RequestHeader(value = Constants.MSISDN_KEY, defaultValue = Constants.MSISDN_DEFAULT_VALUE) String msisdn,
			@RequestHeader(value = Constants.LANGUAGE_KEY, defaultValue = Constants.LANGUAGE_DEFAULT_VALUE) String lang,
			@RequestHeader(value = Constants.USER_AGENT_KEY, defaultValue = Constants.USER_AGENT_DEFAULT_VALUE) String userAgent,
			@RequestHeader(value = Constants.SUBSCRIBER_TYPE_KEY, defaultValue = Constants.SUBSCRIBER_TYPE_DEFAULT_VALUE) String userType,
			@RequestHeader(value = Constants.TARIFF_TYPE_KEY, defaultValue = Constants.TARIFF_TYPE_DEFAULT_VALUE) String tariffType,
			@RequestHeader(value = Constants.APP_PHASE_KEY, defaultValue = Constants.APP_PHASE_DEFAULT_VALUE) String isFromB2B,
			HttpServletRequest servletRequest) throws ClassNotFoundException, SQLException, JsonParseException,
			JsonMappingException, IOException, JMSException {
		ObjectMapper mapper = new ObjectMapper();

		SupplementaryOfferingsBusiness supplementaryOfferingsBusiness = new SupplementaryOfferingsBusiness();
		ChangeSupplementaryOfferingsRequest changeSupplementaryOfferingRequest = new ChangeSupplementaryOfferingsRequest();
		ChangeSupplementaryOfferingsResponse changeSupplementaryOfferingResponse = new ChangeSupplementaryOfferingsResponse();

		String TRANSACTION_NAME = Transactions.CHANGE_SUPPLIMENTRY_OFFERING_TRANSACTION_NAME + " CONTROLLER";
		String requestTime = Utilities.getReportDateTime();
		try {
			/*
			 * String msisdn =
			 * Utilities.getValueFromJSON(Utilities.splitToken(Utilities.
			 * decodeString(token)).toString(), Constants.MSISDN_KEY);
			 */

			// Populating report object before processing business logic.
			changeSupplementaryOfferingResponse.setLogsReport(Utilities.preBusinessReportLoggingData("", msisdn,
					TRANSACTION_NAME, servletRequest.getRemoteAddr(), userAgent, userType, tariffType, data, lang,
					changeSupplementaryOfferingResponse.getLogsReport()));

			data = Utilities.addAttributesToRequest(data, msisdn, servletRequest, lang, userAgent);

			Utilities.printInfoLog(msisdn + "-Request Landed in-" + TRANSACTION_NAME, logger);
			Utilities.printInfoLog(msisdn + "-Request Data-" + data, logger);
			JSONObject jObject = new JSONObject(data);
			if(jObject.has("individualMsisdn")){
				jObject.remove("msisdn");
				jObject.put("msisdn", jObject.get("individualMsisdn"));
				jObject.remove("individualMsisdn");
			}
			changeSupplementaryOfferingRequest = mapper.readValue(jObject.toString(), ChangeSupplementaryOfferingsRequest.class);

			// Logging specific params
			changeSupplementaryOfferingResponse.getLogsReport()
					.setSupplementaryOfferingId(changeSupplementaryOfferingRequest.getOfferingId());

			changeSupplementaryOfferingResponse.getLogsReport()
					.setSupplementaryOfferingName(changeSupplementaryOfferingRequest.getOfferName());
			changeSupplementaryOfferingResponse.getLogsReport()
					.setActionType(changeSupplementaryOfferingRequest.getActionType());

			String requestValidationStatus = Validator.validateRequest(msisdn, changeSupplementaryOfferingRequest);

			if (requestValidationStatus.equalsIgnoreCase(Constants.VALIDATION_SUCCESS_CODE)) {
				changeSupplementaryOfferingRequest.setIsB2B(isFromB2B);
				changeSupplementaryOfferingResponse = supplementaryOfferingsBusiness.changeSupplementaryOffering(msisdn,
						changeSupplementaryOfferingRequest, changeSupplementaryOfferingResponse);
			} else {
				changeSupplementaryOfferingResponse.setCallStatus(Constants.Call_Status_False);
				changeSupplementaryOfferingResponse.setResultCode(Constants.VALIDATION_FAILURE_CODE);
				changeSupplementaryOfferingResponse.setResultDesc(requestValidationStatus);
			}

			changeSupplementaryOfferingResponse.getLogsReport()
					.setResponseCode(changeSupplementaryOfferingResponse.getResultCode());
			changeSupplementaryOfferingResponse.getLogsReport().setRequestTime(requestTime);
			changeSupplementaryOfferingResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
			changeSupplementaryOfferingResponse.getLogsReport()
					.setResponse(mapper.writeValueAsString(changeSupplementaryOfferingResponse));

			// Sending report log into queue.
			changeSupplementaryOfferingResponse.getLogsReport().setAppVersion(Constants.APP_VERSION_PHASE2);
			Utilities.prepareLogReportForQueue(changeSupplementaryOfferingResponse.getLogsReport());

			Utilities.printInfoLog(msisdn + "-Response Returned from-" + TRANSACTION_NAME + "-"
					+ mapper.writeValueAsString(changeSupplementaryOfferingResponse), logger);
		} catch (Exception e) {
			Utilities.printErrorLog(Utilities.convertStackTraceToString(e), logger);
			changeSupplementaryOfferingResponse.setCallStatus(Constants.Call_Status_False);

			changeSupplementaryOfferingResponse.setResultCode(Constants.EXCEPTION_CODE);
			changeSupplementaryOfferingResponse
					.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("unexpected.error", lang));

			changeSupplementaryOfferingResponse.getLogsReport()
					.setResponseCode(changeSupplementaryOfferingResponse.getResultCode());
			changeSupplementaryOfferingResponse.getLogsReport().setRequestTime(requestTime);
			changeSupplementaryOfferingResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
			changeSupplementaryOfferingResponse.getLogsReport()
					.setResponse(mapper.writeValueAsString(changeSupplementaryOfferingResponse));

			// Sending report log into queue.
			changeSupplementaryOfferingResponse.getLogsReport().setAppVersion(Constants.APP_VERSION_PHASE2);
			Utilities.prepareLogReportForQueue(changeSupplementaryOfferingResponse.getLogsReport());

		}
		return changeSupplementaryOfferingResponse;
	}
	
	@RequestMapping(value = "/changesupplementaryofferingbulk", method = RequestMethod.POST, headers = Constants.CONTENT_TYPE_DEFAULT_VALUE)
	public ChangeSupplementaryOfferingsResponsebulk changeSupplementaryOfferingbulk(@RequestBody String data,
			@RequestHeader(value = Constants.MSISDN_KEY, defaultValue = Constants.MSISDN_DEFAULT_VALUE) String msisdn,
			@RequestHeader(value = Constants.LANGUAGE_KEY, defaultValue = Constants.LANGUAGE_DEFAULT_VALUE) String lang,
			@RequestHeader(value = Constants.USER_AGENT_KEY, defaultValue = Constants.USER_AGENT_DEFAULT_VALUE) String userAgent,
			@RequestHeader(value = Constants.SUBSCRIBER_TYPE_KEY, defaultValue = Constants.SUBSCRIBER_TYPE_DEFAULT_VALUE) String userType,
			@RequestHeader(value = Constants.TARIFF_TYPE_KEY, defaultValue = Constants.TARIFF_TYPE_DEFAULT_VALUE) String tariffType,
			@RequestHeader(value = Constants.APP_PHASE_KEY, defaultValue = Constants.APP_PHASE_DEFAULT_VALUE) String isFromB2B,
			HttpServletRequest servletRequest) throws ClassNotFoundException, SQLException, JsonParseException,
			JsonMappingException, IOException, JMSException {
		ObjectMapper mapper = new ObjectMapper();

		SupplementaryOfferingsBusiness supplementaryOfferingsBusiness = new SupplementaryOfferingsBusiness();
		ChangeSupplementaryOfferingsRequestBulk changeSupplementaryOfferingRequest = new ChangeSupplementaryOfferingsRequestBulk();
		ChangeSupplementaryOfferingsResponsebulk changeSupplementaryOfferingResponse = new ChangeSupplementaryOfferingsResponsebulk();

		String TRANSACTION_NAME = Transactions.CHANGE_SUPPLIMENTRY_OFFERING_BULK_TRANSACTION_NAME + " CONTROLLER";
		String requestTime = Utilities.getReportDateTime();
		try {
			/*
			 * String msisdn =
			 * Utilities.getValueFromJSON(Utilities.splitToken(Utilities.
			 * decodeString(token)).toString(), Constants.MSISDN_KEY);
			 */

			// Populating report object before processing business logic.
			changeSupplementaryOfferingResponse.setLogsReport(Utilities.preBusinessReportLoggingData("", msisdn,
					TRANSACTION_NAME, servletRequest.getRemoteAddr(), userAgent, userType, tariffType, data, lang,
					changeSupplementaryOfferingResponse.getLogsReport()));

			data = Utilities.addAttributesToRequest(data, msisdn, servletRequest, lang, userAgent);

			Utilities.printInfoLog(msisdn + "-Request Landed in-" + TRANSACTION_NAME, logger);
			Utilities.printInfoLog(msisdn + "-Request Data-" + data, logger);

			changeSupplementaryOfferingRequest = mapper.readValue(data, ChangeSupplementaryOfferingsRequestBulk.class);

			// Logging specific params
			changeSupplementaryOfferingResponse.getLogsReport()
					.setSupplementaryOfferingId(changeSupplementaryOfferingRequest.getOfferingId());
			changeSupplementaryOfferingResponse.getLogsReport()
					.setActionType(changeSupplementaryOfferingRequest.getActionType());

			String requestValidationStatus = Validator.validateRequest(msisdn, changeSupplementaryOfferingRequest);

			if (requestValidationStatus.equalsIgnoreCase(Constants.VALIDATION_SUCCESS_CODE)) {
				changeSupplementaryOfferingRequest.setIsB2B(isFromB2B);
				changeSupplementaryOfferingResponse = supplementaryOfferingsBusiness.changeSupplementaryOfferingbulk(msisdn,
						changeSupplementaryOfferingRequest, changeSupplementaryOfferingResponse);
			} else {
				changeSupplementaryOfferingResponse.setCallStatus(Constants.Call_Status_False);
				changeSupplementaryOfferingResponse.setResultCode(Constants.VALIDATION_FAILURE_CODE);
				changeSupplementaryOfferingResponse.setResultDesc(requestValidationStatus);
			}

			changeSupplementaryOfferingResponse.getLogsReport()
					.setResponseCode(changeSupplementaryOfferingResponse.getResultCode());
			changeSupplementaryOfferingResponse.getLogsReport().setRequestTime(requestTime);
			changeSupplementaryOfferingResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
			changeSupplementaryOfferingResponse.getLogsReport()
					.setResponse(mapper.writeValueAsString(changeSupplementaryOfferingResponse));

			// Sending report log into queue.
			changeSupplementaryOfferingResponse.getLogsReport().setAppVersion(Constants.APP_VERSION_PHASE2);
			Utilities.prepareLogReportForQueue(changeSupplementaryOfferingResponse.getLogsReport());

			Utilities.printInfoLog(msisdn + "-Response Returned from-" + TRANSACTION_NAME + "-"
					+ mapper.writeValueAsString(changeSupplementaryOfferingResponse), logger);
		} catch (Exception e) {
			Utilities.printErrorLog(Utilities.convertStackTraceToString(e), logger);
			changeSupplementaryOfferingResponse.setCallStatus(Constants.Call_Status_False);

			changeSupplementaryOfferingResponse.setResultCode(Constants.EXCEPTION_CODE);
			changeSupplementaryOfferingResponse
					.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("unexpected.error", lang));

			changeSupplementaryOfferingResponse.getLogsReport()
					.setResponseCode(changeSupplementaryOfferingResponse.getResultCode());
			changeSupplementaryOfferingResponse.getLogsReport().setRequestTime(requestTime);
			changeSupplementaryOfferingResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
			changeSupplementaryOfferingResponse.getLogsReport()
					.setResponse(mapper.writeValueAsString(changeSupplementaryOfferingResponse));

			// Sending report log into queue.
			changeSupplementaryOfferingResponse.getLogsReport().setAppVersion(Constants.APP_VERSION_PHASE2);
			Utilities.prepareLogReportForQueue(changeSupplementaryOfferingResponse.getLogsReport());

		}
		return changeSupplementaryOfferingResponse;
	}
}
