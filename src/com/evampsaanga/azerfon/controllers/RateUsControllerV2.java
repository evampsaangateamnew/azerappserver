package com.evampsaanga.azerfon.controllers;

import java.io.IOException;
import java.sql.SQLException;

import javax.jms.JMSException;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONObject;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.evampsaanga.azerfon.business.RateUsBusiness;
import com.evampsaanga.azerfon.common.messagesmappings.GetMessagesMappings;
import com.evampsaanga.azerfon.common.utilities.Constants;
import com.evampsaanga.azerfon.common.utilities.GetConfigurations;
import com.evampsaanga.azerfon.common.utilities.Transactions;
import com.evampsaanga.azerfon.common.utilities.Utilities;
import com.evampsaanga.azerfon.common.utilities.Validator;
import com.evampsaanga.azerfon.models.getrateus.GetRateUsRequest;
import com.evampsaanga.azerfon.models.getrateus.GetRateUsResponse;
import com.evampsaanga.azerfon.models.getrateus.GetRateUsResponseData;
import com.evampsaanga.azerfon.models.rateus.RateUsRequest;
import com.evampsaanga.azerfon.models.rateus.RateUsResponse;
import com.evampsaanga.azerfon.restclient.RestClient;

@RestController
@RequestMapping("/rateV2")
public class RateUsControllerV2 {
	Logger logger = Logger.getLogger(RateUsControllerV2.class);

	@RequestMapping(value = "/rateus", method = RequestMethod.POST, headers = Constants.CONTENT_TYPE_DEFAULT_VALUE)
	public RateUsResponse rateus(@RequestBody String data,
			@RequestHeader(value = Constants.MSISDN_KEY, defaultValue = Constants.MSISDN_DEFAULT_VALUE) String msisdn,
			@RequestHeader(value = Constants.LANGUAGE_KEY, defaultValue = Constants.LANGUAGE_DEFAULT_VALUE) String lang,
			@RequestHeader(value = Constants.USER_AGENT_KEY, defaultValue = Constants.USER_AGENT_DEFAULT_VALUE) String userAgent,
			@RequestHeader(value = Constants.SUBSCRIBER_TYPE_KEY, defaultValue = Constants.SUBSCRIBER_TYPE_DEFAULT_VALUE) String userType,
			@RequestHeader(value = Constants.TARIFF_TYPE_KEY, defaultValue = Constants.TARIFF_TYPE_DEFAULT_VALUE) String tariffType,
			@RequestHeader(value = Constants.APP_PHASE_KEY, defaultValue = Constants.APP_PHASE_DEFAULT_VALUE) String isFromB2B,
			HttpServletRequest servletRequest) throws ClassNotFoundException, SQLException, JsonParseException,
			JsonMappingException, IOException, JMSException {
		ObjectMapper mapper = new ObjectMapper();
		RateUsRequest rateUsRequest = new RateUsRequest();
		RateUsResponse rateUsResponse = new RateUsResponse();
		RateUsBusiness rateUsBusiness = new RateUsBusiness();

		String TRANSACTION_NAME = Transactions.TARRIF_DETAILS_TRANSACTION_NAME + " CONTROLLER";
		String requestTime = Utilities.getReportDateTime();
		try {
			rateUsResponse.setLogsReport(
					Utilities.preBusinessReportLoggingData("", msisdn, TRANSACTION_NAME, servletRequest.getRemoteAddr(),
							userAgent, userType, tariffType, data, lang, rateUsResponse.getLogsReport()));
			Utilities.printInfoLog(msisdn + "-Request Landed in-" + TRANSACTION_NAME, logger);
			Utilities.printInfoLog(msisdn + "-Request Data-" + data, logger);
			data = Utilities.addAttributesToRequest(data, msisdn, servletRequest, lang, userAgent);
			rateUsRequest = mapper.readValue(data, RateUsRequest.class);
			String requestValidationStatus = Validator.validateRequest(msisdn, rateUsRequest);
			if (requestValidationStatus.equalsIgnoreCase(Constants.VALIDATION_SUCCESS_CODE)) {

				rateUsRequest.setIsB2B(isFromB2B);
				rateUsResponse = rateUsBusiness.rateus(msisdn, rateUsRequest, rateUsResponse);
				rateUsResponse.getLogsReport().setResponseCode(rateUsResponse.getResultCode());
				rateUsResponse.getLogsReport().setRequestTime(requestTime);
				rateUsResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
				rateUsResponse.getLogsReport().setResponse(mapper.writeValueAsString(rateUsResponse));

				// Sending report log into queue.
				rateUsResponse.getLogsReport().setAppVersion(Constants.APP_VERSION_PHASE2);
				Utilities.prepareLogReportForQueue(rateUsResponse.getLogsReport());
			} else {
				rateUsResponse.setCallStatus(Constants.Call_Status_False);
				rateUsResponse.setResultCode(Constants.VALIDATION_FAILURE_CODE);
				rateUsResponse.setResultDesc(requestValidationStatus);
				rateUsResponse.getLogsReport().setResponseCode(rateUsResponse.getResultCode());
				rateUsResponse.getLogsReport().setRequestTime(requestTime);
				rateUsResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
				rateUsResponse.getLogsReport().setResponse(mapper.writeValueAsString(rateUsResponse));

				// Sending report log into queue.
				rateUsResponse.getLogsReport().setAppVersion(Constants.APP_VERSION_PHASE2);
				Utilities.prepareLogReportForQueue(rateUsResponse.getLogsReport());
			}

		} catch (Exception e) {
			logger.error("ERROR:", e);
			rateUsResponse.setCallStatus(Constants.Call_Status_False);
			rateUsResponse.setResultCode(Constants.EXCEPTION_CODE);

			rateUsResponse.getLogsReport().setResponseCode(rateUsResponse.getResultCode());
			rateUsResponse.getLogsReport().setRequestTime(requestTime);
			rateUsResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
			rateUsResponse.getLogsReport().setResponse(mapper.writeValueAsString(rateUsResponse));

			// Sending report log into queue.
			rateUsResponse.getLogsReport().setAppVersion(Constants.APP_VERSION_PHASE2);
			Utilities.prepareLogReportForQueue(rateUsResponse.getLogsReport());
		}
		logger.info("<<<<<< RESPONSE IS: >>>>>>" + rateUsResponse.getResultCode());
		return rateUsResponse;

	}

	@RequestMapping(value = "/getrateus", method = RequestMethod.POST, headers = Constants.CONTENT_TYPE_DEFAULT_VALUE)
	public GetRateUsResponse getRateus(@RequestBody String data,
			@RequestHeader(value = Constants.MSISDN_KEY, defaultValue = Constants.MSISDN_DEFAULT_VALUE) String msisdn,
			@RequestHeader(value = Constants.LANGUAGE_KEY, defaultValue = Constants.LANGUAGE_DEFAULT_VALUE) String lang,
			@RequestHeader(value = Constants.USER_AGENT_KEY, defaultValue = Constants.USER_AGENT_DEFAULT_VALUE) String userAgent,
			@RequestHeader(value = Constants.SUBSCRIBER_TYPE_KEY, defaultValue = Constants.SUBSCRIBER_TYPE_DEFAULT_VALUE) String userType,
			@RequestHeader(value = Constants.TARIFF_TYPE_KEY, defaultValue = Constants.TARIFF_TYPE_DEFAULT_VALUE) String tariffType,
			@RequestHeader(value = Constants.APP_PHASE_KEY, defaultValue = Constants.APP_PHASE_DEFAULT_VALUE) String isFromB2B,
			HttpServletRequest servletRequest) throws ClassNotFoundException, SQLException, JsonParseException,
			JsonMappingException, IOException, JMSException {
		ObjectMapper mapper = new ObjectMapper();
		GetRateUsRequest rateUsRequest = new GetRateUsRequest();
		GetRateUsResponse rateUsResponse = new GetRateUsResponse();

		String TRANSACTION_NAME = Transactions.GET_RATE_US_TRANSACTION_NAME + " CONTROLLER";
		String requestTime = Utilities.getReportDateTime();
		try {
			rateUsResponse.setLogsReport(
					Utilities.preBusinessReportLoggingData("", msisdn, TRANSACTION_NAME, servletRequest.getRemoteAddr(),
							userAgent, userType, tariffType, data, lang, rateUsResponse.getLogsReport()));
			Utilities.printInfoLog(msisdn + "-Request Landed in-" + TRANSACTION_NAME, logger);
			Utilities.printInfoLog(msisdn + "-Request Data-" + data, logger);
			data = Utilities.addAttributesToRequest(data, msisdn, servletRequest, lang, userAgent);
			rateUsRequest = mapper.readValue(data, GetRateUsRequest.class);
			String requestValidationStatus = Validator.validateRequest(msisdn, rateUsRequest);
			if (requestValidationStatus.equalsIgnoreCase(Constants.VALIDATION_SUCCESS_CODE)) {

				RestClient rc = new RestClient();

				JSONObject requestJsonESB = new JSONObject();
				requestJsonESB.put("lang", "3");
				requestJsonESB.put("entityId", rateUsRequest.getEntityId());
				requestJsonESB.put("type", "attributes");
				requestJsonESB.put("msisdn", rateUsRequest.getMsisdn());

				String path = GetConfigurations.getConfigurationFromCache("magento.app.appresume.url");
				Utilities.printDebugLog(msisdn + "-Request Call to ESB", logger);
				Utilities.printDebugLog(msisdn + "-Path-" + path, logger);
				Utilities.printDebugLog(msisdn + "-Request Packet-" + requestJsonESB.toString(), logger);

				rateUsResponse.getLogsReport().setEsbRequestTime(Utilities.getReportDateTime());

				String response = rc.getResponseFromESB(path, requestJsonESB.toString());
				rateUsResponse.getLogsReport().setEsbResponseTime(Utilities.getReportDateTime());

				Utilities.printDebugLog(msisdn + "-Received response from ESB-" + response, logger);

				// Logging ESB response code and description.
				rateUsResponse.setLogsReport(Utilities.logESBParamsintoReportLog(requestJsonESB.toString(), response,
						rateUsResponse.getLogsReport()));
				if (response != null && !response.isEmpty()) {
					GetRateUsResponseData getRateUsResponseData = new GetRateUsResponseData();
					getRateUsResponseData.setPic_tnc(Utilities.getValueFromJSON(response, "pic_tnc"));
					if (Utilities.getValueFromJSON(response, "rateus_android").length() > 0)
						getRateUsResponseData.setRateus_android(Utilities.getValueFromJSON(response, "rateus_android"));
					if (Utilities.getValueFromJSON(response, "rateus_ios").length() > 0)
						getRateUsResponseData.setRateus_ios(Utilities.getValueFromJSON(response, "rateus_ios"));
					rateUsResponse.setData(getRateUsResponseData);
					rateUsResponse.setCallStatus(Constants.Call_Status_True);
					rateUsResponse.setResultCode(Constants.APP_SERVER_SUCCESS_CODE);
					rateUsResponse.setResultDesc(
							GetMessagesMappings.getMessageFromResourceBundle("success", rateUsRequest.getLang()));

				}
				rateUsResponse.getLogsReport().setResponseCode(rateUsResponse.getResultCode());
				rateUsResponse.getLogsReport().setRequestTime(requestTime);
				rateUsResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
				rateUsResponse.getLogsReport().setResponse(mapper.writeValueAsString(rateUsResponse));

				// Sending report log into queue.
				rateUsResponse.getLogsReport().setAppVersion(Constants.APP_VERSION_PHASE2);
				Utilities.prepareLogReportForQueue(rateUsResponse.getLogsReport());
			} else {
				rateUsResponse.setCallStatus(Constants.Call_Status_False);
				rateUsResponse.setResultCode(Constants.VALIDATION_FAILURE_CODE);
				rateUsResponse.setResultDesc(requestValidationStatus);
				rateUsResponse.getLogsReport().setResponseCode(rateUsResponse.getResultCode());
				rateUsResponse.getLogsReport().setRequestTime(requestTime);
				rateUsResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
				rateUsResponse.getLogsReport().setResponse(mapper.writeValueAsString(rateUsResponse));

				// Sending report log into queue.
				rateUsResponse.getLogsReport().setAppVersion(Constants.APP_VERSION_PHASE2);
				Utilities.prepareLogReportForQueue(rateUsResponse.getLogsReport());
			}

		} catch (Exception e) {
			logger.error("ERROR:", e);
			rateUsResponse.setCallStatus(Constants.Call_Status_False);
			rateUsResponse.setResultCode(Constants.EXCEPTION_CODE);

			rateUsResponse.getLogsReport().setResponseCode(rateUsResponse.getResultCode());
			rateUsResponse.getLogsReport().setRequestTime(requestTime);
			rateUsResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
			rateUsResponse.getLogsReport().setResponse(mapper.writeValueAsString(rateUsResponse));

			// Sending report log into queue.
			rateUsResponse.getLogsReport().setAppVersion(Constants.APP_VERSION_PHASE2);
			Utilities.prepareLogReportForQueue(rateUsResponse.getLogsReport());
		}
		logger.info("<<<<<< RESPONSE IS: >>>>>>" + rateUsResponse.getResultCode());
		return rateUsResponse;

	}
}
