package com.evampsaanga.azerfon.controllers;

import java.io.IOException;
import java.sql.SQLException;

import javax.jms.JMSException;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.evampsaanga.azerfon.business.TopUpBusiness;
import com.evampsaanga.azerfon.common.messagesmappings.GetMessagesMappings;
import com.evampsaanga.azerfon.common.utilities.Constants;
import com.evampsaanga.azerfon.common.utilities.Transactions;
import com.evampsaanga.azerfon.common.utilities.Utilities;
import com.evampsaanga.azerfon.common.utilities.Validator;
import com.evampsaanga.azerfon.models.generalservices.predefineddata.topup.AddPaymentSchedulerRequest;
import com.evampsaanga.azerfon.models.generalservices.predefineddata.topup.AddPaymentSchedulerResponse;
import com.evampsaanga.azerfon.models.generalservices.predefineddata.topup.DeleteFastPaymentRequest;
import com.evampsaanga.azerfon.models.generalservices.predefineddata.topup.DeleteFastPaymentResponse;
import com.evampsaanga.azerfon.models.generalservices.predefineddata.topup.DeletePaymentSchedulerRequest;
import com.evampsaanga.azerfon.models.generalservices.predefineddata.topup.DeletePaymentSchedulerResponse;
import com.evampsaanga.azerfon.models.generalservices.predefineddata.topup.DeleteSavedCardRequest;
import com.evampsaanga.azerfon.models.generalservices.predefineddata.topup.DeleteSavedCardResponse;
import com.evampsaanga.azerfon.models.generalservices.predefineddata.topup.FastPaymentRequest;
import com.evampsaanga.azerfon.models.generalservices.predefineddata.topup.FastPaymentResponse;
import com.evampsaanga.azerfon.models.generalservices.predefineddata.topup.MakePaymentRequest;
import com.evampsaanga.azerfon.models.generalservices.predefineddata.topup.MakePaymentResponse;
import com.evampsaanga.azerfon.models.generalservices.predefineddata.topup.PaymentKeyRequest;
import com.evampsaanga.azerfon.models.generalservices.predefineddata.topup.PaymentKeyResponse;
import com.evampsaanga.azerfon.models.generalservices.predefineddata.topup.SavedCardRequest;
import com.evampsaanga.azerfon.models.generalservices.predefineddata.topup.SavedCardResponse;
import com.evampsaanga.azerfon.models.generalservices.predefineddata.topup.ScheduledPaymentsRequest;
import com.evampsaanga.azerfon.models.generalservices.predefineddata.topup.ScheduledPaymentsResponse;
import com.evampsaanga.azerfon.models.generalservices.predefineddata.topup.VerifyCardRequest;
import com.evampsaanga.azerfon.models.generalservices.predefineddata.topup.VerifyCardResponse;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@RestController
@RequestMapping("/plasticcard")
public class TopUpController {

	Logger logger = Logger.getLogger(TopUpController.class);


	@RequestMapping(value = "/getsavedcards", method = RequestMethod.POST, headers = Constants.CONTENT_TYPE_DEFAULT_VALUE)
	public SavedCardResponse getSavedCards(
			@RequestHeader(value = Constants.TOKEN_KEY, defaultValue = Constants.TOKEN_DEFAULT_VALUE) String token,
			@RequestHeader(value = Constants.LANGUAGE_KEY, defaultValue = Constants.LANGUAGE_DEFAULT_VALUE) String lang,
			@RequestHeader(value = Constants.USER_AGENT_KEY, defaultValue = Constants.USER_AGENT_DEFAULT_VALUE) String userAgent,
			@RequestHeader(value = Constants.SUBSCRIBER_TYPE_KEY, defaultValue = Constants.SUBSCRIBER_TYPE_DEFAULT_VALUE) String userType,
			@RequestHeader(value = Constants.TARIFF_TYPE_KEY, defaultValue = Constants.TARIFF_TYPE_DEFAULT_VALUE) String tariffType,
			HttpServletRequest servletRequest) throws JsonParseException, JsonMappingException, IOException,
			JMSException, ClassNotFoundException, SQLException {

		ObjectMapper mapper = new ObjectMapper();
		String data = "{}";

		SavedCardRequest savedCardRequest = new SavedCardRequest();
		SavedCardResponse savedCardResponse = new SavedCardResponse();

		String TRANSACTION_NAME = Transactions.GET_SAVED_CARDS_TRANSACTION_NAME + " CONTROLLER";
		String requestTime = Utilities.getReportDateTime();

		TopUpBusiness topUpBusiness = new TopUpBusiness();
		
		try {
			String msisdn = Utilities.getValueFromJSON(Utilities.splitToken(Utilities.decodeString(token)).toString(),
					Constants.MSISDN_KEY);
//comment
			// Populating report object before processing business logic.
			savedCardResponse.setLogsReport(Utilities.preBusinessReportLoggingData(token, msisdn, TRANSACTION_NAME,
					servletRequest.getRemoteAddr(), userAgent, userType, tariffType, data, lang,
					savedCardResponse.getLogsReport()));

			String deviceID = servletRequest.getHeader("deviceID");

			data = Utilities.addAttributesToRequest(data, msisdn, servletRequest, lang, userAgent);

			Utilities.printInfoLog(msisdn + "-Request Landed in-" + TRANSACTION_NAME, logger);
			Utilities.printInfoLog(msisdn + "-Request Data-" + data, logger);

			savedCardRequest = mapper.readValue(data, SavedCardRequest.class);

			String requestValidationStatus = Validator.validateRequest(msisdn, savedCardRequest);

			if (requestValidationStatus.equalsIgnoreCase(Constants.VALIDATION_SUCCESS_CODE)) {
				savedCardResponse = topUpBusiness.getSavedCardsBusiness(msisdn, deviceID, savedCardRequest,
						savedCardResponse);
			} else {
				savedCardResponse.setCallStatus(Constants.Call_Status_False);
				savedCardResponse.setResultCode(Constants.VALIDATION_FAILURE_CODE);
				savedCardResponse.setResultDesc(requestValidationStatus);
			}

			savedCardResponse.getLogsReport().setResponseCode(savedCardResponse.getResultCode());
			savedCardResponse.getLogsReport().setRequestTime(requestTime);
			savedCardResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
			savedCardResponse.getLogsReport().setResponse(mapper.writeValueAsString(savedCardResponse));

			// Sending report log into queue.
			Utilities.prepareLogReportForQueue(savedCardResponse.getLogsReport());

			Utilities.printInfoLog(msisdn + "-Response Returned from-" + TRANSACTION_NAME + "-"
					+ mapper.writeValueAsString(savedCardResponse), logger);
		} catch (Exception e) {
			Utilities.printErrorLog(Utilities.convertStackTraceToString(e), logger);
			savedCardResponse.setCallStatus(Constants.Call_Status_False);

			savedCardResponse.setResultCode(Constants.EXCEPTION_CODE);
			savedCardResponse.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("unexpected.error", lang));

			savedCardResponse.getLogsReport().setResponseCode(savedCardResponse.getResultCode());
			savedCardResponse.getLogsReport().setRequestTime(requestTime);
			savedCardResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
			savedCardResponse.getLogsReport().setResponse(mapper.writeValueAsString(savedCardResponse));

			// Sending report log into queue.
			Utilities.prepareLogReportForQueue(savedCardResponse.getLogsReport());
		}
		return savedCardResponse;

	}

	@RequestMapping(value = "/getfastpayments", method = RequestMethod.POST, headers = Constants.CONTENT_TYPE_DEFAULT_VALUE)
	public FastPaymentResponse getFastPayments(
			@RequestHeader(value = Constants.TOKEN_KEY, defaultValue = Constants.TOKEN_DEFAULT_VALUE) String token,
			@RequestHeader(value = Constants.LANGUAGE_KEY, defaultValue = Constants.LANGUAGE_DEFAULT_VALUE) String lang,
			@RequestHeader(value = Constants.USER_AGENT_KEY, defaultValue = Constants.USER_AGENT_DEFAULT_VALUE) String userAgent,
			@RequestHeader(value = Constants.SUBSCRIBER_TYPE_KEY, defaultValue = Constants.SUBSCRIBER_TYPE_DEFAULT_VALUE) String userType,
			@RequestHeader(value = Constants.TARIFF_TYPE_KEY, defaultValue = Constants.TARIFF_TYPE_DEFAULT_VALUE) String tariffType,
			HttpServletRequest servletRequest) throws JsonParseException, JsonMappingException, IOException,
			JMSException, ClassNotFoundException, SQLException {

		ObjectMapper mapper = new ObjectMapper();
		String data = "{}";

		TopUpBusiness topUpBusiness = new TopUpBusiness();
		
		FastPaymentRequest fastPaymentRequest = new FastPaymentRequest();
		FastPaymentResponse fastPaymentResponse = new FastPaymentResponse();

		String TRANSACTION_NAME = Transactions.GET_FAST_PAYMENTS_TRANSACTION_NAME + " CONTROLLER";
		String requestTime = Utilities.getReportDateTime();

		try {
			String msisdn = Utilities.getValueFromJSON(Utilities.splitToken(Utilities.decodeString(token)).toString(),
					Constants.MSISDN_KEY);

			// Populating report object before processing business logic.
			fastPaymentResponse.setLogsReport(Utilities.preBusinessReportLoggingData(token, msisdn, TRANSACTION_NAME,
					servletRequest.getRemoteAddr(), userAgent, userType, tariffType, data, lang,
					fastPaymentResponse.getLogsReport()));

			String deviceID = servletRequest.getHeader("deviceID");

			data = Utilities.addAttributesToRequest(data, msisdn, servletRequest, lang, userAgent);

			Utilities.printInfoLog(msisdn + "-Request Landed in-" + TRANSACTION_NAME, logger);
			Utilities.printInfoLog(msisdn + "-Request Data-" + data, logger);

			fastPaymentRequest = mapper.readValue(data, FastPaymentRequest.class);

			String requestValidationStatus = Validator.validateRequest(msisdn, fastPaymentRequest);

			if (requestValidationStatus.equalsIgnoreCase(Constants.VALIDATION_SUCCESS_CODE)) {
				fastPaymentResponse = topUpBusiness.getFastPaymentBusiness(msisdn, deviceID, fastPaymentRequest,
						fastPaymentResponse);
			} else {
				fastPaymentResponse.setCallStatus(Constants.Call_Status_False);
				fastPaymentResponse.setResultCode(Constants.VALIDATION_FAILURE_CODE);
				fastPaymentResponse.setResultDesc(requestValidationStatus);
				
			}

			fastPaymentResponse.getLogsReport().setResponseCode(fastPaymentResponse.getResultCode());
			fastPaymentResponse.getLogsReport().setRequestTime(requestTime);
			fastPaymentResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
			fastPaymentResponse.getLogsReport().setResponse(mapper.writeValueAsString(fastPaymentResponse));

			// Sending report log into queue.
			Utilities.prepareLogReportForQueue(fastPaymentResponse.getLogsReport());

			Utilities.printInfoLog(msisdn + "-Response Returned from-" + TRANSACTION_NAME + "-"
					+ mapper.writeValueAsString(fastPaymentResponse), logger);
		} catch (Exception e) {
			Utilities.printErrorLog(Utilities.convertStackTraceToString(e), logger);
			fastPaymentResponse.setCallStatus(Constants.Call_Status_False);

			fastPaymentResponse.setResultCode(Constants.EXCEPTION_CODE);
			fastPaymentResponse
					.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("unexpected.error", lang));

			fastPaymentResponse.getLogsReport().setResponseCode(fastPaymentResponse.getResultCode());
			fastPaymentResponse.getLogsReport().setRequestTime(requestTime);
			fastPaymentResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
			fastPaymentResponse.getLogsReport().setResponse(mapper.writeValueAsString(fastPaymentResponse));

			// Sending report log into queue.
			Utilities.prepareLogReportForQueue(fastPaymentResponse.getLogsReport());
		}
		return fastPaymentResponse;

	}

	@RequestMapping(value = "/initiatepayment", method = RequestMethod.POST, headers = Constants.CONTENT_TYPE_DEFAULT_VALUE)
	public PaymentKeyResponse initiatePayment(@RequestBody String data,
			@RequestHeader(value = Constants.TOKEN_KEY, defaultValue = Constants.TOKEN_DEFAULT_VALUE) String token,
			@RequestHeader(value = Constants.LANGUAGE_KEY, defaultValue = Constants.LANGUAGE_DEFAULT_VALUE) String lang,
			@RequestHeader(value = Constants.USER_AGENT_KEY, defaultValue = Constants.USER_AGENT_DEFAULT_VALUE) String userAgent,
			@RequestHeader(value = Constants.SUBSCRIBER_TYPE_KEY, defaultValue = Constants.SUBSCRIBER_TYPE_DEFAULT_VALUE) String userType,
			@RequestHeader(value = Constants.TARIFF_TYPE_KEY, defaultValue = Constants.TARIFF_TYPE_DEFAULT_VALUE) String tariffType,
			HttpServletRequest servletRequest) throws JsonParseException, JsonMappingException, IOException,
			JMSException, ClassNotFoundException, SQLException {

		ObjectMapper mapper = new ObjectMapper();

		TopUpBusiness topUpBusiness = new TopUpBusiness();
		
		PaymentKeyRequest paymentKeyRequest = new PaymentKeyRequest();
		PaymentKeyResponse paymentKeyResponse = new PaymentKeyResponse();

		String TRANSACTION_NAME = Transactions.INITIATE_PAYMENT_TRANSACTION_NAME + " CONTROLLER";
		String requestTime = Utilities.getReportDateTime();

		try {
			String msisdn = Utilities.getValueFromJSON(Utilities.splitToken(Utilities.decodeString(token)).toString(),
					Constants.MSISDN_KEY);

			// Populating report object before processing business logic.
			paymentKeyResponse.setLogsReport(Utilities.preBusinessReportLoggingData(token, msisdn, TRANSACTION_NAME,
					servletRequest.getRemoteAddr(), userAgent, userType, tariffType, data, lang,
					paymentKeyResponse.getLogsReport()));

			String deviceID = servletRequest.getHeader("deviceID");

			data = Utilities.addAttributesToRequest(data, msisdn, servletRequest, lang, userAgent);

			Utilities.printInfoLog(msisdn + "-Request Landed in-" + TRANSACTION_NAME, logger);
			Utilities.printInfoLog(msisdn + "-Request Data-" + data, logger);

			paymentKeyRequest = mapper.readValue(data, PaymentKeyRequest.class);

			String requestValidationStatus = Validator.validateRequest(msisdn, paymentKeyRequest);

			if (requestValidationStatus.equalsIgnoreCase(Constants.VALIDATION_SUCCESS_CODE)) {
				paymentKeyResponse = topUpBusiness.getPaymentKey(msisdn, deviceID, paymentKeyRequest,
						paymentKeyResponse);
			} else {
				paymentKeyResponse.setCallStatus(Constants.Call_Status_False);
				paymentKeyResponse.setResultCode(Constants.VALIDATION_FAILURE_CODE);
				paymentKeyResponse.setResultDesc(requestValidationStatus);
			}

			paymentKeyResponse.getLogsReport().setResponseCode(paymentKeyResponse.getResultCode());
			paymentKeyResponse.getLogsReport().setRequestTime(requestTime);
			paymentKeyResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
			paymentKeyResponse.getLogsReport().setResponse(mapper.writeValueAsString(paymentKeyResponse));

			// Sending report log into queue.
			Utilities.prepareLogReportForQueue(paymentKeyResponse.getLogsReport());

			Utilities.printInfoLog(msisdn + "-Response Returned from-" + TRANSACTION_NAME + "-"
					+ mapper.writeValueAsString(paymentKeyResponse), logger);
		} catch (Exception e) {
			Utilities.printErrorLog(Utilities.convertStackTraceToString(e), logger);
			paymentKeyResponse.setCallStatus(Constants.Call_Status_False);

			paymentKeyResponse.setResultCode(Constants.EXCEPTION_CODE);
			paymentKeyResponse
					.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("unexpected.error", lang));

			paymentKeyResponse.getLogsReport().setResponseCode(paymentKeyResponse.getResultCode());
			paymentKeyResponse.getLogsReport().setRequestTime(requestTime);
			paymentKeyResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
			paymentKeyResponse.getLogsReport().setResponse(mapper.writeValueAsString(paymentKeyResponse));

			// Sending report log into queue.
			Utilities.prepareLogReportForQueue(paymentKeyResponse.getLogsReport());
		}
		return paymentKeyResponse;

	}

	@RequestMapping(value = "/verifycard", method = RequestMethod.POST, headers = Constants.CONTENT_TYPE_DEFAULT_VALUE)
	public VerifyCardResponse verifyCard(@RequestBody String data,
			@RequestHeader(value = Constants.TOKEN_KEY, defaultValue = Constants.TOKEN_DEFAULT_VALUE) String token,
			@RequestHeader(value = Constants.LANGUAGE_KEY, defaultValue = Constants.LANGUAGE_DEFAULT_VALUE) String lang,
			@RequestHeader(value = Constants.USER_AGENT_KEY, defaultValue = Constants.USER_AGENT_DEFAULT_VALUE) String userAgent,
			@RequestHeader(value = Constants.SUBSCRIBER_TYPE_KEY, defaultValue = Constants.SUBSCRIBER_TYPE_DEFAULT_VALUE) String userType,
			@RequestHeader(value = Constants.TARIFF_TYPE_KEY, defaultValue = Constants.TARIFF_TYPE_DEFAULT_VALUE) String tariffType,
			HttpServletRequest servletRequest) throws JsonParseException, JsonMappingException, IOException,
			JMSException, ClassNotFoundException, SQLException {

		ObjectMapper mapper = new ObjectMapper();

		TopUpBusiness topUpBusiness = new TopUpBusiness();
		
		VerifyCardRequest verifyCardRequest = new VerifyCardRequest();
		VerifyCardResponse verifyCardResponse = new VerifyCardResponse();

		String TRANSACTION_NAME = Transactions.VERIFY_CARD_TRANSACTION_NAME + " CONTROLLER";
		String requestTime = Utilities.getReportDateTime();

		try {
			String msisdn = Utilities.getValueFromJSON(Utilities.splitToken(Utilities.decodeString(token)).toString(),
					Constants.MSISDN_KEY);

			// Populating report object before processing business logic.
			verifyCardResponse.setLogsReport(Utilities.preBusinessReportLoggingData(token, msisdn, TRANSACTION_NAME,
					servletRequest.getRemoteAddr(), userAgent, userType, tariffType, data, lang,
					verifyCardResponse.getLogsReport()));

			String deviceID = servletRequest.getHeader("deviceID");

			data = Utilities.addAttributesToRequest(data, msisdn, servletRequest, lang, userAgent);

			Utilities.printInfoLog(msisdn + "-Request Landed in-" + TRANSACTION_NAME, logger);
			Utilities.printInfoLog(msisdn + "-Request Data-" + data, logger);

			verifyCardRequest = mapper.readValue(data, VerifyCardRequest.class);

			String requestValidationStatus = Validator.validateRequest(msisdn, verifyCardRequest);

			if (requestValidationStatus.equalsIgnoreCase(Constants.VALIDATION_SUCCESS_CODE)) {
				verifyCardResponse = topUpBusiness.verifyCard(msisdn, deviceID, verifyCardRequest, verifyCardResponse);
			} else {
				verifyCardResponse.setCallStatus(Constants.Call_Status_False);
				verifyCardResponse.setResultCode(Constants.VALIDATION_FAILURE_CODE);
				verifyCardResponse.setResultDesc(requestValidationStatus);
			}

			verifyCardResponse.getLogsReport().setResponseCode(verifyCardResponse.getResultCode());
			verifyCardResponse.getLogsReport().setRequestTime(requestTime);
			verifyCardResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
			verifyCardResponse.getLogsReport().setResponse(mapper.writeValueAsString(verifyCardResponse));

			// Sending report log into queue.
			Utilities.prepareLogReportForQueue(verifyCardResponse.getLogsReport());

			Utilities.printInfoLog(msisdn + "-Response Returned from-" + TRANSACTION_NAME + "-"
					+ mapper.writeValueAsString(verifyCardResponse), logger);
		} catch (Exception e) {
			Utilities.printErrorLog(Utilities.convertStackTraceToString(e), logger);
			verifyCardResponse.setCallStatus(Constants.Call_Status_False);

			verifyCardResponse.setResultCode(Constants.EXCEPTION_CODE);
			verifyCardResponse
					.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("unexpected.error", lang));

			verifyCardResponse.getLogsReport().setResponseCode(verifyCardResponse.getResultCode());
			verifyCardResponse.getLogsReport().setRequestTime(requestTime);
			verifyCardResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
			verifyCardResponse.getLogsReport().setResponse(mapper.writeValueAsString(verifyCardResponse));

			// Sending report log into queue.
			Utilities.prepareLogReportForQueue(verifyCardResponse.getLogsReport());
		}
		return verifyCardResponse;

	}
	
	@RequestMapping(value = "/makepayment", method = RequestMethod.POST, headers = Constants.CONTENT_TYPE_DEFAULT_VALUE)
	public MakePaymentResponse makePayment(@RequestBody String data,
			@RequestHeader(value = Constants.TOKEN_KEY, defaultValue = Constants.TOKEN_DEFAULT_VALUE) String token,
			@RequestHeader(value = Constants.LANGUAGE_KEY, defaultValue = Constants.LANGUAGE_DEFAULT_VALUE) String lang,
			@RequestHeader(value = Constants.USER_AGENT_KEY, defaultValue = Constants.USER_AGENT_DEFAULT_VALUE) String userAgent,
			@RequestHeader(value = Constants.SUBSCRIBER_TYPE_KEY, defaultValue = Constants.SUBSCRIBER_TYPE_DEFAULT_VALUE) String userType,
			@RequestHeader(value = Constants.TARIFF_TYPE_KEY, defaultValue = Constants.TARIFF_TYPE_DEFAULT_VALUE) String tariffType,
			HttpServletRequest servletRequest) throws JsonParseException, JsonMappingException, IOException,
			JMSException, ClassNotFoundException, SQLException {

		ObjectMapper mapper = new ObjectMapper();

		TopUpBusiness topUpBusiness = new TopUpBusiness();
		
		MakePaymentRequest makePaymentRequest = new MakePaymentRequest();
		MakePaymentResponse makePaymentResponse = new MakePaymentResponse();

		String TRANSACTION_NAME = Transactions.MAKE_PAYMENT_TRANSACTION_NAME + " CONTROLLER";
		String requestTime = Utilities.getReportDateTime();

		try {
			String msisdn = Utilities.getValueFromJSON(Utilities.splitToken(Utilities.decodeString(token)).toString(),
					Constants.MSISDN_KEY);

			// Populating report object before processing business logic.
			makePaymentResponse.setLogsReport(Utilities.preBusinessReportLoggingData(token, msisdn, TRANSACTION_NAME,
					servletRequest.getRemoteAddr(), userAgent, userType, tariffType, data, lang,
					makePaymentResponse.getLogsReport()));

			String deviceID = servletRequest.getHeader("deviceID");

			data = Utilities.addAttributesToRequest(data, msisdn, servletRequest, lang, userAgent);

			Utilities.printInfoLog(msisdn + "-Request Landed in-" + TRANSACTION_NAME, logger);
			Utilities.printInfoLog(msisdn + "-Request Data-" + data, logger);

			makePaymentRequest = mapper.readValue(data, MakePaymentRequest.class);

			String requestValidationStatus = Validator.validateRequest(msisdn, makePaymentRequest);

			if (requestValidationStatus.equalsIgnoreCase(Constants.VALIDATION_SUCCESS_CODE)) {
				makePaymentResponse = topUpBusiness.makePaymentBusiness(msisdn, deviceID, makePaymentRequest,
						makePaymentResponse);
			} else {
				makePaymentResponse.setCallStatus(Constants.Call_Status_False);
				makePaymentResponse.setResultCode(Constants.VALIDATION_FAILURE_CODE);
				makePaymentResponse.setResultDesc(requestValidationStatus);
			}

			makePaymentResponse.getLogsReport().setResponseCode(makePaymentResponse.getResultCode());
			makePaymentResponse.getLogsReport().setRequestTime(requestTime);
			makePaymentResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
			makePaymentResponse.getLogsReport().setResponse(mapper.writeValueAsString(makePaymentResponse));

			// Sending report log into queue.
			Utilities.prepareLogReportForQueue(makePaymentResponse.getLogsReport());

			Utilities.printInfoLog(msisdn + "-Response Returned from-" + TRANSACTION_NAME + "-"
					+ mapper.writeValueAsString(makePaymentResponse), logger);
		} catch (Exception e) {
			Utilities.printErrorLog(Utilities.convertStackTraceToString(e), logger);
			makePaymentResponse.setCallStatus(Constants.Call_Status_False);

			makePaymentResponse.setResultCode(Constants.EXCEPTION_CODE);
			makePaymentResponse
					.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("unexpected.error", lang));

			makePaymentResponse.getLogsReport().setResponseCode(makePaymentResponse.getResultCode());
			makePaymentResponse.getLogsReport().setRequestTime(requestTime);
			makePaymentResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
			makePaymentResponse.getLogsReport().setResponse(mapper.writeValueAsString(makePaymentResponse));

			// Sending report log into queue.
			Utilities.prepareLogReportForQueue(makePaymentResponse.getLogsReport());
		}
		return makePaymentResponse;

	}
	
	@RequestMapping(value = "/deletepayment", method = RequestMethod.POST, headers = Constants.CONTENT_TYPE_DEFAULT_VALUE)
	public DeleteFastPaymentResponse deleteFastPayment(@RequestBody String data,
			@RequestHeader(value = Constants.TOKEN_KEY, defaultValue = Constants.TOKEN_DEFAULT_VALUE) String token,
			@RequestHeader(value = Constants.LANGUAGE_KEY, defaultValue = Constants.LANGUAGE_DEFAULT_VALUE) String lang,
			@RequestHeader(value = Constants.USER_AGENT_KEY, defaultValue = Constants.USER_AGENT_DEFAULT_VALUE) String userAgent,
			@RequestHeader(value = Constants.SUBSCRIBER_TYPE_KEY, defaultValue = Constants.SUBSCRIBER_TYPE_DEFAULT_VALUE) String userType,
			@RequestHeader(value = Constants.TARIFF_TYPE_KEY, defaultValue = Constants.TARIFF_TYPE_DEFAULT_VALUE) String tariffType,
			HttpServletRequest servletRequest) throws JsonParseException, JsonMappingException, IOException,
			JMSException, ClassNotFoundException, SQLException {

		ObjectMapper mapper = new ObjectMapper();

		TopUpBusiness topUpBusiness = new TopUpBusiness();
		
		DeleteFastPaymentRequest deletePaymentRequest = new DeleteFastPaymentRequest();
		DeleteFastPaymentResponse deletePaymentResponse = new DeleteFastPaymentResponse();

		String TRANSACTION_NAME = Transactions.DELETE_PAYMENT_TRANSACTION_NAME + " CONTROLLER";
		String requestTime = Utilities.getReportDateTime();

		try {
			String msisdn = Utilities.getValueFromJSON(Utilities.splitToken(Utilities.decodeString(token)).toString(),
					Constants.MSISDN_KEY);

			// Populating report object before processing business logic.
			deletePaymentResponse.setLogsReport(Utilities.preBusinessReportLoggingData(token, msisdn, TRANSACTION_NAME,
					servletRequest.getRemoteAddr(), userAgent, userType, tariffType, data, lang,
					deletePaymentResponse.getLogsReport()));

			String deviceID = servletRequest.getHeader("deviceID");

			data = Utilities.addAttributesToRequest(data, msisdn, servletRequest, lang, userAgent);

			Utilities.printInfoLog(msisdn + "-Request Landed in-" + TRANSACTION_NAME, logger);
			Utilities.printInfoLog(msisdn + "-Request Data-" + data, logger);

			deletePaymentRequest = mapper.readValue(data, DeleteFastPaymentRequest.class);

			String requestValidationStatus = Validator.validateRequest(msisdn, deletePaymentRequest);

			if (requestValidationStatus.equalsIgnoreCase(Constants.VALIDATION_SUCCESS_CODE)) {
				deletePaymentResponse = topUpBusiness.deleteFastPaymentBusiness(msisdn, deviceID, deletePaymentRequest,
						deletePaymentResponse);
			} else {
				deletePaymentResponse.setCallStatus(Constants.Call_Status_False);
				deletePaymentResponse.setResultCode(Constants.VALIDATION_FAILURE_CODE);
				deletePaymentResponse.setResultDesc(requestValidationStatus);
			}

			deletePaymentResponse.getLogsReport().setResponseCode(deletePaymentResponse.getResultCode());
			deletePaymentResponse.getLogsReport().setRequestTime(requestTime);
			deletePaymentResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
			deletePaymentResponse.getLogsReport().setResponse(mapper.writeValueAsString(deletePaymentResponse));

			// Sending report log into queue.
			Utilities.prepareLogReportForQueue(deletePaymentResponse.getLogsReport());

			Utilities.printInfoLog(msisdn + "-Response Returned from-" + TRANSACTION_NAME + "-"
					+ mapper.writeValueAsString(deletePaymentResponse), logger);
		} catch (Exception e) {
			Utilities.printErrorLog(Utilities.convertStackTraceToString(e), logger);
			deletePaymentResponse.setCallStatus(Constants.Call_Status_False);

			deletePaymentResponse.setResultCode(Constants.EXCEPTION_CODE);
			deletePaymentResponse
					.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("unexpected.error", lang));

			deletePaymentResponse.getLogsReport().setResponseCode(deletePaymentResponse.getResultCode());
			deletePaymentResponse.getLogsReport().setRequestTime(requestTime);
			deletePaymentResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
			deletePaymentResponse.getLogsReport().setResponse(mapper.writeValueAsString(deletePaymentResponse));

			// Sending report log into queue.
			Utilities.prepareLogReportForQueue(deletePaymentResponse.getLogsReport());
		}
		return deletePaymentResponse;

	}
	
	@RequestMapping(value = "/deletesavedcard", method = RequestMethod.POST, headers = Constants.CONTENT_TYPE_DEFAULT_VALUE)
	public DeleteSavedCardResponse deleteSavedCard(@RequestBody String data,
			@RequestHeader(value = Constants.TOKEN_KEY, defaultValue = Constants.TOKEN_DEFAULT_VALUE) String token,
			@RequestHeader(value = Constants.LANGUAGE_KEY, defaultValue = Constants.LANGUAGE_DEFAULT_VALUE) String lang,
			@RequestHeader(value = Constants.USER_AGENT_KEY, defaultValue = Constants.USER_AGENT_DEFAULT_VALUE) String userAgent,
			@RequestHeader(value = Constants.SUBSCRIBER_TYPE_KEY, defaultValue = Constants.SUBSCRIBER_TYPE_DEFAULT_VALUE) String userType,
			@RequestHeader(value = Constants.TARIFF_TYPE_KEY, defaultValue = Constants.TARIFF_TYPE_DEFAULT_VALUE) String tariffType,
			HttpServletRequest servletRequest) throws JsonParseException, JsonMappingException, IOException,
			JMSException, ClassNotFoundException, SQLException {

		ObjectMapper mapper = new ObjectMapper();

		TopUpBusiness topUpBusiness = new TopUpBusiness();
		
		DeleteSavedCardRequest deleteSavedCardRequest = new DeleteSavedCardRequest();
		DeleteSavedCardResponse deleteSavedCardResponse = new DeleteSavedCardResponse();

		String TRANSACTION_NAME = Transactions.DELETE_SAVED_CARD_TRANSACTION_NAME + " CONTROLLER";
		String requestTime = Utilities.getReportDateTime();

		try {
			String msisdn = Utilities.getValueFromJSON(Utilities.splitToken(Utilities.decodeString(token)).toString(),
					Constants.MSISDN_KEY);

			// Populating report object before processing business logic.
			deleteSavedCardResponse.setLogsReport(Utilities.preBusinessReportLoggingData(token, msisdn, TRANSACTION_NAME,
					servletRequest.getRemoteAddr(), userAgent, userType, tariffType, data, lang,
					deleteSavedCardResponse.getLogsReport()));

			String deviceID = servletRequest.getHeader("deviceID");

			data = Utilities.addAttributesToRequest(data, msisdn, servletRequest, lang, userAgent);

			Utilities.printInfoLog(msisdn + "-Request Landed in-" + TRANSACTION_NAME, logger);
			Utilities.printInfoLog(msisdn + "-Request Data-" + data, logger);

			deleteSavedCardRequest = mapper.readValue(data, DeleteSavedCardRequest.class);

			String requestValidationStatus = Validator.validateRequest(msisdn, deleteSavedCardRequest);

			if (requestValidationStatus.equalsIgnoreCase(Constants.VALIDATION_SUCCESS_CODE)) {
				deleteSavedCardResponse = topUpBusiness.deleteSavedCardBusiness(msisdn, deviceID, deleteSavedCardRequest,
						deleteSavedCardResponse);
			} else {
				deleteSavedCardResponse.setCallStatus(Constants.Call_Status_False);
				deleteSavedCardResponse.setResultCode(Constants.VALIDATION_FAILURE_CODE);
				deleteSavedCardResponse.setResultDesc(requestValidationStatus);
			}

			deleteSavedCardResponse.getLogsReport().setResponseCode(deleteSavedCardResponse.getResultCode());
			deleteSavedCardResponse.getLogsReport().setRequestTime(requestTime);
			deleteSavedCardResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
			deleteSavedCardResponse.getLogsReport().setResponse(mapper.writeValueAsString(deleteSavedCardResponse));

			// Sending report log into queue.
			Utilities.prepareLogReportForQueue(deleteSavedCardResponse.getLogsReport());

			Utilities.printInfoLog(msisdn + "-Response Returned from-" + TRANSACTION_NAME + "-"
					+ mapper.writeValueAsString(deleteSavedCardResponse), logger);
		} catch (Exception e) {
			Utilities.printErrorLog(Utilities.convertStackTraceToString(e), logger);
			deleteSavedCardResponse.setCallStatus(Constants.Call_Status_False);

			deleteSavedCardResponse.setResultCode(Constants.EXCEPTION_CODE);
			deleteSavedCardResponse
					.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("unexpected.error", lang));

			deleteSavedCardResponse.getLogsReport().setResponseCode(deleteSavedCardResponse.getResultCode());
			deleteSavedCardResponse.getLogsReport().setRequestTime(requestTime);
			deleteSavedCardResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
			deleteSavedCardResponse.getLogsReport().setResponse(mapper.writeValueAsString(deleteSavedCardResponse));

			// Sending report log into queue.
			Utilities.prepareLogReportForQueue(deleteSavedCardResponse.getLogsReport());
		}
		return deleteSavedCardResponse;

	}
	

	@RequestMapping(value = "/addpaymentscheduler", method = RequestMethod.POST, headers = Constants.CONTENT_TYPE_DEFAULT_VALUE)
	public AddPaymentSchedulerResponse addPaymentScheduler(@RequestBody String data,
			@RequestHeader(value = Constants.TOKEN_KEY, defaultValue = Constants.TOKEN_DEFAULT_VALUE) String token,
			@RequestHeader(value = Constants.LANGUAGE_KEY, defaultValue = Constants.LANGUAGE_DEFAULT_VALUE) String lang,
			@RequestHeader(value = Constants.USER_AGENT_KEY, defaultValue = Constants.USER_AGENT_DEFAULT_VALUE) String userAgent,
			@RequestHeader(value = Constants.SUBSCRIBER_TYPE_KEY, defaultValue = Constants.SUBSCRIBER_TYPE_DEFAULT_VALUE) String userType,
			@RequestHeader(value = Constants.TARIFF_TYPE_KEY, defaultValue = Constants.TARIFF_TYPE_DEFAULT_VALUE) String tariffType,
			HttpServletRequest servletRequest) throws JsonParseException, JsonMappingException, IOException,
			JMSException, ClassNotFoundException, SQLException {

		ObjectMapper mapper = new ObjectMapper();

		TopUpBusiness topUpBusiness = new TopUpBusiness();
		
		AddPaymentSchedulerRequest addPaymentSchedulerRequest = new AddPaymentSchedulerRequest();
		AddPaymentSchedulerResponse addPaymentSchedulerResponse = new AddPaymentSchedulerResponse();

		String TRANSACTION_NAME = Transactions.ADD_PAYMENT_SCHEDULER_TRANSACTION_NAME + " CONTROLLER";
		String requestTime = Utilities.getReportDateTime();

		try {
			String msisdn = Utilities.getValueFromJSON(Utilities.splitToken(Utilities.decodeString(token)).toString(),
					Constants.MSISDN_KEY);

			// Populating report object before processing business logic.
			addPaymentSchedulerResponse.setLogsReport(Utilities.preBusinessReportLoggingData(token, msisdn, TRANSACTION_NAME,
					servletRequest.getRemoteAddr(), userAgent, userType, tariffType, data, lang,
					addPaymentSchedulerResponse.getLogsReport()));

			String deviceID = servletRequest.getHeader("deviceID");

			data = Utilities.addAttributesToRequest(data, msisdn, servletRequest, lang, userAgent);

			Utilities.printInfoLog(msisdn + "-Request Landed in-" + TRANSACTION_NAME, logger);
			Utilities.printInfoLog(msisdn + "-Request Data-" + data, logger);

			addPaymentSchedulerRequest = mapper.readValue(data, AddPaymentSchedulerRequest.class);

			String requestValidationStatus = Validator.validateRequest(msisdn, addPaymentSchedulerRequest);

			if (requestValidationStatus.equalsIgnoreCase(Constants.VALIDATION_SUCCESS_CODE)) {
				addPaymentSchedulerResponse = topUpBusiness.addPaymentSchedulerBusiness(msisdn, deviceID, addPaymentSchedulerRequest,
						addPaymentSchedulerResponse);
			} else {
				addPaymentSchedulerResponse.setCallStatus(Constants.Call_Status_False);
				addPaymentSchedulerResponse.setResultCode(Constants.VALIDATION_FAILURE_CODE);
				addPaymentSchedulerResponse.setResultDesc(requestValidationStatus);
			}

			addPaymentSchedulerResponse.getLogsReport().setResponseCode(addPaymentSchedulerResponse.getResultCode());
			addPaymentSchedulerResponse.getLogsReport().setRequestTime(requestTime);
			addPaymentSchedulerResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
			addPaymentSchedulerResponse.getLogsReport().setResponse(mapper.writeValueAsString(addPaymentSchedulerResponse));

			// Sending report log into queue.
			Utilities.prepareLogReportForQueue(addPaymentSchedulerResponse.getLogsReport());

			Utilities.printInfoLog(msisdn + "-Response Returned from-" + TRANSACTION_NAME + "-"
					+ mapper.writeValueAsString(addPaymentSchedulerResponse), logger);
		} catch (Exception e) {
			Utilities.printErrorLog(Utilities.convertStackTraceToString(e), logger);
			addPaymentSchedulerResponse.setCallStatus(Constants.Call_Status_False);

			addPaymentSchedulerResponse.setResultCode(Constants.EXCEPTION_CODE);
			addPaymentSchedulerResponse
					.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("unexpected.error", lang));

			addPaymentSchedulerResponse.getLogsReport().setResponseCode(addPaymentSchedulerResponse.getResultCode());
			addPaymentSchedulerResponse.getLogsReport().setRequestTime(requestTime);
			addPaymentSchedulerResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
			addPaymentSchedulerResponse.getLogsReport().setResponse(mapper.writeValueAsString(addPaymentSchedulerResponse));

			// Sending report log into queue.
			Utilities.prepareLogReportForQueue(addPaymentSchedulerResponse.getLogsReport());
		}
		return addPaymentSchedulerResponse;

	}
	
	@RequestMapping(value = "/deletepaymentscheduler", method = RequestMethod.POST, headers = Constants.CONTENT_TYPE_DEFAULT_VALUE)
	public DeletePaymentSchedulerResponse deletePaymentScheduler(@RequestBody String data,
			@RequestHeader(value = Constants.TOKEN_KEY, defaultValue = Constants.TOKEN_DEFAULT_VALUE) String token,
			@RequestHeader(value = Constants.LANGUAGE_KEY, defaultValue = Constants.LANGUAGE_DEFAULT_VALUE) String lang,
			@RequestHeader(value = Constants.USER_AGENT_KEY, defaultValue = Constants.USER_AGENT_DEFAULT_VALUE) String userAgent,
			@RequestHeader(value = Constants.SUBSCRIBER_TYPE_KEY, defaultValue = Constants.SUBSCRIBER_TYPE_DEFAULT_VALUE) String userType,
			@RequestHeader(value = Constants.TARIFF_TYPE_KEY, defaultValue = Constants.TARIFF_TYPE_DEFAULT_VALUE) String tariffType,
			HttpServletRequest servletRequest) throws JsonParseException, JsonMappingException, IOException,
			JMSException, ClassNotFoundException, SQLException {

		ObjectMapper mapper = new ObjectMapper();

		TopUpBusiness topUpBusiness = new TopUpBusiness();
		
		DeletePaymentSchedulerRequest addPaymentSchedulerRequest = new DeletePaymentSchedulerRequest();
		DeletePaymentSchedulerResponse addPaymentSchedulerResponse = new DeletePaymentSchedulerResponse();

		String TRANSACTION_NAME = Transactions.DELETE_PAYMENT_SCHEDULER_TRANSACTION_NAME + " CONTROLLER";
		String requestTime = Utilities.getReportDateTime();

		try {
			String msisdn = Utilities.getValueFromJSON(Utilities.splitToken(Utilities.decodeString(token)).toString(),
					Constants.MSISDN_KEY);

			// Populating report object before processing business logic.
			addPaymentSchedulerResponse.setLogsReport(Utilities.preBusinessReportLoggingData(token, msisdn, TRANSACTION_NAME,
					servletRequest.getRemoteAddr(), userAgent, userType, tariffType, data, lang,
					addPaymentSchedulerResponse.getLogsReport()));

			String deviceID = servletRequest.getHeader("deviceID");

			data = Utilities.addAttributesToRequest(data, msisdn, servletRequest, lang, userAgent);

			Utilities.printInfoLog(msisdn + "-Request Landed in-" + TRANSACTION_NAME, logger);
			Utilities.printInfoLog(msisdn + "-Request Data-" + data, logger);

			addPaymentSchedulerRequest = mapper.readValue(data, DeletePaymentSchedulerRequest.class);

			String requestValidationStatus = Validator.validateRequest(msisdn, addPaymentSchedulerRequest);

			if (requestValidationStatus.equalsIgnoreCase(Constants.VALIDATION_SUCCESS_CODE)) {
				addPaymentSchedulerResponse = topUpBusiness.deletePaymentSchedulerBusiness(msisdn, deviceID, addPaymentSchedulerRequest,
						addPaymentSchedulerResponse);
			} else {
				addPaymentSchedulerResponse.setCallStatus(Constants.Call_Status_False);
				addPaymentSchedulerResponse.setResultCode(Constants.VALIDATION_FAILURE_CODE);
				addPaymentSchedulerResponse.setResultDesc(requestValidationStatus);
			}

			addPaymentSchedulerResponse.getLogsReport().setResponseCode(addPaymentSchedulerResponse.getResultCode());
			addPaymentSchedulerResponse.getLogsReport().setRequestTime(requestTime);
			addPaymentSchedulerResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
			addPaymentSchedulerResponse.getLogsReport().setResponse(mapper.writeValueAsString(addPaymentSchedulerResponse));

			// Sending report log into queue.
			Utilities.prepareLogReportForQueue(addPaymentSchedulerResponse.getLogsReport());

			Utilities.printInfoLog(msisdn + "-Response Returned from-" + TRANSACTION_NAME + "-"
					+ mapper.writeValueAsString(addPaymentSchedulerResponse), logger);
		} catch (Exception e) {
			Utilities.printErrorLog(Utilities.convertStackTraceToString(e), logger);
			addPaymentSchedulerResponse.setCallStatus(Constants.Call_Status_False);

			addPaymentSchedulerResponse.setResultCode(Constants.EXCEPTION_CODE);
			addPaymentSchedulerResponse
					.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("unexpected.error", lang));

			addPaymentSchedulerResponse.getLogsReport().setResponseCode(addPaymentSchedulerResponse.getResultCode());
			addPaymentSchedulerResponse.getLogsReport().setRequestTime(requestTime);
			addPaymentSchedulerResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
			addPaymentSchedulerResponse.getLogsReport().setResponse(mapper.writeValueAsString(addPaymentSchedulerResponse));

			// Sending report log into queue.
			Utilities.prepareLogReportForQueue(addPaymentSchedulerResponse.getLogsReport());
		}
		return addPaymentSchedulerResponse;

	}
	
	@RequestMapping(value = "/getscheduledpayments", method = RequestMethod.POST, headers = Constants.CONTENT_TYPE_DEFAULT_VALUE)
	public ScheduledPaymentsResponse getScheduledPayments(
			@RequestHeader(value = Constants.TOKEN_KEY, defaultValue = Constants.TOKEN_DEFAULT_VALUE) String token,
			@RequestHeader(value = Constants.LANGUAGE_KEY, defaultValue = Constants.LANGUAGE_DEFAULT_VALUE) String lang,
			@RequestHeader(value = Constants.USER_AGENT_KEY, defaultValue = Constants.USER_AGENT_DEFAULT_VALUE) String userAgent,
			@RequestHeader(value = Constants.SUBSCRIBER_TYPE_KEY, defaultValue = Constants.SUBSCRIBER_TYPE_DEFAULT_VALUE) String userType,
			@RequestHeader(value = Constants.TARIFF_TYPE_KEY, defaultValue = Constants.TARIFF_TYPE_DEFAULT_VALUE) String tariffType,
			HttpServletRequest servletRequest) throws JsonParseException, JsonMappingException, IOException,
			JMSException, ClassNotFoundException, SQLException {

		String data = "{}";
		
		ObjectMapper mapper = new ObjectMapper();

		ScheduledPaymentsRequest addPaymentSchedulerRequest = new ScheduledPaymentsRequest();
		ScheduledPaymentsResponse addPaymentSchedulerResponse = new ScheduledPaymentsResponse();

		String TRANSACTION_NAME = Transactions.GET_SCHEDULED_PAYMENTS_TRANSACTION_NAME + " CONTROLLER";
		String requestTime = Utilities.getReportDateTime();

		TopUpBusiness topUpBusiness = new TopUpBusiness();
		
		try {
			String msisdn = Utilities.getValueFromJSON(Utilities.splitToken(Utilities.decodeString(token)).toString(),
					Constants.MSISDN_KEY);

			// Populating report object before processing business logic.
			addPaymentSchedulerResponse.setLogsReport(Utilities.preBusinessReportLoggingData(token, msisdn, TRANSACTION_NAME,
					servletRequest.getRemoteAddr(), userAgent, userType, tariffType, data, lang,
					addPaymentSchedulerResponse.getLogsReport()));

			String deviceID = servletRequest.getHeader("deviceID"); 

			data = Utilities.addAttributesToRequest(data, msisdn, servletRequest, lang, userAgent);

			Utilities.printInfoLog(msisdn + "-Request Landed in-" + TRANSACTION_NAME, logger);
			Utilities.printInfoLog(msisdn + "-Request Data-" + data, logger);

			addPaymentSchedulerRequest = mapper.readValue(data, ScheduledPaymentsRequest.class);

			String requestValidationStatus = Validator.validateRequest(msisdn, addPaymentSchedulerRequest);

			if (requestValidationStatus.equalsIgnoreCase(Constants.VALIDATION_SUCCESS_CODE)) {
				addPaymentSchedulerResponse = topUpBusiness.getScheduledPaymentsBusiness(msisdn, deviceID, addPaymentSchedulerRequest,
						addPaymentSchedulerResponse);
			} else {
				addPaymentSchedulerResponse.setCallStatus(Constants.Call_Status_False);
				addPaymentSchedulerResponse.setResultCode(Constants.VALIDATION_FAILURE_CODE);
				addPaymentSchedulerResponse.setResultDesc(requestValidationStatus);
			}

			addPaymentSchedulerResponse.getLogsReport().setResponseCode(addPaymentSchedulerResponse.getResultCode());
			addPaymentSchedulerResponse.getLogsReport().setRequestTime(requestTime);
			addPaymentSchedulerResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
			addPaymentSchedulerResponse.getLogsReport().setResponse(mapper.writeValueAsString(addPaymentSchedulerResponse));

			// Sending report log into queue.
			Utilities.prepareLogReportForQueue(addPaymentSchedulerResponse.getLogsReport());

			Utilities.printInfoLog(msisdn + "-Response Returned from-" + TRANSACTION_NAME + "-"
					+ mapper.writeValueAsString(addPaymentSchedulerResponse), logger);
		} catch (Exception e) {
			Utilities.printErrorLog(Utilities.convertStackTraceToString(e), logger);
			addPaymentSchedulerResponse.setCallStatus(Constants.Call_Status_False);

			addPaymentSchedulerResponse.setResultCode(Constants.EXCEPTION_CODE);
			addPaymentSchedulerResponse
					.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("unexpected.error", lang));

			addPaymentSchedulerResponse.getLogsReport().setResponseCode(addPaymentSchedulerResponse.getResultCode());
			addPaymentSchedulerResponse.getLogsReport().setRequestTime(requestTime);
			addPaymentSchedulerResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
			addPaymentSchedulerResponse.getLogsReport().setResponse(mapper.writeValueAsString(addPaymentSchedulerResponse));

			// Sending report log into queue.
			Utilities.prepareLogReportForQueue(addPaymentSchedulerResponse.getLogsReport());
		}
		return addPaymentSchedulerResponse;

	}
	
	

}
