/**
 * 
 */
package com.evampsaanga.azerfon.controllers;

import java.io.IOException;
import java.sql.SQLException;

import javax.jms.JMSException;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONObject;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.evampsaanga.azerfon.business.NotificationsBusiness;
import com.evampsaanga.azerfon.common.messagesmappings.GetMessagesMappings;
import com.evampsaanga.azerfon.common.utilities.Constants;
import com.evampsaanga.azerfon.common.utilities.Transactions;
import com.evampsaanga.azerfon.common.utilities.Utilities;
import com.evampsaanga.azerfon.common.utilities.Validator;
import com.evampsaanga.azerfon.models.notifications.addfcm.AddFCMRequest;
import com.evampsaanga.azerfon.models.notifications.addfcm.AddFCMResponse;
import com.evampsaanga.azerfon.models.notifications.getnotifications.GetNotificationsRequest;
import com.evampsaanga.azerfon.models.notifications.getnotifications.GetNotificationsResponse;
import com.evampsaanga.azerfon.models.notifications.getnotificationscount.GetNotificationsCountRequest;
import com.evampsaanga.azerfon.models.notifications.getnotificationscount.GetNotificationsCountResponse;

/**
 * @author Evamp & Saanga
 *
 */
@RestController
@RequestMapping("/notificationsV2")
public class NotificationControllerV2 {

	Logger logger = Logger.getLogger(NotificationControllerV2.class);

	@RequestMapping(value = "/getnotifications", method = RequestMethod.POST, headers = Constants.CONTENT_TYPE_DEFAULT_VALUE)
	public GetNotificationsResponse getNotifications(@RequestBody String data,
			@RequestHeader(value = Constants.TOKEN_KEY, defaultValue = Constants.TOKEN_DEFAULT_VALUE) String token,
			@RequestHeader(value = Constants.LANGUAGE_KEY, defaultValue = Constants.LANGUAGE_DEFAULT_VALUE) String lang,
			@RequestHeader(value = Constants.USER_AGENT_KEY, defaultValue = Constants.USER_AGENT_DEFAULT_VALUE) String userAgent,
			@RequestHeader(value = Constants.SUBSCRIBER_TYPE_KEY, defaultValue = Constants.SUBSCRIBER_TYPE_DEFAULT_VALUE) String userType,
			@RequestHeader(value = Constants.TARIFF_TYPE_KEY, defaultValue = Constants.TARIFF_TYPE_DEFAULT_VALUE) String tariffType,
			HttpServletRequest servletRequest) throws ClassNotFoundException, SQLException, JsonParseException,
			JsonMappingException, IOException, JMSException {
		ObjectMapper mapper = new ObjectMapper();
		JSONObject jsonObject = new JSONObject();
		NotificationsBusiness notificationsBusiness = new NotificationsBusiness();
		GetNotificationsRequest getNotificationsRequest = new GetNotificationsRequest();
		GetNotificationsResponse getNotificationsResponse = new GetNotificationsResponse();
		String requestTime = Utilities.getReportDateTime();

		String TRANSACTION_NAME = Transactions.NOTIFICATIONS_HISTORY_TRANSACTION_NAME + " CONTROLLER";
		try {
			data = jsonObject.toString();
			String msisdn = Utilities.getValueFromJSON(Utilities.splitToken(Utilities.decodeString(token)).toString(),
					Constants.MSISDN_KEY);

			// Populating report object before processing business logic.
			getNotificationsResponse.setLogsReport(Utilities.preBusinessReportLoggingData(token, msisdn,
					TRANSACTION_NAME, servletRequest.getRemoteAddr(), userAgent, userType, tariffType, data, lang,
					getNotificationsResponse.getLogsReport()));

			data = Utilities.addAttributesToRequest(data, msisdn, servletRequest, lang, userAgent);

			Utilities.printInfoLog(msisdn + "-Request Landed in-" + TRANSACTION_NAME, logger);
			Utilities.printInfoLog(msisdn + "-Request Data-" + data, logger);

			getNotificationsRequest = mapper.readValue(data, GetNotificationsRequest.class);

			String requestValidationStatus = Validator.validateRequest(msisdn, getNotificationsRequest);

			if (requestValidationStatus.equalsIgnoreCase(Constants.VALIDATION_SUCCESS_CODE)) {

				getNotificationsResponse = notificationsBusiness.getNotificationsBusiness(msisdn,
						getNotificationsRequest, getNotificationsResponse);

			} else {
				getNotificationsResponse.setCallStatus(Constants.Call_Status_False);
				getNotificationsResponse.setResultCode(Constants.VALIDATION_FAILURE_CODE);
				getNotificationsResponse.setResultDesc(requestValidationStatus);
			}

			getNotificationsResponse.getLogsReport().setResponseCode(getNotificationsResponse.getResultCode());
			getNotificationsResponse.getLogsReport().setRequestTime(requestTime);
			getNotificationsResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
			getNotificationsResponse.getLogsReport().setResponse(mapper.writeValueAsString(getNotificationsResponse));

			// Sending report log into queue.
			getNotificationsResponse.getLogsReport().setAppVersion(Constants.APP_VERSION_PHASE2);
			Utilities.prepareLogReportForQueue(getNotificationsResponse.getLogsReport());

			Utilities.printInfoLog(msisdn + "-Response Returned from-" + TRANSACTION_NAME + "-"
					+ mapper.writeValueAsString(getNotificationsResponse), logger);
		} catch (Exception e) {
			Utilities.printErrorLog(Utilities.convertStackTraceToString(e), logger);
			getNotificationsResponse.setCallStatus(Constants.Call_Status_False);
			getNotificationsResponse.setResultCode(Constants.EXCEPTION_CODE);
			getNotificationsResponse
					.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("unexpected.error", lang));

			getNotificationsResponse.getLogsReport().setResponseCode(getNotificationsResponse.getResultCode());
			getNotificationsResponse.getLogsReport().setRequestTime(requestTime);
			getNotificationsResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
			getNotificationsResponse.getLogsReport().setResponse(mapper.writeValueAsString(getNotificationsResponse));

			// Sending report log into queue.
			getNotificationsResponse.getLogsReport().setAppVersion(Constants.APP_VERSION_PHASE2);
			Utilities.prepareLogReportForQueue(getNotificationsResponse.getLogsReport());

		}
		return getNotificationsResponse;
	}

	@RequestMapping(value = "/getnotificationscount", method = RequestMethod.POST, headers = Constants.CONTENT_TYPE_DEFAULT_VALUE)
	public GetNotificationsCountResponse getNotificationCount(@RequestBody String data,
			@RequestHeader(value = Constants.TOKEN_KEY, defaultValue = Constants.TOKEN_DEFAULT_VALUE) String token,
			@RequestHeader(value = Constants.LANGUAGE_KEY, defaultValue = Constants.LANGUAGE_DEFAULT_VALUE) String lang,
			@RequestHeader(value = Constants.USER_AGENT_KEY, defaultValue = Constants.USER_AGENT_DEFAULT_VALUE) String userAgent,
			@RequestHeader(value = Constants.SUBSCRIBER_TYPE_KEY, defaultValue = Constants.SUBSCRIBER_TYPE_DEFAULT_VALUE) String userType,
			@RequestHeader(value = Constants.TARIFF_TYPE_KEY, defaultValue = Constants.TARIFF_TYPE_DEFAULT_VALUE) String tariffType,
			HttpServletRequest servletRequest) throws ClassNotFoundException, SQLException, JsonParseException,
			JsonMappingException, IOException, JMSException {
		ObjectMapper mapper = new ObjectMapper();
		JSONObject jsonObject = new JSONObject();
		NotificationsBusiness notificationsBusiness = new NotificationsBusiness();
		GetNotificationsCountRequest getNotificationsCountRequest = new GetNotificationsCountRequest();
		GetNotificationsCountResponse getNotificationsCountResponse = new GetNotificationsCountResponse();
		String requestTime = Utilities.getReportDateTime();

		String TRANSACTION_NAME = Transactions.NOTIFICATIONS_COUNT_TRANSACTION_NAME + " CONTROLLER";
		try {
			data = jsonObject.toString();
			String msisdn = Utilities.getValueFromJSON(Utilities.splitToken(Utilities.decodeString(token)).toString(),
					Constants.MSISDN_KEY);

			// Populating report object before processing business logic.
			getNotificationsCountResponse.setLogsReport(Utilities.preBusinessReportLoggingData(token, msisdn,
					TRANSACTION_NAME, servletRequest.getRemoteAddr(), userAgent, userType, tariffType, data, lang,
					getNotificationsCountResponse.getLogsReport()));

			data = Utilities.addAttributesToRequest(data, msisdn, servletRequest, lang, userAgent);

			Utilities.printInfoLog(msisdn + "-Request Landed in-" + TRANSACTION_NAME, logger);
			Utilities.printInfoLog(msisdn + "-Request Data-" + data, logger);

			getNotificationsCountRequest = mapper.readValue(data, GetNotificationsCountRequest.class);

			String requestValidationStatus = Validator.validateRequest(msisdn, getNotificationsCountRequest);

			if (requestValidationStatus.equalsIgnoreCase(Constants.VALIDATION_SUCCESS_CODE)) {

				getNotificationsCountResponse = notificationsBusiness.getNotificationsCountBusiness(msisdn,
						getNotificationsCountRequest, getNotificationsCountResponse);

			} else {
				getNotificationsCountResponse.setCallStatus(Constants.Call_Status_False);
				getNotificationsCountResponse.setResultCode(Constants.VALIDATION_FAILURE_CODE);
				getNotificationsCountResponse.setResultDesc(requestValidationStatus);
			}

			getNotificationsCountResponse.getLogsReport()
					.setResponseCode(getNotificationsCountResponse.getResultCode());
			getNotificationsCountResponse.getLogsReport().setRequestTime(requestTime);
			getNotificationsCountResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
			getNotificationsCountResponse.getLogsReport()
					.setResponse(mapper.writeValueAsString(getNotificationsCountResponse));

			// Sending report log into queue.
			getNotificationsCountResponse.getLogsReport().setAppVersion(Constants.APP_VERSION_PHASE2);
			Utilities.prepareLogReportForQueue(getNotificationsCountResponse.getLogsReport());

			Utilities.printInfoLog(msisdn + "-Response Returned from-" + TRANSACTION_NAME + "-"
					+ mapper.writeValueAsString(getNotificationsCountResponse), logger);
		} catch (Exception e) {
			Utilities.printErrorLog(Utilities.convertStackTraceToString(e), logger);
			getNotificationsCountResponse.setCallStatus(Constants.Call_Status_False);
			getNotificationsCountResponse.setResultCode(Constants.EXCEPTION_CODE);
			getNotificationsCountResponse
					.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("unexpected.error", lang));

			getNotificationsCountResponse.getLogsReport()
					.setResponseCode(getNotificationsCountResponse.getResultCode());
			getNotificationsCountResponse.getLogsReport().setRequestTime(requestTime);
			getNotificationsCountResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
			getNotificationsCountResponse.getLogsReport()
					.setResponse(mapper.writeValueAsString(getNotificationsCountResponse));

			// Sending report log into queue.
			getNotificationsCountResponse.getLogsReport().setAppVersion(Constants.APP_VERSION_PHASE2);
			Utilities.prepareLogReportForQueue(getNotificationsCountResponse.getLogsReport());

		}
		return getNotificationsCountResponse;
	}

	@RequestMapping(value = "/addfcm", method = RequestMethod.POST, headers = Constants.CONTENT_TYPE_DEFAULT_VALUE)
	public AddFCMResponse addFCM(@RequestBody String data,
			@RequestHeader(value = Constants.TOKEN_KEY, defaultValue = Constants.TOKEN_DEFAULT_VALUE) String token,
			@RequestHeader(value = Constants.LANGUAGE_KEY, defaultValue = Constants.LANGUAGE_DEFAULT_VALUE) String lang,
			@RequestHeader(value = Constants.USER_AGENT_KEY, defaultValue = Constants.USER_AGENT_DEFAULT_VALUE) String userAgent,
			@RequestHeader(value = Constants.SUBSCRIBER_TYPE_KEY, defaultValue = Constants.SUBSCRIBER_TYPE_DEFAULT_VALUE) String userType,
			@RequestHeader(value = Constants.TARIFF_TYPE_KEY, defaultValue = Constants.TARIFF_TYPE_DEFAULT_VALUE) String tariffType,
			HttpServletRequest servletRequest) throws ClassNotFoundException, SQLException, IOException, JMSException {

		ObjectMapper mapper = new ObjectMapper();
		NotificationsBusiness notificationsBusiness = new NotificationsBusiness();
		AddFCMRequest addFCMRequest = new AddFCMRequest();
		AddFCMResponse addFCMResponse = new AddFCMResponse();
		String requestTime = Utilities.getReportDateTime();
		String TRANSACTION_NAME = Transactions.ADD_FCM_KEY_TRANSACTION_NAME + " CONTROLLER";
		try {
			String msisdn = Utilities.getValueFromJSON(Utilities.splitToken(Utilities.decodeString(token)).toString(),
					Constants.MSISDN_KEY);
			String deviceID = servletRequest.getHeader("deviceID");

			// Populating report object before processing business logic.
			addFCMResponse.setLogsReport(Utilities.preBusinessReportLoggingData(token, msisdn, TRANSACTION_NAME,
					servletRequest.getRemoteAddr(), userAgent, userType, tariffType, data, lang,
					addFCMResponse.getLogsReport()));

			data = Utilities.addAttributesToRequest(data, msisdn, servletRequest, lang, userAgent);

			Utilities.printInfoLog(msisdn + "-Request Landed in-" + TRANSACTION_NAME, logger);
			Utilities.printInfoLog(msisdn + "-Request Data-" + data, logger);

			addFCMRequest = mapper.readValue(data, AddFCMRequest.class);
			addFCMRequest.setSubscriberType(userType);
			addFCMRequest.setTariffType(tariffType);
			
			Utilities.printInfoLog(msisdn + "Channel in  addFCMRequest" + addFCMRequest.getChannel(), logger);
			String requestValidationStatus = Validator.validateRequest(msisdn, addFCMRequest);

			if (requestValidationStatus.equalsIgnoreCase(Constants.VALIDATION_SUCCESS_CODE)) {

				addFCMResponse = notificationsBusiness.addFCMBusiness(msisdn, deviceID, addFCMRequest, addFCMResponse);

			} else {
				addFCMResponse.setCallStatus(Constants.Call_Status_False);
				addFCMResponse.setResultCode(Constants.VALIDATION_FAILURE_CODE);
				addFCMResponse.setResultDesc(requestValidationStatus);
			}

			addFCMResponse.getLogsReport().setResponseCode(addFCMResponse.getResultCode());
			addFCMResponse.getLogsReport().setRequestTime(requestTime);
			addFCMResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
			addFCMResponse.getLogsReport().setResponse(mapper.writeValueAsString(addFCMResponse));

			// Sending report log into queue.
			addFCMResponse.getLogsReport().setAppVersion(Constants.APP_VERSION_PHASE2);
			Utilities.prepareLogReportForQueue(addFCMResponse.getLogsReport());

			Utilities.printInfoLog(msisdn + "-Response Returned from-" + TRANSACTION_NAME + "-"
					+ mapper.writeValueAsString(addFCMResponse), logger);
		} catch (Exception e) {
			Utilities.printErrorLog(Utilities.convertStackTraceToString(e), logger);
			addFCMResponse.setCallStatus(Constants.Call_Status_False);
			addFCMResponse.setResultCode(Constants.EXCEPTION_CODE);
			addFCMResponse.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("unexpected.error", lang));

			addFCMResponse.getLogsReport().setResponseCode(addFCMResponse.getResultCode());
			addFCMResponse.getLogsReport().setRequestTime(requestTime);
			addFCMResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
			addFCMResponse.getLogsReport().setResponse(mapper.writeValueAsString(addFCMResponse));

			// Sending report log into queue.
			addFCMResponse.getLogsReport().setAppVersion(Constants.APP_VERSION_PHASE2);
			Utilities.prepareLogReportForQueue(addFCMResponse.getLogsReport());
		}

		return addFCMResponse;
	}

}
