/**
 * 
 */
package com.evampsaanga.azerfon.controllers;

import java.io.IOException;
import java.sql.SQLException;

import javax.jms.JMSException;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONObject;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.evampsaanga.azerfon.business.GeneralServicesBusiness;
import com.evampsaanga.azerfon.common.messagesmappings.GetMessagesMappings;
import com.evampsaanga.azerfon.common.utilities.Constants;
import com.evampsaanga.azerfon.common.utilities.GetConfigurations;
import com.evampsaanga.azerfon.common.utilities.Transactions;
import com.evampsaanga.azerfon.common.utilities.Utilities;
import com.evampsaanga.azerfon.common.utilities.Validator;
import com.evampsaanga.azerfon.models.generalservices.contactus.ContactUsRequest;
import com.evampsaanga.azerfon.models.generalservices.contactus.ContactUsResponse;
import com.evampsaanga.azerfon.models.generalservices.faqs.FAQSRequest;
import com.evampsaanga.azerfon.models.generalservices.faqs.FAQSResponse;
import com.evampsaanga.azerfon.models.generalservices.getroaming.GetRoamingRequest;
import com.evampsaanga.azerfon.models.generalservices.getroaming.GetRoamingResponse;
import com.evampsaanga.azerfon.models.generalservices.lostreport.ReportLostSIMRequest;
import com.evampsaanga.azerfon.models.generalservices.lostreport.ReportLostSIMResponse;
import com.evampsaanga.azerfon.models.generalservices.sendinternetsettings.SendInternetSettingsRequest;
import com.evampsaanga.azerfon.models.generalservices.sendinternetsettings.SendInternetSettingsResponse;
import com.evampsaanga.azerfon.models.generalservices.storelocator.StoreLocatorRequest;
import com.evampsaanga.azerfon.models.generalservices.storelocator.StoreLocatorResponse;
import com.evampsaanga.azerfon.models.generalservices.uploadimage.UploadImageRequest;
import com.evampsaanga.azerfon.models.generalservices.uploadimage.UploadImageResponse;
import com.evampsaanga.azerfon.models.verifyappversion.VerifyAppVersionRequest;
import com.evampsaanga.azerfon.models.verifyappversion.VerifyAppVersionResponse;
import com.evampsaanga.azerfon.models.generalservices.exchangeservice.*;

/**
 * @author Evamp & Saanga
 *
 */
@RestController
@RequestMapping("/generalservices")
public class GeneralServicesController {

	Logger logger = Logger.getLogger(GeneralServicesController.class);

	@RequestMapping(value = "/getfaqs", method = RequestMethod.POST, headers = Constants.CONTENT_TYPE_DEFAULT_VALUE)
	public FAQSResponse getFAQs(@RequestBody String data,
			@RequestHeader(value = Constants.TOKEN_KEY, defaultValue = Constants.TOKEN_DEFAULT_VALUE) String token,
			@RequestHeader(value = Constants.LANGUAGE_KEY, defaultValue = Constants.LANGUAGE_DEFAULT_VALUE) String lang,
			@RequestHeader(value = Constants.USER_AGENT_KEY, defaultValue = Constants.USER_AGENT_DEFAULT_VALUE) String userAgent,
			@RequestHeader(value = Constants.SUBSCRIBER_TYPE_KEY, defaultValue = Constants.SUBSCRIBER_TYPE_DEFAULT_VALUE) String userType,
			@RequestHeader(value = Constants.TARIFF_TYPE_KEY, defaultValue = Constants.TARIFF_TYPE_DEFAULT_VALUE) String tariffType,
			HttpServletRequest servletRequest) throws ClassNotFoundException, SQLException, JsonParseException,
			JsonMappingException, IOException, JMSException {
		ObjectMapper mapper = new ObjectMapper();
		JSONObject jsonObject = new JSONObject();
		GeneralServicesBusiness generalServicesBusiness = new GeneralServicesBusiness();
		FAQSRequest faqsRequest = new FAQSRequest();
		FAQSResponse faqsResponse = new FAQSResponse();

		String TRANSACTION_NAME = Transactions.APP_FAQ_TRANSACTION_NAME + " CONTROLLER";
		String requestTime = Utilities.getReportDateTime();
		try {
			data = jsonObject.toString(); // use it when there is no request
			// body required.

			String msisdn = Utilities.getValueFromJSON(Utilities.splitToken(Utilities.decodeString(token)).toString(),
					Constants.MSISDN_KEY);

			// Populating report object before processing business logic.
			faqsResponse.setLogsReport(Utilities.preBusinessReportLoggingData(token, msisdn, TRANSACTION_NAME,
					servletRequest.getRemoteAddr(), userAgent, userType, tariffType, data, lang,
					faqsResponse.getLogsReport()));

			data = Utilities.addAttributesToRequest(data, msisdn, servletRequest, lang, userAgent);

			Utilities.printInfoLog(msisdn + "-Request Landed in-" + TRANSACTION_NAME, logger);
			Utilities.printInfoLog(msisdn + "-Request Data-" + data, logger);

			faqsRequest = mapper.readValue(data, FAQSRequest.class);

			String requestValidationStatus = Validator.validateRequest(msisdn, faqsRequest);

			if (requestValidationStatus.equalsIgnoreCase(Constants.VALIDATION_SUCCESS_CODE)) {

				faqsResponse = generalServicesBusiness.faqsBusiness(msisdn, faqsRequest, faqsResponse);

			} else {
				faqsResponse.setCallStatus(Constants.Call_Status_False);
				faqsResponse.setResultCode(Constants.VALIDATION_FAILURE_CODE);
				faqsResponse.setResultDesc(requestValidationStatus);
			}

			faqsResponse.getLogsReport().setResponseCode(faqsResponse.getResultCode());
			faqsResponse.getLogsReport().setRequestTime(requestTime);
			faqsResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
			faqsResponse.getLogsReport().setResponse(mapper.writeValueAsString(faqsResponse));

			// Sending report log into queue.
			Utilities.prepareLogReportForQueue(faqsResponse.getLogsReport());

			Utilities.printInfoLog(msisdn + "-Response Returned from-" + TRANSACTION_NAME + "-"
					+ mapper.writeValueAsString(faqsResponse), logger);
		} catch (Exception e) {
			Utilities.printErrorLog(Utilities.convertStackTraceToString(e), logger);
			faqsResponse.setCallStatus(Constants.Call_Status_False);

			faqsResponse.setResultCode(Constants.EXCEPTION_CODE);
			faqsResponse.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("unexpected.error", lang));

			faqsResponse.getLogsReport().setResponseCode(faqsResponse.getResultCode());
			faqsResponse.getLogsReport().setRequestTime(requestTime);
			faqsResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
			faqsResponse.getLogsReport().setResponse(mapper.writeValueAsString(faqsResponse));

			// Sending report log into queue.
			Utilities.prepareLogReportForQueue(faqsResponse.getLogsReport());
		}
		return faqsResponse;
	}

	@RequestMapping(value = "/getcontactusdetails", method = RequestMethod.POST, headers = Constants.CONTENT_TYPE_DEFAULT_VALUE)
	public ContactUsResponse getContactUsDetails(@RequestBody String data,
			@RequestHeader(value = Constants.TOKEN_KEY, defaultValue = Constants.TOKEN_DEFAULT_VALUE) String token,
			@RequestHeader(value = Constants.LANGUAGE_KEY, defaultValue = Constants.LANGUAGE_DEFAULT_VALUE) String lang,
			@RequestHeader(value = Constants.USER_AGENT_KEY, defaultValue = Constants.USER_AGENT_DEFAULT_VALUE) String userAgent,
			@RequestHeader(value = Constants.SUBSCRIBER_TYPE_KEY, defaultValue = Constants.SUBSCRIBER_TYPE_DEFAULT_VALUE) String userType,
			@RequestHeader(value = Constants.TARIFF_TYPE_KEY, defaultValue = Constants.TARIFF_TYPE_DEFAULT_VALUE) String tariffType,
			HttpServletRequest servletRequest) throws ClassNotFoundException, SQLException, JsonParseException,
			JsonMappingException, IOException, JMSException {
		ObjectMapper mapper = new ObjectMapper();
		JSONObject jsonObject = new JSONObject();
		GeneralServicesBusiness generalServicesBusiness = new GeneralServicesBusiness();
		ContactUsRequest contactUsRequest = new ContactUsRequest();
		ContactUsResponse contactUsResponse = new ContactUsResponse();

		String TRANSACTION_NAME = Transactions.CONTACTUS__TRANSACTION_NAME + " CONTROLLER";
		String requestTime = Utilities.getReportDateTime();
		try {
			data = jsonObject.toString();
			String msisdn = Utilities.getValueFromJSON(Utilities.splitToken(Utilities.decodeString(token)).toString(),
					Constants.MSISDN_KEY);

			// Populating report object before processing business logic.
			contactUsResponse.setLogsReport(Utilities.preBusinessReportLoggingData(token, msisdn, TRANSACTION_NAME,
					servletRequest.getRemoteAddr(), userAgent, userType, tariffType, data, lang,
					contactUsResponse.getLogsReport()));

			data = Utilities.addAttributesToRequest(data, msisdn, servletRequest, lang, userAgent);

			Utilities.printInfoLog(msisdn + "-Request Landed in-" + TRANSACTION_NAME, logger);
			Utilities.printInfoLog(msisdn + "-Request Data-" + data, logger);

			contactUsRequest = mapper.readValue(data, ContactUsRequest.class);
			// Store ID value is same as of Language ID.
			contactUsRequest.setStoreId(contactUsRequest.getLang());

			String requestValidationStatus = Validator.validateRequest(msisdn, contactUsRequest);

			if (requestValidationStatus.equalsIgnoreCase(Constants.VALIDATION_SUCCESS_CODE)) {

				contactUsResponse = generalServicesBusiness.contactUsBusiness(msisdn, contactUsRequest,
						contactUsResponse);

			} else {
				contactUsResponse.setCallStatus(Constants.Call_Status_False);
				contactUsResponse.setResultCode(Constants.VALIDATION_FAILURE_CODE);
				contactUsResponse.setResultDesc(requestValidationStatus);
			}

			contactUsResponse.getLogsReport().setResponseCode(contactUsResponse.getResultCode());
			contactUsResponse.getLogsReport().setRequestTime(requestTime);
			contactUsResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
			contactUsResponse.getLogsReport().setResponse(mapper.writeValueAsString(contactUsResponse));

			// Sending report log into queue.
			Utilities.prepareLogReportForQueue(contactUsResponse.getLogsReport());

			Utilities.printInfoLog(msisdn + "-Response Returned from-" + TRANSACTION_NAME + "-"
					+ mapper.writeValueAsString(contactUsResponse), logger);
		} catch (Exception e) {
			Utilities.printErrorLog(Utilities.convertStackTraceToString(e), logger);
			contactUsResponse.setCallStatus(Constants.Call_Status_False);

			contactUsResponse.setResultCode(Constants.EXCEPTION_CODE);
			contactUsResponse.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("unexpected.error", lang));

			contactUsResponse.getLogsReport().setResponseCode(contactUsResponse.getResultCode());
			contactUsResponse.getLogsReport().setRequestTime(requestTime);
			contactUsResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
			contactUsResponse.getLogsReport().setResponse(mapper.writeValueAsString(contactUsResponse));

			// Sending report log into queue.
			Utilities.prepareLogReportForQueue(contactUsResponse.getLogsReport());

		}
		return contactUsResponse;
	}

	@RequestMapping(value = "/getstoresdetails", method = RequestMethod.POST, headers = Constants.CONTENT_TYPE_DEFAULT_VALUE)
	public StoreLocatorResponse getStoreLocatorDetails(@RequestBody String data,
			@RequestHeader(value = Constants.TOKEN_KEY, defaultValue = Constants.TOKEN_DEFAULT_VALUE) String token,
			@RequestHeader(value = Constants.LANGUAGE_KEY, defaultValue = Constants.LANGUAGE_DEFAULT_VALUE) String lang,
			@RequestHeader(value = Constants.USER_AGENT_KEY, defaultValue = Constants.USER_AGENT_DEFAULT_VALUE) String userAgent,
			@RequestHeader(value = Constants.SUBSCRIBER_TYPE_KEY, defaultValue = Constants.SUBSCRIBER_TYPE_DEFAULT_VALUE) String userType,
			@RequestHeader(value = Constants.TARIFF_TYPE_KEY, defaultValue = Constants.TARIFF_TYPE_DEFAULT_VALUE) String tariffType,
			HttpServletRequest servletRequest) throws ClassNotFoundException, SQLException, JsonParseException,
			JsonMappingException, IOException, JMSException {

		ObjectMapper mapper = new ObjectMapper();
		JSONObject jsonObject = new JSONObject();
		GeneralServicesBusiness generalServicesBusiness = new GeneralServicesBusiness();
		StoreLocatorRequest storeLocatorRequest = new StoreLocatorRequest();
		StoreLocatorResponse storeLocatorResponse = new StoreLocatorResponse();

		String TRANSACTION_NAME = Transactions.STORE_LOCATOR_TRANSACTION_NAME + " CONTROLLER";
		String requestTime = Utilities.getReportDateTime();
		try {
			data = jsonObject.toString(); // use it when there is no request
			// body required.

			String msisdn = Utilities.getValueFromJSON(Utilities.splitToken(Utilities.decodeString(token)).toString(),
					Constants.MSISDN_KEY);

			// Populating report object before processing business logic.
			storeLocatorResponse.setLogsReport(Utilities.preBusinessReportLoggingData(token, msisdn, TRANSACTION_NAME,
					servletRequest.getRemoteAddr(), userAgent, userType, tariffType, data, lang,
					storeLocatorResponse.getLogsReport()));

			data = Utilities.addAttributesToRequest(data, msisdn, servletRequest, lang, userAgent);

			Utilities.printInfoLog(msisdn + "-Request Landed in-" + TRANSACTION_NAME, logger);
			Utilities.printInfoLog(msisdn + "-Request Data-" + data, logger);

			storeLocatorRequest = mapper.readValue(data, StoreLocatorRequest.class);

			String requestValidationStatus = Validator.validateRequest(msisdn, storeLocatorRequest);

			if (requestValidationStatus.equalsIgnoreCase(Constants.VALIDATION_SUCCESS_CODE)) {

				storeLocatorResponse = generalServicesBusiness.getStoreLocatorsDetailsBusiness(msisdn,
						storeLocatorRequest, storeLocatorResponse);
			} else {
				storeLocatorResponse.setCallStatus(Constants.Call_Status_False);
				storeLocatorResponse.setResultCode(Constants.VALIDATION_FAILURE_CODE);
				storeLocatorResponse.setResultDesc(requestValidationStatus);
			}

			storeLocatorResponse.getLogsReport().setResponseCode(storeLocatorResponse.getResultCode());
			storeLocatorResponse.getLogsReport().setRequestTime(requestTime);
			storeLocatorResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
			storeLocatorResponse.getLogsReport().setResponse(mapper.writeValueAsString(storeLocatorResponse));

			// Sending report log into queue.
			Utilities.prepareLogReportForQueue(storeLocatorResponse.getLogsReport());
			Utilities.printInfoLog(msisdn + "-Response Returned from-" + TRANSACTION_NAME + "-"
					+ mapper.writeValueAsString(storeLocatorResponse), logger);
		} catch (Exception e) {
			Utilities.printErrorLog(Utilities.convertStackTraceToString(e), logger);
			storeLocatorResponse.setCallStatus(Constants.Call_Status_False);

			storeLocatorResponse.setResultCode(Constants.EXCEPTION_CODE);
			storeLocatorResponse
					.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("unexpected.error", lang));

			storeLocatorResponse.getLogsReport().setResponseCode(storeLocatorResponse.getResultCode());
			storeLocatorResponse.getLogsReport().setRequestTime(requestTime);
			storeLocatorResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
			storeLocatorResponse.getLogsReport().setResponse(mapper.writeValueAsString(storeLocatorResponse));

			// Sending report log into queue.
			Utilities.prepareLogReportForQueue(storeLocatorResponse.getLogsReport());
		}

		return storeLocatorResponse;
	}

	@RequestMapping(value = "/reportlostsim", method = RequestMethod.POST, headers = Constants.CONTENT_TYPE_DEFAULT_VALUE)
	public ReportLostSIMResponse reportlostsim(@RequestBody String data,
			@RequestHeader(value = Constants.TOKEN_KEY, defaultValue = Constants.TOKEN_DEFAULT_VALUE) String token,
			@RequestHeader(value = Constants.LANGUAGE_KEY, defaultValue = Constants.LANGUAGE_DEFAULT_VALUE) String lang,
			@RequestHeader(value = Constants.USER_AGENT_KEY, defaultValue = Constants.USER_AGENT_DEFAULT_VALUE) String userAgent,
			@RequestHeader(value = Constants.SUBSCRIBER_TYPE_KEY, defaultValue = Constants.SUBSCRIBER_TYPE_DEFAULT_VALUE) String userType,
			@RequestHeader(value = Constants.TARIFF_TYPE_KEY, defaultValue = Constants.TARIFF_TYPE_DEFAULT_VALUE) String tariffType,
			HttpServletRequest servletRequest) throws ClassNotFoundException, SQLException, JsonParseException,
			JsonMappingException, IOException, JMSException {
		ObjectMapper mapper = new ObjectMapper();

		GeneralServicesBusiness generalServicesBusiness = new GeneralServicesBusiness();
		ReportLostSIMRequest reportLostSIMRequest = new ReportLostSIMRequest();
		ReportLostSIMResponse reportLostSIMResponse = new ReportLostSIMResponse();

		String TRANSACTION_NAME = Transactions.REPORT_LOST_SIM_TRANSACTION_NAME + " CONTROLLER";
		String requestTime = Utilities.getReportDateTime();
		try {

			String msisdn = Utilities.getValueFromJSON(Utilities.splitToken(Utilities.decodeString(token)).toString(),
					Constants.MSISDN_KEY);

			// Populating report object before processing business logic.
			reportLostSIMResponse.setLogsReport(Utilities.preBusinessReportLoggingData(token, msisdn, TRANSACTION_NAME,
					servletRequest.getRemoteAddr(), userAgent, userType, tariffType, data, lang,
					reportLostSIMResponse.getLogsReport()));

			data = Utilities.addAttributesToRequest(data, msisdn, servletRequest, lang, userAgent);

			Utilities.printInfoLog(msisdn + "-Request Landed in-" + TRANSACTION_NAME, logger);
			Utilities.printInfoLog(msisdn + "-Request Data-" + data, logger);

			reportLostSIMRequest = mapper.readValue(data, ReportLostSIMRequest.class);

			String requestValidationStatus = Validator.validateRequest(msisdn, reportLostSIMRequest);

			if (requestValidationStatus.equalsIgnoreCase(Constants.VALIDATION_SUCCESS_CODE)) {

				reportLostSIMResponse = generalServicesBusiness.reportLostSIMBusiness(msisdn, reportLostSIMRequest,
						reportLostSIMResponse);
			} else {
				reportLostSIMResponse.setCallStatus(Constants.Call_Status_False);
				reportLostSIMResponse.setResultCode(Constants.VALIDATION_FAILURE_CODE);
				reportLostSIMResponse.setResultDesc(requestValidationStatus);
			}

			reportLostSIMResponse.getLogsReport().setResponseCode(reportLostSIMResponse.getResultCode());
			reportLostSIMResponse.getLogsReport().setRequestTime(requestTime);
			reportLostSIMResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
			reportLostSIMResponse.getLogsReport().setResponse(mapper.writeValueAsString(reportLostSIMResponse));

			// Sending report log into queue.
			Utilities.prepareLogReportForQueue(reportLostSIMResponse.getLogsReport());

			Utilities.printInfoLog(msisdn + "-Response Returned from-" + TRANSACTION_NAME + "-"
					+ mapper.writeValueAsString(reportLostSIMResponse), logger);
		} catch (Exception e) {
			Utilities.printErrorLog(Utilities.convertStackTraceToString(e), logger);
			reportLostSIMResponse.setCallStatus(Constants.Call_Status_False);

			reportLostSIMResponse.setResultCode(Constants.EXCEPTION_CODE);
			reportLostSIMResponse
					.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("unexpected.error", lang));

			reportLostSIMResponse.getLogsReport().setResponseCode(reportLostSIMResponse.getResultCode());
			reportLostSIMResponse.getLogsReport().setRequestTime(requestTime);
			reportLostSIMResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
			reportLostSIMResponse.getLogsReport().setResponse(mapper.writeValueAsString(reportLostSIMResponse));

			// Sending report log into queue.
			Utilities.prepareLogReportForQueue(reportLostSIMResponse.getLogsReport());
		}
		return reportLostSIMResponse;
	}

	@RequestMapping(value = "/uploadimage", method = RequestMethod.POST, headers = Constants.CONTENT_TYPE_DEFAULT_VALUE)
	public UploadImageResponse uploadProfilePicture(@RequestBody String data,
			@RequestHeader(value = Constants.TOKEN_KEY, defaultValue = Constants.TOKEN_DEFAULT_VALUE) String token,
			@RequestHeader(value = Constants.LANGUAGE_KEY, defaultValue = Constants.LANGUAGE_DEFAULT_VALUE) String lang,
			@RequestHeader(value = Constants.USER_AGENT_KEY, defaultValue = Constants.USER_AGENT_DEFAULT_VALUE) String userAgent,
			@RequestHeader(value = Constants.SUBSCRIBER_TYPE_KEY, defaultValue = Constants.SUBSCRIBER_TYPE_DEFAULT_VALUE) String userType,
			@RequestHeader(value = Constants.TARIFF_TYPE_KEY, defaultValue = Constants.TARIFF_TYPE_DEFAULT_VALUE) String tariffType,
			HttpServletRequest servletRequest) throws ClassNotFoundException, SQLException, IOException, JMSException {
		ObjectMapper mapper = new ObjectMapper();

		GeneralServicesBusiness generalServicesBusiness = new GeneralServicesBusiness();
		UploadImageRequest uploadImageRequest = new UploadImageRequest();
		UploadImageResponse uploadImageResponse = new UploadImageResponse();

		String TRANSACTION_NAME = Transactions.UPLOAD_IMAGE_TRANSACTION_NAME + " CONTROLLER";
		String requestTime = Utilities.getReportDateTime();
		try {
			String msisdn = Utilities.getValueFromJSON(Utilities.splitToken(Utilities.decodeString(token)).toString(),
					Constants.MSISDN_KEY);

			// Populating report object before processing business logic.
			uploadImageResponse.setLogsReport(Utilities.preBusinessReportLoggingData(token, msisdn, TRANSACTION_NAME,
					servletRequest.getRemoteAddr(), userAgent, userType, tariffType, data, lang,
					uploadImageResponse.getLogsReport()));

			data = Utilities.addAttributesToRequest(data, msisdn, servletRequest, lang, userAgent);

			Utilities.printInfoLog(msisdn + "-Request Landed in-" + TRANSACTION_NAME, logger);
			String dataWithImage = data;
			data = Utilities.removeParamsFromJSONObject(data, "image");
			Utilities.printInfoLog(msisdn + "-Request Data-" + data, logger);

			uploadImageRequest = mapper.readValue(dataWithImage, UploadImageRequest.class);

			String requestValidationStatus = Validator.validateRequest(msisdn, uploadImageRequest);

			if (requestValidationStatus.equalsIgnoreCase(Constants.VALIDATION_SUCCESS_CODE)) {

				uploadImageResponse = generalServicesBusiness.uploadImageBusiness(msisdn, uploadImageRequest,
						uploadImageResponse);
			} else {
				uploadImageResponse.setCallStatus(Constants.Call_Status_False);
				uploadImageResponse.setResultCode(Constants.VALIDATION_FAILURE_CODE);
				uploadImageResponse.setResultDesc(requestValidationStatus);
			}

			uploadImageResponse.getLogsReport().setResponseCode(uploadImageResponse.getResultCode());
			uploadImageResponse.getLogsReport().setRequestTime(requestTime);
			uploadImageResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
			uploadImageResponse.getLogsReport().setResponse(mapper.writeValueAsString(uploadImageResponse));

			// Sending report log into queue.
			Utilities.prepareLogReportForQueue(uploadImageResponse.getLogsReport());

			Utilities.printInfoLog(msisdn + "-Response Returned from-" + TRANSACTION_NAME + "-"
					+ mapper.writeValueAsString(uploadImageResponse), logger);
		} catch (Exception e) {
			Utilities.printErrorLog(Utilities.convertStackTraceToString(e), logger);
			uploadImageResponse.setCallStatus(Constants.Call_Status_False);

			uploadImageResponse.setResultCode(Constants.EXCEPTION_CODE);
			uploadImageResponse
					.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("unexpected.error", lang));

			uploadImageResponse.getLogsReport().setResponseCode(uploadImageResponse.getResultCode());
			uploadImageResponse.getLogsReport().setRequestTime(requestTime);
			uploadImageResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
			uploadImageResponse.getLogsReport().setResponse(mapper.writeValueAsString(uploadImageResponse));

			// Sending report log into queue.
			Utilities.prepareLogReportForQueue(uploadImageResponse.getLogsReport());
		}
		return uploadImageResponse;
	}

	@RequestMapping(value = "/verifyappversion", method = RequestMethod.POST, headers = Constants.CONTENT_TYPE_DEFAULT_VALUE)
	public VerifyAppVersionResponse verifyAppVersion(@RequestBody String data,
			@RequestHeader(value = Constants.TOKEN_KEY, defaultValue = Constants.TOKEN_DEFAULT_VALUE) String token,
			@RequestHeader(value = Constants.LANGUAGE_KEY, defaultValue = Constants.LANGUAGE_DEFAULT_VALUE) String lang,
			@RequestHeader(value = Constants.USER_AGENT_KEY, defaultValue = Constants.USER_AGENT_DEFAULT_VALUE) String userAgent,
			@RequestHeader(value = Constants.SUBSCRIBER_TYPE_KEY, defaultValue = Constants.SUBSCRIBER_TYPE_DEFAULT_VALUE) String userType,
			@RequestHeader(value = Constants.TARIFF_TYPE_KEY, defaultValue = Constants.TARIFF_TYPE_DEFAULT_VALUE) String tariffType,
			HttpServletRequest servletRequest) throws ClassNotFoundException, SQLException, JsonParseException,
			JsonMappingException, IOException, JMSException {
		ObjectMapper mapper = new ObjectMapper();

		GeneralServicesBusiness generalServicesBusiness = new GeneralServicesBusiness();
		VerifyAppVersionRequest verifyAppVersionRequest = new VerifyAppVersionRequest();
		VerifyAppVersionResponse verifyAppVersionResponse = new VerifyAppVersionResponse();

		String TRANSACTION_NAME = Transactions.VERIFY_APP_VERSION_TRANSACTION_NAME + " CONTROLLER";
		String requestTime = Utilities.getReportDateTime();
		try {

			String msisdn = "Number not applicable.";

			String deviceID = servletRequest.getHeader("deviceID");

			// Populating report object before processing business logic.
			verifyAppVersionResponse.setLogsReport(
					Utilities.preBusinessReportLoggingData("", msisdn, TRANSACTION_NAME, servletRequest.getRemoteAddr(),
							userAgent, "", "", data, lang, verifyAppVersionResponse.getLogsReport()));

			data = Utilities.addAttributesToRequest(data, msisdn, servletRequest, lang, userAgent);

			Utilities.printInfoLog(msisdn + "-Request Landed in-" + TRANSACTION_NAME, logger);
			Utilities.printInfoLog(msisdn + "-Request Data-" + data, logger);

			verifyAppVersionRequest = mapper.readValue(data, VerifyAppVersionRequest.class);

			String requestValidationStatus = Validator.validateRequest(msisdn, verifyAppVersionRequest);

			if (requestValidationStatus.equalsIgnoreCase(Constants.VALIDATION_SUCCESS_CODE)) {
				verifyAppVersionRequest.setIsB2B("B2C");
				verifyAppVersionResponse = generalServicesBusiness.verifyAppVersionBusiness(msisdn, deviceID,
						verifyAppVersionRequest, verifyAppVersionResponse);
			} else {
				verifyAppVersionResponse.setCallStatus(Constants.Call_Status_False);
				verifyAppVersionResponse.setResultCode(Constants.VALIDATION_FAILURE_CODE);
				verifyAppVersionResponse.setResultDesc(requestValidationStatus);
			}

			verifyAppVersionResponse.getLogsReport().setResponseCode(verifyAppVersionResponse.getResultCode());
			verifyAppVersionResponse.getLogsReport().setRequestTime(requestTime);
			verifyAppVersionResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
			verifyAppVersionResponse.getLogsReport().setResponse(mapper.writeValueAsString(verifyAppVersionResponse));

			//get appstores url from cache
			Utilities.printInfoLog(msisdn + "-PLAY_STORE ANDROID" + GetConfigurations.getConfigurationFromCache("appVersion.update.url.android"), logger);
			verifyAppVersionResponse.getData().setPlayStore(GetConfigurations.getConfigurationFromCache("appVersion.update.url.android"));
			Utilities.printInfoLog(msisdn + "-PLAY_STORE IOS" + GetConfigurations.getConfigurationFromCache("appVersion.update.url.ios"), logger);
			verifyAppVersionResponse.getData().setAppStore(GetConfigurations.getConfigurationFromCache("appVersion.update.url.ios"));
			
			// Sending report log into queue.
			Utilities.prepareLogReportForQueue(verifyAppVersionResponse.getLogsReport());

			Utilities.printInfoLog(msisdn + "-Response Returned from-" + TRANSACTION_NAME + "-"
					+ mapper.writeValueAsString(verifyAppVersionResponse), logger);
		} catch (Exception e) {
			Utilities.printErrorLog(Utilities.convertStackTraceToString(e), logger);
			verifyAppVersionResponse.setCallStatus(Constants.Call_Status_False);

			verifyAppVersionResponse.setResultCode(Constants.EXCEPTION_CODE);
			verifyAppVersionResponse
					.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("unexpected.error", lang));

			verifyAppVersionResponse.getLogsReport().setResponseCode(verifyAppVersionResponse.getResultCode());
			verifyAppVersionResponse.getLogsReport().setRequestTime(requestTime);
			verifyAppVersionResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
			verifyAppVersionResponse.getLogsReport().setResponse(mapper.writeValueAsString(verifyAppVersionResponse));

			// Sending report log into queue.
			Utilities.prepareLogReportForQueue(verifyAppVersionResponse.getLogsReport());
		}
		return verifyAppVersionResponse;
	}

	@RequestMapping(value = "/sendinternetsettings", method = RequestMethod.POST, headers = Constants.CONTENT_TYPE_DEFAULT_VALUE)
	public SendInternetSettingsResponse sendInternetSettings(@RequestBody String data,
			@RequestHeader(value = Constants.TOKEN_KEY, defaultValue = Constants.TOKEN_DEFAULT_VALUE) String token,
			@RequestHeader(value = Constants.LANGUAGE_KEY, defaultValue = Constants.LANGUAGE_DEFAULT_VALUE) String lang,
			@RequestHeader(value = Constants.USER_AGENT_KEY, defaultValue = Constants.USER_AGENT_DEFAULT_VALUE) String userAgent,
			@RequestHeader(value = Constants.SUBSCRIBER_TYPE_KEY, defaultValue = Constants.SUBSCRIBER_TYPE_DEFAULT_VALUE) String userType,
			@RequestHeader(value = Constants.TARIFF_TYPE_KEY, defaultValue = Constants.TARIFF_TYPE_DEFAULT_VALUE) String tariffType,
			HttpServletRequest servletRequest) throws ClassNotFoundException, SQLException, JsonParseException,
			JsonMappingException, IOException, JMSException {
		ObjectMapper mapper = new ObjectMapper();
		JSONObject jsonObject = new JSONObject();
		GeneralServicesBusiness generalServicesBusiness = new GeneralServicesBusiness();
		SendInternetSettingsRequest sendInternetSettingsRequest = new SendInternetSettingsRequest();
		SendInternetSettingsResponse sendInternetSettingsResponse = new SendInternetSettingsResponse();

		String TRANSACTION_NAME = Transactions.SEND_INTERNET_SETTINGS + " CONTROLLER";
		String requestTime = Utilities.getReportDateTime();
		try {
			data = jsonObject.toString(); // use it when there is no request
			// body required.

			String msisdn = Utilities.getValueFromJSON(Utilities.splitToken(Utilities.decodeString(token)).toString(),
					Constants.MSISDN_KEY);

			// Populating report object before processing business logic.
			sendInternetSettingsResponse.setLogsReport(Utilities.preBusinessReportLoggingData(token, msisdn,
					TRANSACTION_NAME, servletRequest.getRemoteAddr(), userAgent, userType, tariffType, data, lang,
					sendInternetSettingsResponse.getLogsReport()));

			data = Utilities.addAttributesToRequest(data, msisdn, servletRequest, lang, userAgent);

			Utilities.printInfoLog(msisdn + "-Request Landed in-" + TRANSACTION_NAME, logger);
			Utilities.printInfoLog(msisdn + "-Request Data-" + data, logger);

			sendInternetSettingsRequest = mapper.readValue(data, SendInternetSettingsRequest.class);

			String requestValidationStatus = Validator.validateRequest(msisdn, sendInternetSettingsRequest);

			if (requestValidationStatus.equalsIgnoreCase(Constants.VALIDATION_SUCCESS_CODE)) {

				sendInternetSettingsResponse = generalServicesBusiness.sendInternetSettingsBusiness(msisdn,
						sendInternetSettingsRequest, sendInternetSettingsResponse);
			} else {
				sendInternetSettingsResponse.setCallStatus(Constants.Call_Status_False);
				sendInternetSettingsResponse.setResultCode(Constants.VALIDATION_FAILURE_CODE);
				sendInternetSettingsResponse.setResultDesc(requestValidationStatus);
			}

			sendInternetSettingsResponse.getLogsReport().setResponseCode(sendInternetSettingsResponse.getResultCode());
			sendInternetSettingsResponse.getLogsReport().setRequestTime(requestTime);
			sendInternetSettingsResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
			sendInternetSettingsResponse.getLogsReport()
					.setResponse(mapper.writeValueAsString(sendInternetSettingsResponse));

			// Sending report log into queue.
			Utilities.prepareLogReportForQueue(sendInternetSettingsResponse.getLogsReport());

			Utilities.printInfoLog(msisdn + "-Response Returned from-" + TRANSACTION_NAME + "-"
					+ mapper.writeValueAsString(sendInternetSettingsResponse), logger);
		} catch (Exception e) {
			Utilities.printErrorLog(Utilities.convertStackTraceToString(e), logger);
			sendInternetSettingsResponse.setCallStatus(Constants.Call_Status_False);

			sendInternetSettingsResponse.setResultCode(Constants.EXCEPTION_CODE);
			sendInternetSettingsResponse
					.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("unexpected.error", lang));

			sendInternetSettingsResponse.getLogsReport().setResponseCode(sendInternetSettingsResponse.getResultCode());
			sendInternetSettingsResponse.getLogsReport().setRequestTime(requestTime);
			sendInternetSettingsResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
			sendInternetSettingsResponse.getLogsReport()
					.setResponse(mapper.writeValueAsString(sendInternetSettingsResponse));

			// Sending report log into queue.
			Utilities.prepareLogReportForQueue(sendInternetSettingsResponse.getLogsReport());
		}
		return sendInternetSettingsResponse;
	}

	// start of of GeneralServices getRoaming API in Controller
	@RequestMapping(value = "/getroaming", method = RequestMethod.POST, headers = Constants.CONTENT_TYPE_DEFAULT_VALUE)
	public GetRoamingResponse getroaming(@RequestBody String data,
			@RequestHeader(value = Constants.TOKEN_KEY, defaultValue = Constants.TOKEN_DEFAULT_VALUE) String token,
			@RequestHeader(value = Constants.LANGUAGE_KEY, defaultValue = Constants.LANGUAGE_DEFAULT_VALUE) String lang,
			@RequestHeader(value = Constants.USER_AGENT_KEY, defaultValue = Constants.USER_AGENT_DEFAULT_VALUE) String userAgent,
			@RequestHeader(value = Constants.SUBSCRIBER_TYPE_KEY, defaultValue = Constants.SUBSCRIBER_TYPE_DEFAULT_VALUE) String userType,
			@RequestHeader(value = Constants.TARIFF_TYPE_KEY, defaultValue = Constants.TARIFF_TYPE_DEFAULT_VALUE) String tariffType,
			HttpServletRequest servletRequest) throws ClassNotFoundException, SQLException, JsonParseException,
			JsonMappingException, IOException, JMSException {
		ObjectMapper mapper = new ObjectMapper();
		JSONObject jsonObject = new JSONObject();
		GeneralServicesBusiness generalServicesBusiness = new GeneralServicesBusiness();
		GetRoamingRequest roaamingrequest = new GetRoamingRequest();
		GetRoamingResponse responsegetroaming = new GetRoamingResponse();

		String TRANSACTION_NAME = Transactions.ROAMING_COUNTRIES + " CONTROLLER";
		String requestTime = Utilities.getReportDateTime();
		try {
			// data = jsonObject.toString(); // use it when there is no request
			// body required.

			String msisdn = Utilities.getValueFromJSON(Utilities.splitToken(Utilities.decodeString(token)).toString(),
					Constants.MSISDN_KEY);

			// Populating report object before processing business logic.
			responsegetroaming.setLogsReport(Utilities.preBusinessReportLoggingData(token, msisdn, TRANSACTION_NAME,
					servletRequest.getRemoteAddr(), userAgent, userType, tariffType, data, lang,
					responsegetroaming.getLogsReport()));

			data = Utilities.addAttributesToRequest(data, msisdn, servletRequest, lang, userAgent);

			Utilities.printInfoLog(msisdn + "-Request Landed in-" + TRANSACTION_NAME, logger);
			Utilities.printInfoLog(msisdn + "-Request Data-" + data, logger);

			roaamingrequest = mapper.readValue(data, GetRoamingRequest.class);

			String requestValidationStatus = Validator.validateRequest(msisdn, roaamingrequest);

			if (requestValidationStatus.equalsIgnoreCase(Constants.VALIDATION_SUCCESS_CODE)) {
				roaamingrequest.setBrandName(userType);
				responsegetroaming = generalServicesBusiness.getRoaming(msisdn, roaamingrequest, responsegetroaming);
			} else {
				responsegetroaming.setCallStatus(Constants.Call_Status_False);
				responsegetroaming.setResultCode(Constants.VALIDATION_FAILURE_CODE);
				responsegetroaming.setResultDesc(requestValidationStatus);
			}

			responsegetroaming.getLogsReport().setResponseCode(responsegetroaming.getResultCode());
			responsegetroaming.getLogsReport().setRequestTime(requestTime);
			responsegetroaming.getLogsReport().setResponseTime(Utilities.getReportDateTime());
			responsegetroaming.getLogsReport().setResponse(mapper.writeValueAsString(responsegetroaming));

			// Sending report log into queue.
			Utilities.prepareLogReportForQueue(responsegetroaming.getLogsReport());

			Utilities.printInfoLog(msisdn + "-Response Returned from-" + TRANSACTION_NAME + "-"
					+ mapper.writeValueAsString(responsegetroaming), logger);
		} catch (Exception e) {
			Utilities.printErrorLog(Utilities.convertStackTraceToString(e), logger);
			responsegetroaming.setCallStatus(Constants.Call_Status_False);

			responsegetroaming.setResultCode(Constants.EXCEPTION_CODE);
			responsegetroaming
					.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("unexpected.error", lang));

			responsegetroaming.getLogsReport().setResponseCode(responsegetroaming.getResultCode());
			responsegetroaming.getLogsReport().setRequestTime(requestTime);
			responsegetroaming.getLogsReport().setResponseTime(Utilities.getReportDateTime());
			responsegetroaming.getLogsReport().setResponse(mapper.writeValueAsString(responsegetroaming));

			// Sending report log into queue.
			Utilities.prepareLogReportForQueue(responsegetroaming.getLogsReport());
		}
		return responsegetroaming;
	}

	@RequestMapping(value = "/exchangeservice", method = RequestMethod.POST, headers = Constants.CONTENT_TYPE_DEFAULT_VALUE)
	public ExchangeServiceResponse getExchangeServices(@RequestBody String data,
			@RequestHeader(value = Constants.TOKEN_KEY, defaultValue = Constants.TOKEN_DEFAULT_VALUE) String token,
			@RequestHeader(value = Constants.LANGUAGE_KEY, defaultValue = Constants.LANGUAGE_DEFAULT_VALUE) String lang,
			@RequestHeader(value = Constants.USER_AGENT_KEY, defaultValue = Constants.USER_AGENT_DEFAULT_VALUE) String userAgent,
			@RequestHeader(value = Constants.SUBSCRIBER_TYPE_KEY, defaultValue = Constants.SUBSCRIBER_TYPE_DEFAULT_VALUE) String userType,
			@RequestHeader(value = Constants.TARIFF_TYPE_KEY, defaultValue = Constants.TARIFF_TYPE_DEFAULT_VALUE) String tariffType,
			HttpServletRequest servletRequest) throws ClassNotFoundException, SQLException, JsonParseException,
			JsonMappingException, IOException, JMSException {
		ObjectMapper mapper = new ObjectMapper();
		JSONObject jsonObject = new JSONObject();
		GeneralServicesBusiness generalServicesBusiness = new GeneralServicesBusiness();
		ExchangeServiceRequestClient exchangeServiceRequest = new ExchangeServiceRequestClient();
		ExchangeServiceResponse exchangeServiceResponse = new ExchangeServiceResponse();

		String TRANSACTION_NAME = Transactions.EXCHNAGE_SERVICE_TRANSACTION_NAME + " CONTROLLER";
		String requestTime = Utilities.getReportDateTime();
		try {
			//data = jsonObject.toString(); // use it when there is no request
			// body required.

			String msisdn = Utilities.getValueFromJSON(Utilities.splitToken(Utilities.decodeString(token)).toString(),
					Constants.MSISDN_KEY);

			// Populating report object before processing business logic.
			exchangeServiceResponse.setLogsReport(Utilities.preBusinessReportLoggingData(token, msisdn,
					TRANSACTION_NAME, servletRequest.getRemoteAddr(), userAgent, userType, tariffType, data, lang,
					exchangeServiceResponse.getLogsReport()));

			data = Utilities.addAttributesToRequest(data, msisdn, servletRequest, lang, userAgent);

			Utilities.printInfoLog(msisdn + "-Request Landed in-" + TRANSACTION_NAME, logger);
			Utilities.printInfoLog(msisdn + "-Request Data-" + data, logger);

			exchangeServiceRequest = mapper.readValue(data, ExchangeServiceRequestClient.class);

			String requestValidationStatus = Validator.validateRequest(msisdn, exchangeServiceRequest);

			if (requestValidationStatus.equalsIgnoreCase(Constants.VALIDATION_SUCCESS_CODE)) {

				exchangeServiceResponse = generalServicesBusiness.exchangeServicesBusiness(msisdn,
						exchangeServiceRequest, exchangeServiceResponse);

			} else {
				exchangeServiceResponse.setCallStatus(Constants.Call_Status_False);
				exchangeServiceResponse.setResultCode(Constants.VALIDATION_FAILURE_CODE);
				exchangeServiceResponse.setResultDesc(requestValidationStatus);
			}

			exchangeServiceResponse.getLogsReport().setResponseCode(exchangeServiceResponse.getResultCode());
			exchangeServiceResponse.getLogsReport().setRequestTime(requestTime);
			exchangeServiceResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
			exchangeServiceResponse.getLogsReport().setResponse(mapper.writeValueAsString(exchangeServiceResponse));

			// Sending report log into queue.
			Utilities.prepareLogReportForQueue(exchangeServiceResponse.getLogsReport());

			Utilities.printInfoLog(msisdn + "-Response Returned from-" + TRANSACTION_NAME + "-"
					+ mapper.writeValueAsString(exchangeServiceResponse), logger);
		} catch (Exception e) {
			Utilities.printErrorLog(Utilities.convertStackTraceToString(e), logger);
			exchangeServiceResponse.setCallStatus(Constants.Call_Status_False);

			exchangeServiceResponse.setResultCode(Constants.EXCEPTION_CODE);
			exchangeServiceResponse
					.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("unexpected.error", lang));

			exchangeServiceResponse.getLogsReport().setResponseCode(exchangeServiceResponse.getResultCode());
			exchangeServiceResponse.getLogsReport().setRequestTime(requestTime);
			exchangeServiceResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
			exchangeServiceResponse.getLogsReport().setResponse(mapper.writeValueAsString(exchangeServiceResponse));

			// Sending report log into queue.
			Utilities.prepareLogReportForQueue(exchangeServiceResponse.getLogsReport());
		}
		return exchangeServiceResponse;
	}

	@RequestMapping(value = "/exchangeservicevalues", method = RequestMethod.POST, headers = Constants.CONTENT_TYPE_DEFAULT_VALUE)
	public ExchangeServicesValuesResponse getExchangeServicesValues(@RequestBody String data,
			@RequestHeader(value = Constants.TOKEN_KEY, defaultValue = Constants.TOKEN_DEFAULT_VALUE) String token,
			@RequestHeader(value = Constants.LANGUAGE_KEY, defaultValue = Constants.LANGUAGE_DEFAULT_VALUE) String lang,
			@RequestHeader(value = Constants.USER_AGENT_KEY, defaultValue = Constants.USER_AGENT_DEFAULT_VALUE) String userAgent,
			@RequestHeader(value = Constants.SUBSCRIBER_TYPE_KEY, defaultValue = Constants.SUBSCRIBER_TYPE_DEFAULT_VALUE) String userType,
			@RequestHeader(value = Constants.TARIFF_TYPE_KEY, defaultValue = Constants.TARIFF_TYPE_DEFAULT_VALUE) String tariffType,
			HttpServletRequest servletRequest) throws ClassNotFoundException, SQLException, JsonParseException,
			JsonMappingException, IOException, JMSException {
		ObjectMapper mapper = new ObjectMapper();
		JSONObject jsonObject = new JSONObject();
		GeneralServicesBusiness generalServicesBusiness = new GeneralServicesBusiness();
		ExchangeServiceValuesRequest exchangeServiceRequest = new ExchangeServiceValuesRequest();
		ExchangeServicesValuesResponse exchangeServiceResponse = new ExchangeServicesValuesResponse();

		String TRANSACTION_NAME = Transactions.EXCHNAGE_SERVICE_TRANSACTION_NAME + " CONTROLLER";
		String requestTime = Utilities.getReportDateTime();
		try {
			/*data = jsonObject.toString();*/ // use it when there is no request
			// body required.

			String msisdn = Utilities.getValueFromJSON(Utilities.splitToken(Utilities.decodeString(token)).toString(),
					Constants.MSISDN_KEY);

			// Populating report object before processing business logic.
			exchangeServiceResponse.setLogsReport(Utilities.preBusinessReportLoggingData(token, msisdn,
					TRANSACTION_NAME, servletRequest.getRemoteAddr(), userAgent, userType, tariffType, data, lang,
					exchangeServiceResponse.getLogsReport()));

			data = Utilities.addAttributesToRequest(data, msisdn, servletRequest, lang, userAgent);

			Utilities.printInfoLog(msisdn + "-Request Landed in-" + TRANSACTION_NAME, logger);
			Utilities.printInfoLog(msisdn + "-Request Data-" + data, logger);

			exchangeServiceRequest = mapper.readValue(data, ExchangeServiceValuesRequest.class);

			String requestValidationStatus = Validator.validateRequest(msisdn, exchangeServiceRequest);

			if (requestValidationStatus.equalsIgnoreCase(Constants.VALIDATION_SUCCESS_CODE)) {

				exchangeServiceResponse = generalServicesBusiness.exchangeServicesValuesBusiness(msisdn,
						exchangeServiceRequest, exchangeServiceResponse);

			} else {
				exchangeServiceResponse.setCallStatus(Constants.Call_Status_False);
				exchangeServiceResponse.setResultCode(Constants.VALIDATION_FAILURE_CODE);
				exchangeServiceResponse.setResultDesc(requestValidationStatus);
			}

			exchangeServiceResponse.getLogsReport().setResponseCode(exchangeServiceResponse.getResultCode());
			exchangeServiceResponse.getLogsReport().setRequestTime(requestTime);
			exchangeServiceResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
			exchangeServiceResponse.getLogsReport().setResponse(mapper.writeValueAsString(exchangeServiceResponse));

			// Sending report log into queue.
			Utilities.prepareLogReportForQueue(exchangeServiceResponse.getLogsReport());

			Utilities.printInfoLog(msisdn + "-Response Returned from-" + TRANSACTION_NAME + "-"
					+ mapper.writeValueAsString(exchangeServiceResponse), logger);
		} catch (Exception e) {
			Utilities.printErrorLog(Utilities.convertStackTraceToString(e), logger);
			exchangeServiceResponse.setCallStatus(Constants.Call_Status_False);

			exchangeServiceResponse.setResultCode(Constants.EXCEPTION_CODE);
			exchangeServiceResponse
					.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("unexpected.error", lang));

			exchangeServiceResponse.getLogsReport().setResponseCode(exchangeServiceResponse.getResultCode());
			exchangeServiceResponse.getLogsReport().setRequestTime(requestTime);
			exchangeServiceResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
			exchangeServiceResponse.getLogsReport().setResponse(mapper.writeValueAsString(exchangeServiceResponse));

			// Sending report log into queue.
			Utilities.prepareLogReportForQueue(exchangeServiceResponse.getLogsReport());
		}
		return exchangeServiceResponse;
	}
}
