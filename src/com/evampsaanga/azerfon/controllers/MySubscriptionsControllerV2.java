/**
 * 
 */
package com.evampsaanga.azerfon.controllers;

import java.io.IOException;
import java.net.SocketException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.jms.JMSException;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.evampsaanga.azerfon.business.MySubscriptionsBusiness;
import com.evampsaanga.azerfon.common.messagesmappings.GetMessagesMappings;
import com.evampsaanga.azerfon.common.utilities.Constants;
import com.evampsaanga.azerfon.common.utilities.Transactions;
import com.evampsaanga.azerfon.common.utilities.Utilities;
import com.evampsaanga.azerfon.common.utilities.Validator;
import com.evampsaanga.azerfon.models.mysubscriptions.getsubscriptions.MySubscriptionsRequest;
import com.evampsaanga.azerfon.models.supplementaryofferings.getsupplementaryofferings.SupplementaryOfferingsResponse;
import com.evampsaanga.azerfon.models.supplementaryofferings.getsupplementaryofferings.SupplementaryOfferingsResponseModified;
import com.evampsaanga.azerfon.models.supplementaryofferings.getsupplementaryofferings.SupplementaryOfferingsResponseModifiedData;
import com.evampsaanga.azerfon.models.supplementaryofferings.getsupplementaryofferings.commonofferings.SupplementryOfferingsData;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * @author Evamp & Saanga
 *
 */

@RestController
@RequestMapping("/mysubscriptionsV2")
public class MySubscriptionsControllerV2 {

    Logger logger = Logger.getLogger(MySubscriptionsControllerV2.class);

    @RequestMapping(value = "/getindividualsubscriptions", method = RequestMethod.POST, headers = Constants.CONTENT_TYPE_DEFAULT_VALUE)
    public SupplementaryOfferingsResponse getIndividualSubscriptions(@RequestBody String data,
	    @RequestHeader(value = Constants.MSISDN_KEY, defaultValue = Constants.MSISDN_DEFAULT_VALUE) String msisdn,
	    @RequestHeader(value = Constants.LANGUAGE_KEY, defaultValue = Constants.LANGUAGE_DEFAULT_VALUE) String lang,
	    @RequestHeader(value = Constants.USER_AGENT_KEY, defaultValue = Constants.USER_AGENT_DEFAULT_VALUE) String userAgent,
	    @RequestHeader(value = Constants.SUBSCRIBER_TYPE_KEY, defaultValue = Constants.SUBSCRIBER_TYPE_DEFAULT_VALUE) String userType,
	    @RequestHeader(value = Constants.TARIFF_TYPE_KEY, defaultValue = Constants.TARIFF_TYPE_DEFAULT_VALUE) String tariffType,
	    @RequestHeader(value = Constants.APP_PHASE_KEY, defaultValue = Constants.APP_PHASE_DEFAULT_VALUE) String isFromB2B,
	    HttpServletRequest servletRequest) throws ClassNotFoundException, SQLException, IOException, JMSException {
	ObjectMapper mapper = new ObjectMapper();
	MySubscriptionsBusiness mySubscriptionsBusiness = new MySubscriptionsBusiness();
	MySubscriptionsRequest mySubscriptionsRequest = new MySubscriptionsRequest();
	SupplementaryOfferingsResponse mySubscriptionsResponse = new SupplementaryOfferingsResponse();
	
	

	String TRANSACTION_NAME = Transactions.MYSUBSCRIPTION_TRANSACTION_NAME + " CONTROLLER";
	String requestTime = Utilities.getReportDateTime();
	try {

	    // Populating report object before processing business logic.
	    mySubscriptionsResponse.setLogsReport(
		    Utilities.preBusinessReportLoggingData("", msisdn, TRANSACTION_NAME, servletRequest.getRemoteAddr(),
			    userAgent, userType, tariffType, data, lang, mySubscriptionsResponse.getLogsReport()));

	    data = Utilities.addAttributesToRequest(data, msisdn, servletRequest, lang, userAgent);

	    Utilities.printInfoLog(msisdn + "-Request Landed in-" + TRANSACTION_NAME, logger);
	    Utilities.printInfoLog(msisdn + "-Request Data-" + data, logger);

	    mySubscriptionsRequest = mapper.readValue(data, MySubscriptionsRequest.class);

	    if (!mySubscriptionsRequest.getIndividualMsisdn().equals("")
		    || !mySubscriptionsRequest.getIndividualMsisdn().equals(null)) {
		mySubscriptionsRequest.setMsisdn(mySubscriptionsRequest.getIndividualMsisdn());
	    } else {

		mySubscriptionsResponse.setCallStatus(Constants.Call_Status_False);
		mySubscriptionsResponse.setResultDesc("----Invalid Individual MSISDN------");
		mySubscriptionsResponse.setResultCode(Constants.MSISDN_NOT_VALID_FORGOT_PASSWORD);

		mySubscriptionsResponse.getLogsReport().setResponseCode(mySubscriptionsResponse.getResultCode());
		mySubscriptionsResponse.getLogsReport().setRequestTime(requestTime);
		mySubscriptionsResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
		mySubscriptionsResponse.getLogsReport().setResponse(mapper.writeValueAsString(mySubscriptionsResponse));
		Utilities.prepareLogReportForQueue(mySubscriptionsResponse.getLogsReport());
		return mySubscriptionsResponse;
	    }
	    
	   
	    String requestValidationStatus = Validator.validateRequest(msisdn, mySubscriptionsRequest);

	    if (requestValidationStatus.equalsIgnoreCase(Constants.VALIDATION_SUCCESS_CODE)) {

		mySubscriptionsResponse = mySubscriptionsBusiness.getSubscriptions(msisdn, mySubscriptionsRequest,
			mySubscriptionsResponse);

		/*
		 * SupplementaryOfferingsResponse mySubscriptionsResponseDeep=null;
		 * mySubscriptionsResponseDeep = (SupplementaryOfferingsResponse)
		 * mySubscriptionsResponse.clone();
		 */

		// sortSubscriptionOfferings(mySubscriptionsResponseDeep);
		Utilities.printInfoLog(msisdn
			+ "---------Usage Controller MySbscriptions BEFORE--------------- allowedCheckImlp" + data,
			logger);
		SupplementaryOfferingsResponse mySubscriptionsResponseDeep = null;
		mySubscriptionsResponseDeep = (SupplementaryOfferingsResponse) mySubscriptionsResponse.clone();
		mySubscriptionsResponseDeep = MySubscriptionsController.allowedCheckImlp(mySubscriptionsResponseDeep,
			mySubscriptionsResponse, mySubscriptionsRequest);
		sortSubscriptionOfferings(mySubscriptionsResponseDeep);
		Utilities.printDebugLog(
			mySubscriptionsRequest.getMsisdn() + "-Usage Controller MySbscriptions BefoeReturn: "
				+ mapper.writeValueAsString(mySubscriptionsResponseDeep),
			logger);
		return mySubscriptionsResponseDeep;
	    } else {
		mySubscriptionsResponse.setCallStatus(Constants.Call_Status_False);
		mySubscriptionsResponse.setResultCode(Constants.VALIDATION_FAILURE_CODE);
		mySubscriptionsResponse.setResultDesc(requestValidationStatus);
	    }

	    mySubscriptionsResponse.getLogsReport().setResponseCode(mySubscriptionsResponse.getResultCode());
	    mySubscriptionsResponse.getLogsReport().setRequestTime(requestTime);
	    mySubscriptionsResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
	    mySubscriptionsResponse.getLogsReport().setResponse(mapper.writeValueAsString(mySubscriptionsResponse));

	    // Sending report log into queue.
	    Utilities.prepareLogReportForQueue(mySubscriptionsResponse.getLogsReport());

	    Utilities.printInfoLog(msisdn + "-Response Returned from-" + TRANSACTION_NAME + "-"
		    + mapper.writeValueAsString(mySubscriptionsResponse), logger);
	} catch (Exception e) {
	    Utilities.printErrorLog(Utilities.convertStackTraceToString(e), logger);
	    mySubscriptionsResponse.setCallStatus(Constants.Call_Status_False);

	    mySubscriptionsResponse.setResultCode(Constants.EXCEPTION_CODE);
	    mySubscriptionsResponse
		    .setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("unexpected.error", lang));

	    mySubscriptionsResponse.getLogsReport().setResponseCode(mySubscriptionsResponse.getResultCode());
	    mySubscriptionsResponse.getLogsReport().setRequestTime(requestTime);
	    mySubscriptionsResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
	    mySubscriptionsResponse.getLogsReport().setResponse(mapper.writeValueAsString(mySubscriptionsResponse));

	    // Sending report log into queue.
	    Utilities.prepareLogReportForQueue(mySubscriptionsResponse.getLogsReport());

	}
	return mySubscriptionsResponse;
    }

    @RequestMapping(value = "/getsubscriptions", method = RequestMethod.POST, headers = Constants.CONTENT_TYPE_DEFAULT_VALUE)
    public SupplementaryOfferingsResponseModified getSubscriptions(@RequestBody String data,
	    @RequestHeader(value = Constants.MSISDN_KEY, defaultValue = Constants.MSISDN_DEFAULT_VALUE) String msisdn,
	    @RequestHeader(value = Constants.LANGUAGE_KEY, defaultValue = Constants.LANGUAGE_DEFAULT_VALUE) String lang,
	    @RequestHeader(value = Constants.USER_AGENT_KEY, defaultValue = Constants.USER_AGENT_DEFAULT_VALUE) String userAgent,
	    @RequestHeader(value = Constants.SUBSCRIBER_TYPE_KEY, defaultValue = Constants.SUBSCRIBER_TYPE_DEFAULT_VALUE) String userType,
	    @RequestHeader(value = Constants.TARIFF_TYPE_KEY, defaultValue = Constants.TARIFF_TYPE_DEFAULT_VALUE) String tariffType,
	    @RequestHeader(value = Constants.APP_PHASE_KEY, defaultValue = Constants.APP_PHASE_DEFAULT_VALUE) String isFromB2B,
	    HttpServletRequest servletRequest) throws ClassNotFoundException, SQLException, IOException, JMSException {
	ObjectMapper mapper = new ObjectMapper();
	SupplementaryOfferingsResponseModified response = null;

	MySubscriptionsBusiness mySubscriptionsBusiness = new MySubscriptionsBusiness();
	MySubscriptionsRequest mySubscriptionsRequest = new MySubscriptionsRequest();
	SupplementaryOfferingsResponse mySubscriptionsResponse = new SupplementaryOfferingsResponse();

	String TRANSACTION_NAME = Transactions.MYSUBSCRIPTION_TRANSACTION_NAME + " CONTROLLER";
	String requestTime = Utilities.getReportDateTime();
	try {
	    /*
	     * String msisdn =
	     * Utilities.getValueFromJSON(Utilities.splitToken(Utilities.decodeString(token)
	     * ).toString(), Constants.MSISDN_KEY);
	     */

	    // Populating report object before processing business logic.
	    mySubscriptionsResponse.setLogsReport(
		    Utilities.preBusinessReportLoggingData("", msisdn, TRANSACTION_NAME, servletRequest.getRemoteAddr(),
			    userAgent, userType, tariffType, data, lang, mySubscriptionsResponse.getLogsReport()));

	    data = Utilities.addAttributesToRequest(data, msisdn, servletRequest, lang, userAgent);

	    Utilities.printInfoLog(msisdn + "-Request Landed in-" + TRANSACTION_NAME, logger);
	    Utilities.printInfoLog(msisdn + "-Request Data-" + data, logger);

	    mySubscriptionsRequest = mapper.readValue(data, MySubscriptionsRequest.class);
	    if (!mySubscriptionsRequest.getIndividualMsisdn().equals("")
		    || !mySubscriptionsRequest.getIndividualMsisdn().equals(null)) {
		mySubscriptionsRequest.setMsisdn(mySubscriptionsRequest.getIndividualMsisdn());
	    }
	    // As this is attribute is for B2C, therefore, setting dummy value to avoid flow
	    // error.
	    mySubscriptionsRequest.setOfferingName("nil");
	    if(mySubscriptionsRequest.getBrandName()==null || mySubscriptionsRequest.getBrandName().isEmpty())
	    {
	    	logger.info("Setting Brand Name");
	    	mySubscriptionsRequest.setBrandName(mySubscriptionsRequest.getMsisdn());
	    	
	    	logger.info("Brand Name is ::"+mySubscriptionsRequest.getBrandName());
	    }
	    String requestValidationStatus = Validator.validateRequest(msisdn, mySubscriptionsRequest);

	    if (requestValidationStatus.equalsIgnoreCase(Constants.VALIDATION_SUCCESS_CODE)) {

		mySubscriptionsResponse = mySubscriptionsBusiness.getSubscriptionsV2(msisdn, mySubscriptionsRequest,
			mySubscriptionsResponse);
		sortSubscriptionOfferings(mySubscriptionsResponse);
	    } else {
		mySubscriptionsResponse.setCallStatus(Constants.Call_Status_False);
		mySubscriptionsResponse.setResultCode(Constants.VALIDATION_FAILURE_CODE);
		mySubscriptionsResponse.setResultDesc(requestValidationStatus);
	    }

	    mySubscriptionsResponse.getLogsReport().setResponseCode(mySubscriptionsResponse.getResultCode());
	    mySubscriptionsResponse.getLogsReport().setRequestTime(requestTime);
	    mySubscriptionsResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
	    mySubscriptionsResponse.getLogsReport().setResponse(mapper.writeValueAsString(mySubscriptionsResponse));

	    // Sending report log into queue.
	    mySubscriptionsResponse.getLogsReport().setAppVersion(Constants.APP_VERSION_PHASE2);
	    Utilities.prepareLogReportForQueue(mySubscriptionsResponse.getLogsReport());

	    Utilities.printInfoLog(msisdn + "-Response Returned from-" + TRANSACTION_NAME + "-"
		    + mapper.writeValueAsString(mySubscriptionsResponse), logger);

	    /// start new logic
	    if (mySubscriptionsResponse.getData() != null) {

		SupplementaryOfferingsResponse mySubscriptionsResponseDeep = null;

		mySubscriptionsResponseDeep = (SupplementaryOfferingsResponse) mySubscriptionsResponse.clone();

		response = ModifyResponse(mySubscriptionsResponseDeep, mySubscriptionsResponse);
	    }
	    // end of new logic

	} catch (Exception e) {
	    Utilities.printErrorLog(Utilities.convertStackTraceToString(e), logger);
	    mySubscriptionsResponse.setCallStatus(Constants.Call_Status_False);

	    mySubscriptionsResponse.setResultCode(Constants.EXCEPTION_CODE);
	    mySubscriptionsResponse
		    .setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("unexpected.error", lang));

	    mySubscriptionsResponse.getLogsReport().setResponseCode(mySubscriptionsResponse.getResultCode());
	    mySubscriptionsResponse.getLogsReport().setRequestTime(requestTime);
	    mySubscriptionsResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
	    mySubscriptionsResponse.getLogsReport().setResponse(mapper.writeValueAsString(mySubscriptionsResponse));

	    // Sending report log into queue.
	    mySubscriptionsResponse.getLogsReport().setAppVersion(Constants.APP_VERSION_PHASE2);
	    Utilities.prepareLogReportForQueue(mySubscriptionsResponse.getLogsReport());

	}
	return response;
    }

    private SupplementaryOfferingsResponseModified ModifyResponse(
	    SupplementaryOfferingsResponse mySubscriptionsResponseDeep,
	    SupplementaryOfferingsResponse mySubscriptionsResponse) throws SocketException, JsonProcessingException {

	ObjectMapper mapper = new ObjectMapper();

	SupplementaryOfferingsResponseModified response = new SupplementaryOfferingsResponseModified();
	SupplementaryOfferingsResponseModifiedData data = new SupplementaryOfferingsResponseModifiedData();

	List<SupplementryOfferingsData> offers = new ArrayList<SupplementryOfferingsData>();

	if (mySubscriptionsResponseDeep.getData().getInternet().getOffers() != null
		&& !mySubscriptionsResponseDeep.getData().getInternet().getOffers().isEmpty()) {
	    offers.addAll(mySubscriptionsResponseDeep.getData().getInternet().getOffers());
	}
	if (mySubscriptionsResponseDeep.getData().getCampaign().getOffers() != null
		&& !mySubscriptionsResponseDeep.getData().getCampaign().getOffers().isEmpty()) {
	    offers.addAll(mySubscriptionsResponseDeep.getData().getCampaign().getOffers());
	}
	if (mySubscriptionsResponseDeep.getData().getSms().getOffers() != null
		&& !mySubscriptionsResponseDeep.getData().getSms().getOffers().isEmpty()) {
	    offers.addAll(mySubscriptionsResponseDeep.getData().getSms().getOffers());
	}
	if (mySubscriptionsResponseDeep.getData().getCall().getOffers() != null
		&& !mySubscriptionsResponseDeep.getData().getCall().getOffers().isEmpty()) {
	    offers.addAll(mySubscriptionsResponseDeep.getData().getCall().getOffers());
	}
	if (mySubscriptionsResponseDeep.getData().getTm().getOffers() != null
		&& !mySubscriptionsResponseDeep.getData().getTm().getOffers().isEmpty()) {
	    offers.addAll(mySubscriptionsResponseDeep.getData().getTm().getOffers());
	}
	if (mySubscriptionsResponseDeep.getData().getHybrid().getOffers() != null
		&& !mySubscriptionsResponseDeep.getData().getHybrid().getOffers().isEmpty()) {
	    offers.addAll(mySubscriptionsResponseDeep.getData().getHybrid().getOffers());
	}
	if (mySubscriptionsResponseDeep.getData().getRoaming().getOffers() != null
		&& !mySubscriptionsResponseDeep.getData().getRoaming().getOffers().isEmpty()) {
	    offers.addAll(mySubscriptionsResponseDeep.getData().getRoaming().getOffers());
	}
	if (mySubscriptionsResponseDeep.getData().getInternetInclusiveOffers().getOffers() != null
		&& !mySubscriptionsResponseDeep.getData().getInternetInclusiveOffers().getOffers().isEmpty()) {
	    offers.addAll(mySubscriptionsResponseDeep.getData().getInternetInclusiveOffers().getOffers());
	}
	if (mySubscriptionsResponseDeep.getData().getVoiceInclusiveOffers().getOffers() != null
		&& !mySubscriptionsResponseDeep.getData().getVoiceInclusiveOffers().getOffers().isEmpty()) {
	    offers.addAll(mySubscriptionsResponseDeep.getData().getVoiceInclusiveOffers().getOffers());
	}
	if (mySubscriptionsResponseDeep.getData().getSmsInclusiveOffers().getOffers() != null
		&& !mySubscriptionsResponseDeep.getData().getSmsInclusiveOffers().getOffers().isEmpty()) {
	    offers.addAll(mySubscriptionsResponseDeep.getData().getSmsInclusiveOffers().getOffers());
	}

	data.setOffers(offers);
	response.setData(data);
	response.setCallStatus(mySubscriptionsResponseDeep.getCallStatus());
	response.setResultCode(mySubscriptionsResponseDeep.getResultCode());
	response.setResultDesc(mySubscriptionsResponseDeep.getResultDesc());
	response.setLogsReport(mySubscriptionsResponseDeep.getLogsReport());

	Utilities.printInfoLog("-Modifyed Response-" + mapper.writeValueAsString(response), logger);
	return response;
    }

    private void sortSubscriptionOfferings(SupplementaryOfferingsResponse supplementaryOfferingsResponse) {

	if (supplementaryOfferingsResponse != null && supplementaryOfferingsResponse.getData() != null) {

	    // Sorting Internet offers
	    if (supplementaryOfferingsResponse.getData().getInternet() != null) {
		List<SupplementryOfferingsData> offers = supplementaryOfferingsResponse.getData().getInternet()
			.getOffers();
		if (offers != null && offers.size() > 0) {
		    Collections.sort(offers, new Comparator<SupplementryOfferingsData>() {
			@Override
			public int compare(SupplementryOfferingsData o1, SupplementryOfferingsData o2) {
			    if (o1.getHeader() != null && o2.getHeader() != null
				    && o1.getHeader().getSortOrderMS() != null
				    && o2.getHeader().getSortOrderMS() != null) {
				return o1.getHeader().getSortOrderMS().compareTo(o2.getHeader().getSortOrderMS());
			    } else {
				return Constants.MAX_INT;
			    }
			}
		    });
		}
	    }

	    // Sorting Compaign offers
	    if (supplementaryOfferingsResponse.getData().getCampaign() != null) {
		List<SupplementryOfferingsData> offers = supplementaryOfferingsResponse.getData().getCampaign()
			.getOffers();
		if (offers != null && offers.size() > 0) {
		    Collections.sort(offers, new Comparator<SupplementryOfferingsData>() {
			@Override
			public int compare(SupplementryOfferingsData o1, SupplementryOfferingsData o2) {
			    if (o1.getHeader() != null && o2.getHeader() != null
				    && o1.getHeader().getSortOrderMS() != null
				    && o2.getHeader().getSortOrderMS() != null) {
				return o1.getHeader().getSortOrderMS().compareTo(o2.getHeader().getSortOrderMS());
			    } else {
				return Constants.MAX_INT;
			    }
			}
		    });
		}
	    }

	    // Sorting SMS offers
	    if (supplementaryOfferingsResponse.getData().getSms() != null) {
		List<SupplementryOfferingsData> offers = supplementaryOfferingsResponse.getData().getSms().getOffers();
		if (offers != null && offers.size() > 0) {
		    Collections.sort(offers, new Comparator<SupplementryOfferingsData>() {
			@Override
			public int compare(SupplementryOfferingsData o1, SupplementryOfferingsData o2) {
			    if (o1.getHeader() != null && o2.getHeader() != null
				    && o1.getHeader().getSortOrderMS() != null
				    && o2.getHeader().getSortOrderMS() != null) {
				return o1.getHeader().getSortOrderMS().compareTo(o2.getHeader().getSortOrderMS());
			    } else {
				return Constants.MAX_INT;
			    }
			}
		    });
		}
	    }

	    // Sorting SMS offers
	    if (supplementaryOfferingsResponse.getData().getCall() != null) {
		List<SupplementryOfferingsData> offers = supplementaryOfferingsResponse.getData().getCall().getOffers();
		if (offers != null && offers.size() > 0) {
		    Collections.sort(offers, new Comparator<SupplementryOfferingsData>() {
			@Override
			public int compare(SupplementryOfferingsData o1, SupplementryOfferingsData o2) {
			    if (o1.getHeader() != null && o2.getHeader() != null
				    && o1.getHeader().getSortOrderMS() != null
				    && o2.getHeader().getSortOrderMS() != null) {
				return o1.getHeader().getSortOrderMS().compareTo(o2.getHeader().getSortOrderMS());
			    } else {
				return Constants.MAX_INT;
			    }
			}
		    });
		}
	    }

	    // Sorting TM offers
	    if (supplementaryOfferingsResponse.getData().getTm() != null) {
		List<SupplementryOfferingsData> offers = supplementaryOfferingsResponse.getData().getTm().getOffers();
		if (offers != null && offers.size() > 0) {
		    Collections.sort(offers, new Comparator<SupplementryOfferingsData>() {
			@Override
			public int compare(SupplementryOfferingsData o1, SupplementryOfferingsData o2) {
			    if (o1.getHeader() != null && o2.getHeader() != null
				    && o1.getHeader().getSortOrderMS() != null
				    && o2.getHeader().getSortOrderMS() != null) {
				return o1.getHeader().getSortOrderMS().compareTo(o2.getHeader().getSortOrderMS());
			    } else {
				return Constants.MAX_INT;
			    }
			}
		    });
		}
	    }

	    // Sorting Hybrid offers
	    if (supplementaryOfferingsResponse.getData().getHybrid() != null) {
		List<SupplementryOfferingsData> offers = supplementaryOfferingsResponse.getData().getHybrid()
			.getOffers();
		if (offers != null && offers.size() > 0) {
		    Collections.sort(offers, new Comparator<SupplementryOfferingsData>() {
			@Override
			public int compare(SupplementryOfferingsData o1, SupplementryOfferingsData o2) {
			    if (o1.getHeader() != null && o2.getHeader() != null
				    && o1.getHeader().getSortOrderMS() != null
				    && o2.getHeader().getSortOrderMS() != null) {
				return o1.getHeader().getSortOrderMS().compareTo(o2.getHeader().getSortOrderMS());
			    } else {
				return Constants.MAX_INT;
			    }
			}
		    });
		}
	    }

	    // Sorting Roaming offers
	    if (supplementaryOfferingsResponse.getData().getRoaming() != null) {
		List<SupplementryOfferingsData> offers = supplementaryOfferingsResponse.getData().getRoaming()
			.getOffers();
		if (offers != null && offers.size() > 0) {
		    Collections.sort(offers, new Comparator<SupplementryOfferingsData>() {
			@Override
			public int compare(SupplementryOfferingsData o1, SupplementryOfferingsData o2) {
			    if (o1.getHeader() != null && o2.getHeader() != null
				    && o1.getHeader().getSortOrderMS() != null
				    && o2.getHeader().getSortOrderMS() != null) {
				return o1.getHeader().getSortOrderMS().compareTo(o2.getHeader().getSortOrderMS());
			    } else {
				return Constants.MAX_INT;
			    }
			}
		    });
		}
	    }

	}
    }

    @RequestMapping(value = "/getsubscriptionsforportal", method = RequestMethod.POST, headers = Constants.CONTENT_TYPE_DEFAULT_VALUE)
    public SupplementaryOfferingsResponse getSubscriptionsForPortal(@RequestBody String data,
	    @RequestHeader(value = "credentials") String token,
	    @RequestHeader(value = Constants.LANGUAGE_KEY, defaultValue = Constants.LANGUAGE_DEFAULT_VALUE) String lang,
	    @RequestHeader(value = Constants.USER_AGENT_KEY, defaultValue = Constants.USER_AGENT_DEFAULT_VALUE) String userAgent,
	    HttpServletRequest servletRequest) throws ClassNotFoundException, SQLException, JsonParseException,
	    JsonMappingException, IOException, JMSException {
	ObjectMapper mapper = new ObjectMapper();
	MySubscriptionsBusiness mySubscriptionsBusiness = new MySubscriptionsBusiness();
	MySubscriptionsRequest mySubscriptionsRequest = new MySubscriptionsRequest();
	SupplementaryOfferingsResponse mySubscriptionsResponse = new SupplementaryOfferingsResponse();

	String TRANSACTION_NAME = Transactions.MYSUBSCRIPTION__FOR_PORTAL_TRANSACTION_NAME + " CONTROLLER";
	String requestTime = Utilities.getReportDateTime();
	try {
	    String msisdn = Utilities.getValueFromJSON(data, Constants.MSISDN_KEY);

	    // Populating report object before processing business logic.
	    mySubscriptionsResponse.setLogsReport(
		    Utilities.preBusinessReportLoggingData("", msisdn, TRANSACTION_NAME, servletRequest.getRemoteAddr(),
			    userAgent, "", "", data, lang, mySubscriptionsResponse.getLogsReport()));

	    data = Utilities.addAttributesToRequest(data, msisdn, servletRequest, lang, userAgent);

	    Utilities.printInfoLog(msisdn + "-Request Landed in-" + TRANSACTION_NAME, logger);
	    Utilities.printInfoLog(msisdn + "-Request Data-" + data, logger);

	    mySubscriptionsRequest = mapper.readValue(data, MySubscriptionsRequest.class);

	    String requestValidationStatus = Validator.validateRequest(msisdn, mySubscriptionsRequest);

	    if (requestValidationStatus.equalsIgnoreCase(Constants.VALIDATION_SUCCESS_CODE)) {

		mySubscriptionsResponse = mySubscriptionsBusiness.getSubscriptions(msisdn, mySubscriptionsRequest,
			mySubscriptionsResponse);
	    } else {
		mySubscriptionsResponse.setCallStatus(Constants.Call_Status_False);
		mySubscriptionsResponse.setResultCode(Constants.VALIDATION_FAILURE_CODE);
		mySubscriptionsResponse.setResultDesc(requestValidationStatus);
	    }

	    mySubscriptionsResponse.getLogsReport().setResponseCode(mySubscriptionsResponse.getResultCode());
	    mySubscriptionsResponse.getLogsReport().setRequestTime(requestTime);
	    mySubscriptionsResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
	    mySubscriptionsResponse.getLogsReport().setResponse(mapper.writeValueAsString(mySubscriptionsResponse));

	    // Sending report log into queue.
	    mySubscriptionsResponse.getLogsReport().setAppVersion(Constants.APP_VERSION_PHASE2);
	    Utilities.prepareLogReportForQueue(mySubscriptionsResponse.getLogsReport());

	    Utilities.printInfoLog(msisdn + "-Response Returned from-" + TRANSACTION_NAME + "-"
		    + mapper.writeValueAsString(mySubscriptionsResponse), logger);
	} catch (Exception e) {
	    Utilities.printErrorLog(Utilities.convertStackTraceToString(e), logger);
	    mySubscriptionsResponse.setCallStatus(Constants.Call_Status_False);

	    mySubscriptionsResponse.setResultCode(Constants.EXCEPTION_CODE);
	    mySubscriptionsResponse
		    .setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("unexpected.error", lang));

	    mySubscriptionsResponse.getLogsReport().setResponseCode(mySubscriptionsResponse.getResultCode());
	    mySubscriptionsResponse.getLogsReport().setRequestTime(requestTime);
	    mySubscriptionsResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
	    mySubscriptionsResponse.getLogsReport().setResponse(mapper.writeValueAsString(mySubscriptionsResponse));

	    // Sending report log into queue.
	    mySubscriptionsResponse.getLogsReport().setAppVersion(Constants.APP_VERSION_PHASE2);
	    Utilities.prepareLogReportForQueue(mySubscriptionsResponse.getLogsReport());

	}
	return mySubscriptionsResponse;
    }
}
