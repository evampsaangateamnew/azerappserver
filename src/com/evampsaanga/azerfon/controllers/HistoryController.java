/**
 * 
 */
package com.evampsaanga.azerfon.controllers;

import java.io.IOException;
import java.sql.SQLException;

import javax.jms.JMSException;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.evampsaanga.azerfon.business.CustomerServicesBusiness;
import com.evampsaanga.azerfon.business.HistoryBusiness;
import com.evampsaanga.azerfon.common.messagesmappings.GetMessagesMappings;
import com.evampsaanga.azerfon.common.utilities.Constants;
import com.evampsaanga.azerfon.common.utilities.Transactions;
import com.evampsaanga.azerfon.common.utilities.Utilities;
import com.evampsaanga.azerfon.common.utilities.Validator;
import com.evampsaanga.azerfon.models.customerservices.resendpin.ResendPINRequest;
import com.evampsaanga.azerfon.models.customerservices.resendpin.ResendPINResponse;
import com.evampsaanga.azerfon.models.history.getoperationshistory.OperationsHistoryRequest;
import com.evampsaanga.azerfon.models.history.getoperationshistory.OperationsHistoryResponse;
import com.evampsaanga.azerfon.models.history.getusagedetails.UsageDetailsRequest;
import com.evampsaanga.azerfon.models.history.getusagedetails.UsageDetailsResponse;
import com.evampsaanga.azerfon.models.history.getusagesummary.UsageSummaryRequest;
import com.evampsaanga.azerfon.models.history.getusagesummary.UsageSummaryResponse;
import com.evampsaanga.azerfon.models.history.verifyaccountdetails.VerifyAccountDetailsRequest;
import com.evampsaanga.azerfon.models.history.verifyaccountdetails.VerifyAccountDetailsResponse;
import com.evampsaanga.azerfon.models.history.verifypin.UsageDetailsVerifyPinRequest;
import com.evampsaanga.azerfon.models.history.verifypin.UsageDetailsVerifyPinResponse;

/**
 * @author Evamp & Saanga
 *
 */

@RestController
@RequestMapping("/history")
public class HistoryController {

    Logger logger = Logger.getLogger(HistoryController.class);

    @RequestMapping(value = "/historyresendpin", method = RequestMethod.POST, headers = Constants.CONTENT_TYPE_DEFAULT_VALUE)
    public ResendPINResponse resendPIN(@RequestBody String data,
	    @RequestHeader(value = Constants.TOKEN_KEY, defaultValue = Constants.TOKEN_DEFAULT_VALUE) String token,
	    @RequestHeader(value = Constants.LANGUAGE_KEY, defaultValue = Constants.LANGUAGE_DEFAULT_VALUE) String lang,
	    @RequestHeader(value = Constants.USER_AGENT_KEY, defaultValue = Constants.USER_AGENT_DEFAULT_VALUE) String userAgent,
	    @RequestHeader(value = Constants.SUBSCRIBER_TYPE_KEY, defaultValue = Constants.SUBSCRIBER_TYPE_DEFAULT_VALUE) String userType,
	    @RequestHeader(value = Constants.TARIFF_TYPE_KEY, defaultValue = Constants.TARIFF_TYPE_DEFAULT_VALUE) String tariffType,
	    HttpServletRequest servletRequest) throws JsonParseException, JsonMappingException, IOException,
	    JMSException, ClassNotFoundException, SQLException {
	ObjectMapper mapper = new ObjectMapper();
	CustomerServicesBusiness customerServicesBusiness = new CustomerServicesBusiness();
	ResendPINRequest resendPINRequest = new ResendPINRequest();
	ResendPINResponse resendPINResponse = new ResendPINResponse();
	String TRANSACTION_NAME = Transactions.HISTORY_RESEND_OTP_TRANSACTION_NAME + " CONTROLLER";
	String requestTime = Utilities.getReportDateTime();
	try {
	    String msisdn = Utilities.getValueFromJSON(Utilities.splitToken(Utilities.decodeString(token)).toString(),
		    Constants.MSISDN_KEY);

	    // Populating report object before processing business logic.
	    resendPINResponse.setLogsReport(Utilities.preBusinessReportLoggingData(token, msisdn, TRANSACTION_NAME,
		    servletRequest.getRemoteAddr(), userAgent, userType, tariffType, data, lang,
		    resendPINResponse.getLogsReport()));

	    String deviceID = servletRequest.getHeader("deviceID");

	    data = Utilities.addAttributesToRequest(data, msisdn, servletRequest, lang, userAgent);

	    Utilities.printInfoLog(msisdn + "-Request Landed in-" + TRANSACTION_NAME, logger);
	    Utilities.printInfoLog(msisdn + "-Request Data-" + data, logger);

	    resendPINRequest = mapper.readValue(data, ResendPINRequest.class);

	    String requestValidationStatus = Validator.validateRequest(msisdn, resendPINRequest);

	    if (requestValidationStatus.equalsIgnoreCase(Constants.VALIDATION_SUCCESS_CODE)) {
		resendPINResponse = customerServicesBusiness.resendPinBusiness(msisdn, deviceID, resendPINRequest,
			resendPINResponse);
	    } else {
		resendPINResponse.setCallStatus(Constants.Call_Status_False);
		resendPINResponse.setResultCode(Constants.VALIDATION_FAILURE_CODE);
		resendPINResponse.setResultDesc(requestValidationStatus);
	    }

	    resendPINResponse.getLogsReport().setResponseCode(resendPINResponse.getResultCode());
	    resendPINResponse.getLogsReport().setRequestTime(requestTime);
	    resendPINResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
	    resendPINResponse.getLogsReport().setResponse(mapper.writeValueAsString(resendPINResponse));

	    // Sending report log into queue.
	    Utilities.prepareLogReportForQueue(resendPINResponse.getLogsReport());

	    Utilities.printInfoLog(msisdn + "-Response Returned from-" + TRANSACTION_NAME + "-"
		    + mapper.writeValueAsString(resendPINResponse), logger);
	} catch (Exception e) {
	    Utilities.printErrorLog(Utilities.convertStackTraceToString(e), logger);
	    resendPINResponse.setCallStatus(Constants.Call_Status_False);

	    resendPINResponse.setResultCode(Constants.EXCEPTION_CODE);
	    resendPINResponse.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("unexpected.error", lang));

	    resendPINResponse.getLogsReport().setResponseCode(resendPINResponse.getResultCode());
	    resendPINResponse.getLogsReport().setRequestTime(requestTime);
	    resendPINResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
	    resendPINResponse.getLogsReport().setResponse(mapper.writeValueAsString(resendPINResponse));

	    // Sending report log into queue.
	    Utilities.prepareLogReportForQueue(resendPINResponse.getLogsReport());
	}
	return resendPINResponse;
    }

    @RequestMapping(value = "/getusagesummary", method = RequestMethod.POST, headers = Constants.CONTENT_TYPE_DEFAULT_VALUE)
    public UsageSummaryResponse getUsageSummary(@RequestBody String data,
	    @RequestHeader(value = Constants.TOKEN_KEY, defaultValue = Constants.TOKEN_DEFAULT_VALUE) String token,
	    @RequestHeader(value = Constants.LANGUAGE_KEY, defaultValue = Constants.LANGUAGE_DEFAULT_VALUE) String lang,
	    @RequestHeader(value = Constants.USER_AGENT_KEY, defaultValue = Constants.USER_AGENT_DEFAULT_VALUE) String userAgent,
	    @RequestHeader(value = Constants.SUBSCRIBER_TYPE_KEY, defaultValue = Constants.SUBSCRIBER_TYPE_DEFAULT_VALUE) String userType,
	    @RequestHeader(value = Constants.TARIFF_TYPE_KEY, defaultValue = Constants.TARIFF_TYPE_DEFAULT_VALUE) String tariffType,
	    HttpServletRequest servletRequest) throws ClassNotFoundException, SQLException, JsonParseException,
	    JsonMappingException, IOException, JMSException {
	ObjectMapper mapper = new ObjectMapper();
	HistoryBusiness usageHistoryBusiness = new HistoryBusiness();
	UsageSummaryRequest usageSummaryRequest = new UsageSummaryRequest();
	UsageSummaryResponse usageSummaryResponse = new UsageSummaryResponse();

	String TRANSACTION_NAME = Transactions.GET_CDRS_SUMMARY_TRANSACTION_NAME + " CONTROLLER";
	String requestTime = Utilities.getReportDateTime();
	try {
	    String msisdn = Utilities.getValueFromJSON(Utilities.splitToken(Utilities.decodeString(token)).toString(),
		    Constants.MSISDN_KEY);

	    // Populating report object before processing business logic.
	    usageSummaryResponse.setLogsReport(Utilities.preBusinessReportLoggingData(token, msisdn, TRANSACTION_NAME,
		    servletRequest.getRemoteAddr(), userAgent, userType, tariffType, data, lang,
		    usageSummaryResponse.getLogsReport()));

	    data = Utilities.addAttributesToRequest(data, msisdn, servletRequest, lang, userAgent);

	    Utilities.printInfoLog(msisdn + "-Request Landed in-" + TRANSACTION_NAME, logger);
	    Utilities.printInfoLog(msisdn + "-Request Data-" + data, logger);

	    usageSummaryRequest = mapper.readValue(data, UsageSummaryRequest.class);

	    // Specific params for report logging
	    usageSummaryResponse.getLogsReport().setAccountID(usageSummaryRequest.getAccountId());

	    String requestValidationStatus = Validator.validateRequest(msisdn, usageSummaryRequest);

	    if (requestValidationStatus.equalsIgnoreCase(Constants.VALIDATION_SUCCESS_CODE)) {

		usageSummaryResponse = usageHistoryBusiness.getUsageSummary(msisdn, usageSummaryRequest,
			usageSummaryResponse);
	    } else {
		usageSummaryResponse.setCallStatus(Constants.Call_Status_False);
		usageSummaryResponse.setResultCode(Constants.VALIDATION_FAILURE_CODE);
		usageSummaryResponse.setResultDesc(requestValidationStatus);
	    }

	    usageSummaryResponse.getLogsReport().setResponseCode(usageSummaryResponse.getResultCode());
	    usageSummaryResponse.getLogsReport().setRequestTime(requestTime);
	    usageSummaryResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
	    usageSummaryResponse.getLogsReport().setResponse(mapper.writeValueAsString(usageSummaryResponse));

	    // Sending report log into queue.
	    Utilities.prepareLogReportForQueue(usageSummaryResponse.getLogsReport());

	    Utilities.printInfoLog(msisdn + "-Response Returned from-" + TRANSACTION_NAME + "-"
		    + mapper.writeValueAsString(usageSummaryResponse), logger);
	} catch (Exception e) {
	    Utilities.printErrorLog(Utilities.convertStackTraceToString(e), logger);
	    usageSummaryResponse.setCallStatus(Constants.Call_Status_False);

	    usageSummaryResponse.setResultCode(Constants.EXCEPTION_CODE);
	    usageSummaryResponse
		    .setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("unexpected.error", lang));

	    usageSummaryResponse.getLogsReport().setResponseCode(usageSummaryResponse.getResultCode());
	    usageSummaryResponse.getLogsReport().setRequestTime(requestTime);
	    usageSummaryResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
	    usageSummaryResponse.getLogsReport().setResponse(mapper.writeValueAsString(usageSummaryResponse));

	    // Sending report log into queue.
	    Utilities.prepareLogReportForQueue(usageSummaryResponse.getLogsReport());
	}
	return usageSummaryResponse;
    }

    @RequestMapping(value = "/getusagedetails", method = RequestMethod.POST, headers = Constants.CONTENT_TYPE_DEFAULT_VALUE)
    public UsageDetailsResponse getDetailedUsageHistory(@RequestBody String data,
	    @RequestHeader(value = Constants.TOKEN_KEY, defaultValue = Constants.TOKEN_DEFAULT_VALUE) String token,
	    @RequestHeader(value = Constants.LANGUAGE_KEY, defaultValue = Constants.LANGUAGE_DEFAULT_VALUE) String lang,
	    @RequestHeader(value = Constants.USER_AGENT_KEY, defaultValue = Constants.USER_AGENT_DEFAULT_VALUE) String userAgent,
	    @RequestHeader(value = Constants.SUBSCRIBER_TYPE_KEY, defaultValue = Constants.SUBSCRIBER_TYPE_DEFAULT_VALUE) String userType,
	    @RequestHeader(value = Constants.TARIFF_TYPE_KEY, defaultValue = Constants.TARIFF_TYPE_DEFAULT_VALUE) String tariffType,
	    HttpServletRequest servletRequest) throws ClassNotFoundException, SQLException, JsonParseException,
	    JsonMappingException, IOException, JMSException {
	ObjectMapper mapper = new ObjectMapper();
	HistoryBusiness usageHistoryBusiness = new HistoryBusiness();
	UsageDetailsRequest usageDetailsRequest = new UsageDetailsRequest();
	UsageDetailsResponse usageDetailsResponse = new UsageDetailsResponse();

	String TRANSACTION_NAME = Transactions.GET_CDRS_BY_DATE_TRANSACTION_NAME + " CONTROLLER";
	String requestTime = Utilities.getReportDateTime();
	try {
	    String msisdn = Utilities.getValueFromJSON(Utilities.splitToken(Utilities.decodeString(token)).toString(),
		    Constants.MSISDN_KEY);

	    data = Utilities.addAttributesToRequest(data, msisdn, servletRequest, lang, userAgent);

	    Utilities.printInfoLog(msisdn + "-Request Landed in-" + TRANSACTION_NAME, logger);
	    Utilities.printInfoLog(msisdn + "-Request Data-" + data, logger);

	    usageDetailsRequest = mapper.readValue(data, UsageDetailsRequest.class);

	    // Populating report object before processing business logic.
	    usageDetailsResponse.setLogsReport(Utilities.preBusinessReportLoggingData(token, msisdn, TRANSACTION_NAME,
		    servletRequest.getRemoteAddr(), userAgent, userType, tariffType, data, lang,
		    usageDetailsResponse.getLogsReport()));

	    // Specific params for report logging
	    usageDetailsResponse.getLogsReport().setAccountID(usageDetailsRequest.getAccountId());

	    String requestValidationStatus = Validator.validateRequest(msisdn, usageDetailsRequest);

	    if (requestValidationStatus.equalsIgnoreCase(Constants.VALIDATION_SUCCESS_CODE)) {

		usageDetailsResponse = usageHistoryBusiness.getUsageDetails(msisdn, usageDetailsRequest,
			usageDetailsResponse);
	    } else {
		usageDetailsResponse.setCallStatus(Constants.Call_Status_False);
		usageDetailsResponse.setResultCode(Constants.VALIDATION_FAILURE_CODE);
		usageDetailsResponse.setResultDesc(requestValidationStatus);
	    }

	    usageDetailsResponse.getLogsReport().setResponseCode(usageDetailsResponse.getResultCode());
	    usageDetailsResponse.getLogsReport().setRequestTime(requestTime);
	    usageDetailsResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
	    usageDetailsResponse.getLogsReport().setResponse(mapper.writeValueAsString(usageDetailsResponse));

	    // Sending report log into queue.
	    Utilities.prepareLogReportForQueue(usageDetailsResponse.getLogsReport());

	    Utilities.printInfoLog(msisdn + "-Response Returned from-" + TRANSACTION_NAME + "-"
		    + mapper.writeValueAsString(usageDetailsResponse), logger);
	} catch (Exception e) {
	    Utilities.printErrorLog(Utilities.convertStackTraceToString(e), logger);
	    usageDetailsResponse.setCallStatus(Constants.Call_Status_False);

	    usageDetailsResponse.setResultCode(Constants.EXCEPTION_CODE);
	    usageDetailsResponse
		    .setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("unexpected.error", lang));

	    usageDetailsResponse.getLogsReport().setResponseCode(usageDetailsResponse.getResultCode());
	    usageDetailsResponse.getLogsReport().setRequestTime(requestTime);
	    usageDetailsResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
	    usageDetailsResponse.getLogsReport().setResponse(mapper.writeValueAsString(usageDetailsResponse));

	    // Sending report log into queue.
	    Utilities.prepareLogReportForQueue(usageDetailsResponse.getLogsReport());

	}
	return usageDetailsResponse;
    }

    @RequestMapping(value = "/getoperationshistory", method = RequestMethod.POST, headers = Constants.CONTENT_TYPE_DEFAULT_VALUE)
    public OperationsHistoryResponse getOperationsHistory(@RequestBody String data,
	    @RequestHeader(value = Constants.TOKEN_KEY, defaultValue = Constants.TOKEN_DEFAULT_VALUE) String token,
	    @RequestHeader(value = Constants.LANGUAGE_KEY, defaultValue = Constants.LANGUAGE_DEFAULT_VALUE) String lang,
	    @RequestHeader(value = Constants.USER_AGENT_KEY, defaultValue = Constants.USER_AGENT_DEFAULT_VALUE) String userAgent,
	    @RequestHeader(value = Constants.SUBSCRIBER_TYPE_KEY, defaultValue = Constants.SUBSCRIBER_TYPE_DEFAULT_VALUE) String userType,
	    @RequestHeader(value = Constants.TARIFF_TYPE_KEY, defaultValue = Constants.TARIFF_TYPE_DEFAULT_VALUE) String tariffType,
	    HttpServletRequest servletRequest) throws ClassNotFoundException, SQLException, JsonParseException,
	    JsonMappingException, IOException, JMSException {

	ObjectMapper mapper = new ObjectMapper();
	HistoryBusiness historyBusiness = new HistoryBusiness();
	OperationsHistoryRequest operationsHistoryRequest = new OperationsHistoryRequest();
	OperationsHistoryResponse operationsHistoryResponse = new OperationsHistoryResponse();

	String TRANSACTION_NAME = Transactions.GET_CDRS_OPERATION_HISTORY_TRANSACTION_NAME + " CONTROLLER";
	String requestTime = Utilities.getReportDateTime();
	try {
	    String msisdn = Utilities.getValueFromJSON(Utilities.splitToken(Utilities.decodeString(token)).toString(),
		    Constants.MSISDN_KEY);

	    // Populating report object before processing business logic.
	    operationsHistoryResponse.setLogsReport(Utilities.preBusinessReportLoggingData(token, msisdn,
		    TRANSACTION_NAME, servletRequest.getRemoteAddr(), userAgent, userType, tariffType, data, lang,
		    operationsHistoryResponse.getLogsReport()));

	    data = Utilities.addAttributesToRequest(data, msisdn, servletRequest, lang, userAgent);

	    Utilities.printInfoLog(msisdn + "-Request Landed in-" + TRANSACTION_NAME, logger);
	    Utilities.printInfoLog(msisdn + "-Request Data-" + data, logger);

	    operationsHistoryRequest = mapper.readValue(data, OperationsHistoryRequest.class);

	    // Specific params for report logging
	    operationsHistoryResponse.getLogsReport().setAccountID(operationsHistoryRequest.getAccountId());
	    String requestValidationStatus = Validator.validateRequest(msisdn, operationsHistoryRequest);

	    if (requestValidationStatus.equalsIgnoreCase(Constants.VALIDATION_SUCCESS_CODE)) {

		operationsHistoryResponse = historyBusiness.getOperationsHistory(msisdn, operationsHistoryRequest,
			operationsHistoryResponse);
	    } else {
		operationsHistoryResponse.setCallStatus(Constants.Call_Status_False);
		operationsHistoryResponse.setResultCode(Constants.VALIDATION_FAILURE_CODE);
		operationsHistoryResponse.setResultDesc(requestValidationStatus);
	    }

	    operationsHistoryResponse.getLogsReport().setResponseCode(operationsHistoryResponse.getResultCode());
	    operationsHistoryResponse.getLogsReport().setRequestTime(requestTime);
	    operationsHistoryResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
	    operationsHistoryResponse.getLogsReport().setResponse(mapper.writeValueAsString(operationsHistoryResponse));

	    // Sending report log into queue.
	    Utilities.prepareLogReportForQueue(operationsHistoryResponse.getLogsReport());

	    Utilities.printInfoLog(msisdn + "-Response Returned from-" + TRANSACTION_NAME + "-"
		    + mapper.writeValueAsString(operationsHistoryResponse), logger);
	} catch (Exception e) {
	    Utilities.printErrorLog(Utilities.convertStackTraceToString(e), logger);
	    operationsHistoryResponse.setCallStatus(Constants.Call_Status_False);

	    operationsHistoryResponse.setResultCode(Constants.EXCEPTION_CODE);
	    operationsHistoryResponse
		    .setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("unexpected.error", lang));

	    operationsHistoryResponse.getLogsReport().setResponseCode(operationsHistoryResponse.getResultCode());
	    operationsHistoryResponse.getLogsReport().setRequestTime(requestTime);
	    operationsHistoryResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
	    operationsHistoryResponse.getLogsReport().setResponse(mapper.writeValueAsString(operationsHistoryResponse));

	    // Sending report log into queue.
	    Utilities.prepareLogReportForQueue(operationsHistoryResponse.getLogsReport());

	}
	return operationsHistoryResponse;
    }

    @RequestMapping(value = "/verifyaccountdetails", method = RequestMethod.POST, headers = Constants.CONTENT_TYPE_DEFAULT_VALUE)
    public VerifyAccountDetailsResponse verifyAccountDetails(@RequestBody String data,
	    @RequestHeader(value = Constants.TOKEN_KEY, defaultValue = Constants.TOKEN_DEFAULT_VALUE) String token,
	    @RequestHeader(value = Constants.LANGUAGE_KEY, defaultValue = Constants.LANGUAGE_DEFAULT_VALUE) String lang,
	    @RequestHeader(value = Constants.USER_AGENT_KEY, defaultValue = Constants.USER_AGENT_DEFAULT_VALUE) String userAgent,
	    @RequestHeader(value = Constants.SUBSCRIBER_TYPE_KEY, defaultValue = Constants.SUBSCRIBER_TYPE_DEFAULT_VALUE) String userType,
	    @RequestHeader(value = Constants.TARIFF_TYPE_KEY, defaultValue = Constants.TARIFF_TYPE_DEFAULT_VALUE) String tariffType,
	    HttpServletRequest servletRequest) throws ClassNotFoundException, SQLException, JsonParseException,
	    JsonMappingException, IOException, JMSException {

	ObjectMapper mapper = new ObjectMapper();
	HistoryBusiness historyBusiness = new HistoryBusiness();
	VerifyAccountDetailsRequest verifyAccountDetailsRequest = new VerifyAccountDetailsRequest();
	VerifyAccountDetailsResponse verifyAccountDetailsResponse = new VerifyAccountDetailsResponse();

	String TRANSACTION_NAME = Transactions.GET_CDRSBY_DATE_OTP_TRANSACTION_NAME + " CONTROLLER";
	String requestTime = Utilities.getReportDateTime();
	try {
	    String msisdn = Utilities.getValueFromJSON(Utilities.splitToken(Utilities.decodeString(token)).toString(),
		    Constants.MSISDN_KEY);

	    // Populating report object before processing business logic.
	    verifyAccountDetailsResponse.setLogsReport(Utilities.preBusinessReportLoggingData(token, msisdn,
		    TRANSACTION_NAME, servletRequest.getRemoteAddr(), userAgent, userType, tariffType, data, lang,
		    verifyAccountDetailsResponse.getLogsReport()));

	    data = Utilities.addAttributesToRequest(data, msisdn, servletRequest, lang, userAgent);

	    Utilities.printInfoLog(msisdn + "-Request Landed in-" + TRANSACTION_NAME, logger);
	    Utilities.printInfoLog(msisdn + "-Request Data-" + data, logger);

	    verifyAccountDetailsRequest = mapper.readValue(data, VerifyAccountDetailsRequest.class);

	    String requestValidationStatus = Validator.validateRequest(msisdn, verifyAccountDetailsRequest);

	    if (requestValidationStatus.equalsIgnoreCase(Constants.VALIDATION_SUCCESS_CODE)) {

		verifyAccountDetailsResponse = historyBusiness.verifyAccountDetailsBusiness(msisdn,
			verifyAccountDetailsRequest, verifyAccountDetailsResponse);
	    } else {
		verifyAccountDetailsResponse.setCallStatus(Constants.Call_Status_False);
		verifyAccountDetailsResponse.setResultCode(Constants.VALIDATION_FAILURE_CODE);
		verifyAccountDetailsResponse.setResultDesc(requestValidationStatus);
	    }

	    verifyAccountDetailsResponse.getLogsReport().setResponseCode(verifyAccountDetailsResponse.getResultCode());
	    verifyAccountDetailsResponse.getLogsReport().setRequestTime(requestTime);
	    verifyAccountDetailsResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
	    verifyAccountDetailsResponse.getLogsReport()
		    .setResponse(mapper.writeValueAsString(verifyAccountDetailsResponse));

	    // Sending report log into queue.
	    Utilities.prepareLogReportForQueue(verifyAccountDetailsResponse.getLogsReport());

	    Utilities.printInfoLog(msisdn + "-Response Returned from-" + TRANSACTION_NAME + "-"
		    + mapper.writeValueAsString(verifyAccountDetailsResponse), logger);
	} catch (Exception e) {
	    Utilities.printErrorLog(Utilities.convertStackTraceToString(e), logger);
	    verifyAccountDetailsResponse.setCallStatus(Constants.Call_Status_False);

	    verifyAccountDetailsResponse.setResultCode(Constants.EXCEPTION_CODE);
	    verifyAccountDetailsResponse
		    .setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("unexpected.error", lang));

	    verifyAccountDetailsResponse.getLogsReport().setResponseCode(verifyAccountDetailsResponse.getResultCode());
	    verifyAccountDetailsResponse.getLogsReport().setRequestTime(requestTime);
	    verifyAccountDetailsResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
	    verifyAccountDetailsResponse.getLogsReport()
		    .setResponse(mapper.writeValueAsString(verifyAccountDetailsResponse));

	    // Sending report log into queue.
	    Utilities.prepareLogReportForQueue(verifyAccountDetailsResponse.getLogsReport());

	}
	return verifyAccountDetailsResponse;
    }

    @RequestMapping(value = "/verifypin", method = RequestMethod.POST, headers = Constants.CONTENT_TYPE_DEFAULT_VALUE)
    public UsageDetailsVerifyPinResponse verifyPin(@RequestBody String data,
	    @RequestHeader(value = Constants.TOKEN_KEY, defaultValue = Constants.TOKEN_DEFAULT_VALUE) String token,
	    @RequestHeader(value = Constants.LANGUAGE_KEY, defaultValue = Constants.LANGUAGE_DEFAULT_VALUE) String lang,
	    @RequestHeader(value = Constants.USER_AGENT_KEY, defaultValue = Constants.USER_AGENT_DEFAULT_VALUE) String userAgent,
	    @RequestHeader(value = Constants.SUBSCRIBER_TYPE_KEY, defaultValue = Constants.SUBSCRIBER_TYPE_DEFAULT_VALUE) String userType,
	    @RequestHeader(value = Constants.TARIFF_TYPE_KEY, defaultValue = Constants.TARIFF_TYPE_DEFAULT_VALUE) String tariffType,
	    HttpServletRequest servletRequest) throws ClassNotFoundException, SQLException, JsonParseException,
	    JsonMappingException, IOException, JMSException {

	ObjectMapper mapper = new ObjectMapper();
	HistoryBusiness historyBusiness = new HistoryBusiness();
	UsageDetailsVerifyPinRequest usageDetailsVerifyPinRequest = new UsageDetailsVerifyPinRequest();
	UsageDetailsVerifyPinResponse usageDetailsVerifyPinResponse = new UsageDetailsVerifyPinResponse();

	String TRANSACTION_NAME = Transactions.VERIFY_CDRS_BY_DATE_OTP_TRANSACTION_NAME + " CONTROLLER";
	String requestTime = Utilities.getReportDateTime();
	try {
	    String msisdn = Utilities.getValueFromJSON(Utilities.splitToken(Utilities.decodeString(token)).toString(),
		    Constants.MSISDN_KEY);

	    // Populating report object before processing business logic.
	    usageDetailsVerifyPinResponse.setLogsReport(Utilities.preBusinessReportLoggingData(token, msisdn,
		    TRANSACTION_NAME, servletRequest.getRemoteAddr(), userAgent, userType, tariffType, data, lang,
		    usageDetailsVerifyPinResponse.getLogsReport()));

	    data = Utilities.addAttributesToRequest(data, msisdn, servletRequest, lang, userAgent);

	    Utilities.printInfoLog(msisdn + "-Request Landed in-" + TRANSACTION_NAME, logger);
	    Utilities.printInfoLog(msisdn + "-Request Data-" + data, logger);

	    usageDetailsVerifyPinRequest = mapper.readValue(data, UsageDetailsVerifyPinRequest.class);

	    String requestValidationStatus = Validator.validateRequest(msisdn, usageDetailsVerifyPinRequest);

	    if (requestValidationStatus.equalsIgnoreCase(Constants.VALIDATION_SUCCESS_CODE)) {

		usageDetailsVerifyPinResponse = historyBusiness.verifyPinBusiness(msisdn, usageDetailsVerifyPinRequest,
			usageDetailsVerifyPinResponse);

	    } else {
		usageDetailsVerifyPinResponse.setCallStatus(Constants.Call_Status_False);
		usageDetailsVerifyPinResponse.setResultCode(Constants.VALIDATION_FAILURE_CODE);
		usageDetailsVerifyPinResponse.setResultDesc(requestValidationStatus);
	    }

	    usageDetailsVerifyPinResponse.getLogsReport()
		    .setResponseCode(usageDetailsVerifyPinResponse.getResultCode());
	    usageDetailsVerifyPinResponse.getLogsReport().setRequestTime(requestTime);
	    usageDetailsVerifyPinResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
	    usageDetailsVerifyPinResponse.getLogsReport()
		    .setResponse(mapper.writeValueAsString(usageDetailsVerifyPinResponse));

	    // Sending report log into queue.
	    Utilities.prepareLogReportForQueue(usageDetailsVerifyPinResponse.getLogsReport());

	    Utilities.printInfoLog(msisdn + "-Response Returned from-" + TRANSACTION_NAME + "-"
		    + mapper.writeValueAsString(usageDetailsVerifyPinResponse), logger);
	} catch (Exception e) {
	    Utilities.printErrorLog(Utilities.convertStackTraceToString(e), logger);
	    usageDetailsVerifyPinResponse.setCallStatus(Constants.Call_Status_False);

	    usageDetailsVerifyPinResponse.setResultCode(Constants.EXCEPTION_CODE);
	    usageDetailsVerifyPinResponse
		    .setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("unexpected.error", lang));

	    usageDetailsVerifyPinResponse.getLogsReport()
		    .setResponseCode(usageDetailsVerifyPinResponse.getResultCode());
	    usageDetailsVerifyPinResponse.getLogsReport().setRequestTime(requestTime);
	    usageDetailsVerifyPinResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
	    usageDetailsVerifyPinResponse.getLogsReport()
		    .setResponse(mapper.writeValueAsString(usageDetailsVerifyPinResponse));

	    // Sending report log into queue.
	    Utilities.prepareLogReportForQueue(usageDetailsVerifyPinResponse.getLogsReport());

	}
	return usageDetailsVerifyPinResponse;
    }

}
