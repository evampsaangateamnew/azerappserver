package com.evampsaanga.azerfon.controllers;

import java.io.IOException;
import java.sql.SQLException;

import javax.jms.JMSException;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.evampsaanga.azerfon.business.ActionHistoryBusiness;
import com.evampsaanga.azerfon.common.messagesmappings.GetMessagesMappings;
import com.evampsaanga.azerfon.common.utilities.Constants;
import com.evampsaanga.azerfon.common.utilities.Transactions;
import com.evampsaanga.azerfon.common.utilities.Utilities;
import com.evampsaanga.azerfon.common.utilities.Validator;
import com.evampsaanga.azerfon.models.actionhistory.ActionHistoryRequest;
import com.evampsaanga.azerfon.models.actionhistory.ActionHistoryResponse;

/**
 * @author Aqeel Abbas
 *
 */
@RestController
@RequestMapping("/actionhistory")
public class ActionHistoryControllerV2 {
	Logger logger = Logger.getLogger(AppMenuControllerV2.class);

	@RequestMapping(value = "/getactionhistory", method = RequestMethod.POST, headers = Constants.CONTENT_TYPE_DEFAULT_VALUE)
	public ActionHistoryResponse getAppMenu(@RequestBody String data,
			@RequestHeader(value = Constants.MSISDN_KEY, defaultValue = Constants.MSISDN_DEFAULT_VALUE) String msisdn,
			@RequestHeader(value = Constants.LANGUAGE_KEY, defaultValue = Constants.LANGUAGE_DEFAULT_VALUE) String lang,
			@RequestHeader(value = Constants.USER_AGENT_KEY, defaultValue = Constants.USER_AGENT_DEFAULT_VALUE) String userAgent,
			@RequestHeader(value = Constants.SUBSCRIBER_TYPE_KEY, defaultValue = Constants.SUBSCRIBER_TYPE_DEFAULT_VALUE) String userType,
			@RequestHeader(value = Constants.TARIFF_TYPE_KEY, defaultValue = Constants.TARIFF_TYPE_DEFAULT_VALUE) String tariffType,
			@RequestHeader(value = Constants.APP_PHASE_KEY, defaultValue = Constants.APP_PHASE_DEFAULT_VALUE) String isFromB2B,
			HttpServletRequest servletRequest) throws ClassNotFoundException, SQLException, JsonParseException,
			JsonMappingException, IOException, JMSException {
		
		ObjectMapper mapper = new ObjectMapper();
		ActionHistoryBusiness actionHistoryBusiness = new ActionHistoryBusiness();
		ActionHistoryRequest request = new ActionHistoryRequest();
		ActionHistoryResponse response = new ActionHistoryResponse();

		String requestTime = Utilities.getReportDateTime();
		String TRANSACTION_NAME = Transactions.ACTION_HISTORY_TRANSACTION_NAME + " CONTROLLER";
		Utilities.printInfoLog(msisdn + "-Request Landed in-" + TRANSACTION_NAME, logger);
		Utilities.printInfoLog(msisdn + "-Request Data-" + "", logger);
		try {
			response.setLogsReport(
					Utilities.preBusinessReportLoggingData("", msisdn, TRANSACTION_NAME, servletRequest.getRemoteAddr(),
							userAgent, userType, tariffType, "", lang, response.getLogsReport()));
			data = Utilities.addAttributesToRequest(data, msisdn, servletRequest, lang, userAgent);
			/*
			 * request.setChannel(userAgent); request.setMsisdn(msisdn);
			 * request.setiP(servletRequest.getRemoteAddr()); request.setLang(lang);
			 */
			request = mapper.readValue(data, ActionHistoryRequest.class);
			String requestValidationStatus = Validator.validateRequest(msisdn, request);
			logger.info("<<<<  Validation Status >>>>:" + requestValidationStatus + " -- " + msisdn);
			if (msisdn.equals("Unknow Msisdn") || msisdn == null) {
				requestValidationStatus = "Unknow Msisdn";
			}
			if (requestValidationStatus.equalsIgnoreCase(Constants.VALIDATION_SUCCESS_CODE)) {

				response = actionHistoryBusiness.getActionHistory(msisdn, request, response);

			} else {
				response.setCallStatus(Constants.Call_Status_False);
				response.setResultCode(Constants.VALIDATION_FAILURE_CODE);
				response.setResultDesc(requestValidationStatus);
			}
			response.getLogsReport().setRequestTime(requestTime);
			response.getLogsReport().setResponseTime(Utilities.getReportDateTime());
			response.getLogsReport().setResponse(mapper.writeValueAsString(response));
			response.getLogsReport().setResponseCode(response.getResultCode());

			// Sending report log into queue.
			response.getLogsReport().setAppVersion(Constants.APP_VERSION_PHASE2);
			Utilities.prepareLogReportForQueue(response.getLogsReport());

			Utilities.printInfoLog(
					msisdn + "-Response Returned from-" + TRANSACTION_NAME + "-" + mapper.writeValueAsString(response),
					logger);
		} catch (Exception e) {
			Utilities.printErrorLog(Utilities.convertStackTraceToString(e), logger);
			response.setCallStatus(Constants.Call_Status_False);
			response.setResultCode(Constants.EXCEPTION_CODE);
			response.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("unexpected.error", lang));

			response.getLogsReport().setResponseCode(response.getResultCode());
			response.getLogsReport().setRequestTime(requestTime);
			response.getLogsReport().setResponseTime(Utilities.getReportDateTime());
			response.getLogsReport().setResponse(mapper.writeValueAsString(response));

			// Sending report log into queue.
			response.getLogsReport().setAppVersion(Constants.APP_VERSION_PHASE2);
			Utilities.prepareLogReportForQueue(response.getLogsReport());
		}
		return response;
	}
}
