/**
 * 
 */
package com.evampsaanga.azerfon.controllers;

import java.io.IOException;
import java.sql.SQLException;

import javax.jms.JMSException;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONObject;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.evampsaanga.azerfon.models.companyinvoice.CompanyInvoiceRequest;
import com.evampsaanga.azerfon.models.companyinvoice.CompanyInvoiceResponse;
import com.evampsaanga.azerfon.business.GeneralServicesBusiness;
import com.evampsaanga.azerfon.common.messagesmappings.GetMessagesMappings;
import com.evampsaanga.azerfon.common.utilities.Constants;
import com.evampsaanga.azerfon.common.utilities.GetConfigurations;
import com.evampsaanga.azerfon.common.utilities.Transactions;
import com.evampsaanga.azerfon.common.utilities.Utilities;
import com.evampsaanga.azerfon.common.utilities.Validator;
import com.evampsaanga.azerfon.models.acceptTNC.AcceptTnCRequest;
import com.evampsaanga.azerfon.models.acceptTNC.AcceptTnCResponse;
import com.evampsaanga.azerfon.models.generalservices.contactus.ContactUsRequest;
import com.evampsaanga.azerfon.models.generalservices.contactus.ContactUsResponse;
import com.evampsaanga.azerfon.models.generalservices.faqs.FAQSRequest;
import com.evampsaanga.azerfon.models.generalservices.faqs.FAQSResponse;
import com.evampsaanga.azerfon.models.generalservices.lostreport.ReportLostSIMRequest;
import com.evampsaanga.azerfon.models.generalservices.lostreport.ReportLostSIMResponse;
import com.evampsaanga.azerfon.models.generalservices.storelocator.StoreLocatorRequest;
import com.evampsaanga.azerfon.models.generalservices.storelocator.StoreLocatorResponse;
import com.evampsaanga.azerfon.models.generalservices.uploadimage.UploadImageRequest;
import com.evampsaanga.azerfon.models.generalservices.uploadimage.UploadImageResponse;
import com.evampsaanga.azerfon.models.msisdninvoice.MSISDNDetailsResponse;
import com.evampsaanga.azerfon.models.msisdninvoice.MSISDNInvoiceRequest;
import com.evampsaanga.azerfon.models.msisdninvoice.MSISDNSummaryResponse;
import com.evampsaanga.azerfon.models.simswap.SimSwapRequest;
import com.evampsaanga.azerfon.models.simswap.SimSwapResponse;
import com.evampsaanga.azerfon.models.verifyappversion.VerifyAppVersionRequest;
import com.evampsaanga.azerfon.models.verifyappversion.VerifyAppVersionResponse;
import com.evampsaanga.azerfon.models.verifyappversion.VerifyAppVersionResponseData;

/**
 * @author Aqeel Abbas
 *
 */
@RestController
@RequestMapping("/generalservicesV2")
public class GeneralServicesControllerV2 {

	Logger logger = Logger.getLogger(GeneralServicesControllerV2.class);

	@RequestMapping(value = "/getfaqs", method = RequestMethod.POST, headers = Constants.CONTENT_TYPE_DEFAULT_VALUE)
	public FAQSResponse getFAQs(@RequestBody String data,
			@RequestHeader(value = Constants.MSISDN_KEY, defaultValue = Constants.MSISDN_DEFAULT_VALUE) String msisdn,
			@RequestHeader(value = Constants.LANGUAGE_KEY, defaultValue = Constants.LANGUAGE_DEFAULT_VALUE) String lang,
			@RequestHeader(value = Constants.USER_AGENT_KEY, defaultValue = Constants.USER_AGENT_DEFAULT_VALUE) String userAgent,
			@RequestHeader(value = Constants.SUBSCRIBER_TYPE_KEY, defaultValue = Constants.SUBSCRIBER_TYPE_DEFAULT_VALUE) String userType,
			@RequestHeader(value = Constants.TARIFF_TYPE_KEY, defaultValue = Constants.TARIFF_TYPE_DEFAULT_VALUE) String tariffType,
			@RequestHeader(value = Constants.APP_PHASE_KEY, defaultValue = Constants.APP_PHASE_DEFAULT_VALUE) String isFromB2B,
			HttpServletRequest servletRequest) throws ClassNotFoundException, SQLException, JsonParseException,
			JsonMappingException, IOException, JMSException {
		ObjectMapper mapper = new ObjectMapper();
		JSONObject jsonObject = new JSONObject();
		GeneralServicesBusiness generalServicesBusiness = new GeneralServicesBusiness();
		FAQSRequest faqsRequest = new FAQSRequest();
		FAQSResponse faqsResponse = new FAQSResponse();

		String TRANSACTION_NAME = Transactions.APP_FAQ_TRANSACTION_NAME + " CONTROLLER";
		String requestTime = Utilities.getReportDateTime();
		try {
			data = jsonObject.toString(); // use it when there is no request
			// body required.

			// Populating report object before processing business logic.
			faqsResponse.setLogsReport(
					Utilities.preBusinessReportLoggingData("", msisdn, TRANSACTION_NAME, servletRequest.getRemoteAddr(),
							userAgent, userType, tariffType, data, lang, faqsResponse.getLogsReport()));

			data = Utilities.addAttributesToRequest(data, msisdn, servletRequest, lang, userAgent);

			Utilities.printInfoLog(msisdn + "-Request Landed in-" + TRANSACTION_NAME, logger);
			Utilities.printInfoLog(msisdn + "-Request Data-" + data, logger);

			faqsRequest = mapper.readValue(data, FAQSRequest.class);

			String requestValidationStatus = Validator.validateRequest(msisdn, faqsRequest);

			if (requestValidationStatus.equalsIgnoreCase(Constants.VALIDATION_SUCCESS_CODE)) {
				faqsRequest.setIsB2B(isFromB2B);
				faqsResponse = generalServicesBusiness.faqsBusinessV2(msisdn, faqsRequest, faqsResponse);

			} else {
				faqsResponse.setCallStatus(Constants.Call_Status_False);
				faqsResponse.setResultCode(Constants.VALIDATION_FAILURE_CODE);
				faqsResponse.setResultDesc(requestValidationStatus);
			}

			faqsResponse.getLogsReport().setResponseCode(faqsResponse.getResultCode());
			faqsResponse.getLogsReport().setRequestTime(requestTime);
			faqsResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
			faqsResponse.getLogsReport().setResponse(mapper.writeValueAsString(faqsResponse));

			// Sending report log into queue.
			faqsResponse.getLogsReport().setAppVersion(Constants.APP_VERSION_PHASE2);
			Utilities.prepareLogReportForQueue(faqsResponse.getLogsReport());

			Utilities.printInfoLog(msisdn + "-Response Returned from-" + TRANSACTION_NAME + "-"
					+ mapper.writeValueAsString(faqsResponse), logger);
		} catch (Exception e) {
			Utilities.printErrorLog(Utilities.convertStackTraceToString(e), logger);
			faqsResponse.setCallStatus(Constants.Call_Status_False);

			faqsResponse.setResultCode(Constants.EXCEPTION_CODE);
			faqsResponse.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("unexpected.error", lang));

			faqsResponse.getLogsReport().setResponseCode(faqsResponse.getResultCode());
			faqsResponse.getLogsReport().setRequestTime(requestTime);
			faqsResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
			faqsResponse.getLogsReport().setResponse(mapper.writeValueAsString(faqsResponse));

			// Sending report log into queue.
			faqsResponse.getLogsReport().setAppVersion(Constants.APP_VERSION_PHASE2);
			Utilities.prepareLogReportForQueue(faqsResponse.getLogsReport());
		}
		return faqsResponse;
	}

	// for phase 2 accept terms and conditions
	@RequestMapping(value = "/acceptTnC", method = RequestMethod.POST, headers = Constants.CONTENT_TYPE_DEFAULT_VALUE)
	public AcceptTnCResponse acceptTermsnConditions(@RequestBody String data,
			@RequestHeader(value = Constants.MSISDN_KEY, defaultValue = Constants.MSISDN_DEFAULT_VALUE) String msisdn,
			@RequestHeader(value = Constants.LANGUAGE_KEY, defaultValue = Constants.LANGUAGE_DEFAULT_VALUE) String lang,
			@RequestHeader(value = Constants.USER_AGENT_KEY, defaultValue = Constants.USER_AGENT_DEFAULT_VALUE) String userAgent,
			@RequestHeader(value = Constants.SUBSCRIBER_TYPE_KEY, defaultValue = Constants.SUBSCRIBER_TYPE_DEFAULT_VALUE) String userType,
			@RequestHeader(value = Constants.TARIFF_TYPE_KEY, defaultValue = Constants.TARIFF_TYPE_DEFAULT_VALUE) String tariffType,
			@RequestHeader(value = Constants.APP_PHASE_KEY, defaultValue = Constants.APP_PHASE_DEFAULT_VALUE) String isFromB2B,
			HttpServletRequest servletRequest) throws ClassNotFoundException, SQLException, JsonParseException,
			JsonMappingException, IOException, JMSException {
		// variable declaration
		ObjectMapper mapper = new ObjectMapper();
		GeneralServicesBusiness generalServicesBusiness = new GeneralServicesBusiness();
		AcceptTnCRequest acceptTnCRequest = new AcceptTnCRequest();
		AcceptTnCResponse acceptTnCResponse = new AcceptTnCResponse();
		String TRANSACTION_NAME = Transactions.ACCEPT_TNC_NAME + " CONTROLLER";
		String requestTime = Utilities.getReportDateTime();

		try {
			// Populating report object before processing business logic.
			acceptTnCResponse.setLogsReport(
					Utilities.preBusinessReportLoggingData("", msisdn, TRANSACTION_NAME, servletRequest.getRemoteAddr(),
							userAgent, userType, tariffType, data, lang, acceptTnCResponse.getLogsReport()));
			data = Utilities.addAttributesToRequest(data, msisdn, servletRequest, lang, userAgent);
			// printing logs
			Utilities.printInfoLog(msisdn + "-Request Landed in-" + TRANSACTION_NAME, logger);
			Utilities.printInfoLog(msisdn + "-Request Data-" + data, logger);
			// Mapping bean
			acceptTnCRequest = mapper.readValue(data, AcceptTnCRequest.class);
			// validating request packet
			String requestValidationStatus = Validator.validateRequest(msisdn, acceptTnCRequest);
			if (requestValidationStatus.equalsIgnoreCase(Constants.VALIDATION_SUCCESS_CODE)) {
				// calling business logic
				acceptTnCRequest.setIsB2B(isFromB2B);
				acceptTnCResponse = generalServicesBusiness.AccpetTnCBusiness(msisdn, acceptTnCRequest,
						acceptTnCResponse);
				acceptTnCResponse.getLogsReport().setResponseCode(acceptTnCResponse.getResultCode());
				acceptTnCResponse.getLogsReport().setRequestTime(requestTime);
				acceptTnCResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
				acceptTnCResponse.getLogsReport().setResponse(mapper.writeValueAsString(acceptTnCResponse));
				// Sending report log into queue.
				acceptTnCResponse.getLogsReport().setAppVersion(Constants.APP_VERSION_PHASE2);
				Utilities.prepareLogReportForQueue(acceptTnCResponse.getLogsReport());
				Utilities.printInfoLog(msisdn + "-Response Returned from-" + TRANSACTION_NAME + "-"
						+ mapper.writeValueAsString(acceptTnCResponse), logger);
				return acceptTnCResponse;
			} else {
				logger.error("ERROR: <<<<< Validation of Request Packet Failed >>>>>");
				acceptTnCResponse.setCallStatus(Constants.Call_Status_False);
				acceptTnCResponse.setResultCode(Constants.VALIDATION_FAILURE_CODE);
				acceptTnCResponse.setResultDesc(requestValidationStatus);
			}

		} catch (Exception e) {
			Utilities.printErrorLog(Utilities.convertStackTraceToString(e), logger);
			acceptTnCResponse.setCallStatus(Constants.Call_Status_False);
			acceptTnCResponse.setResultCode(Constants.EXCEPTION_CODE);
			acceptTnCResponse.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("unexpected.error", lang));
			acceptTnCResponse.getLogsReport().setResponseCode(acceptTnCResponse.getResultCode());
			acceptTnCResponse.getLogsReport().setRequestTime(requestTime);
			acceptTnCResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
			acceptTnCResponse.getLogsReport().setResponse(mapper.writeValueAsString(acceptTnCResponse));

			// Sending report log into queue.
			acceptTnCResponse.getLogsReport().setAppVersion(Constants.APP_VERSION_PHASE2);
			Utilities.prepareLogReportForQueue(acceptTnCResponse.getLogsReport());
		}
		return acceptTnCResponse;
	}

	@RequestMapping(value = "/getcontactusdetails", method = RequestMethod.POST, headers = Constants.CONTENT_TYPE_DEFAULT_VALUE)
	public ContactUsResponse getContactUsDetails(@RequestBody String data,
			@RequestHeader(value = Constants.MSISDN_KEY, defaultValue = Constants.MSISDN_DEFAULT_VALUE) String msisdn,
			@RequestHeader(value = Constants.LANGUAGE_KEY, defaultValue = Constants.LANGUAGE_DEFAULT_VALUE) String lang,
			@RequestHeader(value = Constants.USER_AGENT_KEY, defaultValue = Constants.USER_AGENT_DEFAULT_VALUE) String userAgent,
			@RequestHeader(value = Constants.SUBSCRIBER_TYPE_KEY, defaultValue = Constants.SUBSCRIBER_TYPE_DEFAULT_VALUE) String userType,
			@RequestHeader(value = Constants.TARIFF_TYPE_KEY, defaultValue = Constants.TARIFF_TYPE_DEFAULT_VALUE) String tariffType,
			@RequestHeader(value = Constants.APP_PHASE_KEY, defaultValue = Constants.APP_PHASE_DEFAULT_VALUE) String isFromB2B,
			HttpServletRequest servletRequest) throws ClassNotFoundException, SQLException, JsonParseException,
			JsonMappingException, IOException, JMSException {
		ObjectMapper mapper = new ObjectMapper();
		JSONObject jsonObject = new JSONObject();
		GeneralServicesBusiness generalServicesBusiness = new GeneralServicesBusiness();
		ContactUsRequest contactUsRequest = new ContactUsRequest();
		ContactUsResponse contactUsResponse = new ContactUsResponse();

		String TRANSACTION_NAME = Transactions.CONTACTUS__TRANSACTION_NAME + " CONTROLLER";
		String requestTime = Utilities.getReportDateTime();
		try {
			data = jsonObject.toString();
			/*
			 * String msisdn = Utilities.getValueFromJSON(Utilities.splitToken(Utilities.
			 * decodeString(token)).toString(), Constants.MSISDN_KEY);
			 */

			// Populating report object before processing business logic.
			contactUsResponse.setLogsReport(
					Utilities.preBusinessReportLoggingData("", msisdn, TRANSACTION_NAME, servletRequest.getRemoteAddr(),
							userAgent, userType, tariffType, data, lang, contactUsResponse.getLogsReport()));

			data = Utilities.addAttributesToRequest(data, msisdn, servletRequest, lang, userAgent);

			Utilities.printInfoLog(msisdn + "-Request Landed in-" + TRANSACTION_NAME, logger);
			Utilities.printInfoLog(msisdn + "-Request Data-" + data, logger);

			contactUsRequest = mapper.readValue(data, ContactUsRequest.class);
			// Store ID value is same as of Language ID.
			contactUsRequest.setStoreId(contactUsRequest.getLang());

			String requestValidationStatus = Validator.validateRequest(msisdn, contactUsRequest);

			if (requestValidationStatus.equalsIgnoreCase(Constants.VALIDATION_SUCCESS_CODE)) {

				contactUsRequest.setIsB2B(isFromB2B);
				contactUsResponse = generalServicesBusiness.contactUsBusiness(msisdn, contactUsRequest,
						contactUsResponse);

			} else {
				logger.error("ERROR: <<<<< Validation of Request Packet Failed >>>>>");
				contactUsResponse.setCallStatus(Constants.Call_Status_False);
				contactUsResponse.setResultCode(Constants.VALIDATION_FAILURE_CODE);
				contactUsResponse.setResultDesc(requestValidationStatus);
			}

			contactUsResponse.getLogsReport().setResponseCode(contactUsResponse.getResultCode());
			contactUsResponse.getLogsReport().setRequestTime(requestTime);
			contactUsResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
			contactUsResponse.getLogsReport().setResponse(mapper.writeValueAsString(contactUsResponse));

			// Sending report log into queue.
			contactUsResponse.getLogsReport().setAppVersion(Constants.APP_VERSION_PHASE2);
			Utilities.prepareLogReportForQueue(contactUsResponse.getLogsReport());

			Utilities.printInfoLog(msisdn + "-Response Returned from-" + TRANSACTION_NAME + "-"
					+ mapper.writeValueAsString(contactUsResponse), logger);
		} catch (Exception e) {
			Utilities.printErrorLog(Utilities.convertStackTraceToString(e), logger);
			contactUsResponse.setCallStatus(Constants.Call_Status_False);

			contactUsResponse.setResultCode(Constants.EXCEPTION_CODE);
			contactUsResponse.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("unexpected.error", lang));

			contactUsResponse.getLogsReport().setResponseCode(contactUsResponse.getResultCode());
			contactUsResponse.getLogsReport().setRequestTime(requestTime);
			contactUsResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
			contactUsResponse.getLogsReport().setResponse(mapper.writeValueAsString(contactUsResponse));

			// Sending report log into queue.
			contactUsResponse.getLogsReport().setAppVersion(Constants.APP_VERSION_PHASE2);
			Utilities.prepareLogReportForQueue(contactUsResponse.getLogsReport());

		}
		return contactUsResponse;
	}

	@RequestMapping(value = "/getstoresdetails", method = RequestMethod.POST, headers = Constants.CONTENT_TYPE_DEFAULT_VALUE)
	public StoreLocatorResponse getStoreLocatorDetails(@RequestBody String data,
			@RequestHeader(value = Constants.MSISDN_KEY, defaultValue = Constants.MSISDN_DEFAULT_VALUE) String msisdn,
			@RequestHeader(value = Constants.LANGUAGE_KEY, defaultValue = Constants.LANGUAGE_DEFAULT_VALUE) String lang,
			@RequestHeader(value = Constants.USER_AGENT_KEY, defaultValue = Constants.USER_AGENT_DEFAULT_VALUE) String userAgent,
			@RequestHeader(value = Constants.SUBSCRIBER_TYPE_KEY, defaultValue = Constants.SUBSCRIBER_TYPE_DEFAULT_VALUE) String userType,
			@RequestHeader(value = Constants.TARIFF_TYPE_KEY, defaultValue = Constants.TARIFF_TYPE_DEFAULT_VALUE) String tariffType,
			@RequestHeader(value = Constants.APP_PHASE_KEY, defaultValue = Constants.APP_PHASE_DEFAULT_VALUE) String isFromB2B,
			HttpServletRequest servletRequest) throws ClassNotFoundException, SQLException, JsonParseException,
			JsonMappingException, IOException, JMSException {

		ObjectMapper mapper = new ObjectMapper();
		JSONObject jsonObject = new JSONObject();
		GeneralServicesBusiness generalServicesBusiness = new GeneralServicesBusiness();
		StoreLocatorRequest storeLocatorRequest = new StoreLocatorRequest();
		StoreLocatorResponse storeLocatorResponse = new StoreLocatorResponse();

		String TRANSACTION_NAME = Transactions.STORE_LOCATOR_TRANSACTION_NAME + " CONTROLLER";
		String requestTime = Utilities.getReportDateTime();
		try {
			data = jsonObject.toString(); // use it when there is no request
			// body required.

			/*
			 * String msisdn = Utilities.getValueFromJSON(Utilities.splitToken(Utilities.
			 * decodeString(token)).toString(), Constants.MSISDN_KEY);
			 */

			// Populating report object before processing business logic.
			storeLocatorResponse.setLogsReport(
					Utilities.preBusinessReportLoggingData("", msisdn, TRANSACTION_NAME, servletRequest.getRemoteAddr(),
							userAgent, userType, tariffType, data, lang, storeLocatorResponse.getLogsReport()));

			data = Utilities.addAttributesToRequest(data, msisdn, servletRequest, lang, userAgent);

			Utilities.printInfoLog(msisdn + "-Request Landed in-" + TRANSACTION_NAME, logger);
			Utilities.printInfoLog(msisdn + "-Request Data-" + data, logger);

			storeLocatorRequest = mapper.readValue(data, StoreLocatorRequest.class);

			String requestValidationStatus = Validator.validateRequest(msisdn, storeLocatorRequest);
			storeLocatorRequest.setIsB2B(isFromB2B);
			if (requestValidationStatus.equalsIgnoreCase(Constants.VALIDATION_SUCCESS_CODE)) {

				storeLocatorResponse = generalServicesBusiness.getStoreLocatorsDetailsBusinessV2(msisdn,
						storeLocatorRequest, storeLocatorResponse);
			} else {
				storeLocatorResponse.setCallStatus(Constants.Call_Status_False);
				storeLocatorResponse.setResultCode(Constants.VALIDATION_FAILURE_CODE);
				storeLocatorResponse.setResultDesc(requestValidationStatus);
			}

			storeLocatorResponse.getLogsReport().setResponseCode(storeLocatorResponse.getResultCode());
			storeLocatorResponse.getLogsReport().setRequestTime(requestTime);
			storeLocatorResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
			storeLocatorResponse.getLogsReport().setResponse(mapper.writeValueAsString(storeLocatorResponse));

			// Sending report log into queue.
			storeLocatorResponse.getLogsReport().setAppVersion(Constants.APP_VERSION_PHASE2);
			Utilities.prepareLogReportForQueue(storeLocatorResponse.getLogsReport());
			Utilities.printInfoLog(msisdn + "-Response Returned from-" + TRANSACTION_NAME + "-"
					+ mapper.writeValueAsString(storeLocatorResponse), logger);
		} catch (Exception e) {
			Utilities.printErrorLog(Utilities.convertStackTraceToString(e), logger);
			storeLocatorResponse.setCallStatus(Constants.Call_Status_False);

			storeLocatorResponse.setResultCode(Constants.EXCEPTION_CODE);
			storeLocatorResponse
					.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("unexpected.error", lang));

			storeLocatorResponse.getLogsReport().setResponseCode(storeLocatorResponse.getResultCode());
			storeLocatorResponse.getLogsReport().setRequestTime(requestTime);
			storeLocatorResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
			storeLocatorResponse.getLogsReport().setResponse(mapper.writeValueAsString(storeLocatorResponse));

			// Sending report log into queue.
			storeLocatorResponse.getLogsReport().setAppVersion(Constants.APP_VERSION_PHASE2);
			Utilities.prepareLogReportForQueue(storeLocatorResponse.getLogsReport());
		}

		return storeLocatorResponse;
	}

	@RequestMapping(value = "/reportlostsim", method = RequestMethod.POST, headers = Constants.CONTENT_TYPE_DEFAULT_VALUE)
	public ReportLostSIMResponse reportlostsim(@RequestBody String data,
			@RequestHeader(value = Constants.MSISDN_KEY, defaultValue = Constants.MSISDN_DEFAULT_VALUE) String msisdn,
			@RequestHeader(value = Constants.LANGUAGE_KEY, defaultValue = Constants.LANGUAGE_DEFAULT_VALUE) String lang,
			@RequestHeader(value = Constants.USER_AGENT_KEY, defaultValue = Constants.USER_AGENT_DEFAULT_VALUE) String userAgent,
			@RequestHeader(value = Constants.SUBSCRIBER_TYPE_KEY, defaultValue = Constants.SUBSCRIBER_TYPE_DEFAULT_VALUE) String userType,
			@RequestHeader(value = Constants.TARIFF_TYPE_KEY, defaultValue = Constants.TARIFF_TYPE_DEFAULT_VALUE) String tariffType,
			@RequestHeader(value = Constants.APP_PHASE_KEY, defaultValue = Constants.APP_PHASE_DEFAULT_VALUE) String isFromB2B,
			HttpServletRequest servletRequest) throws ClassNotFoundException, SQLException, JsonParseException,
			JsonMappingException, IOException, JMSException {
		ObjectMapper mapper = new ObjectMapper();

		GeneralServicesBusiness generalServicesBusiness = new GeneralServicesBusiness();
		ReportLostSIMRequest reportLostSIMRequest = new ReportLostSIMRequest();
		ReportLostSIMResponse reportLostSIMResponse = new ReportLostSIMResponse();

		String TRANSACTION_NAME = Transactions.REPORT_LOST_SIM_TRANSACTION_NAME + " CONTROLLER";
		String requestTime = Utilities.getReportDateTime();
		try {
			// JSONObject jObj = new JSONObject(data);

			// Populating report object before processing business logic.
			reportLostSIMResponse.setLogsReport(
					Utilities.preBusinessReportLoggingData("", msisdn, TRANSACTION_NAME, servletRequest.getRemoteAddr(),
							userAgent, userType, tariffType, data, lang, reportLostSIMResponse.getLogsReport()));

			data = Utilities.addAttributesToRequest(data, msisdn, servletRequest, lang, userAgent);

			Utilities.printInfoLog(msisdn + "-Request Landed in-" + TRANSACTION_NAME, logger);
			Utilities.printInfoLog(msisdn + "-Request Data-" + data, logger);
			JSONObject jObject = new JSONObject(data);
			if (jObject.has("individualMsisdn")) {
				jObject.remove("msisdn");
				jObject.put("msisdn", jObject.get("individualMsisdn"));
				jObject.remove("individualMsisdn");
			}
			reportLostSIMRequest = mapper.readValue(jObject.toString(), ReportLostSIMRequest.class);

			String requestValidationStatus = Validator.validateRequest(msisdn, reportLostSIMRequest);

			if (requestValidationStatus.equalsIgnoreCase(Constants.VALIDATION_SUCCESS_CODE)) {
				reportLostSIMRequest.setIsB2B(isFromB2B);
				reportLostSIMResponse = generalServicesBusiness.reportLostSIMBusiness(msisdn, reportLostSIMRequest,
						reportLostSIMResponse);
			} else {
				reportLostSIMResponse.setCallStatus(Constants.Call_Status_False);
				reportLostSIMResponse.setResultCode(Constants.VALIDATION_FAILURE_CODE);
				reportLostSIMResponse.setResultDesc(requestValidationStatus);
			}

			reportLostSIMResponse.getLogsReport().setResponseCode(reportLostSIMResponse.getResultCode());
			reportLostSIMResponse.getLogsReport().setRequestTime(requestTime);
			reportLostSIMResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
			reportLostSIMResponse.getLogsReport().setResponse(mapper.writeValueAsString(reportLostSIMResponse));

			// Sending report log into queue.
			reportLostSIMResponse.getLogsReport().setAppVersion(Constants.APP_VERSION_PHASE2);
			Utilities.prepareLogReportForQueue(reportLostSIMResponse.getLogsReport());

			Utilities.printInfoLog(msisdn + "-Response Returned from-" + TRANSACTION_NAME + "-"
					+ mapper.writeValueAsString(reportLostSIMResponse), logger);
		} catch (Exception e) {
			Utilities.printErrorLog(Utilities.convertStackTraceToString(e), logger);
			reportLostSIMResponse.setCallStatus(Constants.Call_Status_False);

			reportLostSIMResponse.setResultCode(Constants.EXCEPTION_CODE);
			reportLostSIMResponse
					.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("unexpected.error", lang));

			reportLostSIMResponse.getLogsReport().setResponseCode(reportLostSIMResponse.getResultCode());
			reportLostSIMResponse.getLogsReport().setRequestTime(requestTime);
			reportLostSIMResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
			reportLostSIMResponse.getLogsReport().setResponse(mapper.writeValueAsString(reportLostSIMResponse));

			// Sending report log into queue.
			reportLostSIMResponse.getLogsReport().setAppVersion(Constants.APP_VERSION_PHASE2);
			Utilities.prepareLogReportForQueue(reportLostSIMResponse.getLogsReport());
		}
		return reportLostSIMResponse;
	}

	@RequestMapping(value = "/uploadimage", method = RequestMethod.POST, headers = Constants.CONTENT_TYPE_DEFAULT_VALUE)
	public UploadImageResponse uploadProfilePicture(@RequestBody String data,
			@RequestHeader(value = Constants.MSISDN_KEY, defaultValue = Constants.MSISDN_DEFAULT_VALUE) String msisdn,
			@RequestHeader(value = Constants.LANGUAGE_KEY, defaultValue = Constants.LANGUAGE_DEFAULT_VALUE) String lang,
			@RequestHeader(value = Constants.USER_AGENT_KEY, defaultValue = Constants.USER_AGENT_DEFAULT_VALUE) String userAgent,
			@RequestHeader(value = Constants.SUBSCRIBER_TYPE_KEY, defaultValue = Constants.SUBSCRIBER_TYPE_DEFAULT_VALUE) String userType,
			@RequestHeader(value = Constants.TARIFF_TYPE_KEY, defaultValue = Constants.TARIFF_TYPE_DEFAULT_VALUE) String tariffType,
			@RequestHeader(value = Constants.APP_PHASE_KEY, defaultValue = Constants.APP_PHASE_DEFAULT_VALUE) String isFromB2B,
			HttpServletRequest servletRequest) throws ClassNotFoundException, SQLException, IOException, JMSException {
		ObjectMapper mapper = new ObjectMapper();

		GeneralServicesBusiness generalServicesBusiness = new GeneralServicesBusiness();
		UploadImageRequest uploadImageRequest = new UploadImageRequest();
		UploadImageResponse uploadImageResponse = new UploadImageResponse();

		String TRANSACTION_NAME = Transactions.UPLOAD_IMAGE_TRANSACTION_NAME + " CONTROLLER";
		String requestTime = Utilities.getReportDateTime();
		try {
			/*
			 * String msisdn = Utilities.getValueFromJSON(Utilities.splitToken(Utilities.
			 * decodeString(token)).toString(), Constants.MSISDN_KEY);
			 */

			// Populating report object before processing business logic.
			uploadImageResponse.setLogsReport(
					Utilities.preBusinessReportLoggingData("", msisdn, TRANSACTION_NAME, servletRequest.getRemoteAddr(),
							userAgent, userType, tariffType, data, lang, uploadImageResponse.getLogsReport()));

			data = Utilities.addAttributesToRequest(data, msisdn, servletRequest, lang, userAgent);

			Utilities.printInfoLog(msisdn + "-Request Landed in-" + TRANSACTION_NAME, logger);
			String dataWithImage = data;
			data = Utilities.removeParamsFromJSONObject(data, "image");
			Utilities.printInfoLog(msisdn + "-Request Data-" + data, logger);

			uploadImageRequest = mapper.readValue(dataWithImage, UploadImageRequest.class);

			String requestValidationStatus = Validator.validateRequest(msisdn, uploadImageRequest);

			if (requestValidationStatus.equalsIgnoreCase(Constants.VALIDATION_SUCCESS_CODE)) {

				uploadImageRequest.setIsB2B(isFromB2B);
				uploadImageResponse = generalServicesBusiness.uploadImageBusiness(msisdn, uploadImageRequest,
						uploadImageResponse);
			} else {
				uploadImageResponse.setCallStatus(Constants.Call_Status_False);
				uploadImageResponse.setResultCode(Constants.VALIDATION_FAILURE_CODE);
				uploadImageResponse.setResultDesc(requestValidationStatus);
			}

			uploadImageResponse.getLogsReport().setResponseCode(uploadImageResponse.getResultCode());
			uploadImageResponse.getLogsReport().setRequestTime(requestTime);
			uploadImageResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
			uploadImageResponse.getLogsReport().setResponse(mapper.writeValueAsString(uploadImageResponse));

			// Sending report log into queue.
			uploadImageResponse.getLogsReport().setAppVersion(Constants.APP_VERSION_PHASE2);
			Utilities.prepareLogReportForQueue(uploadImageResponse.getLogsReport());

			Utilities.printInfoLog(msisdn + "-Response Returned from-" + TRANSACTION_NAME + "-"
					+ mapper.writeValueAsString(uploadImageResponse), logger);
		} catch (Exception e) {
			Utilities.printErrorLog(Utilities.convertStackTraceToString(e), logger);
			uploadImageResponse.setCallStatus(Constants.Call_Status_False);

			uploadImageResponse.setResultCode(Constants.EXCEPTION_CODE);
			uploadImageResponse
					.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("unexpected.error", lang));

			uploadImageResponse.getLogsReport().setResponseCode(uploadImageResponse.getResultCode());
			uploadImageResponse.getLogsReport().setRequestTime(requestTime);
			uploadImageResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
			uploadImageResponse.getLogsReport().setResponse(mapper.writeValueAsString(uploadImageResponse));

			// Sending report log into queue.
			uploadImageResponse.getLogsReport().setAppVersion(Constants.APP_VERSION_PHASE2);
			Utilities.prepareLogReportForQueue(uploadImageResponse.getLogsReport());
		}
		return uploadImageResponse;
	}

	@RequestMapping(value = "/verifyappversion", method = RequestMethod.POST, headers = Constants.CONTENT_TYPE_DEFAULT_VALUE)
	public VerifyAppVersionResponse verifyAppVersion(@RequestBody String data,
			@RequestHeader(value = Constants.MSISDN_KEY, defaultValue = Constants.MSISDN_DEFAULT_VALUE) String msisdn,
			@RequestHeader(value = Constants.LANGUAGE_KEY, defaultValue = Constants.LANGUAGE_DEFAULT_VALUE) String lang,
			@RequestHeader(value = Constants.USER_AGENT_KEY, defaultValue = Constants.USER_AGENT_DEFAULT_VALUE) String userAgent,
			@RequestHeader(value = Constants.SUBSCRIBER_TYPE_KEY, defaultValue = Constants.SUBSCRIBER_TYPE_DEFAULT_VALUE) String userType,
			@RequestHeader(value = Constants.TARIFF_TYPE_KEY, defaultValue = Constants.TARIFF_TYPE_DEFAULT_VALUE) String tariffType,
			@RequestHeader(value = Constants.APP_PHASE_KEY, defaultValue = Constants.APP_PHASE_DEFAULT_VALUE) String isFromB2B,
			HttpServletRequest servletRequest) throws ClassNotFoundException, SQLException, JsonParseException,
			JsonMappingException, IOException, JMSException {
		ObjectMapper mapper = new ObjectMapper();

		GeneralServicesBusiness generalServicesBusiness = new GeneralServicesBusiness();
		VerifyAppVersionRequest verifyAppVersionRequest = new VerifyAppVersionRequest();
		VerifyAppVersionResponse verifyAppVersionResponse = new VerifyAppVersionResponse();

		String TRANSACTION_NAME = Transactions.VERIFY_APP_VERSION_TRANSACTION_NAME + " CONTROLLER";
		String requestTime = Utilities.getReportDateTime();
		try {

			// msisdn = "Number not applicable.";

			String deviceID = servletRequest.getHeader("deviceID");

			// Populating report object before processing business logic.
			verifyAppVersionResponse.setLogsReport(
					Utilities.preBusinessReportLoggingData("", msisdn, TRANSACTION_NAME, servletRequest.getRemoteAddr(),
							userAgent, "", "", data, lang, verifyAppVersionResponse.getLogsReport()));

			data = Utilities.addAttributesToRequest(data, msisdn, servletRequest, lang, userAgent);

			Utilities.printInfoLog(msisdn + "-Request Landed in-" + TRANSACTION_NAME, logger);
			Utilities.printInfoLog(msisdn + "-Request Data-" + data, logger);

			verifyAppVersionRequest = mapper.readValue(data, VerifyAppVersionRequest.class);

			String requestValidationStatus = Validator.validateRequest(msisdn, verifyAppVersionRequest);

			if (requestValidationStatus.equalsIgnoreCase(Constants.VALIDATION_SUCCESS_CODE)) {
				verifyAppVersionRequest.setIsB2B(isFromB2B);
				verifyAppVersionResponse = generalServicesBusiness.verifyAppVersionBusiness(msisdn, deviceID,
						verifyAppVersionRequest, verifyAppVersionResponse);
				if (verifyAppVersionResponse.getData() == null) {
					VerifyAppVersionResponseData appVersionResponseData = new VerifyAppVersionResponseData();
					verifyAppVersionResponse.setData(appVersionResponseData);
				}
				Utilities.printInfoLog(msisdn + "-PLAY_STORE ANDROID"
						+ GetConfigurations.getConfigurationFromCache("appVersion.update.url.android"), logger);
				verifyAppVersionResponse.getData()
						.setPlayStore(GetConfigurations.getConfigurationFromCache("appVersion.update.url.android"));
				Utilities.printInfoLog(msisdn + "-PLAY_STORE IOS"
						+ GetConfigurations.getConfigurationFromCache("appVersion.update.url.ios"), logger);
				verifyAppVersionResponse.getData()
						.setAppStore(GetConfigurations.getConfigurationFromCache("appVersion.update.url.ios"));
			} else {
				verifyAppVersionResponse.setCallStatus(Constants.Call_Status_False);
				verifyAppVersionResponse.setResultCode(Constants.VALIDATION_FAILURE_CODE);
				verifyAppVersionResponse.setResultDesc(requestValidationStatus);
			}

			verifyAppVersionResponse.getLogsReport().setResponseCode(verifyAppVersionResponse.getResultCode());
			verifyAppVersionResponse.getLogsReport().setRequestTime(requestTime);
			verifyAppVersionResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
			verifyAppVersionResponse.getLogsReport().setResponse(mapper.writeValueAsString(verifyAppVersionResponse));

			// Sending report log into queue.
			verifyAppVersionResponse.getLogsReport().setAppVersion(Constants.APP_VERSION_PHASE2);
			Utilities.prepareLogReportForQueue(verifyAppVersionResponse.getLogsReport());

			Utilities.printInfoLog(msisdn + "-Response Returned from-" + TRANSACTION_NAME + "-"
					+ mapper.writeValueAsString(verifyAppVersionResponse), logger);
		} catch (Exception e) {
			Utilities.printErrorLog(Utilities.convertStackTraceToString(e), logger);
			verifyAppVersionResponse.setCallStatus(Constants.Call_Status_False);

			verifyAppVersionResponse.setResultCode(Constants.EXCEPTION_CODE);
			verifyAppVersionResponse
					.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("unexpected.error", lang));

			verifyAppVersionResponse.getLogsReport().setResponseCode(verifyAppVersionResponse.getResultCode());
			verifyAppVersionResponse.getLogsReport().setRequestTime(requestTime);
			verifyAppVersionResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
			verifyAppVersionResponse.getLogsReport().setResponse(mapper.writeValueAsString(verifyAppVersionResponse));

			// Sending report log into queue.
			verifyAppVersionResponse.getLogsReport().setAppVersion(Constants.APP_VERSION_PHASE2);
			Utilities.prepareLogReportForQueue(verifyAppVersionResponse.getLogsReport());
		}
		return verifyAppVersionResponse;
	}

	@RequestMapping(value = "/companyinvoice", method = RequestMethod.POST, headers = Constants.CONTENT_TYPE_DEFAULT_VALUE)
	public CompanyInvoiceResponse getcompanyInvoice(@RequestBody String data,
			@RequestHeader(value = Constants.MSISDN_KEY, defaultValue = Constants.MSISDN_DEFAULT_VALUE) String msisdn,
			@RequestHeader(value = Constants.LANGUAGE_KEY, defaultValue = Constants.LANGUAGE_DEFAULT_VALUE) String lang,
			@RequestHeader(value = Constants.USER_AGENT_KEY, defaultValue = Constants.USER_AGENT_DEFAULT_VALUE) String userAgent,
			@RequestHeader(value = Constants.SUBSCRIBER_TYPE_KEY, defaultValue = Constants.SUBSCRIBER_TYPE_DEFAULT_VALUE) String userType,
			@RequestHeader(value = Constants.TARIFF_TYPE_KEY, defaultValue = Constants.TARIFF_TYPE_DEFAULT_VALUE) String tariffType,
			@RequestHeader(value = Constants.APP_PHASE_KEY, defaultValue = Constants.APP_PHASE_DEFAULT_VALUE) String isFromB2B,
			HttpServletRequest servletRequest) throws ClassNotFoundException, SQLException, JsonParseException,
			JsonMappingException, IOException, JMSException {
		ObjectMapper mapper = new ObjectMapper();
		GeneralServicesBusiness generalServicesBusiness = new GeneralServicesBusiness();
		CompanyInvoiceRequest companyInvoiceRequest = new CompanyInvoiceRequest();
		CompanyInvoiceResponse companyInvoiceResponse = new CompanyInvoiceResponse();

		String TRANSACTION_NAME = Transactions.COMPANY_INVOICE + " CONTROLLER";
		String requestTime = Utilities.getReportDateTime();
		try {
//			data = jsonObject.toString();

			// Populating report object before processing business logic.
			companyInvoiceResponse.setLogsReport(
					Utilities.preBusinessReportLoggingData("", msisdn, TRANSACTION_NAME, servletRequest.getRemoteAddr(),
							userAgent, userType, tariffType, data, lang, companyInvoiceResponse.getLogsReport()));

			data = Utilities.addAttributesToRequest(data, msisdn, servletRequest, lang, userAgent);

			Utilities.printInfoLog(msisdn + "-Request Landed in-" + TRANSACTION_NAME, logger);
			Utilities.printInfoLog(msisdn + "-Request Data-" + data, logger);

			companyInvoiceRequest = mapper.readValue(data, CompanyInvoiceRequest.class);

			String requestValidationStatus = Validator.validateRequest(msisdn, CompanyInvoiceRequest.class);

			if (requestValidationStatus.equalsIgnoreCase(Constants.VALIDATION_SUCCESS_CODE)) {

				companyInvoiceRequest.setIsB2B(isFromB2B);
				companyInvoiceResponse = generalServicesBusiness.getCompanySummary(msisdn, companyInvoiceRequest,
						companyInvoiceResponse);

			} else {
				logger.error("ERROR: <<<<< Validation of Request Packet Failed >>>>>");
				companyInvoiceResponse.setResultCode(Constants.VALIDATION_FAILURE_CODE);
				companyInvoiceResponse.setResultDesc(requestValidationStatus);
			}

			companyInvoiceResponse.getLogsReport().setResponseCode(companyInvoiceResponse.getResultCode());
			companyInvoiceResponse.getLogsReport().setRequestTime(requestTime);
			companyInvoiceResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
			companyInvoiceResponse.getLogsReport().setResponse(mapper.writeValueAsString(companyInvoiceResponse));

			// Sending report log into queue.
			companyInvoiceResponse.getLogsReport().setAppVersion(Constants.APP_VERSION_PHASE2);
			Utilities.prepareLogReportForQueue(companyInvoiceResponse.getLogsReport());

			Utilities.printInfoLog(msisdn + "-Response Returned from-" + TRANSACTION_NAME + "-"
					+ mapper.writeValueAsString(companyInvoiceResponse), logger);
		} catch (Exception e) {
			Utilities.printErrorLog(Utilities.convertStackTraceToString(e), logger);
			companyInvoiceResponse.setCallStatus(Constants.Call_Status_False);

			companyInvoiceResponse.setResultCode(Constants.EXCEPTION_CODE);
			companyInvoiceResponse
					.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("unexpected.error", lang));

			companyInvoiceResponse.getLogsReport().setResponseCode(companyInvoiceResponse.getResultCode());
			companyInvoiceResponse.getLogsReport().setRequestTime(requestTime);
			companyInvoiceResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
			companyInvoiceResponse.getLogsReport().setResponse(mapper.writeValueAsString(companyInvoiceResponse));

			// Sending report log into queue.
			companyInvoiceResponse.getLogsReport().setAppVersion(Constants.APP_VERSION_PHASE2);
			Utilities.prepareLogReportForQueue(companyInvoiceResponse.getLogsReport());

		}
		return companyInvoiceResponse;
	}

	@RequestMapping(value = "/msisdninvoicesummary", method = RequestMethod.POST, headers = Constants.CONTENT_TYPE_DEFAULT_VALUE)
	public MSISDNSummaryResponse getMSISDNInvoiceSummary(@RequestBody String data,
			@RequestHeader(value = Constants.MSISDN_KEY, defaultValue = Constants.MSISDN_DEFAULT_VALUE) String msisdn,
			@RequestHeader(value = Constants.LANGUAGE_KEY, defaultValue = Constants.LANGUAGE_DEFAULT_VALUE) String lang,
			@RequestHeader(value = Constants.USER_AGENT_KEY, defaultValue = Constants.USER_AGENT_DEFAULT_VALUE) String userAgent,
			@RequestHeader(value = Constants.SUBSCRIBER_TYPE_KEY, defaultValue = Constants.SUBSCRIBER_TYPE_DEFAULT_VALUE) String userType,
			@RequestHeader(value = Constants.TARIFF_TYPE_KEY, defaultValue = Constants.TARIFF_TYPE_DEFAULT_VALUE) String tariffType,
			@RequestHeader(value = Constants.APP_PHASE_KEY, defaultValue = Constants.APP_PHASE_DEFAULT_VALUE) String isFromB2B,
			HttpServletRequest servletRequest) throws ClassNotFoundException, SQLException, JsonParseException,
			JsonMappingException, IOException, JMSException {
		ObjectMapper mapper = new ObjectMapper();
		GeneralServicesBusiness generalServicesBusiness = new GeneralServicesBusiness();
		MSISDNInvoiceRequest msisdnInvoiceRequest = new MSISDNInvoiceRequest();
		MSISDNSummaryResponse msisdnSummaryResponse = new MSISDNSummaryResponse();

		String TRANSACTION_NAME = Transactions.MSISDN_INVOICE_SUMMARY + " CONTROLLER";
		String requestTime = Utilities.getReportDateTime();
		try {
//			data = jsonObject.toString();

			// Populating report object before processing business logic.
			msisdnSummaryResponse.setLogsReport(
					Utilities.preBusinessReportLoggingData("", msisdn, TRANSACTION_NAME, servletRequest.getRemoteAddr(),
							userAgent, userType, tariffType, data, lang, msisdnSummaryResponse.getLogsReport()));

			data = Utilities.addAttributesToRequest(data, msisdn, servletRequest, lang, userAgent);

			Utilities.printInfoLog(msisdn + "-Request Landed in-" + TRANSACTION_NAME, logger);
			Utilities.printInfoLog(msisdn + "-Request Data-" + data, logger);

			msisdnInvoiceRequest = mapper.readValue(data, MSISDNInvoiceRequest.class);

			String requestValidationStatus = Validator.validateRequest(msisdn, MSISDNInvoiceRequest.class);

			if (requestValidationStatus.equalsIgnoreCase(Constants.VALIDATION_SUCCESS_CODE)) {

				msisdnInvoiceRequest.setIsB2B(isFromB2B);
				msisdnSummaryResponse = generalServicesBusiness.getInvoiceSummary(msisdn, msisdnInvoiceRequest,
						msisdnSummaryResponse);

			} else {
				logger.error("ERROR: <<<<< Validation of Request Packet Failed >>>>>");
				msisdnSummaryResponse.setResultCode(Constants.VALIDATION_FAILURE_CODE);
				msisdnSummaryResponse.setResultDesc(requestValidationStatus);
			}

			msisdnSummaryResponse.getLogsReport().setResponseCode(msisdnSummaryResponse.getResultCode());
			msisdnSummaryResponse.getLogsReport().setRequestTime(requestTime);
			msisdnSummaryResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
			msisdnSummaryResponse.getLogsReport().setResponse(mapper.writeValueAsString(msisdnSummaryResponse));

			// Sending report log into queue.
			msisdnSummaryResponse.getLogsReport().setAppVersion(Constants.APP_VERSION_PHASE2);
			Utilities.prepareLogReportForQueue(msisdnSummaryResponse.getLogsReport());

			Utilities.printInfoLog(msisdn + "-Response Returned from-" + TRANSACTION_NAME + "-"
					+ mapper.writeValueAsString(msisdnSummaryResponse), logger);
		} catch (Exception e) {
			Utilities.printErrorLog(Utilities.convertStackTraceToString(e), logger);
			msisdnSummaryResponse.setCallStatus(Constants.Call_Status_False);

			msisdnSummaryResponse.setResultCode(Constants.EXCEPTION_CODE);
			msisdnSummaryResponse
					.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("unexpected.error", lang));

			msisdnSummaryResponse.getLogsReport().setResponseCode(msisdnSummaryResponse.getResultCode());
			msisdnSummaryResponse.getLogsReport().setRequestTime(requestTime);
			msisdnSummaryResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
			msisdnSummaryResponse.getLogsReport().setResponse(mapper.writeValueAsString(msisdnSummaryResponse));

			// Sending report log into queue.
			msisdnSummaryResponse.getLogsReport().setAppVersion(Constants.APP_VERSION_PHASE2);
			Utilities.prepareLogReportForQueue(msisdnSummaryResponse.getLogsReport());

		}
		return msisdnSummaryResponse;
	}

	@RequestMapping(value = "/msisdninvoicedetails", method = RequestMethod.POST, headers = Constants.CONTENT_TYPE_DEFAULT_VALUE)
	public MSISDNDetailsResponse getMSISDNInvoiceDetails(@RequestBody String data,
			@RequestHeader(value = Constants.MSISDN_KEY, defaultValue = Constants.MSISDN_DEFAULT_VALUE) String msisdn,
			@RequestHeader(value = Constants.LANGUAGE_KEY, defaultValue = Constants.LANGUAGE_DEFAULT_VALUE) String lang,
			@RequestHeader(value = Constants.USER_AGENT_KEY, defaultValue = Constants.USER_AGENT_DEFAULT_VALUE) String userAgent,
			@RequestHeader(value = Constants.SUBSCRIBER_TYPE_KEY, defaultValue = Constants.SUBSCRIBER_TYPE_DEFAULT_VALUE) String userType,
			@RequestHeader(value = Constants.TARIFF_TYPE_KEY, defaultValue = Constants.TARIFF_TYPE_DEFAULT_VALUE) String tariffType,
			@RequestHeader(value = Constants.APP_PHASE_KEY, defaultValue = Constants.APP_PHASE_DEFAULT_VALUE) String isFromB2B,
			HttpServletRequest servletRequest) throws ClassNotFoundException, SQLException, JsonParseException,
			JsonMappingException, IOException, JMSException {
		ObjectMapper mapper = new ObjectMapper();
		GeneralServicesBusiness generalServicesBusiness = new GeneralServicesBusiness();
		MSISDNInvoiceRequest msisdnInvoiceRequest = new MSISDNInvoiceRequest();
		MSISDNDetailsResponse msisdnDetailsResponse = new MSISDNDetailsResponse();

		String TRANSACTION_NAME = Transactions.MSISDN_INVOICE_DETAILS + " CONTROLLER";
		String requestTime = Utilities.getReportDateTime();
		try {
//			data = jsonObject.toString();

			// Populating report object before processing business logic.
			msisdnDetailsResponse.setLogsReport(
					Utilities.preBusinessReportLoggingData("", msisdn, TRANSACTION_NAME, servletRequest.getRemoteAddr(),
							userAgent, userType, tariffType, data, lang, msisdnDetailsResponse.getLogsReport()));

			data = Utilities.addAttributesToRequest(data, msisdn, servletRequest, lang, userAgent);

			Utilities.printInfoLog(msisdn + "-Request Landed in-" + TRANSACTION_NAME, logger);
			Utilities.printInfoLog(msisdn + "-Request Data-" + data, logger);

			msisdnInvoiceRequest = mapper.readValue(data, MSISDNInvoiceRequest.class);

			String requestValidationStatus = Validator.validateRequest(msisdn, MSISDNInvoiceRequest.class);

			if (requestValidationStatus.equalsIgnoreCase(Constants.VALIDATION_SUCCESS_CODE)) {

				msisdnInvoiceRequest.setIsB2B(isFromB2B);
				msisdnDetailsResponse = generalServicesBusiness.getInvoiceDetails(msisdn, msisdnInvoiceRequest,
						msisdnDetailsResponse);

			} else {
				logger.error("ERROR: <<<<< Validation of Request Packet Failed >>>>>");
				msisdnDetailsResponse.setResultCode(Constants.VALIDATION_FAILURE_CODE);
				msisdnDetailsResponse.setResultDesc(requestValidationStatus);
			}
			logger.info(" <<<<< Responce back in Controller >>>>>");
			msisdnDetailsResponse.getLogsReport().setResponseCode(msisdnDetailsResponse.getResultCode());
			msisdnDetailsResponse.getLogsReport().setRequestTime(requestTime);
			msisdnDetailsResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
			msisdnDetailsResponse.getLogsReport().setResponse(mapper.writeValueAsString(msisdnDetailsResponse));

			// Sending report log into queue.
			msisdnDetailsResponse.getLogsReport().setAppVersion(Constants.APP_VERSION_PHASE2);
			Utilities.prepareLogReportForQueue(msisdnDetailsResponse.getLogsReport());

			Utilities.printInfoLog(msisdn + "-Response Returned from-" + TRANSACTION_NAME + "-"
					+ mapper.writeValueAsString(msisdnDetailsResponse), logger);
		} catch (Exception e) {
			Utilities.printErrorLog(Utilities.convertStackTraceToString(e), logger);
			msisdnDetailsResponse.setCallStatus(Constants.Call_Status_False);

			msisdnDetailsResponse.setResultCode(Constants.EXCEPTION_CODE);
			msisdnDetailsResponse
					.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("unexpected.error", lang));

			msisdnDetailsResponse.getLogsReport().setResponseCode(msisdnDetailsResponse.getResultCode());
			msisdnDetailsResponse.getLogsReport().setRequestTime(requestTime);
			msisdnDetailsResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
			msisdnDetailsResponse.getLogsReport().setResponse(mapper.writeValueAsString(msisdnDetailsResponse));

			// Sending report log into queue.
			msisdnDetailsResponse.getLogsReport().setAppVersion(Constants.APP_VERSION_PHASE2);
			Utilities.prepareLogReportForQueue(msisdnDetailsResponse.getLogsReport());

		}
		return msisdnDetailsResponse;
	}

	@RequestMapping(value = "/verify", method = RequestMethod.POST, headers = Constants.CONTENT_TYPE_DEFAULT_VALUE)
	public SimSwapResponse getSimSwapVerify(@RequestBody String data,
			@RequestHeader(value = Constants.MSISDN_KEY, defaultValue = Constants.MSISDN_DEFAULT_VALUE) String msisdn,
			@RequestHeader(value = Constants.LANGUAGE_KEY, defaultValue = Constants.LANGUAGE_DEFAULT_VALUE) String lang,
			@RequestHeader(value = Constants.USER_AGENT_KEY, defaultValue = Constants.USER_AGENT_DEFAULT_VALUE) String userAgent,
			@RequestHeader(value = Constants.SUBSCRIBER_TYPE_KEY, defaultValue = Constants.SUBSCRIBER_TYPE_DEFAULT_VALUE) String userType,
			@RequestHeader(value = Constants.TARIFF_TYPE_KEY, defaultValue = Constants.TARIFF_TYPE_DEFAULT_VALUE) String tariffType,
			@RequestHeader(value = Constants.APP_PHASE_KEY, defaultValue = Constants.APP_PHASE_DEFAULT_VALUE) String isFromB2B,
			HttpServletRequest servletRequest) throws ClassNotFoundException, SQLException, JsonParseException,
			JsonMappingException, IOException, JMSException {
		ObjectMapper mapper = new ObjectMapper();
		JSONObject jsonObject = new JSONObject();
		GeneralServicesBusiness generalServicesBusiness = new GeneralServicesBusiness();
		SimSwapRequest simSwapRequest = new SimSwapRequest();
		SimSwapResponse simSwapResponse = new SimSwapResponse();

		String TRANSACTION_NAME = Transactions.SIMSWAP_TRANSACTION_NAME + " CONTROLLER";
		String requestTime = Utilities.getReportDateTime();
		try {
//		data = jsonObject.toString();

			// Populating report object before processing business logic.
			simSwapResponse.setLogsReport(
					Utilities.preBusinessReportLoggingData("", msisdn, TRANSACTION_NAME, servletRequest.getRemoteAddr(),
							userAgent, userType, tariffType, data, lang, simSwapResponse.getLogsReport()));

			data = Utilities.addAttributesToRequest(data, msisdn, servletRequest, lang, userAgent);

			Utilities.printInfoLog(msisdn + "-Request Landed in-" + TRANSACTION_NAME, logger);
			Utilities.printInfoLog(msisdn + "-Request Data-" + data, logger);

			simSwapRequest = mapper.readValue(data, SimSwapRequest.class);

			String requestValidationStatus = Validator.validateRequest(msisdn, simSwapRequest);

			if (requestValidationStatus.equalsIgnoreCase(Constants.VALIDATION_SUCCESS_CODE)) {

				simSwapRequest.setIsB2B(isFromB2B);
				simSwapResponse = generalServicesBusiness.simswapVerify(msisdn, simSwapRequest, simSwapResponse);

			} else {
				logger.error("ERROR: <<<<< Validation of Request Packet Failed >>>>>");
				simSwapResponse.setCallStatus(Constants.Call_Status_False);
				simSwapResponse.setResultCode(Constants.VALIDATION_FAILURE_CODE);
				simSwapResponse.setResultDesc(requestValidationStatus);
			}

			simSwapResponse.getLogsReport().setResponseCode(simSwapResponse.getResultCode());
			simSwapResponse.getLogsReport().setRequestTime(requestTime);
			simSwapResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
			simSwapResponse.getLogsReport().setResponse(mapper.writeValueAsString(simSwapResponse));

			// Sending report log into queue.
			simSwapResponse.getLogsReport().setAppVersion(Constants.APP_VERSION_PHASE2);
			Utilities.prepareLogReportForQueue(simSwapResponse.getLogsReport());

			Utilities.printInfoLog(msisdn + "-Response Returned from-" + TRANSACTION_NAME + "-"
					+ mapper.writeValueAsString(simSwapResponse), logger);
		} catch (Exception e) {
			Utilities.printErrorLog(Utilities.convertStackTraceToString(e), logger);
			simSwapResponse.setCallStatus(Constants.Call_Status_False);

			simSwapResponse.setResultCode(Constants.EXCEPTION_CODE);
			simSwapResponse.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("unexpected.error", lang));

			simSwapResponse.getLogsReport().setResponseCode(simSwapResponse.getResultCode());
			simSwapResponse.getLogsReport().setRequestTime(requestTime);
			simSwapResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
			simSwapResponse.getLogsReport().setResponse(mapper.writeValueAsString(simSwapResponse));

			// Sending report log into queue.
			simSwapResponse.getLogsReport().setAppVersion(Constants.APP_VERSION_PHASE2);
			Utilities.prepareLogReportForQueue(simSwapResponse.getLogsReport());

		}
		return simSwapResponse;
	}

	@RequestMapping(value = "/addApplication", method = RequestMethod.POST, headers = Constants.CONTENT_TYPE_DEFAULT_VALUE)
	public SimSwapResponse getSimSwapDocumentValid(@RequestBody String data,
			@RequestHeader(value = Constants.MSISDN_KEY, defaultValue = Constants.MSISDN_DEFAULT_VALUE) String msisdn,
			@RequestHeader(value = Constants.LANGUAGE_KEY, defaultValue = Constants.LANGUAGE_DEFAULT_VALUE) String lang,
			@RequestHeader(value = Constants.USER_AGENT_KEY, defaultValue = Constants.USER_AGENT_DEFAULT_VALUE) String userAgent,
			@RequestHeader(value = Constants.SUBSCRIBER_TYPE_KEY, defaultValue = Constants.SUBSCRIBER_TYPE_DEFAULT_VALUE) String userType,
			@RequestHeader(value = Constants.TARIFF_TYPE_KEY, defaultValue = Constants.TARIFF_TYPE_DEFAULT_VALUE) String tariffType,
			@RequestHeader(value = Constants.APP_PHASE_KEY, defaultValue = Constants.APP_PHASE_DEFAULT_VALUE) String isFromB2B,
			HttpServletRequest servletRequest) throws ClassNotFoundException, SQLException, JsonParseException,
			JsonMappingException, IOException, JMSException {
		ObjectMapper mapper = new ObjectMapper();
		JSONObject jsonObject = new JSONObject();
		GeneralServicesBusiness generalServicesBusiness = new GeneralServicesBusiness();
		SimSwapRequest simSwapRequest = new SimSwapRequest();
		SimSwapResponse simSwapResponse = new SimSwapResponse();

		String TRANSACTION_NAME = Transactions.SIMSWAP_TRANSACTION_NAME + " CONTROLLER";
		String requestTime = Utilities.getReportDateTime();
		try {

			// Populating report object before processing business logic.
			simSwapResponse.setLogsReport(
					Utilities.preBusinessReportLoggingData("", msisdn, TRANSACTION_NAME, servletRequest.getRemoteAddr(),
							userAgent, userType, tariffType, data, lang, simSwapResponse.getLogsReport()));

			data = Utilities.addAttributesToRequest(data, msisdn, servletRequest, lang, userAgent);

			Utilities.printInfoLog(msisdn + "-Request Landed in-" + TRANSACTION_NAME, logger);
			Utilities.printInfoLog(msisdn + "-Request Data-" + data, logger);

			simSwapRequest = mapper.readValue(data, SimSwapRequest.class);

			String requestValidationStatus = Validator.validateRequest(msisdn, simSwapRequest);

			if (requestValidationStatus.equalsIgnoreCase(Constants.VALIDATION_SUCCESS_CODE)) {

				simSwapRequest.setIsB2B(isFromB2B);
				simSwapResponse = generalServicesBusiness.simswapAddApplication(msisdn, simSwapRequest,
						simSwapResponse);

			} else {
				logger.error("ERROR: <<<<< Validation of Request Packet Failed >>>>>");
				simSwapResponse.setCallStatus(Constants.Call_Status_False);
				simSwapResponse.setResultCode(Constants.VALIDATION_FAILURE_CODE);
				simSwapResponse.setResultDesc(requestValidationStatus);
			}

			simSwapResponse.getLogsReport().setResponseCode(simSwapResponse.getResultCode());
			simSwapResponse.getLogsReport().setRequestTime(requestTime);
			simSwapResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
			simSwapResponse.getLogsReport().setResponse(mapper.writeValueAsString(simSwapResponse));

			// Sending report log into queue.
			simSwapResponse.getLogsReport().setAppVersion(Constants.APP_VERSION_PHASE2);
			Utilities.prepareLogReportForQueue(simSwapResponse.getLogsReport());

			Utilities.printInfoLog(msisdn + "-Response Returned from-" + TRANSACTION_NAME + "-"
					+ mapper.writeValueAsString(simSwapResponse), logger);
		} catch (Exception e) {
			Utilities.printErrorLog(Utilities.convertStackTraceToString(e), logger);
			simSwapResponse.setCallStatus(Constants.Call_Status_False);

			simSwapResponse.setResultCode(Constants.EXCEPTION_CODE);
			simSwapResponse.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("unexpected.error", lang));

			simSwapResponse.getLogsReport().setResponseCode(simSwapResponse.getResultCode());
			simSwapResponse.getLogsReport().setRequestTime(requestTime);
			simSwapResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
			simSwapResponse.getLogsReport().setResponse(mapper.writeValueAsString(simSwapResponse));

			// Sending report log into queue.
			simSwapResponse.getLogsReport().setAppVersion(Constants.APP_VERSION_PHASE2);
			Utilities.prepareLogReportForQueue(simSwapResponse.getLogsReport());

		}
		return simSwapResponse;
	}

	@RequestMapping(value = "/documentValid", method = RequestMethod.POST, headers = Constants.CONTENT_TYPE_DEFAULT_VALUE)
	public SimSwapResponse getSimSwapAddApplication(@RequestBody String data,
			@RequestHeader(value = Constants.MSISDN_KEY, defaultValue = Constants.MSISDN_DEFAULT_VALUE) String msisdn,
			@RequestHeader(value = Constants.LANGUAGE_KEY, defaultValue = Constants.LANGUAGE_DEFAULT_VALUE) String lang,
			@RequestHeader(value = Constants.USER_AGENT_KEY, defaultValue = Constants.USER_AGENT_DEFAULT_VALUE) String userAgent,
			@RequestHeader(value = Constants.SUBSCRIBER_TYPE_KEY, defaultValue = Constants.SUBSCRIBER_TYPE_DEFAULT_VALUE) String userType,
			@RequestHeader(value = Constants.TARIFF_TYPE_KEY, defaultValue = Constants.TARIFF_TYPE_DEFAULT_VALUE) String tariffType,
			@RequestHeader(value = Constants.APP_PHASE_KEY, defaultValue = Constants.APP_PHASE_DEFAULT_VALUE) String isFromB2B,
			HttpServletRequest servletRequest) throws ClassNotFoundException, SQLException, JsonParseException,
			JsonMappingException, IOException, JMSException {
		ObjectMapper mapper = new ObjectMapper();
		JSONObject jsonObject = new JSONObject();
		GeneralServicesBusiness generalServicesBusiness = new GeneralServicesBusiness();
		SimSwapRequest simSwapRequest = new SimSwapRequest();
		SimSwapResponse simSwapResponse = new SimSwapResponse();

		String TRANSACTION_NAME = Transactions.SIMSWAP_TRANSACTION_NAME + " CONTROLLER";
		String requestTime = Utilities.getReportDateTime();
		try {

			// Populating report object before processing business logic.
			simSwapResponse.setLogsReport(
					Utilities.preBusinessReportLoggingData("", msisdn, TRANSACTION_NAME, servletRequest.getRemoteAddr(),
							userAgent, userType, tariffType, data, lang, simSwapResponse.getLogsReport()));

			data = Utilities.addAttributesToRequest(data, msisdn, servletRequest, lang, userAgent);

			Utilities.printInfoLog(msisdn + "-Request Landed in-" + TRANSACTION_NAME, logger);
			Utilities.printInfoLog(msisdn + "-Request Data-" + data, logger);

			simSwapRequest = mapper.readValue(data, SimSwapRequest.class);

			String requestValidationStatus = Validator.validateRequest(msisdn, simSwapRequest);

			if (requestValidationStatus.equalsIgnoreCase(Constants.VALIDATION_SUCCESS_CODE)) {

				simSwapRequest.setIsB2B(isFromB2B);
				simSwapResponse = generalServicesBusiness.simswapDocumentValid(msisdn, simSwapRequest, simSwapResponse);

			} else {
				logger.error("ERROR: <<<<< Validation of Request Packet Failed >>>>>");
				simSwapResponse.setCallStatus(Constants.Call_Status_False);
				simSwapResponse.setResultCode(Constants.VALIDATION_FAILURE_CODE);
				simSwapResponse.setResultDesc(requestValidationStatus);
			}

			simSwapResponse.getLogsReport().setResponseCode(simSwapResponse.getResultCode());
			simSwapResponse.getLogsReport().setRequestTime(requestTime);
			simSwapResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
			simSwapResponse.getLogsReport().setResponse(mapper.writeValueAsString(simSwapResponse));

			// Sending report log into queue.
			simSwapResponse.getLogsReport().setAppVersion(Constants.APP_VERSION_PHASE2);
			Utilities.prepareLogReportForQueue(simSwapResponse.getLogsReport());

			Utilities.printInfoLog(msisdn + "-Response Returned from-" + TRANSACTION_NAME + "-"
					+ mapper.writeValueAsString(simSwapResponse), logger);
		} catch (Exception e) {
			Utilities.printErrorLog(Utilities.convertStackTraceToString(e), logger);
			simSwapResponse.setCallStatus(Constants.Call_Status_False);

			simSwapResponse.setResultCode(Constants.EXCEPTION_CODE);
			simSwapResponse.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("unexpected.error", lang));

			simSwapResponse.getLogsReport().setResponseCode(simSwapResponse.getResultCode());
			simSwapResponse.getLogsReport().setRequestTime(requestTime);
			simSwapResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
			simSwapResponse.getLogsReport().setResponse(mapper.writeValueAsString(simSwapResponse));

			// Sending report log into queue.
			simSwapResponse.getLogsReport().setAppVersion(Constants.APP_VERSION_PHASE2);
			Utilities.prepareLogReportForQueue(simSwapResponse.getLogsReport());

		}
		return simSwapResponse;
	}

}
