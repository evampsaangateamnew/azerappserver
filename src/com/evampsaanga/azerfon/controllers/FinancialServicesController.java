/**
 * 
 */
package com.evampsaanga.azerfon.controllers;

import java.io.IOException;
import java.sql.SQLException;

import javax.jms.JMSException;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.evampsaanga.azerfon.business.FinancialServicesBusiness;
import com.evampsaanga.azerfon.common.messagesmappings.GetMessagesMappings;
import com.evampsaanga.azerfon.common.utilities.Constants;
import com.evampsaanga.azerfon.common.utilities.Transactions;
import com.evampsaanga.azerfon.common.utilities.Utilities;
import com.evampsaanga.azerfon.common.utilities.Validator;
import com.evampsaanga.azerfon.models.financialservices.getloan.GetLoanRequest;
import com.evampsaanga.azerfon.models.financialservices.getloan.GetLoanResponse;
import com.evampsaanga.azerfon.models.financialservices.loanhistory.LoanHistoryRequest;
import com.evampsaanga.azerfon.models.financialservices.loanhistory.LoanHistoryResponse;
import com.evampsaanga.azerfon.models.financialservices.moneytransfer.MoneyTransferRequest;
import com.evampsaanga.azerfon.models.financialservices.moneytransfer.MoneyTransferResponse;
import com.evampsaanga.azerfon.models.financialservices.paymenthistory.PaymentHistoryRequest;
import com.evampsaanga.azerfon.models.financialservices.paymenthistory.PaymentHistoryResponse;
import com.evampsaanga.azerfon.models.financialservices.requestmoney.RequestMoneyRequest;
import com.evampsaanga.azerfon.models.financialservices.requestmoney.RequestMoneyResponse;
import com.evampsaanga.azerfon.models.financialservices.topups.TopupRequest;
import com.evampsaanga.azerfon.models.financialservices.topups.TopupResponse;

/**
 * @author Evamp & Saanga
 * 
 */
@RestController
@RequestMapping("/financialservices")
public class FinancialServicesController {

	Logger logger = Logger.getLogger(FinancialServicesController.class);

	@RequestMapping(value = "/requesttopup", method = RequestMethod.POST, headers = Constants.CONTENT_TYPE_DEFAULT_VALUE)
	public TopupResponse getTopups(@RequestBody String data,
			@RequestHeader(value = Constants.TOKEN_KEY, defaultValue = Constants.TOKEN_DEFAULT_VALUE) String token,
			@RequestHeader(value = Constants.USER_AGENT_KEY, defaultValue = Constants.USER_AGENT_DEFAULT_VALUE) String userAgent,
			@RequestHeader(value = Constants.LANGUAGE_KEY, defaultValue = Constants.LANGUAGE_DEFAULT_VALUE) String lang,
			@RequestHeader(value = Constants.SUBSCRIBER_TYPE_KEY, defaultValue = Constants.SUBSCRIBER_TYPE_DEFAULT_VALUE) String userType,
			@RequestHeader(value = Constants.TARIFF_TYPE_KEY, defaultValue = Constants.TARIFF_TYPE_DEFAULT_VALUE) String tariffType,
			HttpServletRequest servletRequest) throws IOException, JMSException, ClassNotFoundException, SQLException {
		ObjectMapper mapper = new ObjectMapper();
		TopupRequest topupRequest = new TopupRequest();
		TopupResponse topupResponse = new TopupResponse();
		FinancialServicesBusiness financialServicesBusiness = new FinancialServicesBusiness();
		String TRANSACTION_NAME = Transactions.TOPUP_TRANSACTION_NAME + " CONTROLLER";
		String requestTime = Utilities.getReportDateTime();
		try {
			String msisdn = Utilities.getValueFromJSON(Utilities.splitToken(Utilities.decodeString(token)).toString(),
					Constants.MSISDN_KEY);

			// Populating report object before processing business logic.
			topupResponse.setLogsReport(Utilities.preBusinessReportLoggingData(token, msisdn, TRANSACTION_NAME,
					servletRequest.getRemoteAddr(), userAgent, userType, tariffType, data, lang,
					topupResponse.getLogsReport()));

			data = Utilities.addAttributesToRequest(data, msisdn, servletRequest, lang, userAgent);
			Utilities.printInfoLog(msisdn + "-Request Landed in-" + TRANSACTION_NAME, logger);

			/*
			 * Below JSON modification is done just to mask scratch card number. Purpose is
			 * not to print scratch card number in logs or database.
			 */
			String tempData = data;
			String pinCard = Utilities.getValueFromJSON(tempData, "cardPinNumber");
			pinCard = Utilities.maskString(pinCard, Constants.SCRATCH_CARD_MASKING_COUNT);
			tempData = Utilities.removeParamsFromJSONObject(tempData, "cardPinNumber");
			tempData = Utilities.addParamsToJSONObject(tempData, "cardPinNumber", pinCard);

			Utilities.printInfoLog(msisdn + "-Request Data-" + tempData, logger);
			topupRequest = mapper.readValue(data, TopupRequest.class);

			// Logging specific params in report.
			topupResponse.getLogsReport().setReceiverMsisdn(topupRequest.getTopupnum());
			topupResponse.getLogsReport().setMediumForTopUp("Scratch Card");
			topupResponse.getLogsReport().setScratchCardNumber(
					Utilities.maskString(topupRequest.getCardPinNumber(), Constants.SCRATCH_CARD_MASKING_COUNT));

			String requestValidationStatus = Validator.validateRequest(msisdn, topupRequest);

			if (requestValidationStatus.equalsIgnoreCase(Constants.VALIDATION_SUCCESS_CODE)) {

				topupResponse = financialServicesBusiness.getTopupBusiness(msisdn, topupRequest, topupResponse);
			} else {
				topupResponse.setCallStatus(Constants.Call_Status_False);
				topupResponse.setResultCode(Constants.VALIDATION_FAILURE_CODE);
				topupResponse.setResultDesc(requestValidationStatus);
			}

			topupResponse.getLogsReport().setRequestTime(requestTime);
			topupResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
			topupResponse.getLogsReport().setResponse(mapper.writeValueAsString(topupResponse));
			topupResponse.getLogsReport().setResponseCode(topupResponse.getResultCode());

			// Sending report log into queue.
			Utilities.prepareLogReportForQueue(topupResponse.getLogsReport());

			Utilities.printInfoLog(msisdn + "-Response Returned from-" + TRANSACTION_NAME + "-"
					+ mapper.writeValueAsString(topupResponse), logger);

		} catch (Exception e) {
			Utilities.printErrorLog(Utilities.convertStackTraceToString(e), logger);
			topupResponse.setCallStatus(Constants.Call_Status_False);

			topupResponse.setResultCode(Constants.EXCEPTION_CODE);
			topupResponse.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("unexpected.error", lang));

			topupResponse.getLogsReport().setResponseCode(topupResponse.getResultCode());
			topupResponse.getLogsReport().setRequestTime(requestTime);
			topupResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
			topupResponse.getLogsReport().setResponse(mapper.writeValueAsString(topupResponse));

			// Sending report log into queue.
			Utilities.prepareLogReportForQueue(topupResponse.getLogsReport());
		}

		return topupResponse;
	}

	@RequestMapping(value = "/requestmoneytransfer", method = RequestMethod.POST, headers = Constants.CONTENT_TYPE_DEFAULT_VALUE)
	public MoneyTransferResponse moneyTransfer(@RequestBody String data,
			@RequestHeader(value = Constants.TOKEN_KEY, defaultValue = Constants.TOKEN_DEFAULT_VALUE) String token,
			@RequestHeader(value = Constants.USER_AGENT_KEY, defaultValue = Constants.USER_AGENT_DEFAULT_VALUE) String userAgent,
			@RequestHeader(value = Constants.LANGUAGE_KEY, defaultValue = Constants.LANGUAGE_DEFAULT_VALUE) String lang,
			@RequestHeader(value = Constants.SUBSCRIBER_TYPE_KEY, defaultValue = Constants.SUBSCRIBER_TYPE_DEFAULT_VALUE) String userType,
			@RequestHeader(value = Constants.TARIFF_TYPE_KEY, defaultValue = Constants.TARIFF_TYPE_DEFAULT_VALUE) String tariffType,
			HttpServletRequest servletRequest) throws JsonParseException, JsonMappingException, IOException,
			JMSException, ClassNotFoundException, SQLException {
		ObjectMapper mapper = new ObjectMapper();
		MoneyTransferRequest moneyTransferRequest = new MoneyTransferRequest();
		MoneyTransferResponse moneyTransferResponse = new MoneyTransferResponse();
		FinancialServicesBusiness financialServicesBusiness = new FinancialServicesBusiness();
		String TRANSACTION_NAME = Transactions.TRANSFER_MONEY_TRANSACTION_NAME + " CONTROLLER";
		String requestTime = Utilities.getReportDateTime();

		try {
			String msisdn = Utilities.getValueFromJSON(Utilities.splitToken(Utilities.decodeString(token)).toString(),
					Constants.MSISDN_KEY);

			// Populating report object before processing business logic.
			moneyTransferResponse.setLogsReport(Utilities.preBusinessReportLoggingData(token, msisdn, TRANSACTION_NAME,
					servletRequest.getRemoteAddr(), userAgent, userType, tariffType, data, lang,
					moneyTransferResponse.getLogsReport()));

			data = Utilities.addAttributesToRequest(data, msisdn, servletRequest, lang, userAgent);
			Utilities.printInfoLog(msisdn + "-Request Landed in-" + TRANSACTION_NAME, logger);
			Utilities.printInfoLog(msisdn + "-Request Data-" + data, logger);
			moneyTransferRequest = mapper.readValue(data, MoneyTransferRequest.class);

			// Logging specific params in report.
			moneyTransferResponse.getLogsReport().setReceiverMsisdn(moneyTransferRequest.getTransferee());
			moneyTransferResponse.getLogsReport().setAmount(moneyTransferRequest.getAmount());

			String requestValidationStatus = Validator.validateRequest(msisdn, moneyTransferRequest);

			if (requestValidationStatus.equalsIgnoreCase(Constants.VALIDATION_SUCCESS_CODE)) {

				moneyTransferResponse = financialServicesBusiness.moneyTransferBusiness(msisdn, moneyTransferRequest,
						moneyTransferResponse);
			} else {
				moneyTransferResponse.setCallStatus(Constants.Call_Status_False);
				moneyTransferResponse.setResultCode(Constants.VALIDATION_FAILURE_CODE);
				moneyTransferResponse.setResultDesc(requestValidationStatus);
			}

			moneyTransferResponse.getLogsReport().setResponseCode(moneyTransferResponse.getResultCode());
			moneyTransferResponse.getLogsReport().setRequestTime(requestTime);
			moneyTransferResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
			moneyTransferResponse.getLogsReport().setResponse(mapper.writeValueAsString(moneyTransferResponse));

			// Sending report log into queue.
			Utilities.prepareLogReportForQueue(moneyTransferResponse.getLogsReport());

			Utilities.printInfoLog(msisdn + "-Response Returned from-" + TRANSACTION_NAME + "-"
					+ mapper.writeValueAsString(moneyTransferResponse), logger);

		} catch (Exception e) {
			Utilities.printErrorLog(Utilities.convertStackTraceToString(e), logger);
			moneyTransferResponse.setCallStatus(Constants.Call_Status_False);

			moneyTransferResponse.setResultCode(Constants.EXCEPTION_CODE);
			moneyTransferResponse
					.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("unexpected.error", lang));

			moneyTransferResponse.getLogsReport().setResponseCode(moneyTransferResponse.getResultCode());
			moneyTransferResponse.getLogsReport().setRequestTime(requestTime);
			moneyTransferResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
			moneyTransferResponse.getLogsReport().setResponse(mapper.writeValueAsString(moneyTransferResponse));

			// Sending report log into queue.
			Utilities.prepareLogReportForQueue(moneyTransferResponse.getLogsReport());

		}

		return moneyTransferResponse;
	}

	@RequestMapping(value = "/getloan", method = RequestMethod.POST, headers = Constants.CONTENT_TYPE_DEFAULT_VALUE)
	public GetLoanResponse getLoan(@RequestBody String data,
			@RequestHeader(value = Constants.TOKEN_KEY, defaultValue = Constants.TOKEN_DEFAULT_VALUE) String token,
			@RequestHeader(value = Constants.LANGUAGE_KEY, defaultValue = Constants.LANGUAGE_DEFAULT_VALUE) String lang,
			@RequestHeader(value = Constants.USER_AGENT_KEY, defaultValue = Constants.USER_AGENT_DEFAULT_VALUE) String userAgent,
			@RequestHeader(value = Constants.SUBSCRIBER_TYPE_KEY, defaultValue = Constants.SUBSCRIBER_TYPE_DEFAULT_VALUE) String userType,
			@RequestHeader(value = Constants.TARIFF_TYPE_KEY, defaultValue = Constants.TARIFF_TYPE_DEFAULT_VALUE) String tariffType,
			HttpServletRequest servletRequest) throws ClassNotFoundException, SQLException, JsonParseException,
			JsonMappingException, IOException, JMSException {
		ObjectMapper mapper = new ObjectMapper();
		FinancialServicesBusiness financialServicesBusiness = new FinancialServicesBusiness();
		GetLoanRequest getLoanRequest = new GetLoanRequest();
		GetLoanResponse getLoanResponse = new GetLoanResponse();

		String TRANSACTION_NAME = Transactions.LOAN_REQUEST_TRANSACTION_NAME + " CONTROLLER";
		String requestTime = Utilities.getReportDateTime();
		try {

			String msisdn = Utilities.getValueFromJSON(Utilities.splitToken(Utilities.decodeString(token)).toString(),
					Constants.MSISDN_KEY);

			// Populating report object before processing business logic.
			getLoanResponse.setLogsReport(Utilities.preBusinessReportLoggingData(token, msisdn, TRANSACTION_NAME,
					servletRequest.getRemoteAddr(), userAgent, userType, tariffType, data, lang,
					getLoanResponse.getLogsReport()));

			data = Utilities.addAttributesToRequest(data, msisdn, servletRequest, lang, userAgent);

			Utilities.printInfoLog(msisdn + "-Request Landed in-" + TRANSACTION_NAME, logger);
			Utilities.printInfoLog(msisdn + "-Request Data-" + data, logger);

			getLoanRequest = mapper.readValue(data, GetLoanRequest.class);

			// Logging specific params in report.
			getLoanResponse.getLogsReport().setAmount(getLoanRequest.getLoanAmount());

			String requestValidationStatus = Validator.validateRequest(msisdn, getLoanRequest);

			if (requestValidationStatus.equalsIgnoreCase(Constants.VALIDATION_SUCCESS_CODE)) {

				getLoanResponse = financialServicesBusiness.getLoanBusiness(msisdn, getLoanRequest, getLoanResponse);
			} else {
				getLoanResponse.setCallStatus(Constants.Call_Status_False);
				getLoanResponse.setResultCode(Constants.VALIDATION_FAILURE_CODE);
				getLoanResponse.setResultDesc(requestValidationStatus);
			}

			getLoanResponse.getLogsReport().setResponseCode(getLoanResponse.getResultCode());
			getLoanResponse.getLogsReport().setRequestTime(requestTime);
			getLoanResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
			getLoanResponse.getLogsReport().setResponse(mapper.writeValueAsString(getLoanResponse));

			// Sending report log into queue.
			Utilities.prepareLogReportForQueue(getLoanResponse.getLogsReport());

			Utilities.printInfoLog(msisdn + "-Response Returned from-" + TRANSACTION_NAME + "-"
					+ mapper.writeValueAsString(getLoanResponse), logger);
		} catch (Exception e) {
			Utilities.printErrorLog(Utilities.convertStackTraceToString(e), logger);
			getLoanResponse.setCallStatus(Constants.Call_Status_False);

			getLoanResponse.setResultCode(Constants.EXCEPTION_CODE);
			getLoanResponse.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("unexpected.error", lang));

			getLoanResponse.getLogsReport().setResponseCode(getLoanResponse.getResultCode());
			getLoanResponse.getLogsReport().setRequestTime(requestTime);
			getLoanResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
			getLoanResponse.getLogsReport().setResponse(mapper.writeValueAsString(getLoanResponse));

			// Sending report log into queue.
			Utilities.prepareLogReportForQueue(getLoanResponse.getLogsReport());
		}
		return getLoanResponse;
	}

	@RequestMapping(value = "/getloanhistory", method = RequestMethod.POST, headers = Constants.CONTENT_TYPE_DEFAULT_VALUE)
	public LoanHistoryResponse getLoanHistory(@RequestBody String data,
			@RequestHeader(value = Constants.TOKEN_KEY, defaultValue = Constants.TOKEN_DEFAULT_VALUE) String token,
			@RequestHeader(value = Constants.LANGUAGE_KEY, defaultValue = Constants.LANGUAGE_DEFAULT_VALUE) String lang,
			@RequestHeader(value = Constants.USER_AGENT_KEY, defaultValue = Constants.USER_AGENT_DEFAULT_VALUE) String userAgent,
			@RequestHeader(value = Constants.SUBSCRIBER_TYPE_KEY, defaultValue = Constants.SUBSCRIBER_TYPE_DEFAULT_VALUE) String userType,
			@RequestHeader(value = Constants.TARIFF_TYPE_KEY, defaultValue = Constants.TARIFF_TYPE_DEFAULT_VALUE) String tariffType,
			HttpServletRequest servletRequest) throws ClassNotFoundException, SQLException, JsonParseException,
			JsonMappingException, IOException, JMSException {
		ObjectMapper mapper = new ObjectMapper();
		LoanHistoryRequest loanHistoryRequest = new LoanHistoryRequest();
		LoanHistoryResponse loanHistoryResponse = new LoanHistoryResponse();
		FinancialServicesBusiness financialServicesBusiness = new FinancialServicesBusiness();

		String TRANSACTION_NAME = Transactions.LOAN_REQUEST_HISTORY_TRANSACTION_NAME + " CONTROLLER";
		String requestTime = Utilities.getReportDateTime();
		try {

			String msisdn = Utilities.getValueFromJSON(Utilities.splitToken(Utilities.decodeString(token)).toString(),
					Constants.MSISDN_KEY);

			// Populating report object before processing business logic.
			loanHistoryResponse.setLogsReport(Utilities.preBusinessReportLoggingData(token, msisdn, TRANSACTION_NAME,
					servletRequest.getRemoteAddr(), userAgent, userType, tariffType, data, lang,
					loanHistoryResponse.getLogsReport()));

			data = Utilities.addAttributesToRequest(data, msisdn, servletRequest, lang, userAgent);

			Utilities.printInfoLog(msisdn + "-Request Landed in-" + TRANSACTION_NAME, logger);
			Utilities.printInfoLog(msisdn + "-Request Data-" + data, logger);

			loanHistoryRequest = mapper.readValue(data, LoanHistoryRequest.class);

			String requestValidationStatus = Validator.validateRequest(msisdn, loanHistoryRequest);

			if (requestValidationStatus.equalsIgnoreCase(Constants.VALIDATION_SUCCESS_CODE)) {

				loanHistoryResponse = financialServicesBusiness.getLoanHistory(msisdn, loanHistoryRequest,
						loanHistoryResponse);
			} else {
				loanHistoryResponse.setCallStatus(Constants.Call_Status_False);
				loanHistoryResponse.setResultCode(Constants.VALIDATION_FAILURE_CODE);
				loanHistoryResponse.setResultDesc(requestValidationStatus);
			}

			loanHistoryResponse.getLogsReport().setResponseCode(loanHistoryResponse.getResultCode());
			loanHistoryResponse.getLogsReport().setRequestTime(requestTime);
			loanHistoryResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
			loanHistoryResponse.getLogsReport().setResponse(mapper.writeValueAsString(loanHistoryResponse));

			// Sending report log into queue.
			Utilities.prepareLogReportForQueue(loanHistoryResponse.getLogsReport());

			Utilities.printInfoLog(msisdn + "-Response Returned from-" + TRANSACTION_NAME + "-"
					+ mapper.writeValueAsString(loanHistoryResponse), logger);
		} catch (Exception e) {
			Utilities.printErrorLog(Utilities.convertStackTraceToString(e), logger);
			loanHistoryResponse.setCallStatus(Constants.Call_Status_False);

			loanHistoryResponse.setResultCode(Constants.EXCEPTION_CODE);
			loanHistoryResponse
					.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("unexpected.error", lang));

			loanHistoryResponse.getLogsReport().setResponseCode(loanHistoryResponse.getResultCode());
			loanHistoryResponse.getLogsReport().setRequestTime(requestTime);
			loanHistoryResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
			loanHistoryResponse.getLogsReport().setResponse(mapper.writeValueAsString(loanHistoryResponse));

			// Sending report log into queue.
			Utilities.prepareLogReportForQueue(loanHistoryResponse.getLogsReport());

		}
		return loanHistoryResponse;
	}

	@RequestMapping(value = "/getpaymenthistory", method = RequestMethod.POST, headers = Constants.CONTENT_TYPE_DEFAULT_VALUE)
	public PaymentHistoryResponse getPaymentHistory(@RequestBody String data,
			@RequestHeader(value = Constants.TOKEN_KEY, defaultValue = Constants.TOKEN_DEFAULT_VALUE) String token,
			@RequestHeader(value = Constants.LANGUAGE_KEY, defaultValue = Constants.LANGUAGE_DEFAULT_VALUE) String lang,
			@RequestHeader(value = Constants.USER_AGENT_KEY, defaultValue = Constants.USER_AGENT_DEFAULT_VALUE) String userAgent,
			@RequestHeader(value = Constants.SUBSCRIBER_TYPE_KEY, defaultValue = Constants.SUBSCRIBER_TYPE_DEFAULT_VALUE) String userType,
			@RequestHeader(value = Constants.TARIFF_TYPE_KEY, defaultValue = Constants.TARIFF_TYPE_DEFAULT_VALUE) String tariffType,
			HttpServletRequest servletRequest) throws ClassNotFoundException, SQLException, JsonParseException,
			JsonMappingException, IOException, JMSException {

		ObjectMapper mapper = new ObjectMapper();
		PaymentHistoryRequest paymentHistoryRequest = new PaymentHistoryRequest();
		PaymentHistoryResponse paymentHistoryResponse = new PaymentHistoryResponse();
		FinancialServicesBusiness financialServicesBusiness = new FinancialServicesBusiness();

		String TRANSACTION_NAME = Transactions.LOAN_PAYMENT_HISTORY_TRANSACTION_NAME + " CONTROLLER";
		String requestTime = Utilities.getReportDateTime();
		try {

			String msisdn = Utilities.getValueFromJSON(Utilities.splitToken(Utilities.decodeString(token)).toString(),
					Constants.MSISDN_KEY);

			// Populating report object before processing business logic.
			paymentHistoryResponse.setLogsReport(Utilities.preBusinessReportLoggingData(token, msisdn, TRANSACTION_NAME,
					servletRequest.getRemoteAddr(), userAgent, userType, tariffType, data, lang,
					paymentHistoryResponse.getLogsReport()));

			data = Utilities.addAttributesToRequest(data, msisdn, servletRequest, lang, userAgent);

			Utilities.printInfoLog(msisdn + "-Request Landed in-" + TRANSACTION_NAME, logger);
			Utilities.printInfoLog(msisdn + "-Request Data-" + data, logger);

			paymentHistoryRequest = mapper.readValue(data, PaymentHistoryRequest.class);

			String requestValidationStatus = Validator.validateRequest(msisdn, paymentHistoryRequest);

			if (requestValidationStatus.equalsIgnoreCase(Constants.VALIDATION_SUCCESS_CODE)) {
				paymentHistoryResponse = financialServicesBusiness.getPaymentHistory(msisdn, paymentHistoryRequest,
						paymentHistoryResponse);
			} else {
				paymentHistoryResponse.setCallStatus(Constants.Call_Status_False);
				paymentHistoryResponse.setResultCode(Constants.VALIDATION_FAILURE_CODE);
				paymentHistoryResponse.setResultDesc(requestValidationStatus);
			}

			paymentHistoryResponse.getLogsReport().setResponseCode(paymentHistoryResponse.getResultCode());
			paymentHistoryResponse.getLogsReport().setRequestTime(requestTime);
			paymentHistoryResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
			paymentHistoryResponse.getLogsReport().setResponse(mapper.writeValueAsString(paymentHistoryResponse));

			// Sending report log into queue.
			Utilities.prepareLogReportForQueue(paymentHistoryResponse.getLogsReport());

			Utilities.printInfoLog(msisdn + "-Response Returned from-" + TRANSACTION_NAME + "-"
					+ mapper.writeValueAsString(paymentHistoryResponse), logger);
		} catch (Exception e) {
			Utilities.printErrorLog(Utilities.convertStackTraceToString(e), logger);
			paymentHistoryResponse.setCallStatus(Constants.Call_Status_False);

			paymentHistoryResponse.setResultCode(Constants.EXCEPTION_CODE);
			paymentHistoryResponse
					.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("unexpected.error", lang));

			paymentHistoryResponse.getLogsReport().setResponseCode(paymentHistoryResponse.getResultCode());
			paymentHistoryResponse.getLogsReport().setRequestTime(requestTime);
			paymentHistoryResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
			paymentHistoryResponse.getLogsReport().setResponse(mapper.writeValueAsString(paymentHistoryResponse));

			// Sending report log into queue.
			Utilities.prepareLogReportForQueue(paymentHistoryResponse.getLogsReport());
		}
		return paymentHistoryResponse;
	}

	@RequestMapping(value = "/requestmoney", method = RequestMethod.POST, headers = Constants.CONTENT_TYPE_DEFAULT_VALUE)
	public RequestMoneyResponse requestMoney(@RequestBody String data,
			@RequestHeader(value = Constants.TOKEN_KEY, defaultValue = Constants.TOKEN_DEFAULT_VALUE) String token,
			@RequestHeader(value = Constants.LANGUAGE_KEY, defaultValue = Constants.LANGUAGE_DEFAULT_VALUE) String lang,
			@RequestHeader(value = Constants.USER_AGENT_KEY, defaultValue = Constants.USER_AGENT_DEFAULT_VALUE) String userAgent,
			@RequestHeader(value = Constants.SUBSCRIBER_TYPE_KEY, defaultValue = Constants.SUBSCRIBER_TYPE_DEFAULT_VALUE) String userType,
			@RequestHeader(value = Constants.TARIFF_TYPE_KEY, defaultValue = Constants.TARIFF_TYPE_DEFAULT_VALUE) String tariffType,
			HttpServletRequest servletRequest) throws ClassNotFoundException, SQLException, JsonParseException,
			JsonMappingException, IOException, JMSException {

		ObjectMapper mapper = new ObjectMapper();
		RequestMoneyRequest requestMoneyRequest = new RequestMoneyRequest();
		RequestMoneyResponse requestMoneyResponse = new RequestMoneyResponse();
		FinancialServicesBusiness financialServicesBusiness = new FinancialServicesBusiness();

		String TRANSACTION_NAME = Transactions.REQUEST_MONEY_TRANSACTION_NAME + " CONTROLLER";
		String requestTime = Utilities.getReportDateTime();
		try {

			String msisdn = Utilities.getValueFromJSON(Utilities.splitToken(Utilities.decodeString(token)).toString(),
					Constants.MSISDN_KEY);

			// Populating report object before processing business logic.
			requestMoneyResponse.setLogsReport(Utilities.preBusinessReportLoggingData(token, msisdn, TRANSACTION_NAME,
					servletRequest.getRemoteAddr(), userAgent, userType, tariffType, data, lang,
					requestMoneyResponse.getLogsReport()));

			data = Utilities.addAttributesToRequest(data, msisdn, servletRequest, lang, userAgent);

			Utilities.printInfoLog(msisdn + "-Request Landed in-" + TRANSACTION_NAME, logger);
			Utilities.printInfoLog(msisdn + "-Request Data-" + data, logger);

			requestMoneyRequest = mapper.readValue(data, RequestMoneyRequest.class);

			String requestValidationStatus = Validator.validateRequest(msisdn, requestMoneyRequest);

			if (requestValidationStatus.equalsIgnoreCase(Constants.VALIDATION_SUCCESS_CODE)) {
				requestMoneyResponse = financialServicesBusiness.requestMoneyBusiness(msisdn, requestMoneyRequest,
						requestMoneyResponse);
			} else {
				requestMoneyResponse.setCallStatus(Constants.Call_Status_False);
				requestMoneyResponse.setResultCode(Constants.VALIDATION_FAILURE_CODE);
				requestMoneyResponse.setResultDesc(requestValidationStatus);
			}

			requestMoneyResponse.getLogsReport().setResponseCode(requestMoneyResponse.getResultCode());
			requestMoneyResponse.getLogsReport().setRequestTime(requestTime);
			requestMoneyResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
			requestMoneyResponse.getLogsReport().setResponse(mapper.writeValueAsString(requestMoneyResponse));

			// Sending report log into queue.
			Utilities.prepareLogReportForQueue(requestMoneyResponse.getLogsReport());

			Utilities.printInfoLog(msisdn + "-Response Returned from-" + TRANSACTION_NAME + "-"
					+ mapper.writeValueAsString(requestMoneyResponse), logger);
		} catch (Exception e) {
			Utilities.printErrorLog(Utilities.convertStackTraceToString(e), logger);
			requestMoneyResponse.setCallStatus(Constants.Call_Status_False);

			requestMoneyResponse.setResultCode(Constants.EXCEPTION_CODE);
			requestMoneyResponse
					.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("unexpected.error", lang));

			requestMoneyResponse.getLogsReport().setResponseCode(requestMoneyResponse.getResultCode());
			requestMoneyResponse.getLogsReport().setRequestTime(requestTime);
			requestMoneyResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
			requestMoneyResponse.getLogsReport().setResponse(mapper.writeValueAsString(requestMoneyResponse));

			// Sending report log into queue.
			Utilities.prepareLogReportForQueue(requestMoneyResponse.getLogsReport());
		}
		return requestMoneyResponse;
	}
}