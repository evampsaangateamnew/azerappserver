/**
 * 
 */
package com.evampsaanga.azerfon.controllers;

import java.io.IOException;
import java.net.SocketException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.jms.JMSException;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.log4j.Logger;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
//import com.sun.xml.internal.bind.v2.runtime.unmarshaller.XsiNilLoader.Array;
import com.evampsaanga.azerfon.business.CustomerServicesBusiness;
import com.evampsaanga.azerfon.business.MySubscriptionsBusiness;
import com.evampsaanga.azerfon.common.messagesmappings.GetMessagesMappings;
import com.evampsaanga.azerfon.common.utilities.Constants;
import com.evampsaanga.azerfon.common.utilities.GetConfigurations;
import com.evampsaanga.azerfon.common.utilities.Transactions;
import com.evampsaanga.azerfon.common.utilities.Utilities;
import com.evampsaanga.azerfon.common.utilities.Validator;
import com.evampsaanga.azerfon.models.customerservices.customerinfo.CustomerInfoRequest;
import com.evampsaanga.azerfon.models.customerservices.customerinfo.CustomerInfoResponseData;
import com.evampsaanga.azerfon.models.mysubscriptions.getsubscriptions.MySubscriptionsRequest;
import com.evampsaanga.azerfon.models.supplementaryofferings.getsupplementaryofferings.SupplementaryOfferingsResponse;
import com.evampsaanga.azerfon.models.supplementaryofferings.getsupplementaryofferings.commonofferings.SupplementryOfferingsData;

/**
 * @author Evamp & Saanga
 *
 */

@RestController
@RequestMapping("/mysubscriptions")
public class MySubscriptionsController {

    static Logger logger = Logger.getLogger(MySubscriptionsController.class);

    @RequestMapping(value = "/getsubscriptions", method = RequestMethod.POST, headers = Constants.CONTENT_TYPE_DEFAULT_VALUE)
    public SupplementaryOfferingsResponse getSubscriptions(@RequestBody String data,
	    @RequestHeader(value = Constants.TOKEN_KEY, defaultValue = Constants.TOKEN_DEFAULT_VALUE) String token,
	    @RequestHeader(value = Constants.LANGUAGE_KEY, defaultValue = Constants.LANGUAGE_DEFAULT_VALUE) String lang,
	    @RequestHeader(value = Constants.USER_AGENT_KEY, defaultValue = Constants.USER_AGENT_DEFAULT_VALUE) String userAgent,
	    @RequestHeader(value = Constants.SUBSCRIBER_TYPE_KEY, defaultValue = Constants.SUBSCRIBER_TYPE_DEFAULT_VALUE) String userType,
	    @RequestHeader(value = Constants.TARIFF_TYPE_KEY, defaultValue = Constants.TARIFF_TYPE_DEFAULT_VALUE) String tariffType,
	    HttpServletRequest servletRequest) throws ClassNotFoundException, SQLException, IOException, JMSException {
	ObjectMapper mapper = new ObjectMapper();
	MySubscriptionsBusiness mySubscriptionsBusiness = new MySubscriptionsBusiness();
	MySubscriptionsRequest mySubscriptionsRequest = new MySubscriptionsRequest();
	SupplementaryOfferingsResponse mySubscriptionsResponse = new SupplementaryOfferingsResponse();

	String TRANSACTION_NAME = Transactions.MYSUBSCRIPTION_TRANSACTION_NAME + " CONTROLLER";
	String requestTime = Utilities.getReportDateTime();
	try {
	    String msisdn = Utilities.getValueFromJSON(Utilities.splitToken(Utilities.decodeString(token)).toString(),
		    Constants.MSISDN_KEY);

	    // Populating report object before processing business logic.
	    mySubscriptionsResponse.setLogsReport(Utilities.preBusinessReportLoggingData(token, msisdn,
		    TRANSACTION_NAME, servletRequest.getRemoteAddr(), userAgent, userType, tariffType, data, lang,
		    mySubscriptionsResponse.getLogsReport()));

	    data = Utilities.addAttributesToRequest(data, msisdn, servletRequest, lang, userAgent);

	    Utilities.printInfoLog(msisdn + "-Request Landed in-" + TRANSACTION_NAME, logger);
	    Utilities.printInfoLog(msisdn + "-Request Data-" + data, logger);

	    mySubscriptionsRequest = mapper.readValue(data, MySubscriptionsRequest.class);

	    String requestValidationStatus = Validator.validateRequest(msisdn, mySubscriptionsRequest);

	    if (requestValidationStatus.equalsIgnoreCase(Constants.VALIDATION_SUCCESS_CODE)) {

		mySubscriptionsResponse = mySubscriptionsBusiness.getSubscriptions(msisdn, mySubscriptionsRequest,
			mySubscriptionsResponse);
		
		/*SupplementaryOfferingsResponse mySubscriptionsResponseDeep=null;
		 mySubscriptionsResponseDeep = (SupplementaryOfferingsResponse) mySubscriptionsResponse.clone();*/
		
		//sortSubscriptionOfferings(mySubscriptionsResponseDeep);
		 Utilities.printInfoLog(msisdn + "---------Usage Controller MySbscriptions BEFORE--------------- allowedCheckImlp" + data, logger);
	    	SupplementaryOfferingsResponse mySubscriptionsResponseDeep=null;
	    	 mySubscriptionsResponseDeep = (SupplementaryOfferingsResponse) mySubscriptionsResponse.clone();
	    	 mySubscriptionsResponseDeep= allowedCheckImlp( mySubscriptionsResponseDeep, mySubscriptionsResponse ,mySubscriptionsRequest);
		 sortSubscriptionOfferings(mySubscriptionsResponseDeep);
		 Utilities.printDebugLog(mySubscriptionsRequest.getMsisdn()
 				+ "-Usage Controller MySbscriptions BefoeReturn: " + mapper.writeValueAsString(mySubscriptionsResponseDeep),
 				logger);
	    	 return mySubscriptionsResponseDeep;
	    } else {
		mySubscriptionsResponse.setCallStatus(Constants.Call_Status_False);
		mySubscriptionsResponse.setResultCode(Constants.VALIDATION_FAILURE_CODE);
		mySubscriptionsResponse.setResultDesc(requestValidationStatus);
	    }

	    mySubscriptionsResponse.getLogsReport().setResponseCode(mySubscriptionsResponse.getResultCode());
	    mySubscriptionsResponse.getLogsReport().setRequestTime(requestTime);
	    mySubscriptionsResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
	    mySubscriptionsResponse.getLogsReport().setResponse(mapper.writeValueAsString(mySubscriptionsResponse));

	    // Sending report log into queue.
	    Utilities.prepareLogReportForQueue(mySubscriptionsResponse.getLogsReport());

	    Utilities.printInfoLog(msisdn + "-Response Returned from-" + TRANSACTION_NAME + "-"
		    + mapper.writeValueAsString(mySubscriptionsResponse), logger);
	} catch (Exception e) {
	    Utilities.printErrorLog(Utilities.convertStackTraceToString(e), logger);
	    mySubscriptionsResponse.setCallStatus(Constants.Call_Status_False);

	    mySubscriptionsResponse.setResultCode(Constants.EXCEPTION_CODE);
	    mySubscriptionsResponse
		    .setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("unexpected.error", lang));

	    mySubscriptionsResponse.getLogsReport().setResponseCode(mySubscriptionsResponse.getResultCode());
	    mySubscriptionsResponse.getLogsReport().setRequestTime(requestTime);
	    mySubscriptionsResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
	    mySubscriptionsResponse.getLogsReport().setResponse(mapper.writeValueAsString(mySubscriptionsResponse));

	    // Sending report log into queue.
	    Utilities.prepareLogReportForQueue(mySubscriptionsResponse.getLogsReport());

	}
	return mySubscriptionsResponse;
    }

    private void sortSubscriptionOfferings(SupplementaryOfferingsResponse supplementaryOfferingsResponse) {

    	
	if (supplementaryOfferingsResponse != null && supplementaryOfferingsResponse.getData() != null) {

	    // Sorting Internet offers
	    if (supplementaryOfferingsResponse.getData().getInternet() != null) {
		List<SupplementryOfferingsData> offers = supplementaryOfferingsResponse.getData().getInternet()
			.getOffers();
		if (offers != null && offers.size() > 0) {
		    Collections.sort(offers, new Comparator<SupplementryOfferingsData>() {
			@Override
			public int compare(SupplementryOfferingsData o1, SupplementryOfferingsData o2) {
			    if (o1.getHeader() != null && o2.getHeader() != null
				    && o1.getHeader().getSortOrderMS() != null
				    && o2.getHeader().getSortOrderMS() != null) {
				return o1.getHeader().getSortOrderMS().compareTo(o2.getHeader().getSortOrderMS());
			    } else {
				return Constants.MAX_INT;
			    }
			}
		    });
		  // Collections.reverse(offers);
		}
	    }
         
	    // Sorting Compaign offers
	    if (supplementaryOfferingsResponse.getData().getCampaign() != null) {
		List<SupplementryOfferingsData> offers = supplementaryOfferingsResponse.getData().getCampaign()
			.getOffers();
		if (offers != null && offers.size() > 0) {
		    Collections.sort(offers, new Comparator<SupplementryOfferingsData>() {
			@Override
			public int compare(SupplementryOfferingsData o1, SupplementryOfferingsData o2) {
			    if (o1.getHeader() != null && o2.getHeader() != null
				    && o1.getHeader().getSortOrderMS() != null
				    && o2.getHeader().getSortOrderMS() != null) {
				return o1.getHeader().getSortOrderMS().compareTo(o2.getHeader().getSortOrderMS());
			    } else {
				return Constants.MAX_INT;
			    }
			}
		    });
		  //  Collections.reverse(offers);
		}
	    }

	    // Sorting SMS offers
	    if (supplementaryOfferingsResponse.getData().getSms() != null) {
		List<SupplementryOfferingsData> offers = supplementaryOfferingsResponse.getData().getSms().getOffers();
		if (offers != null && offers.size() > 0) {
		    Collections.sort(offers, new Comparator<SupplementryOfferingsData>() {
			@Override
			public int compare(SupplementryOfferingsData o1, SupplementryOfferingsData o2) {
			    if (o1.getHeader() != null && o2.getHeader() != null
				    && o1.getHeader().getSortOrderMS() != null
				    && o2.getHeader().getSortOrderMS() != null) {
				return o1.getHeader().getSortOrderMS().compareTo(o2.getHeader().getSortOrderMS());
			    } else {
				return Constants.MAX_INT;
			    }
			}
		    });
		   // Collections.reverse(offers);
		}
	    }

	    // Sorting SMS offers
	    if (supplementaryOfferingsResponse.getData().getCall() != null) {
		List<SupplementryOfferingsData> offers = supplementaryOfferingsResponse.getData().getCall().getOffers();
		if (offers != null && offers.size() > 0) {
		    Collections.sort(offers, new Comparator<SupplementryOfferingsData>() {
			@Override
			public int compare(SupplementryOfferingsData o1, SupplementryOfferingsData o2) {
			    if (o1.getHeader() != null && o2.getHeader() != null
				    && o1.getHeader().getSortOrderMS() != null
				    && o2.getHeader().getSortOrderMS() != null) {
				return o1.getHeader().getSortOrderMS().compareTo(o2.getHeader().getSortOrderMS());
			    } else {
				return Constants.MAX_INT;
			    }
			}
		    });
		 //   Collections.reverse(offers);
		}
	    }

	    // Sorting TM offers
	    if (supplementaryOfferingsResponse.getData().getTm() != null) {
		List<SupplementryOfferingsData> offers = supplementaryOfferingsResponse.getData().getTm().getOffers();
		if (offers != null && offers.size() > 0) {
		    Collections.sort(offers, new Comparator<SupplementryOfferingsData>() {
			@Override
			public int compare(SupplementryOfferingsData o1, SupplementryOfferingsData o2) {
			    if (o1.getHeader() != null && o2.getHeader() != null
				    && o1.getHeader().getSortOrderMS() != null
				    && o2.getHeader().getSortOrderMS() != null) {
				return o1.getHeader().getSortOrderMS().compareTo(o2.getHeader().getSortOrderMS());
			    } else {
				return Constants.MAX_INT;
			    }
			}
		    });
		 //   Collections.reverse(offers);
		}
	    }

	    // Sorting Hybrid offers
	    if (supplementaryOfferingsResponse.getData().getHybrid() != null) {
		List<SupplementryOfferingsData> offers = supplementaryOfferingsResponse.getData().getHybrid()
			.getOffers();
		if (offers != null && offers.size() > 0) {
		    Collections.sort(offers, new Comparator<SupplementryOfferingsData>() {
			@Override
			public int compare(SupplementryOfferingsData o1, SupplementryOfferingsData o2) {
			    if (o1.getHeader() != null && o2.getHeader() != null
				    && o1.getHeader().getSortOrderMS() != null
				    && o2.getHeader().getSortOrderMS() != null) {
				return o1.getHeader().getSortOrderMS().compareTo(o2.getHeader().getSortOrderMS());
			    } else {
				return Constants.MAX_INT;
			    }
			}
		    });
		   // Collections.reverse(offers);
		}
	    }

	    // Sorting Roaming offers
	    if (supplementaryOfferingsResponse.getData().getRoaming() != null) {
		List<SupplementryOfferingsData> offers = supplementaryOfferingsResponse.getData().getRoaming()
			.getOffers();
		if (offers != null && offers.size() > 0) {
		    Collections.sort(offers, new Comparator<SupplementryOfferingsData>() {
			@Override
			public int compare(SupplementryOfferingsData o1, SupplementryOfferingsData o2) {
			    if (o1.getHeader() != null && o2.getHeader() != null
				    && o1.getHeader().getSortOrderMS() != null
				    && o2.getHeader().getSortOrderMS() != null) {
				return o1.getHeader().getSortOrderMS().compareTo(o2.getHeader().getSortOrderMS());
			    } else {
				return Constants.MAX_INT;
			    }
			}
		    });
		    
		}
	    }

	}
    }

    @RequestMapping(value = "/getsubscriptionsforportal", method = RequestMethod.POST, headers = Constants.CONTENT_TYPE_DEFAULT_VALUE)
    public SupplementaryOfferingsResponse getSubscriptionsForPortal(@RequestBody String data,
	    @RequestHeader(value = "credentials") String token,
	    @RequestHeader(value = Constants.LANGUAGE_KEY, defaultValue = Constants.LANGUAGE_DEFAULT_VALUE) String lang,
	    @RequestHeader(value = Constants.USER_AGENT_KEY, defaultValue = Constants.USER_AGENT_DEFAULT_VALUE) String userAgent,
	    HttpServletRequest servletRequest) throws ClassNotFoundException, SQLException, JsonParseException,
	    JsonMappingException, IOException, JMSException {
	ObjectMapper mapper = new ObjectMapper();
	MySubscriptionsBusiness mySubscriptionsBusiness = new MySubscriptionsBusiness();
	MySubscriptionsRequest mySubscriptionsRequest = new MySubscriptionsRequest();
	SupplementaryOfferingsResponse mySubscriptionsResponse = new SupplementaryOfferingsResponse();

	String TRANSACTION_NAME = Transactions.MYSUBSCRIPTION__FOR_PORTAL_TRANSACTION_NAME + " CONTROLLER";
	String requestTime = Utilities.getReportDateTime();
	try {
	    String msisdn = Utilities.getValueFromJSON(data, Constants.MSISDN_KEY);

	    // Populating report object before processing business logic.
	    mySubscriptionsResponse.setLogsReport(
		    Utilities.preBusinessReportLoggingData("", msisdn, TRANSACTION_NAME, servletRequest.getRemoteAddr(),
			    userAgent, "", "", data, lang, mySubscriptionsResponse.getLogsReport()));

	    data = Utilities.addAttributesToRequest(data, msisdn, servletRequest, lang, userAgent);

	    Utilities.printInfoLog(msisdn + "-Request Landed in-" + TRANSACTION_NAME, logger);
	    Utilities.printInfoLog(msisdn + "-Request Data-" + data, logger);

	    mySubscriptionsRequest = mapper.readValue(data, MySubscriptionsRequest.class);

	    String requestValidationStatus = Validator.validateRequest(msisdn, mySubscriptionsRequest);

	    if (requestValidationStatus.equalsIgnoreCase(Constants.VALIDATION_SUCCESS_CODE)) {

		mySubscriptionsResponse = mySubscriptionsBusiness.getSubscriptions(msisdn, mySubscriptionsRequest,
			mySubscriptionsResponse);
	    } else {
		mySubscriptionsResponse.setCallStatus(Constants.Call_Status_False);
		mySubscriptionsResponse.setResultCode(Constants.VALIDATION_FAILURE_CODE);
		mySubscriptionsResponse.setResultDesc(requestValidationStatus);
	    }

	    mySubscriptionsResponse.getLogsReport().setResponseCode(mySubscriptionsResponse.getResultCode());
	    mySubscriptionsResponse.getLogsReport().setRequestTime(requestTime);
	    mySubscriptionsResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
	    mySubscriptionsResponse.getLogsReport().setResponse(mapper.writeValueAsString(mySubscriptionsResponse));

	    // Sending report log into queue.
	    Utilities.prepareLogReportForQueue(mySubscriptionsResponse.getLogsReport());

	    Utilities.printInfoLog(msisdn + "-Response Returned from-" + TRANSACTION_NAME + "-"
		    + mapper.writeValueAsString(mySubscriptionsResponse), logger);
	} catch (Exception e) {
	    Utilities.printErrorLog(Utilities.convertStackTraceToString(e), logger);
	    mySubscriptionsResponse.setCallStatus(Constants.Call_Status_False);

	    mySubscriptionsResponse.setResultCode(Constants.EXCEPTION_CODE);
	    mySubscriptionsResponse
		    .setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("unexpected.error", lang));

	    mySubscriptionsResponse.getLogsReport().setResponseCode(mySubscriptionsResponse.getResultCode());
	    mySubscriptionsResponse.getLogsReport().setRequestTime(requestTime);
	    mySubscriptionsResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
	    mySubscriptionsResponse.getLogsReport().setResponse(mapper.writeValueAsString(mySubscriptionsResponse));

	    // Sending report log into queue.
	    Utilities.prepareLogReportForQueue(mySubscriptionsResponse.getLogsReport());

	}
	return mySubscriptionsResponse;
    }
    public static CustomerInfoResponseData customerDataResponse( MySubscriptionsRequest mySubscriptionsRequest)
    {
    ArrayList<String> offeringIdsCustomer = new ArrayList<String>();
    CustomerInfoResponseData custInfoResponse = new CustomerInfoResponseData();
    	try {
    	Utilities.printDebugLog(mySubscriptionsRequest.getMsisdn() + "-Received suplementaryOfferings Methos: -" ,
				logger);
    	
    	CustomerInfoRequest customerInfoRequest = new CustomerInfoRequest();

    	customerInfoRequest.setChannel(mySubscriptionsRequest.getChannel());
    	customerInfoRequest.setiP(mySubscriptionsRequest.getiP());
    	customerInfoRequest.setLang(mySubscriptionsRequest.getLang());
    	customerInfoRequest.setMsisdn(mySubscriptionsRequest.getMsisdn());
      
    	CustomerServicesBusiness customerBusiness = new CustomerServicesBusiness();
    	
			custInfoResponse = customerBusiness.getCustomerData(mySubscriptionsRequest.getMsisdn(), "", customerInfoRequest, custInfoResponse,
					Constants.TOKEN_CREATION_CALL_TYPE_EXTERNAL, "", "");
		
    	
    	
    	} catch (SocketException e) {
			// TODO Auto-generated catch block
    		logger.error("ERROR:", e);
		}
    	catch (Exception e) {
			// TODO Auto-generated catch block
    		logger.error("ERROR:", e);
		}
    	return custInfoResponse;
   
    }
    public static SupplementaryOfferingsResponse allowedCheckImlp(SupplementaryOfferingsResponse mySubscriptionsResponseDeep,SupplementaryOfferingsResponse mySubscriptionsResponse ,MySubscriptionsRequest mySubscriptionsRequest)
    {
    	System.out.println("---------Usage Controller MySbscriptions IN IN--------------- allowedCheckImlp");
    	ObjectMapper mapper=new ObjectMapper();

    	
    	
    	try {
    		Utilities.printInfoLog( "---------Usage Controller MySbscriptions IN IN--------------- allowedCheckImlp" , logger);
    		
    		
    		ArrayList<String> offeringIdsCustomer=new ArrayList<String>();
    		ArrayList<String> specialIds=new ArrayList<String>();
    		CustomerInfoResponseData custInfoResponse=customerDataResponse(mySubscriptionsRequest);
   
    		if (custInfoResponse.getResultCode().equalsIgnoreCase(Constants.ESB_SUCCESS_CODE)) {

    			specialIds=custInfoResponse.getCustomerData().getSpecialOffersTariffData().getOffersSpecialIds();
        		for (int i = 0; i < custInfoResponse.getSupplementaryOfferingList().size(); i++) {
        			offeringIdsCustomer.add(custInfoResponse.getSupplementaryOfferingList().get(i).getOfferingId());
        		}
        		
    				Utilities.printDebugLog(mySubscriptionsRequest.getMsisdn() + "-Received From Subscription customerDataInternalLogic OfferingIdS: -" + offeringIdsCustomer,
    						logger);	
        	}
    		

    		Utilities.printDebugLog(mySubscriptionsRequest.getMsisdn()
    				+ "-Usage Controller MySbscriptions customer: " + offeringIdsCustomer,
    				logger);
    		
		
		 
		 //START OF Logic for Internet
		 for(int i=0; i<mySubscriptionsResponse.getData().getInternet().getOffers().size();i++)
			{
				String offeringId=mySubscriptionsResponse.getData().getInternet().getOffers().get(i).getHeader().getOfferingId();
				String btnDeActivate=mySubscriptionsResponse.getData().getInternet().getOffers().get(i).getHeader().getBtnDeactivate();
				String btnAllowedFor=mySubscriptionsResponse.getData().getInternet().getOffers().get(i).getHeader().getAllowedForRenew();
				String btnreNew=mySubscriptionsResponse.getData().getInternet().getOffers().get(i).getHeader().getBtnRenew();
				if(offeringIdsCustomer.contains(offeringId))
				{
					Utilities.printDebugLog(mySubscriptionsRequest.getMsisdn()+" :"
							+ offeringId+"-Internet Usage Controller MySbscriptions custoemrData  offeringId: " + offeringId,
							logger);
					
					Utilities.printDebugLog(mySubscriptionsRequest.getMsisdn()+" :"
							+ offeringId+ "-Internet Usage Controller MySbscriptions in AllowedFor: " + btnAllowedFor,
							logger);
					Utilities.printDebugLog(mySubscriptionsRequest.getMsisdn()
							+ "-Internet Usage Controller MySbscriptions data in btRenew: " + btnreNew,
							logger);
					
					
					if(btnDeActivate.equalsIgnoreCase("1"))
					{
						mySubscriptionsResponseDeep.getData().getInternet().getOffers().get(i).getHeader().setBtnDeactivate("1");
					}
					else
					{
						mySubscriptionsResponseDeep.getData().getInternet().getOffers().get(i).getHeader().setBtnDeactivate("0");
					}
					if(btnreNew.equalsIgnoreCase("1"))
					{
					
						
						List<String> AllowedFor = new ArrayList<String>(Arrays.asList(btnAllowedFor.split(",")));
						if(btnAllowedFor==null || btnAllowedFor.equalsIgnoreCase("null") || !CollectionUtils.intersection(offeringIdsCustomer, AllowedFor).isEmpty())
						{
							
							mySubscriptionsResponseDeep.getData().getInternet().getOffers().get(i).getHeader().setBtnRenew("1");
							Utilities.printDebugLog(mySubscriptionsRequest.getMsisdn()
									+ "-Internet Usage Controller MySbscriptions in AllowedFor exist in customerData: "+mySubscriptionsResponseDeep.getData().getInternet().getOffers().get(i).getHeader().getBtnRenew() ,
									logger);
						}
						else
						{
							mySubscriptionsResponseDeep.getData().getInternet().getOffers().get(i).getHeader().setBtnRenew("0");
							Utilities.printDebugLog(mySubscriptionsRequest.getMsisdn()
									+ "-Usage Controller MySbscriptions in AllowedFor DOEST NOT exist in customerData: "+mySubscriptionsResponseDeep.getData().getInternet().getOffers().get(i).getHeader().getBtnRenew() ,
									logger);
						}
					}
				}
				else
				{
					
					mySubscriptionsResponseDeep.getData().getInternet().getOffers().get(i).getHeader().setBtnDeactivate("0");
					mySubscriptionsResponseDeep.getData().getInternet().getOffers().get(i).getHeader().setBtnRenew("0");
					Utilities.printDebugLog(mySubscriptionsRequest.getMsisdn()+ " :"+offeringId 
							+ "-Usage Controller MySbscriptions OFFER DOES NOT exist in customerData: "+mySubscriptionsResponseDeep.getData().getInternet().getOffers().get(i).getHeader().getBtnDeactivate() ,
							logger);
				}
				//END OF CUSTOEMR DATA CHECK
				//START of price check for special offers,  If any offer in mysusbcripions is a special offers we have to show price from DB.
				if(specialIds.contains(offeringId))
				{
					Utilities.printDebugLog(mySubscriptionsRequest.getMsisdn()+ " :"+offeringId 
							+ "-Usage Controller MySbscriptions Contains SpecialIds: "+specialIds + "OfferingID :"+ offeringId,
							logger);
					String mrcFinal= GetConfigurations
							.getMigrationPrices(mySubscriptionsRequest.getMsisdn(),"Offer",custInfoResponse.getCustomerData().getGroupIds(),offeringId);
					Utilities.printDebugLog(mySubscriptionsRequest.getMsisdn()+"Usage Controller MySbscriptions Contains SpecialIds: FROM DB- MRC FINAL" + mrcFinal, logger);
					if(mrcFinal!=null && !mrcFinal.isEmpty())
					{
						mySubscriptionsResponseDeep.getData().getInternet().getOffers().get(i).getHeader().setPrice(mrcFinal);
					}
					else {
						mySubscriptionsResponseDeep.getData().getInternet().getOffers().get(i).getHeader().setPrice("");
					}
				}
				//END of price check for special offers,  If any offer in mysusbcripions is a special offers we have to show price from DB.
				
			}
		// END of Logic For Internet
		 //START OF Logic for SMS
		 for(int i=0; i<mySubscriptionsResponse.getData().getSms().getOffers().size();i++)
			{
				String offeringId=mySubscriptionsResponse.getData().getSms().getOffers().get(i).getHeader().getOfferingId();
				String btnDeActivate=mySubscriptionsResponse.getData().getSms().getOffers().get(i).getHeader().getBtnDeactivate();
				String btnAllowedFor=mySubscriptionsResponse.getData().getSms().getOffers().get(i).getHeader().getAllowedForRenew();
				String btnreNew=mySubscriptionsResponse.getData().getSms().getOffers().get(i).getHeader().getBtnRenew();
				if(offeringIdsCustomer.contains(offeringId))
				{
					Utilities.printDebugLog(mySubscriptionsRequest.getMsisdn()+" :"
							+ offeringId+"-SMS Usage Controller MySbscriptions custoemrData  offeringId: " + offeringId,
							logger);
					
					Utilities.printDebugLog(mySubscriptionsRequest.getMsisdn()+" :"
							+ offeringId+ "-SMS Usage Controller MySbscriptions in AllowedFor: " + btnAllowedFor,
							logger);
					Utilities.printDebugLog(mySubscriptionsRequest.getMsisdn()
							+ "-SMS Usage Controller MySbscriptions data in btRenew: " + btnreNew,
							logger);
					
					
					if(btnDeActivate.equalsIgnoreCase("1"))
					{
						mySubscriptionsResponseDeep.getData().getSms().getOffers().get(i).getHeader().setBtnDeactivate("1");
					}
					else
					{
						mySubscriptionsResponseDeep.getData().getSms().getOffers().get(i).getHeader().setBtnDeactivate("0");
					}
					if(btnreNew.equalsIgnoreCase("1"))
					{
					
						
						List<String> AllowedFor = new ArrayList<String>(Arrays.asList(btnAllowedFor.split(",")));
						if(btnAllowedFor==null || btnAllowedFor.equalsIgnoreCase("null") || !CollectionUtils.intersection(offeringIdsCustomer, AllowedFor).isEmpty())
						{
							
							mySubscriptionsResponseDeep.getData().getSms().getOffers().get(i).getHeader().setBtnRenew("1");
							Utilities.printDebugLog(mySubscriptionsRequest.getMsisdn()
									+ "-SMS Usage Controller MySbscriptions in AllowedFor exist in customerData: "+mySubscriptionsResponseDeep.getData().getSms().getOffers().get(i).getHeader().getBtnRenew() ,
									logger);
						}
						else
						{
							mySubscriptionsResponseDeep.getData().getSms().getOffers().get(i).getHeader().setBtnRenew("0");
							Utilities.printDebugLog(mySubscriptionsRequest.getMsisdn()
									+ "-SMS Usage Controller MySbscriptions data in AllowedFor DOEST NOT exist in customerData: "+mySubscriptionsResponseDeep.getData().getSms().getOffers().get(i).getHeader().getBtnRenew() ,
									logger);
						}
					}
				}
				else
				{
					
					mySubscriptionsResponseDeep.getData().getSms().getOffers().get(i).getHeader().setBtnDeactivate("0");
					mySubscriptionsResponseDeep.getData().getSms().getOffers().get(i).getHeader().setBtnRenew("0");
					Utilities.printDebugLog(mySubscriptionsRequest.getMsisdn()+ " :"+offeringId 
							+ "-SMS Usage Controller MySbscriptions OFFER DOES NOT exist in customerData: "+mySubscriptionsResponseDeep.getData().getSms().getOffers().get(i).getHeader().getBtnDeactivate() ,
							logger);
				}
				//END OF CUSTOEMR DATA CHECK
				//START of price check for special offers,  If any offer in mysusbcripions is a special offers we have to show price from DB.
				if(specialIds.contains(offeringId))
				{
					Utilities.printDebugLog(mySubscriptionsRequest.getMsisdn()+ " :"+offeringId 
							+ "-Usage Controller MySbscriptions Contains SpecialIds: "+specialIds + "OfferingID :"+ offeringId,
							logger);
					String mrcFinal= GetConfigurations
							.getMigrationPrices(mySubscriptionsRequest.getMsisdn(),"Offer",custInfoResponse.getCustomerData().getGroupIds(),offeringId);
					Utilities.printDebugLog(mySubscriptionsRequest.getMsisdn()+"Usage Controller MySbscriptions Contains SpecialIds: FROM DB- MRC FINAL" + mrcFinal, logger);
					if(mrcFinal!=null && !mrcFinal.isEmpty())
					{
						mySubscriptionsResponseDeep.getData().getSms().getOffers().get(i).getHeader().setPrice(mrcFinal);
					}
					else {
						mySubscriptionsResponseDeep.getData().getSms().getOffers().get(i).getHeader().setPrice("");
					}
				}
				//END of price check for special offers,  If any offer in mysusbcripions is a special offers we have to show price from DB.
			
			}
		// END of Logic For SMS
		 //START OF Logic for ROAMING
		 for(int i=0; i<mySubscriptionsResponse.getData().getRoaming().getOffers().size();i++)
			{
				String offeringId=mySubscriptionsResponse.getData().getRoaming().getOffers().get(i).getHeader().getOfferingId();
				String btnDeActivate=mySubscriptionsResponse.getData().getRoaming().getOffers().get(i).getHeader().getBtnDeactivate();
				String btnAllowedFor=mySubscriptionsResponse.getData().getRoaming().getOffers().get(i).getHeader().getAllowedForRenew();
				String btnreNew=mySubscriptionsResponse.getData().getRoaming().getOffers().get(i).getHeader().getBtnRenew();
				if(offeringIdsCustomer.contains(offeringId))
				{
					Utilities.printDebugLog(mySubscriptionsRequest.getMsisdn()+" :"
							+ offeringId+"-Roaming Usage Controller MySbscriptions custoemrData  offeringId: " + offeringId,
							logger);
					
					Utilities.printDebugLog(mySubscriptionsRequest.getMsisdn()+" :"
							+ offeringId+ "-Roaming Usage Controller MySbscriptions in AllowedFor: " + btnAllowedFor,
							logger);
					Utilities.printDebugLog(mySubscriptionsRequest.getMsisdn()
							+ "-Roaming Usage Controller MySbscriptions data in btRenew: " + btnreNew,
							logger);
					
					
					if(btnDeActivate.equalsIgnoreCase("1"))
					{
						mySubscriptionsResponseDeep.getData().getRoaming().getOffers().get(i).getHeader().setBtnDeactivate("1");
					}
					else
					{
						mySubscriptionsResponseDeep.getData().getRoaming().getOffers().get(i).getHeader().setBtnDeactivate("0");
					}
					if(btnreNew.equalsIgnoreCase("1"))
					{
					
						
						List<String> AllowedFor = new ArrayList<String>(Arrays.asList(btnAllowedFor.split(",")));
						if(btnAllowedFor==null || btnAllowedFor.equalsIgnoreCase("null") || !CollectionUtils.intersection(offeringIdsCustomer, AllowedFor).isEmpty())
						{
							
							mySubscriptionsResponseDeep.getData().getRoaming().getOffers().get(i).getHeader().setBtnRenew("1");
							Utilities.printDebugLog(mySubscriptionsRequest.getMsisdn()
									+ "-Roaming Usage Controller MySbscriptions in AllowedFor exist in customerData: "+mySubscriptionsResponseDeep.getData().getRoaming().getOffers().get(i).getHeader().getBtnRenew() ,
									logger);
						}
						else
						{
							mySubscriptionsResponseDeep.getData().getRoaming().getOffers().get(i).getHeader().setBtnRenew("0");
							Utilities.printDebugLog(mySubscriptionsRequest.getMsisdn()
									+ "-Roaming Usage Controller MySbscriptions data in AllowedFor DOEST NOT exist in customerData: "+mySubscriptionsResponseDeep.getData().getRoaming().getOffers().get(i).getHeader().getBtnRenew() ,
									logger);
						}
					}
				}
				else
				{
					
					mySubscriptionsResponseDeep.getData().getRoaming().getOffers().get(i).getHeader().setBtnDeactivate("0");
					mySubscriptionsResponseDeep.getData().getRoaming().getOffers().get(i).getHeader().setBtnRenew("0");
					Utilities.printDebugLog(mySubscriptionsRequest.getMsisdn()+ " :"+offeringId 
							+ "-Roaming Usage Controller MySbscriptions OFFER DOES NOT exist in customerData: "+mySubscriptionsResponseDeep.getData().getRoaming().getOffers().get(i).getHeader().getBtnDeactivate() ,
							logger);
				}
				//END OF CUSTOEMR DATA CHECK
				//START of price check for special offers,  If any offer in mysusbcripions is a special offers we have to show price from DB.
				if(specialIds.contains(offeringId))
				{
					Utilities.printDebugLog(mySubscriptionsRequest.getMsisdn()+ " :"+offeringId 
							+ "-Usage Controller MySbscriptions Contains SpecialIds: "+specialIds + "OfferingID :"+ offeringId,
							logger);
					String mrcFinal= GetConfigurations
							.getMigrationPrices(mySubscriptionsRequest.getMsisdn(),"Offer",custInfoResponse.getCustomerData().getGroupIds(),offeringId);
					Utilities.printDebugLog(mySubscriptionsRequest.getMsisdn()+"Usage Controller MySbscriptions Contains SpecialIds: FROM DB- MRC FINAL" + mrcFinal, logger);
					if(mrcFinal!=null && !mrcFinal.isEmpty())
					{
						mySubscriptionsResponseDeep.getData().getRoaming().getOffers().get(i).getHeader().setPrice(mrcFinal);
					}
					else {
						mySubscriptionsResponseDeep.getData().getRoaming().getOffers().get(i).getHeader().setPrice("");
					}
				}
				//END of price check for special offers,  If any offer in mysusbcripions is a special offers we have to show price from DB.
			
			}
		// END of Logic For ROAMING
		 //START OF Logic for CALL
		 for(int i=0; i<mySubscriptionsResponse.getData().getCall().getOffers().size();i++)
			{
				String offeringId=mySubscriptionsResponse.getData().getCall().getOffers().get(i).getHeader().getOfferingId();
				String btnDeActivate=mySubscriptionsResponse.getData().getCall().getOffers().get(i).getHeader().getBtnDeactivate();
				String btnAllowedFor=mySubscriptionsResponse.getData().getCall().getOffers().get(i).getHeader().getAllowedForRenew();
				String btnreNew=mySubscriptionsResponse.getData().getCall().getOffers().get(i).getHeader().getBtnRenew();
				if(offeringIdsCustomer.contains(offeringId))
				{
					Utilities.printDebugLog(mySubscriptionsRequest.getMsisdn()+" :"
							+ offeringId+"-Call Usage Controller MySbscriptions custoemrData  offeringId: " + offeringId,
							logger);
					
					Utilities.printDebugLog(mySubscriptionsRequest.getMsisdn()+" :"
							+ offeringId+ "-Call Usage Controller MySbscriptions in AllowedFor: " + btnAllowedFor,
							logger);
					Utilities.printDebugLog(mySubscriptionsRequest.getMsisdn()
							+ "-Call Usage Controller MySbscriptions data in btRenew: " + btnreNew,
							logger);
					
					
					if(btnDeActivate.equalsIgnoreCase("1"))
					{
						mySubscriptionsResponseDeep.getData().getCall().getOffers().get(i).getHeader().setBtnDeactivate("1");
					}
					else
					{
						mySubscriptionsResponseDeep.getData().getCall().getOffers().get(i).getHeader().setBtnDeactivate("0");
					}
					if(btnreNew.equalsIgnoreCase("1"))
					{
					
						
						List<String> AllowedFor = new ArrayList<String>(Arrays.asList(btnAllowedFor.split(",")));
						if(btnAllowedFor==null || btnAllowedFor.equalsIgnoreCase("null") || !CollectionUtils.intersection(offeringIdsCustomer, AllowedFor).isEmpty())
						{
							
							mySubscriptionsResponseDeep.getData().getCall().getOffers().get(i).getHeader().setBtnRenew("1");
							Utilities.printDebugLog(mySubscriptionsRequest.getMsisdn()
									+ "-Call Usage Controller MySbscriptions in AllowedFor exist in customerData: "+mySubscriptionsResponseDeep.getData().getCall().getOffers().get(i).getHeader().getBtnRenew() ,
									logger);
						}
						else
						{
							mySubscriptionsResponseDeep.getData().getCall().getOffers().get(i).getHeader().setBtnRenew("0");
							Utilities.printDebugLog(mySubscriptionsRequest.getMsisdn()
									+ "-Call Usage Controller MySbscriptions data in AllowedFor DOEST NOT exist in customerData: "+mySubscriptionsResponseDeep.getData().getCall().getOffers().get(i).getHeader().getBtnRenew() ,
									logger);
						}
					}
				}
				else
				{
					
					mySubscriptionsResponseDeep.getData().getCall().getOffers().get(i).getHeader().setBtnDeactivate("0");
					mySubscriptionsResponseDeep.getData().getCall().getOffers().get(i).getHeader().setBtnRenew("0");
					Utilities.printDebugLog(mySubscriptionsRequest.getMsisdn()+ " :"+offeringId 
							+ "-Call Usage Controller MySbscriptions OFFER DOES NOT exist in customerData: "+mySubscriptionsResponseDeep.getData().getCall().getOffers().get(i).getHeader().getBtnDeactivate() ,
							logger);
				}
				
				//END OF CUSTOEMR DATA CHECK
				//START of price check for special offers,  If any offer in mysusbcripions is a special offers we have to show price from DB.
				if(specialIds.contains(offeringId))
				{
					Utilities.printDebugLog(mySubscriptionsRequest.getMsisdn()+ " :"+offeringId 
							+ "-Usage Controller MySbscriptions Contains SpecialIds: "+specialIds + "OfferingID :"+ offeringId,
							logger);
					String mrcFinal= GetConfigurations
							.getMigrationPrices(mySubscriptionsRequest.getMsisdn(),"Offer",custInfoResponse.getCustomerData().getGroupIds(),offeringId);
					Utilities.printDebugLog(mySubscriptionsRequest.getMsisdn()+"Usage Controller MySbscriptions Contains SpecialIds: FROM DB- MRC FINAL" + mrcFinal, logger);
					if(mrcFinal!=null && !mrcFinal.isEmpty())
					{
						mySubscriptionsResponseDeep.getData().getCall().getOffers().get(i).getHeader().setPrice(mrcFinal);
					}
					else {
						mySubscriptionsResponseDeep.getData().getCall().getOffers().get(i).getHeader().setPrice("");
					}
				}
				//END of price check for special offers,  If any offer in mysusbcripions is a special offers we have to show price from DB.
			
			}
		// END of Logic For CALL 
		 //START OF Logic for Hybrid
		 for(int i=0; i<mySubscriptionsResponse.getData().getHybrid().getOffers().size();i++)
			{
				String offeringId=mySubscriptionsResponse.getData().getHybrid().getOffers().get(i).getHeader().getOfferingId();
				String btnDeActivate=mySubscriptionsResponse.getData().getHybrid().getOffers().get(i).getHeader().getBtnDeactivate();
				String btnAllowedFor=mySubscriptionsResponse.getData().getHybrid().getOffers().get(i).getHeader().getAllowedForRenew();
				String btnreNew=mySubscriptionsResponse.getData().getHybrid().getOffers().get(i).getHeader().getBtnRenew();
				if(offeringIdsCustomer.contains(offeringId))
				{
					Utilities.printDebugLog(mySubscriptionsRequest.getMsisdn()+" :"
							+ offeringId+"-Hybrid Usage Controller MySbscriptions custoemrData  offeringId: " + offeringId,
							logger);
					
					Utilities.printDebugLog(mySubscriptionsRequest.getMsisdn()+" :"
							+ offeringId+ "-Hybrid Usage Controller MySbscriptions in AllowedFor: " + btnAllowedFor,
							logger);
					Utilities.printDebugLog(mySubscriptionsRequest.getMsisdn()
							+ "-Hybrid Usage Controller MySbscriptions data in btRenew: " + btnreNew,
							logger);
					
					
					if(btnDeActivate.equalsIgnoreCase("1"))
					{
						mySubscriptionsResponseDeep.getData().getHybrid().getOffers().get(i).getHeader().setBtnDeactivate("1");
					}
					else
					{
						mySubscriptionsResponseDeep.getData().getHybrid().getOffers().get(i).getHeader().setBtnDeactivate("0");
					}
					if(btnreNew.equalsIgnoreCase("1"))
					{
					
						
						List<String> AllowedFor = new ArrayList<String>(Arrays.asList(btnAllowedFor.split(",")));
						if(btnAllowedFor==null || btnAllowedFor.equalsIgnoreCase("null") || !CollectionUtils.intersection(offeringIdsCustomer, AllowedFor).isEmpty())
						{
							
							mySubscriptionsResponseDeep.getData().getHybrid().getOffers().get(i).getHeader().setBtnRenew("1");
							Utilities.printDebugLog(mySubscriptionsRequest.getMsisdn()
									+ "-Hybrid Usage Controller MySbscriptions in AllowedFor exist in customerData: "+mySubscriptionsResponseDeep.getData().getHybrid().getOffers().get(i).getHeader().getBtnRenew() ,
									logger);
						}
						else
						{
							mySubscriptionsResponseDeep.getData().getHybrid().getOffers().get(i).getHeader().setBtnRenew("0");
							Utilities.printDebugLog(mySubscriptionsRequest.getMsisdn()
									+ "-Hybrid Usage Controller MySbscriptions data in AllowedFor DOEST NOT exist in customerData: "+mySubscriptionsResponseDeep.getData().getHybrid().getOffers().get(i).getHeader().getBtnRenew() ,
									logger);
						}
					}
				}
				else
				{
					
					mySubscriptionsResponseDeep.getData().getHybrid().getOffers().get(i).getHeader().setBtnDeactivate("0");
					mySubscriptionsResponseDeep.getData().getHybrid().getOffers().get(i).getHeader().setBtnRenew("0");
					Utilities.printDebugLog(mySubscriptionsRequest.getMsisdn()+ " :"+offeringId 
							+ "-Hybrid Usage Controller MySbscriptions OFFER DOES NOT exist in customerData: "+mySubscriptionsResponseDeep.getData().getHybrid().getOffers().get(i).getHeader().getBtnDeactivate() ,
							logger);
				}
				//END OF CUSTOEMR DATA CHECK
				//START of price check for special offers,  If any offer in mysusbcripions is a special offers we have to show price from DB.
				if(specialIds.contains(offeringId))
				{
					Utilities.printDebugLog(mySubscriptionsRequest.getMsisdn()+ " :"+offeringId 
							+ "-Usage Controller MySbscriptions Contains SpecialIds: "+specialIds + "OfferingID :"+ offeringId,
							logger);
					String mrcFinal= GetConfigurations
							.getMigrationPrices(mySubscriptionsRequest.getMsisdn(),"Offer",custInfoResponse.getCustomerData().getGroupIds(),offeringId);
					Utilities.printDebugLog(mySubscriptionsRequest.getMsisdn()+"Usage Controller MySbscriptions Contains SpecialIds: FROM DB- MRC FINAL" + mrcFinal, logger);
					if(mrcFinal!=null && !mrcFinal.isEmpty())
					{
						mySubscriptionsResponseDeep.getData().getHybrid().getOffers().get(i).getHeader().setPrice(mrcFinal);
					}
					else {
						mySubscriptionsResponseDeep.getData().getHybrid().getOffers().get(i).getHeader().setPrice("");
					}
				}
				//END of price check for special offers,  If any offer in mysusbcripions is a special offers we have to show price from DB.
			
			}
		// END of Logic For hybrid 
		 //START OF Logic for InternetInclusiveOffers()
		 for(int i=0; i<mySubscriptionsResponse.getData().getInternetInclusiveOffers().getOffers().size();i++)
			{
				String offeringId=mySubscriptionsResponse.getData().getInternetInclusiveOffers().getOffers().get(i).getHeader().getOfferingId();
				String btnDeActivate=mySubscriptionsResponse.getData().getInternetInclusiveOffers().getOffers().get(i).getHeader().getBtnDeactivate();
				String btnAllowedFor=mySubscriptionsResponse.getData().getInternetInclusiveOffers().getOffers().get(i).getHeader().getAllowedForRenew();
				String btnreNew=mySubscriptionsResponse.getData().getInternetInclusiveOffers().getOffers().get(i).getHeader().getBtnRenew();
				if(offeringIdsCustomer.contains(offeringId))
				{
					Utilities.printDebugLog(mySubscriptionsRequest.getMsisdn()+" :"
							+ offeringId+"-InternetInclusiveOffers Usage Controller MySbscriptions custoemrData  offeringId: " + offeringId,
							logger);
					
					Utilities.printDebugLog(mySubscriptionsRequest.getMsisdn()+" :"
							+ offeringId+ "-InternetInclusiveOffers Usage Controller MySbscriptions in AllowedFor: " + btnAllowedFor,
							logger);
					Utilities.printDebugLog(mySubscriptionsRequest.getMsisdn()
							+ "-InternetInclusiveOffers Usage Controller MySbscriptions data in btRenew: " + btnreNew,
							logger);
					
					
					if(btnDeActivate.equalsIgnoreCase("1"))
					{
						mySubscriptionsResponseDeep.getData().getInternetInclusiveOffers().getOffers().get(i).getHeader().setBtnDeactivate("1");
					}
					else
					{
						mySubscriptionsResponseDeep.getData().getInternetInclusiveOffers().getOffers().get(i).getHeader().setBtnDeactivate("0");
					}
					if(btnreNew.equalsIgnoreCase("1"))
					{
					
						
						List<String> AllowedFor = new ArrayList<String>(Arrays.asList(btnAllowedFor.split(",")));
						if(btnAllowedFor==null || btnAllowedFor.equalsIgnoreCase("null") || !CollectionUtils.intersection(offeringIdsCustomer, AllowedFor).isEmpty())
						{
							
							mySubscriptionsResponseDeep.getData().getInternetInclusiveOffers().getOffers().get(i).getHeader().setBtnRenew("1");
							Utilities.printDebugLog(mySubscriptionsRequest.getMsisdn()
									+ "-InternetInclusiveOffers Usage Controller MySbscriptions in AllowedFor exist in customerData: "+mySubscriptionsResponseDeep.getData().getInternetInclusiveOffers().getOffers().get(i).getHeader().getBtnRenew() ,
									logger);
						}
						else
						{
							mySubscriptionsResponseDeep.getData().getInternetInclusiveOffers().getOffers().get(i).getHeader().setBtnRenew("0");
							Utilities.printDebugLog(mySubscriptionsRequest.getMsisdn()
									+ "-InternetInclusiveOffers Usage Controller MySbscriptions data in AllowedFor DOEST NOT exist in customerData: "+mySubscriptionsResponseDeep.getData().getInternetInclusiveOffers().getOffers().get(i).getHeader().getBtnRenew() ,
									logger);
						}
					}
				}
				else
				{
					
					mySubscriptionsResponseDeep.getData().getInternetInclusiveOffers().getOffers().get(i).getHeader().setBtnDeactivate("0");
					mySubscriptionsResponseDeep.getData().getInternetInclusiveOffers().getOffers().get(i).getHeader().setBtnRenew("0");
					Utilities.printDebugLog(mySubscriptionsRequest.getMsisdn()+ " :"+offeringId 
							+ "-InternetInclusiveOffers Usage Controller MySbscriptions OFFER DOES NOT exist in customerData: "+mySubscriptionsResponseDeep.getData().getInternetInclusiveOffers().getOffers().get(i).getHeader().getBtnDeactivate() ,
							logger);
				}
				//END OF CUSTOEMR DATA CHECK
				//START of price check for special offers,  If any offer in mysusbcripions is a special offers we have to show price from DB.
				if(specialIds.contains(offeringId))
				{
					Utilities.printDebugLog(mySubscriptionsRequest.getMsisdn()+ " :"+offeringId 
							+ "-Usage Controller MySbscriptions Contains SpecialIds: "+specialIds + "OfferingID :"+ offeringId,
							logger);
					String mrcFinal= GetConfigurations
							.getMigrationPrices(mySubscriptionsRequest.getMsisdn(),"Offer",custInfoResponse.getCustomerData().getGroupIds(),offeringId);
					Utilities.printDebugLog(mySubscriptionsRequest.getMsisdn()+"Usage Controller MySbscriptions Contains SpecialIds: FROM DB- MRC FINAL" + mrcFinal, logger);
					if(mrcFinal!=null && !mrcFinal.isEmpty())
					{
						mySubscriptionsResponseDeep.getData().getInternetInclusiveOffers().getOffers().get(i).getHeader().setPrice(mrcFinal);
					}
					else {
						mySubscriptionsResponseDeep.getData().getInternetInclusiveOffers().getOffers().get(i).getHeader().setPrice("");
					}
				}
				//END of price check for special offers,  If any offer in mysusbcripions is a special offers we have to show price from DB.
			
			}
		// END of Logic For InternetInclusiveOffers() 
		//START OF Logic for InternetInclusiveOffers()
		 for(int i=0; i<mySubscriptionsResponse.getData().getVoiceInclusiveOffers().getOffers().size();i++)
			{
				String offeringId=mySubscriptionsResponse.getData().getVoiceInclusiveOffers().getOffers().get(i).getHeader().getOfferingId();
				String btnDeActivate=mySubscriptionsResponse.getData().getVoiceInclusiveOffers().getOffers().get(i).getHeader().getBtnDeactivate();
				String btnAllowedFor=mySubscriptionsResponse.getData().getVoiceInclusiveOffers().getOffers().get(i).getHeader().getAllowedForRenew();
				String btnreNew=mySubscriptionsResponse.getData().getVoiceInclusiveOffers().getOffers().get(i).getHeader().getBtnRenew();
				if(offeringIdsCustomer.contains(offeringId))
				{
					Utilities.printDebugLog(mySubscriptionsRequest.getMsisdn()+" :"
							+ offeringId+"-VoiceInclusiveOffers Usage Controller MySbscriptions custoemrData  offeringId: " + offeringId,
							logger);
					
					Utilities.printDebugLog(mySubscriptionsRequest.getMsisdn()+" :"
							+ offeringId+ "-VoiceInclusiveOffers Usage Controller MySbscriptions in AllowedFor: " + btnAllowedFor,
							logger);
					Utilities.printDebugLog(mySubscriptionsRequest.getMsisdn()
							+ "-VoiceInclusiveOffers Usage Controller MySbscriptions data in btRenew: " + btnreNew,
							logger);
					
					
					if(btnDeActivate.equalsIgnoreCase("1"))
					{
						mySubscriptionsResponseDeep.getData().getVoiceInclusiveOffers().getOffers().get(i).getHeader().setBtnDeactivate("1");
					}
					else
					{
						mySubscriptionsResponseDeep.getData().getVoiceInclusiveOffers().getOffers().get(i).getHeader().setBtnDeactivate("0");
					}
					if(btnreNew.equalsIgnoreCase("1"))
					{
					
						
						List<String> AllowedFor = new ArrayList<String>(Arrays.asList(btnAllowedFor.split(",")));
						if(btnAllowedFor==null || btnAllowedFor.equalsIgnoreCase("null") || !CollectionUtils.intersection(offeringIdsCustomer, AllowedFor).isEmpty())
						{
							
							mySubscriptionsResponseDeep.getData().getVoiceInclusiveOffers().getOffers().get(i).getHeader().setBtnRenew("1");
							Utilities.printDebugLog(mySubscriptionsRequest.getMsisdn()
									+ "-VoiceInclusiveOffers Usage Controller MySbscriptions in AllowedFor exist in customerData: "+mySubscriptionsResponseDeep.getData().getVoiceInclusiveOffers().getOffers().get(i).getHeader().getBtnRenew() ,
									logger);
						}
						else
						{
							mySubscriptionsResponseDeep.getData().getVoiceInclusiveOffers().getOffers().get(i).getHeader().setBtnRenew("0");
							Utilities.printDebugLog(mySubscriptionsRequest.getMsisdn()
									+ "-VoiceInclusiveOffers Usage Controller MySbscriptions data in AllowedFor DOEST NOT exist in customerData: "+mySubscriptionsResponseDeep.getData().getVoiceInclusiveOffers().getOffers().get(i).getHeader().getBtnRenew() ,
									logger);
						}
					}
				}
				else
				{
					
					mySubscriptionsResponseDeep.getData().getVoiceInclusiveOffers().getOffers().get(i).getHeader().setBtnDeactivate("0");
					mySubscriptionsResponseDeep.getData().getVoiceInclusiveOffers().getOffers().get(i).getHeader().setBtnRenew("0");
					Utilities.printDebugLog(mySubscriptionsRequest.getMsisdn()+ " :"+offeringId 
							+ "-VoiceInclusiveOffers Usage Controller MySbscriptions OFFER DOES NOT exist in customerData: "+mySubscriptionsResponseDeep.getData().getVoiceInclusiveOffers().getOffers().get(i).getHeader().getBtnDeactivate() ,
							logger);
				}
				//END OF CUSTOEMR DATA CHECK
				//START of price check for special offers,  If any offer in mysusbcripions is a special offers we have to show price from DB.
				if(specialIds.contains(offeringId))
				{
					Utilities.printDebugLog(mySubscriptionsRequest.getMsisdn()+ " :"+offeringId 
							+ "-Usage Controller MySbscriptions Contains SpecialIds: "+specialIds + "OfferingID :"+ offeringId,
							logger);
					String mrcFinal= GetConfigurations
							.getMigrationPrices(mySubscriptionsRequest.getMsisdn(),"Offer",custInfoResponse.getCustomerData().getGroupIds(),offeringId);
					Utilities.printDebugLog(mySubscriptionsRequest.getMsisdn()+"Usage Controller MySbscriptions Contains SpecialIds: FROM DB- MRC FINAL" + mrcFinal, logger);
					if(mrcFinal!=null && !mrcFinal.isEmpty())
					{
						mySubscriptionsResponseDeep.getData().getVoiceInclusiveOffers().getOffers().get(i).getHeader().setPrice(mrcFinal);
					}
					else {
						mySubscriptionsResponseDeep.getData().getVoiceInclusiveOffers().getOffers().get(i).getHeader().setPrice("");
					}
				}
				//END of price check for special offers,  If any offer in mysusbcripions is a special offers we have to show price from DB.
			
			}
		// END of Logic For VoiceInclusiveOffers
			//START OF Logic for SmsInclusiveOffers()
		 for(int i=0; i<mySubscriptionsResponse.getData().getSmsInclusiveOffers().getOffers().size();i++)
			{
				String offeringId=mySubscriptionsResponse.getData().getSmsInclusiveOffers().getOffers().get(i).getHeader().getOfferingId();
				String btnDeActivate=mySubscriptionsResponse.getData().getSmsInclusiveOffers().getOffers().get(i).getHeader().getBtnDeactivate();
				String btnAllowedFor=mySubscriptionsResponse.getData().getSmsInclusiveOffers().getOffers().get(i).getHeader().getAllowedForRenew();
				String btnreNew=mySubscriptionsResponse.getData().getSmsInclusiveOffers().getOffers().get(i).getHeader().getBtnRenew();
				if(offeringIdsCustomer.contains(offeringId))
				{
					Utilities.printDebugLog(mySubscriptionsRequest.getMsisdn()+" :"
							+ offeringId+"-SmsInclusiveOffers Usage Controller MySbscriptions custoemrData  offeringId: " + offeringId,
							logger);
					
					Utilities.printDebugLog(mySubscriptionsRequest.getMsisdn()+" :"
							+ offeringId+ "-SmsInclusiveOffers Usage Controller MySbscriptions in AllowedFor: " + btnAllowedFor,
							logger);
					Utilities.printDebugLog(mySubscriptionsRequest.getMsisdn()
							+ "-SmsInclusiveOffers Usage Controller MySbscriptions data in btRenew: " + btnreNew,
							logger);
					
					
					if(btnDeActivate.equalsIgnoreCase("1"))
					{
						mySubscriptionsResponseDeep.getData().getSmsInclusiveOffers().getOffers().get(i).getHeader().setBtnDeactivate("1");
					}
					else
					{
						mySubscriptionsResponseDeep.getData().getSmsInclusiveOffers().getOffers().get(i).getHeader().setBtnDeactivate("0");
					}
					if(btnreNew.equalsIgnoreCase("1"))
					{
					
						
						List<String> AllowedFor = new ArrayList<String>(Arrays.asList(btnAllowedFor.split(",")));
						if(btnAllowedFor==null || btnAllowedFor.equalsIgnoreCase("null") || !CollectionUtils.intersection(offeringIdsCustomer, AllowedFor).isEmpty())
						{
							
							mySubscriptionsResponseDeep.getData().getSmsInclusiveOffers().getOffers().get(i).getHeader().setBtnRenew("1");
							Utilities.printDebugLog(mySubscriptionsRequest.getMsisdn()
									+ "-SmsInclusiveOffers Usage Controller MySbscriptions in AllowedFor exist in customerData: "+mySubscriptionsResponseDeep.getData().getSmsInclusiveOffers().getOffers().get(i).getHeader().getBtnRenew() ,
									logger);
						}
						else
						{
							mySubscriptionsResponseDeep.getData().getSmsInclusiveOffers().getOffers().get(i).getHeader().setBtnRenew("0");
							Utilities.printDebugLog(mySubscriptionsRequest.getMsisdn()
									+ "-SmsInclusiveOffers Usage Controller MySbscriptions data in AllowedFor DOEST NOT exist in customerData: "+mySubscriptionsResponseDeep.getData().getSmsInclusiveOffers().getOffers().get(i).getHeader().getBtnRenew() ,
									logger);
						}
					}
				}
				else
				{
					
					mySubscriptionsResponseDeep.getData().getSmsInclusiveOffers().getOffers().get(i).getHeader().setBtnDeactivate("0");
					mySubscriptionsResponseDeep.getData().getSmsInclusiveOffers().getOffers().get(i).getHeader().setBtnRenew("0");
					Utilities.printDebugLog(mySubscriptionsRequest.getMsisdn()+ " :"+offeringId 
							+ "-SmsInclusiveOffers Usage Controller MySbscriptions OFFER DOES NOT exist in customerData: "+mySubscriptionsResponseDeep.getData().getSmsInclusiveOffers().getOffers().get(i).getHeader().getBtnDeactivate() ,
							logger);
				}
				//END OF CUSTOEMR DATA CHECK
				//START of price check for special offers,  If any offer in mysusbcripions is a special offers we have to show price from DB.
				if(specialIds.contains(offeringId))
				{
					Utilities.printDebugLog(mySubscriptionsRequest.getMsisdn()+ " :"+offeringId 
							+ "-Usage Controller MySbscriptions Contains SpecialIds: "+specialIds + "OfferingID :"+ offeringId,
							logger);
					String mrcFinal= GetConfigurations
							.getMigrationPrices(mySubscriptionsRequest.getMsisdn(),"Offer",custInfoResponse.getCustomerData().getGroupIds(),offeringId);
					Utilities.printDebugLog(mySubscriptionsRequest.getMsisdn()+"Usage Controller MySbscriptions Contains SpecialIds: FROM DB- MRC FINAL" + mrcFinal, logger);
					if(mrcFinal!=null && !mrcFinal.isEmpty())
					{
						mySubscriptionsResponseDeep.getData().getSmsInclusiveOffers().getOffers().get(i).getHeader().setPrice(mrcFinal);
					}
					else {
						mySubscriptionsResponseDeep.getData().getSmsInclusiveOffers().getOffers().get(i).getHeader().setPrice("");
					}
				}
				//END of price check for special offers,  If any offer in mysusbcripions is a special offers we have to show price from DB.
			
			}
		// END of Logic For SmsInclusiveOffers
    	} catch (Exception e) {
			// TODO: handle exception
		}
    	mySubscriptionsResponseDeep.setCallStatus(mySubscriptionsResponse.getCallStatus());
		mySubscriptionsResponseDeep.setLogsReport(mySubscriptionsResponse.getLogsReport());
		mySubscriptionsResponseDeep.setResultCode(mySubscriptionsResponse.getResultCode());
		mySubscriptionsResponseDeep.setResultDesc(mySubscriptionsResponse.getResultDesc());
    	return mySubscriptionsResponseDeep;
    }
}
