/**
 * 
 */
package com.evampsaanga.azerfon.controllers;

import java.io.IOException;
import java.sql.SQLException;

import javax.jms.JMSException;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONObject;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.evampsaanga.azerfon.business.AppMenuBusiness;
import com.evampsaanga.azerfon.common.messagesmappings.GetMessagesMappings;
import com.evampsaanga.azerfon.common.utilities.Constants;
import com.evampsaanga.azerfon.common.utilities.Transactions;
import com.evampsaanga.azerfon.common.utilities.Utilities;
import com.evampsaanga.azerfon.common.utilities.Validator;
import com.evampsaanga.azerfon.models.history.getoperationshistory.OperationsHistoryRequest;
import com.evampsaanga.azerfon.models.menues.appmenu.AppMenuRequest;

/**
 * @author Evamp & Saanga
 *
 */
@RestController
@RequestMapping("/menuV2")
public class AppMenuControllerV2 {

	
	Logger logger = Logger.getLogger(AppMenuControllerV2.class);

	@RequestMapping(value = "/getappmenu", method = RequestMethod.POST, headers = Constants.CONTENT_TYPE_DEFAULT_VALUE)
	public com.evampsaanga.azerfon.models.menues.appmenuV2.AppMenuResponse getAppMenu(@RequestBody String data,
			@RequestHeader(value = Constants.MSISDN_KEY, defaultValue = Constants.MSISDN_DEFAULT_VALUE) String msisdn,
			@RequestHeader(value = Constants.LANGUAGE_KEY, defaultValue = Constants.LANGUAGE_DEFAULT_VALUE) String lang,
			@RequestHeader(value = Constants.USER_AGENT_KEY, defaultValue = Constants.USER_AGENT_DEFAULT_VALUE) String userAgent,
			@RequestHeader(value = Constants.SUBSCRIBER_TYPE_KEY, defaultValue = Constants.SUBSCRIBER_TYPE_DEFAULT_VALUE) String userType,
			@RequestHeader(value = Constants.TARIFF_TYPE_KEY, defaultValue = Constants.TARIFF_TYPE_DEFAULT_VALUE) String tariffType,
			@RequestHeader(value = Constants.APP_PHASE_KEY, defaultValue = Constants.APP_PHASE_DEFAULT_VALUE) String isFromB2B,
			HttpServletRequest servletRequest) throws ClassNotFoundException, SQLException, JsonParseException,
			JsonMappingException, IOException, JMSException {
		ObjectMapper mapper = new ObjectMapper();
		AppMenuBusiness appMenuBusiness = new AppMenuBusiness();
		AppMenuRequest appMenuRequest = new AppMenuRequest();
		com.evampsaanga.azerfon.models.menues.appmenuV2.AppMenuResponse appMenuResponse = new com.evampsaanga.azerfon.models.menues.appmenuV2.AppMenuResponse();
		String requestTime = Utilities.getReportDateTime();

		String TRANSACTION_NAME = Transactions.APP_MENU_TRANSACTION_NAME + " CONTROLLER";
		Utilities.printInfoLog(msisdn + "-Request Landed in-" + TRANSACTION_NAME, logger);
		Utilities.printInfoLog(msisdn + "-Request Data-" + data, logger);
		try {

			// Populating report object before processing business logic.
			appMenuResponse.setLogsReport(
					Utilities.preBusinessReportLoggingData("", msisdn, TRANSACTION_NAME, servletRequest.getRemoteAddr(),
							userAgent, userType, tariffType, data, lang, appMenuResponse.getLogsReport()));
//			appMenuRequest.setChannel(userAgent);
//			appMenuRequest.setMsisdn(msisdn);
//			appMenuRequest.setiP(servletRequest.getRemoteAddr());
//			appMenuRequest.setLang(lang);
//			appMenuRequest.setIsB2B(isFromB2B);
			data = Utilities.addAttributesToRequest(data, msisdn, servletRequest, lang, userAgent);
			
			JSONObject jObject = new JSONObject(data);
			appMenuRequest = mapper.readValue(jObject.toString(), AppMenuRequest.class);
			
			String requestValidationStatus = Validator.validateRequest(msisdn, appMenuRequest);
			logger.info("<<<<  Validation Status >>>>:" + requestValidationStatus + " -- " + msisdn);
			if (msisdn.equals("Unknow Msisdn") || msisdn == null) {
				requestValidationStatus = "Unknow Msisdn";
			}
			if (requestValidationStatus.equalsIgnoreCase(Constants.VALIDATION_SUCCESS_CODE)) {

				appMenuResponse = appMenuBusiness.getAppMenuBusinessV2(msisdn, appMenuRequest, appMenuResponse);

			} else {
				appMenuResponse.setCallStatus(Constants.Call_Status_False);
				appMenuResponse.setResultCode(Constants.VALIDATION_FAILURE_CODE);
				appMenuResponse.setResultDesc(requestValidationStatus);
			}

			appMenuResponse.getLogsReport().setRequestTime(requestTime);
			appMenuResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
			appMenuResponse.getLogsReport().setResponse(mapper.writeValueAsString(appMenuResponse));
			appMenuResponse.getLogsReport().setResponseCode(appMenuResponse.getResultCode());

			// Sending report log into queue.
			appMenuResponse.getLogsReport().setAppVersion(Constants.APP_VERSION_PHASE2);
			Utilities.prepareLogReportForQueue(appMenuResponse.getLogsReport());

			Utilities.printInfoLog(msisdn + "-Response Returned from-" + TRANSACTION_NAME + "-"
					+ mapper.writeValueAsString(appMenuResponse), logger);
		} catch (Exception e) {
			Utilities.printErrorLog(Utilities.convertStackTraceToString(e), logger);
			appMenuResponse.setCallStatus(Constants.Call_Status_False);
			appMenuResponse.setResultCode(Constants.EXCEPTION_CODE);
			appMenuResponse.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("unexpected.error", lang));

			appMenuResponse.getLogsReport().setResponseCode(appMenuResponse.getResultCode());
			appMenuResponse.getLogsReport().setRequestTime(requestTime);
			appMenuResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
			appMenuResponse.getLogsReport().setResponse(mapper.writeValueAsString(appMenuResponse));

			// Sending report log into queue.
			appMenuResponse.getLogsReport().setAppVersion(Constants.APP_VERSION_PHASE2);
			Utilities.prepareLogReportForQueue(appMenuResponse.getLogsReport());

		}
		return appMenuResponse;
	}
}
