package com.evampsaanga.azerfon.restclient;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.security.SecureRandom;
import java.security.cert.X509Certificate;
import java.util.ResourceBundle;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import javax.print.DocFlavor.STRING;

import org.apache.log4j.Logger;

import com.evampsaanga.azerfon.common.utilities.Utilities;

public class RestClient {

	Logger logger = Logger.getLogger(RestClient.class);
	ResourceBundle rb = ResourceBundle.getBundle("com.evampsaanga.azerfon.common.propfiles.Config");

	public String getResponseFromESB(String url, String postData) throws SocketException {

		String line = "";
		String esbResponse = "";
		String isfromb2b = "";
		String[] reqpkt = postData.split("%");

		if (reqpkt.length > 1) {

			isfromb2b = reqpkt[1];
			postData = reqpkt[0];
		}

		try {

			// System.out.println("Rest Client-ESB Request Path: " + url);
			// System.out.println("Rest Client-ESB Request: " + postData);
			// String tempdata = postData;
			// JSONObject jsonObject = new JSONObject(tempdata);

			// if (jsonObject.has("customer_attr")) {
			// JSONObject jsonObjecttemp2 = (JSONObject)
			// jsonObject.get("customer_attr");
			// if (jsonObjecttemp2.has("password")) {
			// jsonObjecttemp2.remove("password");
			// jsonObjecttemp2.put("password", "**************");
			// } else {
			// jsonObjecttemp2.remove("new_password");
			// jsonObjecttemp2.put("new_password", "**************");
			// }
			// } else if (jsonObject.has("password")) {
			// jsonObject.remove("password");
			// jsonObject.put("password", "**************");
			// }

			TrustManager[] trustAllCerts = { new X509TrustManager() {
				@Override
				public X509Certificate[] getAcceptedIssuers() {
					return null;
				}

				@Override
				public void checkClientTrusted(X509Certificate[] certs, String authType) {
				}

				@Override
				public void checkServerTrusted(X509Certificate[] certs, String authType) {
				}
			} };

			SSLContext sc = SSLContext.getInstance("SSL");
			sc.init(null, trustAllCerts, new SecureRandom());

			HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());

			HostnameVerifier allHostsValid = new HostnameVerifier() {
				@Override
				public boolean verify(String hostname, SSLSession session) {
					return true;
				}
			};

			HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);

			URL url_1 = new URL(url);

			HttpURLConnection conn = (HttpURLConnection) url_1.openConnection();

			conn.setDoOutput(true);
			conn.setRequestMethod(this.rb.getString("conf_requestMethod"));
			conn.setRequestProperty("Content-Type", this.rb.getString("conf_contentType"));

			conn.setRequestProperty("credentials", this.rb.getString("conf_credentials"));

			conn.setRequestProperty("isFromB2B", isfromb2b);

			conn.setConnectTimeout(Integer.parseInt(rb.getString("conf_connectionTimeOut")));
			conn.setReadTimeout(Integer.parseInt(rb.getString("conf_readTimeOut")));
			conn.setRequestProperty("http.keepAlive", "false");
			OutputStreamWriter writer = new OutputStreamWriter(conn.getOutputStream());

			writer.write(postData);
			writer.flush();

			BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
			while ((line = reader.readLine()) != null) {

				esbResponse = esbResponse + line;
			}
			writer.close();
			reader.close();
			conn.disconnect();
			if (esbResponse.equalsIgnoreCase("null"))
				esbResponse = "{}";
		} catch (Exception e) {
			Utilities.printErrorLog(Utilities.convertStackTraceToString(e), logger);
			if (e instanceof SocketTimeoutException) {
				Utilities.printDebugLog("TIME OUT: ESB Call failed. ", logger);
			}

		}
		return esbResponse;
	}

	// public String getInvoiceFileFromURL(String url, String postData) throws
	// Exception {
	//
	// HttpClient client = new DefaultHttpClient();
	//
	// HttpPost post = new HttpPost(url);
	//
	// StringEntity input = new StringEntity(postData);
	// post.setEntity(input);
	// post.addHeader("Content-Type", this.rb.getString("conf_contenttype"));
	//
	// post.addHeader("username", this.rb.getString("conf_credentials"));
	//
	// HttpResponse response = client.execute(post);
	// byte[] content = EntityUtils.toByteArray(response.getEntity());
	//
	// String resresult = Base64.getEncoder().encodeToString(content);
	//
	// return resresult;
	// }
}