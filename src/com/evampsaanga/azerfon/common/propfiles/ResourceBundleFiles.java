/**
 * 
 */
package com.evampsaanga.azerfon.common.propfiles;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.SocketException;
import java.util.Properties;

import org.apache.log4j.Logger;

import com.evampsaanga.azerfon.common.utilities.Constants;
import com.evampsaanga.azerfon.common.utilities.Utilities;
import com.sun.istack.Nullable;

/**
 * @author Evamp & Saanga
 *
 */
public class ResourceBundleFiles {
	static Logger logger = Logger.getLogger(ResourceBundleFiles.class);
	private static Properties propsForConfig = null;
	private static Properties propsForRoutes = null;
	private static Properties propsForDBQueries = null;
	private static Properties propsForValidations = null;

	@Nullable
	public static synchronized void loadConfigProperties() throws SocketException {
		if (propsForConfig == null) {
			FileInputStream fileInput = null;
			try {
				File file = new File(Constants.GET_BASE_CONFIG_PATH + "Config.properties");
				fileInput = new FileInputStream(file);
				Reader reader = new InputStreamReader(fileInput, "UTF-8");
				propsForConfig = new Properties();
				propsForConfig.load(reader);
			} catch (Exception e) {
				Utilities.printErrorLog(Utilities.convertStackTraceToString(e), logger);
			} finally {
				try {
					fileInput.close();
				} catch (Exception e) {
					Utilities.printErrorLog(Utilities.convertStackTraceToString(e), logger);
				}
			}
		}
	}

	@Nullable
	public static synchronized void loadESBRoutesProperties() throws SocketException {
		if (propsForRoutes == null) {

			FileInputStream fileInput = null;
			try {
				File file = new File(Constants.GET_BASE_CONFIG_PATH + "ESBRoutes.properties");

				fileInput = new FileInputStream(file);
				Reader reader = new InputStreamReader(fileInput, "UTF-8");
				propsForRoutes = new Properties();
				propsForRoutes.load(reader);
				fileInput.close();
			} catch (Exception e) {
				Utilities.printErrorLog(Utilities.convertStackTraceToString(e), logger);
			} finally {
				try {
					fileInput.close();
				} catch (Exception e) {
					Utilities.printErrorLog(Utilities.convertStackTraceToString(e), logger);
				}
			}
		}
	}

	@Nullable
	public static synchronized void loadDBQueriesProperties() throws SocketException {
		if (propsForDBQueries == null) {
			FileInputStream fileInput = null;
			try {
				File file = new File(Constants.GET_BASE_CONFIG_PATH + "DBQueries.properties");
				fileInput = new FileInputStream(file);
				Reader reader = new InputStreamReader(fileInput, "UTF-8");
				propsForDBQueries = new Properties();
				propsForDBQueries.load(reader);
				fileInput.close();
			} catch (Exception e) {
				Utilities.printErrorLog(Utilities.convertStackTraceToString(e), logger);
			} finally {
				try {
					fileInput.close();
				} catch (Exception e) {
					Utilities.printErrorLog(Utilities.convertStackTraceToString(e), logger);
				}
			}
		}

	}

	@Nullable
	public static synchronized void loadValidationsProperties() {

		if (propsForValidations == null) {
			FileInputStream fileInput = null;
			try {
				File file = new File(Constants.GET_BASE_CONFIG_PATH + "validations.properties");
				fileInput = new FileInputStream(file);
				Reader reader = new InputStreamReader(fileInput, "UTF-8");
				propsForValidations = new Properties();
				propsForValidations.load(reader);
				fileInput.close();
			} catch (Exception e) {
				try {
					Utilities.printErrorLog(Utilities.convertStackTraceToString(e), logger);
				} catch (SocketException e1) {
					e1.printStackTrace();
				}
			} finally {
				try {
					fileInput.close();
				} catch (Exception e) {
					try {
						Utilities.printErrorLog(Utilities.convertStackTraceToString(e), logger);
					} catch (SocketException e1) {
						e1.printStackTrace();
					}
				}
			}
		}
	}

	public static Properties getResourceBundleForConfig() {
		try {
			loadConfigProperties();
			return propsForConfig;
		} catch (Exception e) {
			try {
				Utilities.printErrorLog(Utilities.convertStackTraceToString(e), logger);
			} catch (SocketException e1) {

				e1.printStackTrace();
			}
		}
		return propsForConfig;
	}

	public static Properties getResourceBundleForESBRoutes() {
		try {
			loadESBRoutesProperties();
			return propsForRoutes;
		} catch (Exception e) {
			try {
				Utilities.printErrorLog(Utilities.convertStackTraceToString(e), logger);
			} catch (SocketException e1) {

				e1.printStackTrace();
			}
		}
		return propsForConfig;
	}

	public static Properties getResourceBundleForDBQueries() {
		try {
			loadDBQueriesProperties();
			return propsForDBQueries;
		} catch (Exception e) {
			try {
				Utilities.printErrorLog(Utilities.convertStackTraceToString(e), logger);
			} catch (SocketException e1) {

				e1.printStackTrace();
			}
		}
		return propsForConfig;
	}

	public static Properties getResourceBundleForValidations() throws SocketException {
		loadValidationsProperties();
		return propsForValidations;
	}

	public static void resetPropertyFile(int propFileNumber) throws SocketException {
		if (propFileNumber == 1) {
			Utilities.printDebugLog("Request Received to reset property file CONFIGURATIONS", logger);
			propsForConfig = null;
			Utilities.printDebugLog("Object is: " + propsForConfig, logger);
		}
		if (propFileNumber == 2) {
			Utilities.printDebugLog("Request Received to reset property file ESB ROUTES", logger);
			propsForRoutes = null;
			Utilities.printDebugLog("Object is: " + propsForRoutes, logger);
		}
		if (propFileNumber == 3) {
			Utilities.printDebugLog("Request Received to reset property file DB QUERIES", logger);
			propsForDBQueries = null;
			Utilities.printDebugLog("Object is: " + propsForDBQueries, logger);
		}
		if (propFileNumber == 4) {
			Utilities.printDebugLog("Request Received to reset property file VALIDATIONS", logger);
			propsForValidations = null;
			Utilities.printDebugLog("Object is: " + propsForValidations, logger);
		}
	}

}
