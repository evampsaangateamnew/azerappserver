package com.evampsaanga.azerfon.common.messagesmappings;

import java.net.SocketException;
import java.sql.SQLException;
import java.util.HashMap;

import org.apache.log4j.Logger;

import com.evampsaanga.azerfon.common.utilities.Constants;
import com.evampsaanga.azerfon.common.utilities.GetConfigurations;
import com.evampsaanga.azerfon.common.utilities.Utilities;

public class GetMessagesMappings {

    static Logger logger = Logger.getLogger(GetMessagesMappings.class);

    /*
     * private static Properties propsLabelsEnglish = null; private static
     * Properties propsLabelsAzeri = null; private static Properties
     * propslabelsRussian = null;
     * 
     * private static Properties propsMessagesEnglish = null; private static
     * Properties propsMessagesAzeri = null; private static Properties
     * propsMessagesRussian = null;
     */

    
    public static HashMap<String, String> labelsNdMessagesCacheHashMap = new HashMap<>();

    /*
     * @Nullable public static synchronized void loadEnglishLabelsProperties() {
     * if (propsLabelsEnglish == null) { FileInputStream fileInput = null; try {
     * File file = new File(Constants.GET_BASE_CONFIG_PATH +
     * "Labels_English_Mapping.properties");
     * Utilities.printDebugLog("File Extension: " + file.exists() + " : " +
     * file.getAbsolutePath(), logger);
     * 
     * fileInput = new FileInputStream(file); Reader reader = new
     * InputStreamReader(fileInput, "UTF-8"); propsLabelsEnglish = new
     * Properties(); propsLabelsEnglish.load(reader); } catch (Exception e) {
     * Utilities.printErrorLog(Utilities.convertStackTraceToString(e), logger);
     * } finally { try { fileInput.close(); } catch (Exception e) {
     * Utilities.printErrorLog(Utilities.convertStackTraceToString(e), logger);
     * } } } }
     * 
     * @Nullable public static synchronized void loadAzeriLabelsProperties() {
     * if (propsLabelsAzeri == null) { FileInputStream fileInput = null; try {
     * File file = new File(Constants.GET_BASE_CONFIG_PATH +
     * "Labels_Azeri_Mapping.properties");
     * 
     * fileInput = new FileInputStream(file); Reader reader = new
     * InputStreamReader(fileInput, "UTF-8"); propsLabelsAzeri = new
     * Properties(); propsLabelsAzeri.load(reader); } catch (Exception e) {
     * Utilities.printErrorLog(Utilities.convertStackTraceToString(e), logger);
     * } finally { try { fileInput.close(); } catch (Exception e) {
     * Utilities.printErrorLog(Utilities.convertStackTraceToString(e), logger);
     * } } } }
     * 
     * @Nullable public static synchronized void loadRussianLabelsProperties() {
     * if (propslabelsRussian == null) { FileInputStream fileInput = null; try {
     * File file = new File(Constants.GET_BASE_CONFIG_PATH +
     * "Labels_Russian_Mapping.properties");
     * 
     * fileInput = new FileInputStream(file); Reader reader = new
     * InputStreamReader(fileInput, "UTF-8"); propslabelsRussian = new
     * Properties(); propslabelsRussian.load(reader);
     * 
     * } catch (Exception e) {
     * Utilities.printErrorLog(Utilities.convertStackTraceToString(e), logger);
     * } finally { try { fileInput.close(); } catch (Exception e) {
     * Utilities.printErrorLog(Utilities.convertStackTraceToString(e), logger);
     * } } } }
     * 
     * @Nullable public static synchronized void loadEnglishMessagesProperties()
     * { if (propsMessagesEnglish == null) { FileInputStream fileInput = null;
     * try { File file = new File(Constants.GET_BASE_CONFIG_PATH +
     * "Messages_English_Mapping.properties"); fileInput = new
     * FileInputStream(file); Reader reader = new InputStreamReader(fileInput,
     * "UTF-8"); propsMessagesEnglish = new Properties();
     * propsMessagesEnglish.load(reader); } catch (Exception e) {
     * Utilities.printErrorLog(Utilities.convertStackTraceToString(e), logger);
     * } finally { try { fileInput.close(); } catch (Exception e) {
     * Utilities.printErrorLog(Utilities.convertStackTraceToString(e), logger);
     * } } } }
     * 
     * @Nullable public static synchronized void loadAzeriMessagesProperties() {
     * if (propsMessagesAzeri == null) { FileInputStream fileInput = null; try {
     * File file = new File(Constants.GET_BASE_CONFIG_PATH +
     * "Messages_Azeri_Mapping.properties");
     * 
     * fileInput = new FileInputStream(file); Reader reader = new
     * InputStreamReader(fileInput, "UTF-8"); propsMessagesAzeri = new
     * Properties(); propsMessagesAzeri.load(reader); } catch (Exception e) {
     * Utilities.printErrorLog(Utilities.convertStackTraceToString(e), logger);
     * } finally { try { fileInput.close(); } catch (Exception e) {
     * Utilities.printErrorLog(Utilities.convertStackTraceToString(e), logger);
     * } } } }
     * 
     * @Nullable public static synchronized void loadRussianMessagesProperties()
     * { if (propsMessagesRussian == null) { FileInputStream fileInput = null;
     * try { File file = new File(Constants.GET_BASE_CONFIG_PATH +
     * "Messages_Russian_Mapping.properties");
     * 
     * fileInput = new FileInputStream(file); Reader reader = null; reader = new
     * InputStreamReader(fileInput, "UTF-8"); propsMessagesRussian = new
     * Properties(); propsMessagesRussian.load(reader); } catch (Exception e) {
     * Utilities.printErrorLog(Utilities.convertStackTraceToString(e), logger);
     * } finally { try { fileInput.close(); } catch (Exception e) {
     * Utilities.printErrorLog(Utilities.convertStackTraceToString(e), logger);
     * } } } }
     */

    public static String getMessageFromResourceBundle(String keyname, String lang)
	    throws SocketException, SQLException {

	Utilities.printDebugLog("GET MESSAGE (FROM CONFIGURATION CACHE) FOR KEY-" + keyname + "." + getLang(lang),
		logger);

	return GetConfigurations.getConfigurationFromCache(keyname + "." + getLang(lang));

	// Utilities.printInfoLog(
	// "Cache Check in HashMap " +
	// labelsNdMessagesCacheHashMap.containsKey(keyname + "." +
	// getLang(lang)),
	// logger);
	// if (labelsNdMessagesCacheHashMap.containsKey(keyname + "." +
	// getLang(lang))) {
	// Utilities.printInfoLog("Cache " +
	// labelsNdMessagesCacheHashMap.get(keyname + "." + getLang(lang)),
	// logger);
	// return labelsNdMessagesCacheHashMap.get(keyname + "." +
	// getLang(lang));
	// } else {
	// Utilities.printInfoLog("Cache Reloading messages", logger);
	// reloadLabelsNdMessagesCache();
	// Utilities.printInfoLog("Cache Check in HashMap after reload"
	// + labelsNdMessagesCacheHashMap.containsKey(keyname + "." + lang),
	// logger);
	// if (labelsNdMessagesCacheHashMap.containsKey(keyname + "." +
	// getLang(lang)))
	// return labelsNdMessagesCacheHashMap.get(keyname + "." +
	// getLang(lang));
	// else
	// return "";
	// }
    }

    public static String getLabelsFromResourceBundle(String keyname, String lang) throws SocketException, SQLException {

	Utilities.printDebugLog("GET LABEL (FROM CONFIGURATION CACHE) FOR KEY-" + keyname + "." + getLang(lang),
		logger);
	return GetConfigurations.getConfigurationFromCache(keyname + "." + getLang(lang));

	// Utilities.printInfoLog(
	// "Cache Check in HashMap " +
	// labelsNdMessagesCacheHashMap.containsKey(keyname + "." +
	// getLang(lang)),
	// logger);
	// if (labelsNdMessagesCacheHashMap.containsKey(keyname + "." +
	// getLang(lang))) {
	// Utilities.printInfoLog("Cache " +
	// labelsNdMessagesCacheHashMap.get(keyname + "." + getLang(lang)),
	// logger);
	// return labelsNdMessagesCacheHashMap.get(keyname + "." +
	// getLang(lang));
	// } else {
	// Utilities.printInfoLog("Cache Reloading messages", logger);
	// reloadLabelsNdMessagesCache();
	// Utilities.printInfoLog("Cache Check in HashMap after reload"
	// + labelsNdMessagesCacheHashMap.containsKey(keyname + "." + lang),
	// logger);
	// if (labelsNdMessagesCacheHashMap.containsKey(keyname + "." +
	// getLang(lang)))
	// return labelsNdMessagesCacheHashMap.get(keyname + "." +
	// getLang(lang));
	// else
	// return "";
	// }
    }

    // public static void reloadLabelsNdMessagesCache() throws SocketException {
    // PreparedStatement preparedStatement = null;
    // ResultSet resultSet = null;
    // try {
    // String getAllConfiguration = "select * from configuration_items";
    // preparedStatement =
    // DBUtility.getMAGConnection().prepareStatement(getAllConfiguration);
    // resultSet = preparedStatement.executeQuery();
    // while (resultSet.next())
    // labelsNdMessagesCacheHashMap.put(resultSet.getString("key"),
    // resultSet.getString("value"));
    // } catch (Exception e) {
    // Utilities.printErrorLog(Utilities.convertStackTraceToString(e), logger);
    // } finally {
    // try {
    // if (resultSet != null)
    // resultSet.close();
    // if (preparedStatement != null)
    // preparedStatement.close();
    // } catch (Exception e) {
    // Utilities.printErrorLog(Utilities.convertStackTraceToString(e), logger);
    // }
    // }
    // }

    public static String getLang(String lang) {
	String language = "";
	if (lang != null && lang.equals(Constants.LANGUAGE_AZERI_MAPPING)) {
	    language = Constants.LANGUAGE_AZERI_MAPPING_DESC;
	} else if (lang != null && lang.equals(Constants.LANGUAGE_ENGLISH_MAPPING)) {
	    language = Constants.LANGUAGE_ENGLISH_MAPPING_DESC;
	} else if (lang != null && lang.equals(Constants.LANGUAGE_RUSSIAN_MAPPING)) {
	    language = Constants.LANGUAGE_RUSSIAN_MAPPING_DESC;
	} else {
	    language = Constants.LANGUAGE_ENGLISH_MAPPING_DESC;
	}
	return language;
    }

}
