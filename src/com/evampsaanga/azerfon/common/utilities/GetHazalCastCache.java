package com.evampsaanga.azerfon.common.utilities;

import java.net.SocketException;
import java.sql.SQLException;

import org.apache.log4j.Logger;

import com.hazelcast.client.HazelcastClient;
import com.hazelcast.client.config.ClientConfig;
import com.hazelcast.client.config.ClientNetworkConfig;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IMap;
import com.evampsaanga.azerfon.appserver.refreshappservercache.CustomerModelCache;

public class GetHazalCastCache {
	static Logger logger = Logger.getLogger(GetHazalCastCache.class);
	public static IMap<String, CustomerModelCache> customerCache;
	public static ClientConfig clientConfig = null;
	public static HazelcastInstance client = null;
	static ClientNetworkConfig clientNetworkConfig = null;
	public static   IMap<String, String> configurationCache= null;

	public static void initHazelcast() throws SocketException {
		Utilities.printDebugLog("UserFromHazelCache initHazelcast Method Called", logger);
		clientConfig = new ClientConfig();

		String ipAddress = "";
		try {
			//ipAddress = GetConfigurations.getConfigurationFromCache("esb.logs.esb.hazlecastcache.ip");
			ipAddress=GetConfigurations.getDBConfig("hazelCastIp");
			Utilities.printDebugLog("UserFromHazelCache Hazalcast Ip from conf" + ipAddress + "", logger);
		} catch (SocketException e) {
			// TODO Auto-generated catch block
			Utilities.printErrorLog(Utilities.convertStackTraceToString(e), logger);
		}
		ClientNetworkConfig clientNetworkConfig = new ClientNetworkConfig().addAddress(ipAddress);
		clientNetworkConfig.setConnectionAttemptLimit(0).setConnectionTimeout(1);
		clientConfig.setNetworkConfig(clientNetworkConfig);
		client = HazelcastClient.newHazelcastClient(clientConfig);
		customerCache = client.getMap("azercustomers");
		configurationCache=client.getMap("middlewareconfiguration");
	}

}
