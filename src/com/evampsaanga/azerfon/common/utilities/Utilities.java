package com.evampsaanga.azerfon.common.utilities;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.Security;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Enumeration;
import java.util.List;
import java.util.StringTokenizer;

import javax.crypto.NoSuchPaddingException;
import javax.jms.JMSException;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import com.evampsaanga.azerfon.appserver.refreshappservercache.CustomerModelCache;
import com.evampsaanga.azerfon.common.messagesmappings.GetMessagesMappings;
import com.evampsaanga.azerfon.models.supplementaryofferings.getsupplementaryofferings.commonofferings.details_and_description.Price;
import com.evampsaanga.azerfon.models.verifyappversion.TimeStamps;
import com.evampsaanga.azerfon.restclient.RestClient;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import Decoder.BASE64Decoder;

/**
 * @author Evamp & Saanga
 * 
 */

public class Utilities {

    static Logger logger = Logger.getLogger(Utilities.class);
    private static String ipAddress = null;

    public static Date getDate(String candidate) {

	List<SimpleDateFormat> knownPatterns = new ArrayList<SimpleDateFormat>();
	knownPatterns.add(new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'"));
	knownPatterns.add(new SimpleDateFormat("yyyy-MM-dd'T'HH:mm.ss'Z'"));
	knownPatterns.add(new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss"));
	knownPatterns.add(new SimpleDateFormat("yyyy-MM-dd' 'HH:mm:ss"));
	knownPatterns.add(new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssXXX"));

	for (SimpleDateFormat pattern : knownPatterns) {
	    try {
		// Take a try
		return new Date(pattern.parse(candidate).getTime());

	    } catch (ParseException pe) {
		// Loop on
	    }
	}

	return null;
    }

    public static String parseDateDynamically(String msisdn, String dateTime, String formatWithTime) {

	Date daa = getDate(dateTime);

	String pattern = formatWithTime;
	SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);

	String date = simpleDateFormat.format(daa);
	// System.out.println(msisdn + "-Parsed Date-" + date);

	return date;
    }

    public static String parseDateToTimeFormat(String msisdn, String dbDate) throws SocketException, ParseException {

	Utilities.printDebugLog("Database Date: " + dbDate, logger);
	DateFormat df = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
	DateFormat outputformat = new SimpleDateFormat("hh:mm aa");
	Date date = null;
	String output = null;
	date = df.parse(dbDate);
	output = outputformat.format(date);
	Utilities.printDebugLog("Output Date: " + output, logger);
	return output;
    }

    public static <T> T JsonToObject(String json, Class<T> type) throws Exception, IOException {

	logger.debug(json);
	return new ObjectMapper().readValue(json, type);
    }

    public static String convertStackTraceToString(Exception e) {

	StringWriter sw = new StringWriter();
	PrintWriter pw = new PrintWriter(sw);
	e.printStackTrace(pw);
	return sw.toString();
    }

    public static String getValueFromJSON(String data, String key) throws JSONException {

	JSONObject dataJSON = new JSONObject(data);

	if (Utilities.hasJSONKey(data, key))
	    return dataJSON.getString(key);
	else
	    return "";
    }

    public static boolean hasJSONKey(String data, String key) throws JSONException {

	JSONObject dataJSON = new JSONObject(data);
	return dataJSON.has(key);
    }

    public static String addParamsToJSONObject(String data, String key, String value) throws JSONException {

	JSONObject dataJSON = null;

	if (data == null || data.length() == 0)
	    dataJSON = new JSONObject();
	else
	    dataJSON = new JSONObject(data);

	dataJSON.put(key, value);
	return dataJSON.toString();
    }

    public static String removeParamsFromJSONObject(String data, String key) throws JSONException {

	JSONObject dataJSON = new JSONObject(data);
	dataJSON.remove(key);
	return dataJSON.toString();
    }

    public static void printInfoLog(String msg, Logger logger) throws SocketException {

	logger.info(msg);
	printOnConsole(msg);
    }

    public static void printDebugLog(String msg, Logger logger) throws SocketException {

	logger.debug(msg);
	printOnConsole(msg);
    }

    public static void printErrorLog(String msg, Logger logger) throws SocketException {

	logger.error(msg);
	printOnConsole(msg);
    }

    public static void printTraceLog(String msg, Logger logger) throws SocketException {

	logger.trace(msg);
	printOnConsole(msg);
    }

    /**
     * Below console output statements should be commented when deploying to
     * Production.
     * 
     * @throws SocketException
     */

    public static void printOnConsole(String msg) throws SocketException {

	// TODO
	/*
	 * if (!(getServerIP().equalsIgnoreCase(Constants.PRODUCTION_APPSERVER_1) ||
	 * getServerIP().equalsIgnoreCase(Constants.PRODUCTION_APPSERVER_1))) {
	 * //System.out.println(msg + "\n"); }
	 */
	// System.out.println(msg);
    }

    public static String addAttributesToRequest(String data, String msisdn, HttpServletRequest servletRequest,
	    String lang, String userAgent) throws JSONException {

	/**
	 * Below commented statement should uncomment when deploying code to PRODUCTION
	 * and comment the hardcoded ip.
	 */

	// data = Utilities.addParamsToJSONObject(data, Constants.SOURCE_IP_KEY,
	// "10.220.48.7");
	data = Utilities.addParamsToJSONObject(data, Constants.SOURCE_IP_KEY, servletRequest.getRemoteAddr());
	data = Utilities.addParamsToJSONObject(data, Constants.LANGUAGE_KEY, lang);
	data = Utilities.addParamsToJSONObject(data, Constants.MSISDN_KEY, msisdn);
	data = Utilities.addParamsToJSONObject(data, Constants.SOURCE_CHANNEL_KEY, getUserAgentMappings(userAgent));
	return data;
    }

    private static String getUserAgentMappings(String userAgent) {

	if (userAgent.contains("iPhone"))
	    return "ios";
	return userAgent;
    }

    public static String prepareToken(String msisdn, String deviceID, String passHash)
	    throws UnsupportedEncodingException, SocketException {

	String token = "";

	String preparedToken = Utilities.getHashmapKeyForSession(msisdn, deviceID, passHash) + "@"
		+ getCurrentDateTime();

	Utilities.printDebugLog("Plain token is: " + preparedToken, logger);

	/*
	 * Below hash map will put the time as value of that token which is being once
	 * locked in preparedToken variable.
	 */
	// AppCache.getHashmapSessions().put(Utilities.getHashmapKeyForSession(msisdn,
	// deviceID),
	// Utilities.getTokens(preparedToken, "@").get(2));

	token = Utilities.encodeString(Encrypter.getCipher(preparedToken).trim());
	Utilities.printDebugLog("Encrypted token is: " + token, logger);

	return token;
    }

    // Below format is used in token preparation and creation time @param in
    // logs.
    public static String getCurrentDateTime() {

	return new SimpleDateFormat(Constants.DATE_TIME_FORMAT_FOR_TOKEN).format(new Date()).trim();
    }

    public static String getReportDateTime() {

	return new SimpleDateFormat(Constants.DATE_TIME_FORMAT_FOR_REPORT_LOGGER).format(new Date()).trim();
    }

    public static String getCurrentDateForFNFNumbers(String fromFormat, String toFormat, String date)
	    throws ParseException {

	return new SimpleDateFormat(toFormat).format(new SimpleDateFormat(fromFormat).parse(date));

    }

    public static String getHashmapKeyForSession(String msisdn, String deviceID, String hashPass) {

	return msisdn + "@" + deviceID + "@" + hashPass;
    }

    public static void prepareLogReportForQueue(LogsReport logsReport) throws JsonParseException, JsonMappingException,
	    IOException, JMSException, ClassNotFoundException, SQLException {

	// TODO to be fixed later
	logsReport.setResponse("");
	String logsReportJSONString = new ObjectMapper().writeValueAsString(logsReport);
	MessagingQueue.sendMessageIntoQueue(logsReport.getMsisdn(), logsReportJSONString);
	// logsReport.insertLogsIntoDB(logsReport);

    }

    // public static String getCurrentDate() {
    // SimpleDateFormat format = new SimpleDateFormat("d");
    // String date = format.format(new Date());
    //
    // if (date.endsWith("1") && !date.endsWith("11"))
    // format = new SimpleDateFormat("d'st' MMMM, yyyy");
    // else if (date.endsWith("2") && !date.endsWith("12"))
    // format = new SimpleDateFormat("d'nd' MMMM, yyyy");
    // else if (date.endsWith("3") && !date.endsWith("13"))
    // format = new SimpleDateFormat("d'rd' MMMM, yyyy");
    // else
    // format = new SimpleDateFormat("d'th' MMMM, yyyy");
    //
    // return format.format(new Date());
    //
    // }
    // public static String getTwoLastDigitOfYear(String year) throws
    // ParseException {
    //
    // String[] parts = year.split(Pattern.quote("."));
    // DateFormat df = new SimpleDateFormat("yy");
    // return parts[0] + "'" + df.format(df.parse(parts[1]));
    // }

    public static List<String> getTokens(String str, String key) {
	if (!str.isEmpty()) {

	    StringTokenizer st = new StringTokenizer(str, key);
	    List<String> tokens = new ArrayList<>();
	    while (st.hasMoreTokens()) {
		tokens.add(st.nextToken().trim());
	    }
	    return tokens;
	} else
	    return new ArrayList<>();
    }

    public static List<String> processBullets(String description) throws JSONException {

	ArrayList<String> bulletsList = new ArrayList<>();
	int lastIndex = 0;
	for (int i = 0; i < description.length(); i++) {
	    if (description.charAt(i) == '<') {
		bulletsList.add(description.substring(lastIndex, i));
		i = i + 4;
		lastIndex = i;
	    }
	}
	bulletsList.add(description.substring(lastIndex, description.length()));
	return bulletsList;
    }

    public static String concatStrings(String strOne, String strTwo) {

	return strOne + strTwo;
    }

    public static String getValueFromRequestHeader(HttpServletRequest servletRequest, String key) {

	return servletRequest.getHeader(key);

    }

    public static JSONObject splitToken(String token) throws JSONException, SocketException {

	JSONObject obj = new JSONObject();
	Utilities.printDebugLog("-Token:" + token, logger);
	String newtoken = Encrypter.getPlainText(token);
	Utilities.printDebugLog("-Plain:" + newtoken, logger);

	String[] json = newtoken.split("@");
	if (json.length == 4) {
	    obj.put("msisdn", json[0]);
	    obj.put("deviceID", json[1]);
	    obj.put("passHash", json[2]);
	    obj.put("timeStamp", json[3]);
	}

	return obj;
    }

    public static String decodeString(String token) throws UnsupportedEncodingException {

	return java.net.URLDecoder.decode(token, "UTF-8");
    }

    public static String encodeString(String token) throws UnsupportedEncodingException {

	return java.net.URLEncoder.encode(token, "UTF-8");
    }

    public static String decryptString(String encryptedData) throws UnsupportedEncodingException, SocketException {

	return Encrypter.getPlainText(encryptedData);
    }

    public static String setEmptyIfNull(String key) {

	if (key.equalsIgnoreCase("null") || key == null)
	    return "";
	return key;
    }

    public static Date getDateFromString(String str, String dateFormat) throws ParseException {

	if (!str.isEmpty())
	    return new SimpleDateFormat(dateFormat).parse(str);
	return null;

    }

    public static String getLastMonth(Date date) {

	SimpleDateFormat sdf = new SimpleDateFormat(Constants.DATE_FORMAT);
	Calendar cal = Calendar.getInstance();

	cal.setTime(date);
	cal.add(Calendar.MONTH, -1);
	Date d = cal.getTime();

	return sdf.format(d);

    }

    public static String getNextMonth(Date date) {

	SimpleDateFormat sdf = new SimpleDateFormat(Constants.DATE_FORMAT);
	Calendar cal = Calendar.getInstance();

	cal.setTime(date);
	cal.add(Calendar.MONTH, +1);

	cal.set(Calendar.DATE, cal.getActualMinimum(Calendar.DAY_OF_MONTH));
	Date nextMonthFirstDay = cal.getTime();

	return sdf.format(nextMonthFirstDay);

    }

    // This method serves for those APIs which are receiving JSON as a string in
    // response from ESB.The method then will transform string to JSON.
    public static String stringToJSONString(String response) throws JSONException {

	return new JSONObject(response.substring(1, response.length() - 1).replace("\\", "")).toString();

    }

    public static LogsReport preBusinessReportLoggingData(String uniqueSessionID, String msisdn, String transactionName,
	    String remoteAddr, String userAgent, String userType, String tariffType, String data, String lang,
	    LogsReport logsReport) throws UnsupportedEncodingException, SocketException {

	if (uniqueSessionID.isEmpty()) {

	    logsReport.setUniqueSessionID("Internal Call");

	} else {

	    String sessionId = Utilities.decryptString(Utilities.decodeString(uniqueSessionID)).split("@")[0] + "@"
		    + Utilities.decryptString(Utilities.decodeString(uniqueSessionID)).split("@")[1] + "@"
		    + Utilities.decryptString(Utilities.decodeString(uniqueSessionID)).split("@")[3];
	    logsReport.setUniqueSessionID(sessionId);
	}

	logsReport.setMsisdn(msisdn);
	logsReport.setRequest(data);
	logsReport.setTransactionName(transactionName.replace("CONTROLLER", "").trim());
	logsReport.setIp(remoteAddr);
	logsReport.setChannel(userAgent);
	logsReport.setUserType(userType);
	logsReport.setTariffType(tariffType);
	logsReport.setLang(lang);
	logsReport.setCreatedTimeStamp(Utilities.getReportDateTime());

	// As for APP Server, ESB is the only third party.Therefore hard coding
	// at single point.

	logsReport.setThirdParyName("ESB");
	return logsReport;
    }

    // Below method will serve for ESB response codes and descriptions. First
    // check if JSON key exists then will set its value to report log.
    public static LogsReport logESBParamsintoReportLog(String esbRequest, String esbResponse, LogsReport logsReport)
	    throws JSONException {

	if (Utilities.hasJSONKey(esbResponse, "returnCode")) {
	    logsReport.setEsbResponseCode(Utilities.getValueFromJSON(esbResponse, "returnCode"));
	}

	if (Utilities.hasJSONKey(esbResponse, "returnMsg")) {
	    logsReport.setEsbResponseDescription(Utilities.getValueFromJSON(esbResponse, "returnMsg"));
	}

	if (Utilities.hasJSONKey(esbResponse, "msg")) {
	    logsReport.setEsbResponseDescription(Utilities.getValueFromJSON(esbResponse, "msg"));
	} else if (Utilities.hasJSONKey(esbResponse, "message")) {
	    logsReport.setEsbResponseDescription(Utilities.getValueFromJSON(esbResponse, "message"));
	}

	logsReport.setEsbRequest(esbRequest);
	logsReport.setEsbResponse(esbResponse);
	return logsReport;
    }

    // Below method is used to mask string at the end, where string contains the
    // actual string which to be masked and n contains no.of characters to be
    // masked.
    public static String maskString(String str, int n) {

	return str.substring(0, (str.length() - n)).concat(getMasking(4));

    }

    private static String getMasking(int passCount) {

	String maskingString = "";

	for (int i = 0; i < passCount; i++) {
	    maskingString += "*";
	}

	return maskingString;
    }

    // public static JSONObject updateToken(JSONObject splitToken) throws
    // JSONException, UnsupportedEncodingException {
    //
    // Utilities.printDebugLog("-Token before update:" + splitToken, logger);
    //
    // return Utilities.splitToken(Utilities.decodeString(
    // Utilities.prepareToken(splitToken.getString("msisdn"),
    // splitToken.getString("deviceID"))));
    //
    // }

    public static String decryptDataWithCipher(String encryptedData) throws Exception {

	byte[] masterKeyby = GetConfigurations.getConfigurationFromCache("validation_m").trim().getBytes();
	byte[] secretKeyby = GetConfigurations.getConfigurationFromCache("validation_s").trim().getBytes();
	Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider());

	MyCipher myCipher = new MyCipher(masterKeyby, secretKeyby);
	return myCipher.decryptthrsecretkey(encryptedData);
    }

    public static String encryptData(String plainData) throws NoSuchAlgorithmException, NoSuchProviderException,
	    NoSuchPaddingException, InvalidKeyException, SocketException, SQLException {

	byte[] masterKeyby = GetConfigurations.getConfigurationFromCache("validation_m").trim().getBytes();
	byte[] secretKeyby = GetConfigurations.getConfigurationFromCache("validation_s").trim().getBytes();

	MyCipher myCipher = new MyCipher(masterKeyby, secretKeyby);
	return MyCipher.encryptdata(plainData.getBytes());
    }

    public static String removeSlashN(String str) {

	if (str.contains("\n")) {
	    return str.replace("\n", "");
	}
	return str;
    }

    public static File getFile(String base64, String fileName, String fileExt, String localPath) throws IOException {

	BASE64Decoder decoder = new BASE64Decoder();
	byte[] decodedBytes = decoder.decodeBuffer(base64);
	String completeFilePath = localPath + fileName + fileExt;
	File file = new File(completeFilePath);
	while (file.exists()) {
	    Utilities.printDebugLog("Old prfile Pic found : Deleting", logger);
	    file.delete();
	    file = new File(completeFilePath);
	}

	FileOutputStream fos = new FileOutputStream(file);

	fos.write(decodedBytes);
	fos.flush();
	fos.close();
	return file;
    }

    public static double getFileSizeInKB(long length) {
	return (length / 1024);

    }

    public static String getProfileImageURL(String msisdn) throws SocketException, SQLException {

	return (GetConfigurations.getConfigurationFromCache("profileImageDownloadLink") + msisdn
		+ Constants.PROFILE_IMAGE_EXT).trim();
    }

    public static String getLocalFilePath(String msisdn) throws SocketException, SQLException {

	return (GetConfigurations.getConfigurationFromCache("localPath") + msisdn + Constants.PROFILE_IMAGE_EXT).trim();
    }

    public static String getQueryFromPrepareStatement(PreparedStatement preparedStatement) {
	return preparedStatement.toString().split(":")[1];
    }

    public static TimeStamps getTimeStampForCache(String cacheType) {

	TimeStamps timeStamps = new TimeStamps();
	timeStamps.setCacheType(cacheType);
	timeStamps.setTimeStamp(Utilities.getCurrentDateTime());
	return timeStamps;
    }

    public static String getErrorMessageFromFile(String key, String lang, String responseCode)
	    throws SocketException, SQLException {

	Utilities.printDebugLog("LOOKING UP FOR ERROR MESSAGE", logger);
	Utilities.printDebugLog("PREFIX KEY: " + key, logger);
	Utilities.printDebugLog("LANG: " + lang, logger);
	Utilities.printDebugLog("RESPONSE CODE: " + responseCode, logger);
	Utilities.printDebugLog("GENERATED COMPLETE KEY : " + key + "." + responseCode, logger);
	Utilities.printDebugLog("FINDING KEY IN MAGENTO API MAPPINGS: " + key + "." + responseCode, logger);

	String message = GetMessagesMappings.getMessageFromResourceBundle(key + "." + responseCode, lang);

	if (message.isEmpty()) {

	    Utilities.printDebugLog("KEY NOT FOUND IN MAGENTO API MAPPINGS: " + key + "." + responseCode, logger);
	    Utilities.printDebugLog("FINDING KEY IN BAKCELL API MAPPINGS: " + key + "." + responseCode, logger);
	    message = GetMessagesMappings.getMessageFromResourceBundle(responseCode, lang);

	    if (message.isEmpty()) {

		Utilities.printDebugLog("KEY NOT FOUND IN BAKCELL API MAPPINGS: " + key + "." + responseCode, logger);
		message = GetMessagesMappings.getMessageFromResourceBundle("esb.unsuccess.message", lang);
		Utilities.printDebugLog("RETURNING DEFAULT MESSAGE: " + key + "." + responseCode, logger);
	    }
	}

	return message;
    }

    public static boolean isOfferingIdExists(String offeringId) {

	if (offeringId.isEmpty() || offeringId == null || offeringId.equalsIgnoreCase("null")) {
	    return false;
	}

	return true;
    }

    // this getEmailByEntityId method is commented because this was calling ESB API
    // to get customerID, Hash_password from cache via API. but now we have changed
    // it, and getting userCache from hazalcast cache module
    /*
     * public static String getEmailByEntityId(String entityId, String msisdn,
     * String customerId) throws SQLException, SocketException { // String str =
     * msisdn+"@bakcell.com;password"; String str = ""; RestClient rc = new
     * RestClient(); JSONObject requestJsonESB = new JSONObject(); try {
     * 
     * requestJsonESB.put("msisdn", msisdn);
     * 
     * String path = GetConfigurations.getESBRoute("getusercache");
     * 
     * Utilities.printDebugLog(msisdn + "-Request Call to ESB", logger);
     * Utilities.printDebugLog(msisdn + "-Path-" + path, logger);
     * Utilities.printDebugLog(msisdn + "-Request Packet-" +
     * requestJsonESB.toString(), logger); String response =
     * rc.getResponseFromESB(path, requestJsonESB.toString());
     * Utilities.printDebugLog(msisdn +
     * "-UserFromCache: Received response from ESB for-" + response, logger);
     * 
     * if (response != null && !response.isEmpty()) { JSONObject jsonObject = new
     * JSONObject(response); str = jsonObject.getString("email") + ";" +
     * jsonObject.getString("password_hash") + ";" + customerId; } } catch
     * (JSONException e) {
     * Utilities.printErrorLog(Utilities.convertStackTraceToString(e), logger); }
     * Utilities .printDebugLog( msisdn +
     * "-UserFromCache: Email and password hash , In getEmailByEntityId -" + str,
     * logger);
     * 
     * return str; }
     */

    public static String getEmailByEntityId(String entityId, String msisdn, String customerId)
	    throws SQLException, SocketException {

	logger.info("Landed in getEmailByEntityId with EntityId :" + entityId + " ,msisdn :" + msisdn
		+ " and customerId" + customerId);

	String str = "";

	if (GetHazalCastCache.customerCache == null) {

	    GetHazalCastCache.initHazelcast();
	}

	if (GetHazalCastCache.customerCache.containsKey(msisdn)) {

	    Utilities.printDebugLog(msisdn + "-UserFromHazelCache: YES FOUND , In getEmailByEntityId -" + str, logger);
	    CustomerModelCache modelCache = GetHazalCastCache.customerCache.get(msisdn);
	    str = modelCache.getEmail() + ";" + modelCache.getPassword_hash() + ";" + modelCache.getCustomerId();
	    Utilities.printDebugLog(msisdn + "-UserFromHazelCache: YES FOUND , In getEmailByEntityId -" + str, logger);

	} else {

	    Utilities.printDebugLog(msisdn + "-UserFromHazelCache: NOT FOUND , In getEmailByEntityId -" + str, logger);
	}

	Utilities.printDebugLog(
		msisdn + "-UserFromHazelCache: BeforeReturn Email and password hash , In getEmailByEntityId -" + str,
		logger);

	return str;
    }

    public static String getEmailByEntityIdV2(String entityId, String msisdn, String customerId)
	    throws SQLException, SocketException {

	// String str = msisdn+"@bakcell.com;password";
	String str = "";
	RestClient rc = new RestClient();
	JSONObject requestJsonESB = new JSONObject();

	try {

	    requestJsonESB.put("lang", "3");
	    requestJsonESB.put("entityId", entityId);
	    requestJsonESB.put("type", "login");

	    String path = GetConfigurations.getConfigurationFromCache("magento.app.appresume.url");

	    Utilities.printDebugLog(msisdn + "-Request Call to Magento", logger);
	    Utilities.printDebugLog(msisdn + "-Path-" + path, logger);
	    Utilities.printDebugLog(msisdn + "-Request Packet-" + requestJsonESB.toString(), logger);
	    String response = rc.getResponseFromESB(path, requestJsonESB.toString());
	    Utilities.printDebugLog(msisdn + "-Received response from Magento-" + response, logger);

	    if (response != null && !response.isEmpty()) {

		JSONObject jsonObject = new JSONObject(response);
		str = jsonObject.getString("email") + ";" + jsonObject.getString("password_hash") + ";" + customerId;
	    }

	} catch (JSONException e) {

	    Utilities.printErrorLog(Utilities.convertStackTraceToString(e), logger);
	}

	return str;
    }

    public static String remapBillingLanguageForApp(String billingLang) throws SocketException, SQLException {

	if (!GetConfigurations.getConfigurationFromCache(billingLang).isEmpty()) {

	    return GetConfigurations.getConfigurationFromCache(billingLang);

	} else {

	    // if billing language not found, return by default AZERI.
	    return Constants.LANGUAGE_AZERI_MAPPING;
	}
    }

    public static String remapBillingLanguageForESB(String billingLang) throws SocketException, SQLException {

	if (!GetConfigurations.getConfigurationFromCache(billingLang).isEmpty()) {

	    return GetConfigurations.getConfigurationFromCache(billingLang);

	} else {

	    // if billing language not found, return by default AZERI.
	    return "2060";
	}
    }

    public static boolean isPriceEmpty(Price price) {

	return (ifNotEmpty(price.getTitle(), price.getValue()));
    }

    public static boolean ifNotEmpty(String title, String value) {

	if ((title != null && !title.isEmpty()) && (value != null && !value.isEmpty())) {

	    return true;

	} else {

	    return false;
	}
    }

    public static String remapCustomerStatus(String customerStatusCode) throws SocketException, SQLException {

	return GetConfigurations.getConfigurationFromCache("customer.status." + customerStatusCode);
    }

    public static String getDateFormatForUsage(String date) throws ParseException {

	if (date == null || date.equalsIgnoreCase("null")) {

	    return null;

	} else if (date.isEmpty()) {

	    return "";

	} else {

	    Date d = new SimpleDateFormat("yyyyMMddHHmmss").parse(date);
	    Calendar cal = Calendar.getInstance();
	    cal.setTime(d);
	    cal.set(11, 0);
	    cal.set(12, 0);
	    cal.set(13, 0);
	    return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(cal.getTime());
	}
    }

    public static String getServerIP() throws SocketException {

	if (ipAddress == null) {

	    Enumeration<NetworkInterface> n = NetworkInterface.getNetworkInterfaces();

	    for (; n.hasMoreElements();) {

		NetworkInterface networkInterface = n.nextElement();
		Enumeration<InetAddress> enumerationInetAddresses = networkInterface.getInetAddresses();

		for (; enumerationInetAddresses.hasMoreElements();) {

		    InetAddress inetAddress = enumerationInetAddresses.nextElement();

		    // System.out.println(" " + inetAddress.getHostAddress());
		    if (inetAddress.getHostAddress().equalsIgnoreCase(Constants.PRODUCTION_APPSERVER_1)) {

			ipAddress = inetAddress.getHostAddress();

		    } else if (inetAddress.getHostAddress().equalsIgnoreCase(Constants.PRODUCTION_APPSERVER_2)) {

			ipAddress = inetAddress.getHostAddress();

		    } else if (inetAddress.getHostAddress().equalsIgnoreCase(Constants.STAGGING_APPSERVER_1)) {

			ipAddress = inetAddress.getHostAddress();

		    } else if (inetAddress.getHostAddress().equalsIgnoreCase(Constants.STAGGING_APPSERVER_2)) {

			ipAddress = inetAddress.getHostAddress();

		    } else if (inetAddress.getHostAddress().equalsIgnoreCase(Constants.DEVELOPMENT_APPSERVER_1)) {

			ipAddress = inetAddress.getHostAddress();
		    }
		}
	    }

	} else {

	    // System.out.println("SERVER IP:" + ipAddress);
	    return ipAddress;
	}

	// System.out.println("SERVER IP::" + ipAddress);
	return ipAddress;
    }
}
