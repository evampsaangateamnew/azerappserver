/**
 * 
 */
package com.evampsaanga.azerfon.common.utilities;

import java.net.SocketException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Properties;

import org.apache.log4j.Logger;

import com.evampsaanga.azerfon.common.propfiles.ResourceBundleFiles;

/**
 * @author Evamp & Saanga
 *
 */
public class GetConfigurations {
	static Logger logger = Logger.getLogger(GetConfigurations.class);

	static Properties propsConfig = ResourceBundleFiles.getResourceBundleForConfig();
	static Properties propsESBRoutes = ResourceBundleFiles.getResourceBundleForESBRoutes();
	static Properties propsDB = ResourceBundleFiles.getResourceBundleForDBQueries();
	static String baseURL = "conf_baseURL";

	public static String getConfigurationFromCache(String key) throws SocketException, SQLException {
		
		
		Utilities.printDebugLog("GET CONFIGURATIONS FOR KEY-" + key, logger);

		if(GetHazalCastCache.configurationCache==null)
			GetHazalCastCache.initHazelcast();
		if (GetHazalCastCache.configurationCache.containsKey(key)) {
			Utilities.printDebugLog("CONFIGURATION FETCHED FROM CACHE WITH KEY-" + key + ":"
					+ GetHazalCastCache.configurationCache.get(key), logger);
			return GetHazalCastCache.configurationCache.get(key);
		} else {
			Utilities.printDebugLog("CONFIGURATIONS NOT FOUND IN CACHE", logger);
//			reloadConfigurationsCache(key);
//			Utilities.printDebugLog("SEARCHING KEY (" + key + ") IN CACHE AFTER RELOAD-"
//					+ GetHazalCastCache.configurationCache.containsKey(key), logger);
//			if (GetHazalCastCache.configurationCache.containsKey(key)) {
//				Utilities.printDebugLog("CONFIGURATION FETCHED FROM CACHE WITH KEY-" + key + ":"
//						+ GetHazalCastCache.configurationCache.get(key), logger);
//				return GetHazalCastCache.configurationCache.get(key);
//			} else {
//				Utilities.printDebugLog("CONFIGURATION KEY(" + key + ") NOT ADDED INTO CONFIGURATION DATABASE.",
//						logger);
//				return "";
//			}
			
			return "";
		}
	}

	public static void reloadConfigurationsCache(String key) throws SocketException, SQLException {

		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;

		Utilities.printDebugLog("READING CONFIGURATIONS FOR KEY FROM DB-" + key, logger);

		String queryGetConfigurations = getDBConfig("getConfigurations");

		Utilities.printDebugLog("QUERY FOR CONFIGURATIONS-" + queryGetConfigurations, logger);

		preparedStatement = DBUtility.getMAGConnection().prepareStatement(queryGetConfigurations);
		resultSet = preparedStatement.executeQuery();

		while (resultSet.next())
			GetHazalCastCache.configurationCache.put(resultSet.getString("key"), resultSet.getString("value"));

		if (resultSet != null)
			resultSet.close();

		if (preparedStatement != null)
			preparedStatement.close();
	}

	public static String getESBRoute(String endPoint) {
		return propsConfig.getProperty(baseURL).trim() + propsESBRoutes.getProperty(endPoint).trim();
	}

	public static String getDBConfig(String key) {
		return propsDB.getProperty(key, "").trim();
	}

	public static String getConfig(String key) {
		return propsConfig.getProperty(key, "").trim();
	}
	public static String getMigrationPrices(String key,String type,String groupIds, String offeringID) throws SocketException, SQLException {
		 Utilities.printDebugLog(key+" getMigrationPrices Method-" , logger);
			
		 String mrcFinal="";
		 PreparedStatement preparedStatement = null;
			
			ResultSet resultSet = null;
			HashMap<String, MigrationPrices> cacheMigrations=new HashMap<String, MigrationPrices>();
			Utilities.printDebugLog(key+"READING Migration Prices FOR KEY FROM DB- key" + key, logger);
			Utilities.printDebugLog(key+"READING Migration Prices FOR KEY FROM DB- Type" + type, logger);
			Utilities.printDebugLog(key+"READING Migration Prices FOR KEY FROM DB- GroupIDs" + groupIds, logger);

			//String queryMigrationPrices = getDBConfig("getMigrations");
			
			
			String queryMigrationPrices="Select * from tariff_migration_prices where type='"+type+"' AND migrate_to='"+offeringID+"' AND customer_group in("+groupIds+")";

			Utilities.printDebugLog(key+"QUERY FOR CONFIGURATIONS- Special" + queryMigrationPrices, logger);

			preparedStatement = DBUtility.getMAGConnection().prepareStatement(queryMigrationPrices);
			
			resultSet = preparedStatement.executeQuery();
		try {			
			if (resultSet.next()){
				MigrationPrices migrations=new MigrationPrices();
				Utilities.printDebugLog(key+"MigrationTo- DB" + resultSet.getString("migrate_to"), logger);
			migrations.setMigrateTo(resultSet.getString("migrate_to"));
			migrations.setMigrateFrom(resultSet.getString("migrate_from"));
			if(resultSet.getString("migration_price")!=null)
			{
			migrations.setMigrationPric(resultSet.getString("migration_price"));
			}
			else
			{
				migrations.setMigrationPric("");
			}
			
			migrations.setMessageAz(resultSet.getString("message_az"));
			migrations.setMessageRu(resultSet.getString("message_ru"));
			migrations.setMessageEn(resultSet.getString("message_en"));
			
			Utilities.printDebugLog(key+"READING Migration Prices FOR KEY FROM DB- MRC CHECK" + resultSet.getString("mrc"), logger);
			if(resultSet.getString("mrc")!=null)
			{
			migrations.setMrc(resultSet.getString("mrc"));
			mrcFinal=resultSet.getString("mrc");
			Utilities.printDebugLog(key+"READING Migration Prices FOR KEY FROM DB- MRC check1" + mrcFinal, logger);
			}
			else
			{
				migrations.setMrc("");
			}
			migrations.setType(resultSet.getString("type"));
			if(resultSet.getString("customer_group")!=null)
			{
				migrations.setCustomerType(resultSet.getString("customer_group"));
			}
			else
			{
				migrations.setCustomerType("");
			}
			
			Utilities.printDebugLog(key+"READING MigrationTo-" , logger);
			AppCache.getHashmapMigrationPricesMessages().put(resultSet.getString("migrate_to"), migrations);
			
			} 
			
			if (resultSet != null)
				resultSet.close();

			if (preparedStatement != null)
				preparedStatement.close();

			Utilities.printDebugLog(key+"READING Migration Prices FOR KEY FROM DB- MRC BeforeReturn" + mrcFinal, logger);
			 return mrcFinal;
		} catch (Exception e) {
			// TODO: handle exception
		}

	 return "";	
	 }
	
		public static void loadGoldenPayMessages() throws SocketException, SQLException {
			Utilities.printDebugLog("Message map " + AppCache.getGoldenPayMessages(), logger);
			Utilities.printDebugLog("Message map size -" + AppCache.getGoldenPayMessages().size(), logger);
			if (AppCache.getGoldenPayMessages() == null || AppCache.getGoldenPayMessages().size() == 0) {
				PreparedStatement preparedStatement = null;
				ResultSet resultSet = null;
				Utilities.printDebugLog("Loading Golden pay messages from DB-", logger);
				String queryGetConfigurations = getDBConfig("getGoldenPayMessages");
				Utilities.printDebugLog("QUERY FOR GET GOLDEN PAY MESSAGES-" + queryGetConfigurations, logger);
				preparedStatement = DBUtility.getMAGConnection().prepareStatement(queryGetConfigurations);
				resultSet = preparedStatement.executeQuery();
				while (resultSet.next())
					AppCache.getGoldenPayMessages().put(resultSet.getString("key"), resultSet.getString("value"));
				if (resultSet != null)
					resultSet.close();
				if (preparedStatement != null)
					preparedStatement.close();
			}
		}
	
	//Commenting this function as we do not want to put this in cache , will be done later
	/* public static HashMap<String, MigrationPrices> getMigrationPrices(String key,String type,String groupIds) throws SocketException, SQLException {
		 Utilities.printDebugLog(key+" getMigrationPrices Method-" , logger);
			PreparedStatement preparedStatement = null;
			ResultSet resultSet = null;
			HashMap<String, MigrationPrices> cacheMigrations=new HashMap<String, MigrationPrices>();
			Utilities.printDebugLog("READING Migration Prices FOR KEY FROM DB-" + key, logger);

			//String queryMigrationPrices = getDBConfig("getMigrations");
			
			String queryMigrationPrices="Select * from tariff_migration_prices where type='"+type+"' AND customer_group in('"+groupIds+" ')";

			Utilities.printDebugLog(key+"QUERY FOR CONFIGURATIONS-" + queryMigrationPrices, logger);

			preparedStatement = DBUtility.getMAGConnection().prepareStatement(queryMigrationPrices);
			
			resultSet = preparedStatement.executeQuery();
		try {			while (resultSet.next()){
				MigrationPrices migrations=new MigrationPrices();
				Utilities.printDebugLog(key+"MigrationTo- DB" + resultSet.getString("migrate_to"), logger);
			migrations.setMigrateTo(resultSet.getString("migrate_to"));
			migrations.setMigrateFrom(resultSet.getString("migrate_from"));
			if(resultSet.getString("migration_price")!=null)
			{
			migrations.setMigrationPric(resultSet.getString("migration_price"));
			}
			else
			{
				migrations.setMigrationPric("");
			}
			
			migrations.setMessageAz(resultSet.getString("message_az"));
			migrations.setMessageRu(resultSet.getString("message_ru"));
			migrations.setMessageEn(resultSet.getString("message_en"));
			if(resultSet.getString("mrc")!=null)
			{
			migrations.setMrc(resultSet.getString("mrc"));
			}
			else
			{
				migrations.setMrc("");
			}
			migrations.setType(resultSet.getString("type"));
			if(resultSet.getString("customer_group")!=null)
			{
				migrations.setCustomerType(resultSet.getString("customer_group"));
			}
			else
			{
				migrations.setCustomerType("");
			}
			
			Utilities.printDebugLog(key+"READING MigrationTo-" , logger);
			AppCache.getHashmapMigrationPricesMessages().put(resultSet.getString("migrate_to"), migrations);
			
			} 
			
			if (resultSet != null)
				resultSet.close();

			if (preparedStatement != null)
				preparedStatement.close();

			
		} catch (Exception e) {
			// TODO: handle exception
		}

	 return cacheMigrations;	
	 }*/
	/*
	 * public static String getGeneralConfig(String key) {
	 * 
	 * return propsConfig.getProperty(key,"").trim(); }
	 */

	/*
	 * public static boolean containsConfigKey(String key) {
	 * 
	 * return propsConfig.containsKey(key); }
	 */

	/*
	 * public static String getGeneralConfig(String key) { return
	 * propsConfig.getProperty(key,"").trim(); }
	 */

}
