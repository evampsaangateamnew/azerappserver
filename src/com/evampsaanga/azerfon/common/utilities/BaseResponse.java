/**
 * 
 */
package com.evampsaanga.azerfon.common.utilities;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * @author Evamp & Saanga
 *
 */
public class BaseResponse {
	private String callStatus;
	private String resultCode;
	private String resultDesc;
	@JsonIgnore
	private LogsReport logsReport;

	public BaseResponse() {
		logsReport = new LogsReport();
	}

	public String getCallStatus() {
		return callStatus;
	}

	public void setCallStatus(String callStatus) {
		this.callStatus = callStatus;
	}

	public String getResultCode() {
		return resultCode;
	}

	public void setResultCode(String resultCode) {
		this.resultCode = resultCode;
	}

	public String getResultDesc() {
		return resultDesc;
	}

	public void setResultDesc(String resultDesc) {
		this.resultDesc = resultDesc;
	}

	public LogsReport getLogsReport() {
		return logsReport;
	}

	public void setLogsReport(LogsReport logsReport) {
		this.logsReport = logsReport;
	}

}
