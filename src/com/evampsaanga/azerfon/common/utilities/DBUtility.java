package com.evampsaanga.azerfon.common.utilities;

import java.net.SocketException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import org.apache.log4j.Logger;
import org.apache.tomcat.jdbc.pool.DataSource;
import org.apache.tomcat.jdbc.pool.PoolProperties;

public class DBUtility {
    static Logger logger = Logger.getLogger(DBUtility.class);

    private static Connection connectionAPPDB = null;
    private static Connection connectionMagento = null;
    private static Connection connectionESBDB = null;
    private static DataSource dataSourceAppDB = null;
    private static DataSource dataSourceESBDB = null;
    private static DataSource dataSourceMagento = null;

    public static DataSource getAPPDataSource() throws SQLException {

	if (dataSourceAppDB == null) {
	    DataSource eds = new DataSource();
	    PoolProperties p = new PoolProperties();
	    String dburl = GetConfigurations.getConfig("url");
	    String username = GetConfigurations.getConfig("username");
	    String password = GetConfigurations.getConfig("password");
	    DriverManager.registerDriver(new com.mysql.jdbc.Driver());

	    p.setDriverClassName("com.mysql.jdbc.Driver");
	    p.setUrl(dburl);
	    p.setUsername(username);
	    p.setPassword(password);
	    p.setMaxActive(Integer.parseInt(GetConfigurations.getConfig("db.connection.app.max.active")));// 1000
	    p.setMinIdle(Integer.parseInt(GetConfigurations.getConfig("db.connection.app.min.idle")));// 200
	    p.setMaxWait(Integer.parseInt(GetConfigurations.getConfig("db.connection.app.max.wait")));// 3000);
	    p.setRemoveAbandoned(true);
	    p.setValidationQuery("select 1");
	    p.setValidationInterval(5000);
	    p.setRemoveAbandonedTimeout(
		    Integer.parseInt(GetConfigurations.getConfig("db.connection.app.abandoned.timeout")));// 5);
	    eds.setPoolProperties(p);
	    dataSourceAppDB = eds;

	}

	return dataSourceAppDB;
    }

    public static DataSource getESBDataSource() throws SQLException {

	if (dataSourceESBDB == null) {
	    DataSource eds = new DataSource();
	    PoolProperties p = new PoolProperties();
	    String dburl = GetConfigurations.getConfig("transaction.url");
	    String username = GetConfigurations.getConfig("transaction.username");
	    String password = GetConfigurations.getConfig("transaction.password");
	    DriverManager.registerDriver(new com.mysql.jdbc.Driver());

	    p.setDriverClassName("com.mysql.jdbc.Driver");
	    p.setUrl(dburl);
	    p.setUsername(username);
	    p.setPassword(password);
	    p.setMaxActive(Integer.parseInt(GetConfigurations.getConfig("db.connection.esb.max.active")));// 1000
	    p.setMinIdle(Integer.parseInt(GetConfigurations.getConfig("db.connection.esb.min.idle")));// 200
	    p.setMaxWait(Integer.parseInt(GetConfigurations.getConfig("db.connection.esb.max.wait")));// 3000);
	    p.setRemoveAbandoned(true);
	    p.setValidationQuery("select 1");
	    p.setValidationInterval(5000);
	    p.setRemoveAbandonedTimeout(
		    Integer.parseInt(GetConfigurations.getConfig("db.connection.esb.abandoned.timeout")));// 5);
	    eds.setPoolProperties(p);
	    dataSourceESBDB = eds;

	}

	return dataSourceESBDB;
    }

    public static DataSource getMagentoDataSource() throws SQLException {

	if (dataSourceMagento == null) {
	    DataSource eds = new DataSource();
	    PoolProperties p = new PoolProperties();
	    String dburl = GetConfigurations.getConfig("urlMAG");
	    String username = GetConfigurations.getConfig("usernameMAG");
	    String password = GetConfigurations.getConfig("passwordMAG");
	    DriverManager.registerDriver(new com.mysql.jdbc.Driver());

	    p.setDriverClassName("com.mysql.jdbc.Driver");
	    p.setUrl(dburl);
	    p.setUsername(username);
	    p.setPassword(password);
	    p.setMaxActive(Integer.parseInt(GetConfigurations.getConfig("db.connection.mag.max.active")));// 1000
	    p.setMinIdle(Integer.parseInt(GetConfigurations.getConfig("db.connection.mag.min.idle")));// 200
	    p.setMaxWait(Integer.parseInt(GetConfigurations.getConfig("db.connection.mag.max.wait")));// 3000);
	    p.setRemoveAbandoned(true);
	    p.setValidationQuery("select 1");
	    p.setValidationInterval(5000);
	    p.setRemoveAbandonedTimeout(
		    Integer.parseInt(GetConfigurations.getConfig("db.connection.mag.abandoned.timeout")));// 5);
	    eds.setPoolProperties(p);
	    dataSourceMagento = eds;

	}

	return dataSourceMagento;
    }

    public static synchronized Connection getConnection() throws SQLException, SocketException {

	if (connectionAPPDB == null || connectionAPPDB.isClosed() || !connectionAPPDB.isValid(0)) {
	    Utilities.printDebugLog("Connecting to Database...", logger);

	    connectionAPPDB = getAPPDataSource().getConnection();
	    //
	    // DriverManager.registerDriver(new com.mysql.jdbc.Driver());
	    //
	    // String dburl = GetConfigurations.getConfig("url");
	    // String username = GetConfigurations.getConfig("username");
	    // String password = GetConfigurations.getConfig("password");
	    //
	    // connection = DriverManager.getConnection(dburl, username, password);
	    // if (connection == null || connection.isClosed() || !connection.isValid(0)) {
	    // Utilities.printDebugLog("Failed to connect to database.", logger);
	    // } else {
	    // Utilities.printDebugLog("Database connected successfully!", logger);
	    // }
	}

	return connectionAPPDB;

    }

    public static synchronized Connection getESBDBConnection() throws SQLException, SocketException {

	if (connectionESBDB == null || connectionESBDB.isClosed() || !connectionESBDB.isValid(0)) {
	    Utilities.printDebugLog("Connecting to Database...", logger);
	    connectionESBDB = getESBDataSource().getConnection();
	    //
	    // DriverManager.registerDriver(new com.mysql.jdbc.Driver());
	    //
	    // String dburl = GetConfigurations.getConfig("transaction.url");
	    // String username = GetConfigurations.getConfig("transaction.username");
	    // String password = GetConfigurations.getConfig("transaction.password");
	    //
	    // connectionESBDB = DriverManager.getConnection(dburl, username, password);
	    //
	    // if (connectionESBDB == null || connectionESBDB.isClosed() ||
	    // !connectionESBDB.isValid(0)) {
	    //
	    // Utilities.printDebugLog("Failed to connect to database.", logger);
	    //
	    // } else {
	    //
	    // Utilities.printDebugLog("Database connected successfully!", logger);
	    // }
	}

	return connectionESBDB;

    }

    public static synchronized Connection getMAGConnection() throws SQLException, SocketException {

	if (connectionMagento == null || connectionMagento.isClosed() || !connectionMagento.isValid(0)) {
	    Utilities.printDebugLog("Connecting to MAGENTO Database...", logger);
	    connectionMagento = getMagentoDataSource().getConnection();

	    // DriverManager.registerDriver(new com.mysql.jdbc.Driver());
	    //
	    // String dburl = GetConfigurations.getConfig("urlMAG");
	    // String username = GetConfigurations.getConfig("usernameMAG");
	    // String password = GetConfigurations.getConfig("passwordMAG");
	    //
	    // connectionMagento = DriverManager.getConnection(dburl, username, password);
	    //
	    // if (connectionMagento == null || connectionMagento.isClosed() ||
	    // !connectionMagento.isValid(0)) {
	    //
	    // Utilities.printDebugLog("Failed to connect to MAGENTO database.", logger);
	    //
	    // } else {
	    //
	    // Utilities.printDebugLog("MAGENTO Database connected successfully!", logger);
	    // }
	}

	return connectionMagento;

    }

}
