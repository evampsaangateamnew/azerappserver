/**
 * 
 */
package com.evampsaanga.azerfon.common.utilities;

import java.lang.reflect.Field;
import java.net.SocketException;
import java.util.Properties;

import org.codehaus.jettison.json.JSONException;

import com.evampsaanga.azerfon.common.propfiles.ResourceBundleFiles;
import com.evampsaanga.azerfon.models.customerservices.authenticateuser.AuthenticateUserRequest;

/**
 * @author Evamp & Saanga
 *
 */
public class Validator {

	public static <T> String validateRequest(String msisdn, T object)
			throws IllegalArgumentException, IllegalAccessException, NoSuchFieldException, SocketException {

		Properties rbValidations = ResourceBundleFiles.getResourceBundleForValidations();

		Field[] fields = object.getClass().getDeclaredFields();
		for (int i = 0; i < fields.length; i++) {
			fields[i].setAccessible(true);
			if (rbValidations.containsKey(fields[i].getName())) {
				if (!(fields[i].get(object) == null)) {

					if (fields[i].get(object).toString()
							.matches(Utilities.getTokens(rbValidations.getProperty(fields[i].getName()), ",").get(0))) {
						return Utilities.getTokens(rbValidations.getProperty(fields[i].getName()), ",").get(1);
					}
				}
			}
		}
		return Constants.VALIDATION_SUCCESS_CODE;
	}

	public static void main(String[] args) throws IllegalArgumentException, IllegalAccessException,
			NoSuchFieldException, SecurityException, JSONException, SocketException {
		System.out.println("SABOOOR");
		AuthenticateUserRequest authenticateUserRequest = new AuthenticateUserRequest();
		authenticateUserRequest.setChannel("android");
		authenticateUserRequest.setiP("10.220.48.130");
		authenticateUserRequest.setLang("asas");
		authenticateUserRequest.setMsisdn("test123");
		authenticateUserRequest.setPassword("123");

		String requestValidationStatus = Validator.validateRequest(null, authenticateUserRequest);
		if (requestValidationStatus.equalsIgnoreCase("true")) {
			System.out.println("Request Validated");
		} else {
			System.out.println("IN ELSE: " + requestValidationStatus);
		}

	}

}
