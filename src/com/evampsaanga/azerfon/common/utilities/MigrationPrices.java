package com.evampsaanga.azerfon.common.utilities;

public class MigrationPrices {
	
	private String migrateTo="";
	private String migrateFrom="";
	private String migrationPric="";
	private String messageAz="";
	private String messageEn="";
	private String messageRu="";
	private String mrc="";
	private String customerType="";
	private String  type="";
	
	public String getMessageEn() {
		return messageEn;
	}
	public void setMessageEn(String messageEn) {
		this.messageEn = messageEn;
	}
	public String getMigrateTo() {
		return migrateTo;
	}
	public void setMigrateTo(String migrateTo) {
		this.migrateTo = migrateTo;
	}
	public String getMigrateFrom() {
		return migrateFrom;
	}
	public void setMigrateFrom(String migrateFrom) {
		this.migrateFrom = migrateFrom;
	}
	public String getMigrationPric() {
		return migrationPric;
	}
	public void setMigrationPric(String migrationPric) {
		this.migrationPric = migrationPric;
	}
	public String getMessageAz() {
		return messageAz;
	}
	public void setMessageAz(String messageAz) {
		this.messageAz = messageAz;
	}
	
	public String getMessageRu() {
		return messageRu;
	}
	public void setMessageRu(String messageRu) {
		this.messageRu = messageRu;
	}
	public String getMrc() {
		return mrc;
	}
	public void setMrc(String mrc) {
		this.mrc = mrc;
	}
	public String getCustomerType() {
		return customerType;
	}
	public void setCustomerType(String customerType) {
		this.customerType = customerType;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}

	

}
