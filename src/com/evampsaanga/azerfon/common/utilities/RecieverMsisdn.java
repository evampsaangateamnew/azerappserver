package com.evampsaanga.azerfon.common.utilities;

import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonIgnoreProperties(ignoreUnknown = true)
	@JsonPropertyOrder({
	"msisdn",
	"groupType",
	"groupIdFrom",
	"tariffIds"
	})
	public class RecieverMsisdn {

	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonProperty("msisdn")
	private String msisdn;
	
	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonProperty("groupType")
	private String groupType;
	
	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonProperty("groupIdFrom")
	private String groupIdFrom;
		
	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonProperty("tariffIds")
	private String tariffIds;
	
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	@JsonProperty("msisdn")
	public String getMsisdn() {
	return msisdn;
	}

	@JsonProperty("msisdn")
	public void setMsisdn(String msisdn) {
	this.msisdn = msisdn;
	}

	@JsonProperty("groupType")
	public String getGroupType() {
	return groupType;
	}

	@JsonProperty("groupType")
	public void setGroupType(String groupType) {
	this.groupType = groupType;
	}

	@JsonProperty("groupIdFrom")
	public String getGroupIdFrom() {
	return groupIdFrom;
	}

	@JsonProperty("groupIdFrom")
	public void setGroupIdFrom(String groupIdFrom) {
	this.groupIdFrom = groupIdFrom;
	}

	@JsonProperty("tariffIds")
	public String getTariffIds() {
	return tariffIds;
	}

	@JsonProperty("tariffIds")
	public void setTariffIds(String tariffIds) {
	this.tariffIds = tariffIds;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
	return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
	this.additionalProperties.put(name, value);
	}


	}


