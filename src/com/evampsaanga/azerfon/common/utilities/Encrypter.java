package com.evampsaanga.azerfon.common.utilities;

import java.io.UnsupportedEncodingException;
import java.net.SocketException;

import org.apache.log4j.Logger;

public class Encrypter {

    static Logger logger = Logger.getLogger(Encrypter.class);

    public static String getCipher(String data) {
	try {
	    String encoded = DesEncrypter.encodeString(AESEncrypter.encodeString(data));
	    return encoded;
	} catch (Exception e) {
	    e.printStackTrace();
	    return "";
	}
    }

    public static String getPlainText(String data) throws SocketException {
	try {
	    String decode = DesEncrypter.decodeString(data);
	    return AESEncrypter.decodeString(decode);
	} catch (Exception e) {
	    e.printStackTrace();
	    Utilities.printErrorLog(Utilities.convertStackTraceToString(e), logger);
	    return "";
	}
    }

    public static void main(String args[]) throws UnsupportedEncodingException, SocketException {

	// System.out.println(Encrypter.getCipher("prod_app_user"));
	System.out.println(
		Encrypter.getPlainText("PmPi5TumDpB0WrmZnAY3HqeAMDEUuMRhriluPz9C8pjfjyt2qf170vBrvlmOgV5/9mitwmW2Ahk="));
	System.out.println(Encrypter.getPlainText(Utilities.decodeString(
		"s23VE8mD47AECpV26BNJYFMbjPRjP1D79bWNBUBDem%2B8k%2BFiiV9K4r7JXM%2B1sEqiqoMcTKUeXnhQ%0AcglR4QIsYledgAn0OTHcM9Qw77UPMD32AS8%2B6V1JSm5ZiItl0YXE1iCb83tXnsBBkmmjRt92Kfm8%0A4vkHzLYNt96tDumaHFalTh1SPWLWe%2Bi4U9Fmf8Xr1FQNeyPIHvOKlPg174vxSOqOX%2BL7FKLI%2F6Nf%0AOi2PK9E3O6wXAecU%2Bl1YyLLT4bEU9nFKlMxcA25%2Br6CKrc07f3E00yOFpYnTuKdLlKPvb%2FEKDGNZ%0AYFSbLo%2Fu5Me2yUKrLHD1tJltjeY%3D")));
    }

}
