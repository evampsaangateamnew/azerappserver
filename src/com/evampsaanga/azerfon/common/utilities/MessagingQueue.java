/**
 * 
 */
package com.evampsaanga.azerfon.common.utilities;

import java.net.SocketException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.IllegalStateException;
import javax.jms.JMSException;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.log4j.Logger;

import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * @author Evamp & Saanga
 *
 */

public class MessagingQueue {
    static Logger logger = Logger.getLogger(MessagingQueue.class);

    private static Session session = null;
    private static MessageProducer producer = null;

    public static void sendMessageIntoQueue(String msisdn, String queueMessage)
	    throws SocketException, JMSException, SQLException {

	ObjectMapper mapper = new ObjectMapper();
	LogsReport logsReport = new LogsReport();
	// try {
	// if (producer == null) {
	// init();
	// }
	// TextMessage message;
	//
	// message = session.createTextMessage();
	//
	// message.setText(queueInputRequest);
	//
	// generalReport = mapper.readValue(queueInputRequest,
	// GeneralReport.class);
	//
	// System.out.println("Message added to Queue with msisdn : " +
	// generalReport.getMsisdn() + " : and data : "
	// + generalReport.toString());
	// // message send to queue
	// producer.send(message);
	//
	// System.out.println("Message sent into Queue.");
	// } catch (Exception e) {
	// }

	try {

	    if (producer == null) {
		init();
	    }
	    TextMessage message;

	    message = session.createTextMessage();
	    logsReport = mapper.readValue(queueMessage, LogsReport.class);

	    logsReport = setTransactionId(logsReport);
	    queueMessage = new ObjectMapper().writeValueAsString(logsReport);
	    message.setText(queueMessage);

	    Utilities.printDebugLog(msisdn + "-Pushing report log into queue...", logger);
	    Utilities.printDebugLog(msisdn + "-Report log data:" + mapper.writeValueAsString(queueMessage), logger);

	    producer.send(message);

	    Utilities.printDebugLog(msisdn + "-Report log data pushed successfully.", logger);

	} catch (IllegalStateException exception) {
	    Utilities.printErrorLog(Utilities.convertStackTraceToString(exception), logger);
	    init();
	    try {
		Thread.sleep(5000);
	    } catch (InterruptedException ex) {
		Utilities.printErrorLog(Utilities.convertStackTraceToString(ex), logger);
	    }
	    sendMessageIntoQueue(msisdn, queueMessage);
	} catch (Exception e) {
	    Utilities.printErrorLog(Utilities.convertStackTraceToString(e), logger);
	}

    }

    private static LogsReport setTransactionId(LogsReport logReport) throws SocketException {
	if (AppCache.getHashmapTransactionNames().isEmpty())
	    loadTransactionsNamesFromDB();
	if (AppCache.getHashmapTransactionNames().containsKey(logReport.getTransactionName())) {
	    logReport.setTransactionID(AppCache.getHashmapTransactionNames().get(logReport.getTransactionName()));
	    Utilities.printDebugLog(logReport.getMsisdn() + "-REPORTS TRANSACTION ID:"
		    + AppCache.getHashmapTransactionNames().get(logReport.getTransactionName()), logger);
	    return logReport;
	} else {
	    Utilities.printDebugLog(
		    logReport.getMsisdn() + "-TRANSACTION ID NOT FOUND FOR NAME:" + logReport.getTransactionName(),
		    logger);
	}
	return logReport;
    }

    public static void init() throws SocketException, JMSException, SQLException {
	ConnectionFactory factory = null;
	Connection connection = null;
	Destination destination = null;

	factory = new ActiveMQConnectionFactory(GetConfigurations.getConfigurationFromCache("queue.ip"));

	connection = factory.createConnection(GetConfigurations.getConfigurationFromCache("queue.username"),
		GetConfigurations.getConfigurationFromCache("queue.password"));

	connection.start();

	session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
	destination = session.createQueue(GetConfigurations.getConfigurationFromCache("queue.name"));
	producer = session.createProducer(destination);

    }

    private static void loadTransactionsNamesFromDB() throws SocketException {
	Utilities.printDebugLog("LOADING TRANSACTION MAPPINGS...", logger);
	ResultSet resultSet = null;
	PreparedStatement prep = null;
	try {
	    String queryStr = GetConfigurations.getDBConfig("getTransactionMappings");
	    java.sql.Connection connection = DBUtility.getESBDBConnection();
	    PreparedStatement preparedStatement = connection.prepareStatement(queryStr);

	    Utilities.printDebugLog(
		    "-GET TRANSACTION MAPPINGS QUERY:" + Utilities.getQueryFromPrepareStatement(preparedStatement),
		    logger);
	    ResultSet rs = preparedStatement.executeQuery();
	    while (rs.next()) {
		// hashmap key will mapped with transaction names.
		Utilities.printDebugLog(
			"Caching Transactions - Transaction Name:" + rs.getString(2) + "-> ID:" + rs.getString(1),
			logger);
		AppCache.getHashmapTransactionNames().put(rs.getString(2), rs.getString(1));
	    }

	} catch (Exception ex) {
	    Utilities.printErrorLog(Utilities.convertStackTraceToString(ex), logger);
	} finally {
	    try {
		if (resultSet != null)
		    resultSet.close();
		if (prep != null)
		    prep.close();
	    } catch (Exception e) {
		Utilities.printErrorLog(Utilities.convertStackTraceToString(e), logger);
	    }
	}

    }

}
