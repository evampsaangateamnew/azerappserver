/**
 * 
 */
package com.evampsaanga.azerfon.business;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.SocketException;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import com.evampsaanga.azerfon.common.messagesmappings.GetMessagesMappings;
import com.evampsaanga.azerfon.common.utilities.Constants;
import com.evampsaanga.azerfon.common.utilities.GetConfigurations;
import com.evampsaanga.azerfon.common.utilities.Transactions;
import com.evampsaanga.azerfon.common.utilities.Utilities;
import com.evampsaanga.azerfon.models.customerservices.forgotpassword.ForgotPasswordResponseDataV2;
import com.evampsaanga.azerfon.models.customerservices.forgotpassword.ForgotPasswordResponseV2;
import com.evampsaanga.azerfon.models.dashboardservice.DashboardRequest;
import com.evampsaanga.azerfon.models.dashboardservice.DashboardResponse;
import com.evampsaanga.azerfon.models.dashboardservice.DashboardResponseData;
import com.evampsaanga.azerfon.models.dashboardservice.LoginData;
import com.evampsaanga.azerfon.models.dashboardservice.balancepic.BalancePicData;
import com.evampsaanga.azerfon.models.dashboardservice.freeunits.FreeResourceDashboard;
import com.evampsaanga.azerfon.models.dashboardservice.queryinvoice.QueryInvoiceData;
import com.evampsaanga.azerfon.models.dashboardservice.usersgroup.UserGroupData;
import com.evampsaanga.azerfon.models.dashboardservice.usersgroup.UserGroupRequest;
import com.evampsaanga.azerfon.models.dashboardservice.usersgroup.UserGroupResponseBase;
import com.evampsaanga.azerfon.models.dashboardservice.usersgroup.UsersGroupData;
import com.evampsaanga.azerfon.models.dashboardservice.usersgroup.UsersGroupResponse;
import com.evampsaanga.azerfon.models.dashboardservice.usersgroup.UsersGroupResponseData;
import com.evampsaanga.azerfon.models.generalservices.predefineddata.PredefinedDataResponseDataV2;
import com.evampsaanga.azerfon.restclient.RestClient;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * @author Evamp & Saanga
 *
 */
public class DashboardBusiness {
	Logger logger = Logger.getLogger(DashboardBusiness.class);

	public DashboardResponse getDashboardbusiness(String isFromB2B, String msisdn, String deviceID,
			DashboardRequest dashboardRequest, DashboardResponse dashboardResponse, String passHash)
			throws ParseException, Exception {
		Utilities.printDebugLog(msisdn + "-Request received in " + Transactions.HOME_PAGE_TRANSACTION_NAME
				+ " BUSINESS with data-" + dashboardRequest.toString(), logger);

		DashboardResponseData resData = new DashboardResponseData();
		RestClient rc = new RestClient();
		ObjectMapper mapper = new ObjectMapper();

		String requestJsonESB = mapper.writeValueAsString(dashboardRequest);
		// requestJsonESB.concat("%" + isFromB2B);
		/*
		 * Redefining request PARAMS for ESB request, As request params are different at
		 * Mobile and ESB end.
		 */
		// requestJsonESB = Utilities.removeParamsFromJSONObject(requestJsonESB,
		// "subscriberType");
		// requestJsonESB = Utilities.addParamsToJSONObject(requestJsonESB,
		// "customerType", subscriberType);

		String path = GetConfigurations.getESBRoute("getdashboardinfo");
		Utilities.printDebugLog(msisdn + "-Request Call to ESB", logger);
		Utilities.printDebugLog(msisdn + "-Path-" + path, logger);
		Utilities.printDebugLog(msisdn + "-Request Packet-" + requestJsonESB, logger);

		dashboardResponse.getLogsReport().setEsbRequestTime(Utilities.getReportDateTime());
		logger.info("E&S -- Request Land Time in Business" + new Date());
		String response = rc.getResponseFromESB(path, requestJsonESB);

		logger.info("E&S -- Response From ESB" + response);

		logger.info(msisdn + "Response Time in Business : " + new Date());
		dashboardResponse.getLogsReport().setEsbResponseTime(Utilities.getReportDateTime());
		// TODO what to do if the information is too much
		// Utilities.printDebugLog(msisdn + "-Received response from ESB-" +
		// response, logger);

		// Logging ESB response code and description.
		dashboardResponse.setLogsReport(
				Utilities.logESBParamsintoReportLog(requestJsonESB, response, dashboardResponse.getLogsReport()));

		if (response != null && !response.isEmpty()) {

			if (Utilities.getValueFromJSON(response, "returnCode").equalsIgnoreCase(Constants.ESB_SUCCESS_CODE)) {

				// mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES,
				// false);
				logger.info("E&S -- Before Parsing Time in Business " + new Date());
				resData = parsejson(msisdn, deviceID, response, dashboardResponse, dashboardRequest.getLang(), passHash,
						dashboardRequest.getIsB2B());
				logger.info("E&S -- After Parseing Time in Business " + new Date());
				if ("400".equals(resData.getReturnCode()) && resData.getLoginData().equals(null)) {
					dashboardResponse.setCallStatus(Constants.Call_Status_False);
					dashboardResponse.setResultCode(Utilities.getValueFromJSON(response, "returnCode"));
					dashboardResponse.setResultDesc(Utilities.getErrorMessageFromFile("home.page",
							dashboardRequest.getLang(), Utilities.getValueFromJSON(response, "returnCode")));
					dashboardResponse.setData(null);
				} else if (resData.getReturnCode().equalsIgnoreCase(Constants.INVALID_TOKEN)) {
					dashboardResponse.setData(resData);
					dashboardResponse.setCallStatus(Constants.Call_Status_False);
					dashboardResponse.setResultCode(Constants.INVALID_TOKEN);
					dashboardResponse.setResultDesc(
							GetMessagesMappings.getMessageFromResourceBundle("success", dashboardRequest.getLang()));

				} else {
					dashboardResponse.setData(resData);
					dashboardResponse.setCallStatus(Constants.Call_Status_True);
					dashboardResponse.setResultCode(Constants.APP_SERVER_SUCCESS_CODE);
					dashboardResponse.setResultDesc(
							GetMessagesMappings.getMessageFromResourceBundle("success", dashboardRequest.getLang()));

				}

			} else {
				dashboardResponse.setCallStatus(Constants.Call_Status_False);
				dashboardResponse.setResultCode(Utilities.getValueFromJSON(response, "returnCode"));
				dashboardResponse.setResultDesc(Utilities.getErrorMessageFromFile("home.page",
						dashboardRequest.getLang(), Utilities.getValueFromJSON(response, "returnCode")));
				dashboardResponse.setData(null);
			}
		} else {
			dashboardResponse.setCallStatus(Constants.Call_Status_False);
			dashboardResponse.setResultCode(Constants.API_FAILURE_CODE);
			dashboardResponse.setResultDesc(
					GetMessagesMappings.getMessageFromResourceBundle("connectivity.error", dashboardRequest.getLang()));
			dashboardResponse.setData(null);
		}
		logger.info("E&S -- Response Dispatch Time in Business" + new Date());
		return dashboardResponse;
	}

	public UserGroupResponseBase getuserGroupData(String isFromB2B, String msisdn, String deviceID,
			UserGroupRequest userGroupRequest, UsersGroupResponse usersGroupResponse) throws ParseException, Exception {
		Utilities.printDebugLog(msisdn + "-Request received in " + Transactions.USER_GROUP_DATA + " BUSINESS with data-"
				+ userGroupRequest.toString(), logger);

		UserGroupResponseBase userGroupResponseBase = new UserGroupResponseBase();
		UsersGroupResponse resData = new UsersGroupResponse();
		RestClient rc = new RestClient();
		ObjectMapper mapper = new ObjectMapper();

		String requestJsonESB = mapper.writeValueAsString(userGroupRequest);

		String path = GetConfigurations.getESBRoute("getusergroupdata");
		Utilities.printDebugLog(msisdn + "-Request Call to ESB", logger);
		Utilities.printDebugLog(msisdn + "-Path-" + path, logger);
		Utilities.printDebugLog(msisdn + "-Request Packet-" + requestJsonESB, logger);

		userGroupResponseBase.getLogsReport().setEsbRequestTime(Utilities.getReportDateTime());
		logger.info("E&S -- Request Land Time in Business" + new Date());
		String response = rc.getResponseFromESB(path, requestJsonESB);
		logger.info("E&S -- Response Time in Business" + new Date());
		userGroupResponseBase.getLogsReport().setEsbResponseTime(Utilities.getReportDateTime());

		Utilities.printDebugLog(msisdn + "-Received response from ESB-" + response, logger);

		// Logging ESB response code and description.
		userGroupResponseBase.setLogsReport(
				Utilities.logESBParamsintoReportLog(requestJsonESB, response, userGroupResponseBase.getLogsReport()));

		if (response != null && !response.isEmpty()) {

			if (Utilities.getValueFromJSON(response, "returnCode").equalsIgnoreCase(Constants.ESB_SUCCESS_CODE)) {

				// mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES,
				// false);

				if (!Utilities.getValueFromJSON(response, "userCount").equals("0")) {
					logger.info("E&S -- Before Parsing Time in Business " + new Date());
					resData = parsejsonforGroupData(msisdn, deviceID, response, usersGroupResponse);
					logger.info("E&S -- After Parseing Time in Business " + new Date());
				}
				userGroupResponseBase.setData(resData);

				resData.setIsLastPage(Utilities.getValueFromJSON(response, "isLastPage"));
				userGroupResponseBase.setCallStatus(Constants.Call_Status_True);
				userGroupResponseBase.setResultCode(Constants.VALIDATION_SUCCESS_CODE);
				if (Utilities.getValueFromJSON(response, "returnMsg").equalsIgnoreCase(Constants.GET_USER_MSG)) {
					userGroupResponseBase.setResultDesc(Constants.GET_USER_MSG);
				} else {
					userGroupResponseBase.setResultDesc(
							GetMessagesMappings.getMessageFromResourceBundle("success", userGroupRequest.getLang()));
				}
			} else {
				userGroupResponseBase.setCallStatus(Constants.Call_Status_False);
				userGroupResponseBase.setResultCode(Utilities.getValueFromJSON(response, "returnCode"));
				userGroupResponseBase.setResultDesc(Utilities.getErrorMessageFromFile("home.page",
						userGroupRequest.getLang(), Utilities.getValueFromJSON(response, "returnCode")));
				userGroupResponseBase.setData(null);
			}
		} else {
			userGroupResponseBase.setCallStatus(Constants.Call_Status_False);
			userGroupResponseBase.setResultCode(Constants.API_FAILURE_CODE);
			userGroupResponseBase.setResultDesc(
					GetMessagesMappings.getMessageFromResourceBundle("connectivity.error", userGroupRequest.getLang()));
			userGroupResponseBase.setData(null);
		}
		logger.info("E&S -- Response Dispatch Time in Business" + new Date());
		return userGroupResponseBase;
	}

	private UsersGroupResponse parsejsonforGroupData(String msisdn, String deviceID, String response,
			UsersGroupResponse usersGroupResponse) throws JSONException, JsonProcessingException {
		ObjectMapper mapper = new ObjectMapper();
		JSONObject jsonObject = new JSONObject(response);
		JSONObject obj = new JSONObject();
		try {
			obj = jsonObject.getJSONObject("groupData");
			Iterator key = obj.keys();
			key = obj.keys();
			UserGroupData userGroups = new UserGroupData();

			ArrayList<UsersGroupResponseData> usersGroupResponseData = new ArrayList<>();
			logger.info("E&S -- Response time before calculating group data " + new Date());
			while (key.hasNext()) {

				ArrayList<UsersGroupData> usersGroupData = new ArrayList<>();
				String currentkey = (String) key.next();
				JSONArray jsonarr = obj.getJSONObject(currentkey).getJSONArray("usersGroupData");
				String groupName = obj.getJSONObject(currentkey).getString("groupName");
				UsersGroupResponseData resGroupData = new UsersGroupResponseData();
				resGroupData.setGroupName(groupName);
				int count = 0;
				// resGroupData.setGroupName(currentkey);
				for (int j = 0; j < jsonarr.length(); j++) {
					count++;
					usersGroupData.add((UsersGroupData) mapper.readValue((jsonarr.getString(j)), UsersGroupData.class));
					logger.info("json string" + jsonarr.getString(j));
					// System.out.println(usersGroupData[i]);
				}
				resGroupData.setUserCount(String.valueOf(count));
				resGroupData.setUsersData(usersGroupData);
				usersGroupResponseData.add(resGroupData);
				// usersGroupResponse.setUserGroups(userGroups);(usersGroupResponseData);
			}
			userGroups.setUsersGroupData(usersGroupResponseData);
			usersGroupResponse.setGroupData(userGroups);
			logger.info("E&S -- Response time after calculating group data " + new Date());
			ArrayList<UsersGroupData> users = new ArrayList<>();
			ArrayList<UsersGroupResponseData> userGroups1 = usersGroupResponse.getGroupData().getUsersGroupData();
			for (int i = 0; i < userGroups1.size(); i++) {
				users.addAll(userGroups1.get(i).getUsersData());
			}
			usersGroupResponse.setUsers(users);
			logger.info("E&S -- Response time after calculating user group data " + new Date());
		} catch (Exception e) {
			logger.error("ERROR", e);
			usersGroupResponse.setGroupData(null);
		}
		logger.info("User Group Response :" + mapper.writeValueAsString(usersGroupResponse));
		return usersGroupResponse;
	}

	public ForgotPasswordResponseV2 getloginBusiness(String isFromB2B, String msisdn, String deviceID,
			DashboardRequest dashboardRequest, ForgotPasswordResponseV2 forgotPasswordResponse)
			throws ParseException, Exception {
		Utilities.printDebugLog(msisdn + "-Request received in " + Transactions.HOME_PAGE_TRANSACTION_NAME
				+ " BUSINESS with data-" + dashboardRequest.toString(), logger);

		RestClient rc = new RestClient();
		ObjectMapper mapper = new ObjectMapper();

		String requestJsonESB = mapper.writeValueAsString(dashboardRequest);
		requestJsonESB.concat("%" + isFromB2B);
		/*
		 * Redefining request PARAMS for ESB request, As request params are different at
		 * Mobile and ESB end.
		 */
		// requestJsonESB = Utilities.removeParamsFromJSONObject(requestJsonESB,
		// "subscriberType");
		// requestJsonESB = Utilities.addParamsToJSONObject(requestJsonESB,
		// "customerType", subscriberType);

		String path = GetConfigurations.getESBRoute("loginUser");
		Utilities.printDebugLog(msisdn + "-Request Call to ESB", logger);
		Utilities.printDebugLog(msisdn + "-Path-" + path, logger);
		Utilities.printDebugLog(msisdn + "-Request Packet-" + requestJsonESB, logger);

		forgotPasswordResponse.getLogsReport().setEsbRequestTime(Utilities.getReportDateTime());
		String response = rc.getResponseFromESB(path, requestJsonESB);
		forgotPasswordResponse.getLogsReport().setEsbResponseTime(Utilities.getReportDateTime());

		Utilities.printDebugLog(msisdn + "-Received response from ESB-" + response, logger);

		// Logging ESB response code and description.
		forgotPasswordResponse.setLogsReport(
				Utilities.logESBParamsintoReportLog(requestJsonESB, response, forgotPasswordResponse.getLogsReport()));
		LoginData loginRes = new LoginData();
		if (response != null && !response.isEmpty()) {

			if (Utilities.getValueFromJSON(response, "returnCode").equalsIgnoreCase(Constants.ESB_SUCCESS_CODE)) {

				// mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES,
				// false);

				loginRes = mapper.readValue(Utilities.getValueFromJSON(response, "loginData"), LoginData.class);

				PredefinedDataResponseDataV2 predefinedData = mapper.readValue(
						Utilities.getValueFromJSON(response, "predefinedData"), PredefinedDataResponseDataV2.class);

				predefinedData.getRedirectionLinks().setOnlinePaymentGatewayURL_AZ(
						GetConfigurations.getConfigurationFromCache("onlinePaymentGatewayURL.AZ"));
				predefinedData.getRedirectionLinks().setOnlinePaymentGatewayURL_EN(
						GetConfigurations.getConfigurationFromCache("onlinePaymentGatewayURL.EN"));
				predefinedData.getRedirectionLinks().setOnlinePaymentGatewayURL_RU(
						GetConfigurations.getConfigurationFromCache("onlinePaymentGatewayURL.RU"));
				String emailAndPassHash = "";
				Utilities.printDebugLog(msisdn + "<<<<<<< Email and password hash Befor IF ELSE", logger);
				if (dashboardRequest.getIsB2B().equalsIgnoreCase("true")) {
					logger.info("<<<<<<<< Email and password hash Before IF  B2B is");
					emailAndPassHash = Utilities.getEmailByEntityIdV2(loginRes.getEntity_id(), loginRes.getMsisdn(),
							loginRes.getCustomer_id());
					logger.info("<<<<<<<< Email and password hash B2B is:" + emailAndPassHash);
				} else {
					logger.info("<<<<<<<< Email and password hash Before IF B2C is");
					emailAndPassHash = Utilities.getEmailByEntityId(loginRes.getEntity_id(), loginRes.getMsisdn(),
							loginRes.getCustomer_id());
					logger.info("<<<<<<<<< Email and password hash B2C is:" + emailAndPassHash);
				}
				loginRes.setToken(Utilities.prepareToken(msisdn, deviceID, emailAndPassHash.split(";")[1]));
				// resData.setLoginData(loginData);
				ForgotPasswordResponseDataV2 dataV2 = new ForgotPasswordResponseDataV2();
				dataV2.setLoginData(loginRes);
				dataV2.setPredefinedData(predefinedData);
				forgotPasswordResponse.setCallStatus(Constants.Call_Status_True);
				forgotPasswordResponse.setResultCode(Constants.VALIDATION_SUCCESS_CODE);
				// forgotPasswordResponse.setResultDesc(resultDesc);
				forgotPasswordResponse.setData(dataV2);

			} else {
				forgotPasswordResponse.setCallStatus(Constants.Call_Status_False);
				forgotPasswordResponse.setResultCode(Utilities.getValueFromJSON(response, "returnCode"));
				forgotPasswordResponse.setResultDesc(Utilities.getErrorMessageFromFile("home.page",
						dashboardRequest.getLang(), Utilities.getValueFromJSON(response, "returnCode")));
				forgotPasswordResponse.setData(null);
			}
		} else {
			forgotPasswordResponse.setCallStatus(Constants.Call_Status_False);
			forgotPasswordResponse.setResultCode(Constants.API_FAILURE_CODE);
			forgotPasswordResponse.setResultDesc(
					GetMessagesMappings.getMessageFromResourceBundle("connectivity.error", dashboardRequest.getLang()));
			forgotPasswordResponse.setData(null);
		}

		return forgotPasswordResponse;
	}

	private DashboardResponseData parsejson(String msisdn, String deviceID, String response,
			DashboardResponse dashboardResponse, String lang, String passHash, String isB2B)
			throws JSONException, JsonParseException, JsonMappingException, IOException {
		DashboardResponseData dashboardResponseData = new DashboardResponseData();
		JSONObject jsonObject = new JSONObject(response);
		// logger.info("E&S -- Response in parser " + response);
		ObjectMapper mapper = new ObjectMapper();
		// String loginData = json.getJSONObject("loginData").toString();
		LoginData loginRes = new LoginData();
		PredefinedDataResponseDataV2 predefinedData = new PredefinedDataResponseDataV2();
		String userCount = jsonObject.getString("userCount");
		// dashboardResponseData.setUserCount(userCount);
		try {
			loginRes = mapper.readValue(Utilities.getValueFromJSON(jsonObject.toString(), "loginData"),
					LoginData.class);
			loginRes.setCustom_profile_image(GetConfigurations.getConfigurationFromCache("profileImageDownloadLink")
					+ loginRes.getCustom_profile_image());
			predefinedData = mapper.readValue(Utilities.getValueFromJSON(jsonObject.toString(), "predefinedData"),
					PredefinedDataResponseDataV2.class);
			predefinedData.setUserCount(userCount);
			predefinedData.getRedirectionLinks().setOnlinePaymentGatewayURL_AZ(
					GetConfigurations.getConfigurationFromCache("onlinePaymentGatewayURL.AZ"));
			predefinedData.getRedirectionLinks().setOnlinePaymentGatewayURL_EN(
					GetConfigurations.getConfigurationFromCache("onlinePaymentGatewayURL.EN"));
			predefinedData.getRedirectionLinks().setOnlinePaymentGatewayURL_RU(
					GetConfigurations.getConfigurationFromCache("onlinePaymentGatewayURL.RU"));
			String emailAndPassHash = "";
			if (isB2B.equalsIgnoreCase("true")) {
				emailAndPassHash = Utilities.getEmailByEntityIdV2(loginRes.getEntity_id(), loginRes.getMsisdn(),
						loginRes.getCustomer_id());
			} else {
				emailAndPassHash = Utilities.getEmailByEntityId(loginRes.getEntity_id(), loginRes.getMsisdn(),
						loginRes.getCustomer_id());
			}

			logger.info("<<<<<<<<< Email and password hash is:" + emailAndPassHash);

			loginRes.setToken(Utilities.prepareToken(msisdn, deviceID, emailAndPassHash.split(";")[1]));
			if (!isHashMatched(msisdn, passHash, loginRes)) {
				dashboardResponseData.setLoginData(null);
				dashboardResponseData.setReturnCode(Constants.INVALID_TOKEN);
				dashboardResponseData
						.setReturnMsg(GetMessagesMappings.getMessageFromResourceBundle("token.session.expired", lang));
				return dashboardResponseData;
			}
			dashboardResponseData.setReturnCode("00");
			dashboardResponseData.setReturnMsg("Successful");
			dashboardResponseData.setLoginData(loginRes);
			dashboardResponseData.setPredefinedData(predefinedData);
			// Group Data
			// JSONObject obj = new JSONObject();
			/*
			 * try { obj = jsonObject.getJSONObject("groupData"); Iterator key = obj.keys();
			 * UsersGroupResponse usersGroupResponse = new UsersGroupResponse();
			 * ArrayList<UsersGroupResponseData> usersGroupResponseData = new ArrayList<>();
			 * ArrayList<String> groupNames = new ArrayList<>();
			 * logger.info("E&S -- Response time before calculating group names " + new
			 * Date()); while (key.hasNext()) { groupNames.add((String) key.next()); } key =
			 * obj.keys(); logger.info("E&S -- Response time before calculating group data "
			 * + new Date()); while (key.hasNext()) { String currentkey = (String)
			 * key.next(); JSONArray jsonarr =
			 * obj.getJSONObject(currentkey).getJSONArray("usersGroupData");
			 * ArrayList<UsersGroupData> usersGroupData = new ArrayList<>();
			 * UsersGroupResponseData resGroupData = new UsersGroupResponseData(); int count
			 * = 0; resGroupData.setGroupName(currentkey); for (int j = 0; j <
			 * jsonarr.length(); j++) { count++; usersGroupData .add((UsersGroupData)
			 * mapper.readValue((jsonarr.getString(j)), UsersGroupData.class)); //
			 * System.out.println(usersGroupData[i]); }
			 * resGroupData.setUsersData(usersGroupData);
			 * resGroupData.setUserCount(String.valueOf(count));
			 * usersGroupResponseData.add(resGroupData);
			 * 
			 * usersGroupResponse.setUsersGroupData(usersGroupResponseData);
			 * dashboardResponseData.setGroupData(usersGroupResponse); }
			 * logger.info("E&S -- Response time after calculating group data " + new
			 * Date()); } catch (Exception e) { e.printStackTrace();
			 * dashboardResponseData.setGroupData(null);
			 * dashboardResponseData.setReturnCode("400");
			 * dashboardResponseData.setReturnMsg("Not Successful"); }
			 */

			/*
			 * try { logger.info( "E&S -- Response time before calculating group user data "
			 * + new Date()); ArrayList<UsersGroupData> users = new ArrayList<>();
			 * ArrayList<UsersGroupResponseData> userGroups =
			 * dashboardResponseData.getGroupData().getUsersGroupData(); for (int i = 0; i <
			 * userGroups.size(); i++) { users.addAll(userGroups.get(i).getUsersData()); }
			 * dashboardResponseData.setUsers(users);
			 * logger.info("E&S -- Response time after calculating user group data " + new
			 * Date()); } catch (Exception e) { e.printStackTrace();
			 * dashboardResponseData.setUsers(null);
			 * dashboardResponseData.setReturnCode("400");
			 * dashboardResponseData.setReturnMsg("Not Successful"); }
			 */

			try {

				BalancePicData balancePicData = mapper.readValue(
						Utilities.getValueFromJSON(jsonObject.toString(), "queryBalancePicResponseData"),
						BalancePicData.class);

				dashboardResponseData.setQueryBalancePicResponseData(balancePicData);
			} catch (Exception e) {
				Utilities.printErrorLog(Utilities.convertStackTraceToString(e), logger);
				dashboardResponseData.setQueryBalancePicResponseData(null);
				dashboardResponseData.setReturnCode("400");
				dashboardResponseData.setReturnMsg("Not Successful");

			}

			// START OF Free Unit parsing

			try {
				FreeResourceDashboard freeUnitPArsing = mapper.readValue(
						Utilities.getValueFromJSON(jsonObject.toString(), "freeResourceDashboard"),
						FreeResourceDashboard.class);
				Utilities.printDebugLog(msisdn + "-freeResourceDashboard"
						+ Utilities.getValueFromJSON(jsonObject.toString(), "freeResources"), logger);
				dashboardResponseData.setFreeResourceDashboard(freeUnitPArsing);
			} catch (Exception e) {
				Utilities.printErrorLog(Utilities.convertStackTraceToString(e), logger);
				dashboardResponseData.setFreeResourceDashboard(null);
				;
				dashboardResponseData.setReturnCode("400");
				dashboardResponseData.setReturnMsg("Not Successful");

			}
			// END OF FRee UNITS PARsing

			JSONArray jsonarr = new JSONArray();
			try {
				if (!(jsonObject.get("queryInvoiceResponseData") instanceof JSONArray)) {
					logger.info("<<<<<<<<<<<<<<< Invoice Response Data is empty >>>>>>>>>>>>>>>");
				} else {
					jsonarr = jsonObject.getJSONArray("queryInvoiceResponseData");
					QueryInvoiceData[] queryInvoiceData = new QueryInvoiceData[jsonarr.length()];
					for (int i = 0; i < jsonarr.length(); i++) {

						queryInvoiceData[i] = (QueryInvoiceData) mapper.readValue((jsonarr.getString(i)),
								QueryInvoiceData.class);
						queryInvoiceData[i].setDueDateDisp( queryInvoiceData[i].getDueDateDisp());
						queryInvoiceData[i].setInvoiceDateDisp(queryInvoiceData[i].getInvoiceDateDisp());
						// System.out.println(queryInvoiceData[i].getDueDate());
					}
					dashboardResponseData.setQueryInvoiceResponseData(queryInvoiceData);
				}
			} catch (Exception e) {
				e.printStackTrace();
				logger.error("INVOICE ERROR", e);
				dashboardResponseData.setQueryInvoiceResponseData(null);
				dashboardResponseData.setReturnCode("400");
				dashboardResponseData.setReturnMsg("Not Successful");
			}
		} catch (Exception e) {
			logger.error("Login Error:", e);
			dashboardResponseData.setLoginData(null);
			dashboardResponseData.setReturnCode("400");
			dashboardResponseData.setReturnMsg("Not Successful");
		}

		return dashboardResponseData;
	}

	private boolean isHashMatched(String msisdn, String passHash, LoginData customerInfo)
			throws ClassNotFoundException, SQLException, JSONException, UnsupportedEncodingException, SocketException {
		/**
		 * We already called Database to get Email and Password hash at the time of
		 * Customer Info and stored hash into token. Therefore, rather than calling DB
		 * again to get hash, getting updated hash from token to compare it with old
		 * token which we got from requested call.
		 *
		 */

		JSONObject tokenSplitted = Utilities.splitToken(Utilities.decodeString(customerInfo.getToken()));
		if (tokenSplitted.has("passHash")) {
			String hashFromDB = Utilities.getValueFromJSON(
					Utilities.splitToken(Utilities.decodeString(customerInfo.getToken())).toString(), "passHash");
			Utilities.printDebugLog(msisdn + "- OLD PASSWORD HASH OBTAINED FROM TOKEN" + passHash, logger);
			Utilities.printDebugLog(msisdn + "- NEW PASSWORD HASH FETCHED FROM MAGENTO-" + hashFromDB, logger);
			if (passHash.equals(hashFromDB))
				Utilities.printDebugLog(msisdn + "- PASSWORD HASH MATCHED SUCCESSFULLY", logger);
			else
				Utilities.printDebugLog(msisdn + "- PASSWORD HASH MATCHED FAILED", logger);
			return ((passHash.equals(hashFromDB)));
		}
		return false;
		// return true;
	}

	public static void main(String[] args) throws JsonParseException, JsonMappingException, JSONException, IOException {
		String jsonstring = "{\"msisdn\":\"774052004\",\"tariffPlan\":\"1262948341\",\"crmSubId\":\"2000024427086\",\"crmCustId\":\"1000023935880\",\"crmCustCode\":\"11000003929879\",\"virtualCorpCode\":\"21000001723217\",\"crmCorpCustId\":\"1000021728481\",\"cugGroupCode\":\"3.65237\",\"groupCode\":\"\",\"crmAccountId\":\"4000124765702\",\"accCode\":\"11000004653486\",\"crmAccIdPaid\":\"\",\"groupName\":\"PayBySubs\",\"groupNameDisplay\":\"Paid By User\",\"tariffNameDisplay\":\"\"}";
		// DashboardResponse dashboardResponse = new DashboardResponse();
		// parsejson(jsonstring, dashboardResponse);
		/*
		 * UsersGroupResponse resData = new UsersGroupResponse(); UsersGroupResponse
		 * usersGroupResponse = new UsersGroupResponse();
		 * resData=parsejsonforGroupData("pic_new1", "1", jsonstring,
		 * usersGroupResponse);
		 */

		JSONObject object = new JSONObject(jsonstring);
		JSONObject obj = object.getJSONObject("groupData");
		System.out.println(obj.keys().toString());

	}

}
