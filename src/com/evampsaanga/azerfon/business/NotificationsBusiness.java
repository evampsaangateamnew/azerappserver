/**
 * 
 */
package com.evampsaanga.azerfon.business;

import java.net.SocketException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.evampsaanga.azerfon.common.messagesmappings.GetMessagesMappings;
import com.evampsaanga.azerfon.common.utilities.Constants;
import com.evampsaanga.azerfon.common.utilities.DBUtility;
import com.evampsaanga.azerfon.common.utilities.GetConfigurations;
import com.evampsaanga.azerfon.common.utilities.Transactions;
import com.evampsaanga.azerfon.common.utilities.Utilities;
import com.evampsaanga.azerfon.models.notifications.addfcm.AddFCMRequest;
import com.evampsaanga.azerfon.models.notifications.addfcm.AddFCMResponse;
import com.evampsaanga.azerfon.models.notifications.addfcm.AddFCMResponseData;
import com.evampsaanga.azerfon.models.notifications.getnotifications.GetNotificationsRequest;
import com.evampsaanga.azerfon.models.notifications.getnotifications.GetNotificationsResponse;
import com.evampsaanga.azerfon.models.notifications.getnotifications.GetNotificationsResponseData;
import com.evampsaanga.azerfon.models.notifications.getnotifications.Notification;
import com.evampsaanga.azerfon.models.notifications.getnotificationscount.GetNotificationsCountRequest;
import com.evampsaanga.azerfon.models.notifications.getnotificationscount.GetNotificationsCountResponse;
import com.evampsaanga.azerfon.models.notifications.getnotificationscount.GetNotificationsCountResponseData;

/**
 * @author Evamp & Saanga
 *
 */
public class NotificationsBusiness {
    Logger logger = Logger.getLogger(NotificationsBusiness.class);

    public GetNotificationsResponse getNotificationsBusiness(String msisdn,
	    GetNotificationsRequest getNotificationsRequest, GetNotificationsResponse getNotificationsResponse)
	    throws ClassNotFoundException, SQLException, ParseException, SocketException {
	Utilities.printDebugLog(msisdn + "-Request received in " + Transactions.NOTIFICATIONS_HISTORY_TRANSACTION_NAME
		+ " BUSINESS with data-" + getNotificationsRequest.toString(), logger);

	Utilities.printDebugLog(msisdn + "-Request Call to DB", logger);

	getNotificationsResponse.setData(this.getNotificationsFromDB(getNotificationsRequest));
	getNotificationsResponse.setCallStatus(Constants.Call_Status_True);
	getNotificationsResponse.setResultCode(Constants.APP_SERVER_SUCCESS_CODE);
	getNotificationsResponse.setResultDesc(
		GetMessagesMappings.getMessageFromResourceBundle("success", getNotificationsRequest.getLang()));

	return getNotificationsResponse;

    }

    public AddFCMResponse addFCMBusiness(String msisdn, String deviceID, AddFCMRequest addFCMRequest,
	    AddFCMResponse addFCMResponse) throws ClassNotFoundException, SQLException, SocketException {
	Utilities.printDebugLog(msisdn + "-Request received in " + Transactions.ADD_FCM_KEY_TRANSACTION_NAME
		+ " BUSINESS with data-" + addFCMRequest.toString(), logger);
	AddFCMResponseData resData = new AddFCMResponseData();

	Utilities.printDebugLog(msisdn + "-Request Call to DB", logger);
	if (addFCMRequest.getCause().equalsIgnoreCase(Constants.ADD_FCM_CAUSE_SETTINGS)) {
	    resData = this.addUpdateFCMConfigurations(addFCMRequest, deviceID, resData);
	} else if (addFCMRequest.getCause().equalsIgnoreCase(Constants.ADD_FCM_CAUSE_LOGIN)) {
	    resData = this.isConfigExists(deviceID, addFCMRequest, resData);
	    /*
	     * either has data or not, if has data then return otherwise add data into
	     * database.
	     */
	    if (!this.hasData(resData)) {
		resData = this.addUpdateFCMConfigurations(addFCMRequest, deviceID, resData);
	    }
	}

	addFCMResponse.setData(resData);
	addFCMResponse.setCallStatus(Constants.Call_Status_True);
	addFCMResponse.setResultCode(Constants.APP_SERVER_SUCCESS_CODE);
	addFCMResponse
		.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("success", addFCMRequest.getLang()));

	return addFCMResponse;
    }

    public GetNotificationsCountResponse getNotificationsCountBusiness(String msisdn,
	    GetNotificationsCountRequest getNotificationsCountRequest,
	    GetNotificationsCountResponse getNotificationsCountResponse)
	    throws ClassNotFoundException, SQLException, SocketException {
	Utilities.printDebugLog(msisdn + "-Request received in " + Transactions.NOTIFICATIONS_COUNT_TRANSACTION_NAME
		+ " BUSINESS with data-" + getNotificationsCountRequest.toString(), logger);

	GetNotificationsCountResponseData resData = new GetNotificationsCountResponseData();

	resData.setNotificationUnreadCount(getUnreadCountByMSISDN(getNotificationsCountRequest.getMsisdn()));
	getNotificationsCountResponse.setData(resData);
	getNotificationsCountResponse.setCallStatus(Constants.Call_Status_True);
	getNotificationsCountResponse.setResultCode(Constants.APP_SERVER_SUCCESS_CODE);
	getNotificationsCountResponse.setResultDesc(
		GetMessagesMappings.getMessageFromResourceBundle("success", getNotificationsCountRequest.getLang()));

	return getNotificationsCountResponse;
    }

    /*
     * Below method will insert configurations parameters if not already stored in
     * database otherwise will update the previous ones.
     */
    public AddFCMResponseData addUpdateFCMConfigurations(AddFCMRequest addFCMRequest, String deviceID,
	    AddFCMResponseData resData) throws SQLException, ClassNotFoundException, SocketException {
	Utilities.printDebugLog(addFCMRequest.getMsisdn() + "-Request received in "
		+ Transactions.ADD_UPDATE_NOTIFICATIONS_CONFIGURATIONS + " Business with data-"
		+ addFCMRequest.toString(), logger);
	String queryStr = "";

	Connection connection = DBUtility.getConnection();
	PreparedStatement preparedStatement = null;
	if (addFCMRequest.getIsEnable().equalsIgnoreCase("0")) {
	    Utilities.printDebugLog(addFCMRequest.getMsisdn() + "-Request received for Turning Off Notification.",
		    logger);
	    queryStr = GetConfigurations.getDBConfig("muteNotificationAgainstNumber");
	    preparedStatement = connection.prepareStatement(queryStr);
	    preparedStatement.setString(1, addFCMRequest.getIsEnable());
	    preparedStatement.setString(2, addFCMRequest.getMsisdn());

	    Utilities.printDebugLog(addFCMRequest.getMsisdn() + "-Request received for Mute Notification QUERY:"
		    + Utilities.getQueryFromPrepareStatement(preparedStatement), logger);

	    Utilities.printDebugLog(addFCMRequest.getMsisdn() + "-Request received for Mute Notification QUERY RESULT:"
		    + preparedStatement.executeUpdate(), logger);

	    // As we are inserting notifications settings but need to return the
	    // same setting back to device.
	    resData.setIsEnable(addFCMRequest.getIsEnable());
	    resData.setRingingStatus(addFCMRequest.getRingingStatus());

	} else if (addFCMRequest.getIsEnable().equalsIgnoreCase("1")
		&& addFCMRequest.getCause().equalsIgnoreCase(Constants.ADD_FCM_CAUSE_SETTINGS)) {
	    Utilities.printDebugLog(
		    addFCMRequest.getMsisdn() + "-Request received from (Settings) for Turning ON Notification.",
		    logger);
	    queryStr = GetConfigurations.getDBConfig("updateFCMStatus");
	    preparedStatement = connection.prepareStatement(queryStr);
	    preparedStatement.setString(1, addFCMRequest.getIsEnable());
	    preparedStatement.setString(2, addFCMRequest.getMsisdn());

	    Utilities.printDebugLog(
		    addFCMRequest.getMsisdn() + "--Request received from (Settings) for Turning ON Notification QUERY:"
			    + Utilities.getQueryFromPrepareStatement(preparedStatement),
		    logger);

	    Utilities.printDebugLog(addFCMRequest.getMsisdn()
		    + "-R-Request received from (Settings) for Turning ON Notification QUERY RESULT:"
		    + preparedStatement.executeUpdate(), logger);

	    resData.setIsEnable(addFCMRequest.getIsEnable());
	    resData.setRingingStatus(addFCMRequest.getRingingStatus());

	} else {
	    queryStr = GetConfigurations.getDBConfig("insertFCMKey");
	    preparedStatement = connection.prepareStatement(queryStr);
	    preparedStatement.setString(1, addFCMRequest.getMsisdn() + "@" + deviceID);
	    preparedStatement.setString(2, deviceID);
	    preparedStatement.setString(3, addFCMRequest.getMsisdn());
	    preparedStatement.setString(4, addFCMRequest.getFcmKey());
	    preparedStatement.setString(5, addFCMRequest.getRingingStatus());
	    preparedStatement.setString(6, addFCMRequest.getIsEnable());
	    preparedStatement.setString(7, addFCMRequest.getSubscriberType());
	    preparedStatement.setString(8, addFCMRequest.getTariffType());
	    preparedStatement.setString(9, addFCMRequest.getChannel());

	    // On Duplicate Key following parameters will be replaced.
	    preparedStatement.setString(10, addFCMRequest.getMsisdn() + "@" + deviceID);
	    preparedStatement.setString(11, deviceID);
	    preparedStatement.setString(12, addFCMRequest.getMsisdn());
	    preparedStatement.setString(13, addFCMRequest.getFcmKey());
	    preparedStatement.setString(14, addFCMRequest.getRingingStatus());
	    preparedStatement.setString(15, addFCMRequest.getIsEnable());
	    preparedStatement.setString(16, addFCMRequest.getSubscriberType());
	    preparedStatement.setString(17, addFCMRequest.getTariffType());

	    Utilities.printDebugLog(addFCMRequest.getChannel() + "-CHANNEL LOGGER ##################:", logger);

	    Utilities.printDebugLog(addFCMRequest.getMsisdn() + "-ADD/Update NOTIFICATIONS CONFIGURATIONS QUERY:"
		    + Utilities.getQueryFromPrepareStatement(preparedStatement), logger);

	    Utilities.printDebugLog(addFCMRequest.getMsisdn() + "-ADD/Update NOTIFICATIONS CONFIGURATIONS QUERY RESULT:"
		    + preparedStatement.executeUpdate(), logger);

	    // As we are inserting notifications settings but need to return the
	    // same setting back to device.
	    resData.setIsEnable(addFCMRequest.getIsEnable());
	    resData.setRingingStatus(addFCMRequest.getRingingStatus());
	}

	if (preparedStatement != null)
	    preparedStatement.close();
	return resData;
    }

    public int updateFCMKey(String deviceID, AddFCMRequest addFCMRequest)
	    throws SQLException, ClassNotFoundException, SocketException {
	Utilities.printDebugLog(addFCMRequest.getMsisdn() + "-UPDATE FCM KEY DATA:" + addFCMRequest.toString(), logger);
	String queryStr = GetConfigurations.getDBConfig("updateFCMKey");
	Connection connection = DBUtility.getConnection();
	PreparedStatement preparedStatement = connection.prepareStatement(queryStr);
	preparedStatement.setString(1, addFCMRequest.getFcmKey());
	preparedStatement.setString(2, addFCMRequest.getSubscriberType());
	preparedStatement.setString(3, addFCMRequest.getTariffType());
	preparedStatement.setString(4, addFCMRequest.getMsisdn());
	preparedStatement.setString(5, deviceID);
	Utilities.printDebugLog(addFCMRequest.getMsisdn() + "-UPDATE FCM KEY QUERY:"
		+ Utilities.getQueryFromPrepareStatement(preparedStatement), logger);
	int queryResult = preparedStatement.executeUpdate();
	Utilities.printDebugLog(addFCMRequest.getMsisdn() + "- UPDATE FCM KEY QUERY RESULT:" + queryResult, logger);

	if (preparedStatement != null)
	    preparedStatement.close();
	return queryResult;
    }

    public AddFCMResponseData isConfigExists(String deviceID, AddFCMRequest addFCMRequest, AddFCMResponseData resData)
	    throws SQLException, ClassNotFoundException, SocketException {

	String queryStr = GetConfigurations.getDBConfig("getSettingByDeviceAndMSISDN");
	Connection connection = DBUtility.getConnection();
	PreparedStatement preparedStatement = connection.prepareStatement(queryStr);
	preparedStatement.setString(1, deviceID);
	preparedStatement.setString(2, addFCMRequest.getMsisdn());
	Utilities.printDebugLog(addFCMRequest.getMsisdn() + "- GET NOTIFICATIONS CONFIGURATIONS QUERY:"
		+ Utilities.getQueryFromPrepareStatement(preparedStatement), logger);

	ResultSet rs = preparedStatement.executeQuery();

	while (rs.next()) {
	    resData.setIsEnable(rs.getString("is_enable"));
	    resData.setRingingStatus(rs.getString("ringing_action"));
	}
	if (rs != null)
	    rs.close();
	if (preparedStatement != null)
	    preparedStatement.close();
	Utilities.printDebugLog(addFCMRequest.getMsisdn() + "-Request Call Update FCM KEY.", logger);
	updateFCMKey(deviceID, addFCMRequest);
	return resData;
    }

    private GetNotificationsResponseData getNotificationsFromDB(GetNotificationsRequest getNotificationsRequest)
	    throws SQLException, ClassNotFoundException, ParseException, SocketException {
	GetNotificationsResponseData resData = new GetNotificationsResponseData();
	List<Notification> notificationsList = new ArrayList<>();

	String queryStr = GetConfigurations.getDBConfig("getNotificationsByMSISDN");
	Connection connection = DBUtility.getConnection();
	PreparedStatement preparedStatement = connection.prepareStatement(queryStr);
	preparedStatement.setString(1, getNotificationsRequest.getMsisdn());

	Utilities.printDebugLog(getNotificationsRequest.getMsisdn() + "- GET NOTIFICATIONS QUERY:"
		+ Utilities.getQueryFromPrepareStatement(preparedStatement), logger);
	ResultSet rs = preparedStatement.executeQuery();

	while (rs.next()) {

	    Notification notification = new Notification();

	    notification.setDatetime(Utilities.parseDateDynamically(getNotificationsRequest.getMsisdn(),
		    rs.getString("datetime"), "dd/MM/YY HH:mm:ss"));

	    if (getNotificationsRequest.getLang().equalsIgnoreCase("4")) {
		notification.setMessage(rs.getString("messageAzeri"));
	    } else if (getNotificationsRequest.getLang().equalsIgnoreCase("3")) {
		notification.setMessage(rs.getString("messageEnglish"));
	    } else if (getNotificationsRequest.getLang().equalsIgnoreCase("2")) {
		notification.setMessage(rs.getString("messageRussian"));
	    }

	    notification.setIcon(rs.getString("icon"));
	    notification.setActionType(rs.getString("actionType"));
	    notification.setActionID(rs.getString("actionID"));
	    notification.setBtnTxt(rs.getString("btnTxt"));

	    notificationsList.add(notification);
	}
	resData.setNotificationsList(notificationsList);
	Utilities.printDebugLog(getNotificationsRequest.getMsisdn() + "-Fetching notifications from database ended.",
		logger);
	if (rs != null)
	    rs.close();
	if (preparedStatement != null)
	    preparedStatement.close();

	/*
	 * As per requirement, When user view history then all notifications read status
	 * against that subscriber number will changed from unread to read.
	 */

	if (!resData.getNotificationsList().isEmpty())
	    updateReadStatus(getNotificationsRequest);
	return resData;
    }

    private void updateReadStatus(GetNotificationsRequest getNotificationsRequest)
	    throws SQLException, ClassNotFoundException, SocketException {
	Utilities.printDebugLog(
		getNotificationsRequest.getMsisdn() + "-UPDATE READ STATUS DATA:" + getNotificationsRequest.toString(),
		logger);
	String queryStr = GetConfigurations.getDBConfig("updateReadStatus");
	Connection connection = DBUtility.getConnection();
	PreparedStatement preparedStatement = connection.prepareStatement(queryStr);
	preparedStatement.setString(1, "0");
	preparedStatement.setString(2, getNotificationsRequest.getMsisdn());

	Utilities.printDebugLog(getNotificationsRequest.getMsisdn() + "-UPDATE READ STATUS QUERY:"
		+ Utilities.getQueryFromPrepareStatement(preparedStatement), logger);
	int queryResult = preparedStatement.executeUpdate();
	Utilities.printDebugLog(
		getNotificationsRequest.getMsisdn() + "- UPDATE READ STATUS QUERY RESULT:" + queryResult, logger);

	if (preparedStatement != null)
	    preparedStatement.close();
    }

    public String getUnreadCountByMSISDN(String msisdn) throws SQLException, ClassNotFoundException, SocketException {
	int count = 0;
	String queryStr = GetConfigurations.getDBConfig("getCountByMSISDN");
	Connection connection = DBUtility.getConnection();
	PreparedStatement preparedStatement = connection.prepareStatement(queryStr);
	preparedStatement.setString(1, msisdn);
	preparedStatement.setString(2, "1");
	Utilities.printDebugLog(
		msisdn + "- GET NOTIFICATIONS COUNT QUERY:" + Utilities.getQueryFromPrepareStatement(preparedStatement),
		logger);

	ResultSet rs = preparedStatement.executeQuery();
	// int i =0;
	while (rs.next()) {
	    count += rs.getInt(1);
	}
	if (rs != null)
	    rs.close();
	if (preparedStatement != null)
	    preparedStatement.close();

	return String.valueOf(count);
    }

    private boolean hasData(AddFCMResponseData resData) {
	if ((resData.getIsEnable() == null || resData.getIsEnable().equalsIgnoreCase("null")
		|| (resData.getRingingStatus() == null || resData.getRingingStatus().equalsIgnoreCase("null")))) {
	    return false;
	}
	return true;
    }

}
