/**
 * 
 */
package com.evampsaanga.azerfon.business;

import java.io.File;
import java.io.IOException;
import java.net.SocketException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import com.evampsaanga.azerfon.common.messagesmappings.GetMessagesMappings;
import com.evampsaanga.azerfon.common.utilities.AppCache;
import com.evampsaanga.azerfon.common.utilities.Constants;
import com.evampsaanga.azerfon.common.utilities.GetConfigurations;
import com.evampsaanga.azerfon.common.utilities.Transactions;
import com.evampsaanga.azerfon.common.utilities.Utilities;
import com.evampsaanga.azerfon.models.acceptTNC.AcceptTnCRequest;
import com.evampsaanga.azerfon.models.acceptTNC.AcceptTnCResponse;
import com.evampsaanga.azerfon.models.acceptTNC.AcceptTnCResponseData;
import com.evampsaanga.azerfon.models.companyinvoice.CompanyInvoiceRequest;
import com.evampsaanga.azerfon.models.companyinvoice.CompanyInvoiceResponse;
import com.evampsaanga.azerfon.models.generalservices.contactus.ContactUsRequest;
import com.evampsaanga.azerfon.models.generalservices.contactus.ContactUsResponse;
import com.evampsaanga.azerfon.models.generalservices.contactus.ContactUsResponseData;
import com.evampsaanga.azerfon.models.generalservices.exchangeservice.ExchangeServiceData;
import com.evampsaanga.azerfon.models.generalservices.exchangeservice.ExchangeServiceRequestClient;
import com.evampsaanga.azerfon.models.generalservices.exchangeservice.ExchangeServiceResponse;
import com.evampsaanga.azerfon.models.generalservices.exchangeservice.ExchangeServiceValuesRequest;
import com.evampsaanga.azerfon.models.generalservices.exchangeservice.ExchangeServicesValuesData;
import com.evampsaanga.azerfon.models.generalservices.exchangeservice.ExchangeServicesValuesEsbResponse;
import com.evampsaanga.azerfon.models.generalservices.exchangeservice.ExchangeServicesValuesResponse;
import com.evampsaanga.azerfon.models.generalservices.faqs.FAQS;
import com.evampsaanga.azerfon.models.generalservices.faqs.FAQSListData;
import com.evampsaanga.azerfon.models.generalservices.faqs.FAQSRequest;
import com.evampsaanga.azerfon.models.generalservices.faqs.FAQSResponse;
import com.evampsaanga.azerfon.models.generalservices.faqs.FAQSResponseData;
import com.evampsaanga.azerfon.models.generalservices.getroaming.GetRoamingEsbResponse;
import com.evampsaanga.azerfon.models.generalservices.getroaming.GetRoamingRequest;
import com.evampsaanga.azerfon.models.generalservices.getroaming.GetRoamingResponse;
import com.evampsaanga.azerfon.models.generalservices.lostreport.LostSIMResponseData;
import com.evampsaanga.azerfon.models.generalservices.lostreport.ReportLostSIMRequest;
import com.evampsaanga.azerfon.models.generalservices.lostreport.ReportLostSIMResponse;
import com.evampsaanga.azerfon.models.generalservices.sendinternetsettings.SendInternetSettingsRequest;
import com.evampsaanga.azerfon.models.generalservices.sendinternetsettings.SendInternetSettingsResponse;
import com.evampsaanga.azerfon.models.generalservices.sendinternetsettings.SendInternetSettingsResponseData;
import com.evampsaanga.azerfon.models.generalservices.storelocator.StoreLocatorRequest;
import com.evampsaanga.azerfon.models.generalservices.storelocator.StoreLocatorResponse;
import com.evampsaanga.azerfon.models.generalservices.storelocator.StoreLocatorResponseData;
import com.evampsaanga.azerfon.models.generalservices.uploadimage.UploadImageRequest;
import com.evampsaanga.azerfon.models.generalservices.uploadimage.UploadImageResponse;
import com.evampsaanga.azerfon.models.generalservices.uploadimage.UploadImageResponseData;
import com.evampsaanga.azerfon.models.msisdninvoice.MSISDNDetailsResponse;
import com.evampsaanga.azerfon.models.msisdninvoice.MSISDNInvoiceRequest;
import com.evampsaanga.azerfon.models.msisdninvoice.MSISDNSummaryResponse;
import com.evampsaanga.azerfon.models.simswap.SimSwapRequest;
import com.evampsaanga.azerfon.models.simswap.SimSwapResponse;
import com.evampsaanga.azerfon.models.simswap.SimSwapResponseData;
import com.evampsaanga.azerfon.models.verifyappversion.TimeStamps;
import com.evampsaanga.azerfon.models.verifyappversion.VerifyAppVersionRequest;
import com.evampsaanga.azerfon.models.verifyappversion.VerifyAppVersionResponse;
import com.evampsaanga.azerfon.models.verifyappversion.VerifyAppVersionResponseData;
import com.evampsaanga.azerfon.restclient.RestClient;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Class is very good
 * {@link com.evampsaanga.azerfon.models.generalservices.faqs.FAQSRequest FAQ}
 * 
 * @author Evamp Saanga
 *
 */
public class GeneralServicesBusiness {
    Logger logger = Logger.getLogger(GeneralServicesBusiness.class);

    public FAQSResponse faqsBusiness(String msisdn, FAQSRequest faqsRequest, FAQSResponse faqsResponse)
	    throws Exception {

	Utilities.printDebugLog(msisdn + "-Request received in " + Transactions.APP_FAQ_TRANSACTION_NAME
		+ " BUSINESS with data-" + faqsRequest.toString(), logger);

	FAQSResponseData resData = new FAQSResponseData();
	RestClient rc = new RestClient();
	ObjectMapper mapper = new ObjectMapper();

	// Get key which will be used to store and retrieve data from hash map.
	String faqsCacheKey = this.getKeyForCache(Constants.HASH_KEY_FAQs, faqsRequest.getLang());

	if (AppCache.getHashmapFAQs().containsKey(faqsCacheKey)) {
	    Utilities.printDebugLog(msisdn + "-FAQs" + Constants.CACHE_EXISTS_DESCRIPTION + "" + faqsCacheKey, logger);

	    faqsResponse = AppCache.getHashmapFAQs().get(faqsCacheKey);
	    faqsResponse.getLogsReport().setIsCached("true");
	    return faqsResponse;
	} else {
	    Utilities.printDebugLog(msisdn + "-FAQs" + Constants.CACHE_DOES_NOT_EXIST_DESCRIPTION + "" + faqsCacheKey,
		    logger);
	    String requestJsonESB = mapper.writeValueAsString(faqsRequest);

	    String path = GetConfigurations.getESBRoute("getFaqs");
	    Utilities.printDebugLog(msisdn + "-Request Call to ESB", logger);
	    Utilities.printDebugLog(msisdn + "-Path-" + path, logger);
	    Utilities.printDebugLog(msisdn + "-Request Packet-" + requestJsonESB, logger);

	    faqsResponse.getLogsReport().setEsbRequestTime(Utilities.getReportDateTime());
	    String response = rc.getResponseFromESB(path, requestJsonESB);
	    faqsResponse.getLogsReport().setEsbResponseTime(Utilities.getReportDateTime());

	    Utilities.printDebugLog(msisdn + "-Received response from ESB-" + response, logger);

	    // Logging ESB response code and description.
	    faqsResponse.setLogsReport(
		    Utilities.logESBParamsintoReportLog(requestJsonESB, response, faqsResponse.getLogsReport()));

	    if (response != null && !response.isEmpty()) {

		if (Utilities.getValueFromJSON(response, "returnCode").equalsIgnoreCase(Constants.ESB_SUCCESS_CODE)) {

		    String responseArray = new JSONObject(response).getJSONArray("data").toString();

		    resData.setFaqlist(parseFAQsData(responseArray));
		    faqsResponse.setData(resData);
		    faqsResponse.setCallStatus(Constants.Call_Status_True);
		    faqsResponse.setResultCode(Constants.APP_SERVER_SUCCESS_CODE);
		    faqsResponse.setResultDesc(
			    GetMessagesMappings.getMessageFromResourceBundle("success", faqsRequest.getLang()));

		    // Storing response in hashmap.
		    AppCache.getHashmapFAQs().put(faqsCacheKey, faqsResponse);

		    // Caching Time-stamp
		    AppCache.getHashmapTimestamps().put(Constants.HASH_KEY_FAQs,
			    Utilities.getTimeStampForCache(Constants.HASH_KEY_FAQs));
		} else {

		    faqsResponse.setCallStatus(Constants.Call_Status_False);
		    faqsResponse.setResultCode(Utilities.getValueFromJSON(response, "returnCode"));
		    faqsResponse.setResultDesc(Utilities.getErrorMessageFromFile("faqs", faqsRequest.getLang(),
			    Utilities.getValueFromJSON(response, "returnCode")));
		}

	    } else {
		faqsResponse.setCallStatus(Constants.Call_Status_False);
		faqsResponse.setResultCode(Constants.API_FAILURE_CODE);

		faqsResponse.setResultDesc(
			GetMessagesMappings.getMessageFromResourceBundle("connectivity.error", faqsRequest.getLang()));
	    }

	    return faqsResponse;
	}
    }

    // for phase 2
    public FAQSResponse faqsBusinessV2(String msisdn, FAQSRequest faqsRequest, FAQSResponse faqsResponse)
	    throws Exception {

	Utilities.printDebugLog(msisdn + "-Request received in " + Transactions.APP_FAQ_TRANSACTION_NAME
		+ " BUSINESS with data-" + faqsRequest.toString(), logger);

	FAQSResponseData resData = new FAQSResponseData();
	RestClient rc = new RestClient();
	ObjectMapper mapper = new ObjectMapper();

	// Get key which will be used to store and retrieve data from hash map.
	String faqsCacheKey = this.getKeyForCache(Constants.HASH_KEY_FAQs, faqsRequest.getLang());

	if (AppCache.getHashmapFAQs().containsKey(faqsCacheKey)) {
	    Utilities.printDebugLog(msisdn + "-FAQs" + Constants.CACHE_EXISTS_DESCRIPTION + "" + faqsCacheKey, logger);

	    faqsResponse = AppCache.getHashmapFAQs().get(faqsCacheKey);
	    faqsResponse.getLogsReport().setIsCached("true");
	    return faqsResponse;
	} else {
	    Utilities.printDebugLog(msisdn + "-FAQs" + Constants.CACHE_DOES_NOT_EXIST_DESCRIPTION + "" + faqsCacheKey,
		    logger);
	    String requestJsonESB = mapper.writeValueAsString(faqsRequest);

	    String path = GetConfigurations.getESBRoute("getFaqsV2");
	    Utilities.printDebugLog(msisdn + "-Request Call to ESB", logger);
	    Utilities.printDebugLog(msisdn + "-Path-" + path, logger);
	    Utilities.printDebugLog(msisdn + "-Request Packet-" + requestJsonESB, logger);

	    faqsResponse.getLogsReport().setEsbRequestTime(Utilities.getReportDateTime());
	    String response = rc.getResponseFromESB(path, requestJsonESB);
	    faqsResponse.getLogsReport().setEsbResponseTime(Utilities.getReportDateTime());

	    Utilities.printDebugLog(msisdn + "-Received response from ESB-" + response, logger);

	    // Logging ESB response code and description.
	    faqsResponse.setLogsReport(
		    Utilities.logESBParamsintoReportLog(requestJsonESB, response, faqsResponse.getLogsReport()));

	    if (response != null && !response.isEmpty()) {

		if (Utilities.getValueFromJSON(response, "returnCode").equalsIgnoreCase(Constants.ESB_SUCCESS_CODE)) {

		    String responseArray = new JSONObject(response).getJSONArray("data").toString();

		    resData.setFaqlist(parseFAQsData(responseArray));
		    faqsResponse.setData(resData);
		    faqsResponse.setCallStatus(Constants.Call_Status_True);
		    faqsResponse.setResultCode(Constants.APP_SERVER_SUCCESS_CODE);
		    faqsResponse.setResultDesc(
			    GetMessagesMappings.getMessageFromResourceBundle("success", faqsRequest.getLang()));

		    // Storing response in hashmap.
		    AppCache.getHashmapFAQs().put(faqsCacheKey, faqsResponse);

		    // Caching Time-stamp
		    AppCache.getHashmapTimestamps().put(Constants.HASH_KEY_FAQs,
			    Utilities.getTimeStampForCache(Constants.HASH_KEY_FAQs));
		} else {

		    faqsResponse.setCallStatus(Constants.Call_Status_False);
		    faqsResponse.setResultCode(Utilities.getValueFromJSON(response, "returnCode"));
		    faqsResponse.setResultDesc(Utilities.getErrorMessageFromFile("faqs", faqsRequest.getLang(),
			    Utilities.getValueFromJSON(response, "returnCode")));
		}

	    } else {
		faqsResponse.setCallStatus(Constants.Call_Status_False);
		faqsResponse.setResultCode(Constants.API_FAILURE_CODE);

		faqsResponse.setResultDesc(
			GetMessagesMappings.getMessageFromResourceBundle("connectivity.error", faqsRequest.getLang()));
	    }

	    return faqsResponse;
	}
    }

    public ContactUsResponse contactUsBusiness(String msisdn, ContactUsRequest contactUsRequest,
	    ContactUsResponse contactUsResponse) throws Exception {

	Utilities.printDebugLog(msisdn + "-Request received in " + Transactions.CONTACTUS__TRANSACTION_NAME
		+ " BUSINESS with data-" + contactUsRequest.toString(), logger);

	// Get key which will be used to store and retrieve data from hash map.
	String contactusCacheKey = this.getKeyForCache(Constants.HASH_KEY_CONTACT_US, contactUsRequest.getLang());
	RestClient rc = new RestClient();
	ObjectMapper mapper = new ObjectMapper();

	if (AppCache.getHashmapContactUs().containsKey(contactusCacheKey)) {
	    Utilities.printDebugLog(
		    msisdn + "-Contact Us" + Constants.CACHE_EXISTS_DESCRIPTION + "" + contactusCacheKey, logger);
	    contactUsResponse = AppCache.getHashmapContactUs().get(contactusCacheKey);
	    contactUsResponse.getLogsReport().setIsCached("true");
	    return contactUsResponse;
	} else {
	    Utilities.printDebugLog(
		    msisdn + "-Contact Us" + Constants.CACHE_DOES_NOT_EXIST_DESCRIPTION + "" + contactusCacheKey,
		    logger);
	    String requestJsonESB = mapper.writeValueAsString(contactUsRequest);

	    String path = GetConfigurations.getESBRoute("getcontactusdetails");
	    Utilities.printDebugLog(msisdn + "-Request Call to ESB", logger);
	    Utilities.printDebugLog(msisdn + "-Path-" + path, logger);
	    Utilities.printDebugLog(msisdn + "-Request Packet-" + requestJsonESB, logger);

	    contactUsResponse.getLogsReport().setEsbRequestTime(Utilities.getReportDateTime());
	    String response = rc.getResponseFromESB(path, requestJsonESB);
	    contactUsResponse.getLogsReport().setEsbResponseTime(Utilities.getReportDateTime());

	    Utilities.printDebugLog(msisdn + "-Received response from ESB-" + response, logger);

	    // Logging ESB response code and description.
	    contactUsResponse.setLogsReport(
		    Utilities.logESBParamsintoReportLog(requestJsonESB, response, contactUsResponse.getLogsReport()));

	    if (response != null && !response.isEmpty()) {

		if (Utilities.getValueFromJSON(response, "returnCode").equalsIgnoreCase(Constants.ESB_SUCCESS_CODE)) {

		    contactUsResponse.setData(mapper.readValue(Utilities.getValueFromJSON(response, "data"),
			    ContactUsResponseData.class));

		    contactUsResponse.getData().setFacebookLink(contactUsResponse.getData().getFacebookLink().trim());
		    contactUsResponse.getData().setGoogleLink(contactUsResponse.getData().getGoogleLink().trim());
		    contactUsResponse.getData().setInstagram(contactUsResponse.getData().getInstagram().trim());
		    contactUsResponse.getData().setLinkedinLink(contactUsResponse.getData().getLinkedinLink().trim());
		    contactUsResponse.getData().setTwitterLink(contactUsResponse.getData().getTwitterLink().trim());
		    contactUsResponse.getData().setYoutubeLink(contactUsResponse.getData().getYoutubeLink().trim());
		    contactUsResponse.getData().setWebsite(contactUsResponse.getData().getWebsite().trim());

		    contactUsResponse.getData()
			    .setAddressLat(GetConfigurations.getConfigurationFromCache("bak.hq.lat"));
		    contactUsResponse.getData()
			    .setAddressLong(GetConfigurations.getConfigurationFromCache("bak.hq.long"));
		    contactUsResponse.getData().setInviteFriendText(GetMessagesMappings
			    .getMessageFromResourceBundle("inviteFriendMessage", contactUsRequest.getLang()));

		    contactUsResponse.setCallStatus(Constants.Call_Status_True);
		    contactUsResponse.setResultCode(Constants.APP_SERVER_SUCCESS_CODE);

		    contactUsResponse.setResultDesc(
			    GetMessagesMappings.getMessageFromResourceBundle("success", contactUsRequest.getLang()));

		    // Storing response in hashmap.
		    AppCache.getHashmapContactUs().put(contactusCacheKey, contactUsResponse);

		    // Caching Time-stamp
		    AppCache.getHashmapTimestamps().put(Constants.HASH_KEY_CONTACT_US,
			    Utilities.getTimeStampForCache(Constants.HASH_KEY_CONTACT_US));
		} else {
		    contactUsResponse.setCallStatus(Constants.Call_Status_False);
		    contactUsResponse.setResultCode(Utilities.getValueFromJSON(response, "returnCode"));
		    contactUsResponse.setResultDesc(Utilities.getErrorMessageFromFile("contact.us",
			    contactUsRequest.getLang(), Utilities.getValueFromJSON(response, "returnCode")));
		}

	    } else {

		contactUsResponse.setCallStatus(Constants.Call_Status_False);
		contactUsResponse.setResultCode(Constants.API_FAILURE_CODE);

		contactUsResponse.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("connectivity.error",
			contactUsRequest.getLang()));
	    }

	    return contactUsResponse;
	}

    }

    public StoreLocatorResponse getStoreLocatorsDetailsBusiness(String msisdn, StoreLocatorRequest storeLocatorRequest,
	    StoreLocatorResponse storeLocatorResponse) throws Exception {
	Utilities.printDebugLog(msisdn + "-Request received in " + Transactions.STORE_LOCATOR_TRANSACTION_NAME
		+ " BUSINESS with data-" + storeLocatorRequest.toString(), logger);

	StoreLocatorResponseData resData = new StoreLocatorResponseData();
	RestClient rc = new RestClient();
	ObjectMapper mapper = new ObjectMapper();

	// Get key which will be used to store and retrieve data from hash map.
	String storeLocatorCacheKey = this.getKeyForCache(Constants.HASH_KEY_STORE_LOCATOR,
		storeLocatorRequest.getLang());

	if (AppCache.getHashmapFAQs().containsKey(storeLocatorCacheKey)) {
	    Utilities.printDebugLog(
		    msisdn + "-STORE LOCATOR" + Constants.CACHE_EXISTS_DESCRIPTION + "" + storeLocatorCacheKey, logger);
	    storeLocatorResponse = AppCache.getHashmapStoreLocator().get(storeLocatorCacheKey);
	    storeLocatorResponse.getLogsReport().setIsCached("true");
	    return storeLocatorResponse;
	} else {
	    String requestJsonESB = mapper.writeValueAsString(storeLocatorRequest);
	    String path = GetConfigurations.getESBRoute("getStoresDetails");

	    Utilities.printDebugLog(msisdn + "-Request Call to ESB", logger);
	    Utilities.printDebugLog(msisdn + "-Path-" + path, logger);
	    Utilities.printDebugLog(msisdn + "-Request Packet-" + requestJsonESB, logger);
	    // String response = HardCodedResponses.STORE_LOCATOR;

	    storeLocatorResponse.getLogsReport().setEsbRequestTime(Utilities.getReportDateTime());
	    String response = rc.getResponseFromESB(path, requestJsonESB);
	    storeLocatorResponse.getLogsReport().setEsbResponseTime(Utilities.getReportDateTime());

	    Utilities.printDebugLog(msisdn + "-Received response from ESB-" + response, logger);

	    // Logging ESB response code and description.
	    storeLocatorResponse.setLogsReport(Utilities.logESBParamsintoReportLog(requestJsonESB, response,
		    storeLocatorResponse.getLogsReport()));

	    if (response != null && !response.isEmpty()) {
		if (Utilities.getValueFromJSON(response, "returnCode").equalsIgnoreCase(Constants.ESB_SUCCESS_CODE)) {
		    resData = mapper.readValue(Utilities.getValueFromJSON(response, "data"),
			    StoreLocatorResponseData.class);
		    storeLocatorResponse.setData(resData);
		    storeLocatorResponse.setCallStatus(Constants.Call_Status_True);
		    storeLocatorResponse.setResultCode(Constants.APP_SERVER_SUCCESS_CODE);
		    storeLocatorResponse.setResultDesc(
			    GetMessagesMappings.getMessageFromResourceBundle("success", storeLocatorRequest.getLang()));

		    // Storing response in hashmap
		    AppCache.getHashmapStoreLocator().put(storeLocatorCacheKey, storeLocatorResponse);

		    // Caching Time-stamp
		    AppCache.getHashmapTimestamps().put(Constants.HASH_KEY_STORE_LOCATOR,
			    Utilities.getTimeStampForCache(Constants.HASH_KEY_STORE_LOCATOR));

		} else {
		    storeLocatorResponse.setCallStatus(Constants.Call_Status_False);
		    storeLocatorResponse.setResultCode(Utilities.getValueFromJSON(response, "returnCode"));
		    storeLocatorResponse.setResultDesc(Utilities.getErrorMessageFromFile("store.locator",
			    storeLocatorRequest.getLang(), Utilities.getValueFromJSON(response, "returnCode")));

		}
	    } else {
		storeLocatorResponse.setCallStatus(Constants.Call_Status_False);
		storeLocatorResponse.setResultCode(Constants.API_FAILURE_CODE);

		storeLocatorResponse.setResultDesc(GetMessagesMappings
			.getMessageFromResourceBundle("connectivity.error", storeLocatorRequest.getLang()));
	    }

	    return storeLocatorResponse;
	}
    }

    public StoreLocatorResponse getStoreLocatorsDetailsBusinessV2(String msisdn,
	    StoreLocatorRequest storeLocatorRequest, StoreLocatorResponse storeLocatorResponse) throws Exception {
	Utilities.printDebugLog(msisdn + "-Request received in " + Transactions.STORE_LOCATOR_TRANSACTION_NAME
		+ " BUSINESS with data-" + storeLocatorRequest.toString(), logger);

	StoreLocatorResponseData resData = new StoreLocatorResponseData();
	RestClient rc = new RestClient();
	ObjectMapper mapper = new ObjectMapper();

	// Get key which will be used to store and retrieve data from hash map.
	String storeLocatorCacheKey = this.getKeyForCache(Constants.HASH_KEY_STORE_LOCATOR,
		storeLocatorRequest.getLang());

	if (AppCache.getHashmapFAQs().containsKey(storeLocatorCacheKey)) {
	    Utilities.printDebugLog(
		    msisdn + "-STORE LOCATOR" + Constants.CACHE_EXISTS_DESCRIPTION + "" + storeLocatorCacheKey, logger);
	    storeLocatorResponse = AppCache.getHashmapStoreLocator().get(storeLocatorCacheKey);
	    storeLocatorResponse.getLogsReport().setIsCached("true");
	    return storeLocatorResponse;
	} else {
	    String requestJsonESB = mapper.writeValueAsString(storeLocatorRequest);
	    String path = GetConfigurations.getESBRoute("getStoresDetailsV2");

	    Utilities.printDebugLog(msisdn + "-Request Call to ESB", logger);
	    Utilities.printDebugLog(msisdn + "-Path-" + path, logger);
	    Utilities.printDebugLog(msisdn + "-Request Packet-" + requestJsonESB, logger);
	    // String response = HardCodedResponses.STORE_LOCATOR;

	    storeLocatorResponse.getLogsReport().setEsbRequestTime(Utilities.getReportDateTime());
	    String response = rc.getResponseFromESB(path, requestJsonESB);
	    storeLocatorResponse.getLogsReport().setEsbResponseTime(Utilities.getReportDateTime());

	    Utilities.printDebugLog(msisdn + "-Received response from ESB-" + response, logger);

	    // Logging ESB response code and description.
	    storeLocatorResponse.setLogsReport(Utilities.logESBParamsintoReportLog(requestJsonESB, response,
		    storeLocatorResponse.getLogsReport()));

	    if (response != null && !response.isEmpty()) {
		if (Utilities.getValueFromJSON(response, "returnCode").equalsIgnoreCase(Constants.ESB_SUCCESS_CODE)) {
		    resData = mapper.readValue(Utilities.getValueFromJSON(response, "data"),
			    StoreLocatorResponseData.class);
		    storeLocatorResponse.setData(resData);
		    storeLocatorResponse.setCallStatus(Constants.Call_Status_True);
		    storeLocatorResponse.setResultCode(Constants.APP_SERVER_SUCCESS_CODE);
		    storeLocatorResponse.setResultDesc(
			    GetMessagesMappings.getMessageFromResourceBundle("success", storeLocatorRequest.getLang()));

		    // Storing response in hashmap
		    AppCache.getHashmapStoreLocator().put(storeLocatorCacheKey, storeLocatorResponse);

		    // Caching Time-stamp
		    AppCache.getHashmapTimestamps().put(Constants.HASH_KEY_STORE_LOCATOR,
			    Utilities.getTimeStampForCache(Constants.HASH_KEY_STORE_LOCATOR));

		} else {
		    storeLocatorResponse.setCallStatus(Constants.Call_Status_False);
		    storeLocatorResponse.setResultCode(Utilities.getValueFromJSON(response, "returnCode"));
		    storeLocatorResponse.setResultDesc(Utilities.getErrorMessageFromFile("store.locator",
			    storeLocatorRequest.getLang(), Utilities.getValueFromJSON(response, "returnCode")));

		}
	    } else {
		storeLocatorResponse.setCallStatus(Constants.Call_Status_False);
		storeLocatorResponse.setResultCode(Constants.API_FAILURE_CODE);

		storeLocatorResponse.setResultDesc(GetMessagesMappings
			.getMessageFromResourceBundle("connectivity.error", storeLocatorRequest.getLang()));
	    }

	    return storeLocatorResponse;
	}
    }

    public ReportLostSIMResponse reportLostSIMBusiness(String msisdn, ReportLostSIMRequest reportLostSIMRequest,
	    ReportLostSIMResponse reportLostSIMResponse) throws Exception {

	Utilities.printDebugLog(msisdn + "-Request received in " + Transactions.REPORT_LOST_SIM_TRANSACTION_NAME
		+ " BUSINESS with data-" + reportLostSIMRequest.toString(), logger);

	RestClient rc = new RestClient();
	ObjectMapper mapper = new ObjectMapper();
	String path = "";

	String requestJsonESB = mapper.writeValueAsString(reportLostSIMRequest);

	if (reportLostSIMRequest.getIsB2B().equalsIgnoreCase("true"))

	{
	    path = GetConfigurations.getESBRoute("reportLostSimV2");
	} else {
	    requestJsonESB = Utilities.removeParamsFromJSONObject(requestJsonESB, "virtualCorpCode");
	    requestJsonESB = Utilities.removeParamsFromJSONObject(requestJsonESB, "type");
	    path = GetConfigurations.getESBRoute("reportLostSim");
	}

	Utilities.printDebugLog(msisdn + "-Request Call to ESB", logger);
	Utilities.printDebugLog(msisdn + "-Path-" + path, logger);
	Utilities.printDebugLog(msisdn + "-Request Packet-" + requestJsonESB, logger);

	reportLostSIMResponse.getLogsReport().setEsbRequestTime(Utilities.getReportDateTime());
	String response = rc.getResponseFromESB(path, requestJsonESB);
	reportLostSIMResponse.getLogsReport().setEsbResponseTime(Utilities.getReportDateTime());

	Utilities.printDebugLog(msisdn + "-Received response from ESB-" + response, logger);

	// Logging ESB response code and description.
	reportLostSIMResponse.setLogsReport(
		Utilities.logESBParamsintoReportLog(requestJsonESB, response, reportLostSIMResponse.getLogsReport()));

	if (response != null && !response.isEmpty()) {

	    if (Utilities.getValueFromJSON(response, "returnCode").equalsIgnoreCase(Constants.ESB_SUCCESS_CODE)) {

		LostSIMResponseData respData = new LostSIMResponseData();

		respData.setMessage(Utilities.getErrorMessageFromFile("report.lost.sim", reportLostSIMRequest.getLang(),
			Utilities.getValueFromJSON(response, "returnCode")));

		reportLostSIMResponse.setData(respData);
		reportLostSIMResponse.setCallStatus(Constants.Call_Status_True);
		reportLostSIMResponse.setResultCode(Constants.APP_SERVER_SUCCESS_CODE);
		reportLostSIMResponse.setResultDesc(Utilities.getErrorMessageFromFile("report.lost.sim",
			reportLostSIMRequest.getLang(), Utilities.getValueFromJSON(response, "returnCode")));
	    } else {
		reportLostSIMResponse.setCallStatus(Constants.Call_Status_False);
		reportLostSIMResponse.setResultCode(Utilities.getValueFromJSON(response, "returnCode"));
		reportLostSIMResponse.setResultDesc(Utilities.getErrorMessageFromFile("report.lost.sim",
			reportLostSIMRequest.getLang(), Utilities.getValueFromJSON(response, "returnCode")));
	    }

	} else {

	    reportLostSIMResponse.setCallStatus(Constants.Call_Status_False);
	    reportLostSIMResponse.setResultCode(Constants.API_FAILURE_CODE);

	    reportLostSIMResponse.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("connectivity.error",
		    reportLostSIMRequest.getLang()));
	}

	return reportLostSIMResponse;
    }

    public UploadImageResponse uploadImageBusiness(String msisdn, UploadImageRequest uploadImageRequest,
	    UploadImageResponse uploadImageResponse) throws JsonParseException, JsonMappingException, IOException,
	    JSONException, SQLException, InterruptedException {
	Utilities.printDebugLog(msisdn + "-Request received in " + Transactions.UPLOAD_IMAGE_TRANSACTION_NAME
		+ " BUSINESS with data-" + uploadImageRequest.toString(), logger);
	File file = null;
	UploadImageResponseData resData = new UploadImageResponseData();

	if (uploadImageRequest.getActionType().equalsIgnoreCase("1")) {
	    if (uploadImageRequest.getExt().equalsIgnoreCase(Constants.PROFILE_IMAGE_EXT)) {
		file = Utilities.getFile(uploadImageRequest.getImage(), uploadImageRequest.getMsisdn(),
			uploadImageRequest.getExt(), GetConfigurations.getConfigurationFromCache("localPath"));
		Utilities.printDebugLog(msisdn + "-Validating File Size...", logger);
		double imageSize = Utilities.getFileSizeInKB(file.length());
		Utilities.printDebugLog(msisdn + "-Image Size: " + imageSize, logger);
		if (imageSize <= Integer
			.parseInt(GetConfigurations.getConfigurationFromCache("profileImageFileSize").trim())) {

		    resData.setImageURL(Utilities.getProfileImageURL(uploadImageRequest.getMsisdn()));
		    uploadImageResponse.setData(resData);
		    uploadImageResponse.setCallStatus(Constants.Call_Status_True);
		    uploadImageResponse.setResultCode(Constants.APP_SERVER_SUCCESS_CODE);
		    uploadImageResponse.setResultDesc(
			    GetMessagesMappings.getMessageFromResourceBundle("success", uploadImageRequest.getLang()));
		    Thread.sleep(1500);
		} else {
		    file.delete();
		    Utilities.printDebugLog(msisdn + "-File size (" + imageSize + ") exceeded the limit.", logger);
		    uploadImageResponse.setCallStatus(Constants.Call_Status_True);
		    uploadImageResponse.setResultCode(Constants.APP_SERVER_SUCCESS_CODE);
		    uploadImageResponse.setResultDesc(GetMessagesMappings
			    .getMessageFromResourceBundle("profileImageSizeExceeded", uploadImageRequest.getLang()));
		}
	    } else {
		uploadImageResponse.setCallStatus(Constants.Call_Status_False);
		uploadImageResponse.setResultCode(Constants.API_FAILURE_CODE);
		uploadImageResponse.setResultDesc(GetMessagesMappings
			.getMessageFromResourceBundle("invalidProfileImage", uploadImageRequest.getLang()));
	    }
	} else if (uploadImageRequest.getActionType().equalsIgnoreCase("3")) {

	    file = new File(Utilities.getLocalFilePath(uploadImageRequest.getMsisdn()));
	    Utilities.printDebugLog(uploadImageRequest.getMsisdn() + "-File Absolute Path:" + file.getAbsolutePath(),
		    logger);
	    if (file.exists()) {
		if (file.delete()) {

		    uploadImageResponse.setCallStatus(Constants.Call_Status_True);
		    uploadImageResponse.setResultCode(Constants.APP_SERVER_SUCCESS_CODE);
		    uploadImageResponse.setResultDesc(
			    GetMessagesMappings.getMessageFromResourceBundle("success", uploadImageRequest.getLang()));
		} else {
		    uploadImageResponse.setCallStatus(Constants.Call_Status_False);
		    uploadImageResponse.setResultCode(Constants.API_FAILURE_CODE);
		    uploadImageResponse.setResultDesc(GetMessagesMappings
			    .getMessageFromResourceBundle("appserver.unsuccess.message", uploadImageRequest.getLang()));
		}
	    } else {
		uploadImageResponse.setCallStatus(Constants.Call_Status_True);
		uploadImageResponse.setResultCode(Constants.APP_SERVER_SUCCESS_CODE);
		uploadImageResponse.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("image.not.exists",
			uploadImageRequest.getLang()));

	    }
	}
	return uploadImageResponse;
    }

    public VerifyAppVersionResponse verifyAppVersionBusiness(String msisdn, String deviceID,
	    VerifyAppVersionRequest verifyAppVersionRequest, VerifyAppVersionResponse verifyAppVersionResponse)
	    throws IOException, SQLException {
	Utilities.printDebugLog(msisdn + "-Request received in VERIFY APP VERSION BUSINESS with data-"
		+ verifyAppVersionRequest.toString(), logger);

	VerifyAppVersionResponseData resData = new VerifyAppVersionResponseData();

	String appType = "B2C";
	if (verifyAppVersionRequest.getIsB2B().equalsIgnoreCase("true"))
	    appType = "B2B";

	String appCurrentVersion = GetConfigurations
		.getConfigurationFromCache("appVersion." + verifyAppVersionRequest.getChannel() + "." + appType);

	if ((GetConfigurations
		.getConfigurationFromCache("appForeUpdate." + appType + "." + verifyAppVersionRequest.getChannel())
		.equalsIgnoreCase("true"))
		&& (!appCurrentVersion.contains(verifyAppVersionRequest.getAppversion()))) {

		resData.setTimeStamps(this.getTimeStamps(msisdn));
	    resData.setMessage(GetMessagesMappings.getMessageFromResourceBundle("forceUpdate." + appType, verifyAppVersionRequest.getLang()));
		
	    verifyAppVersionResponse.setCallStatus(Constants.Call_Status_False);
	    verifyAppVersionResponse.setResultCode(Constants.APP_FORCE_UPDATE);
	    verifyAppVersionResponse.setResultDesc(GetMessagesMappings
		    .getMessageFromResourceBundle("forceUpdate." + appType, verifyAppVersionRequest.getLang()));
	    verifyAppVersionResponse.setData(resData);

	} else if (appCurrentVersion.contains(verifyAppVersionRequest.getAppversion())) {

	    resData.setTimeStamps(this.getTimeStamps(msisdn));
	    resData.setMessage(
		    GetMessagesMappings.getMessageFromResourceBundle("appUptoDate", verifyAppVersionRequest.getLang()));

	    verifyAppVersionResponse.setData(resData);
	    verifyAppVersionResponse.setCallStatus(Constants.Call_Status_True);
	    verifyAppVersionResponse.setResultCode(Constants.APP_SERVER_SUCCESS_CODE);
	    verifyAppVersionResponse.setResultDesc(
		    GetMessagesMappings.getMessageFromResourceBundle("appUptoDate", verifyAppVersionRequest.getLang()));
	} else {

	    resData.setTimeStamps(this.getTimeStamps(msisdn));
	    resData.setMessage(GetMessagesMappings.getMessageFromResourceBundle("appNewVersion",
		    verifyAppVersionRequest.getLang()));

	    verifyAppVersionResponse.setData(resData);
	    verifyAppVersionResponse.setCallStatus(Constants.Call_Status_False);
	    verifyAppVersionResponse.setResultCode(Constants.APP_OPTIONAL_UPDATE);
	    verifyAppVersionResponse.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("appNewVersion",
		    verifyAppVersionRequest.getLang()));
	}
	return verifyAppVersionResponse;
    }

    // public VerifyAppVersionResponse verifyAppVersionBusiness(String msisdn,
    // String deviceID,
    // VerifyAppVersionRequest verifyAppVersionRequest, VerifyAppVersionResponse
    // verifyAppVersionResponse)
    // throws IOException, SQLException {
    // Utilities.printDebugLog(msisdn + "-Request received in " +
    // Transactions.VERIFY_APP_VERSION_TRANSACTION_NAME
    // + " BUSINESS with data-" + verifyAppVersionRequest.toString(), logger);
    //
    // VerifyAppVersionResponseData resData = new VerifyAppVersionResponseData();
    //
    // String appCurrentVersion = GetConfigurations
    // .getConfigurationFromCache("appVersion." +
    // verifyAppVersionRequest.getChannel());
    // String[] appver = appCurrentVersion.split(";");
    //
    // Utilities.printDebugLog("Version-" + appCurrentVersion.trim(), logger);
    // for(int i=0 ; i<appver.length ; i++){
    // Utilities.printDebugLog("Version is-" + appver[i].trim(), logger);
    // }
    // /*Utilities.printDebugLog("Version 1-" +
    // appCurrentVersion.split(";")[0].trim(), logger);
    // Utilities.printDebugLog("Version 2-" +
    // appCurrentVersion.split(";")[1].trim(), logger);*/
    // boolean flag = false;
    // if (verifyAppVersionRequest.getAppversion().trim().equals("2.0.3")
    // || verifyAppVersionRequest.getAppversion().trim().equals("2.0.3") ||
    // verifyAppVersionRequest.getAppversion().trim().equals("1.0.0")) {
    // flag = true;
    // }
    // if (!verifyAppVersionRequest.getChannel().equalsIgnoreCase("android") &&
    // !flag) {
    // resData.setTimeStamps(this.getTimeStamps(msisdn));
    // resData.setMessage(GetMessagesMappings.getMessageFromResourceBundle("appNewVersion",
    // verifyAppVersionRequest.getLang()));
    //
    // verifyAppVersionResponse.setData(resData);
    // verifyAppVersionResponse.setCallStatus(Constants.Call_Status_False);
    // verifyAppVersionResponse.setResultCode(Constants.APP_OPTIONAL_UPDATE);
    // verifyAppVersionResponse.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("appNewVersion",
    // verifyAppVersionRequest.getLang()));
    // } else {
    // if
    // (GetConfigurations.getConfigurationFromCache("appForeUpdate").equalsIgnoreCase("true")
    // && !flag) {
    //
    // verifyAppVersionResponse.setCallStatus(Constants.Call_Status_False);
    // verifyAppVersionResponse.setResultCode(Constants.APP_FORCE_UPDATE);
    // verifyAppVersionResponse.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("forceUpdate",
    // verifyAppVersionRequest.getLang()));
    //
    // } else if (flag) {
    //
    // resData.setTimeStamps(this.getTimeStamps(msisdn));
    // resData.setMessage(GetMessagesMappings.getMessageFromResourceBundle("appUptoDate",
    // verifyAppVersionRequest.getLang()));
    //
    // verifyAppVersionResponse.setData(resData);
    // verifyAppVersionResponse.setCallStatus(Constants.Call_Status_True);
    // verifyAppVersionResponse.setResultCode(Constants.APP_SERVER_SUCCESS_CODE);
    // verifyAppVersionResponse.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("appUptoDate",
    // verifyAppVersionRequest.getLang()));
    // } else {
    //
    // resData.setTimeStamps(this.getTimeStamps(msisdn));
    // resData.setMessage(GetMessagesMappings.getMessageFromResourceBundle("appNewVersion",
    // verifyAppVersionRequest.getLang()));
    //
    // verifyAppVersionResponse.setData(resData);
    // verifyAppVersionResponse.setCallStatus(Constants.Call_Status_False);
    // verifyAppVersionResponse.setResultCode(Constants.APP_OPTIONAL_UPDATE);
    // verifyAppVersionResponse.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("appNewVersion",
    // verifyAppVersionRequest.getLang()));
    // }
    // }
    // return verifyAppVersionResponse;
    // }

    public SendInternetSettingsResponse sendInternetSettingsBusiness(String msisdn,
	    SendInternetSettingsRequest sendInternetSettingsRequest,
	    SendInternetSettingsResponse sendInternetSettingsResponse) throws JSONException, IOException, SQLException {
	Utilities.printDebugLog(msisdn + "-Request received in " + Transactions.SEND_INTERNET_SETTINGS
		+ " BUSINESS with data-" + sendInternetSettingsRequest.toString(), logger);

	SendInternetSettingsResponseData resData = new SendInternetSettingsResponseData();
	RestClient rc = new RestClient();
	ObjectMapper mapper = new ObjectMapper();

	String requestJsonESB = mapper.writeValueAsString(sendInternetSettingsRequest);
	String path = GetConfigurations.getESBRoute("internetSettings");

	Utilities.printDebugLog(msisdn + "-Request Call to ESB", logger);
	Utilities.printDebugLog(msisdn + "-Path-" + path, logger);
	Utilities.printDebugLog(msisdn + "-Request Packet-" + requestJsonESB, logger);

	sendInternetSettingsResponse.getLogsReport().setEsbRequestTime(Utilities.getReportDateTime());
	String response = rc.getResponseFromESB(path, requestJsonESB);
	sendInternetSettingsResponse.getLogsReport().setEsbResponseTime(Utilities.getReportDateTime());

	Utilities.printDebugLog(msisdn + "-Received response from ESB-" + response, logger);

	// Logging ESB response code and description.
	sendInternetSettingsResponse.setLogsReport(Utilities.logESBParamsintoReportLog(requestJsonESB, response,
		sendInternetSettingsResponse.getLogsReport()));

	if (response != null && !response.isEmpty()) {
	    if (Utilities.getValueFromJSON(response, "returnCode").equalsIgnoreCase(Constants.ESB_SUCCESS_CODE)) {

		resData.setMessage(GetMessagesMappings.getMessageFromResourceBundle("success",
			sendInternetSettingsRequest.getLang()));

		sendInternetSettingsResponse.setData(resData);
		sendInternetSettingsResponse.setCallStatus(Constants.Call_Status_True);
		sendInternetSettingsResponse.setResultCode(Constants.APP_SERVER_SUCCESS_CODE);
		sendInternetSettingsResponse.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("success",
			sendInternetSettingsRequest.getLang()));
	    } else {
		sendInternetSettingsResponse.setCallStatus(Constants.Call_Status_False);
		sendInternetSettingsResponse.setResultCode(Utilities.getValueFromJSON(response, "returnCode"));
		sendInternetSettingsResponse.setResultDesc(Utilities.getErrorMessageFromFile("send.internet.settings",
			sendInternetSettingsRequest.getLang(), Utilities.getValueFromJSON(response, "returnCode")));
	    }
	} else {
	    sendInternetSettingsResponse.setCallStatus(Constants.Call_Status_False);
	    sendInternetSettingsResponse.setResultCode(Constants.API_FAILURE_CODE);

	    sendInternetSettingsResponse.setResultDesc(GetMessagesMappings
		    .getMessageFromResourceBundle("connectivity.error", sendInternetSettingsRequest.getLang()));
	}
	return sendInternetSettingsResponse;
    }

    private List<FAQSListData> parseFAQsData(String responseArray) throws JSONException, SocketException {
	List<FAQSListData> faqsListData = new ArrayList<>();

	JSONArray jsonArray = new JSONArray(responseArray);
	Utilities.printDebugLog("FAQS JSON TO BE PARSED-" + jsonArray, logger);
	for (int i = 0; i < jsonArray.length(); i++) {
	    JSONObject obj = jsonArray.getJSONObject(i);
	    List<FAQS> listFAQs = new ArrayList<>();
	    String category = obj.getString("category");
	    FAQSListData faqListData = new FAQSListData();
	    faqListData.setTitle(category);
	    if (!isCategoryExists(category, faqsListData)) {
		Utilities.printDebugLog("FAQS CATEGORY EXISTS-" + category, logger);
		for (int j = 0; j < jsonArray.length(); j++) {
		    JSONObject obj1 = jsonArray.getJSONObject(j);
		    Utilities.printDebugLog("FAQS OBJECT TO BE ADDED-" + obj1, logger);
		    if (category.equalsIgnoreCase(obj1.getString("category"))) {
			Utilities.printDebugLog("FAQS SAME CATEGORY, ADDING OBJECT INTO CHILD LIST-" + obj1, logger);
			FAQS faqs = new FAQS();
			faqs.setQuestion(obj1.getString("question"));
			faqs.setAnswer(obj1.getString("answer"));
			faqs.setSort_order(obj1.getString("sort_order"));
			Utilities.printDebugLog("FAQS CATEGORY-" + category + "-WITH OBJECT-" + faqs.toString(),
				logger);
			listFAQs.add(faqs);
			Utilities.printDebugLog("FAQS CHILD LIST SIZE-" + listFAQs.size()
				+ "-FAQS CHILD LIST AFTER ADDED NEW OBJECT-" + Arrays.toString(listFAQs.toArray()),
				logger);
		    } else {
			Utilities.printDebugLog("FAQS IS DIFFERENT, ADDING PREVIOUS CATEGORY  INTO PARENT LIST-" + obj1,
				logger);
			faqListData.setQAList(listFAQs);
			Utilities.printDebugLog("FAQS PARENT LIST SIZE-" + faqListData.getQAList().size()
				+ "-FAQS PARENT LIST AFTER ADDED NEW OBJECT-"
				+ Arrays.toString(faqListData.getQAList().toArray()), logger);
		    }
		    Utilities.printDebugLog("FAQS PARENT LIST SIZE-" + faqListData.getQAList().size(), logger);
		    if (faqListData.getQAList() == null || faqListData.getQAList().isEmpty()) {
			Utilities.printDebugLog(
				"FAQS PARENT LIST WITH SINGLE CATEGORY-" + faqListData.getQAList().size(), logger);
			faqListData.setQAList(listFAQs);
		    }
		}
		faqsListData.add(faqListData);
	    }
	}
	return faqsListData;
    }

    private boolean isCategoryExists(String category, List<FAQSListData> faqsListData) {
	boolean flag = false;
	for (int i = 0; i < faqsListData.size(); i++) {
	    if (category.equalsIgnoreCase(faqsListData.get(i).getTitle()))
		flag = true;
	}
	return flag;
    }

    private List<TimeStamps> getTimeStamps(String msisdn) throws SocketException {
	List<TimeStamps> timeStampsList = new ArrayList<>();
	Utilities.printDebugLog(
		msisdn + "Cache Time Stamps List size before getting time stamps: " + timeStampsList.size(), logger);
	if (AppCache.getHashmapTimestamps().size() > 0) {
	    Utilities.printDebugLog(msisdn + "Cache Time Stamps has data.", logger);
	    if (AppCache.getHashmapTimestamps().containsKey(Constants.HASH_KEY_APP_MENU)) {
		timeStampsList.add(AppCache.getHashmapTimestamps().get(Constants.HASH_KEY_APP_MENU));
	    }
	    if (AppCache.getHashmapTimestamps().containsKey(Constants.HASH_KEY_CONTACT_US)) {
		timeStampsList.add(AppCache.getHashmapTimestamps().get(Constants.HASH_KEY_CONTACT_US));
	    }
	    if (AppCache.getHashmapTimestamps().containsKey(Constants.HASH_KEY_FAQs)) {
		timeStampsList.add(AppCache.getHashmapTimestamps().get(Constants.HASH_KEY_FAQs));
	    }
	    if (AppCache.getHashmapTimestamps().containsKey(Constants.HASH_KEY_STORE_LOCATOR)) {
		timeStampsList.add(AppCache.getHashmapTimestamps().get(Constants.HASH_KEY_STORE_LOCATOR));
	    }
	    if (AppCache.getHashmapTimestamps().containsKey(Constants.HASH_KEY_SUPPLEMENTARY_OFFERINGS)) {
		timeStampsList.add(AppCache.getHashmapTimestamps().get(Constants.HASH_KEY_SUPPLEMENTARY_OFFERINGS));
	    }
	    if (AppCache.getHashmapTimestamps().containsKey(Constants.HASH_KEY_TARIFFS)) {
		timeStampsList.add(AppCache.getHashmapTimestamps().get(Constants.HASH_KEY_TARIFFS));
	    }

	}

	Utilities.printDebugLog(
		msisdn + "Cache Time Stamps List size after getting time stamps: " + timeStampsList.size(), logger);
	return timeStampsList;
    }

    private String getKeyForCache(String cacheKey, String lang) {

	return cacheKey + "." + lang;
    }

    // for phase 2 accept terms and conditions

    public AcceptTnCResponse AccpetTnCBusiness(String msisdn, AcceptTnCRequest acceptTnCRequest,
	    AcceptTnCResponse acceptTnCResponse) throws JSONException, IOException, SQLException {
	Utilities.printDebugLog(msisdn + "-Request received in " + Transactions.ACCEPT_TNC_NAME + " BUSINESS with data-"
		+ acceptTnCRequest.toString(), logger);

	AcceptTnCResponseData resData = new AcceptTnCResponseData();
	RestClient rc = new RestClient();
	ObjectMapper mapper = new ObjectMapper();
	String requestJsonESB = mapper.writeValueAsString(acceptTnCRequest);
	String path = GetConfigurations.getESBRoute("acceptTnC");

	Utilities.printDebugLog(msisdn + "-Request Call to ESB", logger);
	Utilities.printDebugLog(msisdn + "-Path-" + path, logger);
	Utilities.printDebugLog(msisdn + "-Request Packet-" + requestJsonESB, logger);
	// String response = HardCodedResponses.STORE_LOCATOR;

	acceptTnCResponse.getLogsReport().setEsbRequestTime(Utilities.getReportDateTime());
	String response = rc.getResponseFromESB(path, requestJsonESB);
	acceptTnCResponse.getLogsReport().setEsbResponseTime(Utilities.getReportDateTime());

	Utilities.printDebugLog(msisdn + "-Received response from ESB-" + response, logger);

	// Logging ESB response code and description.
	acceptTnCResponse.setLogsReport(
		Utilities.logESBParamsintoReportLog(requestJsonESB, response, acceptTnCResponse.getLogsReport()));

	if (response != null && !response.isEmpty()) {
	    if (Utilities.getValueFromJSON(response, "returnCode").equalsIgnoreCase(Constants.ESB_SUCCESS_CODE)) {
		resData.setReturnMsg(Utilities.getValueFromJSON(response, "returnMsg"));
		acceptTnCResponse.setData(resData);
		acceptTnCResponse.setCallStatus(Constants.Call_Status_True);
		acceptTnCResponse.setResultCode(Constants.APP_SERVER_SUCCESS_CODE);
		acceptTnCResponse.setResultDesc(
			GetMessagesMappings.getMessageFromResourceBundle("success", acceptTnCRequest.getLang()));
	    } else {
		acceptTnCResponse.setCallStatus(Constants.Call_Status_False);
		acceptTnCResponse.setResultCode(Utilities.getValueFromJSON(response, "returnCode"));
		acceptTnCResponse.setResultDesc(Utilities.getErrorMessageFromFile("store.locator",
			acceptTnCRequest.getLang(), Utilities.getValueFromJSON(response, "returnCode")));

	    }
	} else {
	    acceptTnCResponse.setCallStatus(Constants.Call_Status_False);
	    acceptTnCResponse.setResultCode(Constants.API_FAILURE_CODE);
	    acceptTnCResponse.setResultDesc(
		    GetMessagesMappings.getMessageFromResourceBundle("connectivity.error", acceptTnCRequest.getLang()));
	}

	return acceptTnCResponse;
    }
    // START OF GET ROAMING COUNTRIES LIST B2C

    public GetRoamingResponse getRoaming(String msisdn, GetRoamingRequest request, GetRoamingResponse roamingResponse)
	    throws SQLException, JSONException, IOException {
	ObjectMapper mapper = new ObjectMapper();
	String requestJsonESB = mapper.writeValueAsString(request);

	Utilities.printDebugLog(msisdn + "-Request received in " + Transactions.ROAMING_COUNTRIES
		+ " BUSINESS with data-" + requestJsonESB, logger);

	RestClient rc = new RestClient();
	GetRoamingEsbResponse orderList = new GetRoamingEsbResponse();

	String path = GetConfigurations.getESBRoute("getroaming");

	Utilities.printDebugLog(msisdn + "-Request Call to ESB", logger);
	Utilities.printDebugLog(msisdn + "-Path-" + path, logger);
	Utilities.printDebugLog(msisdn + "-Request Packet-" + requestJsonESB, logger);
	roamingResponse.getLogsReport().setEsbRequestTime(Utilities.getReportDateTime());
	String response = rc.getResponseFromESB(path, requestJsonESB);
	roamingResponse.getLogsReport().setEsbResponseTime(Utilities.getReportDateTime());
	Utilities.printDebugLog(msisdn + "-Received response from ESB-" + response, logger);

	// Logging ESB response code and description.
	roamingResponse.setLogsReport(
		Utilities.logESBParamsintoReportLog(requestJsonESB, response, roamingResponse.getLogsReport()));
	if (response != null && !response.isEmpty()) {
	    if (Utilities.getValueFromJSON(response, "returnCode").equalsIgnoreCase(Constants.ESB_SUCCESS_CODE)) {
		String responseData = Utilities.removeParamsFromJSONObject(response, "returnMsg"); // new
												   // JSONObject(response).get("orderList").toString();
		responseData = Utilities.removeParamsFromJSONObject(responseData, "returnCode");

		logger.info("<<<<<<<< responseData >>>>>>>>" + responseData);
		try {
		    orderList = mapper.readValue(responseData, GetRoamingEsbResponse.class);
		    roamingResponse.setData(orderList.getData());
		    roamingResponse.setResultCode(Constants.APP_SERVER_SUCCESS_CODE);
		    roamingResponse.setResultDesc(
			    GetMessagesMappings.getMessageFromResourceBundle("success", request.getLang()));
		    roamingResponse.setCallStatus(Constants.Call_Status_True);
		    return roamingResponse;
		} catch (Exception e) {
		    logger.error("ERROR:", e);
		    roamingResponse.setCallStatus(Constants.Call_Status_False);
		    roamingResponse.setResultCode(Utilities.getValueFromJSON(response, "returnCode"));
		    roamingResponse.setResultDesc(Utilities.getValueFromJSON(response, "returnMsg"));
		    return roamingResponse;
		}

	    } else {
		roamingResponse.setCallStatus(Constants.Call_Status_False);
		roamingResponse.setResultCode(Utilities.getValueFromJSON(response, "returnCode"));
		roamingResponse.setResultDesc(Utilities.getValueFromJSON(response, "returnMsg"));
		return roamingResponse;
	    }
	} else {
	    roamingResponse.setCallStatus(Constants.Call_Status_False);
	    roamingResponse.setResultCode(Constants.API_FAILURE_CODE);
	    roamingResponse.setResultDesc(
		    GetMessagesMappings.getMessageFromResourceBundle("connectivity.error", request.getLang()));
	    return roamingResponse;
	}
    }

    // END OF GET ROAMING COUNTRIES LIST B2C

    public ExchangeServiceResponse exchangeServicesBusiness(String msisdn,
	    ExchangeServiceRequestClient exchangeServicerequest, ExchangeServiceResponse exchangeServiceResponse)
	    throws Exception {

	Utilities.printDebugLog(msisdn + "-Request received in " + Transactions.EXCHNAGE_SERVICE_TRANSACTION_NAME
		+ " BUSINESS with data-" + exchangeServicerequest.toString(), logger);

	ExchangeServiceData resData = new ExchangeServiceData();
	RestClient rc = new RestClient();
	ObjectMapper mapper = new ObjectMapper();

	// Get key which will be used to store and retrieve data from hash map.

	String requestJsonESB = mapper.writeValueAsString(exchangeServicerequest);

	String path = GetConfigurations.getESBRoute("exchangeService");
	Utilities.printDebugLog(msisdn + "-Request Call to ESB", logger);
	Utilities.printDebugLog(msisdn + "-Path-" + path, logger);
	Utilities.printDebugLog(msisdn + "-Request Packet-" + requestJsonESB, logger);

	exchangeServiceResponse.getLogsReport().setEsbRequestTime(Utilities.getReportDateTime());
	String response = rc.getResponseFromESB(path, requestJsonESB);
	exchangeServiceResponse.getLogsReport().setEsbResponseTime(Utilities.getReportDateTime());

	Utilities.printDebugLog(msisdn + "-Received response from ESB-" + response, logger);

	// Logging ESB response code and description.
	exchangeServiceResponse.setLogsReport(
		Utilities.logESBParamsintoReportLog(requestJsonESB, response, exchangeServiceResponse.getLogsReport()));

	if (response != null && !response.isEmpty()) {

	    if (Utilities.getValueFromJSON(response, "returnCode").equalsIgnoreCase(Constants.ESB_SUCCESS_CODE)) {

		resData.setMessage(
			GetMessagesMappings.getMessageFromResourceBundle("success", exchangeServicerequest.getLang()));
		exchangeServiceResponse.setData(resData);
		exchangeServiceResponse.setCallStatus(Constants.Call_Status_True);
		exchangeServiceResponse.setResultCode(Constants.APP_SERVER_SUCCESS_CODE);
		exchangeServiceResponse.setResultDesc(
			GetMessagesMappings.getMessageFromResourceBundle("success", exchangeServicerequest.getLang()));

	    } else {

		exchangeServiceResponse.setCallStatus(Constants.Call_Status_False);
		exchangeServiceResponse.setResultCode(Utilities.getValueFromJSON(response, "returnCode"));
		exchangeServiceResponse.setResultDesc(Utilities.getErrorMessageFromFile("exchange.service",
			exchangeServicerequest.getLang(), Utilities.getValueFromJSON(response, "returnCode")));
	    }

	} else {
	    exchangeServiceResponse.setCallStatus(Constants.Call_Status_False);
	    exchangeServiceResponse.setResultCode(Constants.API_FAILURE_CODE);

	    exchangeServiceResponse.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("connectivity.error",
		    exchangeServicerequest.getLang()));
	}

	return exchangeServiceResponse;

    }

    public ExchangeServicesValuesResponse exchangeServicesValuesBusiness(String msisdn,
	    ExchangeServiceValuesRequest exchangeServicerequest, ExchangeServicesValuesResponse exchangeServiceResponse)
	    throws Exception {

	Utilities.printDebugLog(msisdn + "-Request received in " + Transactions.EXCHNAGE_SERVICE_TRANSACTION_NAME
		+ " BUSINESS with data-" + exchangeServicerequest.toString(), logger);

	ExchangeServicesValuesData resData = new ExchangeServicesValuesData();
	RestClient rc = new RestClient();
	ObjectMapper mapper = new ObjectMapper();

	// Get key which will be used to store and retrieve data from hash map.

	String requestJsonESB = mapper.writeValueAsString(exchangeServicerequest);

	String path = GetConfigurations.getESBRoute("exchangeServiceValues");
	Utilities.printDebugLog(msisdn + "-Request Call to ESB", logger);
	Utilities.printDebugLog(msisdn + "-Path-" + path, logger);
	Utilities.printDebugLog(msisdn + "-Request Packet-" + requestJsonESB, logger);

	exchangeServiceResponse.getLogsReport().setEsbRequestTime(Utilities.getReportDateTime());
	String response = rc.getResponseFromESB(path, requestJsonESB);
	exchangeServiceResponse.getLogsReport().setEsbResponseTime(Utilities.getReportDateTime());

	Utilities.printDebugLog(msisdn + "-Received response from ESB-" + response, logger);

	// Logging ESB response code and description.
	exchangeServiceResponse.setLogsReport(
		Utilities.logESBParamsintoReportLog(requestJsonESB, response, exchangeServiceResponse.getLogsReport()));

	if (response != null && !response.isEmpty()) {

	    if (Utilities.getValueFromJSON(response, "returnCode").equalsIgnoreCase(Constants.ESB_SUCCESS_CODE)) {

		ExchangeServicesValuesEsbResponse resDataEsb = mapper.readValue(response,
			ExchangeServicesValuesEsbResponse.class);
		ExchangeServicesValuesData data = new ExchangeServicesValuesData();
		data.setCount(resDataEsb.getCount());
		data.setIsServiceEnabled(resDataEsb.getIsServiceEnabled());
		data.setDatavalues(resDataEsb.getDatavalues());
		data.setVoicevalues(resDataEsb.getVoicevalues());
		exchangeServiceResponse.setData(data);
		exchangeServiceResponse.setCallStatus(Constants.Call_Status_True);
		exchangeServiceResponse.setResultCode(Constants.APP_SERVER_SUCCESS_CODE);
		exchangeServiceResponse.setResultDesc(
			GetMessagesMappings.getMessageFromResourceBundle("success", exchangeServicerequest.getLang()));

	    } else {

		exchangeServiceResponse.setCallStatus(Constants.Call_Status_False);
		exchangeServiceResponse.setResultCode(Utilities.getValueFromJSON(response, "returnCode"));
		exchangeServiceResponse.setResultDesc(Utilities.getErrorMessageFromFile("exchange.service",
			exchangeServicerequest.getLang(), Utilities.getValueFromJSON(response, "returnCode")));
	    }

	} else {
	    exchangeServiceResponse.setCallStatus(Constants.Call_Status_False);
	    exchangeServiceResponse.setResultCode(Constants.API_FAILURE_CODE);

	    exchangeServiceResponse.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("connectivity.error",
		    exchangeServicerequest.getLang()));
	}

	return exchangeServiceResponse;

    }

    public CompanyInvoiceResponse getCompanySummary(String msisdn, CompanyInvoiceRequest companyInvoiceRequest,
	    CompanyInvoiceResponse companyInvoiceResponse)
	    throws SocketException, SQLException, JsonProcessingException, JSONException {
	Utilities.printDebugLog(msisdn + "-Request received in " + Transactions.COMPANY_INVOICE + " BUSINESS with data-"
		+ companyInvoiceRequest.toString(), logger);
	ObjectMapper mapper = new ObjectMapper();
	String requestJsonESB = mapper.writeValueAsString(companyInvoiceRequest);
	RestClient rc = new RestClient();

	String path = GetConfigurations.getESBRoute("companyInvoice");

	Utilities.printDebugLog(msisdn + "-Request Call to ESB", logger);
	Utilities.printDebugLog(msisdn + "-Path-" + path, logger);
	Utilities.printDebugLog(msisdn + "-Request Packet-" + requestJsonESB, logger);
	companyInvoiceResponse.getLogsReport().setEsbRequestTime(Utilities.getReportDateTime());
	String response = rc.getResponseFromESB(path, requestJsonESB);
	companyInvoiceResponse.getLogsReport().setEsbResponseTime(Utilities.getReportDateTime());
	Utilities.printDebugLog(msisdn + "-Received response from ESB-" + response, logger);
	if (response != null && !response.isEmpty()) {
	    if (Utilities.getValueFromJSON(response, "returnCode").equalsIgnoreCase(Constants.ESB_SUCCESS_CODE)) {
		String responseData = Utilities.removeParamsFromJSONObject(response, "returnMsg"); // new
												   // JSONObject(response).get("orderList").toString();
		responseData = Utilities.removeParamsFromJSONObject(responseData, "returnCode");

		logger.info("<<<<<<<< responseData >>>>>>>>" + responseData);
		try {

		    // CompanyInvoiceResponse companyInvoiceResponseData = new
		    // CompanyInvoiceResponse();

		    companyInvoiceResponse = mapper.readValue(responseData, CompanyInvoiceResponse.class);

		    // if there is some json object then uncomment following code
		    // if (jsonObject.get(key) instanceof JSONObject)
		    // {
		    // // do something with jsonObject here
		    //
		    // }

		    companyInvoiceResponse.setResultCode(Constants.APP_SERVER_SUCCESS_CODE);
		    companyInvoiceResponse.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("success",
			    companyInvoiceRequest.getLang()));
		    companyInvoiceResponse.setCallStatus(Constants.Call_Status_True);
		    logger.debug(companyInvoiceResponse);
		    return companyInvoiceResponse;
		} catch (Exception e) {
		    logger.error("ERROR:", e);
		    companyInvoiceResponse.setCallStatus(Constants.Call_Status_False);
		    companyInvoiceResponse.setResultCode(Utilities.getValueFromJSON(response, "returnCode"));
		    companyInvoiceResponse.setResultDesc(Utilities.getValueFromJSON(response, "returnMsg"));
		    return companyInvoiceResponse;
		}

	    } else {
		companyInvoiceResponse.setCallStatus(Constants.Call_Status_False);
		companyInvoiceResponse.setResultCode(Utilities.getValueFromJSON(response, "returnCode"));
		companyInvoiceResponse.setResultDesc(Utilities.getValueFromJSON(response, "returnMsg"));
		return companyInvoiceResponse;
	    }
	} else {
	    companyInvoiceResponse.setCallStatus(Constants.Call_Status_False);
	    companyInvoiceResponse.setResultCode(Constants.API_FAILURE_CODE);
	    companyInvoiceResponse.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("connectivity.error",
		    companyInvoiceRequest.getLang()));
	    return companyInvoiceResponse;
	}
    }

    public MSISDNSummaryResponse getInvoiceSummary(String msisdn, MSISDNInvoiceRequest msisdnInvoiceRequest,
	    MSISDNSummaryResponse msisdnSummaryResponse)
	    throws SocketException, SQLException, JsonProcessingException, JSONException {
	Utilities.printDebugLog(msisdn + "-Request received in " + Transactions.MSISDN_INVOICE_SUMMARY
		+ " BUSINESS with data-" + msisdnInvoiceRequest.toString(), logger);
	ObjectMapper mapper = new ObjectMapper();
	String requestJsonESB = mapper.writeValueAsString(msisdnInvoiceRequest);
	RestClient rc = new RestClient();

	String path = GetConfigurations.getESBRoute("InvoiceSummary");

	Utilities.printDebugLog(msisdn + "-Request Call to ESB", logger);
	Utilities.printDebugLog(msisdn + "-Path-" + path, logger);
	Utilities.printDebugLog(msisdn + "-Request Packet-" + requestJsonESB, logger);
	msisdnSummaryResponse.getLogsReport().setEsbRequestTime(Utilities.getReportDateTime());
	String response = rc.getResponseFromESB(path, requestJsonESB);
	msisdnSummaryResponse.getLogsReport().setEsbResponseTime(Utilities.getReportDateTime());
	Utilities.printDebugLog(msisdn + "-Received response from ESB-" + response, logger);
	if (response != null && !response.isEmpty()) {
	    if (Utilities.getValueFromJSON(response, "returnCode").equalsIgnoreCase(Constants.ESB_SUCCESS_CODE)) {
		String responseData = Utilities.removeParamsFromJSONObject(response, "returnMsg"); // new
												   // JSONObject(response).get("orderList").toString();
		responseData = Utilities.removeParamsFromJSONObject(responseData, "returnCode");

		logger.info("<<<<<<<< responseData >>>>>>>>" + responseData);
		try {

		    // CompanyInvoiceResponse companyInvoiceResponseData = new
		    // CompanyInvoiceResponse();

		    msisdnSummaryResponse = mapper.readValue(responseData, MSISDNSummaryResponse.class);

		    // if there is some json object then uncomment following code
		    // if (jsonObject.get(key) instanceof JSONObject)
		    // {
		    // // do something with jsonObject here
		    //
		    // }

		    msisdnSummaryResponse.setCallStatus(Constants.Call_Status_True);
		    msisdnSummaryResponse.setResultCode(Constants.APP_SERVER_SUCCESS_CODE);
		    msisdnSummaryResponse.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("success",
			    msisdnInvoiceRequest.getLang()));
		    logger.debug(msisdnSummaryResponse);
		    return msisdnSummaryResponse;
		} catch (Exception e) {
		    logger.error("ERROR:", e);
		    msisdnSummaryResponse.setCallStatus(Constants.Call_Status_False);
		    msisdnSummaryResponse.setResultCode(Utilities.getValueFromJSON(response, "returnCode"));
		    msisdnSummaryResponse.setResultDesc(Utilities.getValueFromJSON(response, "returnMsg"));
		    return msisdnSummaryResponse;
		}

	    } else {
		msisdnSummaryResponse.setCallStatus(Constants.Call_Status_False);
		msisdnSummaryResponse.setResultCode(Utilities.getValueFromJSON(response, "returnCode"));
		msisdnSummaryResponse.setResultDesc(Utilities.getValueFromJSON(response, "returnMsg"));
		return msisdnSummaryResponse;
	    }
	} else {
	    msisdnSummaryResponse.setCallStatus(Constants.Call_Status_False);
	    msisdnSummaryResponse.setResultCode(Constants.API_FAILURE_CODE);
	    msisdnSummaryResponse.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("connectivity.error",
		    msisdnInvoiceRequest.getLang()));
	    return msisdnSummaryResponse;
	}
    }

    public MSISDNDetailsResponse getInvoiceDetails(String msisdn, MSISDNInvoiceRequest msisdnInvoiceRequest,
	    MSISDNDetailsResponse msisdnDetailsResponse)
	    throws SocketException, SQLException, JsonProcessingException, JSONException {
	Utilities.printDebugLog(msisdn + "-Request received in " + Transactions.MSISDN_INVOICE_DETAILS
		+ " BUSINESS with data-" + msisdnInvoiceRequest.toString(), logger);
	ObjectMapper mapper = new ObjectMapper();
	String requestJsonESB = mapper.writeValueAsString(msisdnInvoiceRequest);
	RestClient rc = new RestClient();

	String path = GetConfigurations.getESBRoute("InvoiceDetails");

	Utilities.printDebugLog(msisdn + "-Request Call to ESB", logger);
	Utilities.printDebugLog(msisdn + "-Path-" + path, logger);
	Utilities.printDebugLog(msisdn + "-Request Packet-" + requestJsonESB, logger);
	msisdnDetailsResponse.getLogsReport().setEsbRequestTime(Utilities.getReportDateTime());
	String response = rc.getResponseFromESB(path, requestJsonESB);
	msisdnDetailsResponse.getLogsReport().setEsbResponseTime(Utilities.getReportDateTime());
	Utilities.printDebugLog(msisdn + "-Received response from ESB-" + response, logger);
	if (response != null && !response.isEmpty()) {
	    if (Utilities.getValueFromJSON(response, "returnCode").equalsIgnoreCase(Constants.ESB_SUCCESS_CODE)) {
		String responseData = Utilities.removeParamsFromJSONObject(response, "returnMsg"); // new
												   // JSONObject(response).get("orderList").toString();
		responseData = Utilities.removeParamsFromJSONObject(responseData, "returnCode");

		logger.info("<<<<<<<< responseData >>>>>>>>" + responseData);
		try {

		    // CompanyInvoiceResponse companyInvoiceResponseData = new
		    // CompanyInvoiceResponse();

		    msisdnDetailsResponse = mapper.readValue(responseData, MSISDNDetailsResponse.class);

		    // if there is some json object then uncomment following code
		    // if (jsonObject.get(key) instanceof JSONObject)
		    // {
		    // // do something with jsonObject here
		    //
		    // }

		    msisdnDetailsResponse.setCallStatus(Constants.Call_Status_True);
		    msisdnDetailsResponse.setResultCode(Constants.APP_SERVER_SUCCESS_CODE);
		    msisdnDetailsResponse.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("success",
			    msisdnInvoiceRequest.getLang()));
		    logger.info(msisdnDetailsResponse);
		    return msisdnDetailsResponse;
		} catch (Exception e) {
		    logger.error("ERROR:", e);
		    msisdnDetailsResponse.setCallStatus(Constants.Call_Status_False);
		    msisdnDetailsResponse.setResultCode(Utilities.getValueFromJSON(response, "returnCode"));
		    msisdnDetailsResponse.setResultDesc(Utilities.getValueFromJSON(response, "returnMsg"));
		    return msisdnDetailsResponse;
		}

	    } else {
		msisdnDetailsResponse.setCallStatus(Constants.Call_Status_False);
		msisdnDetailsResponse.setResultCode(Utilities.getValueFromJSON(response, "returnCode"));
		msisdnDetailsResponse.setResultDesc(Utilities.getValueFromJSON(response, "returnMsg"));
		return msisdnDetailsResponse;
	    }
	} else {
	    msisdnDetailsResponse.setCallStatus(Constants.Call_Status_False);
	    msisdnDetailsResponse.setResultCode(Constants.API_FAILURE_CODE);
	    msisdnDetailsResponse.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("connectivity.error",
		    msisdnInvoiceRequest.getLang()));
	    return msisdnDetailsResponse;
	}
    }

    public SimSwapResponse simswapVerify(String msisdn, SimSwapRequest simSwapRequest, SimSwapResponse simSwapResponse)
	    throws Exception {

	Utilities.printDebugLog(msisdn + "-Request received in " + Transactions.SIMSWAP_TRANSACTION_NAME
		+ " BUSINESS with data-" + simSwapRequest.toString(), logger);
	ObjectMapper mapper = new ObjectMapper();
	String requestJsonESB = mapper.writeValueAsString(simSwapRequest);
	RestClient rc = new RestClient();

	String path = GetConfigurations.getESBRoute("getsimswapverify");

	Utilities.printDebugLog(msisdn + "-Request Call to ESB", logger);
	Utilities.printDebugLog(msisdn + "-Path-" + path, logger);
	Utilities.printDebugLog(msisdn + "-Request Packet-" + requestJsonESB, logger);
	simSwapResponse.getLogsReport().setEsbRequestTime(Utilities.getReportDateTime());
	String response = rc.getResponseFromESB(path, requestJsonESB);
	simSwapResponse.getLogsReport().setEsbResponseTime(Utilities.getReportDateTime());
	Utilities.printDebugLog(msisdn + "-Received response from ESB-" + response, logger);

	if (response != null && !response.isEmpty()) {
	    if (Utilities.getValueFromJSON(response, "returnCode").equalsIgnoreCase(Constants.ESB_SUCCESS_CODE)) {
		String responseData = Utilities.removeParamsFromJSONObject(response, "returnMsg"); // new
												   // JSONObject(response).get("orderList").toString();
		responseData = Utilities.removeParamsFromJSONObject(responseData, "returnCode");

		logger.info("<<<<<<<< responseData >>>>>>>>" + responseData);
		try {
		    SimSwapResponseData simSwapResponseData = new SimSwapResponseData();
		    simSwapResponseData.setMessage(
			    GetMessagesMappings.getMessageFromResourceBundle("success", simSwapRequest.getLang()));
		    simSwapResponseData.setTransactionId(Utilities.getValueFromJSON(response, "transactionId"));
		    simSwapResponse.setData(simSwapResponseData);
		    simSwapResponse.setResultCode(Constants.APP_SERVER_SUCCESS_CODE);
		    simSwapResponse.setResultDesc(
			    GetMessagesMappings.getMessageFromResourceBundle("success", simSwapRequest.getLang()));
		    simSwapResponse.setCallStatus(Constants.Call_Status_True);
		    return simSwapResponse;
		} catch (Exception e) {
		    logger.error("ERROR:", e);
		    simSwapResponse.setCallStatus(Constants.Call_Status_False);
		    simSwapResponse.setResultCode(Utilities.getValueFromJSON(response, "returnCode"));
		    simSwapResponse.setResultDesc(Utilities.getValueFromJSON(response, "returnMsg"));
		    return simSwapResponse;
		}

	    } else {
		simSwapResponse.setCallStatus(Constants.Call_Status_False);
		simSwapResponse.setResultCode(Utilities.getValueFromJSON(response, "returnCode"));
		simSwapResponse.setResultDesc(Utilities.getValueFromJSON(response, "returnMsg"));
		return simSwapResponse;
	    }
	} else {
	    simSwapResponse.setCallStatus(Constants.Call_Status_False);
	    simSwapResponse.setResultCode(Constants.API_FAILURE_CODE);
	    simSwapResponse.setResultDesc(
		    GetMessagesMappings.getMessageFromResourceBundle("connectivity.error", simSwapRequest.getLang()));
	    return simSwapResponse;
	}

    }

    public SimSwapResponse simswapAddApplication(String msisdn, SimSwapRequest simSwapRequest,
	    SimSwapResponse simSwapResponse) throws Exception {

	Utilities.printDebugLog(msisdn + "-Request received in " + Transactions.SIMSWAP_TRANSACTION_NAME
		+ " BUSINESS with data-" + simSwapRequest.toString(), logger);
	ObjectMapper mapper = new ObjectMapper();
	String requestJsonESB = mapper.writeValueAsString(simSwapRequest);
	RestClient rc = new RestClient();

	String path = GetConfigurations.getESBRoute("getsimswapaddapplication");

	Utilities.printDebugLog(msisdn + "-Request Call to ESB", logger);
	Utilities.printDebugLog(msisdn + "-Path-" + path, logger);
	Utilities.printDebugLog(msisdn + "-Request Packet-" + requestJsonESB, logger);
	simSwapResponse.getLogsReport().setEsbRequestTime(Utilities.getReportDateTime());
	String response = rc.getResponseFromESB(path, requestJsonESB);
	simSwapResponse.getLogsReport().setEsbResponseTime(Utilities.getReportDateTime());
	Utilities.printDebugLog(msisdn + "-Received response from ESB-" + response, logger);
	if (response != null && !response.isEmpty()) {
	    if (Utilities.getValueFromJSON(response, "returnCode").equalsIgnoreCase(Constants.ESB_SUCCESS_CODE)) {
		String responseData = Utilities.removeParamsFromJSONObject(response, "returnMsg"); // new
												   // JSONObject(response).get("orderList").toString();
		responseData = Utilities.removeParamsFromJSONObject(responseData, "returnCode");

		logger.info("<<<<<<<< responseData >>>>>>>>" + responseData);
		try {
		    SimSwapResponseData simSwapResponseData = new SimSwapResponseData();
		    simSwapResponseData.setMessage(
			    GetMessagesMappings.getMessageFromResourceBundle("success", simSwapRequest.getLang()));
		    simSwapResponse.setData(simSwapResponseData);
		    simSwapResponse.setCallStatus(Constants.Call_Status_True);
		    simSwapResponse.setResultCode(Constants.APP_SERVER_SUCCESS_CODE);
		    simSwapResponse.setResultDesc(
			    GetMessagesMappings.getMessageFromResourceBundle("success", simSwapRequest.getLang()));
		    return simSwapResponse;
		} catch (Exception e) {
		    logger.error("ERROR:", e);
		    simSwapResponse.setCallStatus(Constants.Call_Status_False);
		    simSwapResponse.setResultCode(Utilities.getValueFromJSON(response, "returnCode"));
		    simSwapResponse.setResultDesc(Utilities.getValueFromJSON(response, "returnMsg"));
		    return simSwapResponse;
		}

	    } else {
		simSwapResponse.setCallStatus(Constants.Call_Status_False);
		simSwapResponse.setResultCode(Utilities.getValueFromJSON(response, "returnCode"));
		simSwapResponse.setResultDesc(Utilities.getValueFromJSON(response, "returnMsg"));
		return simSwapResponse;
	    }
	} else {
	    simSwapResponse.setCallStatus(Constants.Call_Status_False);
	    simSwapResponse.setResultCode(Constants.API_FAILURE_CODE);
	    simSwapResponse.setResultDesc(
		    GetMessagesMappings.getMessageFromResourceBundle("connectivity.error", simSwapRequest.getLang()));
	    return simSwapResponse;
	}
    }

    public SimSwapResponse simswapDocumentValid(String msisdn, SimSwapRequest simSwapRequest,
	    SimSwapResponse simSwapResponse) throws Exception {

	Utilities.printDebugLog(msisdn + "-Request received in " + Transactions.SIMSWAP_TRANSACTION_NAME
		+ " BUSINESS with data-" + simSwapRequest.toString(), logger);
	ObjectMapper mapper = new ObjectMapper();
	String requestJsonESB = mapper.writeValueAsString(simSwapRequest);
	RestClient rc = new RestClient();

	String path = GetConfigurations.getESBRoute("getsimswapdocumentvalid");

	Utilities.printDebugLog(msisdn + "-Request Call to ESB", logger);
	Utilities.printDebugLog(msisdn + "-Path-" + path, logger);
	Utilities.printDebugLog(msisdn + "-Request Packet-" + requestJsonESB, logger);
	simSwapResponse.getLogsReport().setEsbRequestTime(Utilities.getReportDateTime());
	String response = rc.getResponseFromESB(path, requestJsonESB);
	simSwapResponse.getLogsReport().setEsbResponseTime(Utilities.getReportDateTime());
	Utilities.printDebugLog(msisdn + "-Received response from ESB-" + response, logger);
	if (response != null && !response.isEmpty()) {
	    if (Utilities.getValueFromJSON(response, "returnCode").equalsIgnoreCase(Constants.ESB_SUCCESS_CODE)) {
		// String responseData = Utilities.removeParamsFromJSONObject(response,
		// "returnMsg"); //new JSONObject(response).get("orderList").toString();
		// responseData = Utilities.removeParamsFromJSONObject(responseData,
		// "returnCode");

		// logger.info("<<<<<<<< responseData >>>>>>>>" + responseData);
		try {
		    SimSwapResponseData simSwapResponseData = new SimSwapResponseData();
		    simSwapResponseData.setMessage(Utilities.getValueFromJSON(response, "returnMsg"));
		    simSwapResponseData.setSpecialGroup(Utilities.getValueFromJSON(response, "specialGroup"));
		    simSwapResponse.setData(simSwapResponseData);
		    simSwapResponse.setCallStatus(Constants.Call_Status_True);
		    simSwapResponse.setResultCode(Constants.APP_SERVER_SUCCESS_CODE);
		    simSwapResponse.setResultDesc(Utilities.getValueFromJSON(response, "returnMsg"));
		    return simSwapResponse;
		} catch (Exception e) {
		    logger.error("ERROR:", e);
		    simSwapResponse.setCallStatus(Constants.Call_Status_False);
		    simSwapResponse.setResultCode(Utilities.getValueFromJSON(response, "returnCode"));
		    simSwapResponse.setResultDesc(Utilities.getValueFromJSON(response, "returnMsg"));
		    return simSwapResponse;
		}

	    } else {
		simSwapResponse.setCallStatus(Constants.Call_Status_False);
		simSwapResponse.setResultCode(Utilities.getValueFromJSON(response, "returnCode"));
		simSwapResponse.setResultDesc(Utilities.getValueFromJSON(response, "returnMsg"));
		return simSwapResponse;
	    }
	} else {
	    simSwapResponse.setCallStatus(Constants.Call_Status_False);
	    simSwapResponse.setResultCode(Constants.API_FAILURE_CODE);
	    simSwapResponse.setResultDesc(
		    GetMessagesMappings.getMessageFromResourceBundle("connectivity.error", simSwapRequest.getLang()));
	    return simSwapResponse;
	}
    }

}
