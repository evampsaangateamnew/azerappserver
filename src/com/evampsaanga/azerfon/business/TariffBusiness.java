/**
 * 
 */
package com.evampsaanga.azerfon.business;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.evampsaanga.azerfon.common.messagesmappings.GetMessagesMappings;
import com.evampsaanga.azerfon.common.utilities.AppCache;
import com.evampsaanga.azerfon.common.utilities.Constants;
import com.evampsaanga.azerfon.common.utilities.GetConfigurations;
import com.evampsaanga.azerfon.common.utilities.MigrationPrices;
import com.evampsaanga.azerfon.common.utilities.Transactions;
import com.evampsaanga.azerfon.common.utilities.Utilities;
import com.evampsaanga.azerfon.models.dashboardservice.usersgroup.UserGroupData;
import com.evampsaanga.azerfon.models.dashboardservice.usersgroup.UserGroupRequest;
import com.evampsaanga.azerfon.models.dashboardservice.usersgroup.UserGroupResponseBase;
import com.evampsaanga.azerfon.models.dashboardservice.usersgroup.UsersGroupData;
import com.evampsaanga.azerfon.models.dashboardservice.usersgroup.UsersGroupResponse;
import com.evampsaanga.azerfon.models.dashboardservice.usersgroup.UsersGroupResponseData;
import com.evampsaanga.azerfon.models.tariffdetails.Items;
import com.evampsaanga.azerfon.models.tariffdetails.ParseTariffsResponse;
import com.evampsaanga.azerfon.models.tariffdetails.ParseTariffsResponseV2;
import com.evampsaanga.azerfon.models.tariffdetails.TariffRequest;
import com.evampsaanga.azerfon.models.tariffdetails.TariffRequestV2;
import com.evampsaanga.azerfon.models.tariffdetails.TariffResponse;
import com.evampsaanga.azerfon.models.tariffdetails.TariffResponseData;
import com.evampsaanga.azerfon.models.tariffdetails.TariffResponseDataV2;
import com.evampsaanga.azerfon.models.tariffdetails.TariffResponseList;
import com.evampsaanga.azerfon.models.tariffdetails.TariffResponseV2;
import com.evampsaanga.azerfon.models.tariffdetails.changetariff.ChangeTariffRequest;
import com.evampsaanga.azerfon.models.tariffdetails.changetariff.ChangeTariffRequestBulk;
import com.evampsaanga.azerfon.models.tariffdetails.changetariff.ChangeTariffResponse;
import com.evampsaanga.azerfon.models.tariffdetails.changetariff.ChangeTariffResponseBulk;
import com.evampsaanga.azerfon.models.tariffdetails.changetariff.ChangeTariffResponseData;
import com.evampsaanga.azerfon.models.tariffdetails.cug.CloseUserGroupData;
import com.evampsaanga.azerfon.models.tariffdetails.cug.CloseUserGroupRequest;
import com.evampsaanga.azerfon.models.tariffdetails.cug.CloseUserGroupResponseBase;
import com.evampsaanga.azerfon.models.tariffdetails.cug.CloseUsersGroupData;
import com.evampsaanga.azerfon.models.tariffdetails.cug.CloseUsersGroupResponse;
import com.evampsaanga.azerfon.models.tariffdetails.cug.CloseUsersGroupResponseData;
import com.evampsaanga.azerfon.restclient.RestClient;

/**
 * @author Evamp & Saanga
 *
 */
public class TariffBusiness {
	Logger logger = Logger.getLogger(TariffBusiness.class);

	public com.evampsaanga.azerfon.models.tariffdetails.TariffResponse getTariffDetailsBusiness(String msisdn,
			TariffRequest tariffRequest, com.evampsaanga.azerfon.models.tariffdetails.TariffResponse tariffResponse)
			throws Exception {

		Utilities.printDebugLog(msisdn + "-Request received in " + Transactions.TARRIF_DETAILS_TRANSACTION_NAME
				+ " BUSINESS with data-" + tariffRequest.toString(), logger);

		// Get key which will be used to store and retrieve data from hash map.
		String tariffsCacheKey = this.getKeyForCache(Constants.HASH_KEY_TARIFFS, tariffRequest);

		if (AppCache.getHashmapTariffs().containsKey(tariffsCacheKey)) {
			Utilities.printDebugLog(msisdn + "-TARIFFS" + Constants.CACHE_EXISTS_DESCRIPTION + "" + tariffsCacheKey,
					logger);
			tariffResponse = AppCache.getHashmapTariffs().get(tariffsCacheKey);
			tariffResponse.getLogsReport().setIsCached("true");
			// return tariffResponse;

		} else {
			Utilities.printDebugLog(
					msisdn + "-TARIFFS" + Constants.CACHE_DOES_NOT_EXIST_DESCRIPTION + "" + tariffsCacheKey, logger);

			com.evampsaanga.azerfon.models.tariffdetails.TariffResponseList resData = new com.evampsaanga.azerfon.models.tariffdetails.TariffResponseList();
			RestClient rc = new RestClient();
			ObjectMapper mapper = new ObjectMapper();

			String requestJsonESB = mapper.writeValueAsString(tariffRequest);

			String path = GetConfigurations.getESBRoute("gettariffDetials");
			Utilities.printDebugLog(msisdn + "-Request Call to ESB", logger);
			Utilities.printDebugLog(msisdn + "-Path-" + path, logger);
			Utilities.printDebugLog(msisdn + "-Request Packet-" + requestJsonESB, logger);

			tariffResponse.getLogsReport().setEsbRequestTime(Utilities.getReportDateTime());
			String response = rc.getResponseFromESB(path, requestJsonESB);
			tariffResponse.getLogsReport().setEsbResponseTime(Utilities.getReportDateTime());

			Utilities.printDebugLog(msisdn + "-Received response from ESB-" + response, logger);

			if (response != null && !response.isEmpty()) {

				// Converting String to JSON as String.
				// response = Utilities.stringToJSONString(response);

				// Logging ESB response code and description.
				tariffResponse.setLogsReport(
						Utilities.logESBParamsintoReportLog(requestJsonESB, response, tariffResponse.getLogsReport()));

				if (Utilities.getValueFromJSON(response, "returnCode").equalsIgnoreCase(Constants.ESB_SUCCESS_CODE)) {

					/*
					 * if (AppCache.getHashmapMigrationPricesMessages() == null ||
					 * AppCache.getHashmapMigrationPricesMessages().isEmpty()) {
					 * Utilities.printDebugLog(msisdn + "-MigrationCache null so updating From DB",
					 * logger); HashMap<String, MigrationPrices> buildMigrationPricesCache =
					 * GetConfigurations .getMigrationPrices(msisdn);
					 * 
					 * }
					 */
					// Starting logic for Special Tariffs
					JSONObject jsonObject = new JSONObject(response);
					Utilities.printInfoLog(
							msisdn + "TestingTariffResponseData :" + jsonObject.getJSONArray("data").toString(),
							logger);

					// Starting logic for Special Tariffs
					resData = ParseTariffsResponse.parseTariffResponseData(msisdn,
							Utilities.getValueFromJSON(response, "data"), resData);
					Utilities.printInfoLog(msisdn + "-ResponseOfData-" + Utilities.getValueFromJSON(response, "data"),
							logger);

					tariffResponse.setData(resData);
					AppCache.getHashmapTariffs().put(tariffsCacheKey, tariffResponse);
					AppCache.getHashmapTimestamps().put(Constants.HASH_KEY_TARIFFS,
							Utilities.getTimeStampForCache(Constants.HASH_KEY_TARIFFS));

				} else {
					tariffResponse.setCallStatus(Constants.Call_Status_False);
					tariffResponse.setResultCode(Utilities.getValueFromJSON(response, "returnCode"));
					tariffResponse.setResultDesc(Utilities.getErrorMessageFromFile("get.tariffs",
							tariffRequest.getLang(), Utilities.getValueFromJSON(response, "returnCode")));
					return tariffResponse;
				}
			} else {

				tariffResponse.setCallStatus(Constants.Call_Status_False);
				tariffResponse.setResultCode(Constants.API_FAILURE_CODE);

				tariffResponse.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("connectivity.error",
						tariffRequest.getLang()));
				return tariffResponse;

			}

		}
		TariffResponse tariffResponseClone = null;
		tariffResponseClone = (TariffResponse) tariffResponse.clone();
		com.evampsaanga.azerfon.models.tariffdetails.TariffResponseList resDatafinal = new com.evampsaanga.azerfon.models.tariffdetails.TariffResponseList();
		// TariffResponseData tariffReeponseList=new TariffResponseData();
		/* tariffReeponseList= resData.getTariffResponseList(); */
		// tariffReeponseList.setTariffResponseList(tariffResponseList);
		/* TariffResponseData tariffResponseData=new TariffResponseData(); */
		List<TariffResponseData> tariiffResponseList = new ArrayList<TariffResponseData>();

		ObjectMapper mapper = new ObjectMapper();
		Utilities.printInfoLog(msisdn + "-resData-" + mapper.writeValueAsString(tariffResponseClone.getData()), logger);
		Utilities.printInfoLog(msisdn + "-resData specialIDs -" + tariffRequest.getSpecialTariffIds(), logger);

		ArrayList<String> tariffIds = tariffRequest.getSpecialTariffIds();
		for (int i = 0; i < tariffResponseClone.getData().getTariffResponseList().size(); i++) {

			TariffResponseData tariffReeponseObj = new TariffResponseData();
			List<Items> itemsTariff = new ArrayList<Items>();

			List<Items> itemsTariffSpecials = new ArrayList<Items>();
			TariffResponseData tariffReeponseSpecialObj = new TariffResponseData();

			for (int j = 0; j < tariffResponseClone.getData().getTariffResponseList().get(i).getItems().size(); j++) {

				// tariffReeponseSpecialObj.setGroupType("Special");

				Items itemsspecials = new Items();
				Items items = new Items();
				tariffReeponseSpecialObj.setIsSpecial("true");
				tariffReeponseSpecialObj.setGroupType(
						Utilities.getErrorMessageFromFile("get.tariffs.special", tariffRequest.getLang(), "120"));
				String ofeeringID = tariffResponseClone.getData().getTariffResponseList().get(i).getItems().get(j)
						.getHeader().getOfferingId();
				if (tariffIds.contains(ofeeringID)) {
					/*
					 * if (AppCache.getHashmapMigrationPricesMessages().containsKey(resData.
					 * getTariffResponseList().get(i).getItems().get(j).getHeader().getOfferingId())
					 * ) { MigrationPrices valuesFromCache =
					 * AppCache.getHashmapMigrationPricesMessages()
					 * .get(resData.getTariffResponseList().get(i).getItems().get(j).getHeader().
					 * getOfferingId());
					 * resData.getTariffResponseList().get(i).getItems().get(j).getHeader().
					 * setPriceValue(valuesFromCache.getMrc()); }
					 */
					String mrcFinal = GetConfigurations.getMigrationPrices(msisdn, "Tariff",
							tariffRequest.getGroupIds(), tariffResponseClone.getData().getTariffResponseList().get(i)
									.getItems().get(j).getHeader().getOfferingId());
					Utilities.printDebugLog(msisdn + "READING Migration Prices FOR KEY FROM DB- MRC FINAL" + mrcFinal,
							logger);
					if (mrcFinal != null && !mrcFinal.isEmpty()) {
						tariffResponseClone.getData().getTariffResponseList().get(i).getItems().get(j).getHeader()
								.setPriceValue(mrcFinal);
					} else {
						tariffResponseClone.getData().getTariffResponseList().get(i).getItems().get(j).getHeader()
								.setPriceValue("");
					}
					Utilities.printInfoLog(
							msisdn + "resData tariffIDCHECKSpecials :" + tariffResponseClone.getData()
									.getTariffResponseList().get(i).getItems().get(j).getHeader().getOfferingId(),
							logger);
					itemsspecials.setDetails(tariffResponseClone.getData().getTariffResponseList().get(i).getItems()
							.get(j).getDetails());
					itemsspecials.setHeader(
							tariffResponseClone.getData().getTariffResponseList().get(i).getItems().get(j).getHeader());
					itemsspecials.setPackagePrice(tariffResponseClone.getData().getTariffResponseList().get(i)
							.getItems().get(j).getPackagePrice());
					itemsspecials.setSubscribable(tariffResponseClone.getData().getTariffResponseList().get(i)
							.getItems().get(j).getSubscribable());

					itemsTariffSpecials.add(itemsspecials);
					Utilities.printInfoLog(
							msisdn + "resData tariffIDCHECK ItemsSpecials :"
									+ tariffResponseClone.getData().getTariffResponseList().get(i).getItems().get(j),
							logger);

				} else {
					Utilities.printInfoLog(
							msisdn + "resData tariffIDCHECKSimple :" + tariffResponseClone.getData()
									.getTariffResponseList().get(i).getItems().get(j).getHeader().getOfferingId(),
							logger);
					items.setDetails(tariffResponseClone.getData().getTariffResponseList().get(i).getItems().get(j)
							.getDetails());
					items.setHeader(
							tariffResponseClone.getData().getTariffResponseList().get(i).getItems().get(j).getHeader());
					items.setPackagePrice(tariffResponseClone.getData().getTariffResponseList().get(i).getItems().get(j)
							.getPackagePrice());
					items.setSubscribable(tariffResponseClone.getData().getTariffResponseList().get(i).getItems().get(j)
							.getSubscribable());
					tariffReeponseObj
							.setGroupType(tariffResponseClone.getData().getTariffResponseList().get(i).getGroupType());
					itemsTariff.add(items);

					Utilities.printInfoLog(
							msisdn + "resData tariffIDCHECK Items Simple :"
									+ tariffResponseClone.getData().getTariffResponseList().get(i).getItems().get(j),
							logger);
				}
			} // END of Inner for Loop
			if (itemsTariffSpecials != null && !itemsTariffSpecials.isEmpty()) {
				tariffReeponseSpecialObj.setItems(itemsTariffSpecials);
				tariiffResponseList.add(tariffReeponseSpecialObj);
			}
			if (itemsTariff != null && !itemsTariff.isEmpty()) {
				tariffReeponseObj.setItems(itemsTariff);
				tariiffResponseList.add(tariffReeponseObj);
			}
		} // End of Outer For Loop

		resDatafinal.setTariffResponseList(tariiffResponseList);

		com.evampsaanga.azerfon.models.tariffdetails.TariffResponse tariffResponsefinal = new TariffResponse();
		// Storing response in hashmap.
		tariffResponsefinal.setData(resDatafinal);

		// Caching Time-stamp

		tariffResponsefinal.setCallStatus(Constants.Call_Status_True);
		tariffResponsefinal.setResultCode(Constants.APP_SERVER_SUCCESS_CODE);

		tariffResponsefinal
				.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("success", tariffRequest.getLang()));
		return tariffResponsefinal;
	}

	public TariffResponse getTariffDetailsBusinessV2(String msisdn, TariffRequestV2 tariffRequest,
			TariffResponse tariffResponse) throws Exception {

		Utilities.printDebugLog(msisdn + "-Request received in getTariffDetailsBusinessV2"
				+ Transactions.TARRIF_DETAILS_TRANSACTION_NAME + " BUSINESS V2 with data-" + tariffRequest.toString(),
				logger);

		// Get key which will be used to store and retrieve data from hash map.
		String tariffsCacheKey = this.getKeyForCacheV2(Constants.HASH_KEY_TARIFFS, tariffRequest);

		/*
		 * if (AppCache.getHashmapTariffs().containsKey(tariffsCacheKey)) {
		 * Utilities.printDebugLog(msisdn + "-TARIFFS" +
		 * Constants.CACHE_EXISTS_DESCRIPTION + "" + tariffsCacheKey, logger);
		 * tariffResponse = AppCache.getHashmapTariffsV2().get(tariffsCacheKey);
		 * tariffResponse.getLogsReport().setIsCached("true"); return tariffResponse;
		 * 
		 * } else {
		 */

		Utilities.printDebugLog(msisdn + "-TARIFFS" + Constants.CACHE_DOES_NOT_EXIST_DESCRIPTION + "" + tariffsCacheKey,
				logger);
		TariffResponseList resData = new TariffResponseList();
		RestClient rc = new RestClient();
		ObjectMapper mapper = new ObjectMapper();

		/*
		 * if (tariffRequest.getCustomerType().contains("Individual")) {
		 * tariffRequest.setCustomerType("Individual"); } if
		 * (tariffRequest.getCustomerType().contains("Corporate")) {
		 * tariffRequest.setCustomerType("Corporate"); }
		 */

		String requestJsonESB = mapper.writeValueAsString(tariffRequest);

		String path = GetConfigurations.getESBRoute("gettariffDetialsV2");
		Utilities.printDebugLog(msisdn + "-Request Call to ESB gettariffDetialsV2", logger);
		Utilities.printDebugLog(msisdn + "-Path-" + path, logger);
		Utilities.printDebugLog(msisdn + "-Request Packet-" + requestJsonESB, logger);

		tariffResponse.getLogsReport().setEsbRequestTime(Utilities.getReportDateTime());
		String response = rc.getResponseFromESB(path, requestJsonESB);
		tariffResponse.getLogsReport().setEsbResponseTime(Utilities.getReportDateTime());

		Utilities.printDebugLog(msisdn + "-Received response from ESB gettariffDetialsV2-" + response, logger);

		if (response != null && !response.isEmpty()) {

			// Converting String to JSON as String.
			// response = Utilities.stringToJSONString(response);

			// Logging ESB response code and description.
			tariffResponse.setLogsReport(
					Utilities.logESBParamsintoReportLog(requestJsonESB, response, tariffResponse.getLogsReport()));

			if (Utilities.getValueFromJSON(response, "returnCode").equalsIgnoreCase(Constants.ESB_SUCCESS_CODE)) {

				/*
				 * if (AppCache.getHashmapMigrationPricesMessages() == null ||
				 * AppCache.getHashmapMigrationPricesMessages().isEmpty()) {
				 * Utilities.printDebugLog(msisdn + "-MigrationCache null so updating From DB",
				 * logger); HashMap<String, MigrationPrices> buildMigrationPricesCache =
				 * GetConfigurations .getMigrationPrices(msisdn);
				 * 
				 * }
				 */
				// Starting logic for Special Tariffs
				JSONObject jsonObject = new JSONObject(response);
				Utilities.printInfoLog(msisdn + "TestingTariffResponseData gettariffDetialsV2:"
						+ jsonObject.getJSONArray("data").toString(), logger);

				// Starting logic for Special Tariffs
				resData = ParseTariffsResponse.parseTariffResponseData(msisdn,
						Utilities.getValueFromJSON(response, "data"), resData);
				Utilities.printInfoLog(msisdn + "-ResponseOfData-" + Utilities.getValueFromJSON(response, "data"),
						logger);

				// tariffResponse = resData;
				tariffResponse.setData(resData);

				tariffResponse.setCallStatus(Constants.Call_Status_True);
				tariffResponse.setResultCode(Constants.APP_SERVER_SUCCESS_CODE);

				tariffResponse.setResultDesc(
						GetMessagesMappings.getMessageFromResourceBundle("success", tariffRequest.getLang()));
				// Temporary commented cache should be maintained for B2B
//					AppCache.getHashmapTariffs().put(tariffsCacheKey, tariffResponse);
				AppCache.getHashmapTimestamps().put(Constants.HASH_KEY_TARIFFS,
						Utilities.getTimeStampForCache(Constants.HASH_KEY_TARIFFS));

			} else {
				tariffResponse.setCallStatus(Constants.Call_Status_False);
				tariffResponse.setResultCode(Utilities.getValueFromJSON(response, "returnCode"));
				tariffResponse.setResultDesc(Utilities.getErrorMessageFromFile("get.tariffs", tariffRequest.getLang(),
						Utilities.getValueFromJSON(response, "returnCode")));
				return tariffResponse;
			}
		} else {

			tariffResponse.setCallStatus(Constants.Call_Status_False);
			tariffResponse.setResultCode(Constants.API_FAILURE_CODE);

			tariffResponse.setResultDesc(
					GetMessagesMappings.getMessageFromResourceBundle("connectivity.error", tariffRequest.getLang()));
			return tariffResponse;

		}
		logger.info("Response Returned to Device: " + mapper.writeValueAsString(tariffResponse));
		return tariffResponse;

//		}

	}

	public ChangeTariffResponse changeTariffBusiness(String msisdn, ChangeTariffRequest changeTariffRequest,
			ChangeTariffResponse changeTariffResponse)
			throws JSONException, FileNotFoundException, IOException, SQLException {

		Utilities.printDebugLog(msisdn + "-Request received in " + Transactions.CHANGE_TARRIF_TRANSACTION_NAME
				+ " BUSINESS with data-" + changeTariffRequest.toString(), logger);

		ChangeTariffResponseData resData = new ChangeTariffResponseData();
		RestClient rc = new RestClient();
		ObjectMapper mapper = new ObjectMapper();

		String requestJsonESB = mapper.writeValueAsString(changeTariffRequest);

		String path = GetConfigurations.getESBRoute("changeTariff");
		Utilities.printDebugLog(msisdn + "-Request Call to ESB", logger);
		Utilities.printDebugLog(msisdn + "-Path-" + path, logger);
		Utilities.printDebugLog(msisdn + "-Request Packet-" + requestJsonESB, logger);
		requestJsonESB = Utilities.removeParamsFromJSONObject(requestJsonESB, "tariffName");
		changeTariffResponse.getLogsReport().setEsbRequestTime(Utilities.getReportDateTime());
		String response = rc.getResponseFromESB(path, requestJsonESB);
		changeTariffResponse.getLogsReport().setEsbResponseTime(Utilities.getReportDateTime());

		Utilities.printDebugLog(msisdn + "-Received response from ESB-" + response, logger);

		// Logging ESB response code and description.
		changeTariffResponse.setLogsReport(
				Utilities.logESBParamsintoReportLog(requestJsonESB, response, changeTariffResponse.getLogsReport()));
		if (response != null && !response.isEmpty()) {

			if (Utilities.getValueFromJSON(response, "returnCode").equalsIgnoreCase(Constants.ESB_SUCCESS_CODE)) {

				resData.setMessage(
						GetMessagesMappings.getMessageFromResourceBundle("success", changeTariffRequest.getLang()));

				changeTariffResponse.setData(resData);
				changeTariffResponse.setCallStatus(Constants.Call_Status_True);
				changeTariffResponse.setResultCode(Constants.APP_SERVER_SUCCESS_CODE);
				changeTariffResponse.setResultDesc(
						GetMessagesMappings.getMessageFromResourceBundle("success", changeTariffRequest.getLang()));

			} else {

				changeTariffResponse.setCallStatus(Constants.Call_Status_False);
				changeTariffResponse.setResultCode(Utilities.getValueFromJSON(response, "returnCode"));
				changeTariffResponse.setResultDesc(Utilities.getErrorMessageFromFile("change.tariff",
						changeTariffRequest.getLang(), Utilities.getValueFromJSON(response, "returnCode")));

			}
		} else {

			changeTariffResponse.setCallStatus(Constants.Call_Status_False);
			changeTariffResponse.setResultCode(Constants.API_FAILURE_CODE);
			changeTariffResponse.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("connectivity.error",
					changeTariffRequest.getLang()));
		}

		return changeTariffResponse;
	}

	// START OF changeTariff BULK B2B

	public ChangeTariffResponseBulk changeTariffBulkBusiness(String msisdn, ChangeTariffRequestBulk changeTariffRequest,
			ChangeTariffResponseBulk changeTariffResponse)
			throws JSONException, FileNotFoundException, IOException, SQLException {

		Utilities.printDebugLog(msisdn + "-Request received in " + Transactions.CHANGE_TARRIF_TRANSACTION_NAME
				+ " BUSINESS with data-" + changeTariffRequest.toString(), logger);

		ChangeTariffResponseData resData = new ChangeTariffResponseData();
		RestClient rc = new RestClient();
		ObjectMapper mapper = new ObjectMapper();

		String requestJsonESB = mapper.writeValueAsString(changeTariffRequest);

		String path = GetConfigurations.getESBRoute("insertorders");
		Utilities.printDebugLog(msisdn + "-Request Call to ESB", logger);
		Utilities.printDebugLog(msisdn + "-Path-" + path, logger);
		Utilities.printDebugLog(msisdn + "-Request Packet-" + requestJsonESB, logger);
		requestJsonESB = Utilities.removeParamsFromJSONObject(requestJsonESB, "tariffName");
		changeTariffResponse.getLogsReport().setEsbRequestTime(Utilities.getReportDateTime());
		String response = rc.getResponseFromESB(path, requestJsonESB);
		changeTariffResponse.getLogsReport().setEsbResponseTime(Utilities.getReportDateTime());

		Utilities.printDebugLog(msisdn + "-Received response from ESB-" + response, logger);

		// Logging ESB response code and description.
		changeTariffResponse.setLogsReport(
				Utilities.logESBParamsintoReportLog(requestJsonESB, response, changeTariffResponse.getLogsReport()));
		if (response != null && !response.isEmpty()) {

			if (Utilities.getValueFromJSON(response, "returnCode").equalsIgnoreCase(Constants.ESB_SUCCESS_CODE)) {

				resData.setMessage(
						GetMessagesMappings.getMessageFromResourceBundle("success", changeTariffRequest.getLang()));

				changeTariffResponse.setData(resData);
				changeTariffResponse.setCallStatus(Constants.Call_Status_True);
				changeTariffResponse.setResultCode(Constants.APP_SERVER_SUCCESS_CODE);
				changeTariffResponse.setResultDesc(
						GetMessagesMappings.getMessageFromResourceBundle("success", changeTariffRequest.getLang()));

			} else {

				changeTariffResponse.setCallStatus(Constants.Call_Status_False);
				changeTariffResponse.setResultCode(Utilities.getValueFromJSON(response, "returnCode"));
				changeTariffResponse.setResultDesc(Utilities.getErrorMessageFromFile("change.tariff",
						changeTariffRequest.getLang(), Utilities.getValueFromJSON(response, "returnCode")));

			}
		} else {

			changeTariffResponse.setCallStatus(Constants.Call_Status_False);
			changeTariffResponse.setResultCode(Constants.API_FAILURE_CODE);
			changeTariffResponse.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("connectivity.error",
					changeTariffRequest.getLang()));
		}

		return changeTariffResponse;
	}

	public CloseUserGroupResponseBase getcloseUserGroupData(String isFromB2B,
			String msisdn, String deviceID, CloseUserGroupRequest userGroupRequest,
			CloseUsersGroupResponse usersGroupResponse) throws ParseException,
			Exception {
		Utilities.printDebugLog(msisdn + "-Request received in "
				+ Transactions.USER_GROUP_DATA + " BUSINESS with data-"
				+ userGroupRequest.toString(), logger);

		CloseUserGroupResponseBase userGroupResponseBase = new CloseUserGroupResponseBase();
		CloseUsersGroupResponse resData = new CloseUsersGroupResponse();
		RestClient rc = new RestClient();
		ObjectMapper mapper = new ObjectMapper();

		String requestJsonESB = mapper.writeValueAsString(userGroupRequest);

		String path = GetConfigurations.getESBRoute("getcloseusergroupdata");
		Utilities.printDebugLog(msisdn + "-Request Call to ESB", logger);
		Utilities.printDebugLog(msisdn + "-Path-" + path, logger);
		Utilities.printDebugLog(msisdn + "-Request Packet-" + requestJsonESB,
				logger);

		userGroupResponseBase.getLogsReport().setEsbRequestTime(
				Utilities.getReportDateTime());
		logger.info("E&S -- Request Land Time in Business" + new Date());
		String response = rc.getResponseFromESB(path, requestJsonESB);
		logger.info("E&S -- Response Time in Business" + new Date());
		userGroupResponseBase.getLogsReport().setEsbResponseTime(
				Utilities.getReportDateTime());

		Utilities.printDebugLog(msisdn + "-Received response from ESB-"
				+ response, logger);

		// Logging ESB response code and description.
		userGroupResponseBase.setLogsReport(Utilities
				.logESBParamsintoReportLog(requestJsonESB, response,
						userGroupResponseBase.getLogsReport()));

		if (response != null && !response.isEmpty()) {

			if (Utilities.getValueFromJSON(response, "returnCode")
					.equalsIgnoreCase(Constants.ESB_SUCCESS_CODE)) {

				// mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES,
				// false);
				logger.info("E&S -- Before Parsing Time in Business "
						+ new Date());
				resData = parsejsonforGroupData(msisdn, deviceID, response,
						usersGroupResponse);
				logger.info("E&S -- After Parseing Time in Business "
						+ new Date());
				userGroupResponseBase.setData(resData);
				
				resData.setIsLastPage(Utilities.getValueFromJSON(response, "isLastPage"));
				userGroupResponseBase.setCallStatus(Constants.Call_Status_True);
				userGroupResponseBase
						.setResultCode(Constants.VALIDATION_SUCCESS_CODE);
				if (Utilities.getValueFromJSON(response, "returnMsg").equalsIgnoreCase(Constants.GET_USER_MSG)) {
					userGroupResponseBase.setResultDesc(Constants.GET_USER_MSG);
				} else {
					userGroupResponseBase.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("success", userGroupRequest.getLang()));
					}
			}
				else {
				userGroupResponseBase
						.setCallStatus(Constants.Call_Status_False);
				userGroupResponseBase.setResultCode(Utilities.getValueFromJSON(
						response, "returnCode"));
				userGroupResponseBase.setResultDesc(Utilities
						.getErrorMessageFromFile("home.page", userGroupRequest
								.getLang(), Utilities.getValueFromJSON(
								response, "returnCode")));
				userGroupResponseBase.setData(null);
			}
		} else {
			userGroupResponseBase.setCallStatus(Constants.Call_Status_False);
			userGroupResponseBase.setResultCode(Constants.API_FAILURE_CODE);
			userGroupResponseBase.setResultDesc(GetMessagesMappings
					.getMessageFromResourceBundle("connectivity.error",
							userGroupRequest.getLang()));
			userGroupResponseBase.setData(null);
		}
		logger.info("E&S -- Response Dispatch Time in Business" + new Date());
		return userGroupResponseBase;
	}

	private CloseUsersGroupResponse parsejsonforGroupData(String msisdn, String deviceID, String response,
			CloseUsersGroupResponse usersGroupResponse) throws JSONException, JsonProcessingException {
		ObjectMapper mapper = new ObjectMapper();
		JSONObject jsonObject = new JSONObject(response);
		JSONObject obj = new JSONObject();
		try {
			obj = jsonObject.getJSONObject("groupData");
			Iterator key = obj.keys();
			key = obj.keys();
			CloseUserGroupData userGroups = new CloseUserGroupData();

			ArrayList<CloseUsersGroupResponseData> usersGroupResponseData = new ArrayList<>();
			logger.info("E&S -- Response time before calculating group data " + new Date());
			while (key.hasNext()) {

				ArrayList<CloseUsersGroupData> usersGroupData = new ArrayList<>();
				String currentkey = (String) key.next();
				JSONArray jsonarr = obj.getJSONObject(currentkey).getJSONArray("usersGroupData");
				String groupName = obj.getJSONObject(currentkey).getString("groupName");
				CloseUsersGroupResponseData resGroupData = new CloseUsersGroupResponseData();
				resGroupData.setGroupName(groupName);
				int count = 0;
				// resGroupData.setGroupName(currentkey);
				for (int j = 0; j < jsonarr.length(); j++) {
					count++;
					usersGroupData.add(
							(CloseUsersGroupData) mapper.readValue((jsonarr.getString(j)), CloseUsersGroupData.class));
					// System.out.println(usersGroupData[i]);
				}
				resGroupData.setUserCount(String.valueOf(count));
				resGroupData.setUsersData(usersGroupData);
				usersGroupResponseData.add(resGroupData);
				// usersGroupResponse.setUserGroups(userGroups);(usersGroupResponseData);
			}
			userGroups.setUsersGroupData(usersGroupResponseData);
			usersGroupResponse.setGroupData(userGroups);
			logger.info("E&S -- Response time after calculating group data " + new Date());
			ArrayList<CloseUsersGroupData> users = new ArrayList<>();
			ArrayList<CloseUsersGroupResponseData> userGroups1 = usersGroupResponse.getGroupData().getUsersGroupData();
			for (int i = 0; i < userGroups1.size(); i++) {
				users.addAll(userGroups1.get(i).getUsersData());
			}
			usersGroupResponse.setUsers(users);
			logger.info("E&S -- Response time after calculating user group data " + new Date());
		} catch (Exception e) {
			logger.error("ERROR", e);
			usersGroupResponse.setGroupData(null);
		}
		logger.info("User Group Response :" + mapper.writeValueAsString(usersGroupResponse));
		return usersGroupResponse;
	}

	// END Of changeTariff BULK B2B

	private String getKeyForCacheV2(String hashKeyTariffs, TariffRequestV2 tariffRequest) {

		return hashKeyTariffs + tariffRequest.getLang();
	}

	// TODO
	// need to change this as pr commented for phase2
	private String getKeyForCache(String hashKeyTariffs, TariffRequest tariffRequest) {

		return hashKeyTariffs + "." + tariffRequest.getStoreId() + "." + tariffRequest.getSubscriberType() + "."
				+ tariffRequest.getOfferingId();
	}
}