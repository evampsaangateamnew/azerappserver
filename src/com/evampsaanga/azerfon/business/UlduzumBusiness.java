package com.evampsaanga.azerfon.business;
import java.net.SocketException;
import java.sql.SQLException;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONException;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.evampsaanga.azerfon.common.messagesmappings.GetMessagesMappings;
import com.evampsaanga.azerfon.common.utilities.Constants;
import com.evampsaanga.azerfon.common.utilities.GetConfigurations;
import com.evampsaanga.azerfon.common.utilities.Transactions;
import com.evampsaanga.azerfon.common.utilities.Utilities;
import com.evampsaanga.azerfon.models.ulduzum.GetCategoriesDataList;
import com.evampsaanga.azerfon.models.ulduzum.GetCodesEsbResponse;
import com.evampsaanga.azerfon.models.ulduzum.GetCodesResponse;
import com.evampsaanga.azerfon.models.ulduzum.GetMerchantsDataAppServer;
import com.evampsaanga.azerfon.models.ulduzum.GetMerchantsEsbResponse;
import com.evampsaanga.azerfon.models.ulduzum.GetMerchantsRequest;
import com.evampsaanga.azerfon.models.ulduzum.GetMerchantsResponse;
import com.evampsaanga.azerfon.models.ulduzum.GetUnusedCodesDataAppServer;
import com.evampsaanga.azerfon.models.ulduzum.GetUnusedCodesEsbResponse;
import com.evampsaanga.azerfon.models.ulduzum.GetUnusedCodesResponse;
import com.evampsaanga.azerfon.models.ulduzum.GetUsageHistoryDataAppServer;
import com.evampsaanga.azerfon.models.ulduzum.GetUsageHistoryEsbResponse;
import com.evampsaanga.azerfon.models.ulduzum.GetUsageHistoryRequest;
import com.evampsaanga.azerfon.models.ulduzum.GetUsageHistoryResponse;
import com.evampsaanga.azerfon.models.ulduzum.GetUsageTotalsEsbResponse;
import com.evampsaanga.azerfon.models.ulduzum.GetUsageTotalsResponse;
import com.evampsaanga.azerfon.models.ulduzum.UlduzumGetCategoriesResponse;
import com.evampsaanga.azerfon.models.ulduzum.UlduzumRequest;
import com.evampsaanga.azerfon.restclient.RestClient;

public class UlduzumBusiness {
	Logger logger = Logger.getLogger(UlduzumBusiness.class);
	public UlduzumGetCategoriesResponse getCategories(String msisdn, UlduzumRequest request,
			UlduzumGetCategoriesResponse getCategoreisResponse) throws SocketException, JsonProcessingException, JSONException, SQLException {
		ObjectMapper mapper = new ObjectMapper();
		String requestJsonESB = mapper.writeValueAsString(request);
		
		Utilities.printDebugLog(msisdn + "-Request received in " + Transactions.ULDUZUM_GET_CATEGORIES_TRANSACTION_NAME
				+ " BUSINESS with data-" + requestJsonESB , logger);
		
		RestClient rc = new RestClient();
		GetCategoriesDataList orderList = new GetCategoriesDataList();
		
		String path = GetConfigurations.getESBRoute("getcategories");
		
		Utilities.printDebugLog(msisdn + "-Request Call to ESB", logger);
		Utilities.printDebugLog(msisdn + "-Path-" + path, logger);
		Utilities.printDebugLog(msisdn + "-Request Packet-" + requestJsonESB, logger);
		getCategoreisResponse.getLogsReport().setEsbRequestTime(Utilities.getReportDateTime());
		String response = rc.getResponseFromESB(path, requestJsonESB);
		getCategoreisResponse.getLogsReport().setEsbResponseTime(Utilities.getReportDateTime());
		Utilities.printDebugLog(msisdn + "-Received response from ESB-" + response, logger);

		// Logging ESB response code and description.
		getCategoreisResponse.setLogsReport(
				Utilities.logESBParamsintoReportLog(requestJsonESB, response, getCategoreisResponse.getLogsReport()));
		if (response != null && !response.isEmpty()) {
			if (Utilities.getValueFromJSON(response, "returnCode").equalsIgnoreCase(Constants.ESB_SUCCESS_CODE)) {
				String responseData = Utilities.removeParamsFromJSONObject(response, "returnMsg");  //new JSONObject(response).get("orderList").toString();
				responseData = Utilities.removeParamsFromJSONObject(responseData, "returnCode");
				
				logger.info("<<<<<<<< responseData >>>>>>>>" +  responseData);
				try {
					orderList = mapper.readValue(responseData, GetCategoriesDataList.class);
					getCategoreisResponse.setData(orderList.getCategories());
					getCategoreisResponse.setResultCode(Constants.APP_SERVER_SUCCESS_CODE);
					getCategoreisResponse.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("success", request.getLang()));
					getCategoreisResponse.setCallStatus(Constants.Call_Status_True);
					return getCategoreisResponse;
				} catch (Exception e) {
					logger.error("ERROR:", e);
					getCategoreisResponse.setCallStatus(Constants.Call_Status_False);
					getCategoreisResponse.setResultCode(Utilities.getValueFromJSON(response, "returnCode"));
					getCategoreisResponse.setResultDesc(Utilities.getValueFromJSON(response, "returnMsg"));
					return getCategoreisResponse;
				}
				
			}
			else{
				getCategoreisResponse.setCallStatus(Constants.Call_Status_False);
				getCategoreisResponse.setResultCode(Utilities.getValueFromJSON(response, "returnCode"));
				getCategoreisResponse.setResultDesc(Utilities.getValueFromJSON(response, "returnMsg"));
				return getCategoreisResponse;
			}
		}else {
			getCategoreisResponse.setCallStatus(Constants.Call_Status_False);
			getCategoreisResponse.setResultCode(Constants.API_FAILURE_CODE);
			getCategoreisResponse.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("connectivity.error",
					request.getLang()));
			return getCategoreisResponse;
		}
	}

	//Start of getMerchant API,  business
	public GetMerchantsResponse getMerchants(String msisdn, GetMerchantsRequest request,
			GetMerchantsResponse getMerchantsReesponse) throws SocketException, JsonProcessingException, JSONException, SQLException {
		ObjectMapper mapper = new ObjectMapper();
		String requestJsonESB = mapper.writeValueAsString(request);
		
		Utilities.printDebugLog(msisdn + "-Request received in " + Transactions.ULDUZUM_GET_MERCHANTS_TRANSACTION_NAME
				+ " BUSINESS with data-" + requestJsonESB , logger);
		
		
		RestClient rc = new RestClient();
		GetMerchantsEsbResponse orderList = new GetMerchantsEsbResponse();
		
		String path = GetConfigurations.getESBRoute("getmerchants");
		
		Utilities.printDebugLog(msisdn + "-Request Call to ESB", logger);
		Utilities.printDebugLog(msisdn + "-Path-" + path, logger);
		Utilities.printDebugLog(msisdn + "-Request Packet-" + requestJsonESB, logger);
		getMerchantsReesponse.getLogsReport().setEsbRequestTime(Utilities.getReportDateTime());
		String response = rc.getResponseFromESB(path, requestJsonESB);
		getMerchantsReesponse.getLogsReport().setEsbResponseTime(Utilities.getReportDateTime());
		Utilities.printDebugLog(msisdn + "-Received response from ESB-" + response, logger);

		// Logging ESB response code and description.
		getMerchantsReesponse.setLogsReport(
				Utilities.logESBParamsintoReportLog(requestJsonESB, response, getMerchantsReesponse.getLogsReport()));
		if (response != null && !response.isEmpty()) {
			if (Utilities.getValueFromJSON(response, "returnCode").equalsIgnoreCase(Constants.ESB_SUCCESS_CODE)) {
				
				
				logger.info("<<<<<<<< responseData ESB >>>>>>>>" +  response);
				
				GetMerchantsResponse data=new GetMerchantsResponse();
				
				try {
					GetMerchantsDataAppServer dataAndcategory=new GetMerchantsDataAppServer();
					
					orderList = mapper.readValue(response, GetMerchantsEsbResponse.class);
					
					dataAndcategory.setCategoryList(orderList.getCategoryNamelist());
					dataAndcategory.setMaindatalist(orderList.getData());
					
					
					
					//getMerchantsReesponse.setData(orderList.getData();
					getMerchantsReesponse.setData(dataAndcategory);
					getMerchantsReesponse.setResultCode(Constants.APP_SERVER_SUCCESS_CODE);
					getMerchantsReesponse.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("success", request.getLang()));
					getMerchantsReesponse.setCallStatus(Constants.Call_Status_True);
					return getMerchantsReesponse;
				} catch (Exception e) {
					logger.error("ERROR:", e);
					getMerchantsReesponse.setCallStatus(Constants.Call_Status_False);
					getMerchantsReesponse.setResultCode(Utilities.getValueFromJSON(response, "returnCode"));
					getMerchantsReesponse.setResultDesc(Utilities.getValueFromJSON(response, "returnMsg"));
					return getMerchantsReesponse;
				}
				
			}
			else{
				getMerchantsReesponse.setCallStatus(Constants.Call_Status_False);
				getMerchantsReesponse.setResultCode(Utilities.getValueFromJSON(response, "returnCode"));
				getMerchantsReesponse.setResultDesc(Utilities.getValueFromJSON(response, "returnMsg"));
				return getMerchantsReesponse;
			}
		}else {
			getMerchantsReesponse.setCallStatus(Constants.Call_Status_False);
			getMerchantsReesponse.setResultCode(Constants.API_FAILURE_CODE);
			getMerchantsReesponse.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("connectivity.error",
					request.getLang()));
			return getMerchantsReesponse;
		}
	}
	
	//end of getMErchants API  business
	//start getUsageTotals API  business
	public GetUsageTotalsResponse getUsageTotals(String msisdn, UlduzumRequest request,
			GetUsageTotalsResponse responsefrombusiness) throws SocketException, JsonProcessingException, JSONException, SQLException {
		ObjectMapper mapper = new ObjectMapper();
		String requestJsonESB = mapper.writeValueAsString(request);
		
		Utilities.printDebugLog(msisdn + "-Request received in " + Transactions.ULDUZUM_GET_USAGE_TOTALS_TRANSACTION_NAME
				+ " BUSINESS with data-" + requestJsonESB , logger);
		
		
		RestClient rc = new RestClient();
		GetUsageTotalsEsbResponse orderList = new GetUsageTotalsEsbResponse();
		
		String path = GetConfigurations.getESBRoute("getusagetotals");
		
		Utilities.printDebugLog(msisdn + "-Request Call to ESB", logger);
		Utilities.printDebugLog(msisdn + "-Path-" + path, logger);
		Utilities.printDebugLog(msisdn + "-Request Packet-" + requestJsonESB, logger);
		responsefrombusiness.getLogsReport().setEsbRequestTime(Utilities.getReportDateTime());
		String response = rc.getResponseFromESB(path, requestJsonESB);
		responsefrombusiness.getLogsReport().setEsbResponseTime(Utilities.getReportDateTime());
		Utilities.printDebugLog(msisdn + "-Received response from ESB-" + response, logger);

		// Logging ESB response code and description.
		responsefrombusiness.setLogsReport(
				Utilities.logESBParamsintoReportLog(requestJsonESB, response, responsefrombusiness.getLogsReport()));
		if (response != null && !response.isEmpty()) {
			if (Utilities.getValueFromJSON(response, "returnCode").equalsIgnoreCase(Constants.ESB_SUCCESS_CODE)) {
				
				
				logger.info("<<<<<<<< responseData ESB >>>>>>>>" +  response);
				try {
					orderList = mapper.readValue(response, GetUsageTotalsEsbResponse.class);
					responsefrombusiness.setData(orderList.getData());
					responsefrombusiness.setResultCode(Constants.APP_SERVER_SUCCESS_CODE);
					responsefrombusiness.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("success", request.getLang()));
					responsefrombusiness.setCallStatus(Constants.Call_Status_True);
					return responsefrombusiness;
				} catch (Exception e) {
					logger.error("ERROR:", e);
					responsefrombusiness.setCallStatus(Constants.Call_Status_False);
					responsefrombusiness.setResultCode(Utilities.getValueFromJSON(response, "returnCode"));
					responsefrombusiness.setResultDesc(Utilities.getValueFromJSON(response, "returnMsg"));
					return responsefrombusiness;
				}
				
			}
			else{
				responsefrombusiness.setCallStatus(Constants.Call_Status_False);
				responsefrombusiness.setResultCode(Utilities.getValueFromJSON(response, "returnCode"));
				responsefrombusiness.setResultDesc(Utilities.getValueFromJSON(response, "returnMsg"));
				return responsefrombusiness;
			}
		}else {
			responsefrombusiness.setCallStatus(Constants.Call_Status_False);
			responsefrombusiness.setResultCode(Constants.API_FAILURE_CODE);
			responsefrombusiness.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("connectivity.error",
					request.getLang()));
			return responsefrombusiness;
		}
	}
	//end of getUsageTotals API  business
	//start of geyUnsedodes API business
	public GetUnusedCodesResponse getUnsedCodes(String msisdn, UlduzumRequest request,
			GetUnusedCodesResponse responsefrombusiness) throws SocketException, JsonProcessingException, JSONException, SQLException {
		ObjectMapper mapper = new ObjectMapper();
		String requestJsonESB = mapper.writeValueAsString(request);
		
		Utilities.printDebugLog(msisdn + "-Request received in " + Transactions.ULDUZUM_GET_UNUSED_CODES_TRANSACTION_NAME
				+ " BUSINESS with data-" + requestJsonESB , logger);
		
		
		RestClient rc = new RestClient();
		GetUnusedCodesEsbResponse orderList = new GetUnusedCodesEsbResponse();
		
		String path = GetConfigurations.getESBRoute("getunusedcodes");
		
		Utilities.printDebugLog(msisdn + "-Request Call to ESB", logger);
		Utilities.printDebugLog(msisdn + "-Path-" + path, logger);
		Utilities.printDebugLog(msisdn + "-Request Packet-" + requestJsonESB, logger);
		responsefrombusiness.getLogsReport().setEsbRequestTime(Utilities.getReportDateTime());
		String response = rc.getResponseFromESB(path, requestJsonESB);
		responsefrombusiness.getLogsReport().setEsbResponseTime(Utilities.getReportDateTime());
		Utilities.printDebugLog(msisdn + "-Received response from ESB-" + response, logger);

		// Logging ESB response code and description.
		responsefrombusiness.setLogsReport(
				Utilities.logESBParamsintoReportLog(requestJsonESB, response, responsefrombusiness.getLogsReport()));
		if (response != null && !response.isEmpty()) {
			if (Utilities.getValueFromJSON(response, "returnCode").equalsIgnoreCase(Constants.ESB_SUCCESS_CODE)) {
				
				
				logger.info("<<<<<<<< responseData ESB >>>>>>>>" +  response);
				try {
					orderList = mapper.readValue(response, GetUnusedCodesEsbResponse.class);
					
					GetUnusedCodesDataAppServer unusedcodesobject=new GetUnusedCodesDataAppServer();
					unusedcodesobject.setUnusedcodeslist(orderList.getData());
					responsefrombusiness.setData(unusedcodesobject);
					responsefrombusiness.setResultCode(Constants.APP_SERVER_SUCCESS_CODE);
					responsefrombusiness.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("success", request.getLang()));
					responsefrombusiness.setCallStatus(Constants.Call_Status_True);
					return responsefrombusiness;
				} catch (Exception e) {
					logger.error("ERROR:", e);
					responsefrombusiness.setCallStatus(Constants.Call_Status_False);
					responsefrombusiness.setResultCode(Utilities.getValueFromJSON(response, "returnCode"));
					responsefrombusiness.setResultDesc(Utilities.getValueFromJSON(response, "returnMsg"));
					return responsefrombusiness;
				}
				
			}
			else{
				responsefrombusiness.setCallStatus(Constants.Call_Status_False);
				responsefrombusiness.setResultCode(Utilities.getValueFromJSON(response, "returnCode"));
				responsefrombusiness.setResultDesc(Utilities.getValueFromJSON(response, "returnMsg"));
				return responsefrombusiness;
			}
		}else {
			responsefrombusiness.setCallStatus(Constants.Call_Status_False);
			responsefrombusiness.setResultCode(Constants.API_FAILURE_CODE);
			responsefrombusiness.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("connectivity.error",
					request.getLang()));
			return responsefrombusiness;
		}
	}
	//end of getUnsusedCodes API business
	
	//start of geyUsagehistory API business
		public GetUsageHistoryResponse getUsageHistory(String msisdn, GetUsageHistoryRequest request,
				GetUsageHistoryResponse responsefrombusiness) throws SocketException, JsonProcessingException, JSONException, SQLException {
			ObjectMapper mapper = new ObjectMapper();
			String requestJsonESB = mapper.writeValueAsString(request);
			
			Utilities.printDebugLog(msisdn + "-Request received in " + Transactions.ULDUZUM_GET_USAGE_HISTORY_TRANSACTION_NAME
					+ " BUSINESS with data-" + requestJsonESB , logger);
			
			
			RestClient rc = new RestClient();
			GetUsageHistoryEsbResponse orderList = new GetUsageHistoryEsbResponse();
			
			String path = GetConfigurations.getESBRoute("getusagehistory");
			
			Utilities.printDebugLog(msisdn + "-Request Call to ESB", logger);
			Utilities.printDebugLog(msisdn + "-Path-" + path, logger);
			Utilities.printDebugLog(msisdn + "-Request Packet-" + requestJsonESB, logger);
			responsefrombusiness.getLogsReport().setEsbRequestTime(Utilities.getReportDateTime());
			String response = rc.getResponseFromESB(path, requestJsonESB);
			responsefrombusiness.getLogsReport().setEsbResponseTime(Utilities.getReportDateTime());
			Utilities.printDebugLog(msisdn + "-Received response from ESB-" + response, logger);

			// Logging ESB response code and description.
			responsefrombusiness.setLogsReport(
					Utilities.logESBParamsintoReportLog(requestJsonESB, response, responsefrombusiness.getLogsReport()));
			if (response != null && !response.isEmpty()) {
				if (Utilities.getValueFromJSON(response, "returnCode").equalsIgnoreCase(Constants.ESB_SUCCESS_CODE)) {
					
					
					logger.info("<<<<<<<< responseData ESB >>>>>>>>" +  response);
					try {
						orderList = mapper.readValue(response, GetUsageHistoryEsbResponse.class);
						
						GetUsageHistoryDataAppServer datausageobj=new GetUsageHistoryDataAppServer();
						
						
						datausageobj.setUsageHistoryList(orderList.getData());
						
						responsefrombusiness.setData(datausageobj);
						responsefrombusiness.setResultCode(Constants.APP_SERVER_SUCCESS_CODE);
						responsefrombusiness.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("success", request.getLang()));
						responsefrombusiness.setCallStatus(Constants.Call_Status_True);
						return responsefrombusiness;
					} catch (Exception e) {
						logger.error("ERROR:", e);
						responsefrombusiness.setCallStatus(Constants.Call_Status_False);
						responsefrombusiness.setResultCode(Utilities.getValueFromJSON(response, "returnCode"));
						responsefrombusiness.setResultDesc(Utilities.getValueFromJSON(response, "returnMsg"));
						return responsefrombusiness;
					}
					
				}
				else{
					responsefrombusiness.setCallStatus(Constants.Call_Status_False);
					responsefrombusiness.setResultCode(Utilities.getValueFromJSON(response, "returnCode"));
					responsefrombusiness.setResultDesc(Utilities.getValueFromJSON(response, "returnMsg"));
					return responsefrombusiness;
				}
			}else {
				responsefrombusiness.setCallStatus(Constants.Call_Status_False);
				responsefrombusiness.setResultCode(Constants.API_FAILURE_CODE);
				responsefrombusiness.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("connectivity.error",
						request.getLang()));
				return responsefrombusiness;
			}
		}
		//end of getUsageHistory API business
	
		
		//start of getCodes API business
		public GetCodesResponse getCodes(String msisdn, UlduzumRequest request,
				GetCodesResponse responsefrombusiness) throws SocketException, JsonProcessingException, JSONException, SQLException {
			ObjectMapper mapper = new ObjectMapper();
			String requestJsonESB = mapper.writeValueAsString(request);
			
			Utilities.printDebugLog(msisdn + "-Request received in " + Transactions.ULDUZUM_GET_CODES
					+ " BUSINESS with data-" + requestJsonESB , logger);
			
			
			RestClient rc = new RestClient();
			GetCodesEsbResponse orderList = new GetCodesEsbResponse();
			
			String path = GetConfigurations.getESBRoute("getcodes");
			
			Utilities.printDebugLog(msisdn + "-Request Call to ESB", logger);
			Utilities.printDebugLog(msisdn + "-Path-" + path, logger);
			Utilities.printDebugLog(msisdn + "-Request Packet-" + requestJsonESB, logger);
			responsefrombusiness.getLogsReport().setEsbRequestTime(Utilities.getReportDateTime());
			String response = rc.getResponseFromESB(path, requestJsonESB);
			responsefrombusiness.getLogsReport().setEsbResponseTime(Utilities.getReportDateTime());
			Utilities.printDebugLog(msisdn + "-Received response from ESB-" + response, logger);

			// Logging ESB response code and description.
			responsefrombusiness.setLogsReport(
					Utilities.logESBParamsintoReportLog(requestJsonESB, response, responsefrombusiness.getLogsReport()));
			if (response != null && !response.isEmpty()) {
				if (Utilities.getValueFromJSON(response, "returnCode").equalsIgnoreCase(Constants.ESB_SUCCESS_CODE)) {
					
					
					logger.info("<<<<<<<< responseData ESB >>>>>>>>" +  response);
					try {
						orderList = mapper.readValue(response, GetCodesEsbResponse.class);
						responsefrombusiness.setData(orderList.getData());
						responsefrombusiness.setResultCode(Constants.APP_SERVER_SUCCESS_CODE);
						responsefrombusiness.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("success", request.getLang()));
						responsefrombusiness.setCallStatus(Constants.Call_Status_True);
						return responsefrombusiness;
					} catch (Exception e) {
						logger.error("ERROR:", e);
						responsefrombusiness.setCallStatus(Constants.Call_Status_False);
						responsefrombusiness.setResultCode(Utilities.getValueFromJSON(response, "returnCode"));
						responsefrombusiness.setResultDesc(Utilities.getValueFromJSON(response, "returnMsg"));
						return responsefrombusiness;
					}
					
				}
				else{
					responsefrombusiness.setCallStatus(Constants.Call_Status_False);
					responsefrombusiness.setResultCode(Utilities.getValueFromJSON(response, "returnCode"));
					responsefrombusiness.setResultDesc(Utilities.getValueFromJSON(response, "returnMsg"));
					return responsefrombusiness;
				}
			}else {
				responsefrombusiness.setCallStatus(Constants.Call_Status_False);
				responsefrombusiness.setResultCode(Constants.API_FAILURE_CODE);
				responsefrombusiness.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("connectivity.error",
						request.getLang()));
				return responsefrombusiness;
			}
		}
		//end of getCodes API business
		
		
		
		
}
