/**
 * 
 */
package com.evampsaanga.azerfon.business;

import java.io.IOException;
import java.sql.SQLException;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONException;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.evampsaanga.azerfon.common.messagesmappings.GetMessagesMappings;
import com.evampsaanga.azerfon.common.utilities.Constants;
import com.evampsaanga.azerfon.common.utilities.GetConfigurations;
import com.evampsaanga.azerfon.common.utilities.Transactions;
import com.evampsaanga.azerfon.common.utilities.Utilities;
import com.evampsaanga.azerfon.models.broadcastsms.BroadcastSMSRequest;
import com.evampsaanga.azerfon.models.quickservices.getfreesmsstatus.GetFreeSMSStatusRequest;
import com.evampsaanga.azerfon.models.quickservices.getfreesmsstatus.GetFreeSMSStatusResponse;
import com.evampsaanga.azerfon.models.quickservices.getfreesmsstatus.GetFreeSMSStatusResponseData;
import com.evampsaanga.azerfon.models.quickservices.sendfreesms.SendFreeSMSRequest;
import com.evampsaanga.azerfon.models.quickservices.sendfreesms.SendFreeSMSResponse;
import com.evampsaanga.azerfon.models.quickservices.sendfreesms.SendFreeSMSResponseData;
import com.evampsaanga.azerfon.models.quickservices.sendfreesms.SendFreeSMSResponseDataV2;
import com.evampsaanga.azerfon.models.quickservices.sendfreesms.SendFreeSMSResponseV2;
import com.evampsaanga.azerfon.restclient.RestClient;

/**
 * @author Evamp & Saanga
 *
 */
public class QuickServicesBusiness {
    Logger logger = Logger.getLogger(QuickServicesBusiness.class);

    public SendFreeSMSResponse sendFreeSMSBusiness(String msisdn, SendFreeSMSRequest freeSMSRequest,
	    SendFreeSMSResponse freeSMSResponse) throws JSONException, IOException, SQLException {
	Utilities.printDebugLog(msisdn + "-Request received in " + Transactions.SEND_FREE_SMS + " BUSINESS with data-"
		+ freeSMSRequest.toString(), logger);

	SendFreeSMSResponseData resData = new SendFreeSMSResponseData();
	RestClient rc = new RestClient();
	ObjectMapper mapper = new ObjectMapper();

	String requestJsonESB = mapper.writeValueAsString(freeSMSRequest);

	String path = GetConfigurations.getESBRoute("sendFreeSMS");
	Utilities.printDebugLog(msisdn + "<<<<<<<<< -Request Call to ESB >>>>>>>>>", logger);
	Utilities.printDebugLog(msisdn + "-Path-" + path, logger);
	Utilities.printDebugLog(msisdn + "-Request Packet-" + requestJsonESB, logger);

	freeSMSResponse.getLogsReport().setEsbRequestTime(Utilities.getReportDateTime());
	String response = rc.getResponseFromESB(path, requestJsonESB);
	freeSMSResponse.getLogsReport().setEsbResponseTime(Utilities.getReportDateTime());

	Utilities.printDebugLog(msisdn + "-Received response from ESB-" + response, logger);

	// Logging ESB response code and description.
	freeSMSResponse.setLogsReport(
		Utilities.logESBParamsintoReportLog(requestJsonESB, response, freeSMSResponse.getLogsReport()));

	if (response != null && !response.isEmpty()) {

	    if (Utilities.getValueFromJSON(response, "returnCode").equalsIgnoreCase(Constants.ESB_SUCCESS_CODE)) {

		resData = mapper.readValue(Utilities.getValueFromJSON(response, "status"),
			SendFreeSMSResponseData.class);
		resData.setResponseMsg(GetMessagesMappings.getMessageFromResourceBundle("success", freeSMSRequest.getLang()));
		freeSMSResponse.setData(resData);
		
		freeSMSResponse.setCallStatus(Constants.Call_Status_True);
		freeSMSResponse.setResultCode(Constants.APP_SERVER_SUCCESS_CODE);
		freeSMSResponse.setResultDesc(
			GetMessagesMappings.getMessageFromResourceBundle("success", freeSMSRequest.getLang()));

	    } else {
		freeSMSResponse.setCallStatus(Constants.Call_Status_False);
		freeSMSResponse.setResultCode(Utilities.getValueFromJSON(response, "returnCode"));
		freeSMSResponse.setResultDesc(Utilities.getErrorMessageFromFile("free.sms", freeSMSRequest.getLang(),
			Utilities.getValueFromJSON(response, "returnCode")));
	    }
	} else {

	    freeSMSResponse.setCallStatus(Constants.Call_Status_False);
	    freeSMSResponse.setResultCode(Constants.API_FAILURE_CODE);
	    freeSMSResponse.setResultDesc(
		    GetMessagesMappings.getMessageFromResourceBundle("connectivity.error", freeSMSRequest.getLang()));
	}

	return freeSMSResponse;

    }

    public SendFreeSMSResponseV2 sendFreeSMSBusinessV2(String msisdn, BroadcastSMSRequest sendFreeSMSRequest,
    	    SendFreeSMSResponseV2 freeSMSResponse) throws JSONException, IOException, SQLException {
    	Utilities.printDebugLog(msisdn + "-Request received in " + Transactions.SEND_FREE_SMS + " BUSINESS with data-"
    		+ sendFreeSMSRequest.toString(), logger);

    	SendFreeSMSResponseDataV2 resData = new SendFreeSMSResponseDataV2();
    	RestClient rc = new RestClient();
    	ObjectMapper mapper = new ObjectMapper();

    	String requestJsonESB = mapper.writeValueAsString(sendFreeSMSRequest);

    	String path = GetConfigurations.getESBRoute("insertorders");
    	Utilities.printDebugLog(msisdn + "<<<<<<<<< -Request Call to ESB >>>>>>>>>", logger);
    	Utilities.printDebugLog(msisdn + "-Path-" + path, logger);
    	Utilities.printDebugLog(msisdn + "-Request Packet-" + requestJsonESB, logger);

    	freeSMSResponse.getLogsReport().setEsbRequestTime(Utilities.getReportDateTime());
    	String response = rc.getResponseFromESB(path, requestJsonESB);
    	freeSMSResponse.getLogsReport().setEsbResponseTime(Utilities.getReportDateTime());

    	Utilities.printDebugLog(msisdn + "-Received response from ESB-" + response, logger);

    	// Logging ESB response code and description.
    	freeSMSResponse.setLogsReport(
    		Utilities.logESBParamsintoReportLog(requestJsonESB, response, freeSMSResponse.getLogsReport()));

    	if (response != null && !response.isEmpty()) {

    	    if (Utilities.getValueFromJSON(response, "returnCode").equalsIgnoreCase(Constants.ESB_SUCCESS_CODE)) {

    		/*resData = mapper.readValue(Utilities.getValueFromJSON(response, "responseMsg"),
    				SendFreeSMSResponseDataV2.class);*/
    		resData.setResponseMsg(GetMessagesMappings.getMessageFromResourceBundle("success", sendFreeSMSRequest.getLang()));
    		freeSMSResponse.setData(resData);
    		freeSMSResponse.setCallStatus(Constants.Call_Status_True);
    		freeSMSResponse.setResultCode(Constants.APP_SERVER_SUCCESS_CODE);
    		freeSMSResponse.setResultDesc(
    			GetMessagesMappings.getMessageFromResourceBundle("success", sendFreeSMSRequest.getLang()));

    	    } else {
    		freeSMSResponse.setCallStatus(Constants.Call_Status_False);
    		freeSMSResponse.setResultCode(Utilities.getValueFromJSON(response, "returnCode"));
    		freeSMSResponse.setResultDesc(Utilities.getErrorMessageFromFile("free.sms", sendFreeSMSRequest.getLang(),
    			Utilities.getValueFromJSON(response, "returnCode")));
    	    }
    	} else {

    	    freeSMSResponse.setCallStatus(Constants.Call_Status_False);
    	    freeSMSResponse.setResultCode(Constants.API_FAILURE_CODE);
    	    freeSMSResponse.setResultDesc(
    		    GetMessagesMappings.getMessageFromResourceBundle("connectivity.error", sendFreeSMSRequest.getLang()));
    	}

    	return freeSMSResponse;

        }

    
    public GetFreeSMSStatusResponse getFreeSMSStatusBusiness(String msisdn,
	    GetFreeSMSStatusRequest freeSMSStatusRequest, GetFreeSMSStatusResponse freeSMSStatusResponse)
	    throws JSONException, IOException, SQLException {
	Utilities.printDebugLog(msisdn + "-Request received in " + Transactions.SEND_SMS_STATUS + " BUSINESS with data-"
		+ freeSMSStatusRequest.toString(), logger);

	GetFreeSMSStatusResponseData resData = new GetFreeSMSStatusResponseData();
	RestClient rc = new RestClient();
	ObjectMapper mapper = new ObjectMapper();

	String requestJsonESB = mapper.writeValueAsString(freeSMSStatusRequest);

	String path = GetConfigurations.getESBRoute("getFreeSMSStatus");
	Utilities.printDebugLog(msisdn + "-Request Call to ESB", logger);
	Utilities.printDebugLog(msisdn + "-Path-" + path, logger);
	Utilities.printDebugLog(msisdn + "-Request Packet-" + requestJsonESB, logger);

	freeSMSStatusResponse.getLogsReport().setEsbRequestTime(Utilities.getReportDateTime());
	String response = rc.getResponseFromESB(path, requestJsonESB);
	freeSMSStatusResponse.getLogsReport().setEsbResponseTime(Utilities.getReportDateTime());

	Utilities.printDebugLog(msisdn + "-Received response from ESB-" + response, logger);

	// Logging ESB response code and description.
	freeSMSStatusResponse.setLogsReport(
		Utilities.logESBParamsintoReportLog(requestJsonESB, response, freeSMSStatusResponse.getLogsReport()));

	if (response != null && !response.isEmpty()) {

	    if (Utilities.getValueFromJSON(response, "returnCode").equalsIgnoreCase(Constants.ESB_SUCCESS_CODE)) {

		resData = mapper.readValue(Utilities.getValueFromJSON(response, "status"),
			GetFreeSMSStatusResponseData.class);

		freeSMSStatusResponse.setData(resData);
		freeSMSStatusResponse.setCallStatus(Constants.Call_Status_True);
		freeSMSStatusResponse.setResultCode(Constants.APP_SERVER_SUCCESS_CODE);
		freeSMSStatusResponse.setResultDesc(
			GetMessagesMappings.getMessageFromResourceBundle("success", freeSMSStatusRequest.getLang()));

	    } else {

		freeSMSStatusResponse.setCallStatus(Constants.Call_Status_False);
		freeSMSStatusResponse.setResultCode(Utilities.getValueFromJSON(response, "returnCode"));
		freeSMSStatusResponse.setResultDesc(Utilities.getErrorMessageFromFile("free.sms.status",
			freeSMSStatusRequest.getLang(), Utilities.getValueFromJSON(response, "returnCode")));

	    }
	} else {

	    freeSMSStatusResponse.setCallStatus(Constants.Call_Status_False);
	    freeSMSStatusResponse.setResultCode(Constants.API_FAILURE_CODE);
	    freeSMSStatusResponse.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("connectivity.error",
		    freeSMSStatusRequest.getLang()));
	}

	return freeSMSStatusResponse;
    }

}
