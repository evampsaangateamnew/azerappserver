/**
 * 
 */
package com.evampsaanga.azerfon.business;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.log4j.Logger;

import com.evampsaanga.azerfon.common.messagesmappings.GetMessagesMappings;
import com.evampsaanga.azerfon.common.utilities.AppCache;
import com.evampsaanga.azerfon.common.utilities.Constants;
import com.evampsaanga.azerfon.common.utilities.GetConfigurations;
import com.evampsaanga.azerfon.common.utilities.Transactions;
import com.evampsaanga.azerfon.common.utilities.Utilities;
import com.evampsaanga.azerfon.models.customerservices.customerinfo.CustomerInfoRequest;
import com.evampsaanga.azerfon.models.customerservices.customerinfo.CustomerInfoResponseData;
import com.evampsaanga.azerfon.models.mysubscriptions.getsubscriptions.MySubscriptionsRequest;
import com.evampsaanga.azerfon.models.supplementaryofferings.changesupplementaryoffering.ChangeSupplementaryOfferingsRequest;
import com.evampsaanga.azerfon.models.supplementaryofferings.changesupplementaryoffering.ChangeSupplementaryOfferingsRequestBulk;
import com.evampsaanga.azerfon.models.supplementaryofferings.changesupplementaryoffering.ChangeSupplementaryOfferingsResponse;
import com.evampsaanga.azerfon.models.supplementaryofferings.changesupplementaryoffering.ChangeSupplementaryOfferingsResponseData;
import com.evampsaanga.azerfon.models.supplementaryofferings.changesupplementaryoffering.ChangeSupplementaryOfferingsResponseDatabulk;
import com.evampsaanga.azerfon.models.supplementaryofferings.changesupplementaryoffering.ChangeSupplementaryOfferingsResponsebulk;
import com.evampsaanga.azerfon.models.supplementaryofferings.getsupplementaryofferings.ParseSupplementaryOfferingsResponse;
import com.evampsaanga.azerfon.models.supplementaryofferings.getsupplementaryofferings.SpecialOffersRequest;
import com.evampsaanga.azerfon.models.supplementaryofferings.getsupplementaryofferings.SpecialOffersResponse;
import com.evampsaanga.azerfon.models.supplementaryofferings.getsupplementaryofferings.SpecialOffersResponseData;
import com.evampsaanga.azerfon.models.supplementaryofferings.getsupplementaryofferings.SupplementaryOfferingsByIDsRequest;
import com.evampsaanga.azerfon.models.supplementaryofferings.getsupplementaryofferings.SupplementaryOfferingsRequest;
import com.evampsaanga.azerfon.models.supplementaryofferings.getsupplementaryofferings.SupplementaryOfferingsResponse;
import com.evampsaanga.azerfon.models.supplementaryofferings.getsupplementaryofferings.SupplementaryOfferingsResponseData;
import com.evampsaanga.azerfon.models.supplementaryofferings.getsupplementaryofferings.commonofferings.SupplementryOfferingsData;
import com.evampsaanga.azerfon.models.supplementaryofferings.getsupplementaryofferings.commonofferings.header.GroupedOfferAttributes;
import com.evampsaanga.azerfon.models.supplementaryofferings.getsupplementaryofferings.offerscategoriestab.Special;
import com.evampsaanga.azerfon.restclient.RestClient;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * @author Evamp & Saanga
 *
 */
public class SupplementaryOfferingsBusiness {
    Logger logger = Logger.getLogger(SupplementaryOfferingsBusiness.class);

    public SupplementaryOfferingsResponse getSupplementaryOfferings(String msisdn,
	    SupplementaryOfferingsRequest supplementaryOfferingsRequest,
	    SupplementaryOfferingsResponse supplementaryOfferingsResponse) throws Exception {
	Utilities.printDebugLog(msisdn + "-Request received in " + Transactions.SUPPLEMENTARY_SERVICES_TRANSACTION_NAME
		+ " BUSINESS with data-" + supplementaryOfferingsRequest.toString(), logger);
	ObjectMapper mapper = new ObjectMapper();
	// Get key which will be used to store and retrieve data from hash map.
	String supplementaryOfferingsCacheKey = this.getKeyForCache(Constants.HASH_KEY_SUPPLEMENTARY_OFFERINGS,
		supplementaryOfferingsRequest);

	if (AppCache.getHashmapSupplementaryOfferings().containsKey(supplementaryOfferingsCacheKey)) {
	    Utilities.printDebugLog(msisdn + "-SUPPLEMENTARY OFFERINGS" + Constants.CACHE_EXISTS_DESCRIPTION + ""
		    + supplementaryOfferingsCacheKey, logger);
	    supplementaryOfferingsResponse = AppCache.getHashmapSupplementaryOfferings()
		    .get(supplementaryOfferingsCacheKey);
	    supplementaryOfferingsResponse.getLogsReport().setIsCached("true");
	    Utilities.printInfoLog(msisdn + "-Received From customerDataInternalCall SuplemntraryResponseIN_IF_CACHE-"
		    + mapper.writeValueAsString(supplementaryOfferingsResponse), logger);
	    // return supplementaryOfferingsResponse;

	} else {
	    Utilities.printDebugLog(msisdn + "-SUPPLEMENTARY OFFERINGS" + Constants.CACHE_DOES_NOT_EXIST_DESCRIPTION
		    + supplementaryOfferingsCacheKey, logger);
	    SupplementaryOfferingsResponseData resData = new SupplementaryOfferingsResponseData();
	    RestClient rc = new RestClient();
	    String requestJsonESB = mapper.writeValueAsString(supplementaryOfferingsRequest);

	    String path = GetConfigurations.getESBRoute("getSupplementaryOfferings");
	    Utilities.printDebugLog(msisdn + "-Request Call to ESB", logger);
	    Utilities.printDebugLog(msisdn + "-Path-" + path, logger);
	    Utilities.printDebugLog(msisdn + "-Request Packet-" + requestJsonESB, logger);

	    supplementaryOfferingsResponse.getLogsReport().setEsbRequestTime(Utilities.getReportDateTime());
	    String response = rc.getResponseFromESB(path, requestJsonESB);
	    supplementaryOfferingsResponse.getLogsReport().setEsbResponseTime(Utilities.getReportDateTime());

	    Utilities.printDebugLog(msisdn + "-Received response from ESB-" + response, logger);

	    if (response != null && !response.isEmpty()) {
		// Converting String to JSON as String.
		// response = Utilities.stringToJSONString(response);

		// Logging ESB response code and description.

		supplementaryOfferingsResponse.setLogsReport(Utilities.logESBParamsintoReportLog(requestJsonESB,
			response, supplementaryOfferingsResponse.getLogsReport()));

		if (Utilities.getValueFromJSON(response, "returnCode").equalsIgnoreCase(Constants.ESB_SUCCESS_CODE)) {

		    resData = ParseSupplementaryOfferingsResponse.parseSupplementaryOfferingsData(msisdn,
			    Utilities.getValueFromJSON(response, "data"), resData);

		    // SupplementaryOfferingsResponseData response=TmProcessing(resData);

		    // Storing response in hashmap.

		    supplementaryOfferingsResponse.setData(resData);
		    Utilities.printInfoLog(
			    msisdn + "-Received From customerDataInternalCall SuplemntraryResponsesBeforeCache-"
				    + mapper.writeValueAsString(supplementaryOfferingsResponse),
			    logger);
		    AppCache.getHashmapSupplementaryOfferings().put(supplementaryOfferingsCacheKey,
			    supplementaryOfferingsResponse);
		    // Caching Time-stamp

		    Utilities.printInfoLog(msisdn
			    + "-Received From customerDataInternalCall SuplemntraryResponsesAFTERCache-"
			    + mapper.writeValueAsString(
				    AppCache.getHashmapSupplementaryOfferings().get(supplementaryOfferingsCacheKey)),
			    logger);
		    AppCache.getHashmapTimestamps().put(Constants.HASH_KEY_SUPPLEMENTARY_OFFERINGS,
			    Utilities.getTimeStampForCache(Constants.HASH_KEY_SUPPLEMENTARY_OFFERINGS));

		    supplementaryOfferingsResponse.setCallStatus(Constants.Call_Status_True);
		    supplementaryOfferingsResponse.setResultCode(Constants.APP_SERVER_SUCCESS_CODE);
		    supplementaryOfferingsResponse.setResultDesc(GetMessagesMappings
			    .getMessageFromResourceBundle("success", supplementaryOfferingsRequest.getLang()));

		} else {
		    /*
		     * This else is needed just to show empty cards on mobile by sending success APP
		     * SERVER code
		     */
		    supplementaryOfferingsResponse.setData(resData);
		    supplementaryOfferingsResponse.setCallStatus(Constants.Call_Status_True);
		    supplementaryOfferingsResponse.setResultCode(Constants.APP_SERVER_SUCCESS_CODE);
		    supplementaryOfferingsResponse.setResultDesc(Utilities.getErrorMessageFromFile(
			    "supplementary.offers", supplementaryOfferingsRequest.getLang(),
			    Utilities.getValueFromJSON(response, "returnCode")));
		}

	    } else {
		supplementaryOfferingsResponse.setData(resData);
		supplementaryOfferingsResponse.setCallStatus(Constants.Call_Status_False);
		supplementaryOfferingsResponse.setResultCode(Constants.API_FAILURE_CODE);

		supplementaryOfferingsResponse.setResultDesc(GetMessagesMappings
			.getMessageFromResourceBundle("connectivity.error", supplementaryOfferingsRequest.getLang()));
		return supplementaryOfferingsResponse;
	    }

	}

	// START OF LOGIC , to SHOW or Hide OFFERs comparing with Customer Data
	//
	// preparing customerInfo Request Packet
	SupplementaryOfferingsResponse responseupdated = null;
	responseupdated = (SupplementaryOfferingsResponse) supplementaryOfferingsResponse.clone();
	ManipulateResponseForHideVisibleAllowedChecks(msisdn, responseupdated, supplementaryOfferingsResponse,
		supplementaryOfferingsRequest);
	// START SETTING LOGIC TO ELIMINATE SPEICAL OFFERS FROM SUPPLEMENTARY
	if (!msisdn.contains("=")) {
	    ArrayList<SupplementryOfferingsData> offerssms = new ArrayList<SupplementryOfferingsData>();
	    if (responseupdated.getData().getSms() != null && responseupdated.getData().getSms().getOffers() != null
		    && !responseupdated.getData().getSms().getOffers().isEmpty()
		    && responseupdated.getData().getSms().getOffers().size() > 0) {
		for (int i = 0; i < responseupdated.getData().getSms().getOffers().size(); i++) {
		    if (responseupdated.getData().getSms().getOffers().get(i).getHeader() != null) {
			if (!supplementaryOfferingsRequest.getSpecialOfferIds().contains(
				responseupdated.getData().getSms().getOffers().get(i).getHeader().getOfferingId())) {

			    offerssms.add(responseupdated.getData().getSms().getOffers().get(i));

			}
		    }
		}
		responseupdated.getData().getSms().setOffers(offerssms);
	    }
	    ArrayList<SupplementryOfferingsData> offerinternet = new ArrayList<SupplementryOfferingsData>();
	    if (responseupdated.getData().getInternet() != null
		    && responseupdated.getData().getInternet().getOffers() != null
		    && !responseupdated.getData().getInternet().getOffers().isEmpty()
		    && responseupdated.getData().getInternet().getOffers().size() > 0) {
		for (int i = 0; i < responseupdated.getData().getInternet().getOffers().size(); i++) {
		    if (responseupdated.getData().getInternet().getOffers().get(i).getHeader() != null) {
			if (!supplementaryOfferingsRequest.getSpecialOfferIds().contains(responseupdated.getData()
				.getInternet().getOffers().get(i).getHeader().getOfferingId())) {

			    offerinternet.add(responseupdated.getData().getInternet().getOffers().get(i));

			}
		    }
		}
		responseupdated.getData().getInternet().setOffers(offerinternet);
	    }

	    ArrayList<SupplementryOfferingsData> offercall = new ArrayList<SupplementryOfferingsData>();
	    if (responseupdated.getData().getCall() != null && responseupdated.getData().getCall().getOffers() != null
		    && !responseupdated.getData().getCall().getOffers().isEmpty()
		    && responseupdated.getData().getCall().getOffers().size() > 0) {
		for (int i = 0; i < responseupdated.getData().getCall().getOffers().size(); i++) {
		    if (responseupdated.getData().getCall().getOffers().get(i).getHeader() != null) {
			if (!supplementaryOfferingsRequest.getSpecialOfferIds().contains(
				responseupdated.getData().getCall().getOffers().get(i).getHeader().getOfferingId())) {

			    offercall.add(responseupdated.getData().getCall().getOffers().get(i));

			}
		    }
		}
		responseupdated.getData().getCall().setOffers(offercall);
	    }
	    ArrayList<SupplementryOfferingsData> offeroaming = new ArrayList<SupplementryOfferingsData>();
	    if (responseupdated.getData().getRoaming() != null
		    && responseupdated.getData().getRoaming().getOffers() != null
		    && !responseupdated.getData().getRoaming().getOffers().isEmpty()
		    && responseupdated.getData().getRoaming().getOffers().size() > 0) {
		for (int i = 0; i < responseupdated.getData().getRoaming().getOffers().size(); i++) {
		    if (responseupdated.getData().getRoaming().getOffers().get(i).getHeader() != null) {
			if (!supplementaryOfferingsRequest.getSpecialOfferIds().contains(responseupdated.getData()
				.getRoaming().getOffers().get(i).getHeader().getOfferingId())) {

			    offeroaming.add(responseupdated.getData().getRoaming().getOffers().get(i));

			}
		    }
		}
		responseupdated.getData().getRoaming().setOffers(offeroaming);
	    }

	    ArrayList<SupplementryOfferingsData> offerHybrid = new ArrayList<SupplementryOfferingsData>();
	    if (responseupdated.getData().getHybrid() != null
		    && responseupdated.getData().getHybrid().getOffers() != null
		    && !responseupdated.getData().getHybrid().getOffers().isEmpty()
		    && responseupdated.getData().getHybrid().getOffers().size() > 0) {
		for (int i = 0; i < responseupdated.getData().getHybrid().getOffers().size(); i++) {
		    if (responseupdated.getData().getHybrid().getOffers().get(i).getHeader() != null) {
			if (!supplementaryOfferingsRequest.getSpecialOfferIds().contains(
				responseupdated.getData().getHybrid().getOffers().get(i).getHeader().getOfferingId())) {

			    offerHybrid.add(responseupdated.getData().getHybrid().getOffers().get(i));

			}
		    }
		}
		responseupdated.getData().getHybrid().setOffers(offerHybrid);
	    }
	    ArrayList<SupplementryOfferingsData> offerSmsInclusive = new ArrayList<SupplementryOfferingsData>();
	    if (responseupdated.getData().getSmsInclusiveOffers() != null
		    && responseupdated.getData().getSmsInclusiveOffers().getOffers() != null
		    && !responseupdated.getData().getSmsInclusiveOffers().getOffers().isEmpty()
		    && responseupdated.getData().getSmsInclusiveOffers().getOffers().size() > 0) {
		for (int i = 0; i < responseupdated.getData().getSmsInclusiveOffers().getOffers().size(); i++) {
		    if (responseupdated.getData().getSmsInclusiveOffers().getOffers().get(i).getHeader() != null) {
			if (!supplementaryOfferingsRequest.getSpecialOfferIds().contains(responseupdated.getData()
				.getSmsInclusiveOffers().getOffers().get(i).getHeader().getOfferingId())) {

			    offerSmsInclusive.add(responseupdated.getData().getSmsInclusiveOffers().getOffers().get(i));

			}
		    }
		}
		responseupdated.getData().getSmsInclusiveOffers().setOffers(offerSmsInclusive);
	    }
	    ArrayList<SupplementryOfferingsData> offerInternetInclusive = new ArrayList<SupplementryOfferingsData>();
	    if (responseupdated.getData().getInternetInclusiveOffers() != null
		    && responseupdated.getData().getInternetInclusiveOffers().getOffers() != null
		    && !responseupdated.getData().getInternetInclusiveOffers().getOffers().isEmpty()
		    && responseupdated.getData().getInternetInclusiveOffers().getOffers().size() > 0) {
		for (int i = 0; i < responseupdated.getData().getInternetInclusiveOffers().getOffers().size(); i++) {
		    if (responseupdated.getData().getInternetInclusiveOffers().getOffers().get(i).getHeader() != null) {
			if (!supplementaryOfferingsRequest.getSpecialOfferIds().contains(responseupdated.getData()
				.getInternetInclusiveOffers().getOffers().get(i).getHeader().getOfferingId())) {

			    offeroaming.add(responseupdated.getData().getInternetInclusiveOffers().getOffers().get(i));

			}
		    }
		}
		responseupdated.getData().getInternetInclusiveOffers().setOffers(offerInternetInclusive);
	    }
	    ArrayList<SupplementryOfferingsData> offerVoiceInclusive = new ArrayList<SupplementryOfferingsData>();
	    if (responseupdated.getData().getVoiceInclusiveOffers() != null
		    && responseupdated.getData().getVoiceInclusiveOffers().getOffers() != null
		    && !responseupdated.getData().getVoiceInclusiveOffers().getOffers().isEmpty()
		    && responseupdated.getData().getVoiceInclusiveOffers().getOffers().size() > 0) {
		for (int i = 0; i < responseupdated.getData().getVoiceInclusiveOffers().getOffers().size(); i++) {
		    if (responseupdated.getData().getVoiceInclusiveOffers().getOffers().get(i).getHeader() != null) {
			if (!supplementaryOfferingsRequest.getSpecialOfferIds().contains(responseupdated.getData()
				.getVoiceInclusiveOffers().getOffers().get(i).getHeader().getOfferingId())) {

			    offerVoiceInclusive
				    .add(responseupdated.getData().getVoiceInclusiveOffers().getOffers().get(i));

			}
		    }
		}
		responseupdated.getData().getVoiceInclusiveOffers().setOffers(offerVoiceInclusive);
	    }
	} // END OF if(!msisdn.contains("="))
	  // END SETTING LOGIC TO ELIMINATE SPEICAL OFFERS FROM SUPPLEMENTARY
	return responseupdated;
    }

    public SupplementaryOfferingsResponse getSupplementaryOfferingsByIDs(String msisdn,
	    SupplementaryOfferingsByIDsRequest supplementaryOfferingsByIDsRequestst,
	    SupplementaryOfferingsResponse supplementaryOfferingsResponse) throws Exception {
	Utilities.printDebugLog(msisdn + "-Request received in " + Transactions.SUPPLEMENTARY_SERVICES_TRANSACTION_NAME
		+ " BY IDs BUSINESS with data-" + supplementaryOfferingsByIDsRequestst.toString(), logger);

	// Get key which will be used to store and retrieve data from hash map.
	// String supplementaryOfferingsCacheKey =
	// this.getKeyForCacheMySubscriptions(Constants.HASH_KEY_My_SUBSCRIPTIONS,
	// offeringId, supplementaryOfferingsByIDsRequestst);

	SupplementaryOfferingsResponseData resData = new SupplementaryOfferingsResponseData();
	RestClient rc = new RestClient();
	ObjectMapper mapper = new ObjectMapper();

	// supplementaryOfferingsByIDsRequestst.getOfferingIds().add("1376368122");
	String requestJsonESB = mapper.writeValueAsString(supplementaryOfferingsByIDsRequestst);

	// String path =
	// GetConfigurations.getESBRoute("getSupplementaryOfferingsByIDs");
	// String path =
	// "http://10.220.48.129/MAG_DEV01/rest/V1/api/mySubscriptionsOfferingIds";
	String path = GetConfigurations.getConfigurationFromCache("magento.app.supplementary.url");
	// Configur"http://10.220.48.211/stgbakcell/rest/V1/api/mySubscriptionsOfferingIds";

	Utilities.printDebugLog(msisdn + "-Request Call to MagentoSubscriptionIdsLog: ", logger);
	Utilities.printDebugLog(msisdn + "-Path- MagentoSubscriptionIdsLog" + path, logger);
	Utilities.printDebugLog(msisdn + "-Request Packet MagentoSubscriptionIdsLog-" + requestJsonESB, logger);

	supplementaryOfferingsResponse.getLogsReport().setEsbRequestTime(Utilities.getReportDateTime());
	String response = rc.getResponseFromESB(path, requestJsonESB);
	supplementaryOfferingsResponse.getLogsReport().setEsbResponseTime(Utilities.getReportDateTime());

	Utilities.printDebugLog(msisdn + "-Received response from Magento MagentoSubscriptionIdsLog-" + response,
		logger);

	if (response != null && !response.isEmpty()) {
	    // Converting String to JSON as String.
	    // response = Utilities.stringToJSONString(response);

	    // Logging ESB response code and description.
	    supplementaryOfferingsResponse.setLogsReport(Utilities.logESBParamsintoReportLog(requestJsonESB, response,
		    supplementaryOfferingsResponse.getLogsReport()));

	    // if (Utilities.getValueFromJSON(response,
	    // "returnCode").equalsIgnoreCase(Constants.ESB_SUCCESS_CODE)) {
	    if (Utilities.getValueFromJSON(response, "resultCode").equalsIgnoreCase("132")) {

		resData = ParseSupplementaryOfferingsResponse.parseSupplementaryOfferingsData(msisdn,
			Utilities.getValueFromJSON(response, "data"), resData);
		for (int i = 0; i < resData.getInternet().getOffers().size(); i++) {
		    GroupedOfferAttributes groupOffer = resData.getInternet().getOffers().get(i).getHeader()
			    .getOfferGroup();
		    if (groupOffer != null && groupOffer.getGroupName().equalsIgnoreCase("")
			    && groupOffer.getGroupValue().equalsIgnoreCase("")) {
			resData.getInternet().getOffers().get(i).getHeader().setOfferGroup(null);
		    }
		}
		supplementaryOfferingsResponse.setData(resData);

		// Storing response in hashmap.
		this.prepareCache(resData, supplementaryOfferingsByIDsRequestst);
		// Caching Time-stamp
		AppCache.getHashmapTimestamps().put(Constants.HASH_KEY_SUPPLEMENTARY_OFFERINGS,
			Utilities.getTimeStampForCache(Constants.HASH_KEY_SUPPLEMENTARY_OFFERINGS));

		supplementaryOfferingsResponse.setCallStatus(Constants.Call_Status_True);
		supplementaryOfferingsResponse.setResultCode(Constants.APP_SERVER_SUCCESS_CODE);
		supplementaryOfferingsResponse.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("success",
			supplementaryOfferingsByIDsRequestst.getLang()));

	    } else {
		/*
		 * This else is needed just to show empty cards on mobile by sending success APP
		 * SERVER code
		 */
		supplementaryOfferingsResponse.setData(resData);
		supplementaryOfferingsResponse.setCallStatus(Constants.Call_Status_True);
		supplementaryOfferingsResponse.setResultCode(Constants.APP_SERVER_SUCCESS_CODE);
		supplementaryOfferingsResponse.setResultDesc(Utilities.getErrorMessageFromFile("supplementary.offers",
			supplementaryOfferingsByIDsRequestst.getLang(),
			Utilities.getValueFromJSON(response, "resultCode")));
	    }

	} else {
	    supplementaryOfferingsResponse.setData(resData);
	    supplementaryOfferingsResponse.setCallStatus(Constants.Call_Status_False);
	    supplementaryOfferingsResponse.setResultCode(Constants.API_FAILURE_CODE);

	    supplementaryOfferingsResponse.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle(
		    "connectivity.error", supplementaryOfferingsByIDsRequestst.getLang()));
	}

	return supplementaryOfferingsResponse;
    }

    // for phase 2
    public SupplementaryOfferingsResponse getSupplementaryOfferingsByIDsV2(String msisdn,
	    SupplementaryOfferingsByIDsRequest supplementaryOfferingsByIDsRequestst,
	    SupplementaryOfferingsResponse supplementaryOfferingsResponse) throws Exception {
	Utilities.printDebugLog(msisdn + "-Request received in " + Transactions.SUPPLEMENTARY_SERVICES_TRANSACTION_NAME
		+ " BY IDs BUSINESS with data-" + supplementaryOfferingsByIDsRequestst.toString(), logger);

	// Get key which will be used to store and retrieve data from hash map.
	// String supplementaryOfferingsCacheKey =
	// this.getKeyForCacheMySubscriptions(Constants.HASH_KEY_My_SUBSCRIPTIONS,
	// offeringId, supplementaryOfferingsByIDsRequestst);

	SupplementaryOfferingsResponseData resData = new SupplementaryOfferingsResponseData();
	RestClient rc = new RestClient();
	ObjectMapper mapper = new ObjectMapper();

	// supplementaryOfferingsByIDsRequestst.getOfferingIds().add("1376368122");
	String requestJsonESB = mapper.writeValueAsString(supplementaryOfferingsByIDsRequestst);

	String path = GetConfigurations.getESBRoute("getSupplementaryOfferingsByIDsV2");
	// String path =
	// "http://10.220.48.129/MAG_DEV01/rest/V1/api/mySubscriptionsOfferingIds";
	// String path =
	// "http://10.220.48.211/stgbakcell/rest/V1/api/mySubscriptionsOfferingIds";

	Utilities.printDebugLog(msisdn + "-Request Call to ESB", logger);
	Utilities.printDebugLog(msisdn + "-Path-" + path, logger);
	Utilities.printDebugLog(msisdn + "-Request Packet-" + requestJsonESB, logger);

	supplementaryOfferingsResponse.getLogsReport().setEsbRequestTime(Utilities.getReportDateTime());
	String response = rc.getResponseFromESB(path, requestJsonESB);
	supplementaryOfferingsResponse.getLogsReport().setEsbResponseTime(Utilities.getReportDateTime());

	Utilities.printDebugLog(msisdn + "-Received response from ESB-" + response, logger);

	if (response != null && !response.isEmpty()) {
	    // Converting String to JSON as String.
	    // response = Utilities.stringToJSONString(response);

	    // Logging ESB response code and description.
	    supplementaryOfferingsResponse.setLogsReport(Utilities.logESBParamsintoReportLog(requestJsonESB, response,
		    supplementaryOfferingsResponse.getLogsReport()));

	    // if (Utilities.getValueFromJSON(response,
	    // "returnCode").equalsIgnoreCase(Constants.ESB_SUCCESS_CODE)) {
	    if (Utilities.getValueFromJSON(response, "returnCode").equalsIgnoreCase(Constants.ESB_SUCCESS_CODE)) {

		resData = ParseSupplementaryOfferingsResponse.parseSupplementaryOfferingsData(msisdn,
			Utilities.getValueFromJSON(response, "data"), resData);
		for (int i = 0; i < resData.getInternet().getOffers().size(); i++) {
		    GroupedOfferAttributes groupOffer = resData.getInternet().getOffers().get(i).getHeader()
			    .getOfferGroup();
		    if (groupOffer != null && groupOffer.getGroupName().equalsIgnoreCase("")
			    && groupOffer.getGroupValue().equalsIgnoreCase("")) {
			resData.getInternet().getOffers().get(i).getHeader().setOfferGroup(null);
		    }
		}
		supplementaryOfferingsResponse.setData(resData);

		// Storing response in hashmap.
		this.prepareCache(resData, supplementaryOfferingsByIDsRequestst);
		// Caching Time-stamp
		AppCache.getHashmapTimestamps().put(Constants.HASH_KEY_SUPPLEMENTARY_OFFERINGS,
			Utilities.getTimeStampForCache(Constants.HASH_KEY_SUPPLEMENTARY_OFFERINGS));

		supplementaryOfferingsResponse.setCallStatus(Constants.Call_Status_True);
		supplementaryOfferingsResponse.setResultCode(Constants.APP_SERVER_SUCCESS_CODE);
		supplementaryOfferingsResponse.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("success",
			supplementaryOfferingsByIDsRequestst.getLang()));

	    } else {
		/*
		 * This else is needed just to show empty cards on mobile by sending success APP
		 * SERVER code
		 */
		supplementaryOfferingsResponse.setData(resData);
		supplementaryOfferingsResponse.setCallStatus(Constants.Call_Status_True);
		supplementaryOfferingsResponse.setResultCode(Constants.APP_SERVER_SUCCESS_CODE);
		supplementaryOfferingsResponse.setResultDesc(Utilities.getErrorMessageFromFile("supplementary.offers",
			supplementaryOfferingsByIDsRequestst.getLang(),
			Utilities.getValueFromJSON(response, "resultCode")));
	    }

	} else {
	    supplementaryOfferingsResponse.setData(resData);
	    supplementaryOfferingsResponse.setCallStatus(Constants.Call_Status_False);
	    supplementaryOfferingsResponse.setResultCode(Constants.API_FAILURE_CODE);

	    supplementaryOfferingsResponse.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle(
		    "connectivity.error", supplementaryOfferingsByIDsRequestst.getLang()));
	}

	return supplementaryOfferingsResponse;
    }

    private void prepareCache(SupplementaryOfferingsResponseData resData, SupplementaryOfferingsByIDsRequest lang) {

	ObjectMapper mapper = new ObjectMapper();

	for (int i = 0; i < resData.getCall().getOffers().size(); i++) {
	    AppCache.getHashmapMySubscriptions().put(this.getKeyForCacheMySubscriptions(
		    resData.getCall().getOffers().get(i).getHeader().getOfferingId(), lang), resData);
	}
	for (int i = 0; i < resData.getCampaign().getOffers().size(); i++) {
	    AppCache.getHashmapMySubscriptions().put(this.getKeyForCacheMySubscriptions(
		    resData.getCampaign().getOffers().get(i).getHeader().getOfferingId(), lang), resData);
	}
	for (int i = 0; i < resData.getHybrid().getOffers().size(); i++) {
	    AppCache.getHashmapMySubscriptions().put(this.getKeyForCacheMySubscriptions(
		    resData.getHybrid().getOffers().get(i).getHeader().getOfferingId(), lang), resData);
	}
	for (int i = 0; i < resData.getInternet().getOffers().size(); i++) {

	    AppCache.getHashmapMySubscriptions().put(this.getKeyForCacheMySubscriptions(
		    resData.getInternet().getOffers().get(i).getHeader().getOfferingId(), lang), resData);
	}
	for (int i = 0; i < resData.getSms().getOffers().size(); i++) {
	    AppCache.getHashmapMySubscriptions().put(this.getKeyForCacheMySubscriptions(
		    resData.getSms().getOffers().get(i).getHeader().getOfferingId(), lang), resData);
	}
	for (int i = 0; i < resData.getRoaming().getOffers().size(); i++) {
	    AppCache.getHashmapMySubscriptions().put(this.getKeyForCacheMySubscriptions(
		    resData.getRoaming().getOffers().get(i).getHeader().getOfferingId(), lang), resData);
	}
	for (int i = 0; i < resData.getTm().getOffers().size(); i++) {
	    AppCache.getHashmapMySubscriptions().put(this.getKeyForCacheMySubscriptions(
		    resData.getTm().getOffers().get(i).getHeader().getOfferingId(), lang), resData);
	}

    }

    public ChangeSupplementaryOfferingsResponse changeSupplementaryOffering(String msisdn,
	    ChangeSupplementaryOfferingsRequest changeSupplementaryOfferingRequest,
	    ChangeSupplementaryOfferingsResponse changeSupplementaryOfferingResponse) throws Exception {
	Utilities.printDebugLog(
		msisdn + "-Request received in " + Transactions.CHANGE_SUPPLIMENTRY_OFFERING_TRANSACTION_NAME
			+ " BUSINESS with data-" + changeSupplementaryOfferingRequest.toString(),
		logger);

	MySubscriptionsBusiness mySubscriptionsBusiness = new MySubscriptionsBusiness();
	MySubscriptionsRequest mySubscriptionsRequest = new MySubscriptionsRequest();
	SupplementaryOfferingsResponse mySubscriptionsResponse = new SupplementaryOfferingsResponse();

	// Preparing request for My Subscriptions.
	mySubscriptionsRequest.setChannel(changeSupplementaryOfferingRequest.getChannel());
	mySubscriptionsRequest.setiP(changeSupplementaryOfferingRequest.getiP());
	mySubscriptionsRequest.setLang(changeSupplementaryOfferingRequest.getLang());
	mySubscriptionsRequest.setMsisdn(changeSupplementaryOfferingRequest.getMsisdn());
	mySubscriptionsRequest.setOfferingName(changeSupplementaryOfferingRequest.getOfferingName());

	ChangeSupplementaryOfferingsResponseData resData = new ChangeSupplementaryOfferingsResponseData();
	RestClient rc = new RestClient();
	ObjectMapper mapper = new ObjectMapper();

	String requestJsonESB = mapper.writeValueAsString(changeSupplementaryOfferingRequest);

	requestJsonESB = Utilities.removeParamsFromJSONObject(requestJsonESB, "offerName");

	String path = GetConfigurations.getESBRoute("changeSupplementaryOffer");
	Utilities.printDebugLog(msisdn + "-Request Call to ESB", logger);
	Utilities.printDebugLog(msisdn + "-Path-" + path, logger);
	Utilities.printDebugLog(msisdn + "-Request Packet-" + requestJsonESB, logger);

	changeSupplementaryOfferingResponse.getLogsReport().setEsbRequestTime(Utilities.getReportDateTime());
	String response = rc.getResponseFromESB(path, requestJsonESB);
	changeSupplementaryOfferingResponse.getLogsReport().setEsbResponseTime(Utilities.getReportDateTime());

	Utilities.printDebugLog(msisdn + "-Received response from ESB-" + response, logger);

	if (response != null && !response.isEmpty()) {
	    // Logging ESB response code and description.
	    changeSupplementaryOfferingResponse.setLogsReport(Utilities.logESBParamsintoReportLog(requestJsonESB,
		    response, changeSupplementaryOfferingResponse.getLogsReport()));

	    if (Utilities.getValueFromJSON(response, "returnCode").equalsIgnoreCase(Constants.ESB_SUCCESS_CODE)) {

		resData.setMessage(GetMessagesMappings.getMessageFromResourceBundle("success",
			changeSupplementaryOfferingRequest.getLang()));
		mySubscriptionsResponse = mySubscriptionsBusiness.getSubscriptions(msisdn, mySubscriptionsRequest,
			mySubscriptionsResponse);
		resData.setMySubscriptionsData(mySubscriptionsResponse.getData());

		changeSupplementaryOfferingResponse.setData(resData);
		changeSupplementaryOfferingResponse.setCallStatus(Constants.Call_Status_True);
		changeSupplementaryOfferingResponse.setResultCode(Constants.APP_SERVER_SUCCESS_CODE);
		changeSupplementaryOfferingResponse.setResultDesc(GetMessagesMappings
			.getMessageFromResourceBundle("success", changeSupplementaryOfferingRequest.getLang()));

	    } else {

		changeSupplementaryOfferingResponse.setCallStatus(Constants.Call_Status_False);
		changeSupplementaryOfferingResponse.setResultCode(Utilities.getValueFromJSON(response, "returnCode"));
		changeSupplementaryOfferingResponse.setResultDesc(Utilities.getErrorMessageFromFile(
			"change.supplementary.offer", changeSupplementaryOfferingRequest.getLang(),
			Utilities.getValueFromJSON(response, "returnCode")));
	    }
	} else {

	    changeSupplementaryOfferingResponse.setCallStatus(Constants.Call_Status_False);
	    changeSupplementaryOfferingResponse.setResultCode(Constants.API_FAILURE_CODE);
	    changeSupplementaryOfferingResponse.setResultDesc(GetMessagesMappings
		    .getMessageFromResourceBundle("connectivity.error", changeSupplementaryOfferingRequest.getLang()));
	}

	return changeSupplementaryOfferingResponse;

    }

    public ChangeSupplementaryOfferingsResponsebulk changeSupplementaryOfferingbulk(String msisdn,
	    ChangeSupplementaryOfferingsRequestBulk changeSupplementaryOfferingRequest,
	    ChangeSupplementaryOfferingsResponsebulk changeSupplementaryOfferingResponse) throws Exception {
	Utilities.printDebugLog(
		msisdn + "-Request received in " + Transactions.CHANGE_SUPPLIMENTRY_OFFERING_BULK_TRANSACTION_NAME
			+ " BUSINESS with data-" + changeSupplementaryOfferingRequest.toString(),
		logger);

	ChangeSupplementaryOfferingsResponseDatabulk resData = new ChangeSupplementaryOfferingsResponseDatabulk();
	RestClient rc = new RestClient();
	ObjectMapper mapper = new ObjectMapper();

	String requestJsonESB = mapper.writeValueAsString(changeSupplementaryOfferingRequest);

	requestJsonESB = Utilities.removeParamsFromJSONObject(requestJsonESB, "offerName");

	String path = GetConfigurations.getESBRoute("insertorders");
	Utilities.printDebugLog(msisdn + "-Request Call to ESB", logger);
	Utilities.printDebugLog(msisdn + "-Path-" + path, logger);
	Utilities.printDebugLog(msisdn + "-Request Packet-" + requestJsonESB, logger);

	changeSupplementaryOfferingResponse.getLogsReport().setEsbRequestTime(Utilities.getReportDateTime());
	String response = rc.getResponseFromESB(path, requestJsonESB);
	changeSupplementaryOfferingResponse.getLogsReport().setEsbResponseTime(Utilities.getReportDateTime());

	Utilities.printDebugLog(msisdn + "-Received response from ESB-" + response, logger);

	if (response != null && !response.isEmpty()) {
	    // Logging ESB response code and description.
	    changeSupplementaryOfferingResponse.setLogsReport(Utilities.logESBParamsintoReportLog(requestJsonESB,
		    response, changeSupplementaryOfferingResponse.getLogsReport()));

	    if (Utilities.getValueFromJSON(response, "returnCode").equalsIgnoreCase(Constants.ESB_SUCCESS_CODE)) {

		resData.setMessage(GetMessagesMappings.getMessageFromResourceBundle("success",
			changeSupplementaryOfferingRequest.getLang()));

		changeSupplementaryOfferingResponse.setData(resData);
		changeSupplementaryOfferingResponse.setCallStatus(Constants.Call_Status_True);
		changeSupplementaryOfferingResponse.setResultCode(Constants.APP_SERVER_SUCCESS_CODE);
		changeSupplementaryOfferingResponse.setResultDesc(GetMessagesMappings
			.getMessageFromResourceBundle("success", changeSupplementaryOfferingRequest.getLang()));

	    } else {

		changeSupplementaryOfferingResponse.setCallStatus(Constants.Call_Status_False);
		changeSupplementaryOfferingResponse.setResultCode(Utilities.getValueFromJSON(response, "returnCode"));
		changeSupplementaryOfferingResponse.setResultDesc(Utilities.getErrorMessageFromFile(
			"change.supplementary.offer", changeSupplementaryOfferingRequest.getLang(),
			Utilities.getValueFromJSON(response, "returnCode")));
	    }
	} else {

	    changeSupplementaryOfferingResponse.setCallStatus(Constants.Call_Status_False);
	    changeSupplementaryOfferingResponse.setResultCode(Constants.API_FAILURE_CODE);
	    changeSupplementaryOfferingResponse.setResultDesc(GetMessagesMappings
		    .getMessageFromResourceBundle("connectivity.error", changeSupplementaryOfferingRequest.getLang()));
	}

	return changeSupplementaryOfferingResponse;

    }

    // START OF Specials Offers Business
    public SpecialOffersResponse getSpecialOffersBusiness(String msisdn, SpecialOffersRequest specialsOffersRequest,
	    SpecialOffersResponse specialOfffersResponse) throws Exception {
	Utilities.printDebugLog(msisdn + "-Request received in " + Transactions.SUPPLEMENTARY_SERVICES_TRANSACTION_NAME
		+ " BUSINESS with data-" + specialsOffersRequest.toString(), logger);
	ObjectMapper mapper = new ObjectMapper();

	SupplementaryOfferingsResponseData resData = new SupplementaryOfferingsResponseData();
	RestClient rc = new RestClient();
	String requestJsonESB = mapper.writeValueAsString(specialsOffersRequest);

	SupplementaryOfferingsRequest supplementaryOfferingsRequest = new SupplementaryOfferingsRequest();
	SupplementaryOfferingsResponse supplementaryOfferingsResponse = new SupplementaryOfferingsResponse();

	supplementaryOfferingsRequest.setOfferingName(specialsOffersRequest.getOfferingName());
	supplementaryOfferingsRequest.setChannel(specialsOffersRequest.getChannel());
	supplementaryOfferingsRequest.setiP(specialsOffersRequest.getiP());
	supplementaryOfferingsRequest.setLang(specialsOffersRequest.getLang());
	supplementaryOfferingsRequest.setMsisdn(specialsOffersRequest.getMsisdn());

	SupplementaryOfferingsResponse responseFronNormalSuppplementary = getSupplementaryOfferings(msisdn+"=",
			supplementaryOfferingsRequest, supplementaryOfferingsResponse);
			Utilities.printDebugLog(msisdn+ "-cloneResponse After Return: " +mapper.writeValueAsString(responseFronNormalSuppplementary),logger);

			SupplementaryOfferingsResponse getSupplementaryResponse = null;
			getSupplementaryResponse=(SupplementaryOfferingsResponse) responseFronNormalSuppplementary.clone();
			
			
	if (getSupplementaryResponse != null && getSupplementaryResponse.getData() != null) {
	    // Converting String to JSON as String.
	    // response = Utilities.stringToJSONString(response);

	    // Logging ESB response code and description.
	    String response = mapper.writeValueAsString(getSupplementaryResponse);
	    specialOfffersResponse.setLogsReport(Utilities.logESBParamsintoReportLog(requestJsonESB, response,
		    specialOfffersResponse.getLogsReport()));

	    if (getSupplementaryResponse.getResultCode().equals(Constants.APP_SERVER_SUCCESS_CODE)) {

		/*
		 * if (AppCache.getHashmapMigrationPricesMessages() == null ||
		 * AppCache.getHashmapMigrationPricesMessages().isEmpty()) {
		 * Utilities.printDebugLog(msisdn + "-MigrationCache null so updating From DB",
		 * logger); HashMap<String, MigrationPrices> buildMigrationPricesCache =
		 * GetConfigurations
		 * .getMigrationPrices(msisdn,"Offer",specialsOffersRequest.getGroupIds());
		 * 
		 * }
		 */
		SpecialOffersResponseData specialData = new SpecialOffersResponseData();
		Special specials = new Special();

		ArrayList<String> specialIdsFromRequest = new ArrayList<String>();
		specialIdsFromRequest = specialsOffersRequest.getSpecialOfferIds();
		ArrayList<String> updatedSpecialOfferingIds = new ArrayList<String>();
		ArrayList<SupplementryOfferingsData> listOfferingData = new ArrayList<SupplementryOfferingsData>();

		listOfferingData = specialOffersgettingmissing(msisdn, listOfferingData, getSupplementaryResponse,
			specialsOffersRequest, updatedSpecialOfferingIds);
		Utilities.printDebugLog(
			msisdn + "Found missing BforeCall: " + specialsOffersRequest.getSpecialOfferIds(), logger);
		boolean foundOfferingIdscheck = specialIdsFromRequest.removeAll(updatedSpecialOfferingIds);
		if (foundOfferingIdscheck || (specialsOffersRequest.getSpecialOfferIds().size() > 0
			&& updatedSpecialOfferingIds.size() < 1)) {
		    if (specialsOffersRequest.getSpecialOfferIds().size() > 0 && updatedSpecialOfferingIds.size() < 1) {
			specialIdsFromRequest = specialsOffersRequest.getSpecialOfferIds();
			Utilities.printDebugLog(msisdn + "Found missing OFferingIDs SetOriginal"
				+ specialsOffersRequest.getSpecialOfferIds(), logger);
		    } else

		    {
			Utilities.printDebugLog(msisdn + "Found missing OFferingIDs Original"
				+ specialsOffersRequest.getSpecialOfferIds(), logger);
			Utilities.printDebugLog(
				msisdn + "Found missing OFferingIDs MADENEw " + updatedSpecialOfferingIds, logger);
			Utilities.printDebugLog(
				msisdn + "Found missing OFferingIDs TREU and ids are" + specialIdsFromRequest, logger);
		    }

		    SupplementaryOfferingsByIDsRequest supplementaryOfferingIdsRequest = new SupplementaryOfferingsByIDsRequest();
		    SupplementaryOfferingsResponse supplementaryOfferingsIDsResponse = new SupplementaryOfferingsResponse();

		    supplementaryOfferingIdsRequest.setChannel(specialsOffersRequest.getChannel());
		    supplementaryOfferingIdsRequest.setiP(specialsOffersRequest.getiP());
		    supplementaryOfferingIdsRequest.setLang(specialsOffersRequest.getLang());
		    supplementaryOfferingIdsRequest.setMsisdn(specialsOffersRequest.getMsisdn());
		    supplementaryOfferingIdsRequest.setOfferingIds(specialIdsFromRequest);
		    SupplementaryOfferingsResponse resp = getSupplementaryOfferingsByIDs(
			    specialsOffersRequest.getMsisdn(), supplementaryOfferingIdsRequest,
			    supplementaryOfferingsIDsResponse);
		    Utilities.printDebugLog(
			    msisdn + "Found missing MissinsgResponse: " + mapper.writeValueAsString(resp), logger);
		    specialOffersgettingmissing(msisdn, listOfferingData, resp, specialsOffersRequest,
			    updatedSpecialOfferingIds);

		}
		specials.setOffers(listOfferingData);

		specialData.setSpecial(specials);
		specialData.setTm(getSupplementaryResponse.getData().getTm());
		specialOfffersResponse.setData(specialData);

		specialOfffersResponse.setCallStatus(Constants.Call_Status_True);
		specialOfffersResponse.setResultCode(Constants.APP_SERVER_SUCCESS_CODE);
		specialOfffersResponse.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("success",
			supplementaryOfferingsRequest.getLang()));
		return specialOfffersResponse;
	    } else {
		specialOfffersResponse.setCallStatus(Constants.Call_Status_False);
		specialOfffersResponse.setResultCode(getSupplementaryResponse.getResultCode());
		specialOfffersResponse.setResultDesc(Utilities.getErrorMessageFromFile("supplementary.offers",
			specialsOffersRequest.getLang(), getSupplementaryResponse.getResultCode()));
		return specialOfffersResponse;
	    }
	} else {

	    specialOfffersResponse.setCallStatus(Constants.Call_Status_False);
	    specialOfffersResponse.setResultCode(Constants.API_FAILURE_CODE);

	    specialOfffersResponse.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("connectivity.error",
		    supplementaryOfferingsRequest.getLang()));
	    return specialOfffersResponse;
	}

    }

    // END of Special Offers Business

    public ArrayList<SupplementryOfferingsData> specialOffersgettingmissing(String msisdn,
	    ArrayList<SupplementryOfferingsData> listOfferingData,
	    SupplementaryOfferingsResponse getSupplementaryResponse, SpecialOffersRequest specialsOffersRequest,
	    ArrayList<String> updatedSpecialOfferingIds) {
	try {
	    if (getSupplementaryResponse.getData().getCall() != null
		    && getSupplementaryResponse.getData().getCall().getOffers() != null
		    && !getSupplementaryResponse.getData().getCall().getOffers().isEmpty()
		    && getSupplementaryResponse.getData().getCall().getOffers().size() > 0) {
		for (int i = 0; i < getSupplementaryResponse.getData().getCall().getOffers().size(); i++) {
		    if (getSupplementaryResponse.getData().getCall().getOffers().get(i).getHeader() != null) {
			if (specialsOffersRequest.getSpecialOfferIds().contains(getSupplementaryResponse.getData()
				.getCall().getOffers().get(i).getHeader().getOfferingId())) {

			    updatedSpecialOfferingIds.add(getSupplementaryResponse.getData().getCall().getOffers()
				    .get(i).getHeader().getOfferingId());
			    SupplementryOfferingsData dataOfferings = new SupplementryOfferingsData();
			    dataOfferings = (getSupplementaryResponse.getData().getCall().getOffers().get(i));
			    /*
			     * if (AppCache.getHashmapMigrationPricesMessages().containsKey(
			     * getSupplementaryResponse
			     * .getData().getCall().getOffers().get(i).getHeader().getOfferingId())) {
			     * MigrationPrices valuesFromCache =
			     * AppCache.getHashmapMigrationPricesMessages()
			     * .get(getSupplementaryResponse.getData().getCall().getOffers().get(i)
			     * .getHeader().getOfferingId());
			     * 
			     * String mrcFinal= GetConfigurations
			     * .getMigrationPrices(msisdn,"Offer",specialsOffersRequest.getGroupIds());
			     * 
			     * dataOfferings.getHeader().setPrice(valuesFromCache.getMrc()); }
			     */
			    String mrcFinal = GetConfigurations.getMigrationPrices(msisdn, "Offer",
				    specialsOffersRequest.getGroupIds(), getSupplementaryResponse.getData().getCall()
					    .getOffers().get(i).getHeader().getOfferingId());
			    Utilities.printDebugLog(
				    msisdn + "READING Migration Prices FOR KEY FROM DB- MRC FINAL" + mrcFinal, logger);
			    if (mrcFinal != null && !mrcFinal.isEmpty()) {
				dataOfferings.getHeader().setPrice(mrcFinal);
			    } else {
				dataOfferings.getHeader().setPrice("");
			    }

			    listOfferingData.add(dataOfferings);
			}
		    }
		}
	    }
	    if (getSupplementaryResponse.getData().getSms() != null
		    && getSupplementaryResponse.getData().getSms().getOffers() != null
		    && !getSupplementaryResponse.getData().getSms().getOffers().isEmpty()
		    && getSupplementaryResponse.getData().getSms().getOffers().size() > 0) {
		for (int i = 0; i < getSupplementaryResponse.getData().getSms().getOffers().size(); i++) {
		    if (getSupplementaryResponse.getData().getSms().getOffers().get(i).getHeader() != null) {
			if (specialsOffersRequest.getSpecialOfferIds().contains(getSupplementaryResponse.getData()
				.getSms().getOffers().get(i).getHeader().getOfferingId())) {

			    updatedSpecialOfferingIds.add(getSupplementaryResponse.getData().getSms().getOffers().get(i)
				    .getHeader().getOfferingId());

			    SupplementryOfferingsData dataOfferings = new SupplementryOfferingsData();
			    dataOfferings = (getSupplementaryResponse.getData().getSms().getOffers().get(i));
			    /*
			     * if (AppCache.getHashmapMigrationPricesMessages().containsKey(
			     * getSupplementaryResponse
			     * .getData().getSms().getOffers().get(i).getHeader().getOfferingId())) {
			     * MigrationPrices valuesFromCache =
			     * AppCache.getHashmapMigrationPricesMessages()
			     * .get(getSupplementaryResponse.getData().getSms().getOffers().get(i)
			     * .getHeader().getOfferingId());
			     * dataOfferings.getHeader().setPrice(valuesFromCache.getMrc()); }
			     */

			    String mrcFinal = GetConfigurations.getMigrationPrices(msisdn, "Offer",
				    specialsOffersRequest.getGroupIds(), getSupplementaryResponse.getData().getSms()
					    .getOffers().get(i).getHeader().getOfferingId());
			    Utilities.printDebugLog(
				    msisdn + "READING Migration Prices FOR KEY FROM DB- MRC FINAL" + mrcFinal, logger);
			    if (mrcFinal != null && !mrcFinal.isEmpty()) {
				dataOfferings.getHeader().setPrice(mrcFinal);
			    } else {
				dataOfferings.getHeader().setPrice("");
			    }

			    listOfferingData.add(dataOfferings);
			}
		    }
		}
	    }
	    if (getSupplementaryResponse.getData().getInternet() != null
		    && getSupplementaryResponse.getData().getInternet().getOffers() != null
		    && !getSupplementaryResponse.getData().getInternet().getOffers().isEmpty()
		    && getSupplementaryResponse.getData().getInternet().getOffers().size() > 0) {
		for (int i = 0; i < getSupplementaryResponse.getData().getInternet().getOffers().size(); i++) {
		    if (getSupplementaryResponse.getData().getInternet().getOffers().get(i).getHeader() != null) {
			if (specialsOffersRequest.getSpecialOfferIds().contains(getSupplementaryResponse.getData()
				.getInternet().getOffers().get(i).getHeader().getOfferingId())) {

			    updatedSpecialOfferingIds.add(getSupplementaryResponse.getData().getInternet().getOffers()
				    .get(i).getHeader().getOfferingId());

			    SupplementryOfferingsData dataOfferings = new SupplementryOfferingsData();
			    dataOfferings = (getSupplementaryResponse.getData().getInternet().getOffers().get(i));
			    /*
			     * if (AppCache.getHashmapMigrationPricesMessages().containsKey(
			     * getSupplementaryResponse
			     * .getData().getInternet().getOffers().get(i).getHeader().getOfferingId())) {
			     * MigrationPrices valuesFromCache =
			     * AppCache.getHashmapMigrationPricesMessages()
			     * .get(getSupplementaryResponse.getData().getInternet().getOffers().get(i)
			     * .getHeader().getOfferingId());
			     * dataOfferings.getHeader().setPrice(valuesFromCache.getMrc()); }
			     */String mrcFinal = GetConfigurations.getMigrationPrices(msisdn, "Offer",
				    specialsOffersRequest.getGroupIds(), getSupplementaryResponse.getData()
					    .getInternet().getOffers().get(i).getHeader().getOfferingId());
			    Utilities.printDebugLog(
				    msisdn + "READING Migration Prices FOR KEY FROM DB- MRC FINAL" + mrcFinal, logger);
			    if (mrcFinal != null && !mrcFinal.isEmpty()) {
				dataOfferings.getHeader().setPrice(mrcFinal);
			    } else {
				dataOfferings.getHeader().setPrice("");
			    }
			    listOfferingData.add(dataOfferings);
			}
		    }
		}
	    }
	    if (getSupplementaryResponse.getData().getHybrid() != null
		    && getSupplementaryResponse.getData().getHybrid().getOffers() != null
		    && !getSupplementaryResponse.getData().getHybrid().getOffers().isEmpty()
		    && getSupplementaryResponse.getData().getHybrid().getOffers().size() > 0) {
		for (int i = 0; i < getSupplementaryResponse.getData().getHybrid().getOffers().size(); i++) {
		    if (getSupplementaryResponse.getData().getHybrid().getOffers().get(i).getHeader() != null) {
			if (specialsOffersRequest.getSpecialOfferIds().contains(getSupplementaryResponse.getData()
				.getHybrid().getOffers().get(i).getHeader().getOfferingId())) {

			    updatedSpecialOfferingIds.add(getSupplementaryResponse.getData().getHybrid().getOffers()
				    .get(i).getHeader().getOfferingId());

			    SupplementryOfferingsData dataOfferings = new SupplementryOfferingsData();
			    dataOfferings = (getSupplementaryResponse.getData().getHybrid().getOffers().get(i));
			    /*
			     * if (AppCache.getHashmapMigrationPricesMessages().containsKey(
			     * getSupplementaryResponse
			     * .getData().getHybrid().getOffers().get(i).getHeader().getOfferingId())) {
			     * MigrationPrices valuesFromCache =
			     * AppCache.getHashmapMigrationPricesMessages()
			     * .get(getSupplementaryResponse.getData().getHybrid().getOffers().get(i)
			     * .getHeader().getOfferingId());
			     * dataOfferings.getHeader().setPrice(valuesFromCache.getMrc()); }
			     */
			    String mrcFinal = GetConfigurations.getMigrationPrices(msisdn, "Offer",
				    specialsOffersRequest.getGroupIds(), getSupplementaryResponse.getData().getHybrid()
					    .getOffers().get(i).getHeader().getOfferingId());
			    Utilities.printDebugLog(
				    msisdn + "READING Migration Prices FOR KEY FROM DB- MRC FINAL" + mrcFinal, logger);
			    if (mrcFinal != null && !mrcFinal.isEmpty()) {
				dataOfferings.getHeader().setPrice(mrcFinal);
			    } else {
				dataOfferings.getHeader().setPrice("");
			    }

			    listOfferingData.add(dataOfferings);
			}
		    }
		}
	    }
	    if (getSupplementaryResponse.getData().getCampaign() != null
		    && getSupplementaryResponse.getData().getCampaign().getOffers() != null
		    && !getSupplementaryResponse.getData().getCampaign().getOffers().isEmpty()
		    && getSupplementaryResponse.getData().getCampaign().getOffers().size() > 0) {
		for (int i = 0; i < getSupplementaryResponse.getData().getCampaign().getOffers().size(); i++) {
		    if (getSupplementaryResponse.getData().getCampaign().getOffers().get(i).getHeader() != null) {
			if (specialsOffersRequest.getSpecialOfferIds().contains(getSupplementaryResponse.getData()
				.getCampaign().getOffers().get(i).getHeader().getOfferingId())) {

			    updatedSpecialOfferingIds.add(getSupplementaryResponse.getData().getCampaign().getOffers()
				    .get(i).getHeader().getOfferingId());
			    SupplementryOfferingsData dataOfferings = new SupplementryOfferingsData();
			    dataOfferings = (getSupplementaryResponse.getData().getCampaign().getOffers().get(i));
			    /*
			     * if (AppCache.getHashmapMigrationPricesMessages().containsKey(
			     * getSupplementaryResponse
			     * .getData().getCampaign().getOffers().get(i).getHeader().getOfferingId())) {
			     * MigrationPrices valuesFromCache =
			     * AppCache.getHashmapMigrationPricesMessages()
			     * .get(getSupplementaryResponse.getData().getCampaign().getOffers().get(i)
			     * .getHeader().getOfferingId());
			     * dataOfferings.getHeader().setPrice(valuesFromCache.getMrc()); }
			     */String mrcFinal = GetConfigurations.getMigrationPrices(msisdn, "Offer",
				    specialsOffersRequest.getGroupIds(), getSupplementaryResponse.getData()
					    .getCampaign().getOffers().get(i).getHeader().getOfferingId());
			    Utilities.printDebugLog(
				    msisdn + "READING Migration Prices FOR KEY FROM DB- MRC FINAL" + mrcFinal, logger);
			    if (mrcFinal != null && !mrcFinal.isEmpty()) {
				dataOfferings.getHeader().setPrice(mrcFinal);
			    } else {
				dataOfferings.getHeader().setPrice("");
			    }
			    listOfferingData.add(dataOfferings);
			}
		    }
		}
	    }
	    if (getSupplementaryResponse.getData().getRoaming() != null
		    && getSupplementaryResponse.getData().getRoaming().getOffers() != null
		    && !getSupplementaryResponse.getData().getRoaming().getOffers().isEmpty()
		    && getSupplementaryResponse.getData().getRoaming().getOffers().size() > 0) {
		for (int i = 0; i < getSupplementaryResponse.getData().getRoaming().getOffers().size(); i++) {
		    if (getSupplementaryResponse.getData().getRoaming().getOffers().get(i).getHeader() != null) {
			if (specialsOffersRequest.getSpecialOfferIds().contains(getSupplementaryResponse.getData()
				.getRoaming().getOffers().get(i).getHeader().getOfferingId())) {

			    updatedSpecialOfferingIds.add(getSupplementaryResponse.getData().getRoaming().getOffers()
				    .get(i).getHeader().getOfferingId());

			    SupplementryOfferingsData dataOfferings = new SupplementryOfferingsData();
			    dataOfferings = (getSupplementaryResponse.getData().getRoaming().getOffers().get(i));
			    /*
			     * if (AppCache.getHashmapMigrationPricesMessages().containsKey(
			     * getSupplementaryResponse
			     * .getData().getRoaming().getOffers().get(i).getHeader().getOfferingId())) {
			     * MigrationPrices valuesFromCache =
			     * AppCache.getHashmapMigrationPricesMessages()
			     * .get(getSupplementaryResponse.getData().getRoaming().getOffers().get(i)
			     * .getHeader().getOfferingId());
			     * dataOfferings.getHeader().setPrice(valuesFromCache.getMrc()); }
			     */String mrcFinal = GetConfigurations.getMigrationPrices(msisdn, "Offer",
				    specialsOffersRequest.getGroupIds(), getSupplementaryResponse.getData().getRoaming()
					    .getOffers().get(i).getHeader().getOfferingId());
			    Utilities.printDebugLog(
				    msisdn + "READING Migration Prices FOR KEY FROM DB- MRC FINAL" + mrcFinal, logger);
			    if (mrcFinal != null && !mrcFinal.isEmpty()) {
				dataOfferings.getHeader().setPrice(mrcFinal);
			    } else {
				dataOfferings.getHeader().setPrice("");
			    }
			    listOfferingData.add(dataOfferings);
			}
		    }
		}
	    }
	    if (getSupplementaryResponse.getData().getInternetInclusiveOffers() != null
		    && getSupplementaryResponse.getData().getInternetInclusiveOffers().getOffers() != null
		    && !getSupplementaryResponse.getData().getInternetInclusiveOffers().getOffers().isEmpty()
		    && getSupplementaryResponse.getData().getInternetInclusiveOffers().getOffers().size() > 0) {
		for (int i = 0; i < getSupplementaryResponse.getData().getInternetInclusiveOffers().getOffers()
			.size(); i++) {
		    if (getSupplementaryResponse.getData().getInternetInclusiveOffers().getOffers().get(i)
			    .getHeader() != null) {
			if (specialsOffersRequest.getSpecialOfferIds().contains(getSupplementaryResponse.getData()
				.getInternetInclusiveOffers().getOffers().get(i).getHeader().getOfferingId())) {

			    updatedSpecialOfferingIds.add(getSupplementaryResponse.getData()
				    .getInternetInclusiveOffers().getOffers().get(i).getHeader().getOfferingId());

			    SupplementryOfferingsData dataOfferings = new SupplementryOfferingsData();
			    dataOfferings = (getSupplementaryResponse.getData().getInternetInclusiveOffers().getOffers()
				    .get(i));
			    /*
			     * if (AppCache.getHashmapMigrationPricesMessages()
			     * .containsKey(getSupplementaryResponse.getData().getInternetInclusiveOffers()
			     * .getOffers().get(i).getHeader().getOfferingId())) { MigrationPrices
			     * valuesFromCache = AppCache.getHashmapMigrationPricesMessages()
			     * .get(getSupplementaryResponse.getData().getInternetInclusiveOffers()
			     * .getOffers().get(i).getHeader().getOfferingId());
			     * dataOfferings.getHeader().setPrice(valuesFromCache.getMrc()); } else
			     */ String mrcFinal = GetConfigurations.getMigrationPrices(msisdn, "Offer",
				    specialsOffersRequest.getGroupIds(),
				    getSupplementaryResponse.getData().getInternetInclusiveOffers().getOffers().get(i)
					    .getHeader().getOfferingId());
			    Utilities.printDebugLog(
				    msisdn + "READING Migration Prices FOR KEY FROM DB- MRC FINAL" + mrcFinal, logger);
			    if (mrcFinal != null && !mrcFinal.isEmpty()) {
				dataOfferings.getHeader().setPrice(mrcFinal);
			    } else {
				dataOfferings.getHeader().setPrice("");
			    }
			    listOfferingData.add(dataOfferings);
			}
		    }
		}
	    }
	    if (getSupplementaryResponse.getData().getSmsInclusiveOffers() != null
		    && getSupplementaryResponse.getData().getSmsInclusiveOffers().getOffers() != null
		    && !getSupplementaryResponse.getData().getSmsInclusiveOffers().getOffers().isEmpty()
		    && getSupplementaryResponse.getData().getSmsInclusiveOffers().getOffers().size() > 0) {
		for (int i = 0; i < getSupplementaryResponse.getData().getSmsInclusiveOffers().getOffers()
			.size(); i++) {
		    if (getSupplementaryResponse.getData().getSmsInclusiveOffers().getOffers().get(i)
			    .getHeader() != null) {
			if (specialsOffersRequest.getSpecialOfferIds().contains(getSupplementaryResponse.getData()
				.getSmsInclusiveOffers().getOffers().get(i).getHeader().getOfferingId())) {

			    updatedSpecialOfferingIds.add(getSupplementaryResponse.getData().getSmsInclusiveOffers()
				    .getOffers().get(i).getHeader().getOfferingId());

			    SupplementryOfferingsData dataOfferings = new SupplementryOfferingsData();
			    dataOfferings = (getSupplementaryResponse.getData().getSmsInclusiveOffers().getOffers()
				    .get(i));
			    /*
			     * if (AppCache.getHashmapMigrationPricesMessages()
			     * .containsKey(getSupplementaryResponse.getData().getSmsInclusiveOffers()
			     * .getOffers().get(i).getHeader().getOfferingId())) { MigrationPrices
			     * valuesFromCache = AppCache.getHashmapMigrationPricesMessages()
			     * .get(getSupplementaryResponse.getData().getSmsInclusiveOffers().getOffers()
			     * .get(i).getHeader().getOfferingId());
			     * dataOfferings.getHeader().setPrice(valuesFromCache.getMrc()); }
			     */ String mrcFinal = GetConfigurations.getMigrationPrices(msisdn, "Offer",
				    specialsOffersRequest.getGroupIds(), getSupplementaryResponse.getData()
					    .getSmsInclusiveOffers().getOffers().get(i).getHeader().getOfferingId());
			    Utilities.printDebugLog(
				    msisdn + "READING Migration Prices FOR KEY FROM DB- MRC FINAL" + mrcFinal, logger);
			    if (mrcFinal != null && !mrcFinal.isEmpty()) {
				dataOfferings.getHeader().setPrice(mrcFinal);
			    } else {
				dataOfferings.getHeader().setPrice("");
			    }
			    listOfferingData.add(dataOfferings);
			}
		    }
		}
	    }
	} catch (Exception e) {

	}
	return listOfferingData;
    }

    /*
     * public GetCrmSubscriberEsbResponse subscriberResponse() {
     * GetCrmSubscriberEsbResponse resp=new GetCrmSubscriberEsbResponse();
     * RestClient rc = new RestClient();
     * 
     * String requestJsonESB =
     * mapper.writeValueAsString(supplementaryOfferingsRequest); String path =
     * GetConfigurations.getESBRoute("getcategories");
     * 
     * Utilities.printDebugLog(msisdn + "-Request Call to ESB", logger);
     * Utilities.printDebugLog(msisdn + "-Path-" + path, logger);
     * Utilities.printDebugLog(msisdn + "-Request Packet-" + requestJsonESB,
     * logger); getCategoreisResponse.getLogsReport().setEsbRequestTime(Utilities.
     * getReportDateTime()); String response = rc.getResponseFromESB(path,
     * requestJsonESB); return resp; }
     */
    private String getKeyForCache(String hashKeySupplementaryOfferings,
	    SupplementaryOfferingsRequest supplementaryOfferingsRequest) {
	return hashKeySupplementaryOfferings + "." + supplementaryOfferingsRequest.getOfferingName() + "."
		+ supplementaryOfferingsRequest.getLang();
    }

    private String getKeyForCacheMySubscriptions(String offeringID,
	    SupplementaryOfferingsByIDsRequest supplementaryOfferingsByIDsRequest) {
	return Constants.HASH_KEY_My_SUBSCRIPTIONS + "." + offeringID + "."
		+ supplementaryOfferingsByIDsRequest.getLang();
    }

    public static boolean hasValue(String value) {
	try {
	    if (value != null && !value.equalsIgnoreCase("null") && !value.equalsIgnoreCase("")) {
		return true;
	    } else {
		return false;
	    }
	} catch (Exception e) {
	    return false;
	}
    }

    public SupplementaryOfferingsResponse ManipulateResponseForHideVisibleAllowedChecks(String msisdn,
	    SupplementaryOfferingsResponse responseupdated,
	    SupplementaryOfferingsResponse supplementaryOfferingsResponse,
	    SupplementaryOfferingsRequest supplementaryOfferingsRequest) {
	try {
	    ObjectMapper mapper = new ObjectMapper();
	    CustomerInfoResponseData custInfoResponse = new CustomerInfoResponseData();
	    CustomerInfoRequest customerInfoRequest = new CustomerInfoRequest();

	    customerInfoRequest.setChannel(supplementaryOfferingsRequest.getChannel());
	    customerInfoRequest.setiP(supplementaryOfferingsRequest.getiP());
	    customerInfoRequest.setLang(supplementaryOfferingsRequest.getLang());
	    customerInfoRequest.setMsisdn(supplementaryOfferingsRequest.getMsisdn());

	    Utilities.printInfoLog(msisdn + "-Received From customerDataInternalCall SuplemntraryResponseBeforeMatch-"
		    + mapper.writeValueAsString(supplementaryOfferingsResponse), logger);

	    CustomerServicesBusiness customerBusiness = new CustomerServicesBusiness();
	    custInfoResponse = customerBusiness.getCustomerData(msisdn, "", customerInfoRequest, custInfoResponse,
		    Constants.TOKEN_CREATION_CALL_TYPE_EXTERNAL, "", "");

	    if (custInfoResponse.getResultCode().equalsIgnoreCase(Constants.ESB_SUCCESS_CODE)) {

		ArrayList<String> offeringId = new ArrayList<String>();

		for (int i = 0; i < custInfoResponse.getSupplementaryOfferingList().size(); i++) {
		    offeringId.add(custInfoResponse.getSupplementaryOfferingList().get(i).getOfferingId());
		}
		Utilities.printDebugLog(msisdn + "-Received From customerDataInternalLogic OfferingIdS-" + offeringId,
			logger);

		// STRT OF LOGI RELATED TO HIDE FOR AND VISIBLE FOR

		// START TM LOGIC TO CHECK VISIBLE FOR HIDE FOR
		if (supplementaryOfferingsResponse.getData() != null
			&& supplementaryOfferingsResponse.getData().getTm() != null
			&& supplementaryOfferingsResponse.getData().getTm().getOffers() != null) {
		    Utilities.printDebugLog(
			    msisdn + "-Received From customerDataInternalLogic TM OFFERS YES found-"
				    + mapper.writeValueAsString(supplementaryOfferingsResponse.getData().getTm()),
			    logger);
		    ArrayList<SupplementryOfferingsData> offers = new ArrayList<SupplementryOfferingsData>();
		    for (int j = 0; j < supplementaryOfferingsResponse.getData().getTm().getOffers().size(); j++) {
			Utilities.printDebugLog(msisdn + "-Received From customerDataInternalLogic  VisibleFOR_FromTM-"
				+ supplementaryOfferingsResponse.getData().getTm().getOffers().get(j).getHeader()
					.getVisibleFor(),
				logger);
			Utilities.printDebugLog(msisdn + "-Received From customerDataInternalLogic  HisdeFOR_FromTM-"
				+ supplementaryOfferingsResponse.getData().getTm().getOffers().get(j).getHeader()
					.getHideFor(),
				logger);

			// START OF ALoowd for and VISIBLE FOR HIDE FOR CHECKS
			if (offeringId.contains(supplementaryOfferingsResponse.getData().getTm().getOffers().get(j)
				.getHeader().getOfferingId())) {
			    supplementaryOfferingsResponse.getData().getTm().getOffers().get(j).getHeader()
				    .setIsAlreadySusbcribed("true");
			    if (supplementaryOfferingsResponse.getData().getTm().getOffers().get(j).getHeader()
				    .getBtnRenew().equalsIgnoreCase("1")) {
				String allowedFor = supplementaryOfferingsResponse.getData().getTm().getOffers().get(j)
					.getHeader().getAllowedForRenew();

				List<String> AllowedFor = new ArrayList<String>(Arrays.asList(allowedFor.split(",")));
				if (!CollectionUtils.intersection(offeringId, AllowedFor).isEmpty()) {
				    supplementaryOfferingsResponse.getData().getTm().getOffers().get(j).getHeader()
					    .setBtnRenew("1");
				} else {
				    supplementaryOfferingsResponse.getData().getTm().getOffers().get(j).getHeader()
					    .setBtnRenew("0");
				}
			    }
			} else {
			    supplementaryOfferingsResponse.getData().getTm().getOffers().get(j).getHeader()
				    .setIsAlreadySusbcribed("false");
			}

			List<String> visibleForArrrayAfterConversion = new ArrayList<String>();
			if (hasValue(supplementaryOfferingsResponse.getData().getTm().getOffers().get(j).getHeader()
				.getVisibleFor())) {
			    Utilities.printDebugLog(
				    msisdn + "TM CHECK VISIBLE FOR IF NOT NULL  :" + supplementaryOfferingsResponse
					    .getData().getTm().getOffers().get(j).getHeader().getVisibleFor(),
				    logger);
			    String[] visibleForArrayConverting = supplementaryOfferingsResponse.getData().getTm()
				    .getOffers().get(j).getHeader().getVisibleFor().split(",");
			    visibleForArrrayAfterConversion = Arrays.asList(visibleForArrayConverting);

			}

			List<String> HideForArrrayAfterConversion = new ArrayList<String>();
			if (hasValue(supplementaryOfferingsResponse.getData().getTm().getOffers().get(j).getHeader()
				.getHideFor())) {
			    Utilities
				    .printDebugLog(
					    msisdn + "TM CHECK HIDE FOR NOT NULL :" + supplementaryOfferingsResponse
						    .getData().getTm().getOffers().get(j).getHeader().getHideFor(),
					    logger);
			    String[] HideForArrayConverting = supplementaryOfferingsResponse.getData().getTm()
				    .getOffers().get(j).getHeader().getHideFor().split(",");
			    HideForArrrayAfterConversion = Arrays.asList(HideForArrayConverting);
			}

			Utilities.printDebugLog(msisdn + "-"
				+ supplementaryOfferingsResponse.getData().getTm().getOffers().get(j).getHeader()
				+ "- TM CHECK Received From customerDataInternalLogic  ArrayListConversion VisibleFOR_FromInternet-"
				+ visibleForArrrayAfterConversion, logger);
			Utilities.printDebugLog(msisdn + "-"
				+ supplementaryOfferingsResponse.getData().getTm().getOffers().get(j).getHeader()
				+ "- TM CHECK Received From customerDataInternalLogic  ArrayListConversion HisdeFOR_FromInternet-"
				+ HideForArrrayAfterConversion, logger);

			if (visibleForArrrayAfterConversion.size() < 1 && HideForArrrayAfterConversion.size() < 1) {

			    Utilities.printDebugLog(msisdn
				    + "----------------TM CHECK VISIBLE AND HIDE FOR ARRAYLISTS NULL BOTH ELSE--------"
				    + visibleForArrrayAfterConversion, logger);
			    offers.add(supplementaryOfferingsResponse.getData().getTm().getOffers().get(j));

			} else {
			    Utilities.printDebugLog(msisdn
				    + "----------------TM CHECK VISIBLE AND HIDE FOR ARRALISTS NOT NULL IF--------"
				    + visibleForArrrayAfterConversion, logger);

			    boolean isflag = false;
			    if (visibleForArrrayAfterConversion != null && !visibleForArrrayAfterConversion.isEmpty()
				    && !CollectionUtils.intersection(offeringId, visibleForArrrayAfterConversion)
					    .isEmpty()) {

				Utilities.printDebugLog(msisdn + "-"
					+ supplementaryOfferingsResponse.getData().getTm().getOffers().get(j)
						.getHeader().getOfferingId()
					+ "-TM CHECK Received From customerDataInternalLogic Matched VisibleFOR_FromInternet YES-"
					+ visibleForArrrayAfterConversion, logger);
				isflag = true;
				offers.add(supplementaryOfferingsResponse.getData().getTm().getOffers().get(j));

			    } else if (HideForArrrayAfterConversion != null && !HideForArrrayAfterConversion.isEmpty()
				    && !CollectionUtils.intersection(offeringId, HideForArrrayAfterConversion)
					    .isEmpty()) {
				Utilities.printDebugLog(msisdn + "-"
					+ supplementaryOfferingsResponse.getData().getTm().getOffers().get(j)
						.getHeader().getOfferingId()
					+ "-TM CHECK Received From customerDataInternalLogic Matched hideFOR_FromInternetYES-"
					+ HideForArrrayAfterConversion, logger);

			    }

			    else {
				Utilities.printDebugLog(msisdn + "-"
					+ supplementaryOfferingsResponse.getData().getTm().getOffers().get(j)
						.getHeader().getOfferingId()
					+ "-TM CHECK Received From customerDataInternalLogic NOT FOUND IN HIDEFOR :-"
					+ supplementaryOfferingsResponse.getData().getTm().getOffers().get(j)
						.getHeader().getOfferingId(),
					logger);
				if ((!CollectionUtils.intersection(offeringId, HideForArrrayAfterConversion)
					.isEmpty())) {
				    // hide mt daalo
				} else if (CollectionUtils.intersection(offeringId, HideForArrrayAfterConversion)
					.isEmpty() && visibleForArrrayAfterConversion.size() == 0) {
				    offers.add(supplementaryOfferingsResponse.getData().getTm().getOffers().get(j));
				} else if ((!CollectionUtils.intersection(offeringId, visibleForArrrayAfterConversion)
					.isEmpty())) {
				    offers.add(supplementaryOfferingsResponse.getData().getTm().getOffers().get(j));
				}
			    }
			}

			// END OF OF ALoowd for and VISIBLE FOR HIDE FOR CHECKS
		    }

		    // responseupdated =
		    responseupdated.getData().getTm().setOffers(offers);

		} else {
		    ArrayList<SupplementryOfferingsData> offers = new ArrayList<SupplementryOfferingsData>();
		    responseupdated.getData().getTm().setOffers(offers);
		}
		// END OF TM LOGIC TO CHECK VISIBLE FOR HIDE FOR

		// START SMS LOGIC TO CHECK VISIBLE FOR HIDE FOR
		if (supplementaryOfferingsResponse.getData() != null
			&& supplementaryOfferingsResponse.getData().getSms() != null
			&& supplementaryOfferingsResponse.getData().getSms().getOffers() != null) {
		    ArrayList<SupplementryOfferingsData> offers = new ArrayList<SupplementryOfferingsData>();
		    Utilities.printDebugLog(
			    msisdn + "-Received From customerDataInternalLogic SMS OFFERS YES found-"
				    + mapper.writeValueAsString(supplementaryOfferingsResponse.getData().getSms()),
			    logger);
		    for (int j = 0; j < supplementaryOfferingsResponse.getData().getSms().getOffers().size(); j++) {
			Utilities.printDebugLog(msisdn + "-Received From customerDataInternalLogic  VisibleFOR_FromSMS-"
				+ supplementaryOfferingsResponse.getData().getSms().getOffers().get(j).getHeader()
					.getVisibleFor(),
				logger);
			Utilities.printDebugLog(msisdn + "-Received From customerDataInternalLogic  HisdeFOR_FromSMS-"
				+ supplementaryOfferingsResponse.getData().getSms().getOffers().get(j).getHeader()
					.getHideFor(),
				logger);
			// START OF ALoowd for and VISIBLE FOR HIDE FOR CHECKS
			if (offeringId.contains(supplementaryOfferingsResponse.getData().getSms().getOffers().get(j)
				.getHeader().getOfferingId())) {
			    supplementaryOfferingsResponse.getData().getSms().getOffers().get(j).getHeader()
				    .setIsAlreadySusbcribed("true");
			    if (supplementaryOfferingsResponse.getData().getSms().getOffers().get(j).getHeader()
				    .getBtnRenew().equalsIgnoreCase("1")) {
				String allowedFor = supplementaryOfferingsResponse.getData().getSms().getOffers().get(j)
					.getHeader().getAllowedForRenew();

				List<String> AllowedFor = new ArrayList<String>(Arrays.asList(allowedFor.split(",")));
				if (!CollectionUtils.intersection(offeringId, AllowedFor).isEmpty()) {
				    supplementaryOfferingsResponse.getData().getSms().getOffers().get(j).getHeader()
					    .setBtnRenew("1");
				} else {
				    supplementaryOfferingsResponse.getData().getSms().getOffers().get(j).getHeader()
					    .setBtnRenew("0");
				}
			    }
			} else {
			    supplementaryOfferingsResponse.getData().getSms().getOffers().get(j).getHeader()
				    .setIsAlreadySusbcribed("false");
			}

			List<String> visibleForArrrayAfterConversion = new ArrayList<String>();
			if (hasValue(supplementaryOfferingsResponse.getData().getSms().getOffers().get(j).getHeader()
				.getVisibleFor())) {
			    Utilities.printDebugLog(
				    msisdn + "SMS CHECK VISIBLE FOR IF NOT NULL  :" + supplementaryOfferingsResponse
					    .getData().getSms().getOffers().get(j).getHeader().getVisibleFor(),
				    logger);
			    String[] visibleForArrayConverting = supplementaryOfferingsResponse.getData().getSms()
				    .getOffers().get(j).getHeader().getVisibleFor().split(",");
			    visibleForArrrayAfterConversion = Arrays.asList(visibleForArrayConverting);

			}

			List<String> HideForArrrayAfterConversion = new ArrayList<String>();
			if (hasValue(supplementaryOfferingsResponse.getData().getSms().getOffers().get(j).getHeader()
				.getHideFor())) {
			    Utilities
				    .printDebugLog(
					    msisdn + "SMS CHECK HIDE FOR NOT NULL :" + supplementaryOfferingsResponse
						    .getData().getSms().getOffers().get(j).getHeader().getHideFor(),
					    logger);
			    String[] HideForArrayConverting = supplementaryOfferingsResponse.getData().getSms()
				    .getOffers().get(j).getHeader().getHideFor().split(",");
			    HideForArrrayAfterConversion = Arrays.asList(HideForArrayConverting);
			}

			Utilities.printDebugLog(msisdn + "-"
				+ supplementaryOfferingsResponse.getData().getSms().getOffers().get(j).getHeader()
				+ "SMS CHECK Received From customerDataInternalLogic  ArrayListConversion VisibleFOR_FromInternet-"
				+ visibleForArrrayAfterConversion, logger);
			Utilities.printDebugLog(msisdn + "-"
				+ supplementaryOfferingsResponse.getData().getSms().getOffers().get(j).getHeader()
				+ "- SMS CHECK Received From customerDataInternalLogic  ArrayListConversion HisdeFOR_FromInternet-"
				+ HideForArrrayAfterConversion, logger);

			if (visibleForArrrayAfterConversion.size() < 1 && HideForArrrayAfterConversion.size() < 1) {

			    Utilities.printDebugLog(msisdn
				    + "----------------SMS CHECK VISIBLE AND HIDE FOR ARRAYLISTS NULL BOTH ELSE--------"
				    + visibleForArrrayAfterConversion, logger);
			    offers.add(supplementaryOfferingsResponse.getData().getSms().getOffers().get(j));

			} else {
			    Utilities.printDebugLog(msisdn
				    + "----------------SMS CHECK VISIBLE AND HIDE FOR ARRALISTS NOT NULL IF--------"
				    + visibleForArrrayAfterConversion, logger);

			    boolean isflag = false;
			    if (visibleForArrrayAfterConversion != null && !visibleForArrrayAfterConversion.isEmpty()
				    && !CollectionUtils.intersection(offeringId, visibleForArrrayAfterConversion)
					    .isEmpty()) {

				Utilities.printDebugLog(msisdn + "-"
					+ supplementaryOfferingsResponse.getData().getSms().getOffers().get(j)
						.getHeader().getOfferingId()
					+ "-SMS CHECK Received From customerDataInternalLogic Matched VisibleFOR_FromInternet YES-"
					+ visibleForArrrayAfterConversion, logger);
				isflag = true;
				offers.add(supplementaryOfferingsResponse.getData().getSms().getOffers().get(j));

			    } else if (HideForArrrayAfterConversion != null && !HideForArrrayAfterConversion.isEmpty()
				    && !CollectionUtils.intersection(offeringId, HideForArrrayAfterConversion)
					    .isEmpty()) {
				Utilities.printDebugLog(msisdn + "-"
					+ supplementaryOfferingsResponse.getData().getSms().getOffers().get(j)
						.getHeader().getOfferingId()
					+ "-SMS CHECK Received From customerDataInternalLogic Matched hideFOR_FromInternetYES-"
					+ HideForArrrayAfterConversion, logger);

			    }

			    else {
				Utilities.printDebugLog(msisdn + "-"
					+ supplementaryOfferingsResponse.getData().getSms().getOffers().get(j)
						.getHeader().getOfferingId()
					+ "-SMS CHECK Received From customerDataInternalLogic NOT FOUND IN HIDEFOR :-"
					+ supplementaryOfferingsResponse.getData().getSms().getOffers().get(j)
						.getHeader().getOfferingId(),
					logger);
				if ((!CollectionUtils.intersection(offeringId, HideForArrrayAfterConversion)
					.isEmpty())) {
				    // hide mt daalo
				} else if (CollectionUtils.intersection(offeringId, HideForArrrayAfterConversion)
					.isEmpty() && visibleForArrrayAfterConversion.size() == 0) {
				    offers.add(supplementaryOfferingsResponse.getData().getSms().getOffers().get(j));
				} else if ((!CollectionUtils.intersection(offeringId, visibleForArrrayAfterConversion)
					.isEmpty())) {
				    offers.add(supplementaryOfferingsResponse.getData().getSms().getOffers().get(j));
				}
			    }
			}

			// END OF OF ALoowd for and VISIBLE FOR HIDE FOR CHECKS
		    }

		    // responseupdated =
		    responseupdated.getData().getSms().setOffers(offers);

		} else {
		    ArrayList<SupplementryOfferingsData> offers = new ArrayList<SupplementryOfferingsData>();
		    responseupdated.getData().getSms().setOffers(offers);
		}
		// END OF SMS LOGIC TO CHECK VISIBLE FOR HIDE FOR

		// START CALL LOGIC TO CHECK VISIBLE FOR HIDE FOR
		if (supplementaryOfferingsResponse.getData() != null
			&& supplementaryOfferingsResponse.getData().getCall() != null
			&& supplementaryOfferingsResponse.getData().getCall().getOffers() != null) {
		    Utilities.printDebugLog(
			    msisdn + "-Received From customerDataInternalLogic CALL OFFERS YES found-"
				    + mapper.writeValueAsString(supplementaryOfferingsResponse.getData().getCall()),
			    logger);
		    ArrayList<SupplementryOfferingsData> offers = new ArrayList<SupplementryOfferingsData>();
		    for (int j = 0; j < supplementaryOfferingsResponse.getData().getCall().getOffers().size(); j++) {
			Utilities
				.printDebugLog(msisdn + "-Received From customerDataInternalLogic  VisibleFOR_FromCall-"
					+ supplementaryOfferingsResponse.getData().getCall().getOffers().get(j)
						.getHeader().getVisibleFor(),
					logger);
			Utilities.printDebugLog(msisdn + "-Received From customerDataInternalLogic  HisdeFOR_FromCall-"
				+ supplementaryOfferingsResponse.getData().getCall().getOffers().get(j).getHeader()
					.getHideFor(),
				logger);
			// START OF ALoowd for and VISIBLE FOR HIDE FOR CHECKS
			if (offeringId.contains(supplementaryOfferingsResponse.getData().getCall().getOffers().get(j)
				.getHeader().getOfferingId())) {
			    supplementaryOfferingsResponse.getData().getCall().getOffers().get(j).getHeader()
				    .setIsAlreadySusbcribed("true");
			    if (supplementaryOfferingsResponse.getData().getCall().getOffers().get(j).getHeader()
				    .getBtnRenew().equalsIgnoreCase("1")) {
				String allowedFor = supplementaryOfferingsResponse.getData().getCall().getOffers()
					.get(j).getHeader().getAllowedForRenew();

				List<String> AllowedFor = new ArrayList<String>(Arrays.asList(allowedFor.split(",")));
				if (!CollectionUtils.intersection(offeringId, AllowedFor).isEmpty()) {
				    supplementaryOfferingsResponse.getData().getCall().getOffers().get(j).getHeader()
					    .setBtnRenew("1");
				} else {
				    supplementaryOfferingsResponse.getData().getCall().getOffers().get(j).getHeader()
					    .setBtnRenew("0");
				}
			    }
			} else {
			    supplementaryOfferingsResponse.getData().getCall().getOffers().get(j).getHeader()
				    .setIsAlreadySusbcribed("false");
			}

			List<String> visibleForArrrayAfterConversion = new ArrayList<String>();
			if (hasValue(supplementaryOfferingsResponse.getData().getCall().getOffers().get(j).getHeader()
				.getVisibleFor())) {
			    Utilities.printDebugLog(
				    msisdn + "Call CHECK VISIBLE FOR IF NOT NULL  :" + supplementaryOfferingsResponse
					    .getData().getCall().getOffers().get(j).getHeader().getVisibleFor(),
				    logger);
			    String[] visibleForArrayConverting = supplementaryOfferingsResponse.getData().getCall()
				    .getOffers().get(j).getHeader().getVisibleFor().split(",");
			    visibleForArrrayAfterConversion = Arrays.asList(visibleForArrayConverting);

			}

			List<String> HideForArrrayAfterConversion = new ArrayList<String>();
			if (hasValue(supplementaryOfferingsResponse.getData().getCall().getOffers().get(j).getHeader()
				.getHideFor())) {
			    Utilities
				    .printDebugLog(
					    msisdn + "Call CHECK HIDE FOR NOT NULL :" + supplementaryOfferingsResponse
						    .getData().getCall().getOffers().get(j).getHeader().getHideFor(),
					    logger);
			    String[] HideForArrayConverting = supplementaryOfferingsResponse.getData().getCall()
				    .getOffers().get(j).getHeader().getHideFor().split(",");
			    HideForArrrayAfterConversion = Arrays.asList(HideForArrayConverting);
			}

			Utilities.printDebugLog(msisdn + "-"
				+ supplementaryOfferingsResponse.getData().getCall().getOffers().get(j).getHeader()
				+ "- Call CHECK Received From customerDataInternalLogic  ArrayListConversion VisibleFOR_FromInternet-"
				+ visibleForArrrayAfterConversion, logger);
			Utilities.printDebugLog(msisdn + "-"
				+ supplementaryOfferingsResponse.getData().getCall().getOffers().get(j).getHeader()
				+ "- Call CHECK Received From customerDataInternalLogic  ArrayListConversion HisdeFOR_FromInternet-"
				+ HideForArrrayAfterConversion, logger);

			if (visibleForArrrayAfterConversion.size() < 1 && HideForArrrayAfterConversion.size() < 1) {

			    Utilities.printDebugLog(msisdn
				    + "----------------Call CHECK VISIBLE AND HIDE FOR ARRAYLISTS NULL BOTH ELSE--------"
				    + visibleForArrrayAfterConversion, logger);
			    offers.add(supplementaryOfferingsResponse.getData().getCall().getOffers().get(j));

			} else {
			    Utilities.printDebugLog(msisdn
				    + "----------------Call CHECK VISIBLE AND HIDE FOR ARRALISTS NOT NULL IF--------"
				    + visibleForArrrayAfterConversion, logger);

			    boolean isflag = false;
			    if (visibleForArrrayAfterConversion != null && !visibleForArrrayAfterConversion.isEmpty()
				    && !CollectionUtils.intersection(offeringId, visibleForArrrayAfterConversion)
					    .isEmpty()) {

				Utilities.printDebugLog(msisdn + "-"
					+ supplementaryOfferingsResponse.getData().getCall().getOffers().get(j)
						.getHeader().getOfferingId()
					+ "-Call CHECK Received From customerDataInternalLogic Matched VisibleFOR_FromInternet YES-"
					+ visibleForArrrayAfterConversion, logger);
				isflag = true;
				offers.add(supplementaryOfferingsResponse.getData().getCall().getOffers().get(j));

			    } else if (HideForArrrayAfterConversion != null && !HideForArrrayAfterConversion.isEmpty()
				    && !CollectionUtils.intersection(offeringId, HideForArrrayAfterConversion)
					    .isEmpty()) {
				Utilities.printDebugLog(msisdn + "-"
					+ supplementaryOfferingsResponse.getData().getCall().getOffers().get(j)
						.getHeader().getOfferingId()
					+ "-Call CHECK Received From customerDataInternalLogic Matched hideFOR_FromInternetYES-"
					+ HideForArrrayAfterConversion, logger);

			    }

			    else {
				Utilities.printDebugLog(msisdn + "-"
					+ supplementaryOfferingsResponse.getData().getCall().getOffers().get(j)
						.getHeader().getOfferingId()
					+ "-Call CHECK Received From customerDataInternalLogic NOT FOUND IN HIDEFOR :-"
					+ supplementaryOfferingsResponse.getData().getCall().getOffers().get(j)
						.getHeader().getOfferingId(),
					logger);
				if ((!CollectionUtils.intersection(offeringId, HideForArrrayAfterConversion)
					.isEmpty())) {
				    // hide mt daalo
				} else if (CollectionUtils.intersection(offeringId, HideForArrrayAfterConversion)
					.isEmpty() && visibleForArrrayAfterConversion.size() == 0) {
				    offers.add(supplementaryOfferingsResponse.getData().getCall().getOffers().get(j));
				} else if ((!CollectionUtils.intersection(offeringId, visibleForArrrayAfterConversion)
					.isEmpty())) {
				    offers.add(supplementaryOfferingsResponse.getData().getCall().getOffers().get(j));
				}
			    }
			}

			// END OF OF ALoowd for and VISIBLE FOR HIDE FOR CHECKS

		    }

		    // responseupdated =
		    responseupdated.getData().getCall().setOffers(offers);

		} else {
		    ArrayList<SupplementryOfferingsData> offers = new ArrayList<SupplementryOfferingsData>();

		    responseupdated.getData().getCall().setOffers(offers);
		}
		// END OF CALL LOGIC TO CHECK VISIBLE FOR HIDE FOR
		// START INTERNET LOGIC TO CHECK VISIBLE FOR HIDE FOR
		if (supplementaryOfferingsResponse.getData() != null
			&& supplementaryOfferingsResponse.getData().getInternet() != null
			&& supplementaryOfferingsResponse.getData().getInternet().getOffers() != null) {
		    ArrayList<SupplementryOfferingsData> offers = new ArrayList<SupplementryOfferingsData>();
		    Utilities.printDebugLog(msisdn
			    + "-INTERNET CHECK Received From customerDataInternalLogic Internet OFFERS YES found-"
			    + mapper.writeValueAsString(supplementaryOfferingsResponse.getData().getInternet()),
			    logger);
		    Utilities.printDebugLog(msisdn + "-INTERNET CHECK OfferingIdsCUSTOEMRDATa-" + offeringId, logger);
		    for (int j = 0; j < supplementaryOfferingsResponse.getData().getInternet().getOffers()
			    .size(); j++) {
			Utilities.printDebugLog(msisdn + "-"
				+ supplementaryOfferingsResponse.getData().getInternet().getOffers().get(j).getHeader()
				+ "-INTERNET CHECK Received From customerDataInternalLogic  VisibleFOR_FromInternet-"
				+ supplementaryOfferingsResponse.getData().getInternet().getOffers().get(j).getHeader()
					.getVisibleFor(),
				logger);
			Utilities.printDebugLog(msisdn + "-"
				+ supplementaryOfferingsResponse.getData().getInternet().getOffers().get(j).getHeader()
				+ "-INTERNET CHECK Received From customerDataInternalLogic  HisdeFOR_FromInternet-"
				+ supplementaryOfferingsResponse.getData().getInternet().getOffers().get(j).getHeader()
					.getHideFor(),
				logger);

			Utilities.printDebugLog(msisdn
				+ "-INTERNET CHECK Received From customerDataInternalLogic  offeringId: " + offeringId,
				logger);

			Utilities.printDebugLog(
				msisdn + "-IINTERNET CHECK Received From customerDataInternalLogic AllowedFor: "
					+ supplementaryOfferingsResponse.getData().getInternet().getOffers().get(j)
						.getHeader().getAllowedForRenew(),
				logger);
			Utilities.printDebugLog(
				msisdn + "-INTERNET CHECK Received From customerDataInternalLogic btRenew: "
					+ supplementaryOfferingsResponse.getData().getInternet().getOffers().get(j)
						.getHeader().getBtnRenew(),
				logger);
			// START OF ALoowd for and VISIBLE FOR HIDE FOR CHECKS
			if (offeringId.contains(supplementaryOfferingsResponse.getData().getInternet().getOffers()
				.get(j).getHeader().getOfferingId())) {
			    supplementaryOfferingsResponse.getData().getInternet().getOffers().get(j).getHeader()
				    .setIsAlreadySusbcribed("true");
			    if (supplementaryOfferingsResponse.getData().getInternet().getOffers().get(j).getHeader()
				    .getBtnRenew().equalsIgnoreCase("1")) {
				String allowedFor = supplementaryOfferingsResponse.getData().getInternet().getOffers()
					.get(j).getHeader().getAllowedForRenew();

				List<String> AllowedFor = new ArrayList<String>(Arrays.asList(allowedFor.split(",")));
				if (!CollectionUtils.intersection(offeringId, AllowedFor).isEmpty()) {
				    supplementaryOfferingsResponse.getData().getInternet().getOffers().get(j)
					    .getHeader().setBtnRenew("1");
				} else {
				    supplementaryOfferingsResponse.getData().getInternet().getOffers().get(j)
					    .getHeader().setBtnRenew("0");
				}
			    }
			} else {
			    supplementaryOfferingsResponse.getData().getInternet().getOffers().get(j).getHeader()
				    .setIsAlreadySusbcribed("false");
			}

			List<String> visibleForArrrayAfterConversion = new ArrayList<String>();
			if (hasValue(supplementaryOfferingsResponse.getData().getInternet().getOffers().get(j)
				.getHeader().getVisibleFor())) {
			    Utilities.printDebugLog(msisdn + "INTERNET CHECK VISIBLE FOR IF NOT NULL  :"
				    + supplementaryOfferingsResponse.getData().getInternet().getOffers().get(j)
					    .getHeader().getVisibleFor(),
				    logger);
			    String[] visibleForArrayConverting = supplementaryOfferingsResponse.getData().getInternet()
				    .getOffers().get(j).getHeader().getVisibleFor().split(",");
			    visibleForArrrayAfterConversion = Arrays.asList(visibleForArrayConverting);

			}

			List<String> HideForArrrayAfterConversion = new ArrayList<String>();
			if (hasValue(supplementaryOfferingsResponse.getData().getInternet().getOffers().get(j)
				.getHeader().getHideFor())) {
			    Utilities.printDebugLog(
				    msisdn + "INTERNET CHECK HIDE FOR NOT NULL :" + supplementaryOfferingsResponse
					    .getData().getInternet().getOffers().get(j).getHeader().getHideFor(),
				    logger);
			    String[] HideForArrayConverting = supplementaryOfferingsResponse.getData().getInternet()
				    .getOffers().get(j).getHeader().getHideFor().split(",");
			    HideForArrrayAfterConversion = Arrays.asList(HideForArrayConverting);
			}

			Utilities.printDebugLog(msisdn + "-"
				+ supplementaryOfferingsResponse.getData().getInternet().getOffers().get(j).getHeader()
				+ "- INTERNET CHECK Received From customerDataInternalLogic  ArrayListConversion VisibleFOR_FromInternet-"
				+ visibleForArrrayAfterConversion, logger);
			Utilities.printDebugLog(msisdn + "-"
				+ supplementaryOfferingsResponse.getData().getInternet().getOffers().get(j).getHeader()
				+ "- INTERNET CHECK Received From customerDataInternalLogic  ArrayListConversion HisdeFOR_FromInternet-"
				+ HideForArrrayAfterConversion, logger);

			if (visibleForArrrayAfterConversion.size() < 1 && HideForArrrayAfterConversion.size() < 1) {

			    Utilities.printDebugLog(msisdn
				    + "----------------INTERNET CHECK VISIBLE AND HIDE FOR ARRAYLISTS NULL BOTH ELSE--------"
				    + visibleForArrrayAfterConversion, logger);
			    offers.add(supplementaryOfferingsResponse.getData().getInternet().getOffers().get(j));

			} else {
			    Utilities.printDebugLog(msisdn
				    + "----------------INTERNET CHECK VISIBLE AND HIDE FOR ARRALISTS NOT NULL IF--------"
				    + visibleForArrrayAfterConversion, logger);

			    boolean isflag = false;
			    if (visibleForArrrayAfterConversion != null && !visibleForArrrayAfterConversion.isEmpty()
				    && !CollectionUtils.intersection(offeringId, visibleForArrrayAfterConversion)
					    .isEmpty()) {

				Utilities.printDebugLog(msisdn + "-"
					+ supplementaryOfferingsResponse.getData().getInternet().getOffers().get(j)
						.getHeader().getOfferingId()
					+ "-INTERNET CHECK Received From customerDataInternalLogic Matched VisibleFOR_FromInternet YES-"
					+ visibleForArrrayAfterConversion, logger);
				isflag = true;
				offers.add(supplementaryOfferingsResponse.getData().getInternet().getOffers().get(j));

			    } else if (HideForArrrayAfterConversion != null && !HideForArrrayAfterConversion.isEmpty()
				    && !CollectionUtils.intersection(offeringId, HideForArrrayAfterConversion)
					    .isEmpty()) {
				Utilities.printDebugLog(msisdn + "-"
					+ supplementaryOfferingsResponse.getData().getInternet().getOffers().get(j)
						.getHeader().getOfferingId()
					+ "-INTERNET CHECK Received From customerDataInternalLogic Matched hideFOR_FromInternetYES-"
					+ HideForArrrayAfterConversion, logger);

			    }

			    else {
				Utilities.printDebugLog(msisdn + "-"
					+ supplementaryOfferingsResponse.getData().getInternet().getOffers().get(j)
						.getHeader().getOfferingId()
					+ "-INTERNET CHECK Received From customerDataInternalLogic NOT FOUND IN HIDEFOR :-"
					+ supplementaryOfferingsResponse.getData().getInternet().getOffers().get(j)
						.getHeader().getOfferingId(),
					logger);
				if ((!CollectionUtils.intersection(offeringId, HideForArrrayAfterConversion)
					.isEmpty())) {
				    // hide mt daalo
				} else if (CollectionUtils.intersection(offeringId, HideForArrrayAfterConversion)
					.isEmpty() && visibleForArrrayAfterConversion.size() == 0) {
				    offers.add(
					    supplementaryOfferingsResponse.getData().getInternet().getOffers().get(j));
				} else if ((!CollectionUtils.intersection(offeringId, visibleForArrrayAfterConversion)
					.isEmpty())) {
				    offers.add(
					    supplementaryOfferingsResponse.getData().getInternet().getOffers().get(j));
				}
			    }
			}

			// END OF OF ALoowd for and VISIBLE FOR HIDE FOR CHECKS
		    }

		    // responseupdated =
		    responseupdated.getData().getInternet().setOffers(offers);

		} else {
		    ArrayList<SupplementryOfferingsData> offers = new ArrayList<SupplementryOfferingsData>();
		    responseupdated.getData().getInternet().setOffers(offers);
		}
		// END OF INTERNET LOGIC TO CHECK VISIBLE FOR HIDE FOR
		// START ROAMING LOGIC TO CHECK VISIBLE FOR HIDE FOR
		if (supplementaryOfferingsResponse.getData() != null
			&& supplementaryOfferingsResponse.getData().getRoaming() != null
			&& supplementaryOfferingsResponse.getData().getRoaming().getOffers() != null) {
		    Utilities.printDebugLog(
			    msisdn + "-Received From customerDataInternalLogic Roaming OFFERS YES found-"
				    + mapper.writeValueAsString(supplementaryOfferingsResponse.getData().getRoaming()),
			    logger);
		    ArrayList<SupplementryOfferingsData> offers = new ArrayList<SupplementryOfferingsData>();
		    for (int j = 0; j < supplementaryOfferingsResponse.getData().getRoaming().getOffers().size(); j++) {
			Utilities.printDebugLog(
				msisdn + "-Received From customerDataInternalLogic  VisibleFOR_FromRoaming-"
					+ supplementaryOfferingsResponse.getData().getRoaming().getOffers().get(j)
						.getHeader().getVisibleFor(),
				logger);
			Utilities.printDebugLog(
				msisdn + "-Received From customerDataInternalLogic  HisdeFOR_FromgRoaming-"
					+ supplementaryOfferingsResponse.getData().getRoaming().getOffers().get(j)
						.getHeader().getHideFor(),
				logger);

			// START OF ALoowd for and VISIBLE FOR HIDE FOR CHECKS
			if (offeringId.contains(supplementaryOfferingsResponse.getData().getRoaming().getOffers().get(j)
				.getHeader().getOfferingId())) {
			    supplementaryOfferingsResponse.getData().getRoaming().getOffers().get(j).getHeader()
				    .setIsAlreadySusbcribed("true");
			    if (supplementaryOfferingsResponse.getData().getRoaming().getOffers().get(j).getHeader()
				    .getBtnRenew().equalsIgnoreCase("1")) {
				String allowedFor = supplementaryOfferingsResponse.getData().getRoaming().getOffers()
					.get(j).getHeader().getAllowedForRenew();

				List<String> AllowedFor = new ArrayList<String>(Arrays.asList(allowedFor.split(",")));
				if (!CollectionUtils.intersection(offeringId, AllowedFor).isEmpty()) {
				    supplementaryOfferingsResponse.getData().getRoaming().getOffers().get(j).getHeader()
					    .setBtnRenew("1");
				} else {
				    supplementaryOfferingsResponse.getData().getRoaming().getOffers().get(j).getHeader()
					    .setBtnRenew("0");
				}
			    }
			} else {
			    supplementaryOfferingsResponse.getData().getRoaming().getOffers().get(j).getHeader()
				    .setIsAlreadySusbcribed("false");
			}

			List<String> visibleForArrrayAfterConversion = new ArrayList<String>();
			if (hasValue(supplementaryOfferingsResponse.getData().getRoaming().getOffers().get(j)
				.getHeader().getVisibleFor())) {
			    Utilities.printDebugLog(
				    msisdn + "Roaming CHECK VISIBLE FOR IF NOT NULL  :" + supplementaryOfferingsResponse
					    .getData().getRoaming().getOffers().get(j).getHeader().getVisibleFor(),
				    logger);
			    String[] visibleForArrayConverting = supplementaryOfferingsResponse.getData().getRoaming()
				    .getOffers().get(j).getHeader().getVisibleFor().split(",");
			    visibleForArrrayAfterConversion = Arrays.asList(visibleForArrayConverting);

			}

			List<String> HideForArrrayAfterConversion = new ArrayList<String>();
			if (hasValue(supplementaryOfferingsResponse.getData().getRoaming().getOffers().get(j)
				.getHeader().getHideFor())) {
			    Utilities.printDebugLog(
				    msisdn + "Roaming CHECK HIDE FOR NOT NULL :" + supplementaryOfferingsResponse
					    .getData().getRoaming().getOffers().get(j).getHeader().getHideFor(),
				    logger);
			    String[] HideForArrayConverting = supplementaryOfferingsResponse.getData().getRoaming()
				    .getOffers().get(j).getHeader().getHideFor().split(",");
			    HideForArrrayAfterConversion = Arrays.asList(HideForArrayConverting);
			}

			Utilities.printDebugLog(msisdn + "-"
				+ supplementaryOfferingsResponse.getData().getRoaming().getOffers().get(j).getHeader()
				+ "- ROAMING CHECK Received From customerDataInternalLogic  ArrayListConversion VisibleFOR_FromInternet-"
				+ visibleForArrrayAfterConversion, logger);
			Utilities.printDebugLog(msisdn + "-"
				+ supplementaryOfferingsResponse.getData().getRoaming().getOffers().get(j).getHeader()
				+ "- ROAMING CHECK Received From customerDataInternalLogic  ArrayListConversion HisdeFOR_FromInternet-"
				+ HideForArrrayAfterConversion, logger);

			if (visibleForArrrayAfterConversion.size() < 1 && HideForArrrayAfterConversion.size() < 1) {

			    Utilities.printDebugLog(msisdn
				    + "----------------ROAMING CHECK VISIBLE AND HIDE FOR ARRAYLISTS NULL BOTH ELSE--------"
				    + visibleForArrrayAfterConversion, logger);
			    offers.add(supplementaryOfferingsResponse.getData().getRoaming().getOffers().get(j));

			} else {
			    Utilities.printDebugLog(msisdn
				    + "----------------ROAMING CHECK VISIBLE AND HIDE FOR ARRALISTS NOT NULL IF--------"
				    + visibleForArrrayAfterConversion, logger);

			    boolean isflag = false;
			    if (visibleForArrrayAfterConversion != null && !visibleForArrrayAfterConversion.isEmpty()
				    && !CollectionUtils.intersection(offeringId, visibleForArrrayAfterConversion)
					    .isEmpty()) {

				Utilities.printDebugLog(msisdn + "-"
					+ supplementaryOfferingsResponse.getData().getRoaming().getOffers().get(j)
						.getHeader().getOfferingId()
					+ "-ROAMING CHECK Received From customerDataInternalLogic Matched VisibleFOR_FromInternet YES-"
					+ visibleForArrrayAfterConversion, logger);
				isflag = true;
				offers.add(supplementaryOfferingsResponse.getData().getRoaming().getOffers().get(j));

			    } else if (HideForArrrayAfterConversion != null && !HideForArrrayAfterConversion.isEmpty()
				    && !CollectionUtils.intersection(offeringId, HideForArrrayAfterConversion)
					    .isEmpty()) {
				Utilities.printDebugLog(msisdn + "-"
					+ supplementaryOfferingsResponse.getData().getRoaming().getOffers().get(j)
						.getHeader().getOfferingId()
					+ "-ROAMING CHECK Received From customerDataInternalLogic Matched hideFOR_FromInternetYES-"
					+ HideForArrrayAfterConversion, logger);

			    }

			    else {
				Utilities.printDebugLog(msisdn + "-"
					+ supplementaryOfferingsResponse.getData().getRoaming().getOffers().get(j)
						.getHeader().getOfferingId()
					+ "-ROAMING CHECK Received From customerDataInternalLogic NOT FOUND IN HIDEFOR :-"
					+ supplementaryOfferingsResponse.getData().getRoaming().getOffers().get(j)
						.getHeader().getOfferingId(),
					logger);
				if ((!CollectionUtils.intersection(offeringId, HideForArrrayAfterConversion)
					.isEmpty())) {
				    // hide mt daalo
				} else if (CollectionUtils.intersection(offeringId, HideForArrrayAfterConversion)
					.isEmpty() && visibleForArrrayAfterConversion.size() == 0) {
				    offers.add(
					    supplementaryOfferingsResponse.getData().getRoaming().getOffers().get(j));
				} else if ((!CollectionUtils.intersection(offeringId, visibleForArrrayAfterConversion)
					.isEmpty())) {
				    offers.add(
					    supplementaryOfferingsResponse.getData().getRoaming().getOffers().get(j));
				}
			    }
			}

			// END OF OF ALoowd for and VISIBLE FOR HIDE FOR CHECKS

		    }

		    // responseupdated =
		    responseupdated.getData().getRoaming().setOffers(offers);

		} else {
		    ArrayList<SupplementryOfferingsData> offers = new ArrayList<SupplementryOfferingsData>();
		    responseupdated.getData().getRoaming().setOffers(offers);
		}

		responseupdated.getData().getRoaming()
			.setCountries(supplementaryOfferingsResponse.getData().getRoaming().getCountries());

		// END OF ROAMING LOGIC TO CHECK VISIBLE FOR HIDE FOR
		// START HYBRID LOGIC TO CHECK VISIBLE FOR HIDE FOR
		if (supplementaryOfferingsResponse.getData() != null
			&& supplementaryOfferingsResponse.getData().getHybrid() != null
			&& supplementaryOfferingsResponse.getData().getHybrid().getOffers() != null) {
		    Utilities.printDebugLog(
			    msisdn + "-Received From customerDataInternalLogic HYBRIDOFFERS YES found-"
				    + mapper.writeValueAsString(supplementaryOfferingsResponse.getData().getHybrid()),
			    logger);
		    ArrayList<SupplementryOfferingsData> offers = new ArrayList<SupplementryOfferingsData>();
		    for (int j = 0; j < supplementaryOfferingsResponse.getData().getHybrid().getOffers().size(); j++) {
			Utilities.printDebugLog(msisdn + "-Received From customerDataInternalLogic  VisibleFOR_HYBRID-"
				+ supplementaryOfferingsResponse.getData().getHybrid().getOffers().get(j).getHeader()
					.getVisibleFor(),
				logger);
			Utilities.printDebugLog(
				msisdn + "-Received From customerDataInternalLogic  HisdeFOR_FromgHYBRID-"
					+ supplementaryOfferingsResponse.getData().getHybrid().getOffers().get(j)
						.getHeader().getHideFor(),
				logger);

			// START OF ALoowd for and VISIBLE FOR HIDE FOR CHECKS
			if (offeringId.contains(supplementaryOfferingsResponse.getData().getHybrid().getOffers().get(j)
				.getHeader().getOfferingId())) {
			    supplementaryOfferingsResponse.getData().getHybrid().getOffers().get(j).getHeader()
				    .setIsAlreadySusbcribed("true");
			    if (supplementaryOfferingsResponse.getData().getHybrid().getOffers().get(j).getHeader()
				    .getBtnRenew().equalsIgnoreCase("1")) {
				String allowedFor = supplementaryOfferingsResponse.getData().getHybrid().getOffers()
					.get(j).getHeader().getAllowedForRenew();

				List<String> AllowedFor = new ArrayList<String>(Arrays.asList(allowedFor.split(",")));
				if (!CollectionUtils.intersection(offeringId, AllowedFor).isEmpty()) {
				    supplementaryOfferingsResponse.getData().getHybrid().getOffers().get(j).getHeader()
					    .setBtnRenew("1");
				} else {
				    supplementaryOfferingsResponse.getData().getHybrid().getOffers().get(j).getHeader()
					    .setBtnRenew("0");
				}
			    }
			} else {
			    supplementaryOfferingsResponse.getData().getHybrid().getOffers().get(j).getHeader()
				    .setIsAlreadySusbcribed("false");
			}

			List<String> visibleForArrrayAfterConversion = new ArrayList<String>();
			if (hasValue(supplementaryOfferingsResponse.getData().getHybrid().getOffers().get(j).getHeader()
				.getVisibleFor())) {
			    Utilities.printDebugLog(
				    msisdn + "Hybrid CHECK VISIBLE FOR IF NOT NULL  :" + supplementaryOfferingsResponse
					    .getData().getHybrid().getOffers().get(j).getHeader().getVisibleFor(),
				    logger);
			    String[] visibleForArrayConverting = supplementaryOfferingsResponse.getData().getHybrid()
				    .getOffers().get(j).getHeader().getVisibleFor().split(",");
			    visibleForArrrayAfterConversion = Arrays.asList(visibleForArrayConverting);

			}

			List<String> HideForArrrayAfterConversion = new ArrayList<String>();
			if (hasValue(supplementaryOfferingsResponse.getData().getHybrid().getOffers().get(j).getHeader()
				.getHideFor())) {
			    Utilities
				    .printDebugLog(
					    msisdn + "Hybrid CHECK HIDE FOR NOT NULL :" + supplementaryOfferingsResponse
						    .getData().getHybrid().getOffers().get(j).getHeader().getHideFor(),
					    logger);
			    String[] HideForArrayConverting = supplementaryOfferingsResponse.getData().getHybrid()
				    .getOffers().get(j).getHeader().getHideFor().split(",");
			    HideForArrrayAfterConversion = Arrays.asList(HideForArrayConverting);
			}

			Utilities.printDebugLog(msisdn + "-"
				+ supplementaryOfferingsResponse.getData().getHybrid().getOffers().get(j).getHeader()
				+ "- HYBRID CHECK Received From customerDataInternalLogic  ArrayListConversion VisibleFOR_FromInternet-"
				+ visibleForArrrayAfterConversion, logger);
			Utilities.printDebugLog(msisdn + "-"
				+ supplementaryOfferingsResponse.getData().getHybrid().getOffers().get(j).getHeader()
				+ "- HYBRID CHECK Received From customerDataInternalLogic  ArrayListConversion HisdeFOR_FromInternet-"
				+ HideForArrrayAfterConversion, logger);

			if (visibleForArrrayAfterConversion.size() < 1 && HideForArrrayAfterConversion.size() < 1) {

			    Utilities.printDebugLog(msisdn
				    + "----------------HYBRID CHECK VISIBLE AND HIDE FOR ARRAYLISTS NULL BOTH ELSE--------"
				    + visibleForArrrayAfterConversion, logger);
			    offers.add(supplementaryOfferingsResponse.getData().getHybrid().getOffers().get(j));

			} else {
			    Utilities.printDebugLog(msisdn
				    + "----------------HYBRID CHECK VISIBLE AND HIDE FOR ARRALISTS NOT NULL IF--------"
				    + visibleForArrrayAfterConversion, logger);

			    boolean isflag = false;
			    if (visibleForArrrayAfterConversion != null && !visibleForArrrayAfterConversion.isEmpty()
				    && !CollectionUtils.intersection(offeringId, visibleForArrrayAfterConversion)
					    .isEmpty()) {

				Utilities.printDebugLog(msisdn + "-"
					+ supplementaryOfferingsResponse.getData().getHybrid().getOffers().get(j)
						.getHeader().getOfferingId()
					+ "-HYBRID CHECK Received From customerDataInternalLogic Matched VisibleFOR_FromInternet YES-"
					+ visibleForArrrayAfterConversion, logger);
				isflag = true;
				offers.add(supplementaryOfferingsResponse.getData().getHybrid().getOffers().get(j));

			    } else if (HideForArrrayAfterConversion != null && !HideForArrrayAfterConversion.isEmpty()
				    && !CollectionUtils.intersection(offeringId, HideForArrrayAfterConversion)
					    .isEmpty()) {
				Utilities.printDebugLog(msisdn + "-"
					+ supplementaryOfferingsResponse.getData().getHybrid().getOffers().get(j)
						.getHeader().getOfferingId()
					+ "-HYBRID CHECK Received From customerDataInternalLogic Matched hideFOR_FromInternetYES-"
					+ HideForArrrayAfterConversion, logger);

			    }

			    else {
				Utilities.printDebugLog(msisdn + "-"
					+ supplementaryOfferingsResponse.getData().getHybrid().getOffers().get(j)
						.getHeader().getOfferingId()
					+ "-HYBRID CHECK Received From customerDataInternalLogic NOT FOUND IN HIDEFOR :-"
					+ supplementaryOfferingsResponse.getData().getHybrid().getOffers().get(j)
						.getHeader().getOfferingId(),
					logger);
				if ((!CollectionUtils.intersection(offeringId, HideForArrrayAfterConversion)
					.isEmpty())) {
				    // hide mt daalo
				} else if (CollectionUtils.intersection(offeringId, HideForArrrayAfterConversion)
					.isEmpty() && visibleForArrrayAfterConversion.size() == 0) {
				    offers.add(supplementaryOfferingsResponse.getData().getHybrid().getOffers().get(j));
				} else if ((!CollectionUtils.intersection(offeringId, visibleForArrrayAfterConversion)
					.isEmpty())) {
				    offers.add(supplementaryOfferingsResponse.getData().getHybrid().getOffers().get(j));
				}
			    }
			}

			// END OF OF ALoowd for and VISIBLE FOR HIDE FOR CHECKS

		    }

		    // responseupdated =
		    responseupdated.getData().getHybrid().setOffers(offers);

		} else {
		    ArrayList<SupplementryOfferingsData> offers = new ArrayList<SupplementryOfferingsData>();
		    responseupdated.getData().getHybrid().setOffers(offers);
		}
		// END OF HYBRID LOGIC TO CHECK VISIBLE FOR HIDE FOR

		// START InternetInclusiveOffers LOGIC TO CHECK VISIBLE FOR HIDE FOR
		if (supplementaryOfferingsResponse.getData() != null
			&& supplementaryOfferingsResponse.getData().getInternetInclusiveOffers() != null
			&& supplementaryOfferingsResponse.getData().getInternetInclusiveOffers().getOffers() != null) {
		    Utilities.printDebugLog(msisdn
			    + "-Received From customerDataInternalLogic InternetInclusiveOffers OFFERS YES found-"
			    + mapper.writeValueAsString(
				    supplementaryOfferingsResponse.getData().getInternetInclusiveOffers()),
			    logger);
		    ArrayList<SupplementryOfferingsData> offers = new ArrayList<SupplementryOfferingsData>();
		    for (int j = 0; j < supplementaryOfferingsResponse.getData().getInternetInclusiveOffers()
			    .getOffers().size(); j++) {
			Utilities.printDebugLog(msisdn
				+ "-Received From customerDataInternalLogic  VisibleFOR_FromInternetInclusiveOffers-"
				+ supplementaryOfferingsResponse.getData().getInternetInclusiveOffers().getOffers()
					.get(j).getHeader().getVisibleFor(),
				logger);
			Utilities.printDebugLog(msisdn
				+ "-Received From customerDataInternalLogic  HisdeFOR_FromgInternetInclusiveOffers-"
				+ supplementaryOfferingsResponse.getData().getInternetInclusiveOffers().getOffers()
					.get(j).getHeader().getHideFor(),
				logger);

			// START OF ALoowd for and VISIBLE FOR HIDE FOR CHECKS
			if (offeringId.contains(supplementaryOfferingsResponse.getData().getInternetInclusiveOffers()
				.getOffers().get(j).getHeader().getOfferingId())) {
			    supplementaryOfferingsResponse.getData().getInternetInclusiveOffers().getOffers().get(j)
				    .getHeader().setIsAlreadySusbcribed("true");
			    if (supplementaryOfferingsResponse.getData().getInternetInclusiveOffers().getOffers().get(j)
				    .getHeader().getBtnRenew().equalsIgnoreCase("1")) {
				String allowedFor = supplementaryOfferingsResponse.getData()
					.getInternetInclusiveOffers().getOffers().get(j).getHeader()
					.getAllowedForRenew();

				List<String> AllowedFor = new ArrayList<String>(Arrays.asList(allowedFor.split(",")));
				if (!CollectionUtils.intersection(offeringId, AllowedFor).isEmpty()) {
				    supplementaryOfferingsResponse.getData().getInternetInclusiveOffers().getOffers()
					    .get(j).getHeader().setBtnRenew("1");
				} else {
				    supplementaryOfferingsResponse.getData().getInternetInclusiveOffers().getOffers()
					    .get(j).getHeader().setBtnRenew("0");
				}
			    }
			} else {
			    supplementaryOfferingsResponse.getData().getInternetInclusiveOffers().getOffers().get(j)
				    .getHeader().setIsAlreadySusbcribed("false");
			}

			List<String> visibleForArrrayAfterConversion = new ArrayList<String>();
			if (hasValue(supplementaryOfferingsResponse.getData().getInternetInclusiveOffers().getOffers()
				.get(j).getHeader().getVisibleFor())) {
			    Utilities.printDebugLog(msisdn + "InternetInclusiveOffers CHECK VISIBLE FOR IF NOT NULL  :"
				    + supplementaryOfferingsResponse.getData().getInternetInclusiveOffers().getOffers()
					    .get(j).getHeader().getVisibleFor(),
				    logger);
			    String[] visibleForArrayConverting = supplementaryOfferingsResponse.getData()
				    .getInternetInclusiveOffers().getOffers().get(j).getHeader().getVisibleFor()
				    .split(",");
			    visibleForArrrayAfterConversion = Arrays.asList(visibleForArrayConverting);

			}

			List<String> HideForArrrayAfterConversion = new ArrayList<String>();
			if (hasValue(supplementaryOfferingsResponse.getData().getInternetInclusiveOffers().getOffers()
				.get(j).getHeader().getHideFor())) {
			    Utilities.printDebugLog(msisdn + "InternetInclusiveOffers CHECK HIDE FOR NOT NULL :"
				    + supplementaryOfferingsResponse.getData().getInternetInclusiveOffers().getOffers()
					    .get(j).getHeader().getHideFor(),
				    logger);
			    String[] HideForArrayConverting = supplementaryOfferingsResponse.getData()
				    .getInternetInclusiveOffers().getOffers().get(j).getHeader().getHideFor()
				    .split(",");
			    HideForArrrayAfterConversion = Arrays.asList(HideForArrayConverting);
			}

			Utilities.printDebugLog(msisdn + "-"
				+ supplementaryOfferingsResponse.getData().getInternetInclusiveOffers().getOffers()
					.get(j).getHeader()
				+ "- InternetInclusiveOffers CHECK Received From customerDataInternalLogic  ArrayListConversion VisibleFOR_FromInternet-"
				+ visibleForArrrayAfterConversion, logger);
			Utilities.printDebugLog(msisdn + "-"
				+ supplementaryOfferingsResponse.getData().getInternetInclusiveOffers().getOffers()
					.get(j).getHeader()
				+ "- InternetInclusiveOffers CHECK Received From customerDataInternalLogic  ArrayListConversion HisdeFOR_FromInternet-"
				+ HideForArrrayAfterConversion, logger);

			if (visibleForArrrayAfterConversion.size() < 1 && HideForArrrayAfterConversion.size() < 1) {

			    Utilities.printDebugLog(msisdn
				    + "----------------InternetInclusiveOffers CHECK VISIBLE AND HIDE FOR ARRAYLISTS NULL BOTH ELSE--------"
				    + visibleForArrrayAfterConversion, logger);
			    offers.add(supplementaryOfferingsResponse.getData().getInternetInclusiveOffers().getOffers()
				    .get(j));

			} else {
			    Utilities.printDebugLog(msisdn
				    + "----------------InternetInclusiveOffers CHECK VISIBLE AND HIDE FOR ARRALISTS NOT NULL IF--------"
				    + visibleForArrrayAfterConversion, logger);

			    boolean isflag = false;
			    if (visibleForArrrayAfterConversion != null && !visibleForArrrayAfterConversion.isEmpty()
				    && !CollectionUtils.intersection(offeringId, visibleForArrrayAfterConversion)
					    .isEmpty()) {

				Utilities.printDebugLog(msisdn + "-"
					+ supplementaryOfferingsResponse.getData().getInternetInclusiveOffers()
						.getOffers().get(j).getHeader().getOfferingId()
					+ "-InternetInclusiveOffers CHECK Received From customerDataInternalLogic Matched VisibleFOR_FromInternet YES-"
					+ visibleForArrrayAfterConversion, logger);
				isflag = true;
				offers.add(supplementaryOfferingsResponse.getData().getInternetInclusiveOffers()
					.getOffers().get(j));

			    } else if (HideForArrrayAfterConversion != null && !HideForArrrayAfterConversion.isEmpty()
				    && !CollectionUtils.intersection(offeringId, HideForArrrayAfterConversion)
					    .isEmpty()) {
				Utilities.printDebugLog(msisdn + "-"
					+ supplementaryOfferingsResponse.getData().getInternetInclusiveOffers()
						.getOffers().get(j).getHeader().getOfferingId()
					+ "-InternetInclusiveOffers CHECK Received From customerDataInternalLogic Matched hideFOR_FromInternetYES-"
					+ HideForArrrayAfterConversion, logger);

			    }

			    else {
				Utilities.printDebugLog(msisdn + "-"
					+ supplementaryOfferingsResponse.getData().getInternetInclusiveOffers()
						.getOffers().get(j).getHeader().getOfferingId()
					+ "-InternetInclusiveOffers CHECK Received From customerDataInternalLogic NOT FOUND IN HIDEFOR :-"
					+ supplementaryOfferingsResponse.getData().getInternetInclusiveOffers()
						.getOffers().get(j).getHeader().getOfferingId(),
					logger);
				if ((!CollectionUtils.intersection(offeringId, HideForArrrayAfterConversion)
					.isEmpty())) {
				    // hide mt daalo
				} else if (CollectionUtils.intersection(offeringId, HideForArrrayAfterConversion)
					.isEmpty() && visibleForArrrayAfterConversion.size() == 0) {
				    offers.add(supplementaryOfferingsResponse.getData().getInternetInclusiveOffers()
					    .getOffers().get(j));
				} else if ((!CollectionUtils.intersection(offeringId, visibleForArrrayAfterConversion)
					.isEmpty())) {
				    offers.add(supplementaryOfferingsResponse.getData().getInternetInclusiveOffers()
					    .getOffers().get(j));
				}
			    }
			}

			// END OF OF ALoowd for and VISIBLE FOR HIDE FOR CHECKS

		    }

		    // responseupdated =
		    responseupdated.getData().getInternetInclusiveOffers().setOffers(offers);

		} else {
		    ArrayList<SupplementryOfferingsData> offers = new ArrayList<SupplementryOfferingsData>();
		    responseupdated.getData().getInternetInclusiveOffers().setOffers(offers);
		}
		// END OF InternetInclusiveOffers LOGIC TO CHECK VISIBLE FOR HIDE FOR
		// START VoiceInclusiveOffers LOGIC TO CHECK VISIBLE FOR HIDE FOR
		if (supplementaryOfferingsResponse.getData() != null
			&& supplementaryOfferingsResponse.getData().getVoiceInclusiveOffers() != null
			&& supplementaryOfferingsResponse.getData().getVoiceInclusiveOffers().getOffers() != null) {
		    Utilities.printDebugLog(
			    msisdn + "-Received From customerDataInternalLogic VoiceInclusiveOffers OFFERS YES found-"
				    + mapper.writeValueAsString(
					    supplementaryOfferingsResponse.getData().getVoiceInclusiveOffers()),
			    logger);
		    ArrayList<SupplementryOfferingsData> offers = new ArrayList<SupplementryOfferingsData>();
		    for (int j = 0; j < supplementaryOfferingsResponse.getData().getVoiceInclusiveOffers().getOffers()
			    .size(); j++) {
			Utilities.printDebugLog(msisdn
				+ "-Received From customerDataInternalLogic  VisibleFOR_FromVoiceInclusiveOffers-"
				+ supplementaryOfferingsResponse.getData().getVoiceInclusiveOffers().getOffers().get(j)
					.getHeader().getVisibleFor(),
				logger);
			Utilities.printDebugLog(
				msisdn + "-Received From customerDataInternalLogic  HisdeFOR_FromgVoiceInclusiveOffers-"
					+ supplementaryOfferingsResponse.getData().getVoiceInclusiveOffers().getOffers()
						.get(j).getHeader().getHideFor(),
				logger);

			// START OF ALoowd for and VISIBLE FOR HIDE FOR CHECKS
			if (offeringId.contains(supplementaryOfferingsResponse.getData().getVoiceInclusiveOffers()
				.getOffers().get(j).getHeader().getOfferingId())) {
			    supplementaryOfferingsResponse.getData().getVoiceInclusiveOffers().getOffers().get(j)
				    .getHeader().setIsAlreadySusbcribed("true");
			    if (supplementaryOfferingsResponse.getData().getVoiceInclusiveOffers().getOffers().get(j)
				    .getHeader().getBtnRenew().equalsIgnoreCase("1")) {
				String allowedFor = supplementaryOfferingsResponse.getData().getVoiceInclusiveOffers()
					.getOffers().get(j).getHeader().getAllowedForRenew();

				List<String> AllowedFor = new ArrayList<String>(Arrays.asList(allowedFor.split(",")));
				if (!CollectionUtils.intersection(offeringId, AllowedFor).isEmpty()) {
				    supplementaryOfferingsResponse.getData().getVoiceInclusiveOffers().getOffers()
					    .get(j).getHeader().setBtnRenew("1");
				} else {
				    supplementaryOfferingsResponse.getData().getVoiceInclusiveOffers().getOffers()
					    .get(j).getHeader().setBtnRenew("0");
				}
			    }
			} else {
			    supplementaryOfferingsResponse.getData().getVoiceInclusiveOffers().getOffers().get(j)
				    .getHeader().setIsAlreadySusbcribed("false");
			}

			List<String> visibleForArrrayAfterConversion = new ArrayList<String>();
			if (hasValue(supplementaryOfferingsResponse.getData().getVoiceInclusiveOffers().getOffers()
				.get(j).getHeader().getVisibleFor())) {
			    Utilities.printDebugLog(msisdn + "VoiceInclusiveOffers CHECK VISIBLE FOR IF NOT NULL  :"
				    + supplementaryOfferingsResponse.getData().getVoiceInclusiveOffers().getOffers()
					    .get(j).getHeader().getVisibleFor(),
				    logger);
			    String[] visibleForArrayConverting = supplementaryOfferingsResponse.getData()
				    .getVoiceInclusiveOffers().getOffers().get(j).getHeader().getVisibleFor()
				    .split(",");
			    visibleForArrrayAfterConversion = Arrays.asList(visibleForArrayConverting);

			}

			List<String> HideForArrrayAfterConversion = new ArrayList<String>();
			if (hasValue(supplementaryOfferingsResponse.getData().getVoiceInclusiveOffers().getOffers()
				.get(j).getHeader().getHideFor())) {
			    Utilities.printDebugLog(msisdn + "VoiceInclusiveOffers CHECK HIDE FOR NOT NULL :"
				    + supplementaryOfferingsResponse.getData().getVoiceInclusiveOffers().getOffers()
					    .get(j).getHeader().getHideFor(),
				    logger);
			    String[] HideForArrayConverting = supplementaryOfferingsResponse.getData()
				    .getVoiceInclusiveOffers().getOffers().get(j).getHeader().getHideFor().split(",");
			    HideForArrrayAfterConversion = Arrays.asList(HideForArrayConverting);
			}

			Utilities.printDebugLog(msisdn + "-"
				+ supplementaryOfferingsResponse.getData().getVoiceInclusiveOffers().getOffers().get(j)
					.getHeader()
				+ "- VoiceInclusiveOffers CHECK Received From customerDataInternalLogic  ArrayListConversion VisibleFOR_FromInternet-"
				+ visibleForArrrayAfterConversion, logger);
			Utilities.printDebugLog(msisdn + "-"
				+ supplementaryOfferingsResponse.getData().getVoiceInclusiveOffers().getOffers().get(j)
					.getHeader()
				+ "- VoiceInclusiveOffers CHECK Received From customerDataInternalLogic  ArrayListConversion HisdeFOR_FromInternet-"
				+ HideForArrrayAfterConversion, logger);

			if (visibleForArrrayAfterConversion.size() < 1 && HideForArrrayAfterConversion.size() < 1) {

			    Utilities.printDebugLog(msisdn
				    + "----------------VoiceInclusiveOffers CHECK VISIBLE AND HIDE FOR ARRAYLISTS NULL BOTH ELSE--------"
				    + visibleForArrrayAfterConversion, logger);
			    offers.add(supplementaryOfferingsResponse.getData().getVoiceInclusiveOffers().getOffers()
				    .get(j));

			} else {
			    Utilities.printDebugLog(msisdn
				    + "----------------VoiceInclusiveOffers CHECK VISIBLE AND HIDE FOR ARRALISTS NOT NULL IF--------"
				    + visibleForArrrayAfterConversion, logger);

			    boolean isflag = false;
			    if (visibleForArrrayAfterConversion != null && !visibleForArrrayAfterConversion.isEmpty()
				    && !CollectionUtils.intersection(offeringId, visibleForArrrayAfterConversion)
					    .isEmpty()) {

				Utilities.printDebugLog(msisdn + "-"
					+ supplementaryOfferingsResponse.getData().getVoiceInclusiveOffers().getOffers()
						.get(j).getHeader().getOfferingId()
					+ "-VoiceInclusiveOffers CHECK Received From customerDataInternalLogic Matched VisibleFOR_FromInternet YES-"
					+ visibleForArrrayAfterConversion, logger);
				isflag = true;
				offers.add(supplementaryOfferingsResponse.getData().getVoiceInclusiveOffers()
					.getOffers().get(j));

			    } else if (HideForArrrayAfterConversion != null && !HideForArrrayAfterConversion.isEmpty()
				    && !CollectionUtils.intersection(offeringId, HideForArrrayAfterConversion)
					    .isEmpty()) {
				Utilities.printDebugLog(msisdn + "-"
					+ supplementaryOfferingsResponse.getData().getVoiceInclusiveOffers().getOffers()
						.get(j).getHeader().getOfferingId()
					+ "-VoiceInclusiveOffers CHECK Received From customerDataInternalLogic Matched hideFOR_FromInternetYES-"
					+ HideForArrrayAfterConversion, logger);

			    }

			    else {
				Utilities.printDebugLog(msisdn + "-"
					+ supplementaryOfferingsResponse.getData().getVoiceInclusiveOffers().getOffers()
						.get(j).getHeader().getOfferingId()
					+ "-VoiceInclusiveOffers CHECK Received From customerDataInternalLogic NOT FOUND IN HIDEFOR :-"
					+ supplementaryOfferingsResponse.getData().getVoiceInclusiveOffers().getOffers()
						.get(j).getHeader().getOfferingId(),
					logger);
				if ((!CollectionUtils.intersection(offeringId, HideForArrrayAfterConversion)
					.isEmpty())) {
				    // hide mt daalo
				} else if (CollectionUtils.intersection(offeringId, HideForArrrayAfterConversion)
					.isEmpty() && visibleForArrrayAfterConversion.size() == 0) {
				    offers.add(supplementaryOfferingsResponse.getData().getVoiceInclusiveOffers()
					    .getOffers().get(j));
				} else if ((!CollectionUtils.intersection(offeringId, visibleForArrrayAfterConversion)
					.isEmpty())) {
				    offers.add(supplementaryOfferingsResponse.getData().getVoiceInclusiveOffers()
					    .getOffers().get(j));
				}
			    }
			}
			// END OF OF ALoowd for and VISIBLE FOR HIDE FOR CHECKS
		    }
		    responseupdated.getData().getVoiceInclusiveOffers().setOffers(offers);
		} else {
		    ArrayList<SupplementryOfferingsData> offers = new ArrayList<SupplementryOfferingsData>();
		    responseupdated.getData().getVoiceInclusiveOffers().setOffers(offers);
		}
		// END OF VoiceInclusiveOffers LOGIC TO CHECK VISIBLE FOR HIDE FOR
		// START SmsInclusiveOffers LOGIC TO CHECK VISIBLE FOR HIDE FOR
		if (supplementaryOfferingsResponse.getData() != null
			&& supplementaryOfferingsResponse.getData().getSmsInclusiveOffers() != null
			&& supplementaryOfferingsResponse.getData().getSmsInclusiveOffers().getOffers() != null) {
		    Utilities.printDebugLog(
			    msisdn + "-Received From customerDataInternalLogic SmsInclusiveOffers OFFERS YES found-"
				    + mapper.writeValueAsString(
					    supplementaryOfferingsResponse.getData().getSmsInclusiveOffers()),
			    logger);
		    ArrayList<SupplementryOfferingsData> offers = new ArrayList<SupplementryOfferingsData>();
		    for (int j = 0; j < supplementaryOfferingsResponse.getData().getSmsInclusiveOffers().getOffers()
			    .size(); j++) {
			Utilities.printDebugLog(
				msisdn + "-Received From customerDataInternalLogic  VisibleFOR_FromSmsInclusiveOffers-"
					+ supplementaryOfferingsResponse.getData().getSmsInclusiveOffers().getOffers()
						.get(j).getHeader().getVisibleFor(),
				logger);
			Utilities.printDebugLog(
				msisdn + "-Received From customerDataInternalLogic  HisdeFOR_FromgSmsInclusiveOffers-"
					+ supplementaryOfferingsResponse.getData().getSmsInclusiveOffers().getOffers()
						.get(j).getHeader().getHideFor(),
				logger);

			// START OF ALoowd for and VISIBLE FOR HIDE FOR CHECKS
			if (offeringId.contains(supplementaryOfferingsResponse.getData().getSmsInclusiveOffers()
				.getOffers().get(j).getHeader().getOfferingId())) {
			    supplementaryOfferingsResponse.getData().getSmsInclusiveOffers().getOffers().get(j)
				    .getHeader().setIsAlreadySusbcribed("true");
			    if (supplementaryOfferingsResponse.getData().getSmsInclusiveOffers().getOffers().get(j)
				    .getHeader().getBtnRenew().equalsIgnoreCase("1")) {
				String allowedFor = supplementaryOfferingsResponse.getData().getSmsInclusiveOffers()
					.getOffers().get(j).getHeader().getAllowedForRenew();

				List<String> AllowedFor = new ArrayList<String>(Arrays.asList(allowedFor.split(",")));
				if (!CollectionUtils.intersection(offeringId, AllowedFor).isEmpty()) {
				    supplementaryOfferingsResponse.getData().getSmsInclusiveOffers().getOffers().get(j)
					    .getHeader().setBtnRenew("1");
				} else {
				    supplementaryOfferingsResponse.getData().getSmsInclusiveOffers().getOffers().get(j)
					    .getHeader().setBtnRenew("0");
				}
			    }
			} else {
			    supplementaryOfferingsResponse.getData().getSmsInclusiveOffers().getOffers().get(j)
				    .getHeader().setIsAlreadySusbcribed("false");
			}

			List<String> visibleForArrrayAfterConversion = new ArrayList<String>();
			if (hasValue(supplementaryOfferingsResponse.getData().getSmsInclusiveOffers().getOffers().get(j)
				.getHeader().getVisibleFor())) {
			    Utilities.printDebugLog(msisdn + "SmsInclusiveOffers CHECK VISIBLE FOR IF NOT NULL  :"
				    + supplementaryOfferingsResponse.getData().getSmsInclusiveOffers().getOffers()
					    .get(j).getHeader().getVisibleFor(),
				    logger);
			    String[] visibleForArrayConverting = supplementaryOfferingsResponse.getData()
				    .getSmsInclusiveOffers().getOffers().get(j).getHeader().getVisibleFor().split(",");
			    visibleForArrrayAfterConversion = Arrays.asList(visibleForArrayConverting);

			}

			List<String> HideForArrrayAfterConversion = new ArrayList<String>();
			if (hasValue(supplementaryOfferingsResponse.getData().getSmsInclusiveOffers().getOffers().get(j)
				.getHeader().getHideFor())) {
			    Utilities.printDebugLog(msisdn + "SmsInclusiveOffers CHECK HIDE FOR NOT NULL :"
				    + supplementaryOfferingsResponse.getData().getSmsInclusiveOffers().getOffers()
					    .get(j).getHeader().getHideFor(),
				    logger);
			    String[] HideForArrayConverting = supplementaryOfferingsResponse.getData()
				    .getSmsInclusiveOffers().getOffers().get(j).getHeader().getHideFor().split(",");
			    HideForArrrayAfterConversion = Arrays.asList(HideForArrayConverting);
			}

			Utilities.printDebugLog(msisdn + "-"
				+ supplementaryOfferingsResponse.getData().getSmsInclusiveOffers().getOffers().get(j)
					.getHeader()
				+ "- SmsInclusiveOffers CHECK Received From customerDataInternalLogic  ArrayListConversion VisibleFOR_FromInternet-"
				+ visibleForArrrayAfterConversion, logger);
			Utilities.printDebugLog(msisdn + "-"
				+ supplementaryOfferingsResponse.getData().getSmsInclusiveOffers().getOffers().get(j)
					.getHeader()
				+ "- SmsInclusiveOffers CHECK Received From customerDataInternalLogic  ArrayListConversion HisdeFOR_FromInternet-"
				+ HideForArrrayAfterConversion, logger);

			if (visibleForArrrayAfterConversion.size() < 1 && HideForArrrayAfterConversion.size() < 1) {

			    Utilities.printDebugLog(msisdn
				    + "----------------SmsInclusiveOffers CHECK VISIBLE AND HIDE FOR ARRAYLISTS NULL BOTH ELSE--------"
				    + visibleForArrrayAfterConversion, logger);
			    offers.add(supplementaryOfferingsResponse.getData().getSmsInclusiveOffers().getOffers()
				    .get(j));

			} else {
			    Utilities.printDebugLog(msisdn
				    + "----------------InternetInclusiveOffers CHECK VISIBLE AND HIDE FOR ARRALISTS NOT NULL IF--------"
				    + visibleForArrrayAfterConversion, logger);

			    boolean isflag = false;
			    if (visibleForArrrayAfterConversion != null && !visibleForArrrayAfterConversion.isEmpty()
				    && !CollectionUtils.intersection(offeringId, visibleForArrrayAfterConversion)
					    .isEmpty()) {

				Utilities.printDebugLog(msisdn + "-"
					+ supplementaryOfferingsResponse.getData().getSmsInclusiveOffers().getOffers()
						.get(j).getHeader().getOfferingId()
					+ "-SmsInclusiveOffers CHECK Received From customerDataInternalLogic Matched VisibleFOR_FromInternet YES-"
					+ visibleForArrrayAfterConversion, logger);
				isflag = true;
				offers.add(supplementaryOfferingsResponse.getData().getSmsInclusiveOffers().getOffers()
					.get(j));

			    } else if (HideForArrrayAfterConversion != null && !HideForArrrayAfterConversion.isEmpty()
				    && !CollectionUtils.intersection(offeringId, HideForArrrayAfterConversion)
					    .isEmpty()) {
				Utilities.printDebugLog(msisdn + "-"
					+ supplementaryOfferingsResponse.getData().getSmsInclusiveOffers().getOffers()
						.get(j).getHeader().getOfferingId()
					+ "-SmsInclusiveOffers CHECK Received From customerDataInternalLogic Matched hideFOR_FromInternetYES-"
					+ HideForArrrayAfterConversion, logger);

			    }

			    else {
				Utilities.printDebugLog(msisdn + "-"
					+ supplementaryOfferingsResponse.getData().getSmsInclusiveOffers().getOffers()
						.get(j).getHeader().getOfferingId()
					+ "-SmsInclusiveOffers CHECK Received From customerDataInternalLogic NOT FOUND IN HIDEFOR :-"
					+ supplementaryOfferingsResponse.getData().getSmsInclusiveOffers().getOffers()
						.get(j).getHeader().getOfferingId(),
					logger);
				if ((!CollectionUtils.intersection(offeringId, HideForArrrayAfterConversion)
					.isEmpty())) {
				    // hide mt daalo
				} else if (CollectionUtils.intersection(offeringId, HideForArrrayAfterConversion)
					.isEmpty() && visibleForArrrayAfterConversion.size() == 0) {
				    offers.add(supplementaryOfferingsResponse.getData().getSmsInclusiveOffers()
					    .getOffers().get(j));
				} else if ((!CollectionUtils.intersection(offeringId, visibleForArrrayAfterConversion)
					.isEmpty())) {
				    offers.add(supplementaryOfferingsResponse.getData().getSmsInclusiveOffers()
					    .getOffers().get(j));
				}
			    }
			}

			// END OF OF ALoowd for and VISIBLE FOR HIDE FOR CHECKS

		    }

		    // responseupdated =
		    responseupdated.getData().getSmsInclusiveOffers().setOffers(offers);

		} else {
		    ArrayList<SupplementryOfferingsData> offers = new ArrayList<SupplementryOfferingsData>();
		    responseupdated.getData().getSmsInclusiveOffers().setOffers(offers);
		}
		// END OF SmsInclusiveOffers LOGIC TO CHECK VISIBLE FOR HIDE FOR
		// END OF LOGIC RELATED TO HIDE FOR AND VISIBLE FOR

	    } else {
		SupplementaryOfferingsResponse responseupdatedNew = new SupplementaryOfferingsResponse();
		return responseupdatedNew;
	    }
	} catch (Exception e) {

	}

	return responseupdated;
    }
    /*
     * public SupplementaryOfferingsResponseData
     * TmProcessing(SupplementaryOfferingsResponseData response) {
     * 
     * SupplementaryOfferingsResponseData resp=new
     * SupplementaryOfferingsResponseData();
     * 
     * for(int i=0;i<response.getTm().getOffers().size();i++) {
     * response.getTm().getOffers().get(i).getHeader().get }
     * 
     * 
     * return resp; }
     */

    public static void main(String args[]) {
	// Creating an ArrayList with elements
	// {1, 2, 3, 4}
	ArrayList<Integer> arrL = new ArrayList<Integer>();
	arrL.add(1);
	arrL.add(2);
	arrL.add(3);
	arrL.add(4);

	// Using lambda expression to print all elements
	// of arrL
	arrL.forEach(n -> System.out.println(n));
	arrL.forEach(n -> {
	    if (n % 2 == 0)
		System.out.println(n);
	});
	// Using lambda expression to print even elements
	// of arrL

    }
}
