/**
 * 
 */
package com.evampsaanga.azerfon.business;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.SocketException;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.evampsaanga.azerfon.common.messagesmappings.GetMessagesMappings;
import com.evampsaanga.azerfon.common.utilities.Constants;
import com.evampsaanga.azerfon.common.utilities.GetConfigurations;
import com.evampsaanga.azerfon.common.utilities.HardCodedResponses;
import com.evampsaanga.azerfon.common.utilities.Transactions;
import com.evampsaanga.azerfon.common.utilities.Utilities;
import com.evampsaanga.azerfon.models.friendandfamily.addfnf.AddFriendAndFamilyRequest;
import com.evampsaanga.azerfon.models.friendandfamily.addfnf.AddFriendAndFamilyResponse;
import com.evampsaanga.azerfon.models.friendandfamily.deletefnf.DeleteFriendAndFamilyRequest;
import com.evampsaanga.azerfon.models.friendandfamily.deletefnf.DeleteFriendAndFamilyResponse;
import com.evampsaanga.azerfon.models.friendandfamily.getfnf.FNF;
import com.evampsaanga.azerfon.models.friendandfamily.getfnf.FriendAndFamilyResponseData;
import com.evampsaanga.azerfon.models.friendandfamily.getfnf.GetFriendAndFamilyRequest;
import com.evampsaanga.azerfon.models.friendandfamily.getfnf.GetFriendAndFamilyResponse;
import com.evampsaanga.azerfon.models.friendandfamily.updatefnf.UpdateFriendAndFamilyRequest;
import com.evampsaanga.azerfon.models.friendandfamily.updatefnf.UpdateFriendAndFamilyResponse;
import com.evampsaanga.azerfon.restclient.RestClient;

/**
 * @author Evamp & Saanga
 *
 */
public class FriendAndFamilyBusiness {
	Logger logger = Logger.getLogger(FriendAndFamilyBusiness.class);

	public GetFriendAndFamilyResponse getFnf(String msisdn, GetFriendAndFamilyRequest getFriendAndFamilyRequest,
			GetFriendAndFamilyResponse getFriendAndFamilyResponse)
			throws JSONException, ParseException, FileNotFoundException, IOException, SQLException {
		Utilities.printDebugLog(msisdn + "-Request received in " + Transactions.GET_FNF_TRANSACTION_NAME
				+ "  BUSINESS with data-" + getFriendAndFamilyRequest.toString(), logger);

		FriendAndFamilyResponseData resData = new FriendAndFamilyResponseData();
		RestClient rc = new RestClient();
		ObjectMapper mapper = new ObjectMapper();

		String requestJsonESB = mapper.writeValueAsString(getFriendAndFamilyRequest);

		String path = GetConfigurations.getESBRoute("getFnF");
		Utilities.printDebugLog(msisdn + "-Request Call to ESB", logger);
		Utilities.printDebugLog(msisdn + "-Path-" + path, logger);
		Utilities.printDebugLog(msisdn + "-Request Packet-" + requestJsonESB, logger);

		// String response = HardCodedResponses.GET_FNF_NEW;

		getFriendAndFamilyResponse.getLogsReport().setEsbRequestTime(Utilities.getReportDateTime());
		String response = rc.getResponseFromESB(path, requestJsonESB);
		getFriendAndFamilyResponse.getLogsReport().setEsbResponseTime(Utilities.getReportDateTime());

		Utilities.printDebugLog(msisdn + "-Received response from ESB-" + response, logger);

		// Logging ESB response code and description.
		getFriendAndFamilyResponse.setLogsReport(Utilities.logESBParamsintoReportLog(requestJsonESB, response,
				getFriendAndFamilyResponse.getLogsReport()));

		if (response != null && !response.isEmpty()) {

			if (Utilities.getValueFromJSON(response, "returnCode").equalsIgnoreCase(Constants.ESB_SUCCESS_CODE)) {

				resData.setFnfList(this.fetchFNFList(msisdn, response));
				getFriendAndFamilyResponse.setData(resData);
				getFriendAndFamilyResponse.getData().setFnfLimit(Utilities.getValueFromJSON(response, "fnfLimit"));
				getFriendAndFamilyResponse.setCallStatus(Constants.Call_Status_True);
				getFriendAndFamilyResponse.setResultCode(Constants.APP_SERVER_SUCCESS_CODE);
				getFriendAndFamilyResponse.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("success",
						getFriendAndFamilyRequest.getLang()));

			} else {
				
//				Utilities.printDebugLog(msisdn + "--------------------------" , logger);
//				Utilities.printDebugLog(msisdn + "-------GET FNF ELSE CASE---------" , logger);
//				Utilities.printDebugLog(msisdn + "--------------------------", logger);
				getFriendAndFamilyResponse.setCallStatus(Constants.Call_Status_False);
				resData.setFnfLimit(Utilities.getValueFromJSON(response, "fnfLimit"));
				getFriendAndFamilyResponse.setData(resData);
				getFriendAndFamilyResponse.setResultCode(Utilities.getValueFromJSON(response, "returnCode"));
				getFriendAndFamilyResponse.setResultDesc(Utilities.getErrorMessageFromFile("get.fnf",
						getFriendAndFamilyRequest.getLang(), Utilities.getValueFromJSON(response, "returnCode")));
			}
		} else {

			getFriendAndFamilyResponse.setCallStatus(Constants.Call_Status_False);
			getFriendAndFamilyResponse.setResultCode(Constants.API_FAILURE_CODE);
			getFriendAndFamilyResponse.setResultDesc(GetMessagesMappings
					.getMessageFromResourceBundle("connectivity.error", getFriendAndFamilyRequest.getLang()));
		}

		return getFriendAndFamilyResponse;

	}

	public AddFriendAndFamilyResponse addFnf(String msisdn, AddFriendAndFamilyRequest addFriendAndFamilyRequest,
			AddFriendAndFamilyResponse addFriendAndFamilyResponse) throws JSONException, ParseException,
			FileNotFoundException, IOException, InterruptedException, SQLException {
		Utilities.printDebugLog(msisdn + "-Request received in " + Transactions.ADD_FNF_TRANSACTION_NAME
				+ " BUSINESS with data-" + addFriendAndFamilyRequest.toString(), logger);

		FriendAndFamilyResponseData resData = new FriendAndFamilyResponseData();
		RestClient rc = new RestClient();
		ObjectMapper mapper = new ObjectMapper();
		GetFriendAndFamilyRequest getFriendAndFamilyRequest = new GetFriendAndFamilyRequest();
		GetFriendAndFamilyResponse getFriendAndFamilyResponse = new GetFriendAndFamilyResponse();

		getFriendAndFamilyRequest.setChannel(addFriendAndFamilyRequest.getChannel());
		getFriendAndFamilyRequest.setiP(addFriendAndFamilyRequest.getiP());
		getFriendAndFamilyRequest.setMsisdn(addFriendAndFamilyRequest.getMsisdn());
		getFriendAndFamilyRequest.setLang(addFriendAndFamilyRequest.getLang());
		getFriendAndFamilyRequest.setOfferingId(addFriendAndFamilyRequest.getOfferingId());

		String requestJsonESB = mapper.writeValueAsString(addFriendAndFamilyRequest);
		requestJsonESB = Utilities.addParamsToJSONObject(requestJsonESB, "actionType", "");
		requestJsonESB = Utilities.addParamsToJSONObject(requestJsonESB, "isFirstTime", addFriendAndFamilyRequest.getIsFirstTime());
		requestJsonESB = Utilities.addParamsToJSONObject(requestJsonESB, "fnfNumber",
				Utilities.getValueFromJSON(requestJsonESB, "addMsisdn"));
		requestJsonESB = Utilities.removeParamsFromJSONObject(requestJsonESB, "addMsisdn");

		String path = GetConfigurations.getESBRoute("manipulateFNF");
		Utilities.printDebugLog(msisdn + "-Request Call to ESB", logger);
		Utilities.printDebugLog(msisdn + "-Path-" + path, logger);
		Utilities.printDebugLog(msisdn + "-Request Packet-" + requestJsonESB, logger);
		// String response = HardCodedResponses.ADD_FNF;

		addFriendAndFamilyResponse.getLogsReport().setEsbRequestTime(Utilities.getReportDateTime());
		String response = rc.getResponseFromESB(path, requestJsonESB);
		addFriendAndFamilyResponse.getLogsReport().setEsbResponseTime(Utilities.getReportDateTime());

		Utilities.printDebugLog(msisdn + "-Received response from ESB-" + response, logger);

		// Logging ESB response code and description.
		addFriendAndFamilyResponse.setLogsReport(Utilities.logESBParamsintoReportLog(requestJsonESB, response,
				addFriendAndFamilyResponse.getLogsReport()));

		if (response != null && !response.isEmpty()) {

			if (Utilities.getValueFromJSON(response, "returnCode").equalsIgnoreCase(Constants.ESB_SUCCESS_CODE)) {
				Thread.sleep(Constants.SLEEP_TIME_OF_FNF);
				getFriendAndFamilyResponse = this.getFnf(msisdn, getFriendAndFamilyRequest, getFriendAndFamilyResponse);
				resData.setFnfList(getFriendAndFamilyResponse.getData().getFnfList());
				addFriendAndFamilyResponse.setData(resData);
				addFriendAndFamilyResponse.setCallStatus(Constants.Call_Status_True);
				addFriendAndFamilyResponse.setResultCode(Constants.APP_SERVER_SUCCESS_CODE);
				addFriendAndFamilyResponse.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("success",
						addFriendAndFamilyRequest.getLang()));
			} else {
				addFriendAndFamilyResponse.setCallStatus(Constants.Call_Status_False);
				
				addFriendAndFamilyResponse.setResultCode(Utilities.getValueFromJSON(response, "returnCode"));
				addFriendAndFamilyResponse.setResultDesc(Utilities.getErrorMessageFromFile("add.fnf",
						addFriendAndFamilyRequest.getLang(), Utilities.getValueFromJSON(response, "returnCode")));
			}
		} else {

			addFriendAndFamilyResponse.setCallStatus(Constants.Call_Status_False);
			addFriendAndFamilyResponse.setResultCode(Constants.API_FAILURE_CODE);
			addFriendAndFamilyResponse.setResultDesc(GetMessagesMappings
					.getMessageFromResourceBundle("connectivity.error", addFriendAndFamilyRequest.getLang()));
		}

		return addFriendAndFamilyResponse;
	}

	public DeleteFriendAndFamilyResponse deleteFnf(String msisdn,
			DeleteFriendAndFamilyRequest deleteFriendAndFamilyRequest,
			DeleteFriendAndFamilyResponse deleteFriendAndFamilyResponse) throws JSONException, ParseException,
			FileNotFoundException, IOException, InterruptedException, SQLException {
		Utilities.printDebugLog(msisdn + "-Request received in " + Transactions.DELETE_FNF_TRANSACTION_NAME
				+ " BUSINESS with data-" + deleteFriendAndFamilyRequest.toString(), logger);

		FriendAndFamilyResponseData resData = new FriendAndFamilyResponseData();
		RestClient rc = new RestClient();
		ObjectMapper mapper = new ObjectMapper();
		GetFriendAndFamilyRequest getFriendAndFamilyRequest = new GetFriendAndFamilyRequest();
		GetFriendAndFamilyResponse getFriendAndFamilyResponse = new GetFriendAndFamilyResponse();

		getFriendAndFamilyRequest.setChannel(deleteFriendAndFamilyRequest.getChannel());
		getFriendAndFamilyRequest.setiP(deleteFriendAndFamilyRequest.getiP());
		getFriendAndFamilyRequest.setMsisdn(deleteFriendAndFamilyRequest.getMsisdn());
		getFriendAndFamilyRequest.setLang(deleteFriendAndFamilyRequest.getLang());
		getFriendAndFamilyRequest.setOfferingId(deleteFriendAndFamilyRequest.getOfferingId());

		String requestJsonESB = mapper.writeValueAsString(deleteFriendAndFamilyRequest);

		requestJsonESB = Utilities.addParamsToJSONObject(requestJsonESB, "actionType",
				Utilities.getValueFromJSON(requestJsonESB, "deleteMsisdn"));
		requestJsonESB = Utilities.addParamsToJSONObject(requestJsonESB, "fnfNumber", "");
		requestJsonESB = Utilities.removeParamsFromJSONObject(requestJsonESB, "deleteMsisdn");

		String path = GetConfigurations.getESBRoute("manipulateFNF");
		Utilities.printDebugLog(msisdn + "-Request Call to ESB", logger);
		Utilities.printDebugLog(msisdn + "-Path-" + path, logger);
		Utilities.printDebugLog(msisdn + "-Request Packet-" + requestJsonESB, logger);

		// String response = HardCodedResponses.DELETE_FNF;

		deleteFriendAndFamilyResponse.getLogsReport().setEsbRequestTime(Utilities.getReportDateTime());
		String response = rc.getResponseFromESB(path, requestJsonESB);
		deleteFriendAndFamilyResponse.getLogsReport().setEsbResponseTime(Utilities.getReportDateTime());

		Utilities.printDebugLog(msisdn + "-Received response from ESB-" + response, logger);

		// Logging ESB response code and description.
		deleteFriendAndFamilyResponse.setLogsReport(Utilities.logESBParamsintoReportLog(requestJsonESB, response,
				deleteFriendAndFamilyResponse.getLogsReport()));

		if (response != null && !response.isEmpty()) {

			if (Utilities.getValueFromJSON(response, "returnCode").equalsIgnoreCase(Constants.ESB_SUCCESS_CODE)) {
				Thread.sleep(Constants.SLEEP_TIME_OF_FNF);
				getFriendAndFamilyResponse = this.getFnf(msisdn, getFriendAndFamilyRequest, getFriendAndFamilyResponse);
				resData.setFnfList(getFriendAndFamilyResponse.getData().getFnfList());
				deleteFriendAndFamilyResponse.setData(resData);
				deleteFriendAndFamilyResponse.setCallStatus(Constants.Call_Status_True);
				deleteFriendAndFamilyResponse.setResultCode(Constants.APP_SERVER_SUCCESS_CODE);
				deleteFriendAndFamilyResponse.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("success",
						deleteFriendAndFamilyRequest.getLang()));
			} else {
				deleteFriendAndFamilyResponse.setCallStatus(Constants.Call_Status_False);
				deleteFriendAndFamilyResponse.setResultCode(Utilities.getValueFromJSON(response, "returnCode"));
				deleteFriendAndFamilyResponse.setResultDesc(Utilities.getErrorMessageFromFile("delete.fnf",
						deleteFriendAndFamilyRequest.getLang(), Utilities.getValueFromJSON(response, "returnCode")));
			}
		} else {

			deleteFriendAndFamilyResponse.setCallStatus(Constants.Call_Status_False);
			deleteFriendAndFamilyResponse.setResultCode(Constants.API_FAILURE_CODE);
			deleteFriendAndFamilyResponse.setResultDesc(GetMessagesMappings
					.getMessageFromResourceBundle("connectivity.error", deleteFriendAndFamilyRequest.getLang()));
		}

		return deleteFriendAndFamilyResponse;
	}

	private List<FNF> fetchFNFList(String msisdn, String response)
			throws JSONException, ParseException, SocketException {
		List<FNF> fnfList = new ArrayList<>();
		Utilities.printDebugLog(msisdn + "-" + response, logger);

		JSONArray fnfArrayList = new JSONArray(Utilities.getValueFromJSON(response, "list"));
		for (int i = 0; i < fnfArrayList.length(); i++) {
			JSONObject fnfObj = fnfArrayList.getJSONObject(i);

			FNF fnf = new FNF();
			fnf.setMsisdn(fnfObj.getString("msisdn"));
			fnf.setCreatedDate(fnfObj.getString("date"));
			fnfList.add(fnf);

		}
		return fnfList;
	}

	public UpdateFriendAndFamilyResponse updateFnf(String msisdn,
			UpdateFriendAndFamilyRequest updateFriendAndFamilyRequest,
			UpdateFriendAndFamilyResponse updateFriendAndFamilyResponse) throws JSONException, ParseException,
			FileNotFoundException, IOException, InterruptedException, SQLException {
		Utilities.printDebugLog(msisdn + "-Request received in " + Transactions.UPDATE_FNF_TRANSACTION_NAME
				+ " BUSINESS with data-" + updateFriendAndFamilyRequest.toString(), logger);

		FriendAndFamilyResponseData resData = new FriendAndFamilyResponseData();
		RestClient rc = new RestClient();
		ObjectMapper mapper = new ObjectMapper();
		GetFriendAndFamilyRequest getFriendAndFamilyRequest = new GetFriendAndFamilyRequest();
		GetFriendAndFamilyResponse getFriendAndFamilyResponse = new GetFriendAndFamilyResponse();

		getFriendAndFamilyRequest.setChannel(updateFriendAndFamilyRequest.getChannel());
		getFriendAndFamilyRequest.setiP(updateFriendAndFamilyRequest.getiP());
		getFriendAndFamilyRequest.setMsisdn(updateFriendAndFamilyRequest.getMsisdn());
		getFriendAndFamilyRequest.setLang(updateFriendAndFamilyRequest.getLang());
		getFriendAndFamilyRequest.setOfferingId(updateFriendAndFamilyRequest.getOfferingId());

		String requestJsonESB = mapper.writeValueAsString(updateFriendAndFamilyRequest);
		requestJsonESB = Utilities.addParamsToJSONObject(requestJsonESB, "actionType", "");
		requestJsonESB = Utilities.addParamsToJSONObject(requestJsonESB, "fnfNumber",
				Utilities.getValueFromJSON(requestJsonESB, "oldMsisdn"));
		requestJsonESB = Utilities.removeParamsFromJSONObject(requestJsonESB, "oldMsisdn");

		String path = GetConfigurations.getESBRoute("manipulateFNF");
		Utilities.printDebugLog(msisdn + "-Request Call to ESB", logger);
		Utilities.printDebugLog(msisdn + "-Path-" + path, logger);
		Utilities.printDebugLog(msisdn + "-Request Packet-" + requestJsonESB, logger);

		// String response = HardCodedResponses.UPDATE_FNF;

		updateFriendAndFamilyResponse.getLogsReport().setEsbRequestTime(Utilities.getReportDateTime());
		String response = rc.getResponseFromESB(path, requestJsonESB);
		updateFriendAndFamilyResponse.getLogsReport().setEsbResponseTime(Utilities.getReportDateTime());

		Utilities.printDebugLog(msisdn + "-Received response from ESB-" + response, logger);

		// Logging ESB response code and description.
		updateFriendAndFamilyResponse.setLogsReport(Utilities.logESBParamsintoReportLog(requestJsonESB, response,
				updateFriendAndFamilyResponse.getLogsReport()));

		if (response != null && !response.isEmpty()) {

			if (Utilities.getValueFromJSON(response, "returnCode").equalsIgnoreCase(Constants.ESB_SUCCESS_CODE)) {
				Thread.sleep(Constants.SLEEP_TIME_OF_FNF);
				getFriendAndFamilyResponse = this.getFnf(msisdn, getFriendAndFamilyRequest, getFriendAndFamilyResponse);
				resData.setFnfList(getFriendAndFamilyResponse.getData().getFnfList());
				updateFriendAndFamilyResponse.setData(resData);
				updateFriendAndFamilyResponse.setCallStatus(Constants.Call_Status_True);
				updateFriendAndFamilyResponse.setResultCode(Constants.APP_SERVER_SUCCESS_CODE);
				updateFriendAndFamilyResponse.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("success",
						updateFriendAndFamilyRequest.getLang()));
			} else {
				updateFriendAndFamilyResponse.setCallStatus(Constants.Call_Status_False);
				updateFriendAndFamilyResponse.setResultCode(Utilities.getValueFromJSON(response, "returnCode"));
				updateFriendAndFamilyResponse.setResultDesc(Utilities.getErrorMessageFromFile("add.fnf",
						updateFriendAndFamilyRequest.getLang(), Utilities.getValueFromJSON(response, "returnCode")));
			}
		} else {

			updateFriendAndFamilyResponse.setCallStatus(Constants.Call_Status_False);
			updateFriendAndFamilyResponse.setResultCode(Constants.API_FAILURE_CODE);
			updateFriendAndFamilyResponse.setResultDesc(GetMessagesMappings
					.getMessageFromResourceBundle("connectivity.error", updateFriendAndFamilyRequest.getLang()));
		}

		return updateFriendAndFamilyResponse;
	}
}
