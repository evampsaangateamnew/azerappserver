/**
 * 
 */
package com.evampsaanga.azerfon.business;

import java.net.SocketException;
import java.sql.SQLException;

import org.apache.log4j.Logger;

import com.evampsaanga.azerfon.common.utilities.AppCache;
import com.evampsaanga.azerfon.common.utilities.Constants;
import com.evampsaanga.azerfon.common.utilities.GetConfigurations;
import com.evampsaanga.azerfon.common.utilities.GetHazalCastCache;
import com.evampsaanga.azerfon.common.utilities.Transactions;
import com.evampsaanga.azerfon.common.utilities.Utilities;
import com.evampsaanga.azerfon.models.triggers.TriggersRequest;
import com.evampsaanga.azerfon.models.triggers.TriggersResponse;

/**
 * @author Evamp & Saanga
 *
 */
public class TriggersBusiness {

	Logger logger = Logger.getLogger(TriggersBusiness.class);

	public TriggersResponse refreshCacheBusiness(TriggersRequest triggersRequest, TriggersResponse triggersResponse)
			throws SocketException {
		Utilities.printDebugLog("-Request received in TRIGGERS BUSINESS with data-" + triggersRequest.toString(),
				logger);

		if (triggersRequest.getCacheType().equalsIgnoreCase("1")) {

			Utilities.printDebugLog("Trigger received for FAQs", logger);

			Utilities.printDebugLog("Trigger cache size before refresh:" + AppCache.getHashmapFAQs().size(), logger);

			AppCache.getHashmapFAQs().clear();

			Utilities.printDebugLog(Constants.TRIGGERS_CACHE_AFTER_REFRESH + AppCache.getHashmapFAQs().size(), logger);

			if (AppCache.getHashmapFAQs().size() == 0) {

				triggersResponse.setCallStatus(Constants.Call_Status_True);
				triggersResponse.setResultCode(Constants.APP_SERVER_SUCCESS_CODE);
				triggersResponse.setResultDesc(Constants.TRIGGERS_CACHE_REFRESH_SUCCESS_MESSAGE);

			} else {

				triggersResponse.setCallStatus(Constants.Call_Status_False);
				triggersResponse.setResultCode(Constants.API_FAILURE_CODE);
				triggersResponse.setResultDesc(Constants.TRIGGERS_CACHE_REFRESH_FAIL_MESSAGE);

			}
		} else if (triggersRequest.getCacheType().equalsIgnoreCase("2")) {

			Utilities.printDebugLog("Trigger received for CONTACT US", logger);

			Utilities.printDebugLog("Trigger cache size before refresh:" + AppCache.getHashmapContactUs().size(),
					logger);

			AppCache.getHashmapContactUs().clear();

			Utilities.printDebugLog("Trigger cache size After refresh:" + AppCache.getHashmapContactUs().size(),
					logger);

			if (AppCache.getHashmapContactUs().size() == 0) {

				triggersResponse.setCallStatus(Constants.Call_Status_True);
				triggersResponse.setResultCode(Constants.APP_SERVER_SUCCESS_CODE);
				triggersResponse.setResultDesc(Constants.TRIGGERS_CACHE_REFRESH_SUCCESS_MESSAGE);

			} else {

				triggersResponse.setCallStatus(Constants.Call_Status_False);
				triggersResponse.setResultCode(Constants.API_FAILURE_CODE);
				triggersResponse.setResultDesc("Failed to refresh cache.");

			}
		} else if (triggersRequest.getCacheType().equalsIgnoreCase("3")) {

			Utilities.printDebugLog("Trigger received for APP MENU", logger);

			Utilities.printDebugLog("Trigger cache size before refresh:" + AppCache.getHashmapAppMenu().size(), logger);

			AppCache.getHashmapAppMenu().clear();

			Utilities.printDebugLog("Trigger cache size After refresh:" + AppCache.getHashmapAppMenu().size(), logger);

			if (AppCache.getHashmapAppMenu().size() == 0) {

				triggersResponse.setCallStatus(Constants.Call_Status_True);
				triggersResponse.setResultCode(Constants.APP_SERVER_SUCCESS_CODE);
				triggersResponse.setResultDesc(Constants.TRIGGERS_CACHE_REFRESH_SUCCESS_MESSAGE);

			} else {

				triggersResponse.setCallStatus(Constants.Call_Status_False);
				triggersResponse.setResultCode(Constants.API_FAILURE_CODE);
				triggersResponse.setResultDesc("Failed to refresh cache.");

			}
		} else if (triggersRequest.getCacheType().equalsIgnoreCase("4")) {

			Utilities.printDebugLog("Trigger received for STORE LOCATOR", logger);

			Utilities.printDebugLog("Trigger cache size before refresh:" + AppCache.getHashmapStoreLocator().size(),
					logger);

			AppCache.getHashmapStoreLocator().clear();

			Utilities.printDebugLog("Trigger cache size After refresh:" + AppCache.getHashmapStoreLocator().size(),
					logger);

			if (AppCache.getHashmapStoreLocator().size() == 0) {

				triggersResponse.setCallStatus(Constants.Call_Status_True);
				triggersResponse.setResultCode(Constants.APP_SERVER_SUCCESS_CODE);
				triggersResponse.setResultDesc(Constants.TRIGGERS_CACHE_REFRESH_SUCCESS_MESSAGE);

			} else {

				triggersResponse.setCallStatus(Constants.Call_Status_False);
				triggersResponse.setResultCode(Constants.API_FAILURE_CODE);
				triggersResponse.setResultDesc("Failed to refresh cache.");

			}
		} else if (triggersRequest.getCacheType().equalsIgnoreCase("5")) {

			Utilities.printDebugLog("Trigger received for SUPPLEMENTARY OFFERINGS", logger);

			Utilities.printDebugLog(
					"Trigger cache size before refresh:" + AppCache.getHashmapSupplementaryOfferings().size(), logger);

			AppCache.getHashmapSupplementaryOfferings().clear();

			Utilities.printDebugLog(
					"Trigger cache size After refresh:" + AppCache.getHashmapSupplementaryOfferings().size(), logger);

			if (AppCache.getHashmapSupplementaryOfferings().size() == 0) {

				triggersResponse.setCallStatus(Constants.Call_Status_True);
				triggersResponse.setResultCode(Constants.APP_SERVER_SUCCESS_CODE);
				triggersResponse.setResultDesc(Constants.TRIGGERS_CACHE_REFRESH_SUCCESS_MESSAGE);

			} else {

				triggersResponse.setCallStatus(Constants.Call_Status_False);
				triggersResponse.setResultCode(Constants.API_FAILURE_CODE);
				triggersResponse.setResultDesc("Failed to refresh cache.");

			}
		} else if (triggersRequest.getCacheType().equalsIgnoreCase("6")) {

			Utilities.printDebugLog("Trigger received for TARIFFS", logger);

			Utilities.printDebugLog("Trigger cache size before refresh:" + AppCache.getHashmapTariffs().size(), logger);

			AppCache.getHashmapTariffs().clear();

			Utilities.printDebugLog("Trigger cache size After refresh:" + AppCache.getHashmapTariffs().size(), logger);

			if (AppCache.getHashmapTariffs().size() == 0) {

				triggersResponse.setCallStatus(Constants.Call_Status_True);
				triggersResponse.setResultCode(Constants.APP_SERVER_SUCCESS_CODE);
				triggersResponse.setResultDesc(Constants.TRIGGERS_CACHE_REFRESH_SUCCESS_MESSAGE);

			} else {

				triggersResponse.setCallStatus(Constants.Call_Status_False);
				triggersResponse.setResultCode(Constants.API_FAILURE_CODE);
				triggersResponse.setResultDesc(Constants.TRIGGERS_CACHE_REFRESH_FAIL_MESSAGE);

			}
		} else if (triggersRequest.getCacheType().equalsIgnoreCase("7")) {

			Utilities.printDebugLog("Trigger received for Predefined Data", logger);

			Utilities.printDebugLog("Trigger cache size before refresh:" + AppCache.getHashmapPredefinedData().size(),
					logger);

			AppCache.getHashmapPredefinedData().clear();
			AppCache.getHashmapTariffMigration().clear();

			Utilities.printDebugLog(Constants.TRIGGERS_CACHE_AFTER_REFRESH + AppCache.getHashmapPredefinedData().size(),
					logger);

			if (AppCache.getHashmapPredefinedData().size() == 0) {

				triggersResponse.setCallStatus(Constants.Call_Status_True);
				triggersResponse.setResultCode(Constants.APP_SERVER_SUCCESS_CODE);
				triggersResponse.setResultDesc(Constants.TRIGGERS_CACHE_REFRESH_SUCCESS_MESSAGE);

			} else {

				triggersResponse.setCallStatus(Constants.Call_Status_False);
				triggersResponse.setResultCode(Constants.API_FAILURE_CODE);
				triggersResponse.setResultDesc(Constants.TRIGGERS_CACHE_REFRESH_FAIL_MESSAGE);

			}
		} else if (triggersRequest.getCacheType().equalsIgnoreCase("8")) {

			Utilities.printDebugLog("Trigger received for My Subscriptions", logger);

			Utilities.printDebugLog("Trigger cache size before refresh:" + AppCache.getHashmapMySubscriptions().size(),
					logger);

			AppCache.getHashmapMySubscriptions().clear();

			Utilities.printDebugLog(
					Constants.TRIGGERS_CACHE_AFTER_REFRESH + AppCache.getHashmapMySubscriptions().size(), logger);

			if (AppCache.getHashmapMySubscriptions().size() == 0) {

				triggersResponse.setCallStatus(Constants.Call_Status_True);
				triggersResponse.setResultCode(Constants.APP_SERVER_SUCCESS_CODE);
				triggersResponse.setResultDesc(Constants.TRIGGERS_CACHE_REFRESH_SUCCESS_MESSAGE);

			} else {

				triggersResponse.setCallStatus(Constants.Call_Status_False);
				triggersResponse.setResultCode(Constants.API_FAILURE_CODE);
				triggersResponse.setResultDesc(Constants.TRIGGERS_CACHE_REFRESH_FAIL_MESSAGE);

			}
		} else if (triggersRequest.getCacheType().equalsIgnoreCase("9")) {

			Utilities.printDebugLog("Trigger received for APP SERVER Configurations", logger);

			Utilities.printDebugLog("Trigger cache size before refresh:" + GetHazalCastCache.configurationCache.size(),
					logger);

			if (GetHazalCastCache.configurationCache == null)
				GetHazalCastCache.initHazelcast();

			GetHazalCastCache.configurationCache.clear();

			Utilities.printDebugLog(
					Constants.TRIGGERS_CACHE_AFTER_REFRESH + GetHazalCastCache.configurationCache.size(), logger);

			if (GetHazalCastCache.configurationCache.size() == 0) {
				try {
					GetConfigurations.reloadConfigurationsCache("");
				} catch (Exception e) {
					Utilities.printDebugLog("Cache Reload Failed", logger);
					e.printStackTrace();
				}
				triggersResponse.setCallStatus(Constants.Call_Status_True);
				triggersResponse.setResultCode(Constants.APP_SERVER_SUCCESS_CODE);
				triggersResponse.setResultDesc(Constants.TRIGGERS_CACHE_REFRESH_SUCCESS_MESSAGE);

			} else {

				triggersResponse.setCallStatus(Constants.Call_Status_False);
				triggersResponse.setResultCode(Constants.API_FAILURE_CODE);
				triggersResponse.setResultDesc(Constants.TRIGGERS_CACHE_REFRESH_FAIL_MESSAGE);

			}
		} else if (triggersRequest.getCacheType().equalsIgnoreCase("10")) {

			Utilities.printDebugLog("Trigger received for Transaction Names", logger);

			Utilities.printDebugLog("Trigger cache size before refresh:" + AppCache.getHashmapTransactionNames().size(),
					logger);

			AppCache.getHashmapTransactionNames().clear();

			Utilities.printDebugLog(
					Constants.TRIGGERS_CACHE_AFTER_REFRESH + AppCache.getHashmapTransactionNames().size(), logger);

			if (AppCache.getHashmapTransactionNames().size() == 0) {

				triggersResponse.setCallStatus(Constants.Call_Status_True);
				triggersResponse.setResultCode(Constants.APP_SERVER_SUCCESS_CODE);
				triggersResponse.setResultDesc(Constants.TRIGGERS_CACHE_REFRESH_SUCCESS_MESSAGE);

			} else {

				triggersResponse.setCallStatus(Constants.Call_Status_False);
				triggersResponse.setResultCode(Constants.API_FAILURE_CODE);
				triggersResponse.setResultDesc(Constants.TRIGGERS_CACHE_REFRESH_FAIL_MESSAGE);

			}
		} else if (triggersRequest.getCacheType().equalsIgnoreCase("11")) {
			Utilities.printDebugLog(Transactions.TRIGGERS_TRANSACTION_NAME + "Trigger received for golden pay messages",
					logger);

			Utilities.printDebugLog(Transactions.TRIGGERS_TRANSACTION_NAME + "Trigger cache size before refresh:"
					+ AppCache.getGoldenPayMessages().size(), logger);

			AppCache.getGoldenPayMessages().clear();

			Utilities.printDebugLog(Constants.TRIGGERS_CACHE_AFTER_REFRESH + AppCache.getGoldenPayMessages().size(),
					logger);

			if (AppCache.getGoldenPayMessages().size() == 0) {
				triggersResponse.setCallStatus(Constants.Call_Status_True);
				triggersResponse.setResultCode(Constants.APP_SERVER_SUCCESS_CODE);
				triggersResponse.setResultDesc(Constants.TRIGGERS_CACHE_REFRESH_SUCCESS_MESSAGE);

			} else {
				triggersResponse.setCallStatus(Constants.Call_Status_False);
				triggersResponse.setResultCode(Constants.API_FAILURE_CODE);
				triggersResponse.setResultDesc(Constants.TRIGGERS_CACHE_REFRESH_FAIL_MESSAGE);

			}
		} else if (triggersRequest.getCacheType().equalsIgnoreCase("12")) {

			Utilities.printDebugLog("Trigger received for INTERNET OFFERINGS", logger);

			Utilities.printDebugLog(
					"Trigger cache size before refresh:" + AppCache.getHashmapInternetOfferings().size(), logger);

			AppCache.getHashmapInternetOfferings().clear();

			Utilities.printDebugLog("Trigger cache size After refresh:" + AppCache.getHashmapInternetOfferings().size(),
					logger);

			if (AppCache.getHashmapInternetOfferings().size() == 0) {

				triggersResponse.setCallStatus(Constants.Call_Status_True);
				triggersResponse.setResultCode(Constants.APP_SERVER_SUCCESS_CODE);
				triggersResponse.setResultDesc(Constants.TRIGGERS_CACHE_REFRESH_SUCCESS_MESSAGE);

			} else {

				triggersResponse.setCallStatus(Constants.Call_Status_False);
				triggersResponse.setResultCode(Constants.API_FAILURE_CODE);
				triggersResponse.setResultDesc("Failed to refresh cache.");

			}
		} else if (triggersRequest.getCacheType().equalsIgnoreCase("13")) {

			Utilities.printDebugLog("Trigger received for Surveys", logger);

			Utilities.printDebugLog("Trigger cache size before refresh:" + AppCache.getHashMapSurveys().size(), logger);

			AppCache.getHashMapSurveys().clear();

			Utilities.printDebugLog("Trigger cache size After refresh:" + AppCache.getHashMapSurveys().size(), logger);

			if (AppCache.getHashMapSurveys().size() == 0) {

				triggersResponse.setCallStatus(Constants.Call_Status_True);
				triggersResponse.setResultCode(Constants.APP_SERVER_SUCCESS_CODE);
				triggersResponse.setResultDesc(Constants.TRIGGERS_CACHE_REFRESH_SUCCESS_MESSAGE);

			} else {

				triggersResponse.setCallStatus(Constants.Call_Status_False);
				triggersResponse.setResultCode(Constants.API_FAILURE_CODE);
				triggersResponse.setResultDesc("Failed to refresh cache.");

			}
		}

		// clearing cache for all transactions 0 means all
		else if (triggersRequest.getCacheType().equalsIgnoreCase("0")) {

			Utilities.printDebugLog("Trigger received for Transaction Names", logger);

			Utilities.printDebugLog("Trigger cache size before refresh:" + AppCache.getHashmapTransactionNames().size(),
					logger);

			AppCache.getHashmapFAQs().clear();
			AppCache.getHashmapContactUs().clear();
			AppCache.getHashmapAppMenu().clear();
			AppCache.getHashmapStoreLocator().clear();
			AppCache.getHashmapSupplementaryOfferings().clear();
			AppCache.getHashmapTariffs().clear();
			AppCache.getHashmapPredefinedData().clear();
			AppCache.getHashmapTariffMigration().clear();
			AppCache.getHashmapMySubscriptions().clear();
			AppCache.getHashmapInternetOfferings().clear();
			AppCache.getHashMapSurveys().clear();
			if (GetHazalCastCache.configurationCache == null)
				GetHazalCastCache.initHazelcast();
			GetHazalCastCache.configurationCache.clear();
			AppCache.getHashmapTransactionNames().clear();
			AppCache.getGoldenPayMessages().clear();
			Utilities.printDebugLog(
					Constants.TRIGGERS_CACHE_AFTER_REFRESH + AppCache.getHashmapTransactionNames().size(), logger);

			if (AppCache.getHashmapTransactionNames().size() == 0) {
				Utilities.printDebugLog(
						Constants.TRIGGERS_CACHE_AFTER_REFRESH +" in if "+ AppCache.getHashmapTransactionNames().size(), logger);
				try {
					GetConfigurations.reloadConfigurationsCache("");
				} catch (Exception e) {
					Utilities.printDebugLog("Cache Reload Failed", logger);
					e.printStackTrace();
				}

				triggersResponse.setCallStatus(Constants.Call_Status_True);
				triggersResponse.setResultCode(Constants.APP_SERVER_SUCCESS_CODE);
				triggersResponse.setResultDesc(Constants.TRIGGERS_CACHE_REFRESH_SUCCESS_MESSAGE);

			} else {
				Utilities.printDebugLog(
						Constants.TRIGGERS_CACHE_AFTER_REFRESH +" in else "+ AppCache.getHashmapTransactionNames().size(), logger);
				triggersResponse.setCallStatus(Constants.Call_Status_False);
				triggersResponse.setResultCode(Constants.API_FAILURE_CODE);
				triggersResponse.setResultDesc(Constants.TRIGGERS_CACHE_REFRESH_FAIL_MESSAGE);

			}

		} else {

			triggersResponse.setCallStatus(Constants.Call_Status_False);
			triggersResponse.setResultCode(Constants.API_FAILURE_CODE);
			triggersResponse.setResultDesc(
					"Cache against key (" + triggersRequest.getCacheType() + ") not exist in Bakcell E-care system.");

		}

		return triggersResponse;
	}

}
