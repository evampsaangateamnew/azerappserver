/**
 * 
 */
package com.evampsaanga.azerfon.business;

import java.net.SocketException;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.evampsaanga.azerfon.common.messagesmappings.GetMessagesMappings;
import com.evampsaanga.azerfon.common.utilities.AppCache;
import com.evampsaanga.azerfon.common.utilities.Constants;
import com.evampsaanga.azerfon.common.utilities.GetConfigurations;
import com.evampsaanga.azerfon.common.utilities.Transactions;
import com.evampsaanga.azerfon.common.utilities.Utilities;
import com.evampsaanga.azerfon.models.customerservices.customerinfo.CustomerInfoRequest;
import com.evampsaanga.azerfon.models.customerservices.customerinfo.CustomerInfoResponseData;
import com.evampsaanga.azerfon.models.mysubscriptions.getsubscriptions.HeaderUsage;
import com.evampsaanga.azerfon.models.mysubscriptions.getsubscriptions.MySubscriptionsRequest;
import com.evampsaanga.azerfon.models.supplementaryofferings.getsupplementaryofferings.SupplementaryOfferingsByIDsRequest;
import com.evampsaanga.azerfon.models.supplementaryofferings.getsupplementaryofferings.SupplementaryOfferingsResponse;
import com.evampsaanga.azerfon.models.supplementaryofferings.getsupplementaryofferings.SupplementaryOfferingsResponseData;
import com.evampsaanga.azerfon.models.supplementaryofferings.getsupplementaryofferings.commonofferings.SupplementryOfferingsData;
import com.evampsaanga.azerfon.models.supplementaryofferings.getsupplementaryofferings.commonofferings.SupplementryOffersHeaders;
import com.evampsaanga.azerfon.restclient.RestClient;

/**
 * @author Evamp & Saanga
 *
 */
public class MySubscriptionsBusiness {
    Logger logger = Logger.getLogger(MySubscriptionsBusiness.class);
    
    public SupplementaryOfferingsResponse getSubscriptionsV2(String msisdn, MySubscriptionsRequest mySubscriptionsRequest,
    	    SupplementaryOfferingsResponse mySubscriptionsResponse) throws Exception {

    	Utilities.printDebugLog(msisdn + "-Request received in " + Transactions.MYSUBSCRIPTION_TRANSACTION_NAME
    		+ " BUSINESS with data-" + mySubscriptionsRequest.toString(), logger);

    	SupplementaryOfferingsResponseData resData = new SupplementaryOfferingsResponseData();
    	ObjectMapper mapper = new ObjectMapper();
    	RestClient rc = new RestClient();

    	// Below flag will check that after fetching offers from cache only one
    	// called is made to MAGENTO for rest of offering ids.
    	boolean isOffersFetched = false;
    	String requestJsonESB = mapper.writeValueAsString(mySubscriptionsRequest);
    	String path = GetConfigurations.getESBRoute("getMysubscriptionsUsageV2");
    	

    	requestJsonESB = Utilities.removeParamsFromJSONObject(requestJsonESB, "offeringName");
    	requestJsonESB = Utilities.removeParamsFromJSONObject(requestJsonESB, "listOfferingIds");
    	requestJsonESB = Utilities.removeParamsFromJSONObject(requestJsonESB, "brandName");
    	
    	Utilities.printDebugLog(msisdn + "- getMysubscriptionsUsage Request Call to ESB ( GET USAGE DETAILS )", logger);
    	Utilities.printDebugLog(msisdn + "-Path-" + path, logger);
    	Utilities.printDebugLog(msisdn + "-Request Packet-" + requestJsonESB, logger);

    	mySubscriptionsResponse.getLogsReport().setEsbRequestTime(Utilities.getReportDateTime());
    	String response = rc.getResponseFromESB(path, requestJsonESB);
    	mySubscriptionsResponse.getLogsReport().setEsbResponseTime(Utilities.getReportDateTime());

    	Utilities.printDebugLog(msisdn + "-getMysubscriptionsUsage Received response from ESB-" + response, logger);

    	// Logging ESB response code and description.
    	mySubscriptionsResponse.setLogsReport(
    		Utilities.logESBParamsintoReportLog(requestJsonESB, response, mySubscriptionsResponse.getLogsReport()));

    	if (response != null && !response.isEmpty()) {

    	    if (Utilities.getValueFromJSON(response, "returnCode").equalsIgnoreCase(Constants.ESB_SUCCESS_CODE)) {
    		/*
    		 * There is a conditional MAGENTO call in parsing usage
    		 * response. This call is initiated when any offering ID is not
    		 * found in cache.
    		 */

    		JSONArray usageOfferingIDs = new JSONObject(response).getJSONArray("items");
    		JSONObject usageFreeResources = new JSONObject(response).getJSONObject("freeResources");

    		if (usageOfferingIDs.length() > 0) {
    		    resData = this.parseUsageResponse(usageOfferingIDs, mySubscriptionsRequest, resData,
    			    isOffersFetched);

    		    resData = this.parseFreeResources(usageFreeResources, resData, mySubscriptionsRequest.getLang());
    		    List<SupplementryOfferingsData> offersvoiceinclusive=resData.getVoiceInclusiveOffers().getOffers();
    		   
    		   
    		    List<SupplementryOfferingsData> newList = offersvoiceinclusive.stream() 
                        .distinct() 
                        .collect(Collectors.toList()); 
    		    
    		    resData.getVoiceInclusiveOffers().setOffers(newList);
    		    mySubscriptionsResponse.setData(resData);
    		    mySubscriptionsResponse.setCallStatus(Constants.Call_Status_True);
    		    mySubscriptionsResponse.setResultCode(Constants.APP_SERVER_SUCCESS_CODE);
    		    mySubscriptionsResponse.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("success",
    			    mySubscriptionsRequest.getLang()));
    		} else {
    		    // Here we don't have usage but to show empty cards, we need
    		    // to return success code.
    		    mySubscriptionsResponse.setData(resData);
    		    mySubscriptionsResponse.setCallStatus(Constants.Call_Status_True);
    		    mySubscriptionsResponse.setResultCode(Constants.APP_SERVER_SUCCESS_CODE);
    		    mySubscriptionsResponse.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("success",
    			    mySubscriptionsRequest.getLang()));
    		}
    	    } else {
    		mySubscriptionsResponse.setData(resData);
    		mySubscriptionsResponse.setCallStatus(Constants.Call_Status_True);
    		mySubscriptionsResponse.setResultCode(Constants.APP_SERVER_SUCCESS_CODE);
    		mySubscriptionsResponse.setResultDesc(Utilities.getErrorMessageFromFile("my.subscriptions",
    			mySubscriptionsRequest.getLang(), Utilities.getValueFromJSON(response, "returnCode")));
    	    }
    	} else {

    	    mySubscriptionsResponse.setCallStatus(Constants.Call_Status_False);
    	    mySubscriptionsResponse.setResultCode(Constants.API_FAILURE_CODE);
    	    mySubscriptionsResponse.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("connectivity.error",
    		    mySubscriptionsRequest.getLang()));
    	}

    	addUsualOffersToInclusive(mySubscriptionsResponse.getData());

    	
//    	// start new logic
//    	
//    	SupplementaryOfferingsResponse mySubscriptionsResponseDeep=null;
//   	    mySubscriptionsResponseDeep = (SupplementaryOfferingsResponse) mySubscriptionsResponse.clone();
//    	
//    	
//    	//end of new logic
    	
    	Utilities.printDebugLog(msisdn + "**************************************************", logger);
    	Utilities.printDebugLog(msisdn + "-Path-" + path, logger);
    	Utilities.printDebugLog(msisdn + "-Request Packet-" + requestJsonESB, logger);
    	Utilities.printDebugLog(msisdn + "-Received response from ESB (PRINTED AGAIN)-" + response, logger);
    	Utilities.printDebugLog(msisdn + "**************************************************", logger);
    	return mySubscriptionsResponse;
        }

    public SupplementaryOfferingsResponse getSubscriptions(String msisdn, MySubscriptionsRequest mySubscriptionsRequest,
	    SupplementaryOfferingsResponse mySubscriptionsResponse) throws Exception {

	Utilities.printDebugLog(msisdn + "-Request received in " + Transactions.MYSUBSCRIPTION_TRANSACTION_NAME
		+ " BUSINESS with data-" + mySubscriptionsRequest.toString(), logger);

	SupplementaryOfferingsResponseData resData = new SupplementaryOfferingsResponseData();
	ObjectMapper mapper = new ObjectMapper();
	RestClient rc = new RestClient();

	// Below flag will check that after fetching offers from cache only one
	// called is made to MAGENTO for rest of offering ids.
	boolean isOffersFetched = false;
	String requestJsonESB = mapper.writeValueAsString(mySubscriptionsRequest);
	String path = GetConfigurations.getESBRoute("getMysubscriptionsUsage");
	Utilities.printDebugLog(msisdn + "- getMysubscriptionsUsage Request Call to ESB ( GET USAGE DETAILS )", logger);
	Utilities.printDebugLog(msisdn + "-Path-" + path, logger);
	Utilities.printDebugLog(msisdn + "-Request Packet-" + requestJsonESB, logger);

	requestJsonESB = Utilities.removeParamsFromJSONObject(requestJsonESB, "offeringName");
	requestJsonESB = Utilities.removeParamsFromJSONObject(requestJsonESB, "listOfferingIds");
	requestJsonESB = Utilities.removeParamsFromJSONObject(requestJsonESB, "brandName");
	requestJsonESB = Utilities.removeParamsFromJSONObject(requestJsonESB, "groupAccessCode");
	
	mySubscriptionsResponse.getLogsReport().setEsbRequestTime(Utilities.getReportDateTime());
	String response = rc.getResponseFromESB(path, requestJsonESB);
	mySubscriptionsResponse.getLogsReport().setEsbResponseTime(Utilities.getReportDateTime());

	Utilities.printDebugLog(msisdn + "-getMysubscriptionsUsage Received response from ESB-" + response, logger);

	// Logging ESB response code and description.
	mySubscriptionsResponse.setLogsReport(
		Utilities.logESBParamsintoReportLog(requestJsonESB, response, mySubscriptionsResponse.getLogsReport()));

	if (response != null && !response.isEmpty()) {

	    if (Utilities.getValueFromJSON(response, "returnCode").equalsIgnoreCase(Constants.ESB_SUCCESS_CODE)) {
		/*
		 * There is a conditional MAGENTO call in parsing usage
		 * response. This call is initiated when any offering ID is not
		 * found in cache.
		 */

		JSONArray usageOfferingIDs = new JSONObject(response).getJSONArray("items");
		JSONObject usageFreeResources = new JSONObject(response).getJSONObject("freeResources");

		if (usageOfferingIDs.length() > 0) {
		    resData = this.parseUsageResponse(usageOfferingIDs, mySubscriptionsRequest, resData,
			    isOffersFetched);

		    resData = this.parseFreeResources(usageFreeResources, resData, mySubscriptionsRequest.getLang());
		    List<SupplementryOfferingsData> offersvoiceinclusive=resData.getVoiceInclusiveOffers().getOffers();
		   
		   
		    List<SupplementryOfferingsData> newList = offersvoiceinclusive.stream() 
                    .distinct() 
                    .collect(Collectors.toList()); 
		    
		    resData.getVoiceInclusiveOffers().setOffers(newList);
		    mySubscriptionsResponse.setData(resData);
		    mySubscriptionsResponse.setCallStatus(Constants.Call_Status_True);
		    mySubscriptionsResponse.setResultCode(Constants.APP_SERVER_SUCCESS_CODE);
		    mySubscriptionsResponse.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("success",
			    mySubscriptionsRequest.getLang()));
		} else {
		    // Here we don't have usage but to show empty cards, we need
		    // to return success code.
		    mySubscriptionsResponse.setData(resData);
		    mySubscriptionsResponse.setCallStatus(Constants.Call_Status_True);
		    mySubscriptionsResponse.setResultCode(Constants.APP_SERVER_SUCCESS_CODE);
		    mySubscriptionsResponse.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("success",
			    mySubscriptionsRequest.getLang()));
		}
	    } else {
		mySubscriptionsResponse.setData(resData);
		mySubscriptionsResponse.setCallStatus(Constants.Call_Status_True);
		mySubscriptionsResponse.setResultCode(Constants.APP_SERVER_SUCCESS_CODE);
		mySubscriptionsResponse.setResultDesc(Utilities.getErrorMessageFromFile("my.subscriptions",
			mySubscriptionsRequest.getLang(), Utilities.getValueFromJSON(response, "returnCode")));
	    }
	} else {

	    mySubscriptionsResponse.setCallStatus(Constants.Call_Status_False);
	    mySubscriptionsResponse.setResultCode(Constants.API_FAILURE_CODE);
	    mySubscriptionsResponse.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("connectivity.error",
		    mySubscriptionsRequest.getLang()));
	}

	addUsualOffersToInclusive(mySubscriptionsResponse.getData());

	Utilities.printDebugLog(msisdn + "**************************************************", logger);
	Utilities.printDebugLog(msisdn + "-Path-" + path, logger);
	Utilities.printDebugLog(msisdn + "-Request Packet-" + requestJsonESB, logger);
	Utilities.printDebugLog(msisdn + "-Received response from ESB (PRINTED AGAIN)-" + response, logger);
	Utilities.printDebugLog(msisdn + "**************************************************", logger);
	return mySubscriptionsResponse;
    }

    private SupplementaryOfferingsResponseData parseFreeResources(JSONObject usageFreeResources,
	    SupplementaryOfferingsResponseData resData, String lang)
	    throws JSONException, ParseException, SocketException, SQLException {

	SupplementryOfferingsData supplementryOfferingsData = new SupplementryOfferingsData();
	supplementryOfferingsData.setHeader(new SupplementryOffersHeaders());
	supplementryOfferingsData.getHeader()
		.setOfferName(GetMessagesMappings.getLabelsFromResourceBundle("data", lang));
	supplementryOfferingsData.getHeader().setIsFreeResource("true");

	// Parsing Free Resource Usage INTERNET
	JSONArray filteredArrayInternet = this.filterFreeResourceUsageByType(
		usageFreeResources.getJSONArray(Constants.MY_SUBSCRIPTIONS_FREE_USAGE_INTERNET),
		Constants.MY_SUBSCRIPTIONS_FREE_USAGE_INTERNET);

	supplementryOfferingsData.getHeader().setUsage(
		this.parseUsageData(supplementryOfferingsData.getHeader().getUsage(), filteredArrayInternet, lang));

	if (!supplementryOfferingsData.getHeader().getUsage().isEmpty())
	    resData.getInternet().getOffers().add(supplementryOfferingsData);

	// Parsing Free Resource Usage CALL
	supplementryOfferingsData = new SupplementryOfferingsData();
	supplementryOfferingsData.setHeader(new SupplementryOffersHeaders());
	supplementryOfferingsData.getHeader()
		.setOfferName(GetMessagesMappings.getLabelsFromResourceBundle("calls", lang));
	supplementryOfferingsData.getHeader().setIsFreeResource("true");
	JSONArray filteredArrayCall = this.filterFreeResourceUsageByType(
		usageFreeResources.getJSONArray(Constants.MY_SUBSCRIPTIONS_FREE_USAGE_CALL),
		Constants.MY_SUBSCRIPTIONS_FREE_USAGE_CALL);

	supplementryOfferingsData.getHeader().setUsage(
		this.parseUsageData(supplementryOfferingsData.getHeader().getUsage(), filteredArrayCall, lang));

	if (!supplementryOfferingsData.getHeader().getUsage().isEmpty())
	    resData.getCall().getOffers().add(supplementryOfferingsData);

	// Parsing Free Resource Usage SMS
	supplementryOfferingsData = new SupplementryOfferingsData();
	supplementryOfferingsData.setHeader(new SupplementryOffersHeaders());
	supplementryOfferingsData.getHeader()
		.setOfferName(GetMessagesMappings.getLabelsFromResourceBundle("sms", lang));
	supplementryOfferingsData.getHeader().setIsFreeResource("true");
	JSONArray filteredArraySMS = this.filterFreeResourceUsageByType(
		usageFreeResources.getJSONArray(Constants.MY_SUBSCRIPTIONS_FREE_USAGE_SMS),
		Constants.MY_SUBSCRIPTIONS_FREE_USAGE_SMS);

	supplementryOfferingsData.getHeader().setUsage(
		this.parseUsageData(supplementryOfferingsData.getHeader().getUsage(), filteredArraySMS, lang));
	if (!supplementryOfferingsData.getHeader().getUsage().isEmpty())
	    resData.getSms().getOffers().add(supplementryOfferingsData);

	return resData;
    }

    private JSONArray filterFreeResourceUsageByType(JSONArray usageFreeResources, String type) throws JSONException {
	JSONArray freeResourceUsageList = new JSONArray();
	for (int i = 0; i < usageFreeResources.length(); i++) {
	    JSONArray usageArr = usageFreeResources.getJSONObject(i).getJSONArray("usage");
	    for (int j = 0; j < usageArr.length(); j++) {
		JSONObject objUsage = usageArr.getJSONObject(j);
		if (objUsage.getString("type").equalsIgnoreCase(type)) {
		    freeResourceUsageList.put(objUsage);
		}
	    }
	}
	return freeResourceUsageList;
    }

    private SupplementaryOfferingsResponseData parseUsageResponse(JSONArray usageOfferingIDs,
	    MySubscriptionsRequest mySubscriptionsRequest, SupplementaryOfferingsResponseData resData,
	    boolean isOffersFetched) throws Exception {

    	
	ObjectMapper mapper = new ObjectMapper();
	Utilities.printDebugLog(
		mySubscriptionsRequest.getMsisdn() + "-Usage Parser-Received Data-" + usageOfferingIDs.toString(),
		logger);

	Utilities.printDebugLog(mySubscriptionsRequest.getMsisdn() + "-Usage Parser-My Subscription Request-"
		+ mySubscriptionsRequest.toString(), logger);

	/*
	 * ArrayList "Remaining Offering IDs " is used to store that offering
	 * IDs which are not found in APP SERVER cache and need to get the
	 * offers against those IDs from MAGENTO.
	 */	
	JSONArray remainingUsageObjects = new JSONArray();
	JSONArray remainingOfferingIDs = new JSONArray();

	for (int i = 0; i < usageOfferingIDs.length(); i++) {
	    JSONObject usageObject = usageOfferingIDs.getJSONObject(i);
	 //   SupplementaryOfferingsResponseData supplementaryOfferingsMainObj = new SupplementaryOfferingsResponseData();
	    /*
	     * Fetching Offering ID which will be used to search into cached
	     * supplementary offerings.
	     */
	    String offeringId = usageObject.getString("offeringId");
	    String isDaily = usageObject.getString("isDaily");
	    Utilities.printDebugLog(mySubscriptionsRequest.getMsisdn()
				+" getMysubscriptionsUsage Usage Parser-My Subscriptions  isDailyObject From  "+ usageObject.getString("isDaily") +" :AND: "+isDaily+ offeringId,
				logger);
	    SupplementaryOfferingsResponseData supplementaryOfferingsMainObj=new SupplementaryOfferingsResponseData();
	    boolean isOfferFound = false;
	    if (AppCache.getHashmapMySubscriptions().containsKey(
		    this.getKeyForCache(Constants.HASH_KEY_My_SUBSCRIPTIONS, offeringId, mySubscriptionsRequest))) {
		Utilities.printDebugLog(mySubscriptionsRequest.getMsisdn()
			+ "-Usage Parser-My Subscriptions data found in cache against offering id: " + offeringId + " AND KEY :"+this.getKeyForCache(Constants.HASH_KEY_My_SUBSCRIPTIONS, offeringId, mySubscriptionsRequest),
			logger);

		 supplementaryOfferingsMainObj =new SupplementaryOfferingsResponseData(AppCache.getHashmapMySubscriptions().get(
			this.getKeyForCache(Constants.HASH_KEY_My_SUBSCRIPTIONS, offeringId, mySubscriptionsRequest)));
		 
	    
	    }else
	    {
	    	Utilities.printDebugLog(mySubscriptionsRequest.getMsisdn()
	    			+ "-Usage Parser-My Subscriptions data NOT found in cache against offering id: " + offeringId + " AND KEY :"+this.getKeyForCache(Constants.HASH_KEY_My_SUBSCRIPTIONS, offeringId, mySubscriptionsRequest),
	    			logger);
	    }
	    SupplementryOfferingsData offerCall = this
			    .searchAndFetchData(supplementaryOfferingsMainObj.getCall().getOffers(), offeringId);
		    if (offerCall != null) {
		    	offerCall.getHeader().setStatus(null);
			offerCall.getHeader().setIsDaily(isDaily);
			offerCall.getHeader().setUsage(this.parseUsageData(offerCall.getHeader().getUsage(),
				usageObject.getJSONArray("usage"), mySubscriptionsRequest.getLang()));
			offerCall.getHeader().setAttributeList(null);
			resData.getCall().getOffers().add(offerCall);
			isOfferFound = true;
		    }

		    if (!isOfferFound) {
			SupplementryOfferingsData offerInternet = this
				.searchAndFetchData(supplementaryOfferingsMainObj.getInternet().getOffers(), offeringId);
			if (offerInternet != null) {
					
			    offerInternet.getHeader().setStatus(null);
			    offerInternet.getHeader().setIsDaily(isDaily);
			    offerInternet.getHeader().setUsage(this.parseUsageData(offerInternet.getHeader().getUsage(),
				    usageObject.getJSONArray("usage"), mySubscriptionsRequest.getLang()));
			    offerInternet.getHeader().setAttributeList(null);
			    resData.getInternet().getOffers().add(offerInternet);
			    isOfferFound = true;

			}
		    }

		    if (!isOfferFound) {
		    
			SupplementryOfferingsData offerSMS = this
				.searchAndFetchData(supplementaryOfferingsMainObj.getSms().getOffers(), offeringId);
			if (offerSMS != null) {
		
			    offerSMS.getHeader().setStatus(null);
			    offerSMS.getHeader().setIsDaily(isDaily);
			    offerSMS.getHeader().setUsage(this.parseUsageData(offerSMS.getHeader().getUsage(),
				    usageObject.getJSONArray("usage"), mySubscriptionsRequest.getLang()));
			    offerSMS.getHeader().setAttributeList(null);
			    resData.getSms().getOffers().add(offerSMS);
			    isOfferFound = true;
			}
		    }
		    if (!isOfferFound) {
			SupplementryOfferingsData offerTM = this
				.searchAndFetchData(supplementaryOfferingsMainObj.getTm().getOffers(), offeringId);
			if (offerTM != null) {

			    offerTM.getHeader().setStatus(null);
			    offerTM.getHeader().setIsDaily(isDaily);
			    offerTM.getHeader().setUsage(this.parseUsageData(offerTM.getHeader().getUsage(),
				    usageObject.getJSONArray("usage"), mySubscriptionsRequest.getLang()));
			    resData.getTm().getOffers().add(offerTM);
			    isOfferFound = true;
			}
		    }
		    if (!isOfferFound) {
			SupplementryOfferingsData offerCampaign = this
				.searchAndFetchData(supplementaryOfferingsMainObj.getCampaign().getOffers(), offeringId);
			if (offerCampaign != null) {
	
			    offerCampaign.getHeader().setStatus(null);
			    offerCampaign.getHeader().setIsDaily(isDaily);
			    offerCampaign.getHeader().setUsage(this.parseUsageData(offerCampaign.getHeader().getUsage(),
				    usageObject.getJSONArray("usage"), mySubscriptionsRequest.getLang()));
			    offerCampaign.getHeader().setAttributeList(null);
			    resData.getCampaign().getOffers().add(offerCampaign);
			    isOfferFound = true;
			}
		    }
		    if (!isOfferFound) {
			SupplementryOfferingsData offerHybrid = this
				.searchAndFetchData(supplementaryOfferingsMainObj.getHybrid().getOffers(), offeringId);
			if (offerHybrid != null) {
				offerHybrid.getHeader().setStatus(null);
			    offerHybrid.getHeader().setIsDaily(isDaily);
			    offerHybrid.getHeader().setUsage(this.parseUsageData(offerHybrid.getHeader().getUsage(),
				    usageObject.getJSONArray("usage"), mySubscriptionsRequest.getLang()));
			    offerHybrid.getHeader().setAttributeList(null);
			    resData.getHybrid().getOffers().add(offerHybrid);
			    isOfferFound = true;
			}
		    }
		    if (!isOfferFound) {
			SupplementryOfferingsData offerRoaming = this
				.searchAndFetchData(supplementaryOfferingsMainObj.getRoaming().getOffers(), offeringId);
			if (offerRoaming != null) {
		
			    offerRoaming.getHeader().setStatus(null);
			    offerRoaming.getHeader().setIsDaily(isDaily);
			    offerRoaming.getHeader().setUsage(this.parseUsageData(offerRoaming.getHeader().getUsage(),
				    usageObject.getJSONArray("usage"), mySubscriptionsRequest.getLang()));
			    offerRoaming.getHeader().setAttributeList(null);
			    offerRoaming.getHeader().setOfferGroup(null);
			    resData.getRoaming().getOffers().add(offerRoaming);
			    isOfferFound = true;
			}
    }

	  
	    

	    /*
	     * At this step we are confirmed that offer is not found in
	     * cache.Therefore adding into the list which will be sent to
	     * MAGENTO to fetch Data against remaining IDs.
	     */

	    if (!isOfferFound) {
		Utilities.printDebugLog(mySubscriptionsRequest.getMsisdn() + "-Usage Parser offering ID:" + offeringId
			+ " not found in cached offers.", logger);
		remainingUsageObjects.put(usageOfferingIDs.getJSONObject(i));
		remainingOfferingIDs.put(usageOfferingIDs.getJSONObject(i).getString("offeringId"));
	    }

	} // for loop ended.

	if (remainingUsageObjects.length() > 0 && !isOffersFetched) {
	    isOffersFetched = true;
	    Utilities.printDebugLog(mySubscriptionsRequest.getMsisdn()
		    + "-Usage Parser-Following Offerings IDs not found in cache." + remainingUsageObjects, logger);
	    Utilities.printDebugLog(mySubscriptionsRequest.getMsisdn()
		    + "-Usage Parser-Supplementary Offerings cache size Before Call: "
		    + AppCache.getHashmapMySubscriptions().size(), logger);
	    SupplementaryOfferingsBusiness supplementaryOfferingsBusiness = new SupplementaryOfferingsBusiness();

	    SupplementaryOfferingsByIDsRequest supplementaryOfferingsRequest = new SupplementaryOfferingsByIDsRequest();
	    SupplementaryOfferingsResponse supplementaryOfferingsResponse = new SupplementaryOfferingsResponse();

	    supplementaryOfferingsRequest.setChannel(mySubscriptionsRequest.getChannel());
	    supplementaryOfferingsRequest.setiP(mySubscriptionsRequest.getiP());
	    supplementaryOfferingsRequest.setLang(mySubscriptionsRequest.getLang());
	    supplementaryOfferingsRequest.setMsisdn(mySubscriptionsRequest.getMsisdn());
	    TypeReference<List<String>> tRef = new TypeReference<List<String>>() {
	    };
	    List<String> lstUser = mapper.readValue(remainingOfferingIDs.toString(), tRef);
	    supplementaryOfferingsRequest.setOfferingIds(lstUser);

	    supplementaryOfferingsBusiness.getSupplementaryOfferingsByIDs(mySubscriptionsRequest.getMsisdn(),
		    supplementaryOfferingsRequest, supplementaryOfferingsResponse);

	    resData = parseUsageResponse(remainingUsageObjects, mySubscriptionsRequest, resData, isOffersFetched);
	   
	    Utilities.printDebugLog(mySubscriptionsRequest.getMsisdn()
		    + "-Usage Parser-Supplementary Offerings cache size after fetching offers from magento: "
		    + AppCache.getHashmapMySubscriptions().size(), logger);

	}
	parseInclusiveOffers(resData, mySubscriptionsRequest.getMsisdn());

	return resData;

    }

    private void addUsualOffersToInclusive(SupplementaryOfferingsResponseData resData) {
	/**
	 * Adding usual offers to all inclusive if all inclusive array contains
	 * data.
	 */
	if (!resData.getVoiceInclusiveOffers().getOffers().isEmpty())
	    resData.getVoiceInclusiveOffers().getOffers().addAll(resData.getCall().getOffers());
	if (!resData.getInternetInclusiveOffers().getOffers().isEmpty())
	    resData.getInternetInclusiveOffers().getOffers().addAll(resData.getInternet().getOffers());
	if (!resData.getSmsInclusiveOffers().getOffers().isEmpty())
	    resData.getSmsInclusiveOffers().getOffers().addAll(resData.getSms().getOffers());

    }

    private void parseInclusiveOffers(SupplementaryOfferingsResponseData resData, String msisdn)
	    throws SocketException {
	// Parsing for Call inclusive offers.
	Utilities.printDebugLog(msisdn + "Parsing All Inclusive Call", logger);
	ArrayList<String> skipppingIDs=new ArrayList<String>();
	for (int i = 0; i < resData.getCall().getOffers().size(); i++) {
		if(skipppingIDs.contains(resData.getCall().getOffers().get(i).getHeader().getOfferingId()))
		{
			
		}
		else{
		skipppingIDs.add(resData.getCall().getOffers().get(i).getHeader().getOfferingId());
	    
		List<HeaderUsage> usageList = resData.getCall().getOffers().get(i).getHeader().getUsage();
	    for (HeaderUsage usage : usageList) {
		
	    	if (!usage.getType().isEmpty()
			&& !usage.getType().equalsIgnoreCase(Constants.MY_SUBSCRIPTIONS_FREE_USAGE_CALL)) {

		    if (usage.getType().equalsIgnoreCase(Constants.MY_SUBSCRIPTIONS_FREE_USAGE_SMS))
			resData.getSmsInclusiveOffers().getOffers().add(resData.getCall().getOffers().get(i));
		    if (usage.getType().equalsIgnoreCase(Constants.MY_SUBSCRIPTIONS_FREE_USAGE_INTERNET))
			resData.getInternetInclusiveOffers().getOffers().add(resData.getCall().getOffers().get(i));
	    	}
		}
	    }

	}

	// Parsing for Internet inclusive offers.
	Utilities.printDebugLog(msisdn + "Parsing All Inclusive Internet", logger);
	for (int i = 0; i < resData.getInternet().getOffers().size(); i++) {

	    List<HeaderUsage> usageList = resData.getInternet().getOffers().get(i).getHeader().getUsage();
	    for (HeaderUsage usage : usageList) {
		if (!usage.getType().isEmpty()
			&& !usage.getType().equalsIgnoreCase(Constants.MY_SUBSCRIPTIONS_FREE_USAGE_INTERNET)) {

		    if (usage.getType().equalsIgnoreCase(Constants.MY_SUBSCRIPTIONS_FREE_USAGE_SMS))
			resData.getSmsInclusiveOffers().getOffers().add(resData.getInternet().getOffers().get(i));
		    if (usage.getType().equalsIgnoreCase(Constants.MY_SUBSCRIPTIONS_FREE_USAGE_CALL))
			resData.getVoiceInclusiveOffers().getOffers().add(resData.getInternet().getOffers().get(i));
		}
	    }

	}

	// Parsing for SMS inclusive offers.
	Utilities.printDebugLog(msisdn + "Parsing All Inclusive SMS", logger);
	for (int i = 0; i < resData.getSms().getOffers().size(); i++) {
	    List<HeaderUsage> usageList = resData.getSms().getOffers().get(i).getHeader().getUsage();
	    for (HeaderUsage usage : usageList) {
		if (!usage.getType().isEmpty()
			&& !usage.getType().equalsIgnoreCase(Constants.MY_SUBSCRIPTIONS_FREE_USAGE_SMS)) {

		    if (usage.getType().equalsIgnoreCase(Constants.MY_SUBSCRIPTIONS_FREE_USAGE_INTERNET))
			resData.getInternetInclusiveOffers().getOffers().add(resData.getSms().getOffers().get(i));
		    if (usage.getType().equalsIgnoreCase(Constants.MY_SUBSCRIPTIONS_FREE_USAGE_CALL))
			resData.getVoiceInclusiveOffers().getOffers().add(resData.getSms().getOffers().get(i));
		}
	    }

	}
	// Parsing for Hybrid inclusive offers.
	Utilities.printDebugLog(msisdn + "Parsing All Inclusive Hybrid", logger);
	for (int i = 0; i < resData.getHybrid().getOffers().size(); i++) {
	    List<HeaderUsage> usageList = resData.getHybrid().getOffers().get(i).getHeader().getUsage();
	    for (HeaderUsage usage : usageList) {
		if (!usage.getType().isEmpty()) {
		    Utilities.printDebugLog(msisdn + "Hybrid Internet Usage ID-" + usage.getType(), logger);

		    if (usage.getType().equalsIgnoreCase(Constants.MY_SUBSCRIPTIONS_FREE_USAGE_INTERNET)) {
			Utilities.printDebugLog(msisdn + "Adding into Hybrid Internet-" + usage.getType(), logger);
			Utilities.printDebugLog(msisdn + "Hybrid Internet List size-"
				+ resData.getInternetInclusiveOffers().getOffers().size(), logger);
			if (!isExists(resData.getInternetInclusiveOffers().getOffers(),
				resData.getHybrid().getOffers().get(i).getHeader().getId())) {
			    resData.getInternetInclusiveOffers().getOffers()
				    .add(resData.getHybrid().getOffers().get(i));
			}
		    }

		    if (usage.getType().equalsIgnoreCase(Constants.MY_SUBSCRIPTIONS_FREE_USAGE_CALL)) {
			Utilities.printDebugLog(msisdn + "Adding into Hybrid CALL-" + usage.getType(), logger);
			Utilities.printDebugLog(msisdn + "Hybrid Call List size-"
				+ resData.getVoiceInclusiveOffers().getOffers().size(), logger);
			if (!isExists(resData.getInternetInclusiveOffers().getOffers(),
				resData.getHybrid().getOffers().get(i).getHeader().getId())) {
			    resData.getVoiceInclusiveOffers().getOffers().add(resData.getHybrid().getOffers().get(i));
			}
		    }
		    if (usage.getType().equalsIgnoreCase(Constants.MY_SUBSCRIPTIONS_FREE_USAGE_SMS)) {
			Utilities.printDebugLog(msisdn + "Adding into Hybrid SMS-" + usage.getType(), logger);
			Utilities.printDebugLog(
				msisdn + "Hybrid SMS List size-" + resData.getSmsInclusiveOffers().getOffers().size(),
				logger);
			if (!isExists(resData.getInternetInclusiveOffers().getOffers(),
				resData.getHybrid().getOffers().get(i).getHeader().getId())) {
			    resData.getSmsInclusiveOffers().getOffers().add(resData.getHybrid().getOffers().get(i));
			}
		    }
		}
	    }

	}

    }

    private boolean isExists(List<SupplementryOfferingsData> offers, String id) {
	for (SupplementryOfferingsData s : offers)
	    if (s.getHeader().getId().equalsIgnoreCase(id))
		return true;

	return false;
    }

    private SupplementryOfferingsData searchAndFetchData(List<SupplementryOfferingsData> offers, String offeringId)
	    throws SocketException {
	SupplementryOfferingsData offer = null;
	for (int i = 0; i < offers.size(); i++) {
	    if (offers.get(i).getHeader().getOfferingId().trim().equalsIgnoreCase(offeringId.trim())) {
		Utilities.printDebugLog("-Usage Parser-Offering ID  (" + offeringId + ") has found in offer type:"
			+ offers.get(i).getHeader().getType(), logger);
		offer = offers.get(i);
	    }
	}
	return offer;

    }

    private List<HeaderUsage> parseUsageData(List<HeaderUsage> usageList, JSONArray jsonArray, String lang)
	    throws JSONException, ParseException, SocketException, SQLException {
	Utilities.printDebugLog("-Usage Parser- Usage List Data-" + jsonArray, logger);
	usageList = new ArrayList<>();
	for (int i = 0; i < jsonArray.length(); i++) {
	    JSONObject usageObject = jsonArray.getJSONObject(i);
	    HeaderUsage usage = new HeaderUsage();

	    usage.setActivationDate(Utilities.getDateFormatForUsage(usageObject.getString("initialDate")));
	    usage.setRenewalDate(Utilities.getDateFormatForUsage(usageObject.getString("expiryDate")));
	    usage.setIconName(GetConfigurations.getConfigurationFromCache(usageObject.getString("icon")));
	    usage.setRemainingTitle(
		    GetMessagesMappings.getLabelsFromResourceBundle(usageObject.getString("icon"), lang));
	    usage.setRenewalTitle(
		    GetMessagesMappings.getLabelsFromResourceBundle("mySubscriptions.renewalTitle", lang));
	    usage.setTotalUsage(usageObject.getString("totalUnits"));
	    usage.setRemainingUsage(usageObject.getString("remainingUnits"));
	    usage.setUnit(GetMessagesMappings
		    .getLabelsFromResourceBundle(usageObject.getString("unitName").toLowerCase(), lang));
	    usage.setType(usageObject.getString("type"));
	    usageList.add(usage);
	}
	return usageList;
    }

    private String getKeyForCache(String hashKeySupplementaryOfferings, String offeringID,
	    MySubscriptionsRequest mySubscriptionsRequest) throws SocketException {
	Utilities.printDebugLog(
		mySubscriptionsRequest.getMsisdn() + "-Mysubscriptions-> Supplementary Offerings (cached)-> Cache key-"
			+ hashKeySupplementaryOfferings + "." + offeringID + "." + mySubscriptionsRequest.getLang(),
		logger);

	return hashKeySupplementaryOfferings + "." + offeringID + "." + mySubscriptionsRequest.getLang();
    }
  public static void main(String[] args) {
	ArrayList<String> user1=new ArrayList<String>();
	ArrayList<String> users2=new ArrayList<String>();
	
	user1.add("Ghufran1");
	user1.add("asif");
	user1.add("ALii");
	users2.add("Ghufran");
	users2.add("Saboor");
	if(!CollectionUtils.intersection(user1, users2).isEmpty()){
	      // has common
		System.out.println("YES FOUND");
	}
	else{
		System.out.println("NOT FOUND");
	   //no common
	}
//	CollectionsUtils.containsAny(user1, users2);
	
	
}
}
