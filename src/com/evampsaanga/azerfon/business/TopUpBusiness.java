package com.evampsaanga.azerfon.business;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONException;
import org.springframework.stereotype.Service;

import com.evampsaanga.azerfon.common.messagesmappings.GetMessagesMappings;
import com.evampsaanga.azerfon.common.utilities.Constants;
import com.evampsaanga.azerfon.common.utilities.GetConfigurations;
import com.evampsaanga.azerfon.common.utilities.Transactions;
import com.evampsaanga.azerfon.common.utilities.Utilities;
import com.evampsaanga.azerfon.models.generalservices.predefineddata.topup.AddPaymentSchedulerRequest;
import com.evampsaanga.azerfon.models.generalservices.predefineddata.topup.AddPaymentSchedulerResponse;
import com.evampsaanga.azerfon.models.generalservices.predefineddata.topup.CardDetails;
import com.evampsaanga.azerfon.models.generalservices.predefineddata.topup.DeleteFastPaymentRequest;
import com.evampsaanga.azerfon.models.generalservices.predefineddata.topup.DeleteFastPaymentResponse;
import com.evampsaanga.azerfon.models.generalservices.predefineddata.topup.DeletePaymentSchedulerRequest;
import com.evampsaanga.azerfon.models.generalservices.predefineddata.topup.DeletePaymentSchedulerResponse;
import com.evampsaanga.azerfon.models.generalservices.predefineddata.topup.DeleteSavedCardRequest;
import com.evampsaanga.azerfon.models.generalservices.predefineddata.topup.DeleteSavedCardResponse;
import com.evampsaanga.azerfon.models.generalservices.predefineddata.topup.FastPaymentDetails;
import com.evampsaanga.azerfon.models.generalservices.predefineddata.topup.FastPaymentRequest;
import com.evampsaanga.azerfon.models.generalservices.predefineddata.topup.FastPaymentResponse;
import com.evampsaanga.azerfon.models.generalservices.predefineddata.topup.FastPaymentResponseData;
import com.evampsaanga.azerfon.models.generalservices.predefineddata.topup.MakePaymentRequest;
import com.evampsaanga.azerfon.models.generalservices.predefineddata.topup.MakePaymentResponse;
import com.evampsaanga.azerfon.models.generalservices.predefineddata.topup.MakePaymentResponseData;
import com.evampsaanga.azerfon.models.generalservices.predefineddata.topup.PaymentKeyRequest;
import com.evampsaanga.azerfon.models.generalservices.predefineddata.topup.PaymentKeyResponse;
import com.evampsaanga.azerfon.models.generalservices.predefineddata.topup.PaymentKeyResponseData;
import com.evampsaanga.azerfon.models.generalservices.predefineddata.topup.PaymentSchedulerData;
import com.evampsaanga.azerfon.models.generalservices.predefineddata.topup.SavedCardRequest;
import com.evampsaanga.azerfon.models.generalservices.predefineddata.topup.SavedCardResponse;
import com.evampsaanga.azerfon.models.generalservices.predefineddata.topup.SavedCardResponseData;
import com.evampsaanga.azerfon.models.generalservices.predefineddata.topup.ScheduledPaymentsRequest;
import com.evampsaanga.azerfon.models.generalservices.predefineddata.topup.ScheduledPaymentsResponse;
import com.evampsaanga.azerfon.models.generalservices.predefineddata.topup.ScheduledPaymentsResponseData;
import com.evampsaanga.azerfon.models.generalservices.predefineddata.topup.VerifyCardRequest;
import com.evampsaanga.azerfon.models.generalservices.predefineddata.topup.VerifyCardResponse;
import com.evampsaanga.azerfon.models.generalservices.predefineddata.topup.VerifyCardResponseData;
import com.evampsaanga.azerfon.restclient.RestClient;
import com.fasterxml.jackson.databind.ObjectMapper;

@Service
public class TopUpBusiness {

	Logger logger = Logger.getLogger(QuickServicesBusiness.class);

	public SavedCardResponse getSavedCardsBusiness(String msisdn, String deviceID, SavedCardRequest savedCardRequest,
			SavedCardResponse savedCardResponse) throws JSONException, IOException, SQLException {

		Utilities.printDebugLog(msisdn + "-Request received in " + Transactions.GET_SAVED_CARDS_TRANSACTION_NAME
				+ " BUSINESS with data-" + savedCardRequest.toString(), logger);

		RestClient rc = new RestClient();
		ObjectMapper mapper = new ObjectMapper();

		String requestJsonESB = mapper.writeValueAsString(savedCardRequest);

		String path = GetConfigurations.getESBRoute("getSavedCards");
		Utilities.printDebugLog(msisdn + "<<<<<<<<< -Request Call to ESB >>>>>>>>>", logger);
		Utilities.printDebugLog(msisdn + "-Path-" + path, logger);
		Utilities.printDebugLog(msisdn + "-Request Packet-" + requestJsonESB, logger);

		savedCardResponse.getLogsReport().setEsbRequestTime(Utilities.getReportDateTime());
		String response = rc.getResponseFromESB(path, requestJsonESB);
		savedCardResponse.getLogsReport().setEsbResponseTime(Utilities.getReportDateTime());

		Utilities.printDebugLog(msisdn + "-Received response from ESB-" + response, logger);

		// Logging ESB response code and description.
		savedCardResponse.setLogsReport(
				Utilities.logESBParamsintoReportLog(requestJsonESB, response, savedCardResponse.getLogsReport()));

		if (response != null && !response.isEmpty()) {

			if (Utilities.getValueFromJSON(response, "returnCode").equalsIgnoreCase(Constants.ESB_SUCCESS_CODE)) {

				List<CardDetails> resData = new ArrayList<>();
				resData = Arrays.asList(mapper.readValue(Utilities.getValueFromJSON(response, "singleCardDetails"),
						CardDetails[].class));
				SavedCardResponseData savedCardResponseData = new SavedCardResponseData();
				savedCardResponseData.setCardDetails(resData);

				savedCardResponseData.setLastAmount(Utilities.getValueFromJSON(response, "lastAmount"));
				savedCardResponse.setData(savedCardResponseData);
				savedCardResponse.setCallStatus(Constants.Call_Status_True);
				savedCardResponse.setResultCode(Constants.APP_SERVER_SUCCESS_CODE);
				savedCardResponse.setResultDesc(
						GetMessagesMappings.getMessageFromResourceBundle("success", savedCardRequest.getLang()));

			} else {
				savedCardResponse.setCallStatus(Constants.Call_Status_False);
				savedCardResponse.setResultCode(Utilities.getValueFromJSON(response, "returnCode"));
				savedCardResponse.setResultDesc(Utilities.getErrorMessageFromFile("connectivity.error",
						savedCardRequest.getLang(), Utilities.getValueFromJSON(response, "returnCode")));
			}
		} else {

			savedCardResponse.setCallStatus(Constants.Call_Status_False);
			savedCardResponse.setResultCode(Constants.API_FAILURE_CODE);
			savedCardResponse.setResultDesc(
					GetMessagesMappings.getMessageFromResourceBundle("connectivity.error", savedCardRequest.getLang()));
		}
		return savedCardResponse;

	}

	public FastPaymentResponse getFastPaymentBusiness(String msisdn, String deviceID,
			FastPaymentRequest fastPaymentRequest, FastPaymentResponse fastPaymentResponse)
			throws JSONException, IOException, SQLException {

		Utilities.printDebugLog(msisdn + "-Request received in " + Transactions.GET_SAVED_CARDS_TRANSACTION_NAME
				+ " BUSINESS with data-" + fastPaymentRequest.toString(), logger);

		RestClient rc = new RestClient();
		ObjectMapper mapper = new ObjectMapper();

		String requestJsonESB = mapper.writeValueAsString(fastPaymentRequest);

		String path = GetConfigurations.getESBRoute("getFastPayments");
		Utilities.printDebugLog(msisdn + "<<<<<<<<< -Request Call to ESB >>>>>>>>>", logger);
		Utilities.printDebugLog(msisdn + "-Path-" + path, logger);
		Utilities.printDebugLog(msisdn + "-Request Packet-" + requestJsonESB, logger);

		fastPaymentResponse.getLogsReport().setEsbRequestTime(Utilities.getReportDateTime());
		String response = rc.getResponseFromESB(path, requestJsonESB);
		fastPaymentResponse.getLogsReport().setEsbResponseTime(Utilities.getReportDateTime());

		Utilities.printDebugLog(msisdn + "-Received response from ESB-" + response, logger);

		// Logging ESB response code and description.
		fastPaymentResponse.setLogsReport(
				Utilities.logESBParamsintoReportLog(requestJsonESB, response, fastPaymentResponse.getLogsReport()));

		if (response != null && !response.isEmpty()) {

			if (Utilities.getValueFromJSON(response, "returnCode").equalsIgnoreCase(Constants.ESB_SUCCESS_CODE)) {

				List<FastPaymentDetails> resData = new ArrayList<>();
				resData = Arrays.asList(mapper.readValue(Utilities.getValueFromJSON(response, "fastPaymentDetails"),
						FastPaymentDetails[].class));
				FastPaymentResponseData fastPaymentResponseData = new FastPaymentResponseData();
				fastPaymentResponseData.setFastPaymentDetails(resData);

				fastPaymentResponse.setData(fastPaymentResponseData);
				fastPaymentResponse.setCallStatus(Constants.Call_Status_True);
				fastPaymentResponse.setResultCode(Constants.APP_SERVER_SUCCESS_CODE);
				fastPaymentResponse.setResultDesc(
						GetMessagesMappings.getMessageFromResourceBundle("success", fastPaymentRequest.getLang()));

			} else {
				fastPaymentResponse.setCallStatus(Constants.Call_Status_False);
				fastPaymentResponse.setResultCode(Utilities.getValueFromJSON(response, "returnCode"));
				fastPaymentResponse.setResultDesc(Utilities.getErrorMessageFromFile("connectivity.error",
						fastPaymentRequest.getLang(), Utilities.getValueFromJSON(response, "returnCode")));
			}
		} else {

			fastPaymentResponse.setCallStatus(Constants.Call_Status_False);
			fastPaymentResponse.setResultCode(Constants.API_FAILURE_CODE);
			fastPaymentResponse.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("connectivity.error",
					fastPaymentRequest.getLang()));
		}
		return fastPaymentResponse;
	}

	public PaymentKeyResponse getPaymentKey(String msisdn, String deviceID, PaymentKeyRequest paymentKeyRequest,
			PaymentKeyResponse paymentKeyResponse) throws JSONException, IOException, SQLException {

		Utilities.printDebugLog(msisdn + "-Request received in " + Transactions.INITIATE_PAYMENT_TRANSACTION_NAME
				+ " BUSINESS with data-" + paymentKeyRequest.toString(), logger);

		RestClient rc = new RestClient();
		ObjectMapper mapper = new ObjectMapper();

		String requestJsonESB = mapper.writeValueAsString(paymentKeyRequest);

		String path = GetConfigurations.getESBRoute("initiatePayment");
		Utilities.printDebugLog(msisdn + "<<<<<<<<< -Request Call to ESB >>>>>>>>>", logger);
		Utilities.printDebugLog(msisdn + "-Path-" + path, logger);
		Utilities.printDebugLog(msisdn + "-Request Packet-" + requestJsonESB, logger);

		paymentKeyResponse.getLogsReport().setEsbRequestTime(Utilities.getReportDateTime());
		String response = rc.getResponseFromESB(path, requestJsonESB);
		paymentKeyResponse.getLogsReport().setEsbResponseTime(Utilities.getReportDateTime());

		Utilities.printDebugLog(msisdn + "-Received response from ESB-" + response, logger);

		// Logging ESB response code and description.
		paymentKeyResponse.setLogsReport(
				Utilities.logESBParamsintoReportLog(requestJsonESB, response, paymentKeyResponse.getLogsReport()));

		if (response != null && !response.isEmpty()) {

			if (Utilities.getValueFromJSON(response, "returnCode").equalsIgnoreCase(Constants.ESB_SUCCESS_CODE)) {

				PaymentKeyResponseData resData = new PaymentKeyResponseData();
				resData.setPaymentKey(Utilities.getValueFromJSON(response, "payment_key1"));

				paymentKeyResponse.setData(resData);
				paymentKeyResponse.setCallStatus(Constants.Call_Status_True);
				paymentKeyResponse.setResultCode(Constants.APP_SERVER_SUCCESS_CODE);
				paymentKeyResponse.setResultDesc(
						GetMessagesMappings.getMessageFromResourceBundle("success", paymentKeyRequest.getLang()));

			} else {
				paymentKeyResponse.setCallStatus(Constants.Call_Status_False);
				paymentKeyResponse.setResultCode(Utilities.getValueFromJSON(response, "returnCode"));
				paymentKeyResponse.setResultDesc(Utilities.getErrorMessageFromFile("connectivity.error",
						paymentKeyRequest.getLang(), Utilities.getValueFromJSON(response, "returnCode")));
			}
		} else {

			paymentKeyResponse.setCallStatus(Constants.Call_Status_False);
			paymentKeyResponse.setResultCode(Constants.API_FAILURE_CODE);
			paymentKeyResponse.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("connectivity.error",
					paymentKeyRequest.getLang()));
		}

		return paymentKeyResponse;

	}

	public VerifyCardResponse verifyCard(String msisdn, String deviceID, VerifyCardRequest verifyCardRequest,
			VerifyCardResponse verifyCardResponse) throws JSONException, IOException, SQLException {
		Utilities.printDebugLog(msisdn + "-Request received in " + Transactions.VERIFY_CARD_TRANSACTION_NAME
				+ " BUSINESS with data-" + verifyCardRequest.toString(), logger);

		RestClient rc = new RestClient();
		ObjectMapper mapper = new ObjectMapper();

		String requestJsonESB = mapper.writeValueAsString(verifyCardRequest);

		String path = GetConfigurations.getESBRoute("verifyCard");
		Utilities.printDebugLog(msisdn + "<<<<<<<<< -Request Call to ESB >>>>>>>>>", logger);
		Utilities.printDebugLog(msisdn + "-Path-" + path, logger);
		Utilities.printDebugLog(msisdn + "-Request Packet-" + requestJsonESB, logger);

		verifyCardResponse.getLogsReport().setEsbRequestTime(Utilities.getReportDateTime());
		String response = rc.getResponseFromESB(path, requestJsonESB);
		verifyCardResponse.getLogsReport().setEsbResponseTime(Utilities.getReportDateTime());

		Utilities.printDebugLog(msisdn + "-Received response from ESB-" + response, logger);

		// Logging ESB response code and description.
		verifyCardResponse.setLogsReport(
				Utilities.logESBParamsintoReportLog(requestJsonESB, response, verifyCardResponse.getLogsReport()));

		if (response != null && !response.isEmpty()) {

			if (Utilities.getValueFromJSON(response, "returnCode").equalsIgnoreCase(Constants.ESB_SUCCESS_CODE)) {

				VerifyCardResponseData resData = new VerifyCardResponseData();
				resData.setReturnCode(Utilities.getValueFromJSON(response, "returnCode"));

				verifyCardResponse.setData(resData);
				verifyCardResponse.setCallStatus(Constants.Call_Status_True);
				verifyCardResponse.setResultCode(Constants.APP_SERVER_SUCCESS_CODE);
				verifyCardResponse.setResultDesc(
						GetMessagesMappings.getMessageFromResourceBundle("success", verifyCardRequest.getLang()));

			} else {
				verifyCardResponse.setCallStatus(Constants.Call_Status_False);
				verifyCardResponse.setResultCode(Utilities.getValueFromJSON(response, "returnCode"));
				verifyCardResponse.setResultDesc(Utilities.getErrorMessageFromFile("connectivity.error",
						verifyCardRequest.getLang(), Utilities.getValueFromJSON(response, "returnCode")));
			}
		} else {

			verifyCardResponse.setCallStatus(Constants.Call_Status_False);
			verifyCardResponse.setResultCode(Constants.API_FAILURE_CODE);
			verifyCardResponse.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("connectivity.error",
					verifyCardRequest.getLang()));
		}

		return verifyCardResponse;

	}

	public MakePaymentResponse makePaymentBusiness(String msisdn, String deviceID,
			MakePaymentRequest makePaymentRequest, MakePaymentResponse makePaymentResponse)
			throws JSONException, IOException, SQLException {
		Utilities.printDebugLog(msisdn + "-Request received in " + Transactions.VERIFY_CARD_TRANSACTION_NAME
				+ " BUSINESS with data-" + makePaymentRequest.toString(), logger);

		RestClient rc = new RestClient();
		ObjectMapper mapper = new ObjectMapper();

		String requestJsonESB = mapper.writeValueAsString(makePaymentRequest);

		String path = GetConfigurations.getESBRoute("makePayment");
		Utilities.printDebugLog(msisdn + "<<<<<<<<< -Request Call to ESB >>>>>>>>>", logger);
		Utilities.printDebugLog(msisdn + "-Path-" + path, logger);
		Utilities.printDebugLog(msisdn + "-Request Packet-" + requestJsonESB, logger);

		makePaymentResponse.getLogsReport().setEsbRequestTime(Utilities.getReportDateTime());
		String response = rc.getResponseFromESB(path, requestJsonESB);
		makePaymentResponse.getLogsReport().setEsbResponseTime(Utilities.getReportDateTime());

		Utilities.printDebugLog(msisdn + "-Received response from ESB-" + response, logger);

		// Logging ESB response code and description.
		makePaymentResponse.setLogsReport(
				Utilities.logESBParamsintoReportLog(requestJsonESB, response, makePaymentResponse.getLogsReport()));

		if (response != null && !response.isEmpty()) {

			if (Utilities.getValueFromJSON(response, "returnCode").equalsIgnoreCase(Constants.ESB_SUCCESS_CODE)) {

				MakePaymentResponseData resData = new MakePaymentResponseData();
				resData.setResponseData(Utilities.getValueFromJSON(response, "returnCode"));

				makePaymentResponse.setData(resData);
				makePaymentResponse.setCallStatus(Constants.Call_Status_True);
				makePaymentResponse.setResultCode(Constants.APP_SERVER_SUCCESS_CODE);
				makePaymentResponse.setResultDesc(
						GetMessagesMappings.getMessageFromResourceBundle("success", makePaymentRequest.getLang()));

			} else {
				makePaymentResponse.setCallStatus(Constants.Call_Status_False);
				makePaymentResponse.setResultCode(Utilities.getValueFromJSON(response, "returnCode"));
				makePaymentResponse.setResultDesc(Utilities.getErrorMessageFromFile("connectivity.error",
						makePaymentRequest.getLang(), Utilities.getValueFromJSON(response, "returnCode")));
			}
		} else {

			makePaymentResponse.setCallStatus(Constants.Call_Status_False);
			makePaymentResponse.setResultCode(Constants.API_FAILURE_CODE);
			makePaymentResponse.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("connectivity.error",
					makePaymentRequest.getLang()));
		}


		return makePaymentResponse;

	}

	public DeleteFastPaymentResponse deleteFastPaymentBusiness(String msisdn, String deviceID,
			DeleteFastPaymentRequest deletePaymentRequest, DeleteFastPaymentResponse deletePaymentResponse)
			throws JSONException, IOException, SQLException {
		Utilities.printDebugLog(msisdn + "-Request received in " + Transactions.DELETE_PAYMENT_TRANSACTION_NAME
				+ " BUSINESS with data-" + deletePaymentRequest.toString(), logger);

		RestClient rc = new RestClient();
		ObjectMapper mapper = new ObjectMapper();

		String requestJsonESB = mapper.writeValueAsString(deletePaymentRequest);

		String path = GetConfigurations.getESBRoute("deletePayment");
		Utilities.printDebugLog(msisdn + "<<<<<<<<< -Request Call to ESB >>>>>>>>>", logger);
		Utilities.printDebugLog(msisdn + "-Path-" + path, logger);
		Utilities.printDebugLog(msisdn + "-Request Packet-" + requestJsonESB, logger);

		deletePaymentResponse.getLogsReport().setEsbRequestTime(Utilities.getReportDateTime());
		String response = rc.getResponseFromESB(path, requestJsonESB);
		deletePaymentResponse.getLogsReport().setEsbResponseTime(Utilities.getReportDateTime());

		Utilities.printDebugLog(msisdn + "-Received response from ESB-" + response, logger);

		// Logging ESB response code and description.
		deletePaymentResponse.setLogsReport(
				Utilities.logESBParamsintoReportLog(requestJsonESB, response, deletePaymentResponse.getLogsReport()));

		if (response != null && !response.isEmpty()) {

			if (Utilities.getValueFromJSON(response, "returnCode").equalsIgnoreCase(Constants.ESB_SUCCESS_CODE)) {

				String resData = Utilities.getValueFromJSON(response, "returnCode");

//				deletePaymentResponse.setData(resData);
				deletePaymentResponse.setCallStatus(Constants.Call_Status_True);
				deletePaymentResponse.setResultCode(Constants.APP_SERVER_SUCCESS_CODE);
				deletePaymentResponse.setResultDesc(
						GetMessagesMappings.getMessageFromResourceBundle("success", deletePaymentRequest.getLang()));

			} else {
				deletePaymentResponse.setCallStatus(Constants.Call_Status_False);
				deletePaymentResponse.setResultCode(Utilities.getValueFromJSON(response, "returnCode"));
				deletePaymentResponse.setResultDesc(Utilities.getErrorMessageFromFile("connectivity.error",
						deletePaymentRequest.getLang(), Utilities.getValueFromJSON(response, "returnCode")));
			}
		} else {

			deletePaymentResponse.setCallStatus(Constants.Call_Status_False);
			deletePaymentResponse.setResultCode(Constants.API_FAILURE_CODE);
			deletePaymentResponse.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("connectivity.error",
					deletePaymentRequest.getLang()));
		}

		return deletePaymentResponse;

	}

	public DeleteSavedCardResponse deleteSavedCardBusiness(String msisdn, String deviceID,
			DeleteSavedCardRequest deleteSavedCardRequest, DeleteSavedCardResponse deleteSavedCardResponse)
			throws JSONException, IOException, SQLException {
		Utilities.printDebugLog(msisdn + "-Request received in " + Transactions.DELETE_SAVED_CARD_TRANSACTION_NAME
				+ " BUSINESS with data-" + deleteSavedCardRequest.toString(), logger);

		RestClient rc = new RestClient();
		ObjectMapper mapper = new ObjectMapper();

		String requestJsonESB = mapper.writeValueAsString(deleteSavedCardRequest);

		String path = GetConfigurations.getESBRoute("deleteSavedCard");
		Utilities.printDebugLog(msisdn + "<<<<<<<<< -Request Call to ESB >>>>>>>>>", logger);
		Utilities.printDebugLog(msisdn + "-Path-" + path, logger);
		Utilities.printDebugLog(msisdn + "-Request Packet-" + requestJsonESB, logger);

		deleteSavedCardResponse.getLogsReport().setEsbRequestTime(Utilities.getReportDateTime());
		String response = rc.getResponseFromESB(path, requestJsonESB);
		deleteSavedCardResponse.getLogsReport().setEsbResponseTime(Utilities.getReportDateTime());

		Utilities.printDebugLog(msisdn + "-Received response from ESB-" + response, logger);

		// Logging ESB response code and description.
		deleteSavedCardResponse.setLogsReport(
				Utilities.logESBParamsintoReportLog(requestJsonESB, response, deleteSavedCardResponse.getLogsReport()));

		if (response != null && !response.isEmpty()) {

			if (Utilities.getValueFromJSON(response, "returnCode").equalsIgnoreCase(Constants.ESB_SUCCESS_CODE)) {

				String resData = Utilities.getValueFromJSON(response, "returnCode");

//				deleteSavedCardResponse.setData(resData);
				deleteSavedCardResponse.setCallStatus(Constants.Call_Status_True);
				deleteSavedCardResponse.setResultCode(Constants.APP_SERVER_SUCCESS_CODE);
				deleteSavedCardResponse.setResultDesc(
						GetMessagesMappings.getMessageFromResourceBundle("success", deleteSavedCardRequest.getLang()));

			} else {
				deleteSavedCardResponse.setCallStatus(Constants.Call_Status_False);
				deleteSavedCardResponse.setResultCode(Utilities.getValueFromJSON(response, "returnCode"));
				deleteSavedCardResponse.setResultDesc(Utilities.getErrorMessageFromFile("connectivity.error",
						deleteSavedCardRequest.getLang(), Utilities.getValueFromJSON(response, "returnCode")));
			}
		} else {

			deleteSavedCardResponse.setCallStatus(Constants.Call_Status_False);
			deleteSavedCardResponse.setResultCode(Constants.API_FAILURE_CODE);
			deleteSavedCardResponse.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("connectivity.error",
					deleteSavedCardRequest.getLang()));
		}

		return deleteSavedCardResponse;
	}

	public AddPaymentSchedulerResponse addPaymentSchedulerBusiness(String msisdn, String deviceID,
			AddPaymentSchedulerRequest addPaymentSchedulerRequest,
			AddPaymentSchedulerResponse addPaymentSchedulerResponse) throws JSONException, IOException, SQLException {
		Utilities.printDebugLog(msisdn + "-Request received in " + Transactions.ADD_PAYMENT_SCHEDULER_TRANSACTION_NAME
				+ " BUSINESS with data-" + addPaymentSchedulerRequest.toString(), logger);

		RestClient rc = new RestClient();
		ObjectMapper mapper = new ObjectMapper();

		String requestJsonESB = mapper.writeValueAsString(addPaymentSchedulerRequest);

		String path = GetConfigurations.getESBRoute("addPaymentScheduler");
		Utilities.printDebugLog(msisdn + "<<<<<<<<< -Request Call to ESB >>>>>>>>>", logger);
		Utilities.printDebugLog(msisdn + "-Path-" + path, logger);
		Utilities.printDebugLog(msisdn + "-Request Packet-" + requestJsonESB, logger);

		addPaymentSchedulerResponse.getLogsReport().setEsbRequestTime(Utilities.getReportDateTime());
		String response = rc.getResponseFromESB(path, requestJsonESB);
		addPaymentSchedulerResponse.getLogsReport().setEsbResponseTime(Utilities.getReportDateTime());

		Utilities.printDebugLog(msisdn + "-Received response from ESB-" + response, logger);

		// Logging ESB response code and description.
		addPaymentSchedulerResponse.setLogsReport(Utilities.logESBParamsintoReportLog(requestJsonESB, response,
				addPaymentSchedulerResponse.getLogsReport()));

		if (response != null && !response.isEmpty()) {

			if (Utilities.getValueFromJSON(response, "returnCode").equalsIgnoreCase(Constants.ESB_SUCCESS_CODE)) {

				String resData = Utilities.getValueFromJSON(response, "returnCode");

//				addPaymentSchedulerResponse.setResultCode(resData);
				addPaymentSchedulerResponse.setCallStatus(Constants.Call_Status_True);
				addPaymentSchedulerResponse.setResultCode(Constants.APP_SERVER_SUCCESS_CODE);
				addPaymentSchedulerResponse.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("success",
						addPaymentSchedulerRequest.getLang()));

			} else {
				addPaymentSchedulerResponse.setCallStatus(Constants.Call_Status_False);
				addPaymentSchedulerResponse.setResultCode(Utilities.getValueFromJSON(response, "returnCode"));
				addPaymentSchedulerResponse.setResultDesc(Utilities.getErrorMessageFromFile("connectivity.error",
						addPaymentSchedulerRequest.getLang(), Utilities.getValueFromJSON(response, "returnCode")));
			}
		} else {

			addPaymentSchedulerResponse.setCallStatus(Constants.Call_Status_False);
			addPaymentSchedulerResponse.setResultCode(Constants.API_FAILURE_CODE);
			addPaymentSchedulerResponse.setResultDesc(GetMessagesMappings
					.getMessageFromResourceBundle("connectivity.error", addPaymentSchedulerRequest.getLang()));
		}
		return addPaymentSchedulerResponse;

	}

	public DeletePaymentSchedulerResponse deletePaymentSchedulerBusiness(String msisdn, String deviceID,
			DeletePaymentSchedulerRequest deletePaymentSchedulerRequest,
			DeletePaymentSchedulerResponse deletePaymentSchedulerResponse)
			throws JSONException, IOException, SQLException {
		Utilities
				.printDebugLog(msisdn + "-Request received in " + Transactions.DELETE_PAYMENT_SCHEDULER_TRANSACTION_NAME
						+ " BUSINESS with data-" + deletePaymentSchedulerRequest.toString(), logger);

		RestClient rc = new RestClient();
		ObjectMapper mapper = new ObjectMapper();

		String requestJsonESB = mapper.writeValueAsString(deletePaymentSchedulerRequest);

		String path = GetConfigurations.getESBRoute("deletePaymentScheduler");
		Utilities.printDebugLog(msisdn + "<<<<<<<<< -Request Call to ESB >>>>>>>>>", logger);
		Utilities.printDebugLog(msisdn + "-Path-" + path, logger);
		Utilities.printDebugLog(msisdn + "-Request Packet-" + requestJsonESB, logger);

		deletePaymentSchedulerResponse.getLogsReport().setEsbRequestTime(Utilities.getReportDateTime());
		String response = rc.getResponseFromESB(path, requestJsonESB);
		deletePaymentSchedulerResponse.getLogsReport().setEsbResponseTime(Utilities.getReportDateTime());

		Utilities.printDebugLog(msisdn + "-Received response from ESB-" + response, logger);

		// Logging ESB response code and description.
		deletePaymentSchedulerResponse.setLogsReport(Utilities.logESBParamsintoReportLog(requestJsonESB, response,
				deletePaymentSchedulerResponse.getLogsReport()));

		if (response != null && !response.isEmpty()) {

			if (Utilities.getValueFromJSON(response, "returnCode").equalsIgnoreCase(Constants.ESB_SUCCESS_CODE)) {

				String resData = Utilities.getValueFromJSON(response, "returnCode");

				deletePaymentSchedulerResponse.setReturnCode(resData);
				deletePaymentSchedulerResponse.setCallStatus(Constants.Call_Status_True);
				deletePaymentSchedulerResponse.setResultCode(Constants.APP_SERVER_SUCCESS_CODE);
				deletePaymentSchedulerResponse.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("success",
						deletePaymentSchedulerRequest.getLang()));

			} else {
				deletePaymentSchedulerResponse.setCallStatus(Constants.Call_Status_False);
				deletePaymentSchedulerResponse.setResultCode(Utilities.getValueFromJSON(response, "returnCode"));
				deletePaymentSchedulerResponse.setResultDesc(Utilities.getErrorMessageFromFile("connectivity.error",
						deletePaymentSchedulerRequest.getLang(), Utilities.getValueFromJSON(response, "returnCode")));
			}
		} else {

			deletePaymentSchedulerResponse.setCallStatus(Constants.Call_Status_False);
			deletePaymentSchedulerResponse.setResultCode(Constants.API_FAILURE_CODE);
			deletePaymentSchedulerResponse.setResultDesc(GetMessagesMappings
					.getMessageFromResourceBundle("connectivity.error", deletePaymentSchedulerRequest.getLang()));
		}
		return deletePaymentSchedulerResponse;
	}

	public ScheduledPaymentsResponse getScheduledPaymentsBusiness(String msisdn, String deviceID,
			ScheduledPaymentsRequest scheduledPaymentsRequest, ScheduledPaymentsResponse scheduledPaymentsResponse)
			throws JSONException, IOException, SQLException {
		Utilities.printDebugLog(msisdn + "-Request received in " + Transactions.GET_SCHEDULED_PAYMENTS_TRANSACTION_NAME
				+ " BUSINESS with data-" + scheduledPaymentsRequest.toString(), logger);

		RestClient rc = new RestClient();
		ObjectMapper mapper = new ObjectMapper();

		String requestJsonESB = mapper.writeValueAsString(scheduledPaymentsRequest);

		String path = GetConfigurations.getESBRoute("getScheduledPayments");
		Utilities.printDebugLog(msisdn + "<<<<<<<<< -Request Call to ESB >>>>>>>>>", logger);
		Utilities.printDebugLog(msisdn + "-Path-" + path, logger);
		Utilities.printDebugLog(msisdn + "-Request Packet-" + requestJsonESB, logger);

		scheduledPaymentsResponse.getLogsReport().setEsbRequestTime(Utilities.getReportDateTime());
		String response = rc.getResponseFromESB(path, requestJsonESB);
		scheduledPaymentsResponse.getLogsReport().setEsbResponseTime(Utilities.getReportDateTime());

		Utilities.printDebugLog(msisdn + "-Received response from ESB-" + response, logger);

		// Logging ESB response code and description.
		scheduledPaymentsResponse.setLogsReport(Utilities.logESBParamsintoReportLog(requestJsonESB, response,
				scheduledPaymentsResponse.getLogsReport()));

		if (response != null && !response.isEmpty()) {

			if (Utilities.getValueFromJSON(response, "returnCode").equalsIgnoreCase(Constants.ESB_SUCCESS_CODE)) {

				ScheduledPaymentsResponseData data = new ScheduledPaymentsResponseData();
				List<PaymentSchedulerData> resData = new ArrayList<>();
				resData = Arrays
						.asList(mapper.readValue(Utilities.getValueFromJSON(response, "scheduledPaymentDetailsList"),
								PaymentSchedulerData[].class));

				data.setData(resData);

				scheduledPaymentsResponse.setData(data);
				scheduledPaymentsResponse.setCallStatus(Constants.Call_Status_True);
				scheduledPaymentsResponse.setResultCode(Constants.APP_SERVER_SUCCESS_CODE);
				scheduledPaymentsResponse.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("success",
						scheduledPaymentsRequest.getLang()));

			} else {
				scheduledPaymentsResponse.setCallStatus(Constants.Call_Status_False);
				scheduledPaymentsResponse.setResultCode(Utilities.getValueFromJSON(response, "returnCode"));
				scheduledPaymentsResponse.setResultDesc(Utilities.getErrorMessageFromFile("connectivity.error",
						scheduledPaymentsRequest.getLang(), Utilities.getValueFromJSON(response, "returnCode")));
			}
		} else {

			scheduledPaymentsResponse.setCallStatus(Constants.Call_Status_False);
			scheduledPaymentsResponse.setResultCode(Constants.API_FAILURE_CODE);
			scheduledPaymentsResponse.setResultDesc(GetMessagesMappings
					.getMessageFromResourceBundle("connectivity.error", scheduledPaymentsRequest.getLang()));
		}
		return scheduledPaymentsResponse;

	}
}
