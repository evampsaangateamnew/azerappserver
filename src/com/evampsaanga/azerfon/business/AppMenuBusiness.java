/**
 * 
 */
package com.evampsaanga.azerfon.business;

import java.io.IOException;
import java.net.SocketException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.evampsaanga.azerfon.common.messagesmappings.GetMessagesMappings;
import com.evampsaanga.azerfon.common.utilities.AppCache;
import com.evampsaanga.azerfon.common.utilities.Constants;
import com.evampsaanga.azerfon.common.utilities.GetConfigurations;
import com.evampsaanga.azerfon.common.utilities.Transactions;
import com.evampsaanga.azerfon.common.utilities.Utilities;
import com.evampsaanga.azerfon.models.menues.appmenu.AppMenu;
import com.evampsaanga.azerfon.models.menues.appmenu.AppMenuListData;
import com.evampsaanga.azerfon.models.menues.appmenu.AppMenuRequest;
import com.evampsaanga.azerfon.models.menues.appmenuV2.AppMenuResponse;
import com.evampsaanga.azerfon.models.menues.appmenu.AppMenuResponseData;
import com.evampsaanga.azerfon.restclient.RestClient;

/**
 * @author Evamp & Saanga
 *
 */
public class AppMenuBusiness {
	Logger logger = Logger.getLogger(AppMenuBusiness.class);

	public AppMenuResponse getAppMenuBusiness(String msisdn, AppMenuRequest appMenuRequest,
			AppMenuResponse appMenuResponse)
			throws JsonProcessingException, JSONException, SocketException, SQLException 
	{

		Utilities.printDebugLog(msisdn + "-Request received in " + Transactions.APP_MENU_TRANSACTION_NAME
				+ " BUSINESS with data-" + appMenuRequest.toString(), logger);

		// Get key which will be used to store and retrieve data from hash map.
		String appMenuCacheKey = this.getKeyForCache(Constants.HASH_KEY_APP_MENU, appMenuRequest.getOfferingName());

		if (AppCache.getHashmapAppMenu().containsKey(appMenuCacheKey)) {
			Utilities.printDebugLog(msisdn + "-APP MENU" + Constants.CACHE_EXISTS_DESCRIPTION + "" + appMenuCacheKey,
					logger);
			appMenuResponse = AppCache.getHashmapAppMenu().get(appMenuCacheKey);
			appMenuResponse.getLogsReport().setIsCached("true");
			return appMenuResponse;

		} else {
			Utilities.printDebugLog(
					msisdn + "-APP MENU" + Constants.CACHE_DOES_NOT_EXIST_DESCRIPTION + "" + appMenuCacheKey, logger);
			AppMenuResponseData resData = new AppMenuResponseData();
			RestClient rc = new RestClient();
			ObjectMapper mapper = new ObjectMapper();
			com.evampsaanga.azerfon.models.menues.appmenuV2.Data listData = new com.evampsaanga.azerfon.models.menues.appmenuV2.Data();

			String requestJsonESB = mapper.writeValueAsString(appMenuRequest);

			String path = GetConfigurations.getESBRoute("getAppMenu");
			Utilities.printDebugLog(msisdn + "-Request Call to ESB", logger);
			Utilities.printDebugLog(msisdn + "-Path-" + path, logger);
			Utilities.printDebugLog(msisdn + "-Request Packet-" + requestJsonESB, logger);
			// String response = HardCodedResponses.GET_APP_MENU;

			appMenuResponse.getLogsReport().setEsbRequestTime(Utilities.getReportDateTime());
			String response = rc.getResponseFromESB(path, requestJsonESB);
			appMenuResponse.getLogsReport().setEsbResponseTime(Utilities.getReportDateTime());

			Utilities.printDebugLog(msisdn + "-Received response from ESB-" + response, logger);

			// Logging ESB response code and description.
			appMenuResponse.setLogsReport(
					Utilities.logESBParamsintoReportLog(requestJsonESB, response, appMenuResponse.getLogsReport()));

			if (response != null && !response.isEmpty()) {

				if (Utilities.getValueFromJSON(response, "returnCode").equalsIgnoreCase(Constants.ESB_SUCCESS_CODE)) {

					String responseData = new JSONObject(response).get("data").toString();

					try {

						listData = mapper.readValue(responseData,
								com.evampsaanga.azerfon.models.menues.appmenuV2.Data.class);

						// resData.setData(listData);

						appMenuResponse.setData(listData);
						appMenuResponse.setCallStatus(Constants.Call_Status_True);
						appMenuResponse.setResultCode(Constants.APP_SERVER_SUCCESS_CODE);
						appMenuResponse.setResultDesc(
								GetMessagesMappings.getMessageFromResourceBundle("success", appMenuRequest.getLang()));

						// Storing response in hashmap.
						AppCache.getHashmapAppMenu().put(appMenuCacheKey, appMenuResponse);

						// Caching Time-stamp
						AppCache.getHashmapTimestamps().put(Constants.HASH_KEY_APP_MENU,
								Utilities.getTimeStampForCache(Constants.HASH_KEY_APP_MENU));

						return appMenuResponse;

					} catch (IOException e) {

						logger.error("ERROR:", e);
						appMenuResponse.setCallStatus(Constants.Call_Status_False);
						appMenuResponse.setResultCode(Utilities.getValueFromJSON(response, "returnCode"));
						appMenuResponse.setResultDesc(Utilities.getErrorMessageFromFile("app.menu",
								appMenuRequest.getLang(), Utilities.getValueFromJSON(response, "returnCode")));

						return appMenuResponse;
					}

				} else {

					appMenuResponse.setCallStatus(Constants.Call_Status_False);
					appMenuResponse.setResultCode(Utilities.getValueFromJSON(response, "returnCode"));
					appMenuResponse.setResultDesc(Utilities.getErrorMessageFromFile("app.menu",
							appMenuRequest.getLang(), Utilities.getValueFromJSON(response, "returnCode")));
				}
			} else {

				appMenuResponse.setCallStatus(Constants.Call_Status_False);
				appMenuResponse.setResultCode(Constants.API_FAILURE_CODE);
				appMenuResponse.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("connectivity.error",
						appMenuRequest.getLang()));
			}

			return appMenuResponse;
		}
	}

	public com.evampsaanga.azerfon.models.menues.appmenuV2.AppMenuResponse getAppMenuBusinessV2(String msisdn,
			AppMenuRequest appMenuRequest,
			com.evampsaanga.azerfon.models.menues.appmenuV2.AppMenuResponse appMenuResponse)
			throws JsonProcessingException, JSONException, SocketException, SQLException {

		Utilities.printDebugLog(msisdn + "-Request received in " + Transactions.APP_MENU_TRANSACTION_NAME
				+ " BUSINESS with data-" + appMenuRequest.toString(), logger);

		// Get key which will be used to store and retrieve data from hash map.
		String appMenuCacheKey = this.getKeyForCache(Constants.HASH_KEY_APP_MENU, appMenuRequest.getLang());

		/*
		 * if (AppCache.getHashmapAppMenu().containsKey(appMenuCacheKey)) {
		 * Utilities.printDebugLog(msisdn + "-APP MENU" +
		 * Constants.CACHE_EXISTS_DESCRIPTION + "" + appMenuCacheKey, logger);
		 * appMenuResponse = AppCache.getHashmapAppMenu().get(appMenuCacheKey);
		 * appMenuResponse.getLogsReport().setIsCached("true"); return appMenuResponse;
		 * 
		 * } else
		 */ {
			Utilities.printDebugLog(
					msisdn + "-APP MENU" + Constants.CACHE_DOES_NOT_EXIST_DESCRIPTION + "" + appMenuCacheKey, logger);
			// com.evampsaanga.azerfon.models.menues.appmenuV2.AppMenuResponseData
			// resData = new
			// com.evampsaanga.azerfon.models.menues.appmenuV2.AppMenuResponseData();
			RestClient rc = new RestClient();
			ObjectMapper mapper = new ObjectMapper();
			com.evampsaanga.azerfon.models.menues.appmenuV2.Data listData = new com.evampsaanga.azerfon.models.menues.appmenuV2.Data();
			String requestJsonESB = mapper.writeValueAsString(appMenuRequest);

			String path = GetConfigurations.getESBRoute("getAppMenuV2");
			Utilities.printDebugLog(msisdn + "-Request Call to ESB", logger);
			Utilities.printDebugLog(msisdn + "-Path-" + path, logger);
			Utilities.printDebugLog(msisdn + "-Request Packet-" + requestJsonESB, logger);
			// String response = HardCodedResponses.GET_APP_MENU;

			appMenuResponse.getLogsReport().setEsbRequestTime(Utilities.getReportDateTime());
			String response = rc.getResponseFromESB(path, requestJsonESB);
			appMenuResponse.getLogsReport().setEsbResponseTime(Utilities.getReportDateTime());

			// com.evampsaanga.azerfon.models.menues.appmenuV2.AppMenuResponseData
			// responseData = new
			// com.evampsaanga.azerfon.models.menues.appmenuV2.AppMenuResponseData();
			Utilities.printDebugLog(msisdn + "-Received response from ESB-" + response, logger);

			// Logging ESB response code and description.
			appMenuResponse.setLogsReport(
					Utilities.logESBParamsintoReportLog(requestJsonESB, response, appMenuResponse.getLogsReport()));

			if (response != null && !response.isEmpty()) {

				if (Utilities.getValueFromJSON(response, "returnCode").equalsIgnoreCase(Constants.ESB_SUCCESS_CODE)) {

					String responseData = new JSONObject(response).get("data").toString();
					try {
						listData = mapper.readValue(responseData,
								com.evampsaanga.azerfon.models.menues.appmenuV2.Data.class);
						// resData.setData(listData);
						appMenuResponse.setData(listData);
						appMenuResponse.setCallStatus(Constants.Call_Status_True);
						appMenuResponse.setResultCode(Constants.APP_SERVER_SUCCESS_CODE);
						appMenuResponse.setResultDesc(
								GetMessagesMappings.getMessageFromResourceBundle("success", appMenuRequest.getLang()));
						return appMenuResponse;
					} catch (IOException e) {
						// TODO Auto-generated catch block
						logger.error("ERROR:", e);
						appMenuResponse.setCallStatus(Constants.Call_Status_False);
						appMenuResponse.setResultCode(Utilities.getValueFromJSON(response, "returnCode"));
						appMenuResponse.setResultDesc(Utilities.getErrorMessageFromFile("app.menu",
								appMenuRequest.getLang(), Utilities.getValueFromJSON(response, "returnCode")));
						return appMenuResponse;
					}

					// Storing response in hashmap.
					// AppCache.getHashmapAppMenu().put(appMenuCacheKey,
					// appMenuResponse);

					// Caching Time-stamp
					/*
					 * AppCache.getHashmapTimestamps().put(Constants. HASH_KEY_APP_MENU,
					 * Utilities.getTimeStampForCache(Constants. HASH_KEY_APP_MENU));
					 */
				} else {

					appMenuResponse.setCallStatus(Constants.Call_Status_True);
					appMenuResponse.setResultCode(Utilities.getValueFromJSON(response, "returnCode"));
					appMenuResponse.setResultDesc(Utilities.getErrorMessageFromFile("app.menu",
							appMenuRequest.getLang(), Utilities.getValueFromJSON(response, "returnCode")));
				}
			} else {

				appMenuResponse.setCallStatus(Constants.Call_Status_False);
				appMenuResponse.setResultCode(Constants.API_FAILURE_CODE);
				appMenuResponse.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("connectivity.error",
						appMenuRequest.getLang()));
			}

			return appMenuResponse;
		}
	}

	private List<AppMenuListData> parseFAQsData(String responseArray) throws JSONException {
		List<AppMenuListData> listOfAppMenuListData = new ArrayList<AppMenuListData>();

		JSONArray jsonArray = new JSONArray(responseArray);
		for (int i = 0; i < jsonArray.length(); i++) {
			JSONObject obj = jsonArray.getJSONObject(i);
			List<AppMenu> appMenuList = new ArrayList<>();
			String type = obj.getString("type");
			AppMenuListData appMenuListData = new AppMenuListData();
			appMenuListData.setMenuHeader(type);
			if (!isCategoryExists(type, listOfAppMenuListData)) {

				for (int j = 0; j < jsonArray.length(); j++) {
					JSONObject obj1 = jsonArray.getJSONObject(j);

					if (type.equalsIgnoreCase(obj1.getString("type"))) {
						AppMenu appMenu = new AppMenu();

						appMenu.setIdentifier(Utilities.setEmptyIfNull(obj1.getString("identifier")));
						appMenu.setTitle(Utilities.setEmptyIfNull(obj1.getString("title")));
						appMenu.setSortOrder(Utilities.setEmptyIfNull(obj1.getString("sortOrder")));

						appMenuList.add(appMenu);
					} else {
						appMenuListData.setMenuList(appMenuList);

					}
				}
				listOfAppMenuListData.add(appMenuListData);
			}
		}
		return listOfAppMenuListData;
	}

	private boolean isCategoryExists(String type, List<AppMenuListData> listOfAppMenuListData) {
		boolean flag = false;
		for (int i = 0; i < listOfAppMenuListData.size(); i++) {
			if (type.equalsIgnoreCase(listOfAppMenuListData.get(i).getMenuHeader()))
				flag = true;
		}
		return flag;
	}

	private String getKeyForCache(String hashKeyAppMenu, String lang) {

		return hashKeyAppMenu + "." + lang;
	}
}
