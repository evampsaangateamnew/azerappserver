package com.evampsaanga.azerfon.business;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.log4j.Logger;
import com.evampsaanga.azerfon.common.messagesmappings.GetMessagesMappings;
import com.evampsaanga.azerfon.common.utilities.AppCache;
import com.evampsaanga.azerfon.common.utilities.Constants;
import com.evampsaanga.azerfon.common.utilities.GetConfigurations;
import com.evampsaanga.azerfon.common.utilities.Transactions;
import com.evampsaanga.azerfon.common.utilities.Utilities;
import com.evampsaanga.azerfon.models.customerservices.customerinfo.CustomerInfoRequest;
import com.evampsaanga.azerfon.models.customerservices.customerinfo.CustomerInfoResponseData;
import com.evampsaanga.azerfon.models.supplementaryofferings.getsupplementaryofferings.ParseSupplementaryOfferingsResponse;
import com.evampsaanga.azerfon.models.supplementaryofferings.getsupplementaryofferings.internetofferings.InternetOfferingsData;
import com.evampsaanga.azerfon.models.supplementaryofferings.getsupplementaryofferings.internetofferings.InternetOfferingsRequest;
import com.evampsaanga.azerfon.models.supplementaryofferings.getsupplementaryofferings.internetofferings.InternetOfferingsResponse;
import com.evampsaanga.azerfon.models.supplementaryofferings.getsupplementaryofferings.internetofferings.InternetOfferingsResponseData;
import com.evampsaanga.azerfon.restclient.RestClient;
import com.fasterxml.jackson.databind.ObjectMapper;

public class InternetOfferingsBusiness {

	Logger logger = Logger.getLogger(InternetOfferingsBusiness.class);

	public InternetOfferingsResponse getInternetOfferings(String msisdn,
			InternetOfferingsRequest internetOfferingsRequest, InternetOfferingsResponse internetOfferingsResponse)
			throws Exception {
		Utilities.printDebugLog(msisdn + "-Request received in " + Transactions.GET_INTERNET_OFFERINGS_TRANSACTION_NAME
				+ " BUSINESS with data-" + internetOfferingsRequest.toString(), logger);
		ObjectMapper mapper = new ObjectMapper();
		// Get key which will be used to store and retrieve data from hash map.
		String internetOfferingsCacheKey = this.getKeyForCache(Constants.HASH_KEY_INTERNET_OFFERINGS,
				internetOfferingsRequest);

		if (AppCache.getHashmapInternetOfferings().containsKey(internetOfferingsCacheKey)) {
			Utilities.printDebugLog(msisdn + "-INTERNET OFFERINGS" + Constants.CACHE_EXISTS_DESCRIPTION + ""
					+ internetOfferingsCacheKey, logger);
			internetOfferingsResponse = AppCache.getHashmapInternetOfferings().get(internetOfferingsCacheKey);
			internetOfferingsResponse.getLogsReport().setIsCached("true");
			Utilities.printInfoLog(msisdn + "-Received From customerDataInternalCall SuplemntraryResponseIN_IF_CACHE-"
					+ mapper.writeValueAsString(internetOfferingsResponse), logger);
			// return internetOfferingsResponse;

		} else {
			Utilities.printDebugLog(msisdn + "-INTERNET OFFERINGS" + Constants.CACHE_DOES_NOT_EXIST_DESCRIPTION
					+ internetOfferingsCacheKey, logger);
			InternetOfferingsResponseData resData = new InternetOfferingsResponseData();
			RestClient rc = new RestClient();
			String requestJsonESB = mapper.writeValueAsString(internetOfferingsRequest);

			String path = GetConfigurations.getESBRoute("getInternetOfferings");
			Utilities.printDebugLog(msisdn + "-Request Call to ESB", logger);
			Utilities.printDebugLog(msisdn + "-Path-" + path, logger);
			Utilities.printDebugLog(msisdn + "-Request Packet-" + requestJsonESB, logger);

			internetOfferingsResponse.getLogsReport().setEsbRequestTime(Utilities.getReportDateTime());
			String response = rc.getResponseFromESB(path, requestJsonESB);
			internetOfferingsResponse.getLogsReport().setEsbResponseTime(Utilities.getReportDateTime());

			Utilities.printDebugLog(msisdn + "-Received response from ESB-" + response, logger);

			if (response != null && !response.isEmpty()) {
				// Converting String to JSON as String.
				// response = Utilities.stringToJSONString(response);

				// Logging ESB response code and description.

				internetOfferingsResponse.setLogsReport(Utilities.logESBParamsintoReportLog(requestJsonESB, response,
						internetOfferingsResponse.getLogsReport()));

				if (Utilities.getValueFromJSON(response, "returnCode").equalsIgnoreCase(Constants.ESB_SUCCESS_CODE)) {

					resData = ParseSupplementaryOfferingsResponse.parseInternetOfferingsData(msisdn,
							Utilities.getValueFromJSON(response, "data"), resData);

					// InternetOfferingsResponseData response=TmProcessing(resData);

					// Storing response in hashmap.

					internetOfferingsResponse.setData(resData);
					Utilities.printInfoLog(
							msisdn + "-Received From customerDataInternalCall SuplemntraryResponsesBeforeCache-"
									+ mapper.writeValueAsString(internetOfferingsResponse),
							logger);
					AppCache.getHashmapInternetOfferings().put(internetOfferingsCacheKey, internetOfferingsResponse);
					// Caching Time-stamp

					Utilities.printInfoLog(
							msisdn + "-Received From customerDataInternalCall SuplemntraryResponsesAFTERCache-"
									+ mapper.writeValueAsString(
											AppCache.getHashmapInternetOfferings().get(internetOfferingsCacheKey)),
							logger);
					AppCache.getHashmapTimestamps().put(Constants.HASH_KEY_INTERNET_OFFERINGS,
							Utilities.getTimeStampForCache(Constants.HASH_KEY_INTERNET_OFFERINGS));

					internetOfferingsResponse.setCallStatus(Constants.Call_Status_True);
					internetOfferingsResponse.setResultCode(Constants.APP_SERVER_SUCCESS_CODE);
					internetOfferingsResponse.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("success",
							internetOfferingsRequest.getLang()));

				} else {
					/*
					 * This else is needed just to show empty cards on mobile by sending success APP
					 * SERVER code
					 */
					internetOfferingsResponse.setData(resData);
					internetOfferingsResponse.setCallStatus(Constants.Call_Status_True);
					internetOfferingsResponse.setResultCode(Constants.APP_SERVER_SUCCESS_CODE);
					internetOfferingsResponse.setResultDesc(Utilities.getErrorMessageFromFile("internet.offers",
							internetOfferingsRequest.getLang(), Utilities.getValueFromJSON(response, "returnCode")));
				}

			} else {
				internetOfferingsResponse.setData(resData);
				internetOfferingsResponse.setCallStatus(Constants.Call_Status_False);
				internetOfferingsResponse.setResultCode(Constants.API_FAILURE_CODE);

				internetOfferingsResponse.setResultDesc(GetMessagesMappings
						.getMessageFromResourceBundle("connectivity.error", internetOfferingsRequest.getLang()));
				return internetOfferingsResponse;
			}

		}

		// START OF LOGIC , to SHOW or Hide OFFERs comparing with Customer Data
		//
		// preparing customerInfo Request Packet
		InternetOfferingsResponse responseupdated = null;
		responseupdated = (InternetOfferingsResponse) internetOfferingsResponse.clone();
		ManipulateResponseForHideVisibleAllowedChecks(msisdn, responseupdated, internetOfferingsResponse,
				internetOfferingsRequest);
		// START SETTING LOGIC TO ELIMINATE SPEICAL OFFERS FROM INTERNET
		if (!msisdn.contains("=")) {
			ArrayList<InternetOfferingsData> dailyInternetOffers = new ArrayList<InternetOfferingsData>();
			if (responseupdated.getData().getDailyBundles() != null
					&& responseupdated.getData().getDailyBundles().getOffers() != null
					&& !responseupdated.getData().getDailyBundles().getOffers().isEmpty()
					&& responseupdated.getData().getDailyBundles().getOffers().size() > 0) {
				for (int i = 0; i < responseupdated.getData().getDailyBundles().getOffers().size(); i++) {
					if (responseupdated.getData().getDailyBundles().getOffers().get(i).getHeader() != null) {
						if (!internetOfferingsRequest.getSpecialOfferIds().contains(responseupdated.getData()
								.getDailyBundles().getOffers().get(i).getHeader().getOfferingId())) {

							dailyInternetOffers.add(responseupdated.getData().getDailyBundles().getOffers().get(i));

						}
					}
				}
				responseupdated.getData().getDailyBundles().setOffers(dailyInternetOffers);
			}

			ArrayList<InternetOfferingsData> monthlyInternetOffers = new ArrayList<InternetOfferingsData>();
			if (responseupdated.getData().getMonthlyBundles() != null
					&& responseupdated.getData().getMonthlyBundles().getOffers() != null
					&& !responseupdated.getData().getMonthlyBundles().getOffers().isEmpty()
					&& responseupdated.getData().getMonthlyBundles().getOffers().size() > 0) {
				for (int i = 0; i < responseupdated.getData().getMonthlyBundles().getOffers().size(); i++) {
					if (responseupdated.getData().getMonthlyBundles().getOffers().get(i).getHeader() != null) {
						if (!internetOfferingsRequest.getSpecialOfferIds().contains(responseupdated.getData()
								.getMonthlyBundles().getOffers().get(i).getHeader().getOfferingId())) {

							monthlyInternetOffers.add(responseupdated.getData().getMonthlyBundles().getOffers().get(i));

						}
					}
				}
				responseupdated.getData().getMonthlyBundles().setOffers(monthlyInternetOffers);
			}
			
//			ArrayList<InternetOfferingsData> hourlyInternetOffers = new ArrayList<InternetOfferingsData>();
//			if (responseupdated.getData().getHourlyBundles() != null
//					&& responseupdated.getData().getHourlyBundles().getOffers() != null
//					&& !responseupdated.getData().getHourlyBundles().getOffers().isEmpty()
//					&& responseupdated.getData().getHourlyBundles().getOffers().size() > 0) {
//				for (int i = 0; i < responseupdated.getData().getHourlyBundles().getOffers().size(); i++) {
//					if (responseupdated.getData().getHourlyBundles().getOffers().get(i).getHeader() != null) {
//						if (!internetOfferingsRequest.getSpecialOfferIds().contains(responseupdated.getData()
//								.getHourlyBundles().getOffers().get(i).getHeader().getOfferingId())) {
//
//							hourlyInternetOffers.add(responseupdated.getData().getHourlyBundles().getOffers().get(i));
//
//						}
//					}
//				}
//				responseupdated.getData().getHourlyBundles().setOffers(hourlyInternetOffers);
//			}
			
			ArrayList<InternetOfferingsData> allInternetOffers = new ArrayList<InternetOfferingsData>();
			if (responseupdated.getData().getAllBundles() != null
					&& responseupdated.getData().getAllBundles().getOffers() != null
					&& !responseupdated.getData().getAllBundles().getOffers().isEmpty()
					&& responseupdated.getData().getAllBundles().getOffers().size() > 0) {
				for (int i = 0; i < responseupdated.getData().getAllBundles().getOffers().size(); i++) {
					if (responseupdated.getData().getAllBundles().getOffers().get(i).getHeader() != null) {
						if (!internetOfferingsRequest.getSpecialOfferIds().contains(responseupdated.getData()
								.getAllBundles().getOffers().get(i).getHeader().getOfferingId())) {

							allInternetOffers.add(responseupdated.getData().getAllBundles().getOffers().get(i));

						}
					}
				}
				responseupdated.getData().getAllBundles().setOffers(allInternetOffers);
			}

		} // END OF if(!msisdn.contains("="))
			// END SETTING LOGIC TO ELIMINATE SPEICAL OFFERS FROM INTERNET
		return responseupdated;
	}

	public InternetOfferingsResponse ManipulateResponseForHideVisibleAllowedChecks(String msisdn,
			InternetOfferingsResponse responseupdated, InternetOfferingsResponse internetOfferingsResponse,
			InternetOfferingsRequest internetOfferingsRequest) {
		try {
			ObjectMapper mapper = new ObjectMapper();
			CustomerInfoResponseData custInfoResponse = new CustomerInfoResponseData();
			CustomerInfoRequest customerInfoRequest = new CustomerInfoRequest();

			customerInfoRequest.setChannel(internetOfferingsRequest.getChannel());
			customerInfoRequest.setiP(internetOfferingsRequest.getiP());
			customerInfoRequest.setLang(internetOfferingsRequest.getLang());
			customerInfoRequest.setMsisdn(internetOfferingsRequest.getMsisdn());

			Utilities.printInfoLog(msisdn + "-Received From customerDataInternalCall SuplemntraryResponseBeforeMatch-"
					+ mapper.writeValueAsString(internetOfferingsResponse), logger);

			CustomerServicesBusiness customerBusiness = new CustomerServicesBusiness();
			custInfoResponse = customerBusiness.getCustomerData(msisdn, "", customerInfoRequest, custInfoResponse,
					Constants.TOKEN_CREATION_CALL_TYPE_EXTERNAL, "", "");

			if (custInfoResponse.getResultCode().equalsIgnoreCase(Constants.ESB_SUCCESS_CODE)) {

				ArrayList<String> offeringId = new ArrayList<String>();

				for (int i = 0; i < custInfoResponse.getSupplementaryOfferingList().size(); i++) {
					offeringId.add(custInfoResponse.getSupplementaryOfferingList().get(i).getOfferingId());
				}
				Utilities.printDebugLog(msisdn + "-Received From customerDataInternalLogic OfferingIdS-" + offeringId,
						logger);
				// START DailyBundles LOGIC TO CHECK VISIBLE FOR HIDE FOR
				if (internetOfferingsResponse.getData() != null
						&& internetOfferingsResponse.getData().getDailyBundles() != null
						&& internetOfferingsResponse.getData().getDailyBundles().getOffers() != null) {
					Utilities.printDebugLog(
							msisdn + "-Received From customerDataInternalLogic DailyBundles OFFERS YES found-"
									+ mapper.writeValueAsString(internetOfferingsResponse.getData().getDailyBundles()),
							logger);
					ArrayList<InternetOfferingsData> offers = new ArrayList<InternetOfferingsData>();
					for (int j = 0; j < internetOfferingsResponse.getData().getDailyBundles().getOffers().size(); j++) {
						Utilities.printDebugLog(
								msisdn + "-Received From customerDataInternalLogic  VisibleFOR_FromDailyBundles-"
										+ internetOfferingsResponse.getData().getDailyBundles().getOffers().get(j)
												.getHeader().getVisibleFor(),
								logger);
						Utilities.printDebugLog(
								msisdn + "-Received From customerDataInternalLogic  HisdeFOR_FromDailyBundles-"
										+ internetOfferingsResponse.getData().getDailyBundles().getOffers().get(j)
												.getHeader().getHideFor(),
								logger);

						// START OF ALoowd for and VISIBLE FOR HIDE FOR CHECKS
						if (offeringId.contains(internetOfferingsResponse.getData().getDailyBundles().getOffers().get(j)
								.getHeader().getOfferingId())) {
							internetOfferingsResponse.getData().getDailyBundles().getOffers().get(j).getHeader()
									.setIsAlreadySusbcribed("true");
							if (internetOfferingsResponse.getData().getDailyBundles().getOffers().get(j).getHeader()
									.getBtnRenew().equalsIgnoreCase("1")) {
								String allowedFor = internetOfferingsResponse.getData().getDailyBundles().getOffers()
										.get(j).getHeader().getAllowedForRenew();

								List<String> AllowedFor = new ArrayList<String>(Arrays.asList(allowedFor.split(",")));
								if (!CollectionUtils.intersection(offeringId, AllowedFor).isEmpty()) {
									internetOfferingsResponse.getData().getDailyBundles().getOffers().get(j).getHeader()
											.setBtnRenew("1");
								} else {
									internetOfferingsResponse.getData().getDailyBundles().getOffers().get(j).getHeader()
											.setBtnRenew("0");
								}
							}
						} else {
							internetOfferingsResponse.getData().getDailyBundles().getOffers().get(j).getHeader()
									.setIsAlreadySusbcribed("false");
						}

						List<String> visibleForArrrayAfterConversion = new ArrayList<String>();
						if (hasValue(internetOfferingsResponse.getData().getDailyBundles().getOffers().get(j)
								.getHeader().getVisibleFor())) {
							Utilities.printDebugLog(
									msisdn + "DailyBundles CHECK VISIBLE FOR IF NOT NULL  :" + internetOfferingsResponse
											.getData().getDailyBundles().getOffers().get(j).getHeader().getVisibleFor(),
									logger);
							String[] visibleForArrayConverting = internetOfferingsResponse.getData().getDailyBundles()
									.getOffers().get(j).getHeader().getVisibleFor().split(",");
							visibleForArrrayAfterConversion = Arrays.asList(visibleForArrayConverting);

						}

						List<String> HideForArrrayAfterConversion = new ArrayList<String>();
						if (hasValue(internetOfferingsResponse.getData().getDailyBundles().getOffers().get(j)
								.getHeader().getHideFor())) {
							Utilities.printDebugLog(
									msisdn + "DailyBundles CHECK HIDE FOR NOT NULL :" + internetOfferingsResponse
											.getData().getDailyBundles().getOffers().get(j).getHeader().getHideFor(),
									logger);
							String[] HideForArrayConverting = internetOfferingsResponse.getData().getDailyBundles()
									.getOffers().get(j).getHeader().getHideFor().split(",");
							HideForArrrayAfterConversion = Arrays.asList(HideForArrayConverting);
						}

						Utilities.printDebugLog(msisdn + "-"
								+ internetOfferingsResponse.getData().getDailyBundles().getOffers().get(j).getHeader()
								+ "- DailyBundles CHECK Received From customerDataInternalLogic  ArrayListConversion VisibleFOR_FromInternet-"
								+ visibleForArrrayAfterConversion, logger);
						Utilities.printDebugLog(msisdn + "-"
								+ internetOfferingsResponse.getData().getDailyBundles().getOffers().get(j).getHeader()
								+ "- DailyBundles CHECK Received From customerDataInternalLogic  ArrayListConversion HisdeFOR_FromInternet-"
								+ HideForArrrayAfterConversion, logger);

						if (visibleForArrrayAfterConversion.size() < 1 && HideForArrrayAfterConversion.size() < 1) {

							Utilities.printDebugLog(msisdn
									+ "----------------DailyBundles CHECK VISIBLE AND HIDE FOR ARRAYLISTS NULL BOTH ELSE--------"
									+ visibleForArrrayAfterConversion, logger);
							offers.add(internetOfferingsResponse.getData().getDailyBundles().getOffers().get(j));

						} else {
							Utilities.printDebugLog(msisdn
									+ "----------------DailyBundles CHECK VISIBLE AND HIDE FOR ARRALISTS NOT NULL IF--------"
									+ visibleForArrrayAfterConversion, logger);

							boolean isflag = false;
							if (visibleForArrrayAfterConversion != null && !visibleForArrrayAfterConversion.isEmpty()
									&& !CollectionUtils.intersection(offeringId, visibleForArrrayAfterConversion)
											.isEmpty()) {

								Utilities.printDebugLog(msisdn + "-"
										+ internetOfferingsResponse.getData().getDailyBundles().getOffers().get(j)
												.getHeader().getOfferingId()
										+ "-DailyBundles CHECK Received From customerDataInternalLogic Matched VisibleFOR_FromInternet YES-"
										+ visibleForArrrayAfterConversion, logger);
								isflag = true;
								offers.add(internetOfferingsResponse.getData().getDailyBundles().getOffers().get(j));

							} else if (HideForArrrayAfterConversion != null && !HideForArrrayAfterConversion.isEmpty()
									&& !CollectionUtils.intersection(offeringId, HideForArrrayAfterConversion)
											.isEmpty()) {
								Utilities.printDebugLog(msisdn + "-"
										+ internetOfferingsResponse.getData().getDailyBundles().getOffers().get(j)
												.getHeader().getOfferingId()
										+ "-DailyBundles CHECK Received From customerDataInternalLogic Matched hideFOR_FromInternetYES-"
										+ HideForArrrayAfterConversion, logger);

							} else {
								Utilities.printDebugLog(msisdn + "-"
										+ internetOfferingsResponse.getData().getDailyBundles().getOffers().get(j)
												.getHeader().getOfferingId()
										+ "-DailyBundles CHECK Received From customerDataInternalLogic NOT FOUND IN HIDEFOR :-"
										+ internetOfferingsResponse.getData().getDailyBundles().getOffers().get(j)
												.getHeader().getOfferingId(),
										logger);
								if ((!CollectionUtils.intersection(offeringId, HideForArrrayAfterConversion)
										.isEmpty())) {
									// hide mt daalo
								} else if (CollectionUtils.intersection(offeringId, HideForArrrayAfterConversion)
										.isEmpty() && visibleForArrrayAfterConversion.size() == 0) {
									offers.add(
											internetOfferingsResponse.getData().getDailyBundles().getOffers().get(j));
								} else if ((!CollectionUtils.intersection(offeringId, visibleForArrrayAfterConversion)
										.isEmpty())) {
									offers.add(
											internetOfferingsResponse.getData().getDailyBundles().getOffers().get(j));
								}
							}
						}

						// END OF OF ALoowd for and VISIBLE FOR HIDE FOR CHECKS
					}

					// responseupdated =
					responseupdated.getData().getDailyBundles().setOffers(offers);

				} else {
					ArrayList<InternetOfferingsData> offers = new ArrayList<InternetOfferingsData>();
					responseupdated.getData().getDailyBundles().setOffers(offers);
				}
				// END OF DailyBundles LOGIC TO CHECK VISIBLE FOR HIDE FOR

				// START MonthlyBundles LOGIC TO CHECK VISIBLE FOR HIDE FOR
				if (internetOfferingsResponse.getData() != null
						&& internetOfferingsResponse.getData().getMonthlyBundles() != null
						&& internetOfferingsResponse.getData().getMonthlyBundles().getOffers() != null) {
					Utilities.printDebugLog(msisdn
							+ "-Received From customerDataInternalLogic MonthlyBundles OFFERS YES found-"
							+ mapper.writeValueAsString(internetOfferingsResponse.getData().getMonthlyBundles()),
							logger);
					ArrayList<InternetOfferingsData> offers = new ArrayList<InternetOfferingsData>();
					for (int j = 0; j < internetOfferingsResponse.getData().getMonthlyBundles().getOffers()
							.size(); j++) {
						Utilities.printDebugLog(
								msisdn + "-Received From customerDataInternalLogic  VisibleFOR_FromMonthlyBundles-"
										+ internetOfferingsResponse.getData().getMonthlyBundles().getOffers().get(j)
												.getHeader().getVisibleFor(),
								logger);
						Utilities.printDebugLog(
								msisdn + "-Received From customerDataInternalLogic  HisdeFOR_FromMonthlyBundles-"
										+ internetOfferingsResponse.getData().getMonthlyBundles().getOffers().get(j)
												.getHeader().getHideFor(),
								logger);
						// START OF ALoowd for and VISIBLE FOR HIDE FOR CHECKS
						if (offeringId.contains(internetOfferingsResponse.getData().getMonthlyBundles().getOffers()
								.get(j).getHeader().getOfferingId())) {
							internetOfferingsResponse.getData().getMonthlyBundles().getOffers().get(j).getHeader()
									.setIsAlreadySusbcribed("true");
							if (internetOfferingsResponse.getData().getMonthlyBundles().getOffers().get(j).getHeader()
									.getBtnRenew().equalsIgnoreCase("1")) {
								String allowedFor = internetOfferingsResponse.getData().getMonthlyBundles().getOffers()
										.get(j).getHeader().getAllowedForRenew();

								List<String> AllowedFor = new ArrayList<String>(Arrays.asList(allowedFor.split(",")));
								if (!CollectionUtils.intersection(offeringId, AllowedFor).isEmpty()) {
									internetOfferingsResponse.getData().getMonthlyBundles().getOffers().get(j)
											.getHeader().setBtnRenew("1");
								} else {
									internetOfferingsResponse.getData().getMonthlyBundles().getOffers().get(j)
											.getHeader().setBtnRenew("0");
								}
							}
						} else {
							internetOfferingsResponse.getData().getMonthlyBundles().getOffers().get(j).getHeader()
									.setIsAlreadySusbcribed("false");
						}

						List<String> visibleForArrrayAfterConversion = new ArrayList<String>();
						if (hasValue(internetOfferingsResponse.getData().getMonthlyBundles().getOffers().get(j)
								.getHeader().getVisibleFor())) {
							Utilities.printDebugLog(msisdn + "MonthlyBundles CHECK VISIBLE FOR IF NOT NULL  :"
									+ internetOfferingsResponse.getData().getMonthlyBundles().getOffers().get(j)
											.getHeader().getVisibleFor(),
									logger);
							String[] visibleForArrayConverting = internetOfferingsResponse.getData().getMonthlyBundles()
									.getOffers().get(j).getHeader().getVisibleFor().split(",");
							visibleForArrrayAfterConversion = Arrays.asList(visibleForArrayConverting);

						}

						List<String> HideForArrrayAfterConversion = new ArrayList<String>();
						if (hasValue(internetOfferingsResponse.getData().getMonthlyBundles().getOffers().get(j)
								.getHeader().getHideFor())) {
							Utilities.printDebugLog(
									msisdn + "MonthlyBundles CHECK HIDE FOR NOT NULL :" + internetOfferingsResponse
											.getData().getMonthlyBundles().getOffers().get(j).getHeader().getHideFor(),
									logger);
							String[] HideForArrayConverting = internetOfferingsResponse.getData().getMonthlyBundles()
									.getOffers().get(j).getHeader().getHideFor().split(",");
							HideForArrrayAfterConversion = Arrays.asList(HideForArrayConverting);
						}

						Utilities.printDebugLog(msisdn + "-"
								+ internetOfferingsResponse.getData().getMonthlyBundles().getOffers().get(j).getHeader()
								+ "- MonthlyBundles CHECK Received From customerDataInternalLogic  ArrayListConversion VisibleFOR_FromInternet-"
								+ visibleForArrrayAfterConversion, logger);
						Utilities.printDebugLog(msisdn + "-"
								+ internetOfferingsResponse.getData().getMonthlyBundles().getOffers().get(j).getHeader()
								+ "- MonthlyBundles CHECK Received From customerDataInternalLogic  ArrayListConversion HisdeFOR_FromInternet-"
								+ HideForArrrayAfterConversion, logger);

						if (visibleForArrrayAfterConversion.size() < 1 && HideForArrrayAfterConversion.size() < 1) {

							Utilities.printDebugLog(msisdn
									+ "----------------MonthlyBundles CHECK VISIBLE AND HIDE FOR ARRAYLISTS NULL BOTH ELSE--------"
									+ visibleForArrrayAfterConversion, logger);
							offers.add(internetOfferingsResponse.getData().getMonthlyBundles().getOffers().get(j));

						} else {
							Utilities.printDebugLog(msisdn
									+ "----------------MonthlyBundles CHECK VISIBLE AND HIDE FOR ARRALISTS NOT NULL IF--------"
									+ visibleForArrrayAfterConversion, logger);

							boolean isflag = false;
							if (visibleForArrrayAfterConversion != null && !visibleForArrrayAfterConversion.isEmpty()
									&& !CollectionUtils.intersection(offeringId, visibleForArrrayAfterConversion)
											.isEmpty()) {

								Utilities.printDebugLog(msisdn + "-"
										+ internetOfferingsResponse.getData().getMonthlyBundles().getOffers().get(j)
												.getHeader().getOfferingId()
										+ "-MonthlyBundles CHECK Received From customerDataInternalLogic Matched VisibleFOR_FromInternet YES-"
										+ visibleForArrrayAfterConversion, logger);
								isflag = true;
								offers.add(internetOfferingsResponse.getData().getMonthlyBundles().getOffers().get(j));

							} else if (HideForArrrayAfterConversion != null && !HideForArrrayAfterConversion.isEmpty()
									&& !CollectionUtils.intersection(offeringId, HideForArrrayAfterConversion)
											.isEmpty()) {
								Utilities.printDebugLog(msisdn + "-"
										+ internetOfferingsResponse.getData().getMonthlyBundles().getOffers().get(j)
												.getHeader().getOfferingId()
										+ "-MonthlyBundles CHECK Received From customerDataInternalLogic Matched hideFOR_FromInternetYES-"
										+ HideForArrrayAfterConversion, logger);

							} else {
								Utilities.printDebugLog(msisdn + "-"
										+ internetOfferingsResponse.getData().getMonthlyBundles().getOffers().get(j)
												.getHeader().getOfferingId()
										+ "-MonthlyBundles CHECK Received From customerDataInternalLogic NOT FOUND IN HIDEFOR :-"
										+ internetOfferingsResponse.getData().getMonthlyBundles().getOffers().get(j)
												.getHeader().getOfferingId(),
										logger);
								if ((!CollectionUtils.intersection(offeringId, HideForArrrayAfterConversion)
										.isEmpty())) {
									// hide mt daalo
								} else if (CollectionUtils.intersection(offeringId, HideForArrrayAfterConversion)
										.isEmpty() && visibleForArrrayAfterConversion.size() == 0) {
									offers.add(
											internetOfferingsResponse.getData().getMonthlyBundles().getOffers().get(j));
								} else if ((!CollectionUtils.intersection(offeringId, visibleForArrrayAfterConversion)
										.isEmpty())) {
									offers.add(
											internetOfferingsResponse.getData().getMonthlyBundles().getOffers().get(j));
								}
							}
						}

						// END OF OF ALoowd for and VISIBLE FOR HIDE FOR CHECKS

					}

					// responseupdated =
					responseupdated.getData().getMonthlyBundles().setOffers(offers);

				} else {
					ArrayList<InternetOfferingsData> offers = new ArrayList<InternetOfferingsData>();

					responseupdated.getData().getMonthlyBundles().setOffers(offers);
				}
				// END OF MonthlyBundles LOGIC TO CHECK VISIBLE FOR HIDE FOR
				
				
				
				// START AllBundles LOGIC TO CHECK VISIBLE FOR HIDE FOR
				if (internetOfferingsResponse.getData() != null
						&& internetOfferingsResponse.getData().getAllBundles() != null
						&& internetOfferingsResponse.getData().getAllBundles().getOffers() != null) {
					Utilities.printDebugLog(
							msisdn + "-Received From customerDataInternalLogic AllBundles OFFERS YES found-"
									+ mapper.writeValueAsString(internetOfferingsResponse.getData().getAllBundles()),
							logger);
					ArrayList<InternetOfferingsData> offers = new ArrayList<InternetOfferingsData>();
					for (int j = 0; j < internetOfferingsResponse.getData().getAllBundles().getOffers().size(); j++) {
						Utilities.printDebugLog(
								msisdn + "-Received From customerDataInternalLogic  VisibleFOR_FromAllBundles-"
										+ internetOfferingsResponse.getData().getAllBundles().getOffers().get(j)
												.getHeader().getVisibleFor(),
								logger);
						Utilities.printDebugLog(
								msisdn + "-Received From customerDataInternalLogic  HisdeFOR_FromAllBundles-"
										+ internetOfferingsResponse.getData().getAllBundles().getOffers().get(j)
												.getHeader().getHideFor(),
								logger);

						// START OF ALoowd for and VISIBLE FOR HIDE FOR CHECKS
						if (offeringId.contains(internetOfferingsResponse.getData().getAllBundles().getOffers().get(j)
								.getHeader().getOfferingId())) {
							internetOfferingsResponse.getData().getAllBundles().getOffers().get(j).getHeader()
									.setIsAlreadySusbcribed("true");
							if (internetOfferingsResponse.getData().getAllBundles().getOffers().get(j).getHeader()
									.getBtnRenew().equalsIgnoreCase("1")) {
								String allowedFor = internetOfferingsResponse.getData().getAllBundles().getOffers()
										.get(j).getHeader().getAllowedForRenew();

								List<String> AllowedFor = new ArrayList<String>(Arrays.asList(allowedFor.split(",")));
								if (!CollectionUtils.intersection(offeringId, AllowedFor).isEmpty()) {
									internetOfferingsResponse.getData().getAllBundles().getOffers().get(j).getHeader()
											.setBtnRenew("1");
								} else {
									internetOfferingsResponse.getData().getAllBundles().getOffers().get(j).getHeader()
											.setBtnRenew("0");
								}
							}
						} else {
							internetOfferingsResponse.getData().getAllBundles().getOffers().get(j).getHeader()
									.setIsAlreadySusbcribed("false");
						}

						List<String> visibleForArrrayAfterConversion = new ArrayList<String>();
						if (hasValue(internetOfferingsResponse.getData().getAllBundles().getOffers().get(j)
								.getHeader().getVisibleFor())) {
							Utilities.printDebugLog(
									msisdn + "AllBundles CHECK VISIBLE FOR IF NOT NULL  :" + internetOfferingsResponse
											.getData().getAllBundles().getOffers().get(j).getHeader().getVisibleFor(),
									logger);
							String[] visibleForArrayConverting = internetOfferingsResponse.getData().getAllBundles()
									.getOffers().get(j).getHeader().getVisibleFor().split(",");
							visibleForArrrayAfterConversion = Arrays.asList(visibleForArrayConverting);

						}

						List<String> HideForArrrayAfterConversion = new ArrayList<String>();
						if (hasValue(internetOfferingsResponse.getData().getAllBundles().getOffers().get(j)
								.getHeader().getHideFor())) {
							Utilities.printDebugLog(
									msisdn + "AllBundles CHECK HIDE FOR NOT NULL :" + internetOfferingsResponse
											.getData().getAllBundles().getOffers().get(j).getHeader().getHideFor(),
									logger);
							String[] HideForArrayConverting = internetOfferingsResponse.getData().getAllBundles()
									.getOffers().get(j).getHeader().getHideFor().split(",");
							HideForArrrayAfterConversion = Arrays.asList(HideForArrayConverting);
						}

						Utilities.printDebugLog(msisdn + "-"
								+ internetOfferingsResponse.getData().getAllBundles().getOffers().get(j).getHeader()
								+ "- AllBundles CHECK Received From customerDataInternalLogic  ArrayListConversion VisibleFOR_FromInternet-"
								+ visibleForArrrayAfterConversion, logger);
						Utilities.printDebugLog(msisdn + "-"
								+ internetOfferingsResponse.getData().getAllBundles().getOffers().get(j).getHeader()
								+ "- AllBundles CHECK Received From customerDataInternalLogic  ArrayListConversion HisdeFOR_FromInternet-"
								+ HideForArrrayAfterConversion, logger);

						if (visibleForArrrayAfterConversion.size() < 1 && HideForArrrayAfterConversion.size() < 1) {

							Utilities.printDebugLog(msisdn
									+ "----------------AllBundles CHECK VISIBLE AND HIDE FOR ARRAYLISTS NULL BOTH ELSE--------"
									+ visibleForArrrayAfterConversion, logger);
							offers.add(internetOfferingsResponse.getData().getAllBundles().getOffers().get(j));

						} else {
							Utilities.printDebugLog(msisdn
									+ "----------------AllBundles CHECK VISIBLE AND HIDE FOR ARRALISTS NOT NULL IF--------"
									+ visibleForArrrayAfterConversion, logger);

							boolean isflag = false;
							if (visibleForArrrayAfterConversion != null && !visibleForArrrayAfterConversion.isEmpty()
									&& !CollectionUtils.intersection(offeringId, visibleForArrrayAfterConversion)
											.isEmpty()) {

								Utilities.printDebugLog(msisdn + "-"
										+ internetOfferingsResponse.getData().getAllBundles().getOffers().get(j)
												.getHeader().getOfferingId()
										+ "-AllBundles CHECK Received From customerDataInternalLogic Matched VisibleFOR_FromInternet YES-"
										+ visibleForArrrayAfterConversion, logger);
								isflag = true;
								offers.add(internetOfferingsResponse.getData().getAllBundles().getOffers().get(j));

							} else if (HideForArrrayAfterConversion != null && !HideForArrrayAfterConversion.isEmpty()
									&& !CollectionUtils.intersection(offeringId, HideForArrrayAfterConversion)
											.isEmpty()) {
								Utilities.printDebugLog(msisdn + "-"
										+ internetOfferingsResponse.getData().getAllBundles().getOffers().get(j)
												.getHeader().getOfferingId()
										+ "-AllBundles CHECK Received From customerDataInternalLogic Matched hideFOR_FromInternetYES-"
										+ HideForArrrayAfterConversion, logger);

							} else {
								Utilities.printDebugLog(msisdn + "-"
										+ internetOfferingsResponse.getData().getAllBundles().getOffers().get(j)
												.getHeader().getOfferingId()
										+ "-AllBundles CHECK Received From customerDataInternalLogic NOT FOUND IN HIDEFOR :-"
										+ internetOfferingsResponse.getData().getAllBundles().getOffers().get(j)
												.getHeader().getOfferingId(),
										logger);
								if ((!CollectionUtils.intersection(offeringId, HideForArrrayAfterConversion)
										.isEmpty())) {
									// hide mt daalo
								} else if (CollectionUtils.intersection(offeringId, HideForArrrayAfterConversion)
										.isEmpty() && visibleForArrrayAfterConversion.size() == 0) {
									offers.add(
											internetOfferingsResponse.getData().getAllBundles().getOffers().get(j));
								} else if ((!CollectionUtils.intersection(offeringId, visibleForArrrayAfterConversion)
										.isEmpty())) {
									offers.add(
											internetOfferingsResponse.getData().getAllBundles().getOffers().get(j));
								}
							}
						}

						// END OF OF ALoowd for and VISIBLE FOR HIDE FOR CHECKS
					}

					// responseupdated =
					responseupdated.getData().getAllBundles().setOffers(offers);

				} else {
					ArrayList<InternetOfferingsData> offers = new ArrayList<InternetOfferingsData>();
					responseupdated.getData().getAllBundles().setOffers(offers);
				}
				
				// END OF AllBundles LOGIC TO CHECK VISIBLE FOR HIDE FOR
				
//				// START HourlyBundles LOGIC TO CHECK VISIBLE FOR HIDE FOR
//				if (internetOfferingsResponse.getData() != null
//						&& internetOfferingsResponse.getData().getHourlyBundles() != null
//						&& internetOfferingsResponse.getData().getHourlyBundles().getOffers() != null) {
//					Utilities.printDebugLog(
//							msisdn + "-Received From customerDataInternalLogic HourlyBundles OFFERS YES found-"
//									+ mapper.writeValueAsString(internetOfferingsResponse.getData().getHourlyBundles()),
//							logger);
//					ArrayList<InternetOfferingsData> offers = new ArrayList<InternetOfferingsData>();
//					for (int j = 0; j < internetOfferingsResponse.getData().getHourlyBundles().getOffers().size(); j++) {
//						Utilities.printDebugLog(
//								msisdn + "-Received From customerDataInternalLogic  VisibleFOR_FromHourlyBundles-"
//										+ internetOfferingsResponse.getData().getHourlyBundles().getOffers().get(j)
//												.getHeader().getVisibleFor(),
//								logger);
//						Utilities.printDebugLog(
//								msisdn + "-Received From customerDataInternalLogic  HisdeFOR_FromHourlyBundles-"
//										+ internetOfferingsResponse.getData().getHourlyBundles().getOffers().get(j)
//												.getHeader().getHideFor(),
//								logger);
//
//						// START OF ALoowd for and VISIBLE FOR HIDE FOR CHECKS
//						if (offeringId.contains(internetOfferingsResponse.getData().getHourlyBundles().getOffers().get(j)
//								.getHeader().getOfferingId())) {
//							internetOfferingsResponse.getData().getHourlyBundles().getOffers().get(j).getHeader()
//									.setIsAlreadySusbcribed("true");
//							if (internetOfferingsResponse.getData().getHourlyBundles().getOffers().get(j).getHeader()
//									.getBtnRenew().equalsIgnoreCase("1")) {
//								String allowedFor = internetOfferingsResponse.getData().getHourlyBundles().getOffers()
//										.get(j).getHeader().getAllowedForRenew();
//
//								List<String> AllowedFor = new ArrayList<String>(Arrays.asList(allowedFor.split(",")));
//								if (!CollectionUtils.intersection(offeringId, AllowedFor).isEmpty()) {
//									internetOfferingsResponse.getData().getHourlyBundles().getOffers().get(j).getHeader()
//											.setBtnRenew("1");
//								} else {
//									internetOfferingsResponse.getData().getHourlyBundles().getOffers().get(j).getHeader()
//											.setBtnRenew("0");
//								}
//							}
//						} else {
//							internetOfferingsResponse.getData().getHourlyBundles().getOffers().get(j).getHeader()
//									.setIsAlreadySusbcribed("false");
//						}
//
//						List<String> visibleForArrrayAfterConversion = new ArrayList<String>();
//						if (hasValue(internetOfferingsResponse.getData().getHourlyBundles().getOffers().get(j)
//								.getHeader().getVisibleFor())) {
//							Utilities.printDebugLog(
//									msisdn + "HourlyBundles CHECK VISIBLE FOR IF NOT NULL  :" + internetOfferingsResponse
//											.getData().getHourlyBundles().getOffers().get(j).getHeader().getVisibleFor(),
//									logger);
//							String[] visibleForArrayConverting = internetOfferingsResponse.getData().getHourlyBundles()
//									.getOffers().get(j).getHeader().getVisibleFor().split(",");
//							visibleForArrrayAfterConversion = Arrays.asList(visibleForArrayConverting);
//
//						}
//
//						List<String> HideForArrrayAfterConversion = new ArrayList<String>();
//						if (hasValue(internetOfferingsResponse.getData().getHourlyBundles().getOffers().get(j)
//								.getHeader().getHideFor())) {
//							Utilities.printDebugLog(
//									msisdn + "HourlyBundles CHECK HIDE FOR NOT NULL :" + internetOfferingsResponse
//											.getData().getHourlyBundles().getOffers().get(j).getHeader().getHideFor(),
//									logger);
//							String[] HideForArrayConverting = internetOfferingsResponse.getData().getHourlyBundles()
//									.getOffers().get(j).getHeader().getHideFor().split(",");
//							HideForArrrayAfterConversion = Arrays.asList(HideForArrayConverting);
//						}
//
//						Utilities.printDebugLog(msisdn + "-"
//								+ internetOfferingsResponse.getData().getHourlyBundles().getOffers().get(j).getHeader()
//								+ "- HourlyBundles CHECK Received From customerDataInternalLogic  ArrayListConversion VisibleFOR_FromInternet-"
//								+ visibleForArrrayAfterConversion, logger);
//						Utilities.printDebugLog(msisdn + "-"
//								+ internetOfferingsResponse.getData().getHourlyBundles().getOffers().get(j).getHeader()
//								+ "- HourlyBundles CHECK Received From customerDataInternalLogic  ArrayListConversion HisdeFOR_FromInternet-"
//								+ HideForArrrayAfterConversion, logger);
//
//						if (visibleForArrrayAfterConversion.size() < 1 && HideForArrrayAfterConversion.size() < 1) {
//
//							Utilities.printDebugLog(msisdn
//									+ "----------------HourlyBundles CHECK VISIBLE AND HIDE FOR ARRAYLISTS NULL BOTH ELSE--------"
//									+ visibleForArrrayAfterConversion, logger);
//							offers.add(internetOfferingsResponse.getData().getHourlyBundles().getOffers().get(j));
//
//						} else {
//							Utilities.printDebugLog(msisdn
//									+ "----------------HourlyBundles CHECK VISIBLE AND HIDE FOR ARRALISTS NOT NULL IF--------"
//									+ visibleForArrrayAfterConversion, logger);
//
//							boolean isflag = false;
//							if (visibleForArrrayAfterConversion != null && !visibleForArrrayAfterConversion.isEmpty()
//									&& !CollectionUtils.intersection(offeringId, visibleForArrrayAfterConversion)
//											.isEmpty()) {
//
//								Utilities.printDebugLog(msisdn + "-"
//										+ internetOfferingsResponse.getData().getHourlyBundles().getOffers().get(j)
//												.getHeader().getOfferingId()
//										+ "-HourlyBundles CHECK Received From customerDataInternalLogic Matched VisibleFOR_FromInternet YES-"
//										+ visibleForArrrayAfterConversion, logger);
//								isflag = true;
//								offers.add(internetOfferingsResponse.getData().getHourlyBundles().getOffers().get(j));
//
//							} else if (HideForArrrayAfterConversion != null && !HideForArrrayAfterConversion.isEmpty()
//									&& !CollectionUtils.intersection(offeringId, HideForArrrayAfterConversion)
//											.isEmpty()) {
//								Utilities.printDebugLog(msisdn + "-"
//										+ internetOfferingsResponse.getData().getHourlyBundles().getOffers().get(j)
//												.getHeader().getOfferingId()
//										+ "-HourlyBundles CHECK Received From customerDataInternalLogic Matched hideFOR_FromInternetYES-"
//										+ HideForArrrayAfterConversion, logger);
//
//							} else {
//								Utilities.printDebugLog(msisdn + "-"
//										+ internetOfferingsResponse.getData().getHourlyBundles().getOffers().get(j)
//												.getHeader().getOfferingId()
//										+ "-HourlyBundles CHECK Received From customerDataInternalLogic NOT FOUND IN HIDEFOR :-"
//										+ internetOfferingsResponse.getData().getHourlyBundles().getOffers().get(j)
//												.getHeader().getOfferingId(),
//										logger);
//								if ((!CollectionUtils.intersection(offeringId, HideForArrrayAfterConversion)
//										.isEmpty())) {
//									// hide mt daalo
//								} else if (CollectionUtils.intersection(offeringId, HideForArrrayAfterConversion)
//										.isEmpty() && visibleForArrrayAfterConversion.size() == 0) {
//									offers.add(
//											internetOfferingsResponse.getData().getHourlyBundles().getOffers().get(j));
//								} else if ((!CollectionUtils.intersection(offeringId, visibleForArrrayAfterConversion)
//										.isEmpty())) {
//									offers.add(
//											internetOfferingsResponse.getData().getHourlyBundles().getOffers().get(j));
//								}
//							}
//						}
//
//						// END OF OF ALoowd for and VISIBLE FOR HIDE FOR CHECKS
//					}
//
//					// responseupdated =
//					responseupdated.getData().getHourlyBundles().setOffers(offers);
//
//				} else {
//					ArrayList<InternetOfferingsData> offers = new ArrayList<InternetOfferingsData>();
//					responseupdated.getData().getHourlyBundles().setOffers(offers);
//				}
//				
//				// END OF HourlyBundles LOGIC TO CHECK VISIBLE FOR HIDE FOR
				
			} else {
				InternetOfferingsResponse responseupdatedNew = new InternetOfferingsResponse();
				return responseupdatedNew;
			}
		} catch (Exception e) {

		}
		return responseupdated;
	}

	private String getKeyForCache(String hashKeyInternetOfferingsRequest,
			InternetOfferingsRequest internetOfferingsRequest) {
		return hashKeyInternetOfferingsRequest + "." + internetOfferingsRequest.getOfferingName() + "."
				+ internetOfferingsRequest.getLang();
	}

	public static boolean hasValue(String value) {
		try {
			if (value != null && !value.equalsIgnoreCase("null") && !value.equalsIgnoreCase("")) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			return false;
		}
	}

}