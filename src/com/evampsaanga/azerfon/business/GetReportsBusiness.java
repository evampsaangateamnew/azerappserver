package com.evampsaanga.azerfon.business;

import java.io.File;
import java.io.FilenameFilter;
import java.net.SocketException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

import com.evampsaanga.azerfon.common.utilities.Utilities;
import com.evampsaanga.azerfon.models.getreports.GetReportsRequest;
import com.evampsaanga.azerfon.models.getreports.GetReportsResponse;
import com.evampsaanga.azerfon.models.getreports.ReportData;

@Service
public class GetReportsBusiness {
	Logger logger = Logger.getLogger(GetReportsBusiness.class);

	String reportpath = "/opt/tomcat/reports";

	public GetReportsResponse getReports(GetReportsRequest getReportsRequest, GetReportsResponse getReportsResponse)
			throws SocketException {
		Utilities.printDebugLog("-Request received in GET REPORTS BUSINESS with data-" + getReportsRequest.toString(),
				logger);

		File file = new File("/opt/tomcat/reports");

		String path = file.getAbsolutePath();

		FilenameFilter filter = new FilenameFilter() {
			@Override
			public boolean accept(File f, String name) {
				return name.endsWith(".csv");
			}
		};

		file.list(filter);
		SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");

		List<ReportData> reportDataList = new ArrayList<>();

		for (String filename : file.list(filter)) {
			ReportData reportData = new ReportData();

			File file1 = new File("/opt/tomcat/reports/" + filename);

			reportData.setLastModified(sdf.format(file1.lastModified()));
			reportData.setFileName(filename);
			reportData.setFilePath(file1.getAbsolutePath());

			reportDataList.add(reportData);
		}

		getReportsResponse.setReportData(reportDataList);
		Utilities.printDebugLog("Response Returned from GET REPORTS BUSINESS -" + getReportsResponse.toString(),
				logger);

		return getReportsResponse;
	}

}
