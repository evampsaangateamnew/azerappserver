/**
 * 
 */
package com.evampsaanga.azerfon.business;

import java.io.IOException;
import java.net.SocketException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.ObjectUtils.Null;
import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.evampsaanga.azerfon.common.messagesmappings.GetMessagesMappings;
import com.evampsaanga.azerfon.common.utilities.Constants;
import com.evampsaanga.azerfon.common.utilities.GetConfigurations;
import com.evampsaanga.azerfon.common.utilities.Transactions;
import com.evampsaanga.azerfon.common.utilities.Utilities;
import com.evampsaanga.azerfon.models.financialservices.getloan.GetLoanRequest;
import com.evampsaanga.azerfon.models.financialservices.getloan.GetLoanResponse;
import com.evampsaanga.azerfon.models.financialservices.getloan.GetLoanResponseData;
import com.evampsaanga.azerfon.models.financialservices.loanhistory.Loan;
import com.evampsaanga.azerfon.models.financialservices.loanhistory.LoanHistoryEsbResponse;
import com.evampsaanga.azerfon.models.financialservices.loanhistory.LoanHistoryRequest;
import com.evampsaanga.azerfon.models.financialservices.loanhistory.LoanHistoryResponse;
import com.evampsaanga.azerfon.models.financialservices.loanhistory.LoanHistoryResponseData;
import com.evampsaanga.azerfon.models.financialservices.loanhistory.Provisions;
import com.evampsaanga.azerfon.models.financialservices.moneytransfer.MoneyTransferRequest;
import com.evampsaanga.azerfon.models.financialservices.moneytransfer.MoneyTransferResponse;
import com.evampsaanga.azerfon.models.financialservices.moneytransfer.MoneyTransferResponseData;
import com.evampsaanga.azerfon.models.financialservices.paymenthistory.Collection;
import com.evampsaanga.azerfon.models.financialservices.paymenthistory.Payment;
import com.evampsaanga.azerfon.models.financialservices.paymenthistory.PaymentHistoryRequest;
import com.evampsaanga.azerfon.models.financialservices.paymenthistory.PaymentHistoryResponse;
import com.evampsaanga.azerfon.models.financialservices.paymenthistory.PaymentHistoryResponseData;
import com.evampsaanga.azerfon.models.financialservices.requestmoney.RequestMoneyRequest;
import com.evampsaanga.azerfon.models.financialservices.requestmoney.RequestMoneyResponse;
import com.evampsaanga.azerfon.models.financialservices.requestmoney.RequestMoneyResponseData;
import com.evampsaanga.azerfon.models.financialservices.topups.TopupRequest;
import com.evampsaanga.azerfon.models.financialservices.topups.TopupResponse;
import com.evampsaanga.azerfon.models.financialservices.topups.TopupResponseData;
import com.evampsaanga.azerfon.models.ulduzum.GetCategoriesDataList;
import com.evampsaanga.azerfon.restclient.RestClient;

/**
 * @author Evamp & Saanga
 *
 */
public class FinancialServicesBusiness {
	Logger logger = Logger.getLogger(FinancialServicesBusiness.class);

	public TopupResponse getTopupBusiness(String msisdn, TopupRequest topupRequest, TopupResponse topupResponse)
			throws JSONException, IOException, SQLException {
		Utilities.printDebugLog(msisdn + "-Request received in " + Transactions.TOPUP_TRANSACTION_NAME
				+ " BUSINESS with data-" + topupRequest.toString(), logger);

		RestClient rc = new RestClient();
		ObjectMapper mapper = new ObjectMapper();
		TopupResponseData resData = new TopupResponseData();

		String requestJsonESB = mapper.writeValueAsString(topupRequest);
		String path = GetConfigurations.getESBRoute("requesttopups");

		/*
		 * Redefining request parameters for ESB Request as , we have different params
		 * for request at Mobile and ESB end.
		 */
		String subscriberType = Utilities.getValueFromJSON(requestJsonESB, "subscriberType");
		requestJsonESB = Utilities.removeParamsFromJSONObject(requestJsonESB, "subscriberType");
		requestJsonESB = Utilities.addParamsToJSONObject(requestJsonESB, "customerType", subscriberType);

		/*
		 * Below JSON modification is done just to mask scratch card number. Purpose is
		 * not to print scratch card number in logs.
		 */

		String tempData = requestJsonESB;
		String pinCard = Utilities.getValueFromJSON(tempData, "cardPinNumber");
		pinCard = Utilities.maskString(pinCard, Constants.SCRATCH_CARD_MASKING_COUNT);
		tempData = Utilities.removeParamsFromJSONObject(tempData, "cardPinNumber");
		tempData = Utilities.addParamsToJSONObject(tempData, "cardPinNumber", pinCard);

		Utilities.printDebugLog(msisdn + "-Request Call to ESB", logger);
		Utilities.printDebugLog(msisdn + "-Path-" + path, logger);
		Utilities.printDebugLog(msisdn + "-Request Packet-" + tempData, logger);

		topupResponse.getLogsReport().setEsbRequestTime(Utilities.getReportDateTime());
		String response = rc.getResponseFromESB(path, requestJsonESB);
		topupResponse.getLogsReport().setEsbResponseTime(Utilities.getReportDateTime());

		// String response = HardCodedResponses.TOP_UP;

		Utilities.printDebugLog(msisdn + "-Received response from ESB-" + response, logger);

		// Logging ESB response code and description.
		topupResponse.setLogsReport(
				Utilities.logESBParamsintoReportLog(requestJsonESB, response, topupResponse.getLogsReport()));

		if (response != null && !response.isEmpty()) {

			if (Utilities.getValueFromJSON(response, "returnCode").equalsIgnoreCase(Constants.ESB_SUCCESS_CODE)) {
				resData = mapper.readValue(Utilities.getValueFromJSON(response, "balance"), TopupResponseData.class);
				topupResponse.setData(resData);
				topupResponse.setCallStatus(Constants.Call_Status_True);
				topupResponse.setResultCode(Constants.APP_SERVER_SUCCESS_CODE);

				String responseMessage = Utilities.getErrorMessageFromFile("top.up", topupRequest.getLang(),
						Utilities.getValueFromJSON(response, "returnCode"));
				if (responseMessage != null && responseMessage != "") {
					responseMessage = responseMessage.replaceAll("%@NUMBER", topupRequest.getTopupnum());

					topupResponse.setResultDesc(responseMessage);
				} else {
					topupResponse.setResultDesc(Utilities.getErrorMessageFromFile("top.up", topupRequest.getLang(),
							Utilities.getValueFromJSON(response, "returnCode")));
				}

			} else {
				topupResponse.setCallStatus(Constants.Call_Status_False);
				topupResponse.setResultCode(Utilities.getValueFromJSON(response, "returnCode"));
				topupResponse.setResultDesc(Utilities.getErrorMessageFromFile("top.up", topupRequest.getLang(),
						Utilities.getValueFromJSON(response, "returnCode")));

			}
		} else {
			topupResponse.setCallStatus(Constants.Call_Status_False);
			topupResponse.setResultCode(Constants.API_FAILURE_CODE);
			topupResponse.setResultDesc(
					GetMessagesMappings.getMessageFromResourceBundle("connectivity.error", topupRequest.getLang()));
		}

		return topupResponse;
	}

	public MoneyTransferResponse moneyTransferBusiness(String msisdn, MoneyTransferRequest moneyTransferRequest,
			MoneyTransferResponse moneyTransferResponse) throws Exception {
		Utilities.printDebugLog(msisdn + "-Request received in " + Transactions.TRANSFER_MONEY_TRANSACTION_NAME
				+ "  BUSINESS with data-" + moneyTransferRequest.toString(), logger);

		RestClient rc = new RestClient();
		ObjectMapper objectMapper = new ObjectMapper();
		MoneyTransferResponseData resData = new MoneyTransferResponseData();

		String requestJsonESB = objectMapper.writeValueAsString(moneyTransferRequest);
		String path = GetConfigurations.getESBRoute("moneyTransfer");

		Utilities.printDebugLog(msisdn + "-Request Call to ESB", logger);
		Utilities.printDebugLog(msisdn + "-Path-" + path, logger);
		Utilities.printDebugLog(msisdn + "-Request Packet-" + requestJsonESB, logger);

		moneyTransferResponse.getLogsReport().setEsbRequestTime(Utilities.getReportDateTime());
		String response = rc.getResponseFromESB(path, requestJsonESB);
		moneyTransferResponse.getLogsReport().setEsbResponseTime(Utilities.getReportDateTime());

		// String response = HardCodedResponses.TRANSFER_BALANCE;
		Utilities.printDebugLog(msisdn + "-Received response from ESB-" + response, logger);

		// Logging ESB response code and description.
		moneyTransferResponse.setLogsReport(
				Utilities.logESBParamsintoReportLog(requestJsonESB, response, moneyTransferResponse.getLogsReport()));

		if (response != null && !response.isEmpty()) {

			if (Utilities.getValueFromJSON(response, "returnCode").equalsIgnoreCase(Constants.ESB_SUCCESS_CODE)) {
				resData.setOldBalance(Utilities.getValueFromJSON(response, "oldBalance"));
				resData.setNewBalance(Utilities.getValueFromJSON(response, "newBalance"));
				moneyTransferResponse.setData(resData);
				moneyTransferResponse.setCallStatus(Constants.Call_Status_True);
				moneyTransferResponse.setResultCode(Constants.APP_SERVER_SUCCESS_CODE);
				moneyTransferResponse.setResultDesc(
						GetMessagesMappings.getMessageFromResourceBundle("success", moneyTransferRequest.getLang()));

			} else {
				moneyTransferResponse.setCallStatus(Constants.Call_Status_False);
				moneyTransferResponse.setResultCode(Utilities.getValueFromJSON(response, "returnCode"));
				moneyTransferResponse.setResultDesc(Utilities.getErrorMessageFromFile("money.transfer",
						moneyTransferRequest.getLang(), Utilities.getValueFromJSON(response, "returnCode")));
			}

		} else {
			moneyTransferResponse.setCallStatus(Constants.Call_Status_False);
			moneyTransferResponse.setResultCode(Constants.API_FAILURE_CODE);

			moneyTransferResponse.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("connectivity.error",
					moneyTransferRequest.getLang()));
		}

		return moneyTransferResponse;
	}

	public GetLoanResponse getLoanBusiness(String msisdn, GetLoanRequest getLoanRequest,
			GetLoanResponse getLoanResponse) throws Exception {

		Utilities.printDebugLog(msisdn + "-Request received in " + Transactions.LOAN_REQUEST_TRANSACTION_NAME
				+ " BUSINESS with data-" + getLoanRequest.toString(), logger);

		if (getLoanRequest.getLoanAmount() == null) {
			getLoanRequest.setLoanAmount("0");
		}
		GetLoanResponseData resData = new GetLoanResponseData();
		RestClient rc = new RestClient();
		ObjectMapper mapper = new ObjectMapper();

		String requestJsonESB = mapper.writeValueAsString(getLoanRequest);

		requestJsonESB = Utilities.removeParamsFromJSONObject(requestJsonESB, "brandId");
//		requestJsonESB = Utilities.removeParamsFromJSONObject(requestJsonESB, "loanAmount");

		String path = GetConfigurations.getESBRoute("getLoan");
		Utilities.printDebugLog(msisdn + "-Request Call to ESB", logger);
		Utilities.printDebugLog(msisdn + "-Path-" + path, logger);
		Utilities.printDebugLog(msisdn + "-Request Packet-" + requestJsonESB, logger);

		getLoanResponse.getLogsReport().setEsbRequestTime(Utilities.getReportDateTime());
		String response = rc.getResponseFromESB(path, requestJsonESB);
		getLoanResponse.getLogsReport().setEsbResponseTime(Utilities.getReportDateTime());

		// String response = HardCodedResponses.GET_LOAN;
		Utilities.printDebugLog(msisdn + "-Received response from ESB-" + response, logger);

		// Logging ESB response code and description.
		getLoanResponse.setLogsReport(
				Utilities.logESBParamsintoReportLog(requestJsonESB, response, getLoanResponse.getLogsReport()));

		if (response != null && !response.isEmpty()) {

			if (Utilities.getValueFromJSON(response, "returnCode").equalsIgnoreCase(Constants.ESB_SUCCESS_CODE)) {

				resData.setNewBalance(Utilities.getValueFromJSON(response, "newBalance"));
				resData.setNewCredit(Utilities.getValueFromJSON(response, "newCredit"));
				getLoanResponse.setData(resData);
				getLoanResponse.setCallStatus(Constants.Call_Status_True);
				getLoanResponse.setResultCode(Constants.APP_SERVER_SUCCESS_CODE);
				getLoanResponse.setResultDesc(
						GetMessagesMappings.getMessageFromResourceBundle("success", getLoanRequest.getLang()));

			} else {
				resData.setMessage(Utilities.getErrorMessageFromFile("get.loan", getLoanRequest.getLang(),
						Utilities.getValueFromJSON(response, "returnCode")));
				getLoanResponse.setData(resData);
				getLoanResponse.setCallStatus(Constants.Call_Status_False);
				getLoanResponse.setResultCode(Utilities.getValueFromJSON(response, "returnCode"));

				getLoanResponse.setResultDesc(Utilities.getErrorMessageFromFile("get.loan", getLoanRequest.getLang(),
						Utilities.getValueFromJSON(response, "returnCode")));
			}

		} else {
			getLoanResponse.setCallStatus(Constants.Call_Status_False);
			getLoanResponse.setResultCode(Constants.API_FAILURE_CODE);
			getLoanResponse.setResultDesc(
					GetMessagesMappings.getMessageFromResourceBundle("connectivity.error", getLoanRequest.getLang()));
		}

		return getLoanResponse;
	}

	public LoanHistoryResponse getLoanHistory(String msisdn, LoanHistoryRequest loanHistoryRequest,
			LoanHistoryResponse loanHistoryResponse) throws Exception {

		Utilities.printDebugLog(msisdn + "-Request received in " + Transactions.LOAN_REQUEST_HISTORY_TRANSACTION_NAME
				+ " BUSINESS with data-" + loanHistoryRequest.toString(), logger);

		LoanHistoryResponseData resData = new LoanHistoryResponseData();
		RestClient rc = new RestClient();
		ObjectMapper mapper = new ObjectMapper();
		List<Provisions> provisionsList = new ArrayList<Provisions>();

		String requestJsonESB = mapper.writeValueAsString(loanHistoryRequest);

		requestJsonESB = Utilities.addParamsToJSONObject(requestJsonESB, "reportStartDate",
				Utilities.getValueFromJSON(requestJsonESB, "startDate"));
		requestJsonESB = Utilities.addParamsToJSONObject(requestJsonESB, "reportEndDate",
				Utilities.getValueFromJSON(requestJsonESB, "endDate"));
		requestJsonESB = Utilities.removeParamsFromJSONObject(requestJsonESB, "startDate");
		requestJsonESB = Utilities.removeParamsFromJSONObject(requestJsonESB, "endDate");

		String path = GetConfigurations.getESBRoute("getLoanHistory");
		Utilities.printDebugLog(msisdn + "-Request Call to ESB", logger);
		Utilities.printDebugLog(msisdn + "-Path-" + path, logger);
		Utilities.printDebugLog(msisdn + "-Request Packet-" + requestJsonESB, logger);

		// String response = HardCodedResponses.LOAN_HISTORY;

		loanHistoryResponse.getLogsReport().setEsbRequestTime(Utilities.getReportDateTime());
		String response = rc.getResponseFromESB(path, requestJsonESB);
		loanHistoryResponse.getLogsReport().setEsbResponseTime(Utilities.getReportDateTime());

		Utilities.printDebugLog(msisdn + "-Received response from ESB-" + response, logger);

		// Logging ESB response code and description.
		loanHistoryResponse.setLogsReport(
				Utilities.logESBParamsintoReportLog(requestJsonESB, response, loanHistoryResponse.getLogsReport()));

		if (response != null && !response.isEmpty()) {

			if (Utilities.getValueFromJSON(response, "returnCode").equalsIgnoreCase(Constants.ESB_SUCCESS_CODE)) {

				JSONObject jsonObjectResponse = new JSONObject(response);
				List<Loan> loanList = new ArrayList<Loan>();

				String loanListArray = jsonObjectResponse.getJSONArray("loan").toString();
				TypeReference<List<Loan>> mapTypeLoansList = new TypeReference<List<Loan>>() {
				};
				loanList = mapper.readValue(loanListArray, mapTypeLoansList);

				Utilities.printDebugLog(msisdn + "-100000-", logger);

				resData.setLoan(loanList);

				loanHistoryResponse.setData(resData);
				loanHistoryResponse.setCallStatus(Constants.Call_Status_True);
				loanHistoryResponse.setResultCode(Constants.APP_SERVER_SUCCESS_CODE);

				loanHistoryResponse.setResultDesc(
						GetMessagesMappings.getMessageFromResourceBundle("success", loanHistoryRequest.getLang()));

			} else {

				loanHistoryResponse.setCallStatus(Constants.Call_Status_False);
				loanHistoryResponse.setResultCode(Utilities.getValueFromJSON(response, "returnCode"));

				loanHistoryResponse.setResultDesc(Utilities.getErrorMessageFromFile("error.code.",
						loanHistoryRequest.getLang(), Utilities.getValueFromJSON(response, "returnCode")));
			}
		} else {

			loanHistoryResponse.setCallStatus(Constants.Call_Status_False);
			loanHistoryResponse.setResultCode(Constants.API_FAILURE_CODE);

			loanHistoryResponse.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("connectivity.error",
					loanHistoryRequest.getLang()));
		}
		return loanHistoryResponse;
	}

	public PaymentHistoryResponse getPaymentHistory(String msisdn, PaymentHistoryRequest paymentHistoryRequest,
			PaymentHistoryResponse paymentHistoryResponse) throws Exception {

		Utilities.printDebugLog(msisdn + "-Request received in " + Transactions.LOAN_PAYMENT_HISTORY_TRANSACTION_NAME
				+ " BUSINESS with data-" + paymentHistoryRequest.toString(), logger);

		PaymentHistoryResponseData resData = new PaymentHistoryResponseData();
		RestClient rc = new RestClient();
		ObjectMapper mapper = new ObjectMapper();

		String requestJsonESB = mapper.writeValueAsString(paymentHistoryRequest);

		requestJsonESB = Utilities.addParamsToJSONObject(requestJsonESB, "reportStartDate",
				Utilities.getValueFromJSON(requestJsonESB, "startDate"));
		requestJsonESB = Utilities.addParamsToJSONObject(requestJsonESB, "reportEndDate",
				Utilities.getValueFromJSON(requestJsonESB, "endDate"));
		requestJsonESB = Utilities.removeParamsFromJSONObject(requestJsonESB, "startDate");
		requestJsonESB = Utilities.removeParamsFromJSONObject(requestJsonESB, "endDate");

		String path = GetConfigurations.getESBRoute("getPaymentHistory");
		Utilities.printDebugLog(msisdn + "-Request Call to ESB", logger);
		Utilities.printDebugLog(msisdn + "-Path-" + path, logger);
		Utilities.printDebugLog(msisdn + "-Request Packet-" + requestJsonESB, logger);

		// String response = HardCodedResponses.PAYMENT_HISTORY;

		paymentHistoryResponse.getLogsReport().setEsbRequestTime(Utilities.getReportDateTime());
		String response = rc.getResponseFromESB(path, requestJsonESB);
		paymentHistoryResponse.getLogsReport().setEsbResponseTime(Utilities.getReportDateTime());

		Utilities.printDebugLog(msisdn + "-Received response from ESB-" + response, logger);

		// Logging ESB response code and description.

		paymentHistoryResponse.setLogsReport(
				Utilities.logESBParamsintoReportLog(requestJsonESB, response, paymentHistoryResponse.getLogsReport()));

		if (response != null && !response.isEmpty()) {

			if (Utilities.getValueFromJSON(response, "returnCode").equalsIgnoreCase(Constants.ESB_SUCCESS_CODE)) {

				JSONObject jsonObjectResponse = new JSONObject(response);
				JSONArray collectionArray = jsonObjectResponse.getJSONArray("collections");
				List<Payment> paymentList = new ArrayList<Payment>();

				for (int i = 0; i < collectionArray.length(); i++) {

					JSONObject jsonObject = collectionArray.getJSONObject(i);
					Payment payment = new Payment();
					payment.setAmount(jsonObject.getString("chargedAmount"));
					payment.setDateTime(jsonObject.getString("lastChargingDate"));
					payment.setLoanID(Integer.toString(jsonObject.getInt("debtId")));
					paymentList.add(payment);
				}

				resData.setPaymentHistory(paymentList);

				paymentHistoryResponse.setData(resData);
				paymentHistoryResponse.setCallStatus(Constants.Call_Status_True);
				paymentHistoryResponse.setResultCode(Constants.APP_SERVER_SUCCESS_CODE);

				paymentHistoryResponse.setResultDesc(
						GetMessagesMappings.getMessageFromResourceBundle("success", paymentHistoryRequest.getLang()));

			} else {
				paymentHistoryResponse.setCallStatus(Constants.Call_Status_False);
				paymentHistoryResponse.setResultCode(Utilities.getValueFromJSON(response, "returnCode"));

				paymentHistoryResponse.setResultDesc(Utilities.getErrorMessageFromFile("payment.history",
						paymentHistoryRequest.getLang(), Utilities.getValueFromJSON(response, "returnCode")));
			}
		} else {

			paymentHistoryResponse.setCallStatus(Constants.Call_Status_False);
			paymentHistoryResponse.setResultCode(Constants.API_FAILURE_CODE);

			paymentHistoryResponse.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("connectivity.error",
					paymentHistoryRequest.getLang()));
		}

		return paymentHistoryResponse;
	}

	/*
	 * private List<Loan> re_mapMessages(List<Loan> loanHistory, String lang) throws
	 * SocketException, SQLException {
	 * 
	 * for (int i = 0; i < loanHistory.size(); i++) { Loan loan = new Loan(); loan =
	 * loanHistory.get(i); if
	 * (loan.getStatus().equalsIgnoreCase(Constants.LOAN_STATUS_OPEN)) {
	 * loan.setStatus(GetMessagesMappings.getLabelsFromResourceBundle(Constants.
	 * LOAN_STATUS_INPROGRESS, lang)); } else if
	 * (loan.getStatus().equalsIgnoreCase(Constants.LOAN_STATUS_CLOSED)) {
	 * loan.setStatus(GetMessagesMappings.getLabelsFromResourceBundle(Constants.
	 * LOAN_STATUS_PAID, lang)); } loanHistory.set(i, loan); } return loanHistory; }
	 */
	public RequestMoneyResponse requestMoneyBusiness(String msisdn, RequestMoneyRequest requestMoneyRequest,
			RequestMoneyResponse requestMoneyResponse) throws Exception {

		Utilities.printDebugLog(msisdn + "-Request received in " + Transactions.REQUEST_MONEY_TRANSACTION_NAME
				+ " BUSINESS with data-" + requestMoneyRequest.toString(), logger);

		RequestMoneyResponseData resData = new RequestMoneyResponseData();
		RestClient rc = new RestClient();
		ObjectMapper mapper = new ObjectMapper();

		String requestJsonESB = mapper.writeValueAsString(requestMoneyRequest);

		String path = GetConfigurations.getESBRoute("requestMoney");
		Utilities.printDebugLog(msisdn + "-Request Call to ESB", logger);
		Utilities.printDebugLog(msisdn + "-Path-" + path, logger);
		Utilities.printDebugLog(msisdn + "-Request Packet-" + requestJsonESB, logger);

		requestMoneyResponse.getLogsReport().setEsbRequestTime(Utilities.getReportDateTime());
		String response = rc.getResponseFromESB(path, requestJsonESB);
		requestMoneyResponse.getLogsReport().setEsbResponseTime(Utilities.getReportDateTime());

		// String response = HardCodedResponses.GET_LOAN;
		Utilities.printDebugLog(msisdn + "-Received response from ESB-" + response, logger);

		// Logging ESB response code and description.
		requestMoneyResponse.setLogsReport(
				Utilities.logESBParamsintoReportLog(requestJsonESB, response, requestMoneyResponse.getLogsReport()));

		if (response != null && !response.isEmpty()) {

			if (Utilities.getValueFromJSON(response, "returnCode").equalsIgnoreCase(Constants.ESB_SUCCESS_CODE)) {

				requestMoneyResponse.setCallStatus(Constants.Call_Status_True);
				requestMoneyResponse.setResultCode(Constants.APP_SERVER_SUCCESS_CODE);
				requestMoneyResponse.setResultDesc(
						GetMessagesMappings.getMessageFromResourceBundle("success", requestMoneyRequest.getLang()));

			} else {

				requestMoneyResponse.setCallStatus(Constants.Call_Status_False);
				requestMoneyResponse.setResultCode(Utilities.getValueFromJSON(response, "returnCode"));

				requestMoneyResponse.setResultDesc(Utilities.getErrorMessageFromFile("error.code.",
						requestMoneyRequest.getLang(), Utilities.getValueFromJSON(response, "returnCode")));
			}

		} else {
			requestMoneyResponse.setCallStatus(Constants.Call_Status_False);
			requestMoneyResponse.setResultCode(Constants.API_FAILURE_CODE);
			requestMoneyResponse.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("connectivity.error",
					requestMoneyRequest.getLang()));
		}

		return requestMoneyResponse;
	}
}
