/**
 * 
 */
package com.evampsaanga.azerfon.business;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONObject;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.evampsaanga.azerfon.common.messagesmappings.GetMessagesMappings;
import com.evampsaanga.azerfon.common.utilities.Constants;
import com.evampsaanga.azerfon.common.utilities.GetConfigurations;
import com.evampsaanga.azerfon.common.utilities.HardCodedResponses;
import com.evampsaanga.azerfon.common.utilities.Transactions;
import com.evampsaanga.azerfon.common.utilities.Utilities;
import com.evampsaanga.azerfon.models.nartvservices.getsubscriptions.GetSubscriptionsRequest;
import com.evampsaanga.azerfon.models.nartvservices.getsubscriptions.GetSubscriptionsResponse;
import com.evampsaanga.azerfon.models.nartvservices.getsubscriptions.GetSubscriptionsResponseData;
import com.evampsaanga.azerfon.models.nartvservices.getsubscriptions.Subscriptions;
import com.evampsaanga.azerfon.models.nartvservices.migration.MigrationRequest;
import com.evampsaanga.azerfon.models.nartvservices.migration.MigrationResponse;
import com.evampsaanga.azerfon.models.nartvservices.migration.MigrationResponseData;
import com.evampsaanga.azerfon.restclient.RestClient;

/**
 * @author Evamp & Saanga
 *
 */
public class NarTvServicesBusiness {

	Logger logger = Logger.getLogger(NarTvServicesBusiness.class);

	public GetSubscriptionsResponse getSubscriptions(String msisdn, GetSubscriptionsRequest getSubscriptionsRequest,
			GetSubscriptionsResponse getSubscriptionsResponse) throws Exception {

		Utilities.printDebugLog(msisdn + "-Request received in " + Transactions.NAR_TV_GET_SUBSCRIPTIONS
				+ "  BUSINESS with data-" + getSubscriptionsRequest.toString(), logger);

		RestClient rc = new RestClient();
		ObjectMapper objectMapper = new ObjectMapper();
		GetSubscriptionsResponseData resData = new GetSubscriptionsResponseData();
		List<Subscriptions> plansList = new ArrayList<Subscriptions>();

		String requestJsonESB = objectMapper.writeValueAsString(getSubscriptionsRequest);
		String path = GetConfigurations.getESBRoute("getSubscriptions");

		Utilities.printDebugLog(msisdn + "-Request Call to ESB", logger);
		Utilities.printDebugLog(msisdn + "-Path-" + path, logger);
		Utilities.printDebugLog(msisdn + "-Request Packet-" + requestJsonESB, logger);

		getSubscriptionsResponse.getLogsReport().setEsbRequestTime(Utilities.getReportDateTime());
		String response = rc.getResponseFromESB(path, requestJsonESB);
		getSubscriptionsResponse.getLogsReport().setEsbResponseTime(Utilities.getReportDateTime());

		// String response = HardCodedResponses.NAR_TV_GET_SUBSCRIPTIONS;
		Utilities.printDebugLog(msisdn + "-Received response from ESB-" + response, logger);

		// Logging ESB response code and description.
		getSubscriptionsResponse.setLogsReport(Utilities.logESBParamsintoReportLog(requestJsonESB, response,
				getSubscriptionsResponse.getLogsReport()));

		if (response != null && !response.isEmpty()) {

			if (Utilities.getValueFromJSON(response, "returnCode").equalsIgnoreCase(Constants.ESB_SUCCESS_CODE)) {

				JSONObject jsonObject = new JSONObject(response);
				String plans = jsonObject.getJSONArray("subscriptions").toString();
				TypeReference<List<Subscriptions>> mapTypePlansList = new TypeReference<List<Subscriptions>>() {
				};
				plansList = objectMapper.readValue(plans, mapTypePlansList);

				resData.setSubscriptions(plansList);
				resData.setSubscriberId(jsonObject.getString("subscriberId"));

				getSubscriptionsResponse.setData(resData);
				getSubscriptionsResponse.setCallStatus(Constants.Call_Status_True);
				getSubscriptionsResponse.setResultCode(Constants.APP_SERVER_SUCCESS_CODE);
				getSubscriptionsResponse.setResultDesc(
						GetMessagesMappings.getMessageFromResourceBundle("success", getSubscriptionsRequest.getLang()));

			} else {

				getSubscriptionsResponse.setCallStatus(Constants.Call_Status_False);
				getSubscriptionsResponse.setResultCode(Utilities.getValueFromJSON(response, "returnCode"));
				getSubscriptionsResponse.setResultDesc(Utilities.getErrorMessageFromFile("money.transfer",
						getSubscriptionsRequest.getLang(), Utilities.getValueFromJSON(response, "returnCode")));
			}

		} else {
			getSubscriptionsResponse.setCallStatus(Constants.Call_Status_False);
			getSubscriptionsResponse.setResultCode(Constants.API_FAILURE_CODE);

			getSubscriptionsResponse.setResultDesc(GetMessagesMappings
					.getMessageFromResourceBundle("connectivity.error", getSubscriptionsRequest.getLang()));
		}

		return getSubscriptionsResponse;
	}

	public MigrationResponse migrationBusiness(String msisdn, MigrationRequest migrationRequest,
			MigrationResponse migrationResponse) throws Exception {

		Utilities.printDebugLog(msisdn + "-Request received in " + Transactions.NAR_TV_MIGRATE + " BUSINESS with data-"
				+ migrationRequest.toString(), logger);

		MigrationResponseData resData = new MigrationResponseData();
		RestClient rc = new RestClient();
		ObjectMapper mapper = new ObjectMapper();

		String requestJsonESB = mapper.writeValueAsString(migrationRequest);

		String path = GetConfigurations.getESBRoute("migration");
		Utilities.printDebugLog(msisdn + "-Request Call to ESB", logger);
		Utilities.printDebugLog(msisdn + "-Path-" + path, logger);
		Utilities.printDebugLog(msisdn + "-Request Packet-" + requestJsonESB, logger);

		migrationResponse.getLogsReport().setEsbRequestTime(Utilities.getReportDateTime());
		String response = rc.getResponseFromESB(path, requestJsonESB);
		migrationResponse.getLogsReport().setEsbResponseTime(Utilities.getReportDateTime());

		// String response = HardCodedResponses.NAR_TV_MIGRATION;
		Utilities.printDebugLog(msisdn + "-Received response from ESB-" + response, logger);

		// Logging ESB response code and description.
		migrationResponse.setLogsReport(
				Utilities.logESBParamsintoReportLog(requestJsonESB, response, migrationResponse.getLogsReport()));

		if (response != null && !response.isEmpty()) {

			if (Utilities.getValueFromJSON(response, "returnCode").equalsIgnoreCase(Constants.ESB_SUCCESS_CODE)) {

				resData.setMessage(
						GetMessagesMappings.getMessageFromResourceBundle("success", migrationRequest.getLang()));

				migrationResponse.setData(resData);

				migrationResponse.setCallStatus(Constants.Call_Status_True);
				migrationResponse.setResultCode(Constants.APP_SERVER_SUCCESS_CODE);
				migrationResponse.setResultDesc(
						GetMessagesMappings.getMessageFromResourceBundle("success", migrationRequest.getLang()));

			} else {

				migrationResponse.setCallStatus(Constants.Call_Status_False);
				migrationResponse.setResultCode(Utilities.getValueFromJSON(response, "returnCode"));
				migrationResponse.setResultDesc(Utilities.getErrorMessageFromFile("money.transfer",
						migrationRequest.getLang(), Utilities.getValueFromJSON(response, "returnCode")));
			}

		} else {

			migrationResponse.setCallStatus(Constants.Call_Status_False);
			migrationResponse.setResultCode(Constants.API_FAILURE_CODE);
			migrationResponse.setResultDesc(
					GetMessagesMappings.getMessageFromResourceBundle("connectivity.error", migrationRequest.getLang()));
		}

		return migrationResponse;
	}
}
