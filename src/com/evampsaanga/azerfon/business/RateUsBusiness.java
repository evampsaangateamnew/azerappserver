package com.evampsaanga.azerfon.business;

import java.net.SocketException;
import java.sql.SQLException;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONException;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.evampsaanga.azerfon.common.messagesmappings.GetMessagesMappings;
import com.evampsaanga.azerfon.common.utilities.Constants;
import com.evampsaanga.azerfon.common.utilities.GetConfigurations;
import com.evampsaanga.azerfon.common.utilities.Transactions;
import com.evampsaanga.azerfon.common.utilities.Utilities;
import com.evampsaanga.azerfon.models.rateus.RateUsRequest;
import com.evampsaanga.azerfon.models.rateus.RateUsResponse;
import com.evampsaanga.azerfon.models.rateus.RateUsResponseData;
import com.evampsaanga.azerfon.restclient.RestClient;

public class RateUsBusiness {
	Logger logger = Logger.getLogger(TariffBusiness.class);

	public RateUsResponse rateus(String msisdn, RateUsRequest rateUsRequest, RateUsResponse rateUsResponse)
			throws SocketException, JsonProcessingException, JSONException, SQLException {
		Utilities.printDebugLog(msisdn + "-Request received in " + Transactions.RATE_US_TRANSECTION_NAME
				+ " BUSINESS with data-" + rateUsRequest.toString(), logger);
		RateUsResponseData resData = new RateUsResponseData();
		RestClient rc = new RestClient();
		ObjectMapper mapper = new ObjectMapper();

		String requestJsonESB = mapper.writeValueAsString(rateUsRequest);

		String path = GetConfigurations.getESBRoute("rateus");
		Utilities.printDebugLog(msisdn + "-Request Call to ESB", logger);
		Utilities.printDebugLog(msisdn + "-Path-" + path, logger);
		Utilities.printDebugLog(msisdn + "-Request Packet-" + requestJsonESB, logger);
		requestJsonESB = Utilities.removeParamsFromJSONObject(requestJsonESB, "tariffName");
		rateUsResponse.getLogsReport().setEsbRequestTime(Utilities.getReportDateTime());

		String response = rc.getResponseFromESB(path, requestJsonESB);
		rateUsResponse.getLogsReport().setEsbResponseTime(Utilities.getReportDateTime());

		Utilities.printDebugLog(msisdn + "-Received response from ESB-" + response, logger);

		// Logging ESB response code and description.
		rateUsResponse.setLogsReport(
				Utilities.logESBParamsintoReportLog(requestJsonESB, response, rateUsResponse.getLogsReport()));
		if (response != null && !response.isEmpty()) {

			if (Utilities.getValueFromJSON(response, "returnCode").equalsIgnoreCase(Constants.ESB_SUCCESS_CODE)) {

				resData.setResponseMsg(
						GetMessagesMappings.getMessageFromResourceBundle("success", rateUsRequest.getLang()));

				rateUsResponse.setData(resData);
				rateUsResponse.setCallStatus(Constants.Call_Status_True);
				rateUsResponse.setResultCode(Constants.APP_SERVER_SUCCESS_CODE);
				rateUsResponse.setResultDesc(
						GetMessagesMappings.getMessageFromResourceBundle("success", rateUsRequest.getLang()));
				return rateUsResponse;

			} else {

				rateUsResponse.setCallStatus(Constants.Call_Status_False);
				rateUsResponse.setResultCode(Utilities.getValueFromJSON(response, "returnCode"));
				rateUsResponse.setResultDesc(Utilities.getErrorMessageFromFile("change.tariff", rateUsRequest.getLang(),
						Utilities.getValueFromJSON(response, "returnCode")));

			}
		} else {

			rateUsResponse.setCallStatus(Constants.Call_Status_False);
			rateUsResponse.setResultCode(Constants.API_FAILURE_CODE);
			rateUsResponse.setResultDesc(
					GetMessagesMappings.getMessageFromResourceBundle("connectivity.error", rateUsRequest.getLang()));
		}

		return rateUsResponse;
	}

}
