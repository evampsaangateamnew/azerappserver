package com.evampsaanga.azerfon.business;

import java.io.IOException;
import java.net.SocketException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONException;
import org.springframework.stereotype.Service;

import com.evampsaanga.azerfon.common.messagesmappings.GetMessagesMappings;
import com.evampsaanga.azerfon.common.utilities.AppCache;
import com.evampsaanga.azerfon.common.utilities.BaseRequest;
import com.evampsaanga.azerfon.common.utilities.Constants;
import com.evampsaanga.azerfon.common.utilities.GetConfigurations;
import com.evampsaanga.azerfon.common.utilities.Transactions;
import com.evampsaanga.azerfon.common.utilities.Utilities;
import com.evampsaanga.azerfon.models.survey.GetSurveyCountResponseData;
import com.evampsaanga.azerfon.models.survey.GetSurveyRequest;
import com.evampsaanga.azerfon.models.survey.GetSurveyResponse;
import com.evampsaanga.azerfon.models.survey.GetSurveyResponseData;
import com.evampsaanga.azerfon.models.survey.SaveSurveyRequest;
import com.evampsaanga.azerfon.models.survey.SaveSurveyResponse;
import com.evampsaanga.azerfon.models.survey.SaveSurveyResponseData;
import com.evampsaanga.azerfon.models.survey.Survey;
import com.evampsaanga.azerfon.models.survey.UserSurveyData;
import com.evampsaanga.azerfon.restclient.RestClient;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@Service
public class SurveyBusiness {
	Logger logger = Logger.getLogger(TopUpBusiness.class);

	public GetSurveyResponse getSurveyBusiness(String msisdn, GetSurveyRequest getSurveyRequest,
			GetSurveyResponse getSurveyResponse) throws JSONException, IOException, SQLException {

		Utilities.printDebugLog(msisdn + "-Request received in " + Transactions.GET_SURVEY_TRANSACTION_NAME
				+ " BUSINESS with data-" + getSurveyRequest.toString(), logger);

		String surveyCacheKey = Constants.HASH_KEY_SURVEY;

		if (AppCache.getHashMapSurveys().containsKey(surveyCacheKey)) {
			Utilities.printDebugLog(msisdn + "-SURVEYS" + Constants.CACHE_EXISTS_DESCRIPTION + "" + surveyCacheKey,
					logger);
			getSurveyResponse = AppCache.getHashMapSurveys().get(surveyCacheKey);
			getSurveyResponse.getLogsReport().setIsCached("true");
			getSurveyResponse = setSurveyCount(getSurveyRequest, getSurveyResponse);
			getSurveyResponse.getData().setUserSurveys(getUserSurveysBusiness(getSurveyRequest));
			return getSurveyResponse;
		} else {
			Utilities.printDebugLog(
					msisdn + "-SURVEYS" + Constants.CACHE_DOES_NOT_EXIST_DESCRIPTION + "" + surveyCacheKey, logger);
			RestClient rc = new RestClient();
			ObjectMapper mapper = new ObjectMapper();
			String requestJsonESB = mapper.writeValueAsString(getSurveyRequest);
			String path = GetConfigurations.getESBRoute("getSurveys");
			Utilities.printDebugLog(msisdn + "<<<<<<<<< -Request Call to ESB >>>>>>>>>", logger);
			Utilities.printDebugLog(msisdn + "-Path-" + path, logger);
			Utilities.printDebugLog(msisdn + "-Request Packet-" + requestJsonESB, logger);

			getSurveyResponse.getLogsReport().setEsbRequestTime(Utilities.getReportDateTime());
			String response = rc.getResponseFromESB(path, requestJsonESB);
			getSurveyResponse.getLogsReport().setEsbResponseTime(Utilities.getReportDateTime());

			Utilities.printDebugLog(msisdn + "-Received response from ESB-" + response, logger);

			// Logging ESB response code and description.
			getSurveyResponse.setLogsReport(
					Utilities.logESBParamsintoReportLog(requestJsonESB, response, getSurveyResponse.getLogsReport()));

			if (response != null && !response.isEmpty()) {

				if (Utilities.getValueFromJSON(response, "returnCode").equalsIgnoreCase(Constants.ESB_SUCCESS_CODE)) {
					List<Survey> resData = new ArrayList<>();
					resData = Arrays
							.asList(mapper.readValue(Utilities.getValueFromJSON(response, "surveys"), Survey[].class));
					GetSurveyResponseData getSurveyResponseData = new GetSurveyResponseData();
					getSurveyResponseData.setSurveys(resData);
					getSurveyResponseData.setUserSurveys(getUserSurveysBusiness(getSurveyRequest));
					getSurveyResponse.setData(getSurveyResponseData);
					getSurveyResponse.setCallStatus(Constants.Call_Status_True);
					getSurveyResponse.setResultCode(Constants.APP_SERVER_SUCCESS_CODE);
					getSurveyResponse.setResultDesc(
							GetMessagesMappings.getMessageFromResourceBundle("success", getSurveyRequest.getLang()));

					// Storing response in hashmap.
					AppCache.getHashMapSurveys().put(surveyCacheKey, getSurveyResponse);

					// Caching Time-stamp
					AppCache.getHashmapTimestamps().put(Constants.HASH_KEY_SURVEY,
							Utilities.getTimeStampForCache(Constants.HASH_KEY_SURVEY));

				} else {
					getSurveyResponse.setCallStatus(Constants.Call_Status_False);
					getSurveyResponse.setResultCode(Utilities.getValueFromJSON(response, "returnCode"));
					getSurveyResponse.setResultDesc(Utilities.getErrorMessageFromFile("connectivity.error",
							getSurveyRequest.getLang(), Utilities.getValueFromJSON(response, "returnCode")));
				}
			} else {

				getSurveyResponse.setCallStatus(Constants.Call_Status_False);
				getSurveyResponse.setResultCode(Constants.API_FAILURE_CODE);
				getSurveyResponse.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("connectivity.error",
						getSurveyRequest.getLang()));
			}
			return getSurveyResponse;
		}
	}

	public SaveSurveyResponse saveSurveyBusiness(String msisdn, SaveSurveyRequest saveSurveyRequest,
			SaveSurveyResponse saveSurveyResponse) throws JSONException, IOException, SQLException {
		Utilities.printDebugLog(msisdn + "-Request received in " + Transactions.GET_SURVEY_TRANSACTION_NAME
				+ " BUSINESS with data-" + saveSurveyRequest.toString(), logger);

		RestClient rc = new RestClient();
		ObjectMapper mapper = new ObjectMapper();

		String requestJsonESB = mapper.writeValueAsString(saveSurveyRequest);

		String path = GetConfigurations.getESBRoute("saveSurvey");
		Utilities.printDebugLog(msisdn + "<<<<<<<<< -Request Call to ESB >>>>>>>>>", logger);
		Utilities.printDebugLog(msisdn + "-Path-" + path, logger);
		Utilities.printDebugLog(msisdn + "-Request Packet-" + requestJsonESB, logger);

		saveSurveyResponse.getLogsReport().setEsbRequestTime(Utilities.getReportDateTime());
		String response = rc.getResponseFromESB(path, requestJsonESB);
		saveSurveyResponse.getLogsReport().setEsbResponseTime(Utilities.getReportDateTime());

		Utilities.printDebugLog(msisdn + "-Received response from ESB-" + response, logger);

		// Logging ESB response code and description.
		saveSurveyResponse.setLogsReport(
				Utilities.logESBParamsintoReportLog(requestJsonESB, response, saveSurveyResponse.getLogsReport()));

		if (response != null && !response.isEmpty()) {

			if (Utilities.getValueFromJSON(response, "returnCode").equalsIgnoreCase(Constants.ESB_SUCCESS_CODE)) {

				SaveSurveyResponseData resData = new SaveSurveyResponseData();

				GetSurveyRequest getSurveyRequest = new GetSurveyRequest();
				getSurveyRequest.setMsisdn(msisdn);
				getSurveyRequest.setChannel(saveSurveyRequest.getChannel());
				getSurveyRequest.setiP(saveSurveyRequest.getiP());
				getSurveyRequest.setLang(saveSurveyRequest.getLang());
				getSurveyRequest.setIsB2B(saveSurveyRequest.getIsB2B());

				resData.setSurveys(
						getSurveyBusiness(msisdn, getSurveyRequest, new GetSurveyResponse()).getData().getSurveys());
				resData.setUserSurveys(getUserSurveysBusiness(getSurveyRequest));
				saveSurveyResponse.setData(resData);
				saveSurveyResponse.setCallStatus(Constants.Call_Status_True);
				saveSurveyResponse.setResultCode(Constants.APP_SERVER_SUCCESS_CODE);
				saveSurveyResponse.setResultDesc(
						GetMessagesMappings.getMessageFromResourceBundle("success", saveSurveyRequest.getLang()));

			} else {
				saveSurveyResponse.setCallStatus(Constants.Call_Status_False);
				saveSurveyResponse.setResultCode(Utilities.getValueFromJSON(response, "returnCode"));
				saveSurveyResponse.setResultDesc(Utilities.getErrorMessageFromFile("connectivity.error",
						saveSurveyRequest.getLang(), Utilities.getValueFromJSON(response, "returnCode")));
			}
		} else {

			saveSurveyResponse.setCallStatus(Constants.Call_Status_False);
			saveSurveyResponse.setResultCode(Constants.API_FAILURE_CODE);
			saveSurveyResponse.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("connectivity.error",
					saveSurveyRequest.getLang()));
		}
		return saveSurveyResponse;
	}

	private GetSurveyResponse setSurveyCount(GetSurveyRequest getSurveyRequest, GetSurveyResponse getSurveyResponse)
			throws SocketException, JsonProcessingException, JSONException, IOException {

		RestClient rc = new RestClient();
		ObjectMapper mapper = new ObjectMapper();
		String requestJsonESB = mapper.writeValueAsString(getSurveyRequest);
		String path = GetConfigurations.getESBRoute("getAllSurveysCount");
		Utilities.printDebugLog(getSurveyRequest.getMsisdn() + "<<<<<<<<< -Request Call to ESB >>>>>>>>>", logger);
		Utilities.printDebugLog(getSurveyRequest.getMsisdn() + "-Path-" + path, logger);
		Utilities.printDebugLog(getSurveyRequest.getMsisdn() + "-Request Packet-" + requestJsonESB, logger);

		String response = rc.getResponseFromESB(path, requestJsonESB);

		if (response != null && !response.isEmpty()) {
			if (Utilities.getValueFromJSON(response, "returnCode").equalsIgnoreCase(Constants.ESB_SUCCESS_CODE)) {
				List<GetSurveyCountResponseData> resData = new ArrayList<>();
				resData = Arrays.asList(mapper.readValue(Utilities.getValueFromJSON(response, "surveyCountData"),
						GetSurveyCountResponseData[].class));

				HashMap<String, String> hashMap = new HashMap<>();
				for (GetSurveyCountResponseData scrd : resData) {
					hashMap.put(scrd.getSurveyId(), scrd.getSurveyCount());
				}

				for (Survey survey : getSurveyResponse.getData().getSurveys()) {
					survey.setSurveyCount(hashMap.get(survey.getId()));
				}

			} else {
				Utilities.printDebugLog(getSurveyRequest.getMsisdn() + "-Response from ESB getSurveysCount failed",
						logger);
			}
		}
		return getSurveyResponse;
	}

	public List<UserSurveyData> getUserSurveysBusiness(BaseRequest userSurveyRequest)
			throws JSONException, IOException, SQLException {
		String msisdn = userSurveyRequest.getMsisdn();
		List<UserSurveyData> userServeyData = new ArrayList<>();
		Utilities.printDebugLog(
				msisdn + "-Request received in User Survey Data BUSINESS with data-" + userSurveyRequest.toString(),
				logger);

		RestClient rc = new RestClient();
		ObjectMapper mapper = new ObjectMapper();
		String requestJsonESB = mapper.writeValueAsString(userSurveyRequest);

		String path = GetConfigurations.getESBRoute("getUserSurvey");
		Utilities.printDebugLog(msisdn + "<<<<<<<<< -Request Call to ESB >>>>>>>>>", logger);
		Utilities.printDebugLog(msisdn + "-Path-" + path, logger);
		Utilities.printDebugLog(msisdn + "-Request Packet-" + requestJsonESB, logger);

		String response = rc.getResponseFromESB(path, requestJsonESB);
		Utilities.printDebugLog(msisdn + "-Received response from ESB-" + response, logger);
		if (response != null && !response.isEmpty()) {
			if (Utilities.getValueFromJSON(response, "returnCode").equalsIgnoreCase(Constants.ESB_SUCCESS_CODE)) {
				userServeyData = Arrays.asList(
						mapper.readValue(Utilities.getValueFromJSON(response, "userSurveys"), UserSurveyData[].class));
			}
		}
		return userServeyData;
	}

}
