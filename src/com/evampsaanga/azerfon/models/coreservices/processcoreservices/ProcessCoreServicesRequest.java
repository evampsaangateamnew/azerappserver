/**
 * 
 */
package com.evampsaanga.azerfon.models.coreservices.processcoreservices;

import com.evampsaanga.azerfon.common.utilities.BaseRequest;

/**
 * @author Evamp & Saanga
 *
 */
public class ProcessCoreServicesRequest extends BaseRequest {

    private String actionType;
    private String groupType;
    private String accountType;
    private String brand;
    private String offeringId;
    private String number;
    private String userType;

    public String getOfferingId() {
	return offeringId;
    }

    public void setOfferingId(String offeringId) {
	this.offeringId = offeringId;
    }

    public String getNumber() {
	return number;
    }

    public void setNumber(String number) {
	this.number = number;
    }

    public String getActionType() {
	return actionType;
    }

    public void setActionType(String actionType) {
	this.actionType = actionType;
    }

    public String getGroupType() {
	return groupType;
    }

    public void setGroupType(String groupType) {
	this.groupType = groupType;
    }

    public String getAccountType() {
	return accountType;
    }

    public void setAccountType(String accountType) {
	this.accountType = accountType;
    }

    public String getBrand() {
	return brand;
    }

    public void setBrand(String brand) {
	this.brand = brand;
    }

    public String getUserType() {
	return userType;
    }

    public void setUserType(String userType) {
	this.userType = userType;
    }

    @Override
    public String toString() {
	return "ProcessCoreServicesRequest [actionType=" + actionType + ", groupType=" + groupType + ", accountType="
		+ accountType + ", brand=" + brand + ", offeringId=" + offeringId + ", number=" + number + ", userType="
		+ userType + ", getLang()=" + getLang() + ", getiP()=" + getiP() + ", getChannel()=" + getChannel()
		+ ", getMsisdn()=" + getMsisdn() + "]";
    }

}
