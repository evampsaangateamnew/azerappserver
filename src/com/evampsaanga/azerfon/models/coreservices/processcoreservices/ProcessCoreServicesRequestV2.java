/**
 * 
 */
package com.evampsaanga.azerfon.models.coreservices.processcoreservices;

import com.evampsaanga.azerfon.common.utilities.BaseRequest;

/**
 * @author Aqeel Abbas
 *
 */
public class ProcessCoreServicesRequestV2 extends BaseRequest {

	private String offeringId;
	private String actionType;
	private String users;
	private String number;
	private String orderKey;

	public String getOfferingId() {
		return offeringId;
	}

	public void setOfferingId(String offeringId) {
		this.offeringId = offeringId;
	}

	public String getActionType() {
		return actionType;
	}

	public void setActionType(String actionType) {
		this.actionType = actionType;
	}

	public String getUsers() {
		return users;
	}

	public void setUsers(String users) {
		this.users = users;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public String getOrderKey() {
		return orderKey;
	}

	public void setOrderKey(String orderKey) {
		this.orderKey = orderKey;
	}

	@Override
	public String toString() {
		return "ProcessCoreServicesRequestV2 [offeringId=" + offeringId + ", actionType=" + actionType + ", users="
				+ users + ", number=" + number + ", orderKey=" + orderKey + "]";
	}

}
