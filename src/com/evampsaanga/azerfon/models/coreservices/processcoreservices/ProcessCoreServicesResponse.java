/**
 * 
 */
package com.evampsaanga.azerfon.models.coreservices.processcoreservices;

import com.evampsaanga.azerfon.common.utilities.BaseResponse;

/**
 * @author Evamp & Saanga
 *
 */
public class ProcessCoreServicesResponse extends BaseResponse {
	private ProcessCoreServicesResponseData data;

	public ProcessCoreServicesResponseData getData() {
		return data;
	}

	public void setData(ProcessCoreServicesResponseData data) {
		this.data = data;
	}
}
