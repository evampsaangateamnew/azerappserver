package com.evampsaanga.azerfon.models.coreservices.getpaygstatus;

import com.evampsaanga.azerfon.common.utilities.BaseRequest;

public class GetPayGStatusRequest extends BaseRequest {

	@Override
	public String toString() {
		return "GetPayGStatusRequest [toString()=" + super.toString() + "]";
	}

}
