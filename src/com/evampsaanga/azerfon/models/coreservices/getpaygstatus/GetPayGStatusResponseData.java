package com.evampsaanga.azerfon.models.coreservices.getpaygstatus;

public class GetPayGStatusResponseData {
	private String status;
	private String offeringId;

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getOfferingId() {
		return offeringId;
	}

	public void setOfferingId(String offeringId) {
		this.offeringId = offeringId;
	}

	@Override
	public String toString() {
		return "GetPayGStatusResponseData [status=" + status + ", offeringId=" + offeringId + "]";
	}

}
