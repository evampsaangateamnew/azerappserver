package com.evampsaanga.azerfon.models.coreservices.getpaygstatus;

import com.evampsaanga.azerfon.common.utilities.BaseResponse;

public class GetPayGStatusResponse extends BaseResponse {
	private GetPayGStatusResponseData data;

	public GetPayGStatusResponseData getData() {
		return data;
	}

	public void setData(GetPayGStatusResponseData data) {
		this.data = data;
	}

	@Override
	public String toString() {
		return "GetPayGStatusResponse [data=" + data + ", toString()=" + super.toString() + "]";
	}

}
