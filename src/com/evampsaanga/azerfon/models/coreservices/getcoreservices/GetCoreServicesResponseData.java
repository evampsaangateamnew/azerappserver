/**
 * 
 */
package com.evampsaanga.azerfon.models.coreservices.getcoreservices;

import java.util.ArrayList;

/**
 * @author Evamp & Saanga
 *
 */

public class GetCoreServicesResponseData {
	private ArrayList<CoreServicesBaseClass> coreServices = new ArrayList<CoreServicesBaseClass>();

	public ArrayList<CoreServicesBaseClass> getCoreServices() {
		return coreServices;
	}

	public void setCoreServices(ArrayList<CoreServicesBaseClass> coreServices) {
		this.coreServices = coreServices;
	}
}
