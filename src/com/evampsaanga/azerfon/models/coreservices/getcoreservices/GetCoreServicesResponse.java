/**
 * 
 */
package com.evampsaanga.azerfon.models.coreservices.getcoreservices;

import com.evampsaanga.azerfon.common.utilities.BaseResponse;

/**
 * @author Evamp & Saanga
 *
 */
public class GetCoreServicesResponse extends BaseResponse {
	GetCoreServicesResponseData data;

	public GetCoreServicesResponseData getData() {
		return data;
	}

	public void setData(GetCoreServicesResponseData data) {
		this.data = data;
	}

}
