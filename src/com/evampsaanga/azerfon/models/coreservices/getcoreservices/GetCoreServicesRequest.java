/**
 * 
 */
package com.evampsaanga.azerfon.models.coreservices.getcoreservices;

import com.evampsaanga.azerfon.common.utilities.BaseRequest;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * @author Evamp & Saanga
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class GetCoreServicesRequest extends BaseRequest {

    private String accountType;
    private String groupType;
    private String selectedMsisdn;
    private String brand;
    private String userType;
    private String isFrom = "";

    public String getIsFrom() {
	return isFrom;
    }

    public void setIsFrom(String isFrom) {
	this.isFrom = isFrom;
    }

    public String getAccountType() {
	return accountType;
    }

    public void setAccountType(String accountType) {
	this.accountType = accountType;
    }

    public String getGroupType() {
	return groupType;
    }

    public void setGroupType(String groupType) {
	this.groupType = groupType;
    }

    public String getBrand() {
	return brand;
    }

    public void setBrand(String brand) {
	this.brand = brand;
    }

    public String getUserType() {
	return userType;
    }

    public void setUserType(String userType) {
	this.userType = userType;
    }

    public String getSelectedMsisdn() {
	return selectedMsisdn;
    }

    public void setSelectedMsisdn(String selectedMsisdn) {
	this.selectedMsisdn = selectedMsisdn;
    }

    @Override
    public String toString() {
	return "GetCoreServicesRequest [accountType=" + accountType + ", groupType=" + groupType + ", selectedMsisdn="
		+ selectedMsisdn + ", brand=" + brand + ", userType=" + userType + ", isFrom=" + isFrom
		+ ", getIsB2B()=" + getIsB2B() + ", getLang()=" + getLang() + ", getiP()=" + getiP() + ", getChannel()="
		+ getChannel() + ", getMsisdn()=" + getMsisdn() + "]";
    }

}
