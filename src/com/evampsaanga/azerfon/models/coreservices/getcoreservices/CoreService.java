/**
 * 
 */
package com.evampsaanga.azerfon.models.coreservices.getcoreservices;

/**
 * @author Evamp & Saanga
 *
 */
public class CoreService {
	private String offeringId;
	private String title;
	private String desc;
	private String status;

	private String forwardNumber;

	public String getOfferingId() {
		return offeringId;
	}

	public void setOfferingId(String offeringId) {
		this.offeringId = offeringId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getForwardNumber() {
		return forwardNumber;
	}

	public void setForwardNumber(String forwardNumber) {
		this.forwardNumber = forwardNumber;
	}

}
