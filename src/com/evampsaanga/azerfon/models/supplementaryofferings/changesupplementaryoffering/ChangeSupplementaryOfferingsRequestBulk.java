/**
 * 
 */
package com.evampsaanga.azerfon.models.supplementaryofferings.changesupplementaryoffering;

import java.util.ArrayList;

import com.evampsaanga.azerfon.common.utilities.BaseRequest;
import com.evampsaanga.azerfon.models.coreservices.processcoreservices.ProcessCoreServicesRequestDataV2;

/**
 * @author Aqeel Abbas
 *
 */
public class ChangeSupplementaryOfferingsRequestBulk extends BaseRequest {

	private String offeringId;
	private String actionType;
	private ArrayList<ProcessCoreServicesRequestDataV2> users;
	private String actPrice; 
	private String orderKey;
	
	public String getOrderKey() {
		return orderKey;
	}

	public void setOrderKey(String orderKey) {
		this.orderKey = orderKey;
	}

	public String getOfferingId() {
		return offeringId;
	}

	public void setOfferingId(String offeringId) {
		this.offeringId = offeringId;
	}

	public String getActionType() {
		return actionType;
	}

	public void setActionType(String actionType) {
		this.actionType = actionType;
	}

	public ArrayList<ProcessCoreServicesRequestDataV2> getUsers() {
		return users;
	}

	public void setUsers(ArrayList<ProcessCoreServicesRequestDataV2> users) {
		this.users = users;
	}

	public String getActPrice() {
		return actPrice;
	}

	public void setActPrice(String actPrice) {
		this.actPrice = actPrice;
	}

}
