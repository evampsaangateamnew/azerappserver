/**
 * 
 */
package com.evampsaanga.azerfon.models.supplementaryofferings.changesupplementaryoffering;

import com.evampsaanga.azerfon.common.utilities.BaseResponse;

/**
 * @author Evamp & Saanga
 *
 */
public class ChangeSupplementaryOfferingsResponsebulk extends BaseResponse {
	private ChangeSupplementaryOfferingsResponseDatabulk data;

	public ChangeSupplementaryOfferingsResponseDatabulk getData() {
		return data;
	}

	public void setData(ChangeSupplementaryOfferingsResponseDatabulk data) {
		this.data = data;
	}

}
