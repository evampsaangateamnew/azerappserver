/**
 * 
 */
package com.evampsaanga.azerfon.models.supplementaryofferings.changesupplementaryoffering;

import com.evampsaanga.azerfon.common.utilities.BaseResponse;

/**
 * @author Evamp & Saanga
 *
 */
public class ChangeSupplementaryOfferingsResponse extends BaseResponse {
	private ChangeSupplementaryOfferingsResponseData data;

	public ChangeSupplementaryOfferingsResponseData getData() {
		return data;
	}

	public void setData(ChangeSupplementaryOfferingsResponseData data) {
		this.data = data;
	}

}
