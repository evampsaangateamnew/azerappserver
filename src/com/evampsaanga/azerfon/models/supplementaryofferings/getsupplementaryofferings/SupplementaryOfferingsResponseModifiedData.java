/**
 * 
 */
package com.evampsaanga.azerfon.models.supplementaryofferings.getsupplementaryofferings;

import java.util.ArrayList;
import java.util.List;

import com.evampsaanga.azerfon.models.supplementaryofferings.getsupplementaryofferings.commonofferings.SupplementryOfferingsData;

/**
 * @author HamzaFarooque
 *
 */
public class SupplementaryOfferingsResponseModifiedData {
	
	List<SupplementryOfferingsData> offers = new ArrayList<SupplementryOfferingsData>();

	public List<SupplementryOfferingsData> getOffers() {
		return offers;
	}

	public void setOffers(List<SupplementryOfferingsData> offers) {
		this.offers = offers;
	}
	
}
