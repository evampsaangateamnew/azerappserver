package com.evampsaanga.azerfon.models.supplementaryofferings.getsupplementaryofferings;

import com.evampsaanga.azerfon.models.supplementaryofferings.getsupplementaryofferings.offerscategoriestab.Special;
import com.evampsaanga.azerfon.models.supplementaryofferings.getsupplementaryofferings.offerscategoriestab.TM;

public class SpecialOffersResponseData {
	
	 public TM getTm() {
		return tm;
	}
	public void setTm(TM tm) {
		this.tm = tm;
	}
	public Special getSpecial() {
		return special;
	}
	public void setSpecial(Special special) {
		this.special = special;
	}
	private TM tm;
	 private Special special;
	

}
