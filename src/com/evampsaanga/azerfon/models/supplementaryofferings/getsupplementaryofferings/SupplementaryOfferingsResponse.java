/**
 * 
 */
package com.evampsaanga.azerfon.models.supplementaryofferings.getsupplementaryofferings;

import com.evampsaanga.azerfon.common.utilities.BaseResponse;

/**
 * @author Evamp & Saanga
 *
 */
public class SupplementaryOfferingsResponse extends BaseResponse implements Cloneable{
	
	public SupplementaryOfferingsResponse(SupplementaryOfferingsResponse offeringsResponse)
	{
		this.data = new SupplementaryOfferingsResponseData(offeringsResponse.getData());
		this.setCallStatus(offeringsResponse.getCallStatus());
		this.setLogsReport(offeringsResponse.getLogsReport());
		this.setResultCode(offeringsResponse.getResultCode());
		this.setResultDesc(offeringsResponse.getResultDesc());
	}
	
@Override
    public Object clone() throws CloneNotSupportedException
    {
	SupplementaryOfferingsResponse dataObj = (SupplementaryOfferingsResponse) super.clone();
 
	dataObj.data=(SupplementaryOfferingsResponseData) data.clone();
       // student.data=(SupplementaryOfferingsResponseData) data.c
 
        return dataObj;
    }
	
	
	public SupplementaryOfferingsResponse()
	{
	}
	
	private SupplementaryOfferingsResponseData data;

	public SupplementaryOfferingsResponseData getData() {
		return data;
	}

	public void setData(SupplementaryOfferingsResponseData data) {
		this.data = data;
	}
}
