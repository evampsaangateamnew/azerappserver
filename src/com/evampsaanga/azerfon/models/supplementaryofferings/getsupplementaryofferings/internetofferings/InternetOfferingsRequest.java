package com.evampsaanga.azerfon.models.supplementaryofferings.getsupplementaryofferings.internetofferings;

import java.util.ArrayList;

import com.evampsaanga.azerfon.common.utilities.BaseRequest;

public class InternetOfferingsRequest extends BaseRequest {
	private String offeringName;
	private ArrayList<String> specialOfferIds = new ArrayList<String>();

	public String getOfferingName() {
		return offeringName;
	}

	public void setOfferingName(String offeringName) {
		this.offeringName = offeringName;
	}

	public ArrayList<String> getSpecialOfferIds() {
		return specialOfferIds;
	}

	public void setSpecialOfferIds(ArrayList<String> specialOfferIds) {
		this.specialOfferIds = specialOfferIds;
	}

	@Override
	public String toString() {
		return "InternetOfferingsRequest [offeringName=" + offeringName + ", specialOfferIds=" + specialOfferIds
				+ ", toString()=" + super.toString() + "]";
	}

}
