package com.evampsaanga.azerfon.models.supplementaryofferings.getsupplementaryofferings.internetofferings;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.evampsaanga.azerfon.models.supplementaryofferings.getsupplementaryofferings.offerscategoriestab.Filters;

public class MonthlyBundles implements Cloneable {

    private Filters filters;
    private List<InternetOfferingsData> offers;

    public MonthlyBundles() {
        this.offers = new ArrayList<>();
    }

    public Filters getFilters() {
        return filters;
    }

    public void setFilters(Filters filters) {
        this.filters = filters;
    }

    public List<InternetOfferingsData> getOffers() {
        return offers;
    }

    public void setOffers(List<InternetOfferingsData> offers) {
        this.offers = offers;
    }

    public Object clone() throws CloneNotSupportedException {
        // return super.clone();
        MonthlyBundles dataObj = (MonthlyBundles) super.clone();
        ArrayList<InternetOfferingsData> offerListClone = new ArrayList<>();
        Iterator<InternetOfferingsData> iterator = dataObj.getOffers().iterator();
        while (iterator.hasNext()) {
            offerListClone.add((InternetOfferingsData) iterator.next().clone());
        }
        dataObj.offers = offerListClone;
        return dataObj;

    }
}
