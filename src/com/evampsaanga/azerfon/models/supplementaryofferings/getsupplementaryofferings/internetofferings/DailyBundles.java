package com.evampsaanga.azerfon.models.supplementaryofferings.getsupplementaryofferings.internetofferings;

import com.evampsaanga.azerfon.models.supplementaryofferings.getsupplementaryofferings.offerscategoriestab.Filters;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class DailyBundles implements Cloneable {

    private Filters filters;
    private List<InternetOfferingsData> offers;

    public DailyBundles() {
        this.offers = new ArrayList<>();
    }

    public Filters getFilters() {
        return filters;
    }

    public void setFilters(Filters filters) {
        this.filters = filters;
    }

    public List<InternetOfferingsData> getOffers() {
        return offers;
    }

    public void setOffers(List<InternetOfferingsData> offers) {
        this.offers = offers;
    }

    public Object clone() throws CloneNotSupportedException {
        // return super.clone();

        DailyBundles dataObj = (DailyBundles) super.clone();
        //List<InternetOfferingsData> off=dataObj.getOffers();

        ArrayList<InternetOfferingsData> offerListClone = new ArrayList<>();

        Iterator<InternetOfferingsData> iterator = dataObj.getOffers().iterator();
        while (iterator.hasNext()) {
            offerListClone.add((InternetOfferingsData) iterator.next().clone());
        }
        // student.data=(SupplementaryOfferingsResponseData) data.c
        dataObj.offers = offerListClone;
        return dataObj;

    }
}