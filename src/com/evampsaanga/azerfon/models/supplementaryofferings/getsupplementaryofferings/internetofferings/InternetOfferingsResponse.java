package com.evampsaanga.azerfon.models.supplementaryofferings.getsupplementaryofferings.internetofferings;

import com.evampsaanga.azerfon.common.utilities.BaseResponse;

public class InternetOfferingsResponse extends BaseResponse implements Cloneable {
    private InternetOfferingsResponseData data;

    public InternetOfferingsResponse(InternetOfferingsResponse offeringsResponse) {
        this.data = new InternetOfferingsResponseData(offeringsResponse.getData());
        this.setCallStatus(offeringsResponse.getCallStatus());
        this.setLogsReport(offeringsResponse.getLogsReport());
        this.setResultCode(offeringsResponse.getResultCode());
        this.setResultDesc(offeringsResponse.getResultDesc());
    }


    public InternetOfferingsResponse() {
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        InternetOfferingsResponse dataObj = (InternetOfferingsResponse) super.clone();

        dataObj.data = (InternetOfferingsResponseData) data.clone();
        // student.data=(InternetOfferingsResponseData) data.c

        return dataObj;
    }

    public InternetOfferingsResponseData getData() {
        return data;
    }

    public void setData(InternetOfferingsResponseData data) {
        this.data = data;
    }

}
