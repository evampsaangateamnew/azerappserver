package com.evampsaanga.azerfon.models.supplementaryofferings.getsupplementaryofferings.internetofferings;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.evampsaanga.azerfon.models.supplementaryofferings.getsupplementaryofferings.offerscategoriestab.Filters;

public class AllBundles implements Cloneable {

	private Filters filters;
	private List<InternetOfferingsData> offers;

	public AllBundles() {
        this.offers = new ArrayList<>();
    }

	public Filters getFilters() {
		return filters;
	}

	public void setFilters(Filters filters) {
		this.filters = filters;
	}

	public List<InternetOfferingsData> getOffers() {
		return offers;
	}

	public void setOffers(List<InternetOfferingsData> offers) {
		this.offers = offers;
	}

	public Object clone() throws CloneNotSupportedException {
		// return super.clone();

		AllBundles dataObj = (AllBundles) super.clone();
		// List<InternetOfferingsData> off=dataObj.getOffers();

		ArrayList<InternetOfferingsData> offerListClone = new ArrayList<>();

		Iterator<InternetOfferingsData> iterator = dataObj.getOffers().iterator();
		while (iterator.hasNext()) {
			offerListClone.add((InternetOfferingsData) iterator.next().clone());
		}
		// student.data=(SupplementaryOfferingsResponseData) data.c
		dataObj.offers = offerListClone;
		return dataObj;

	}

}
