package com.evampsaanga.azerfon.models.supplementaryofferings.getsupplementaryofferings.internetofferings;

import java.util.ArrayList;

import com.evampsaanga.azerfon.models.supplementaryofferings.getsupplementaryofferings.commonofferings.DateTemplate;
import com.evampsaanga.azerfon.models.supplementaryofferings.getsupplementaryofferings.commonofferings.FreeResourceValidity;
import com.evampsaanga.azerfon.models.supplementaryofferings.getsupplementaryofferings.commonofferings.Rounding;
import com.evampsaanga.azerfon.models.supplementaryofferings.getsupplementaryofferings.commonofferings.TextWithOutTitle;
import com.evampsaanga.azerfon.models.supplementaryofferings.getsupplementaryofferings.commonofferings.TextWithPoints;
import com.evampsaanga.azerfon.models.supplementaryofferings.getsupplementaryofferings.commonofferings.TextWithTitle;
import com.evampsaanga.azerfon.models.supplementaryofferings.getsupplementaryofferings.commonofferings.TimeTemplate;
import com.evampsaanga.azerfon.models.supplementaryofferings.getsupplementaryofferings.commonofferings.TitleSubTitleListAndDesc;
import com.evampsaanga.azerfon.models.supplementaryofferings.getsupplementaryofferings.commonofferings.details_and_description.Price;
import com.evampsaanga.azerfon.models.supplementaryofferings.getsupplementaryofferings.roamingoffers.RoamingDetails;

public class InternetOffersDetailsAndDescription {

	private ArrayList<Price> price = new ArrayList<Price>();
	private Rounding rounding;
	private TextWithTitle textWithTitle;
	private TextWithOutTitle textWithOutTitle;
	private TextWithPoints textWithPoints;
	private TitleSubTitleListAndDesc titleSubTitleListAndDesc;
	private DateTemplate date;
	private TimeTemplate time;
	private RoamingDetails roamingDetails;
	private FreeResourceValidity freeResourceValidity;

	public ArrayList<Price> getPrice() {
		return price;
	}

	public void setPrice(ArrayList<Price> price) {
		this.price = price;
	}

	public Rounding getRounding() {
		return rounding;
	}

	public void setRounding(Rounding rounding) {
		this.rounding = rounding;
	}

	public TextWithTitle getTextWithTitle() {
		return textWithTitle;
	}

	public void setTextWithTitle(TextWithTitle textWithTitle) {
		this.textWithTitle = textWithTitle;
	}

	public TextWithOutTitle getTextWithOutTitle() {
		return textWithOutTitle;
	}

	public void setTextWithOutTitle(TextWithOutTitle textWithOutTitle) {
		this.textWithOutTitle = textWithOutTitle;
	}

	public TextWithPoints getTextWithPoints() {
		return textWithPoints;
	}

	public void setTextWithPoints(TextWithPoints textWithPoints) {
		this.textWithPoints = textWithPoints;
	}

	public TitleSubTitleListAndDesc getTitleSubTitleValueAndDesc() {
		return titleSubTitleListAndDesc;
	}

	public void setTitleSubTitleValueAndDesc(TitleSubTitleListAndDesc titleSubTitleValueAndDesc) {
		this.titleSubTitleListAndDesc = titleSubTitleValueAndDesc;
	}

	public DateTemplate getDate() {
		return date;
	}

	public void setDate(DateTemplate date) {
		this.date = date;
	}

	public TimeTemplate getTime() {
		return time;
	}

	public void setTime(TimeTemplate time) {
		this.time = time;
	}

	public FreeResourceValidity getFreeResourceValidity() {
		return freeResourceValidity;
	}

	public void setFreeResourceValidity(FreeResourceValidity freeResourceValidity) {
		this.freeResourceValidity = freeResourceValidity;
	}

	public RoamingDetails getRoamingDetails() {
		return roamingDetails;
	}

	public void setRoamingDetails(RoamingDetails roamingDetails) {
		this.roamingDetails = roamingDetails;
	}

}