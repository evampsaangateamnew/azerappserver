package com.evampsaanga.azerfon.models.supplementaryofferings.getsupplementaryofferings.internetofferings;

public class InternetOfferingsResponseData implements Cloneable {
	private DailyBundles dailyBundles;
	private MonthlyBundles monthlyBundles;
//	private HourlyBundles hourlyBundles;
	private AllBundles allBundles;

	public InternetOfferingsResponseData() {
		super();
		this.dailyBundles = new DailyBundles();
		this.monthlyBundles = new MonthlyBundles();
//		this.hourlyBundles = new HourlyBundles();
		this.allBundles = new AllBundles();
	}

	public InternetOfferingsResponseData(InternetOfferingsResponseData data) {
		super();
		this.dailyBundles = new DailyBundles();
		this.dailyBundles.getOffers().addAll(data.getDailyBundles().getOffers());

		this.monthlyBundles = new MonthlyBundles();
		this.monthlyBundles.getOffers().addAll(data.getMonthlyBundles().getOffers());
		
//		this.hourlyBundles = new HourlyBundles();
//		this.hourlyBundles.getOffers().addAll(data.getHourlyBundles().getOffers());
		
		this.allBundles = new AllBundles();
		this.allBundles.getOffers().addAll(data.getAllBundles().getOffers());

	}

	public DailyBundles getDailyBundles() {
		return dailyBundles;
	}

	public void setDailyBundles(DailyBundles dailyBundles) {
		this.dailyBundles = dailyBundles;
	}

	public MonthlyBundles getMonthlyBundles() {
		return monthlyBundles;
	}

	public void setMonthlyBundles(MonthlyBundles monthlyBundles) {
		this.monthlyBundles = monthlyBundles;
	}

//	public HourlyBundles getHourlyBundles() {
//		return hourlyBundles;
//	}
//
//	public void setHourlyBundles(HourlyBundles hourlyBundles) {
//		this.hourlyBundles = hourlyBundles;
//	}

	public AllBundles getAllBundles() {
		return allBundles;
	}

	public void setAllBundles(AllBundles allBundles) {
		this.allBundles = allBundles;
	}

	protected Object clone() throws CloneNotSupportedException {
		// return super.clone();

		InternetOfferingsResponseData dataObj = (InternetOfferingsResponseData) super.clone();

		dataObj.dailyBundles = (DailyBundles) dailyBundles.clone();
		dataObj.monthlyBundles = (MonthlyBundles) monthlyBundles.clone();
//		dataObj.hourlyBundles = (HourlyBundles) hourlyBundles.clone();
		dataObj.allBundles = (AllBundles) allBundles.clone();

		return dataObj;

	}

}
