package com.evampsaanga.azerfon.models.supplementaryofferings.getsupplementaryofferings.internetofferings;
public class InternetOfferingsData implements Cloneable {
	/*
	 * As per app designs. Follow information will cater in Header {-Offer Name,
	 * -Price, -Type as group - Batch Details validity and Attributes
	 */
	private InternetOffersHeaders Header;

	/*
	 * As per design Following information will be catered in Details {sub
	 * attributes,-roaming- advantages,- destination- points,- date,- time and
	 * free resource validity}
	 */
	private InternetOffersDetailsAndDescription Details;
	/*
	 * As per design Following information will be catered in Description {sub
	 * attributes,-roaming- advantages,- destination- points,- date,- time and
	 * free resource validity}
	 */
	private InternetOffersDetailsAndDescription Description;

	public InternetOffersHeaders getHeader() {
		return Header;
	}

	public void setHeader(InternetOffersHeaders header) {
		Header = header;
	}

	public InternetOffersDetailsAndDescription getDetails() {
		return Details;
	}

	public void setDetails(InternetOffersDetailsAndDescription details) {
		Details = details;
	}

	public InternetOffersDetailsAndDescription getDescription() {
		return Description;
	}

	public void setDescription(InternetOffersDetailsAndDescription description) {
		Description = description;
	}
	public Object clone() throws CloneNotSupportedException
    {
       // return super.clone();
        
		InternetOfferingsData dataObj = (InternetOfferingsData) super.clone();
        
    	dataObj.Header=(InternetOffersHeaders) Header.clone();
           // student.data=(SupplementaryOfferingsResponseData) data.c
     
            return dataObj;
        
    }
}