package com.evampsaanga.azerfon.models.supplementaryofferings.getsupplementaryofferings.internetofferings;

import java.util.List;

import com.evampsaanga.azerfon.models.mysubscriptions.getsubscriptions.HeaderUsage;
import com.evampsaanga.azerfon.models.supplementaryofferings.getsupplementaryofferings.commonofferings.header.GroupedOfferAttributes;
import com.evampsaanga.azerfon.models.supplementaryofferings.getsupplementaryofferings.commonofferings.header.HeaderAttributes;

public class InternetOffersHeaders implements Cloneable {
	private String id;
	private String type;
	private String offerName;
	private String stickerLabel;
	private String stickerColorCode;
	private String validityTitle;
	private String validityInformation;
	private String validityValue;
	private String price;
	private String appOfferFilter;
	private String offeringId;
	private String offerLevel;
	private String btnDeactivate;
	private String btnRenew;
	private String status;
	private String isFreeResource;
	private String isDaily = "";
	private String allowedForRenew = "";
	private String isAlreadySusbcribed = "";

	private GroupedOfferAttributes offerGroup;
	List<HeaderAttributes> attributeList;

	private Integer sortOrder;
	private Integer sortOrderMS;

	private String preReqOfferId;
//	@JsonIgnore
	private String isTopUp;
	private String visibleFor;
	private String hideFor;

	public String getVisibleFor() {
		return visibleFor;
	}

	public void setVisibleFor(String visibleFor) {
		this.visibleFor = visibleFor;
	}

	public String getHideFor() {
		return hideFor;
	}

	public void setHideFor(String hideFor) {
		this.hideFor = hideFor;
	}

	public String getPreReqOfferId() {
		return preReqOfferId;
	}

	public void setPreReqOfferId(String preReqOfferId) {
		this.preReqOfferId = preReqOfferId;
	}

	public String getIsTopUp() {
		return isTopUp;
	}

	public void setIsTopUp(String isTopUp) {
		this.isTopUp = isTopUp;
	}

	/*
	 * Below list of Header Usage class will only be used in my subscription
	 * section.
	 */
	private List<HeaderUsage> usage;

	public InternetOffersHeaders() {
		this.isFreeResource = "false";
	}

	public String getType() {

		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getOfferName() {
		return offerName;
	}

	public void setOfferName(String offerName) {
		this.offerName = offerName;
	}

	public String getStickerLabel() {
		return stickerLabel;
	}

	public void setStickerLabel(String stickerLabel) {
		this.stickerLabel = stickerLabel;
	}

	public String getStickerColorCode() {
		return stickerColorCode;
	}

	public void setStickerColorCode(String stickerColorCode) {
		this.stickerColorCode = stickerColorCode;
	}

	public String getValidityTitle() {
		return validityTitle;
	}

	public void setValidityTitle(String validityTitle) {
		this.validityTitle = validityTitle;
	}

	public String getValidityInformation() {
		return validityInformation;
	}

	public void setValidityInformation(String validityInformation) {
		this.validityInformation = validityInformation;
	}

	public String getValidityValue() {
		return validityValue;
	}

	public void setValidityValue(String validityValue) {
		this.validityValue = validityValue;
	}

	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}

	public GroupedOfferAttributes getOfferGroup() {
		return offerGroup;
	}

	public void setOfferGroup(GroupedOfferAttributes offerGroup) {
		this.offerGroup = offerGroup;
	}

	public List<HeaderAttributes> getAttributeList() {
		return attributeList;
	}

	public void setAttributeList(List<HeaderAttributes> attributeList) {
		this.attributeList = attributeList;
	}

	public String getAppOfferFilter() {
		return appOfferFilter;
	}

	public void setAppOfferFilter(String appOfferFilter) {
		this.appOfferFilter = appOfferFilter;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getOfferingId() {
		return offeringId;
	}

	public void setOfferingId(String offeringId) {
		this.offeringId = offeringId;
	}

	public String getOfferLevel() {
		return offerLevel;
	}

	public void setOfferLevel(String offerLevel) {
		this.offerLevel = offerLevel;
	}

	public List<HeaderUsage> getUsage() {
		return usage;
	}

	public void setUsage(List<HeaderUsage> usage) {
		this.usage = usage;
	}

	public String getBtnDeactivate() {
		return btnDeactivate;
	}

	public void setBtnDeactivate(String btnDeactivate) {
		this.btnDeactivate = btnDeactivate;
	}

	public String getBtnRenew() {
		return btnRenew;
	}

	public void setBtnRenew(String btnRenew) {
		this.btnRenew = btnRenew;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getIsFreeResource() {
		return isFreeResource;
	}

	public void setIsFreeResource(String isFreeResource) {
		this.isFreeResource = isFreeResource;
	}

	public Integer getSortOrder() {
		return sortOrder;
	}

	public void setSortOrder(Integer sortOrder) {
		this.sortOrder = sortOrder;
	}

	public Integer getSortOrderMS() {
		return sortOrderMS;
	}

	public void setSortOrderMS(Integer sortOrderMS) {
		this.sortOrderMS = sortOrderMS;
	}

	public String getIsDaily() {
		return isDaily;
	}

	public void setIsDaily(String isDaily) {
		this.isDaily = isDaily;
	}

	public String getIsAlreadySusbcribed() {
		return isAlreadySusbcribed;
	}

	public void setIsAlreadySusbcribed(String isAlreadySusbcribed) {
		this.isAlreadySusbcribed = isAlreadySusbcribed;
	}

	public String getAllowedForRenew() {
		return allowedForRenew;
	}

	public void setAllowedForRenew(String allowedForRenew) {
		this.allowedForRenew = allowedForRenew;
	}

	protected Object clone() throws CloneNotSupportedException {
		// return super.clone();

		InternetOffersHeaders dataObj = (InternetOffersHeaders) super.clone();

		/*
		 * dataObj.Header=(InternetOffersHeaders) Header.clone(); //
		 * student.data=(SupplementaryOfferingsResponseData) data.c
		 */
		return dataObj;
	}

}