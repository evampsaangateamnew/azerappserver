/**
 * 
 */
package com.evampsaanga.azerfon.models.supplementaryofferings.getsupplementaryofferings.offerscategoriestab;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.evampsaanga.azerfon.models.supplementaryofferings.getsupplementaryofferings.commonofferings.SupplementryOfferingsData;

/**
 * @author Evamp & Saanga
 *
 */
public class SMS implements Cloneable {
	private Filters filters;
	private List<SupplementryOfferingsData> offers;

	public SMS() {
		offers = new ArrayList<>();
	}

	public Filters getFilters() {
		return filters;
	}

	public void setFilters(Filters filters) {
		this.filters = filters;
	}

	public List<SupplementryOfferingsData> getOffers() {
		return offers;
	}

	public void setOffers(List<SupplementryOfferingsData> offers) {
		this.offers = offers;
	}
	public Object clone() throws CloneNotSupportedException
    {
       // return super.clone();
        
		SMS dataObjsms = (SMS) super.clone();
		//List<SupplementryOfferingsData> off=dataObj.getOffers();
		
		ArrayList<SupplementryOfferingsData> offerListClone = new ArrayList<>();
        
        Iterator<SupplementryOfferingsData> iterator = dataObjsms.getOffers().iterator();
        while(iterator.hasNext()){
        	offerListClone.add((SupplementryOfferingsData) iterator.next().clone());
        }
           // student.data=(SupplementaryOfferingsResponseData) data.c
        dataObjsms.offers=offerListClone;
            return dataObjsms;
        
    }
}
