/**
 * 
 */
package com.evampsaanga.azerfon.models.supplementaryofferings.getsupplementaryofferings.commonofferings;

import java.util.List;

/**
 * @author Evamp & Saanga
 *
 */
public class TextWithPoints {
	private String title;
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getTitleIcon() {
		return titleIcon;
	}

	public void setTitleIcon(String titleIcon) {
		this.titleIcon = titleIcon;
	}

	private String titleIcon;
	List<String> pointsList;

	public List<String> getPointsList() {
		return pointsList;
	}

	public void setPointsList(List<String> pointsList) {
		this.pointsList = pointsList;
	}

}
