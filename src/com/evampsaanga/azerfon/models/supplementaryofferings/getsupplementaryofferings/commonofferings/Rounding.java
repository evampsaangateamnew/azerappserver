/**
 * 
 */
package com.evampsaanga.azerfon.models.supplementaryofferings.getsupplementaryofferings.commonofferings;

import java.util.ArrayList;
import java.util.List;

import com.evampsaanga.azerfon.models.supplementaryofferings.getsupplementaryofferings.commonofferings.details_and_description.DetailsAttributes;

/**
 * @author Evamp & Saanga
 *
 */
public class Rounding {
	private String title;
	private String value;
	private String iconName;
	private String description;
	private String fragmentIcon;
	public String getFragmentIcon() {
		return fragmentIcon;
	}

	public void setFragmentIcon(String fragmentIcon) {
		this.fragmentIcon = fragmentIcon;
	}

	private List<DetailsAttributes> attributeList;

	public Rounding() {
		attributeList = new ArrayList<>();
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getIconName() {
		return iconName;
	}

	public void setIconName(String iconName) {
		this.iconName = iconName;
	}

	public List<DetailsAttributes> getAttributeList() {
		return attributeList;
	}

	public void setAttributeList(List<DetailsAttributes> attributeList) {
		this.attributeList = attributeList;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

}
