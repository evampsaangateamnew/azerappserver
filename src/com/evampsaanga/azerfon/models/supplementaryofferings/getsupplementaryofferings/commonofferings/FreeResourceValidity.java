/**
 * 
 */
package com.evampsaanga.azerfon.models.supplementaryofferings.getsupplementaryofferings.commonofferings;

/**
 * @author Evamp & Saanga
 *
 */
public class FreeResourceValidity {
	private String title;
	private String titleValue;
	private String subTitle;
	private String subTitleValue;
	private String description;
	private String freeResourceIcon;

	public String getFreeResourceIcon() {
		return freeResourceIcon;
	}

	public void setFreeResourceIcon(String freeResourceIcon) {
		this.freeResourceIcon = freeResourceIcon;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getTitleValue() {
		return titleValue;
	}

	public void setTitleValue(String titleValue) {
		this.titleValue = titleValue;
	}

	public String getSubTitle() {
		return subTitle;
	}

	public void setSubTitle(String subTitle) {
		this.subTitle = subTitle;
	}

	public String getSubTitleValue() {
		return subTitleValue;
	}

	public void setSubTitleValue(String subTitleValue) {
		this.subTitleValue = subTitleValue;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

}
