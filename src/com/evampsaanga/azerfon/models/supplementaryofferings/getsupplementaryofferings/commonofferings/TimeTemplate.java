/**
 * 
 */
package com.evampsaanga.azerfon.models.supplementaryofferings.getsupplementaryofferings.commonofferings;

/**
 * @author Evamp & Saanga
 *
 */
public class TimeTemplate {
	private String fromTitle;
	private String fromValue;
	private String toTitle;
	private String toValue;
	private String description;
	private String timeIcon;

	public String getTimeIcon() {
		return timeIcon;
	}

	public void setTimeIcon(String timeIcon) {
		this.timeIcon = timeIcon;
	}

	public String getFromTitle() {
		return fromTitle;
	}

	public void setFromTitle(String fromTitle) {
		this.fromTitle = fromTitle;
	}

	public String getFromValue() {
		return fromValue;
	}

	public void setFromValue(String fromValue) {
		this.fromValue = fromValue;
	}

	public String getToTitle() {
		return toTitle;
	}

	public void setToTitle(String toTitle) {
		this.toTitle = toTitle;
	}

	public String getToValue() {
		return toValue;
	}

	public void setToValue(String toValue) {
		this.toValue = toValue;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
}
