/**
 * 
 */
package com.evampsaanga.azerfon.models.supplementaryofferings.getsupplementaryofferings.commonofferings;

import java.util.List;

import com.evampsaanga.azerfon.models.supplementaryofferings.getsupplementaryofferings.commonofferings.details_and_description.DetailsAttributes;

/**
 * @author Evamp & Saanga
 *
 */
public class TitleSubTitleListAndDesc {
	private String title;
	private List<DetailsAttributes> attributeList;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public List<DetailsAttributes> getAttributeList() {
		return attributeList;
	}

	public void setAttributeList(List<DetailsAttributes> attributeList) {
		this.attributeList = attributeList;
	}

}
