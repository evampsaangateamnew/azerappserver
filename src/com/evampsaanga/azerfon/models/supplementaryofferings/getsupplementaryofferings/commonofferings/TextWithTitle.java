/**
 * 
 */
package com.evampsaanga.azerfon.models.supplementaryofferings.getsupplementaryofferings.commonofferings;

/**
 * @author Evamp & Saanga
 *
 */
public class TextWithTitle {
	private String title;
	private String text;
	private String titleIcon;

	public String getTitleIcon() {
		return titleIcon;
	}

	public void setTitleIcon(String titleIcon) {
		this.titleIcon = titleIcon;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

}
