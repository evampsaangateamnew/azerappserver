/**
 * 
 */
package com.evampsaanga.azerfon.models.supplementaryofferings.getsupplementaryofferings.commonofferings;

import java.util.List;

import com.evampsaanga.azerfon.models.supplementaryofferings.getsupplementaryofferings.commonofferings.details_and_description.DetailsAttributes;

/**
 * @author Evamp & Saanga
 *
 */
public class TitleSubTitleItem {
	private String title;
	private String value;
	private String iconName;
	private String description;
	private List<DetailsAttributes> attributeList;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getIconName() {
		return iconName;
	}

	public void setIconName(String iconName) {
		this.iconName = iconName;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public List<DetailsAttributes> getAttributeList() {
		return attributeList;
	}

	public void setAttributeList(List<DetailsAttributes> attributeList) {
		this.attributeList = attributeList;
	}
}
