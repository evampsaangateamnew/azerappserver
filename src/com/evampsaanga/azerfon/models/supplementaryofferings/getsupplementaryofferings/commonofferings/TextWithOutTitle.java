/**
 * 
 */
package com.evampsaanga.azerfon.models.supplementaryofferings.getsupplementaryofferings.commonofferings;

/**
 * @author Evamp & Saanga
 *
 */
public class TextWithOutTitle {

	private String description;
	private String titleIcon;
	private String Title;

	public String getTitle() {
		return Title;
	}

	public void setTitle(String title) {
		Title = title;
	}

	public String getTitleIcon() {
		return titleIcon;
	}

	public void setTitleIcon(String titleIcon) {
		this.titleIcon = titleIcon;
	}


	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
}
