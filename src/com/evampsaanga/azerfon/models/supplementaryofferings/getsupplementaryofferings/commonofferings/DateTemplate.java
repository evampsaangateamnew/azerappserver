/**
 * 
 */
package com.evampsaanga.azerfon.models.supplementaryofferings.getsupplementaryofferings.commonofferings;

/**
 * @author Evamp & Saanga
 *
 */
public class DateTemplate {
	private String fromTitle;
	private String fromValue;
	private String toTitle;
	private String toValue;
	private String description;
    private String dateIcon;
	public String getDateIcon() {
		return dateIcon;
	}

	public void setDateIcon(String dateIcon) {
		this.dateIcon = dateIcon;
	}

	public String getFromTitle() {
		return fromTitle;
	}

	public void setFromTitle(String fromTitle) {
		this.fromTitle = fromTitle;
	}

	public String getFromValue() {
		return fromValue;
	}

	public void setFromValue(String fromValue) {
		this.fromValue = fromValue;
	}

	public String getToTitle() {
		return toTitle;
	}

	public void setToTitle(String toTitle) {
		this.toTitle = toTitle;
	}

	public String getToValue() {
		return toValue;
	}

	public void setToValue(String toValue) {
		this.toValue = toValue;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
}
