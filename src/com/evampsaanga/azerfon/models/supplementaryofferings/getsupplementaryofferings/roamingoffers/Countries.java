package com.evampsaanga.azerfon.models.supplementaryofferings.getsupplementaryofferings.roamingoffers;


public class Countries {

	private String name;
	private String flag;
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getFlag() {
		return flag;
	}

	public void setFlag(String flag) {
		this.flag = flag;
	}

}
