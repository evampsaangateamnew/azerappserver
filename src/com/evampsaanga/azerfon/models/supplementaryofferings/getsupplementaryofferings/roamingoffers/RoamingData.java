/**
 * 
 */
package com.evampsaanga.azerfon.models.supplementaryofferings.getsupplementaryofferings.roamingoffers;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.evampsaanga.azerfon.models.supplementaryofferings.getsupplementaryofferings.commonofferings.SupplementryOfferingsData;
import com.evampsaanga.azerfon.models.supplementaryofferings.getsupplementaryofferings.offerscategoriestab.Filters;
import com.evampsaanga.azerfon.models.supplementaryofferings.getsupplementaryofferings.offerscategoriestab.SMS;

/**
 * @author Evamp & Saanga
 *
 */
public class RoamingData implements Cloneable {
	private List<Countries> countries;
	private List<SupplementryOfferingsData> offers;
	private Filters filters;

	public RoamingData() {
		this.offers = new ArrayList<>();
		this.countries = new ArrayList<Countries>();
	}

	public List<Countries> getCountries() {
		return countries;
	}

	public Filters getFilters() {
		return filters;
	}

	public void setFilters(Filters filters) {
		this.filters = filters;
	}

	public void setCountries(List<Countries> countries) {
		this.countries = countries;
	}

	public List<SupplementryOfferingsData> getOffers() {
		return offers;
	}

	public void setOffers(List<SupplementryOfferingsData> list) {
		this.offers = list;
	}
	public Object clone() throws CloneNotSupportedException
    {
       // return super.clone();
        
		RoamingData dataObj = (RoamingData) super.clone();
		//List<SupplementryOfferingsData> off=dataObj.getOffers();
		
		ArrayList<SupplementryOfferingsData> offerListClone = new ArrayList<>();
        
        Iterator<SupplementryOfferingsData> iterator = dataObj.getOffers().iterator();
        while(iterator.hasNext()){
        	offerListClone.add((SupplementryOfferingsData) iterator.next().clone());
        }
           // student.data=(SupplementaryOfferingsResponseData) data.c
        dataObj.offers=offerListClone;
            return dataObj;
        
    }
}
