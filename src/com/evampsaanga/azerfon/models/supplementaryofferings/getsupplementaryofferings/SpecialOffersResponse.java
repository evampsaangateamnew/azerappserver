package com.evampsaanga.azerfon.models.supplementaryofferings.getsupplementaryofferings;

import com.evampsaanga.azerfon.common.utilities.BaseResponse;

public class SpecialOffersResponse extends BaseResponse {
	
	SpecialOffersResponseData data=new SpecialOffersResponseData();

	public SpecialOffersResponseData getData() {
		return data;
	}

	public void setData(SpecialOffersResponseData data) {
		this.data = data;
	}
	

}
