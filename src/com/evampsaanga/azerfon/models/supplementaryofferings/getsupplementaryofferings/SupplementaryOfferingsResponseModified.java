/**
 * 
 */
package com.evampsaanga.azerfon.models.supplementaryofferings.getsupplementaryofferings;

import com.evampsaanga.azerfon.common.utilities.BaseResponse;

/**
 * @author HamzaFarooque
 *
 */
public class SupplementaryOfferingsResponseModified extends BaseResponse{
	
	private SupplementaryOfferingsResponseModifiedData data;

	public SupplementaryOfferingsResponseModifiedData getData() {
		return data;
	}

	public void setData(SupplementaryOfferingsResponseModifiedData data) {
		this.data = data;
	}
	
}
