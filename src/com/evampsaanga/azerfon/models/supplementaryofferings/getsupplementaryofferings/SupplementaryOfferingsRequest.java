/**
 * 
 */
package com.evampsaanga.azerfon.models.supplementaryofferings.getsupplementaryofferings;

import java.util.ArrayList;

import com.evampsaanga.azerfon.common.utilities.BaseRequest;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * @author Evamp & Saanga
 *
 */
@JsonIgnoreProperties(ignoreUnknown=true)
public class SupplementaryOfferingsRequest extends BaseRequest {
	private String offeringName;
	private ArrayList<String> specialOfferIds=new ArrayList<String>();	
	public ArrayList<String> getSpecialOfferIds() {
		return specialOfferIds;
	}
	public void setSpecialOfferIds(ArrayList<String> specialOfferIds) {
		this.specialOfferIds = specialOfferIds;
	}
	

	public String getOfferingName() {
		return offeringName;
	}

	public void setOfferingName(String offeringName) {
		this.offeringName = offeringName;
	}


	@Override
	public String toString() {
		return "SupplementaryOfferingsRequest [offeringName=" + offeringName + "]";
	}

}
