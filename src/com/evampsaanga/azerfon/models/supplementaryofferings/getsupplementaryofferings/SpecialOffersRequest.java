package com.evampsaanga.azerfon.models.supplementaryofferings.getsupplementaryofferings;

import java.util.ArrayList;

import com.evampsaanga.azerfon.common.utilities.BaseRequest;

public class SpecialOffersRequest extends BaseRequest {
	
	

	private String offeringName;
	
	private ArrayList<String> specialOfferIds=new ArrayList<String>();	
	private String groupIds;
	public String getGroupIds() {
		return groupIds;
	}
	public void setGroupIds(String groupIds) {
		this.groupIds = groupIds;
	}
	public ArrayList<String> getSpecialOfferIds() {
		return specialOfferIds;
	}
	public void setSpecialOfferIds(ArrayList<String> specialOfferIds) {
		this.specialOfferIds = specialOfferIds;
	}
	
	public String getOfferingName() {
		return offeringName;
	}
	public void setOfferingName(String offeringName) {
		this.offeringName = offeringName;
	}



}
