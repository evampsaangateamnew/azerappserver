package com.evampsaanga.azerfon.models.supplementaryofferings.getsupplementaryofferings;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.evampsaanga.azerfon.models.mysubscriptions.getsubscriptions.HeaderUsage;
import com.evampsaanga.azerfon.models.supplementaryofferings.getsupplementaryofferings.commonofferings.SupplementryOfferingsData;
import com.evampsaanga.azerfon.models.supplementaryofferings.getsupplementaryofferings.offerscategoriestab.Hybrid;

public class InternetInclusiveOffers implements Cloneable{
    private List<SupplementryOfferingsData> offers;
    private List<HeaderUsage> inclusiveUsageList;

    public InternetInclusiveOffers() {
	this.offers = new ArrayList<>();
	this.inclusiveUsageList = new ArrayList<>();
    }

    public List<SupplementryOfferingsData> getOffers() {
	return offers;
    }

    public void setOffers(List<SupplementryOfferingsData> offers) {
	this.offers = offers;
    }

    public List<HeaderUsage> getInclusiveUsageList() {
	return inclusiveUsageList;
    }

    public void setInclusiveUsageList(List<HeaderUsage> inclusiveUsageList) {
	this.inclusiveUsageList = inclusiveUsageList;
    }
    public Object clone() throws CloneNotSupportedException
    {
       // return super.clone();
        
    	InternetInclusiveOffers dataObj = (InternetInclusiveOffers) super.clone();
		//List<SupplementryOfferingsData> off=dataObj.getOffers();
		
		ArrayList<SupplementryOfferingsData> offerListClone = new ArrayList<>();
        
        Iterator<SupplementryOfferingsData> iterator = dataObj.getOffers().iterator();
        while(iterator.hasNext()){
        	offerListClone.add((SupplementryOfferingsData) iterator.next().clone());
        }
           // student.data=(SupplementaryOfferingsResponseData) data.c
        dataObj.offers=offerListClone;
            return dataObj;
        
    }
}
