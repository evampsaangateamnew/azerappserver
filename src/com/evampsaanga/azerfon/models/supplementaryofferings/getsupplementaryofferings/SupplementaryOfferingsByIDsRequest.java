/**
 * 
 */
package com.evampsaanga.azerfon.models.supplementaryofferings.getsupplementaryofferings;

import java.util.List;

import com.evampsaanga.azerfon.common.utilities.BaseRequest;

/**
 * @author Evamp & Saanga
 *
 */
public class SupplementaryOfferingsByIDsRequest extends BaseRequest {

	private List<String> offeringIds;

	public List<String> getOfferingIds() {
		return offeringIds;
	}

	public void setOfferingIds(List<String> offeringIds) {
		this.offeringIds = offeringIds;
	}

	@Override
	public String toString() {
		return "SupplementaryOfferingsByIDsRequest [offeringIds=" + offeringIds + ", getLang()=" + getLang()
				+ ", getiP()=" + getiP() + ", getChannel()=" + getChannel() + ", getMsisdn()=" + getMsisdn() + "]";
	}

}
