/**
 * 
 */
package com.evampsaanga.azerfon.models.supplementaryofferings.getsupplementaryofferings;

import com.evampsaanga.azerfon.models.supplementaryofferings.getsupplementaryofferings.offerscategoriestab.Call;
import com.evampsaanga.azerfon.models.supplementaryofferings.getsupplementaryofferings.offerscategoriestab.Compaign;
import com.evampsaanga.azerfon.models.supplementaryofferings.getsupplementaryofferings.offerscategoriestab.Hybrid;
import com.evampsaanga.azerfon.models.supplementaryofferings.getsupplementaryofferings.offerscategoriestab.Internet;
import com.evampsaanga.azerfon.models.supplementaryofferings.getsupplementaryofferings.offerscategoriestab.SMS;
import com.evampsaanga.azerfon.models.supplementaryofferings.getsupplementaryofferings.offerscategoriestab.TM;
import com.evampsaanga.azerfon.models.supplementaryofferings.getsupplementaryofferings.roamingoffers.RoamingData;

/**
 * @author Evamp & Saanga
 *
 */
public class SupplementaryOfferingsResponseData implements Cloneable {
    private Internet internet;
    private Compaign campaign;
    private SMS sms;
    private Call call;
    private TM tm;
    private Hybrid hybrid;
    private RoamingData roaming;
    private InternetInclusiveOffers internetInclusiveOffers;
    private VoiceInclusiveOffers voiceInclusiveOffers;
    private SMSInclusiveOffers smsInclusiveOffers;

    /**
     * @param internet
     * @param campaign
     * @param sms
     * @param call
     * @param tm
     * @param hybrid
     * @param roaming
     * @param internetInclusiveOffers
     * @param voiceInclusiveOffers
     * @param smsInclusiveOffers
     */
    public SupplementaryOfferingsResponseData() {
	super();
	this.internet = new Internet();
	this.campaign = new Compaign();
	this.sms = new SMS();
	this.call = new Call();
	this.tm = new TM();
	this.hybrid = new Hybrid();
	this.roaming = new RoamingData();
	this.internetInclusiveOffers = new InternetInclusiveOffers();
	this.voiceInclusiveOffers = new VoiceInclusiveOffers();
	this.smsInclusiveOffers = new SMSInclusiveOffers();
    }
    
    public SupplementaryOfferingsResponseData(SupplementaryOfferingsResponseData data) {
    	super();
    	this.internet = new Internet();
    	this.internet.getOffers().addAll(data.getInternet().getOffers());
    	
    	this.campaign = new Compaign();
    	this.campaign.getOffers().addAll(data.getCampaign().getOffers());
    	
    	this.sms = new SMS();
    	this.sms.getOffers().addAll(data.getSms().getOffers());
    	
    	this.call = new Call();
    	this.call.getOffers().addAll(data.getCall().getOffers());
    	
    	this.tm = new TM();
    	this.tm.getOffers().addAll(data.getTm().getOffers());
    	
    	this.hybrid = new Hybrid();
    	this.hybrid.getOffers().addAll(data.getHybrid().getOffers());
    	
    	this.roaming = new RoamingData();
    	this.roaming.getOffers().addAll(data.getRoaming().getOffers());
    	
    	this.internetInclusiveOffers = new InternetInclusiveOffers();
    	this.internetInclusiveOffers.getOffers().addAll(data.getInternetInclusiveOffers().getOffers());
    	
    	this.voiceInclusiveOffers = new VoiceInclusiveOffers();
    	this.voiceInclusiveOffers.getOffers().addAll(data.getVoiceInclusiveOffers().getOffers());
    	
    	this.smsInclusiveOffers = new SMSInclusiveOffers();
    	this.smsInclusiveOffers.getOffers().addAll(data.getSmsInclusiveOffers().getOffers());
        }

    public Internet getInternet() {
	return internet;
    }

    public void setInternet(Internet internet) {
	this.internet = internet;
    }

    public Compaign getCampaign() {
	return campaign;
    }

    public void setCampaign(Compaign campaign) {
	this.campaign = campaign;
    }

    public SMS getSms() {
	return sms;
    }

    public void setSms(SMS sms) {
	this.sms = sms;
    }

    public Call getCall() {
	return call;
    }

    public void setCall(Call call) {
	this.call = call;
    }

    public TM getTm() {
	return tm;
    }

    public void setTm(TM tm) {
	this.tm = tm;
    }

    public Hybrid getHybrid() {
	return hybrid;
    }

    public void setHybrid(Hybrid hybrid) {
	this.hybrid = hybrid;
    }

    public RoamingData getRoaming() {
	return roaming;
    }

    public void setRoaming(RoamingData roaming) {
	this.roaming = roaming;
    }

    public InternetInclusiveOffers getInternetInclusiveOffers() {
	return internetInclusiveOffers;
    }

    public void setInternetInclusiveOffers(InternetInclusiveOffers internetInclusiveOffers) {
	this.internetInclusiveOffers = internetInclusiveOffers;
    }

    public VoiceInclusiveOffers getVoiceInclusiveOffers() {
	return voiceInclusiveOffers;
    }

    public void setVoiceInclusiveOffers(VoiceInclusiveOffers voiceInclusiveOffers) {
	this.voiceInclusiveOffers = voiceInclusiveOffers;
    }

    public SMSInclusiveOffers getSmsInclusiveOffers() {
	return smsInclusiveOffers;
    }

    public void setSmsInclusiveOffers(SMSInclusiveOffers smsInclusiveOffers) {
	this.smsInclusiveOffers = smsInclusiveOffers;
    }
    protected Object clone() throws CloneNotSupportedException
    {
       // return super.clone();
        
        SupplementaryOfferingsResponseData dataObj = (SupplementaryOfferingsResponseData) super.clone();
        
    	dataObj.internet=(Internet) internet.clone();
    	dataObj.sms=(SMS) sms.clone();
    	dataObj.roaming=(RoamingData) roaming.clone();
    	dataObj.call=(Call) call.clone();
    	dataObj.hybrid=(Hybrid) hybrid.clone();
    	dataObj.internetInclusiveOffers=(InternetInclusiveOffers) internetInclusiveOffers.clone();
     	dataObj.voiceInclusiveOffers=(VoiceInclusiveOffers) voiceInclusiveOffers.clone();
     	dataObj.smsInclusiveOffers=(SMSInclusiveOffers) smsInclusiveOffers.clone();
           // student.data=(SupplementaryOfferingsResponseData) data.c
     
            return dataObj;
        
    }
}
