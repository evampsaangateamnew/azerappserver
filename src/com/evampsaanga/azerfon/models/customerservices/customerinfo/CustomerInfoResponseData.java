/**
 * 
 */
package com.evampsaanga.azerfon.models.customerservices.customerinfo;

import java.util.List;

import com.evampsaanga.azerfon.common.utilities.BaseResponse;
import com.evampsaanga.azerfon.models.customerservices.authenticateuser.PrimaryOfferings;
import com.evampsaanga.azerfon.models.customerservices.authenticateuser.SupplementaryOfferings;
import com.evampsaanga.azerfon.models.generalservices.predefineddata.PredefinedDataResponse;

/**
 * @author Evamp & Saanga
 *
 */
public class CustomerInfoResponseData extends BaseResponse {

	private CustomerInfo customerData;
	private List<SupplementaryOfferings> supplementaryOfferingList;
	private PrimaryOfferings primaryOffering;
	private PredefinedDataResponse predefinedData;

	public CustomerInfo getCustomerData() {
		return customerData;
	}

	public void setCustomerData(CustomerInfo customerData) {
		this.customerData = customerData;
	}

	public List<SupplementaryOfferings> getSupplementaryOfferingList() {
		return supplementaryOfferingList;
	}

	public void setSupplementaryOfferingList(List<SupplementaryOfferings> supplementaryOfferingList) {
		this.supplementaryOfferingList = supplementaryOfferingList;
	}

	public PrimaryOfferings getPrimaryOffering() {
		return primaryOffering;
	}

	public void setPrimaryOffering(PrimaryOfferings primaryOffering) {
		this.primaryOffering = primaryOffering;
	}

	public PredefinedDataResponse getPredefinedData() {
		return predefinedData;
	}

	public void setPredefinedData(PredefinedDataResponse predefinedData) {
		this.predefinedData = predefinedData;
	}
}
