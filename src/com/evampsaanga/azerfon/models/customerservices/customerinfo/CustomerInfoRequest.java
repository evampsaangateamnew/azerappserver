/**
 * 
 */
package com.evampsaanga.azerfon.models.customerservices.customerinfo;

/**
 * @author Evamp & Saanga
 *
 */
public class CustomerInfoRequest {

	private String lang;
	private String iP;
	private String channel;
	private String msisdn;

	public String getLang() {
		return lang;
	}

	public void setLang(String lang) {
		this.lang = lang;
	}

	public String getiP() {
		return iP;
	}

	public void setiP(String iP) {
		this.iP = iP;
	}

	public String getChannel() {
		return channel;
	}

	public void setChannel(String channel) {
		this.channel = channel;
	}

	public String getMsisdn() {
		return msisdn;
	}

	public void setMsisdn(String msisdn) {
		this.msisdn = msisdn;
	}

	@Override
	public String toString() {
		return "CustomerInfoRequest [lang=" + lang + ", iP=" + iP + ", channel=" + channel + ", msisdn=" + msisdn + "]";
	}

}
