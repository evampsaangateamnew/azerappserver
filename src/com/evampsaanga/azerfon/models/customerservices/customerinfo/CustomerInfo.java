
package com.evampsaanga.azerfon.models.customerservices.customerinfo;

import java.util.ArrayList;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * @author Evamp & Saanga
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class CustomerInfo {

	private String title;
	private String firstName;
	private String middleName;
	private String lastName;
	private String customerType;
	private String gender;
	private String dob;
	private String accountId;
	private String effectiveDate;
	private String expiryDate;
	private String subscriberType;
	private String status;
	private String statusDetails;
	private String brandId;
	private String brandName;
	private String loyaltySegment;
	private String offeringId;
	private String msisdn;
	private String OfferingName;
	private String OfferingNameDisplay;
	private String SIM;
	private String PIN;
	private String PUK;
	private String EMAIL;
	private String token;
	private String imageURL;
	private String billingLanguage;
	private String language;
	private String customerId;
	private String entityId;
	private String customerStatus;
	private String magCustomerId;
	private String groupType;
	
	private String rateus_android;
	private String rateus_ios;
	private String groupIds;
	private String firstPopup;
	private String lateOnPopup;
	private String popupTitle;
	private String popupContent;
	
    public String getGroupIds() {
		return groupIds;
	}
	public void setGroupIds(String groupIds) {
		this.groupIds = groupIds;
	}
	public ArrayList<String> getHideNumberTariffIds() {
		return hideNumberTariffIds;
	}
	public ProcessSpecialcustomerData getSpecialOffersTariffData() {
		return specialOffersTariffData;
	}

	public void setSpecialOffersTariffData(ProcessSpecialcustomerData specialOffersTariffData) {
		this.specialOffersTariffData = specialOffersTariffData;
	}

	private ProcessSpecialcustomerData specialOffersTariffData=new ProcessSpecialcustomerData();
	
	public void setHideNumberTariffIds(ArrayList<String> hideNumberTariffIds) {
		this.hideNumberTariffIds = hideNumberTariffIds;
	}

	private ArrayList<String> hideNumberTariffIds=null;
	public String getFirstPopup() {
		return firstPopup;
	}

	public void setFirstPopup(String firstPopup) {
		this.firstPopup = firstPopup;
	}

	public String getLateOnPopup() {
		return lateOnPopup;
	}

	public void setLateOnPopup(String lateOnPopup) {
		this.lateOnPopup = lateOnPopup;
	}

	public String getPopupTitle() {
		return popupTitle;
	}

	public void setPopupTitle(String popupTitle) {
		this.popupTitle = popupTitle;
	}

	public String getPopupContent() {
		return popupContent;
	}

	public void setPopupContent(String popupContent) {
		this.popupContent = popupContent;
	}
	
    public String getRateus_android() {
		return rateus_android;
	}

	public void setRateus_android(String rateus_android) {
		this.rateus_android = rateus_android;
	}

	public String getRateus_ios() {
		return rateus_ios;
	}

	public void setRateus_ios(String rateus_ios) {
		this.rateus_ios = rateus_ios;
	}

	
	
	/**
	 * @param title
	 * @param firstName
	 * @param middleName
	 * @param lastName
	 * @param customerType
	 * @param gender
	 * @param dob
	 * @param accountId
	 * @param language
	 * @param effectiveDate
	 * @param expiryDate
	 * @param subscriberType
	 * @param status
	 * @param statusDetails
	 * @param brandId
	 * @param brandName
	 * @param loyaltySegment
	 * @param offeringId
	 * @param msisdn
	 * @param offeringName
	 * @param sIM
	 * @param pIN
	 * @param pUK
	 * @param eMAIL
	 * @param token
	 * @param imageURL
	 * @param billingLanguage
	 * @param customerId
	 */
	public CustomerInfo() {
		this.title = "";
		this.firstName = "";
		this.middleName = "";
		this.lastName = "";
		this.customerType = "";
		this.gender = "";
		this.dob = "";
		this.accountId = "";
		this.effectiveDate = "";
		this.expiryDate = "";
		this.subscriberType = "";
		this.status = "";
		this.statusDetails = "";
		this.brandId = "";
		this.brandName = "";
		this.loyaltySegment = "";
		this.offeringId = "";
		this.msisdn = "";
		this.OfferingName = "";
		this.OfferingNameDisplay = "";
		this.SIM = "";
		this.PIN = "";
		this.PUK = "";
		this.EMAIL = "";
		this.token = "";
		this.imageURL = "";
		this.billingLanguage = "";
		this.language = "";
		this.customerId = "";
		this.entityId = "";
		this.customerStatus = "";
		this.magCustomerId = "";
		this.groupType="";
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getCustomerType() {
		return customerType;
	}

	public void setCustomerType(String customerType) {
		this.customerType = customerType;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getDob() {
		return dob;
	}

	public void setDob(String dob) {
		this.dob = dob;
	}

	public String getAccountId() {
		return accountId;
	}

	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}

	public String getEffectiveDate() {
		return effectiveDate;
	}

	public void setEffectiveDate(String effectiveDate) {
		this.effectiveDate = effectiveDate;
	}

	public String getExpiryDate() {
		return expiryDate;
	}

	public void setExpiryDate(String expiryDate) {
		this.expiryDate = expiryDate;
	}

	public String getSubscriberType() {
		return subscriberType;
	}

	public void setSubscriberType(String subscriberType) {
		this.subscriberType = subscriberType;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getStatusDetails() {
		return statusDetails;
	}

	public void setStatusDetails(String statusDetails) {
		this.statusDetails = statusDetails;
	}

	public String getBrandId() {
		return brandId;
	}

	public void setBrandId(String brandId) {
		this.brandId = brandId;
	}

	public String getBrandName() {
		return brandName;
	}

	public void setBrandName(String brandName) {
		this.brandName = brandName;
	}

	public String getLoyaltySegment() {
		return loyaltySegment;
	}

	public void setLoyaltySegment(String loyaltySegment) {
		this.loyaltySegment = loyaltySegment;
	}

	public String getOfferingId() {
		return offeringId;
	}

	public void setOfferingId(String offeringId) {
		this.offeringId = offeringId;
	}

	public String getMsisdn() {
		return msisdn;
	}

	public void setMsisdn(String msisdn) {
		this.msisdn = msisdn;
	}

	public String getOfferingName() {
		return OfferingName;
	}

	public void setOfferingName(String offeringName) {
		OfferingName = offeringName;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getSIM() {
		return SIM;
	}

	public void setSIM(String sIM) {
		SIM = sIM;
	}

	public String getPIN() {
		return PIN;
	}

	public void setPIN(String pIN) {
		PIN = pIN;
	}

	public String getPUK() {
		return PUK;
	}

	public void setPUK(String pUK) {
		PUK = pUK;
	}

	public String getEMAIL() {
		return EMAIL;
	}

	public void setEMAIL(String eMAIL) {
		EMAIL = eMAIL;
	}

	public String getImageURL() {
		return imageURL;
	}

	public void setImageURL(String imageURL) {
		this.imageURL = imageURL;
	}

	public String getBillingLanguage() {
		return billingLanguage;
	}

	public void setBillingLanguage(String billingLanguage) {
		this.billingLanguage = billingLanguage;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public String getEntityId() {
		return entityId;
	}

	public void setEntityId(String entityId) {
		this.entityId = entityId;
	}

	public String getCustomerStatus() {
		return customerStatus;
	}

	public void setCustomerStatus(String customerStatus) {
		this.customerStatus = customerStatus;
	}

	public String getOfferingNameDisplay() {
		return OfferingNameDisplay;
	}

	public void setOfferingNameDisplay(String offeringNameDisplay) {
		OfferingNameDisplay = offeringNameDisplay;
	}

	@Override
	public String toString() {
		return "CustomerInfo [title=" + title + ", firstName=" + firstName + ", middleName=" + middleName
				+ ", lastName=" + lastName + ", customerType=" + customerType + ", gender=" + gender + ", dob=" + dob
				+ ", accountId=" + accountId + ", effectiveDate=" + effectiveDate + ", expiryDate=" + expiryDate
				+ ", subscriberType=" + subscriberType + ", status=" + status + ", statusDetails=" + statusDetails
				+ ", brandId=" + brandId + ", brandName=" + brandName + ", loyaltySegment=" + loyaltySegment
				+ ", offeringId=" + offeringId + ", msisdn=" + msisdn + ", OfferingName=" + OfferingName
				+ ", OfferingNameDisplay=" + OfferingNameDisplay + ", SIM=" + SIM + ", PIN=" + PIN + ", PUK=" + PUK
				+ ", EMAIL=" + EMAIL + ", token=" + token + ", imageURL=" + imageURL + ", billingLanguage="
				+ billingLanguage + ", language=" + language + ", customerId=" + customerId + ", entityId=" + entityId
				+ ", customerStatus=" + customerStatus + ",groupType=" + groupType + "]";
	}

	public String getMagCustomerId() {
		return magCustomerId;
	}

	public void setMagCustomerId(String magCustomerId) {
		this.magCustomerId = magCustomerId;
	}

	public String getGroupType() {
		return groupType;
	}

	public void setGroupType(String groupType) {
		this.groupType = groupType;
	}

	
	
}
