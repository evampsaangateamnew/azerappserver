/**
 * 
 */
package com.evampsaanga.azerfon.models.customerservices.changepassword;

/**
 * @author Evamp & Saanga
 *
 */
public class ChangePasswordResponseData {
    private String message;

    public String getMessage() {
	return message;
    }

    public void setMessage(String message) {
	this.message = message;
    }

}
