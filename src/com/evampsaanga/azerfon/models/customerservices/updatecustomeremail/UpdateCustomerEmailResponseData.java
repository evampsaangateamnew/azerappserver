/**
 * 
 */
package com.evampsaanga.azerfon.models.customerservices.updatecustomeremail;

/**
 * @author Evamp & Saanga
 *
 */
public class UpdateCustomerEmailResponseData {
	private String message;

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

}
