/**
 * 
 */
package com.evampsaanga.azerfon.models.customerservices.updatecustomeremail;

import com.evampsaanga.azerfon.common.utilities.BaseResponse;

/**
 * @author Evamp & Saanga
 *
 */
public class UpdateCustomerEmailResponse extends BaseResponse {
	UpdateCustomerEmailResponseData data;

	public UpdateCustomerEmailResponseData getData() {
		return data;
	}

	public void setData(UpdateCustomerEmailResponseData data) {
		this.data = data;
	}
}
