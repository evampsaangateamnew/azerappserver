/**
 * 
 */
package com.evampsaanga.azerfon.models.customerservices.resendpin;

import com.evampsaanga.azerfon.common.utilities.BaseRequest;

/**
 * @author Evamp & Saanga
 *
 */
public class ResendPINRequest extends BaseRequest {

	private String cause;

	public String getCause() {
		return cause;
	}

	public void setCause(String cause) {
		this.cause = cause;
	}

	@Override
	public String toString() {
		return "ResendPINRequest [cause=" + cause + ", getLang()=" + getLang() + ", getiP()=" + getiP()
				+ ", getChannel()=" + getChannel() + ", getMsisdn()=" + getMsisdn() + "]";
	}

}
