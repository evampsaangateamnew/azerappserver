package com.evampsaanga.azerfon.models.customerservices.resendpin;

import com.evampsaanga.azerfon.common.utilities.BaseRequest;

public class ResendPinRequestV2 extends BaseRequest{

	private String cause;
	private String userName;

	public String getCause() {
		return cause;
	}

	public void setCause(String cause) {
		this.cause = cause;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

}
