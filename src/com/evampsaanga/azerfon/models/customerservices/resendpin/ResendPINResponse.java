/**
 * 
 */
package com.evampsaanga.azerfon.models.customerservices.resendpin;

import com.evampsaanga.azerfon.common.utilities.BaseResponse;

/**
 * @author Evamp & Saanga
 *
 */
public class ResendPINResponse extends BaseResponse {
	private ResendPINResponseData data;

	public ResendPINResponseData getData() {
		return data;
	}

	public void setData(ResendPINResponseData data) {
		this.data = data;
	}

}
