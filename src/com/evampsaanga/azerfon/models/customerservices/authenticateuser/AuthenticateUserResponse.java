/**
 * 
 */
package com.evampsaanga.azerfon.models.customerservices.authenticateuser;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.evampsaanga.azerfon.common.utilities.BaseResponse;

/**
 * @author Evamp & Saanga
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class AuthenticateUserResponse extends BaseResponse {

	private AuthenticateUserResponseData data;

	public AuthenticateUserResponseData getData() {
		return data;
	}

	public void setData(AuthenticateUserResponseData data) {
		this.data = data;
	}
}
