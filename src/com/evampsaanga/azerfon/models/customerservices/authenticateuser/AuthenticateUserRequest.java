/**
 * 
 */
package com.evampsaanga.azerfon.models.customerservices.authenticateuser;

import com.evampsaanga.azerfon.common.utilities.BaseRequest;

/**
 * @author Evamp & Saanga
 *
 */
public class AuthenticateUserRequest extends BaseRequest {

	private String password;

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Override
	public String toString() {
		return "AuthenticateUserRequest [password=" + password + ", getLang()=" + getLang() + ", getiP()=" + getiP()
				+ ", getChannel()=" + getChannel() + ", getMsisdn()=" + getMsisdn() + "]";
	}

}
