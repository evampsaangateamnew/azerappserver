/**
 * 
 */
package com.evampsaanga.azerfon.models.customerservices.signup;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * @author Evamp & Saanga
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class SignupResponseData {

	private String pin;

	public String getPin() {
		return pin;
	}

	public void setPin(String pin) {
		this.pin = pin;
	}

}
