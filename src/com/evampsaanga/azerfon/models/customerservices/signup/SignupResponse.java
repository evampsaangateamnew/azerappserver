/**
 * 
 */
package com.evampsaanga.azerfon.models.customerservices.signup;

import com.evampsaanga.azerfon.common.utilities.BaseResponse;

/**
 * @author Evamp & Saanga
 *
 */
public class SignupResponse extends BaseResponse {
	private SignupResponseData data;

	public SignupResponseData getData() {
		return data;
	}

	public void setData(SignupResponseData data) {
		this.data = data;
	}

}
