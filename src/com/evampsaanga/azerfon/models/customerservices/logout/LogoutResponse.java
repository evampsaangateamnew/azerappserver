/**
 * 
 */
package com.evampsaanga.azerfon.models.customerservices.logout;

import com.evampsaanga.azerfon.common.utilities.BaseResponse;

/**
 * @author Evamp & Saanga
 *
 */
public class LogoutResponse extends BaseResponse {

	@Override
	public String toString() {
		return "LogoutResponse [getCallStatus()=" + getCallStatus() + ", getResultCode()=" + getResultCode()
				+ ", getResultDesc()=" + getResultDesc() + ", getLogsReport()=" + getLogsReport() + "]";
	}

}
