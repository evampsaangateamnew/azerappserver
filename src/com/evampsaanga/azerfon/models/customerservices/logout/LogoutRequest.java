/**
 * 
 */
package com.evampsaanga.azerfon.models.customerservices.logout;

import com.evampsaanga.azerfon.common.utilities.BaseRequest;

/**
 * @author Evamp & Saanga
 *
 */
public class LogoutRequest extends BaseRequest {

	@Override
	public String toString() {
		return "LogoutRequest [getLang()=" + getLang() + ", getiP()=" + getiP() + ", getChannel()=" + getChannel()
				+ ", getMsisdn()=" + getMsisdn() + "]";
	}

}
