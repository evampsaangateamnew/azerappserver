package com.evampsaanga.azerfon.models.customerservices.email;

public class CustomerEmailResponseData {

	private String message;

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	
}
