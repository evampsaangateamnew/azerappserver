package com.evampsaanga.azerfon.models.customerservices.email;

public class CustomerEmailResponse {
	
	private String callStatus;
	private String resultCode;
	private String resultDesc;
	private String exception;
	private CustomerEmailResponseData data;
	
	public String getCallStatus() {
		return callStatus;
	}
	public void setCallStatus(String callStatus) {
		this.callStatus = callStatus;
	}
	public String getResultCode() {
		return resultCode;
	}
	public void setResultCode(String resultCode) {
		this.resultCode = resultCode;
	}
	public String getResultDesc() {
		return resultDesc;
	}
	public void setResultDesc(String resultDesc) {
		this.resultDesc = resultDesc;
	}
	public String getException() {
		return exception;
	}
	public void setException(String exception) {
		this.exception = exception;
	}
	public CustomerEmailResponseData getData() {
		return data;
	}
	public void setData(CustomerEmailResponseData data) {
		this.data = data;
	}
	
	

}
