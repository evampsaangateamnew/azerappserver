package com.evampsaanga.azerfon.models.customerservices.email;

public class CustomerEmailRequest {

	private String msisdn;
	private String iP;
	private String lang;
	private String channel;
	private String emailId;
	
	public String getMsisdn() {
		return msisdn;
	}
	public void setMsisdn(String msisdn) {
		this.msisdn = msisdn;
	}
	public String getiP() {
		return iP;
	}
	public void setiP(String iP) {
		this.iP = iP;
	}
	public String getLang() {
		return lang;
	}
	public void setLang(String lang) {
		this.lang = lang;
	}
	public String getChannel() {
		return channel;
	}
	public void setChannel(String channel) {
		this.channel = channel;
	}
	public String getEmailId() {
		return emailId;
	}
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	@Override
	public String toString() {
		return "CustomerEmailRequest [msisdn=" + msisdn + ", iP=" + iP + ", lang=" + lang + ", channel=" + channel
				+ ", emailId=" + emailId + "]";
	}
	
	
	
	
}
