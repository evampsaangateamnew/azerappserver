/**
 * 
 */
package com.evampsaanga.azerfon.models.customerservices.sendpinV2;

import com.evampsaanga.azerfon.common.utilities.BaseResponse;

/**
 * @author Evamp & Saanga
 *
 */
public class SendPINResponse extends BaseResponse {
	private SendPINResponseData data;

	public SendPINResponseData getData() {
		return data;
	}

	public void setData(SendPINResponseData data) {
		this.data = data;
	}

}
