/**
 * 
 */
package com.evampsaanga.azerfon.models.customerservices.forgotpassword;

import com.evampsaanga.azerfon.common.utilities.BaseResponse;

/**
 * @author Aqeel Abbas
 *
 */
public class ForgotPasswordResponseV2 extends BaseResponse{
	private ForgotPasswordResponseDataV2 data;

	public ForgotPasswordResponseDataV2 getData() {
		return data;
	}

	public void setData(ForgotPasswordResponseDataV2 data) {
		this.data = data;
	}

}
