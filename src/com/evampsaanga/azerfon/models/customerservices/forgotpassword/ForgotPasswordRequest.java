/**
 * 
 */
package com.evampsaanga.azerfon.models.customerservices.forgotpassword;

import com.evampsaanga.azerfon.common.utilities.BaseRequest;

/**
 * @author Evamp & Saanga
 *
 */
public class ForgotPasswordRequest extends BaseRequest {

	private String password;
	private String confirmPassword;
	private String temp; 

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getConfirmPassword() {
		return confirmPassword;
	}

	public void setConfirmPassword(String confirmPassword) {
		this.confirmPassword = confirmPassword;
	}

	@Override
	public String toString() {
		return "ForgotPasswordRequest [password=" + password + ", confirmPassword=" + confirmPassword + ", getLang()="
				+ getLang() + ", getiP()=" + getiP() + ", getChannel()=" + getChannel() + ", getMsisdn()=" + getMsisdn()
				+ "]";
	}

	public String getTemp() {
		return temp;
	}

	public void setTemp(String temp) {
		this.temp = temp;
	}

}
