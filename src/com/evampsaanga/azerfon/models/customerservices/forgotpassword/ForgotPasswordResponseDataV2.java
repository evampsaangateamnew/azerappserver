/**
 * 
 */
package com.evampsaanga.azerfon.models.customerservices.forgotpassword;

import com.evampsaanga.azerfon.models.dashboardservice.LoginData;
import com.evampsaanga.azerfon.models.generalservices.predefineddata.PredefinedDataResponseDataV2;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * @author Aqeel Abbas
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ForgotPasswordResponseDataV2 {

	LoginData loginData;

	PredefinedDataResponseDataV2 predefinedData;

	public PredefinedDataResponseDataV2 getPredefinedData() {
		return predefinedData;
	}

	public void setPredefinedData(PredefinedDataResponseDataV2 predefinedData) {
		this.predefinedData = predefinedData;
	}

	public LoginData getLoginData() {
		return loginData;
	}

	public void setLoginData(LoginData loginData) {
		this.loginData = loginData;
	}

}
