/**
 * 
 */
package com.evampsaanga.azerfon.models.customerservices.verifyotp;

import com.evampsaanga.azerfon.common.utilities.BaseResponse;

/**
 * @author Evamp & Saanga
 *
 */
public class VerifyOTPResponse extends BaseResponse {
	private VerifyOTPResponseData data;

	public VerifyOTPResponseData getData() {
		return data;
	}

	public void setData(VerifyOTPResponseData data) {
		this.data = data;
	}

}
