/**
 * 
 */
package com.evampsaanga.azerfon.models.customerservices.verifyotp;

import com.evampsaanga.azerfon.common.utilities.BaseRequest;

/**
 * @author Evamp & Saanga
 *
 */
public class VerifyOTPRequest extends BaseRequest {
	private String cause;
	private String pin;

	public String getCause() {
		return cause;
	}

	public void setCause(String cause) {
		this.cause = cause;
	}

	public String getPin() {
		return pin;
	}

	public void setPin(String pin) {
		this.pin = pin;
	}

	@Override
	public String toString() {
		return "VerifyOTPRequest [cause=" + cause + ", pin=" + pin + ", getLang()=" + getLang() + ", getiP()=" + getiP()
				+ ", getChannel()=" + getChannel() + ", getMsisdn()=" + getMsisdn() + "]";
	}

}
