/**
 * 
 */
package com.evampsaanga.azerfon.models.customerservices.verifyotp;

/**
 * @author Evamp & Saanga
 *
 */
public class VerifyOTPResponseData {
	private String message;
	private String content;
	
	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

}
