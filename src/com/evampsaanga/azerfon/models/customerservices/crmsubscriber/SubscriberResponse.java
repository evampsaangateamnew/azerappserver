
package com.evampsaanga.azerfon.models.customerservices.crmsubscriber;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "ResponseHeader",
    "GetSubscriberBody"
})
public class SubscriberResponse {

    @JsonProperty("ResponseHeader")
    private ResponseHeader responseHeader;
    @JsonProperty("GetSubscriberBody")
    private GetSubscriberBody getSubscriberBody;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("ResponseHeader")
    public ResponseHeader getResponseHeader() {
        return responseHeader;
    }

    @JsonProperty("ResponseHeader")
    public void setResponseHeader(ResponseHeader responseHeader) {
        this.responseHeader = responseHeader;
    }

    @JsonProperty("GetSubscriberBody")
    public GetSubscriberBody getGetSubscriberBody() {
        return getSubscriberBody;
    }

    @JsonProperty("GetSubscriberBody")
    public void setGetSubscriberBody(GetSubscriberBody getSubscriberBody) {
        this.getSubscriberBody = getSubscriberBody;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
