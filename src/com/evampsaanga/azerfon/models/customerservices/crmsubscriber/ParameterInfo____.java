
package com.evampsaanga.azerfon.models.customerservices.crmsubscriber;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "ParamName",
    "ParamValue"
})
public class ParameterInfo____ {

    @JsonProperty("ParamName")
    private String paramName;
    @JsonProperty("ParamValue")
    private String paramValue;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("ParamName")
    public String getParamName() {
        return paramName;
    }

    @JsonProperty("ParamName")
    public void setParamName(String paramName) {
        this.paramName = paramName;
    }

    @JsonProperty("ParamValue")
    public String getParamValue() {
        return paramValue;
    }

    @JsonProperty("ParamValue")
    public void setParamValue(String paramValue) {
        this.paramValue = paramValue;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
