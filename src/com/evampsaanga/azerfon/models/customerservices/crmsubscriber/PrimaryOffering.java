
package com.evampsaanga.azerfon.models.customerservices.crmsubscriber;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "OfferingId",
    "Status",
    "EffectiveTime",
    "ExpiredTime",
    "ProductList",
    "ExtParamList",
    "GroupMemberFlag"
})
public class PrimaryOffering {

    @JsonProperty("OfferingId")
    private OfferingId offeringId;
    @JsonProperty("Status")
    private String status;
    @JsonProperty("EffectiveTime")
    private String effectiveTime;
    @JsonProperty("ExpiredTime")
    private String expiredTime;
    @JsonProperty("ProductList")
    private ProductList productList;
    @JsonProperty("ExtParamList")
    private ExtParamList_ extParamList;
    @JsonProperty("GroupMemberFlag")
    private String groupMemberFlag;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("OfferingId")
    public OfferingId getOfferingId() {
        return offeringId;
    }

    @JsonProperty("OfferingId")
    public void setOfferingId(OfferingId offeringId) {
        this.offeringId = offeringId;
    }

    @JsonProperty("Status")
    public String getStatus() {
        return status;
    }

    @JsonProperty("Status")
    public void setStatus(String status) {
        this.status = status;
    }

    @JsonProperty("EffectiveTime")
    public String getEffectiveTime() {
        return effectiveTime;
    }

    @JsonProperty("EffectiveTime")
    public void setEffectiveTime(String effectiveTime) {
        this.effectiveTime = effectiveTime;
    }

    @JsonProperty("ExpiredTime")
    public String getExpiredTime() {
        return expiredTime;
    }

    @JsonProperty("ExpiredTime")
    public void setExpiredTime(String expiredTime) {
        this.expiredTime = expiredTime;
    }

    @JsonProperty("ProductList")
    public ProductList getProductList() {
        return productList;
    }

    @JsonProperty("ProductList")
    public void setProductList(ProductList productList) {
        this.productList = productList;
    }

    @JsonProperty("ExtParamList")
    public ExtParamList_ getExtParamList() {
        return extParamList;
    }

    @JsonProperty("ExtParamList")
    public void setExtParamList(ExtParamList_ extParamList) {
        this.extParamList = extParamList;
    }

    @JsonProperty("GroupMemberFlag")
    public String getGroupMemberFlag() {
        return groupMemberFlag;
    }

    @JsonProperty("GroupMemberFlag")
    public void setGroupMemberFlag(String groupMemberFlag) {
        this.groupMemberFlag = groupMemberFlag;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
