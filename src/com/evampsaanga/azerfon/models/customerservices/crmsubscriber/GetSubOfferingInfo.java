
package com.evampsaanga.azerfon.models.customerservices.crmsubscriber;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "OfferingId",
    "Status",
    "EffectiveTime",
    "ExpiredTime",
    "ExtParamList",
    "GroupMemberFlag",
    "ProductList"
})
public class GetSubOfferingInfo {

    @JsonProperty("OfferingId")
    private OfferingId_ offeringId;
    @JsonProperty("Status")
    private String status;
    @JsonProperty("EffectiveTime")
    private String effectiveTime;
    @JsonProperty("ExpiredTime")
    private String expiredTime;
    @JsonProperty("ExtParamList")
    private ExtParamList__ extParamList;
    @JsonProperty("GroupMemberFlag")
    private String groupMemberFlag;
    @JsonProperty("ProductList")
    private ProductList_ productList;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("OfferingId")
    public OfferingId_ getOfferingId() {
        return offeringId;
    }

    @JsonProperty("OfferingId")
    public void setOfferingId(OfferingId_ offeringId) {
        this.offeringId = offeringId;
    }

    @JsonProperty("Status")
    public String getStatus() {
        return status;
    }

    @JsonProperty("Status")
    public void setStatus(String status) {
        this.status = status;
    }

    @JsonProperty("EffectiveTime")
    public String getEffectiveTime() {
        return effectiveTime;
    }

    @JsonProperty("EffectiveTime")
    public void setEffectiveTime(String effectiveTime) {
        this.effectiveTime = effectiveTime;
    }

    @JsonProperty("ExpiredTime")
    public String getExpiredTime() {
        return expiredTime;
    }

    @JsonProperty("ExpiredTime")
    public void setExpiredTime(String expiredTime) {
        this.expiredTime = expiredTime;
    }

    @JsonProperty("ExtParamList")
    public ExtParamList__ getExtParamList() {
        return extParamList;
    }

    @JsonProperty("ExtParamList")
    public void setExtParamList(ExtParamList__ extParamList) {
        this.extParamList = extParamList;
    }

    @JsonProperty("GroupMemberFlag")
    public String getGroupMemberFlag() {
        return groupMemberFlag;
    }

    @JsonProperty("GroupMemberFlag")
    public void setGroupMemberFlag(String groupMemberFlag) {
        this.groupMemberFlag = groupMemberFlag;
    }

    @JsonProperty("ProductList")
    public ProductList_ getProductList() {
        return productList;
    }

    @JsonProperty("ProductList")
    public void setProductList(ProductList_ productList) {
        this.productList = productList;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
