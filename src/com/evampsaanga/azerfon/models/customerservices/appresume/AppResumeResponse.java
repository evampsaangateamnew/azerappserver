/**
 * 
 */
package com.evampsaanga.azerfon.models.customerservices.appresume;

import com.evampsaanga.azerfon.common.utilities.BaseResponse;

/**
 * @author Evamp & Saanga
 *
 */
public class AppResumeResponse extends BaseResponse {
	private AppResumeResponseData data;

	public AppResumeResponseData getData() {
		return data;
	}

	public void setData(AppResumeResponseData data) {
		this.data = data;
	}

}
