/**
 * 
 */
package com.evampsaanga.azerfon.models.customerservices.appresume;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.evampsaanga.azerfon.common.utilities.BaseRequest;

/**
 * @author Evamp & Saanga
 *
 */


public class AppResumeRequest extends BaseRequest {

	private String customerId="";
	 private String entityId="";
	
	 public String getCustomerId() {
		return customerId;
	}
	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}
	public String getEntityId() {
		return entityId;
	}
	public void setEntityId(String entityId) {
		this.entityId = entityId;
	}
	
	@Override
	public String toString() {
		return "AppResume [getLang()=" + getLang() + ", getiP()=" + getiP() + ", getChannel()=" + getChannel()
				+ ", getMsisdn()=" + getMsisdn() + ",getentityId()=" + getEntityId() + ",getCustomerId()=" + getCustomerId() + "]";
	}

}
