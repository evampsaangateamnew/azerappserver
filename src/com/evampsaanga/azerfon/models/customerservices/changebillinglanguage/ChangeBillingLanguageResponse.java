/**
 * 
 */
package com.evampsaanga.azerfon.models.customerservices.changebillinglanguage;

import com.evampsaanga.azerfon.common.utilities.BaseResponse;

/**
 * @author Evamp & Saanga
 *
 */
public class ChangeBillingLanguageResponse extends BaseResponse {
	ChangeBillingLanguageResponseData data;

	public ChangeBillingLanguageResponseData getData() {
		return data;
	}

	public void setData(ChangeBillingLanguageResponseData data) {
		this.data = data;
	}

}
