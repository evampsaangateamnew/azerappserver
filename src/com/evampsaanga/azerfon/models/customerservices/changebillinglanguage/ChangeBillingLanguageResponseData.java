/**
 * 
 */
package com.evampsaanga.azerfon.models.customerservices.changebillinglanguage;

/**
 * @author Evamp & Saanga
 *
 */
public class ChangeBillingLanguageResponseData {
	private String message;

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

}
