/**
 * 
 */
package com.evampsaanga.azerfon.models.customerservices.savecustomer;

import com.evampsaanga.azerfon.common.utilities.BaseResponse;

/**
 * @author Evamp & Saanga
 *
 */
public class SaveCustomerResponse extends BaseResponse {
	private SaveCustomerResponseData data;

	public SaveCustomerResponseData getData() {
		return data;
	}

	public void setData(SaveCustomerResponseData data) {
		this.data = data;
	}

}
