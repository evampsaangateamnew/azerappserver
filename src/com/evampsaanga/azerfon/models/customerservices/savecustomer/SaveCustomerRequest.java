/**
 * 
 */
package com.evampsaanga.azerfon.models.customerservices.savecustomer;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.evampsaanga.azerfon.common.utilities.BaseRequest;
import com.evampsaanga.azerfon.models.customerservices.customerinfo.CustomerInfo;

/**
 * @author Evamp & Saanga
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class SaveCustomerRequest extends BaseRequest {

	private String password;
	private String confirm_password;
	private String terms_and_conditions;
	private String temp; //Value of PIN to ensure same user executing 3rd step
	private CustomerInfo customerData;

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getConfirm_password() {
		return confirm_password;
	}

	public void setConfirm_password(String confirm_password) {
		this.confirm_password = confirm_password;
	}

	public String getTerms_and_conditions() {
		return terms_and_conditions;
	}

	public void setTerms_and_conditions(String terms_and_conditions) {
		this.terms_and_conditions = terms_and_conditions;
	}

	public CustomerInfo getCustomerData() {
		return customerData;
	}

	public void setCustomerData(CustomerInfo customerData) {
		this.customerData = customerData;
	}

	@Override
	public String toString() {
		return "SaveCustomerRequest [password=" + password + ", confirm_password=" + confirm_password
				+ ", terms_and_conditions=" + terms_and_conditions + ", customerData=" + customerData + ", getLang()="
				+ getLang() + ", getiP()=" + getiP() + ", getChannel()=" + getChannel() + ", getMsisdn()=" + getMsisdn()
				+ "]";
	}

	public String getTemp() {
		return temp;
	}

	public void setTemp(String temp) {
		this.temp = temp;
	}

}
