/**
 * 
 */
package com.evampsaanga.azerfon.models.customerservices.savecustomer;

import java.util.List;

import com.evampsaanga.azerfon.models.customerservices.authenticateuser.PrimaryOfferings;
import com.evampsaanga.azerfon.models.customerservices.authenticateuser.SupplementaryOfferings;
import com.evampsaanga.azerfon.models.customerservices.customerinfo.CustomerInfo;
import com.evampsaanga.azerfon.models.generalservices.predefineddata.PredefinedDataResponse;

/**
 * @author Evamp & Saanga
 *
 */
public class SaveCustomerResponseData {
	private CustomerInfo customerData;
	private List<SupplementaryOfferings> supplementaryOfferingList;
	private PrimaryOfferings primaryOffering;
	private PredefinedDataResponse predefinedData;
	private String promoMessage;

	public CustomerInfo getCustomerData() {
		return customerData;
	}

	public void setCustomerData(CustomerInfo customerData) {
		this.customerData = customerData;
	}

	public List<SupplementaryOfferings> getSupplementaryOfferingList() {
		return supplementaryOfferingList;
	}

	public void setSupplementaryOfferingList(List<SupplementaryOfferings> supplementaryOfferingList) {
		this.supplementaryOfferingList = supplementaryOfferingList;
	}

	public PrimaryOfferings getPrimaryOffering() {
		return primaryOffering;
	}

	public void setPrimaryOffering(PrimaryOfferings primaryOffering) {
		this.primaryOffering = primaryOffering;
	}

	public PredefinedDataResponse getPredefinedData() {
		return predefinedData;
	}

	public void setPredefinedData(PredefinedDataResponse predefinedData) {
		this.predefinedData = predefinedData;
	}

	public String getPromoMessage() {
		return promoMessage;
	}

	public void setPromoMessage(String promoMessage) {
		this.promoMessage = promoMessage;
	}

}
