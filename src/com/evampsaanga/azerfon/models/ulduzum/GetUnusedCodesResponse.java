package com.evampsaanga.azerfon.models.ulduzum;

import com.evampsaanga.azerfon.common.utilities.BaseResponse;

public class GetUnusedCodesResponse extends BaseResponse {
	
	public GetUnusedCodesDataAppServer getData() {
		return data;
	}

	public void setData(GetUnusedCodesDataAppServer data) {
		this.data = data;
	}

	private GetUnusedCodesDataAppServer data=new GetUnusedCodesDataAppServer();
	

}
