package com.evampsaanga.azerfon.models.ulduzum;

import java.util.List;

import com.evampsaanga.azerfon.common.utilities.BaseResponse;

public class UlduzumGetCategoriesResponse extends BaseResponse {

	
	private List<DataGetCategories> data =null;

	public List<DataGetCategories> getData() {
		return data;
	}

	public void setData(List<DataGetCategories> data) {
		this.data = data;
	}

}
