package com.evampsaanga.azerfon.models.ulduzum;

import java.util.List;

import com.evampsaanga.azerfon.common.utilities.BaseResponse;

public class GetUsageHistoryResponse extends BaseResponse {
	
	
	private GetUsageHistoryDataAppServer data=new GetUsageHistoryDataAppServer();

	public GetUsageHistoryDataAppServer getData() {
		return data;
	}

	public void setData(GetUsageHistoryDataAppServer data) {
		this.data = data;
	}
	

}
