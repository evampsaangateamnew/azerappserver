package com.evampsaanga.azerfon.models.ulduzum;

import java.util.List;

public class GetMerchantsDataAppServer {
	
	List<GetMerchantsData> maindatalist=null;
	List<CategoryNamelist> categoryList=null;
	
	public List<GetMerchantsData> getMaindatalist() {
		return maindatalist;
	}
	public void setMaindatalist(List<GetMerchantsData> maindatalist) {
		this.maindatalist = maindatalist;
	}
	public List<CategoryNamelist> getCategoryList() {
		return categoryList;
	}
	public void setCategoryList(List<CategoryNamelist> categoryList) {
		this.categoryList = categoryList;
	}

	

}
