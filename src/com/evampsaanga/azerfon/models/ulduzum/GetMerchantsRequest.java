package com.evampsaanga.azerfon.models.ulduzum;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.evampsaanga.azerfon.common.utilities.BaseRequest;

public class GetMerchantsRequest extends BaseRequest{
	
	
	public String getLoyaltySegment() {
		return loyaltySegment;
	}

	public void setLoyaltySegment(String loyaltySegment) {
		this.loyaltySegment = loyaltySegment;
	}

	private String loyaltySegment="";

}
