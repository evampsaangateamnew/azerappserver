
package com.evampsaanga.azerfon.models.ulduzum;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "data"
})
public class GetCategoriesDataList {


    @JsonProperty("data")
    private List<DataGetCategories> categories = null;

	public List<DataGetCategories> getCategories() {
		return categories;
	}

	public void setCategories(List<DataGetCategories> categories) {
		this.categories = categories;
	}
  

 


}
