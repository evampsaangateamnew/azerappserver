package com.evampsaanga.azerfon.models.ulduzum;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.evampsaanga.azerfon.common.utilities.BaseRequest;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class UlduzumRequest extends BaseRequest {
	
	
}
