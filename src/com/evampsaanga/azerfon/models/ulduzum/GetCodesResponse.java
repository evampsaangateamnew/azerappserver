package com.evampsaanga.azerfon.models.ulduzum;

import com.evampsaanga.azerfon.common.utilities.BaseResponse;

public class GetCodesResponse extends BaseResponse {
	
	private GetCodesData data=null;

	public GetCodesData getData() {
		return data;
	}

	public void setData(GetCodesData data) {
		this.data = data;
	}
	

}
