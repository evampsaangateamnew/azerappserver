package com.evampsaanga.azerfon.models.getreports;

public class ReportData {
	private String lastModified;
	private String fileName;
	private String filePath;

	public String getLastModified() {
		return lastModified;
	}

	public void setLastModified(String lastModified) {
		this.lastModified = lastModified;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getFilePath() {
		return filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	@Override
	public String toString() {
		return "ReportData [lastModified=" + lastModified + ", fileName=" + fileName + ", filePath=" + filePath + "]";
	}

}