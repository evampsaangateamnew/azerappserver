/**
 * 
 */
package com.evampsaanga.azerfon.models.usagehistory.getsummary;

/**
 * @author Evamp & Saanga
 *
 */
public class UsageSummaryResponseData {

	private SummaryRecordDetails voiceRecords;
	private SummaryRecordDetails smsRecords;
	private SummaryRecordDetails dataRecords;
	private SummaryRecordDetails otherRecords;

	public SummaryRecordDetails getVoiceRecords() {
		return voiceRecords;
	}

	public void setVoiceRecords(SummaryRecordDetails voiceRecords) {
		this.voiceRecords = voiceRecords;
	}

	public SummaryRecordDetails getSmsRecords() {
		return smsRecords;
	}

	public void setSmsRecords(SummaryRecordDetails smsRecords) {
		this.smsRecords = smsRecords;
	}

	public SummaryRecordDetails getDataRecords() {
		return dataRecords;
	}

	public void setDataRecords(SummaryRecordDetails dataRecords) {
		this.dataRecords = dataRecords;
	}

	public SummaryRecordDetails getOtherRecords() {
		return otherRecords;
	}

	public void setOtherRecords(SummaryRecordDetails otherRecords) {
		this.otherRecords = otherRecords;
	}

}
