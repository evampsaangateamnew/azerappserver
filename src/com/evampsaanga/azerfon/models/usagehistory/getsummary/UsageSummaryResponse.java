/**
 * 
 */
package com.evampsaanga.azerfon.models.usagehistory.getsummary;

/**
 * @author Evamp & Saanga
 *
 */
public class UsageSummaryResponse {
	private String callStatus;
	private String resultCode;
	private String resultDesc;
	private String exception;
	UsageSummaryResponseData data;

	public String getCallStatus() {
		return callStatus;
	}

	public void setCallStatus(String callStatus) {
		this.callStatus = callStatus;
	}

	public String getResultCode() {
		return resultCode;
	}

	public void setResultCode(String resultCode) {
		this.resultCode = resultCode;
	}

	public String getResultDesc() {
		return resultDesc;
	}

	public void setResultDesc(String resultDesc) {
		this.resultDesc = resultDesc;
	}

	public String getException() {
		return exception;
	}

	public void setException(String exception) {
		this.exception = exception;
	}

	public UsageSummaryResponseData getData() {
		return data;
	}

	public void setData(UsageSummaryResponseData data) {
		this.data = data;
	}

}
