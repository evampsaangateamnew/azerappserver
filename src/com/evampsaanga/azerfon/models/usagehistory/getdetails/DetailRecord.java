/**
 * 
 */
package com.evampsaanga.azerfon.models.usagehistory.getdetails;

/**
 * @author Evamp & Saanga
 *
 */
public class DetailRecord {
	private String chargedAmount;
	private String destination;
	private String startDateTime;
	private String endDateTime;
	private String number;
	private String period;
	private String service;
	private String type;
	private String usage;

	public String getChargedAmount() {
		return chargedAmount;
	}

	public void setChargedAmount(String chargedAmount) {
		this.chargedAmount = chargedAmount;
	}

	public String getDestination() {
		return destination;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}

	public String getStartDateTime() {
		return startDateTime;
	}

	public void setStartDateTime(String startDateTime) {
		this.startDateTime = startDateTime;
	}

	public String getEndDateTime() {
		return endDateTime;
	}

	public void setEndDateTime(String endDateTime) {
		this.endDateTime = endDateTime;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public String getPeriod() {
		return period;
	}

	public void setPeriod(String period) {
		this.period = period;
	}

	public String getService() {
		return service;
	}

	public void setService(String service) {
		this.service = service;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getUsage() {
		return usage;
	}

	public void setUsage(String usage) {
		this.usage = usage;
	}

}
