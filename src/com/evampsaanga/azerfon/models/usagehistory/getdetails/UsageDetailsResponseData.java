/**
 * 
 */
package com.evampsaanga.azerfon.models.usagehistory.getdetails;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * @author Evamp & Saanga
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class UsageDetailsResponseData {
	private List<DetailRecord> records;

	public List<DetailRecord> getRecords() {
		return records;
	}

	public void setRecords(List<DetailRecord> records) {
		this.records = records;
	}
}
