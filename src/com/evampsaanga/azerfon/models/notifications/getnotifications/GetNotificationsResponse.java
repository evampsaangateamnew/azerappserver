/**
 * 
 */
package com.evampsaanga.azerfon.models.notifications.getnotifications;

import com.evampsaanga.azerfon.common.utilities.BaseResponse;

/**
 * @author Evamp & Saanga
 *
 */
public class GetNotificationsResponse extends BaseResponse {

	private GetNotificationsResponseData data;

	public GetNotificationsResponseData getData() {
		return data;
	}

	public void setData(GetNotificationsResponseData data) {
		this.data = data;
	}
}
