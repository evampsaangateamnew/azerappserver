/**
 * 
 */
package com.evampsaanga.azerfon.models.notifications.getnotifications;

/**
 * @author Evamp & Saanga
 *
 */
public class Notification {

	private String message;
	private String datetime;
	private String icon;
	private String actionType;
	private String actionID;
	private String btnTxt;

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getDatetime() {
		return datetime;
	}

	public void setDatetime(String datetime) {
		this.datetime = datetime;
	}

	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

	public String getActionType() {
		return actionType;
	}

	public void setActionType(String actionType) {
		this.actionType = actionType;
	}

	public String getActionID() {
		return actionID;
	}

	public void setActionID(String actionID) {
		this.actionID = actionID;
	}

	public String getBtnTxt() {
		return btnTxt;
	}

	public void setBtnTxt(String btnTxt) {
		this.btnTxt = btnTxt;
	}

}
