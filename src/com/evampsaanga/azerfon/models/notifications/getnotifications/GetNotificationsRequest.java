/**
 * 
 */
package com.evampsaanga.azerfon.models.notifications.getnotifications;

import com.evampsaanga.azerfon.common.utilities.BaseRequest;

/**
 * @author Evamp & Saanga
 *
 */
public class GetNotificationsRequest extends BaseRequest {

	@Override
	public String toString() {
		return "GetNotificationsRequest [getLang()=" + getLang() + ", getiP()=" + getiP() + ", getChannel()="
				+ getChannel() + ", getMsisdn()=" + getMsisdn() + "]";
	}

}
