/**
 * 
 */
package com.evampsaanga.azerfon.models.notifications.getnotifications;

import java.util.List;

/**
 * @author Evamp & Saanga
 *
 */
public class GetNotificationsResponseData {
	private List<Notification> notificationsList;

	public List<Notification> getNotificationsList() {
		return notificationsList;
	}

	public void setNotificationsList(List<Notification> notificationsList) {
		this.notificationsList = notificationsList;
	}

}
