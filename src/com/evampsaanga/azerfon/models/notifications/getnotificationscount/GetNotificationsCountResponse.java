/**
 * 
 */
package com.evampsaanga.azerfon.models.notifications.getnotificationscount;

import com.evampsaanga.azerfon.common.utilities.BaseResponse;

/**
 * @author Evamp & Saanga
 *
 */
public class GetNotificationsCountResponse extends BaseResponse {

	GetNotificationsCountResponseData data;

	public GetNotificationsCountResponseData getData() {
		return data;
	}

	public void setData(GetNotificationsCountResponseData data) {
		this.data = data;
	}

}
