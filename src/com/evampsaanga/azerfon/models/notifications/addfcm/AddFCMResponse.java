/**
 * 
 */
package com.evampsaanga.azerfon.models.notifications.addfcm;

import com.evampsaanga.azerfon.common.utilities.BaseResponse;

/**
 * @author Zainab
 *
 */
public class AddFCMResponse extends BaseResponse {

	private AddFCMResponseData data;

	public AddFCMResponseData getData() {
		return data;
	}

	public void setData(AddFCMResponseData data) {
		this.data = data;
	}

}
