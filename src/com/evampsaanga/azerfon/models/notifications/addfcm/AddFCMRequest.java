/**
 * 
 */
package com.evampsaanga.azerfon.models.notifications.addfcm;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.evampsaanga.azerfon.common.utilities.BaseRequest;

/**
 * @author Zainab
 *
 */
public class AddFCMRequest extends BaseRequest {

	private String fcmKey;
	private String ringingStatus;
	private String isEnable;
	private String cause;
	@JsonIgnore
	private String subscriberType;
	@JsonIgnore
	private String tariffType;

	public String getRingingStatus() {
		return ringingStatus;
	}

	public void setRingingStatus(String ringingStatus) {
		this.ringingStatus = ringingStatus;
	}

	public String getFcmKey() {
		return fcmKey;
	}

	public void setFcmKey(String fcmKey) {
		this.fcmKey = fcmKey;
	}

	public String getIsEnable() {
		return isEnable;
	}

	public void setIsEnable(String isEnable) {
		this.isEnable = isEnable;
	}

	public String getCause() {
		return cause;
	}

	public void setCause(String cause) {
		this.cause = cause;
	}

	public String getSubscriberType() {
		return subscriberType;
	}

	public void setSubscriberType(String subscriberType) {
		this.subscriberType = subscriberType;
	}

	public String getTariffType() {
		return tariffType;
	}

	public void setTariffType(String tariffType) {
		this.tariffType = tariffType;
	}

	@Override
	public String toString() {
		return "AddFCMRequest [fcmKey=" + fcmKey + ", ringingStatus=" + ringingStatus + ", isEnable=" + isEnable
				+ ", cause=" + cause + ", subscriberType=" + subscriberType + ", tariffType=" + tariffType + "]";
	}

}
