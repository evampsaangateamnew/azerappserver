/**
 * 
 */
package com.evampsaanga.azerfon.models.notifications.getnotificationsconfigurations;

import com.evampsaanga.azerfon.common.utilities.BaseRequest;

/**
 * @author Evamp & Saanga
 *
 */
public class GetNotificationsConfigurationsRequest extends BaseRequest {

	@Override
	public String toString() {
		return "GetNotificationsConfigurationsRequest [getLang()=" + getLang() + ", getiP()=" + getiP()
				+ ", getChannel()=" + getChannel() + ", getMsisdn()=" + getMsisdn() + "]";
	}

}
