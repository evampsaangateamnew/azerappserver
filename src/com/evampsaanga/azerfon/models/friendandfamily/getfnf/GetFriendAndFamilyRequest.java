/**
 * 
 */
package com.evampsaanga.azerfon.models.friendandfamily.getfnf;

import com.evampsaanga.azerfon.common.utilities.BaseRequest;

/**
 * @author Evamp & Saanga
 *
 */
public class GetFriendAndFamilyRequest extends BaseRequest {
	String offeringId;

	public String getOfferingId() {
		return offeringId;
	}

	public void setOfferingId(String offeringId) {
		this.offeringId = offeringId;
	}

	@Override
	public String toString() {
		return "GetFriendAndFamilyRequest [offeringId=" + offeringId + ", getLang()=" + getLang() + ", getiP()="
				+ getiP() + ", getChannel()=" + getChannel() + ", getMsisdn()=" + getMsisdn() + "]";
	}

}
