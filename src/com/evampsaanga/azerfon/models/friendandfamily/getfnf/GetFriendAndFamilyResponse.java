/**
 * 
 */
package com.evampsaanga.azerfon.models.friendandfamily.getfnf;

import com.evampsaanga.azerfon.common.utilities.BaseResponse;

/**
 * @author Evamp & Saanga
 *
 */
public class GetFriendAndFamilyResponse extends BaseResponse {

	private FriendAndFamilyResponseData data;

	public FriendAndFamilyResponseData getData() {
		return data;
	}

	public void setData(FriendAndFamilyResponseData data) {
		this.data = data;
	}
}
