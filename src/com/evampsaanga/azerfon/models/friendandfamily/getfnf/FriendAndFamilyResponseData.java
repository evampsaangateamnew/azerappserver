/**
 * 
 */
package com.evampsaanga.azerfon.models.friendandfamily.getfnf;

import java.util.List;

/**
 * @author Evamp & Saanga
 *
 */
public class FriendAndFamilyResponseData {

	List<FNF> fnfList;
	private String fnfLimit;

	public List<FNF> getFnfList() {
		return fnfList;
	}

	public void setFnfList(List<FNF> fnfList) {
		this.fnfList = fnfList;
	}

	public String getFnfLimit() {
		return fnfLimit;
	}

	public void setFnfLimit(String fnfLimit) {
		this.fnfLimit = fnfLimit;
	}

}
