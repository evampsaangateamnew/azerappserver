/**
 * 
 */
package com.evampsaanga.azerfon.models.friendandfamily.updatefnf;

import com.evampsaanga.azerfon.common.utilities.BaseResponse;
import com.evampsaanga.azerfon.models.friendandfamily.getfnf.FriendAndFamilyResponseData;

/**
 * @author Evamp & Saanga
 *
 */
public class UpdateFriendAndFamilyResponse extends BaseResponse {
	private FriendAndFamilyResponseData data;

	public FriendAndFamilyResponseData getData() {
		return data;
	}

	public void setData(FriendAndFamilyResponseData data) {
		this.data = data;
	}
}
