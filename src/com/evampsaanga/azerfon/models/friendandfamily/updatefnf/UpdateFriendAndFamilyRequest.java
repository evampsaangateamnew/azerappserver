/**
 * 
 */
package com.evampsaanga.azerfon.models.friendandfamily.updatefnf;

import com.evampsaanga.azerfon.common.utilities.BaseRequest;

/**
 * @author Evamp & Saanga
 *
 */
public class UpdateFriendAndFamilyRequest extends BaseRequest {

	String oldMsisdn;
	String newMsisdn;
	String offeringId;
	String actionType;

	public String getActionType() {
		return actionType;
	}

	public void setActionType(String actionType) {
		this.actionType = actionType;
	}

	public String getOldMsisdn() {
		return oldMsisdn;
	}

	public void setOldMsisdn(String oldMsisdn) {
		this.oldMsisdn = oldMsisdn;
	}

	public String getNewMsisdn() {
		return newMsisdn;
	}

	public void setNewMsisdn(String newMsisdn) {
		this.newMsisdn = newMsisdn;
	}

	public String getOfferingId() {
		return offeringId;
	}

	public void setOfferingId(String offeringId) {
		this.offeringId = offeringId;
	}

	@Override
	public String toString() {
		return "UpdateFriendAndFamilyRequest [oldMsisdn=" + oldMsisdn + ", newMsisdn=" + newMsisdn + ", offeringId="
				+ offeringId + "]";
	}

}
