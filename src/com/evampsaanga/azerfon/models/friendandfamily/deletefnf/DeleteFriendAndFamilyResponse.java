/**
 * 
 */
package com.evampsaanga.azerfon.models.friendandfamily.deletefnf;

import com.evampsaanga.azerfon.common.utilities.BaseResponse;
import com.evampsaanga.azerfon.models.friendandfamily.getfnf.FriendAndFamilyResponseData;

/**
 * @author Evamp & Saanga
 *
 */
public class DeleteFriendAndFamilyResponse extends BaseResponse {
	private FriendAndFamilyResponseData data;

	public FriendAndFamilyResponseData getData() {
		return data;
	}

	public void setData(FriendAndFamilyResponseData data) {
		this.data = data;
	}
}
