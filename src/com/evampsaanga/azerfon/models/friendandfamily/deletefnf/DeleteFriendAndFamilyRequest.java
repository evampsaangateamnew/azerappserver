/**
 * 
 */
package com.evampsaanga.azerfon.models.friendandfamily.deletefnf;

import java.util.ArrayList;

import com.evampsaanga.azerfon.common.utilities.BaseRequest;

/**
 * @author Evamp & Saanga
 *
 */
public class DeleteFriendAndFamilyRequest extends BaseRequest {
	String deleteMsisdn;
	String offeringId;
	private ArrayList<String> deleteFnf=new ArrayList<String>();

	public ArrayList<String> getDeleteFnf() {
		return deleteFnf;
	}

	public void setDeleteFnf(ArrayList<String> deleteFnf) {
		this.deleteFnf = deleteFnf;
	}

	public String getDeleteMsisdn() {
		return deleteMsisdn;
	}

	public void setDeleteMsisdn(String deleteMsisdn) {
		this.deleteMsisdn = deleteMsisdn;
	}

	public String getOfferingId() {
		return offeringId;
	}

	public void setOfferingId(String offeringId) {
		this.offeringId = offeringId;
	}

	@Override
	public String toString() {
		return "DeleteFriendAndFamilyRequest [deleteMsisdn=" + deleteMsisdn + ", offeringId=" + offeringId
				+ ", getLang()=" + getLang() + ", getiP()=" + getiP() + ", getChannel()=" + getChannel()
				+ ", getMsisdn()=" + getMsisdn() + "]";
	}

}
