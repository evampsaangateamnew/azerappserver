/**
 * 
 */
package com.evampsaanga.azerfon.models.friendandfamily.addfnf;

import com.evampsaanga.azerfon.common.utilities.BaseRequest;

/**
 * @author Evamp & Saanga
 *
 */
public class AddFriendAndFamilyRequest extends BaseRequest {
	String addMsisdn;
	String offeringId;
	String isFirstTime;
	
	

	public String getIsFirstTime() {
		return isFirstTime;
	}

	public void setIsFirstTime(String isFirstTime) {
		this.isFirstTime = isFirstTime;
	}

	public String getAddMsisdn() {
		return addMsisdn;
	}

	public void setAddMsisdn(String addMsisdn) {
		this.addMsisdn = addMsisdn;
	}

	public String getOfferingId() {
		return offeringId;
	}

	public void setOfferingId(String offeringId) {
		this.offeringId = offeringId;
	}

	@Override
	public String toString() {
		return "AddFriendAndFamilyRequest [addMsisdn=" + addMsisdn + ", offeringId=" + offeringId + ", getLang()="
				+ getLang() + ", getiP()=" + getiP() + ", getChannel()=" + getChannel() + ", getMsisdn()=" + getMsisdn()
				+ "]";
	}

}
