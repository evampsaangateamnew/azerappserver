/**
 * 
 */
package com.evampsaanga.azerfon.models.friendandfamily.addfnf;

import com.evampsaanga.azerfon.common.utilities.BaseResponse;
import com.evampsaanga.azerfon.models.friendandfamily.getfnf.FriendAndFamilyResponseData;

/**
 * @author Evamp & Saanga
 *
 */
public class AddFriendAndFamilyResponse extends BaseResponse {
	private FriendAndFamilyResponseData data;

	public FriendAndFamilyResponseData getData() {
		return data;
	}

	public void setData(FriendAndFamilyResponseData data) {
		this.data = data;
	}
}
