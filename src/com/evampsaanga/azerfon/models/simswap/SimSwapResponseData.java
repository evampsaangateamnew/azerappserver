package com.evampsaanga.azerfon.models.simswap;

public class SimSwapResponseData {

	private String message;
	private String transactionId;
	private String specialGroup;

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}

	public String getSpecialGroup() {
		return specialGroup;
	}

	public void setSpecialGroup(String specialGroup) {
		this.specialGroup = specialGroup;
	}
	
	
}
