/**
 * 
 */
package com.evampsaanga.azerfon.models.tariffgroupdetails;

import com.evampsaanga.azerfon.common.utilities.BaseResponse;

/**
 * @author Evamp & Saanga
 *
 */
public class TariffResponse extends BaseResponse {

	com.evampsaanga.azerfon.models.tariffdetails.TariffResponseData data;

	public com.evampsaanga.azerfon.models.tariffdetails.TariffResponseData getData() {
		return data;
	}

	public void setData(com.evampsaanga.azerfon.models.tariffdetails.TariffResponseData data) {
		this.data = data;
	}

}
