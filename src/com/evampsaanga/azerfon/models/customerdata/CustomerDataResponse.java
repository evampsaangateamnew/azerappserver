package com.evampsaanga.azerfon.models.customerdata;

import com.evampsaanga.azerfon.common.utilities.BaseResponse;

public class CustomerDataResponse extends BaseResponse {
	CustomerDataResponseData data;

	public CustomerDataResponseData getData() {
		return data;
	}

	public void setData(CustomerDataResponseData data) {
		this.data = data;
	}

}
