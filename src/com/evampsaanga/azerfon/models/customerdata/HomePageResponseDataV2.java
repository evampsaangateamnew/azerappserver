package com.evampsaanga.azerfon.models.customerdata;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.evampsaanga.azerfon.models.generalservices.predefineddata.PredefinedDataResponse;
import com.evampsaanga.azerfon.models.homepageservices.HomePageResponseData;

@JsonIgnoreProperties(ignoreUnknown = true)
public class HomePageResponseDataV2 {

	private HomePageResponseData homePageData;
	private CustomerDataResponseData customerInfo;
	private PredefinedDataResponse predefinedData;

	public PredefinedDataResponse getPredefinedData() {
		return predefinedData;
	}

	public void setPredefinedData(PredefinedDataResponse predefinedData) {
		this.predefinedData = predefinedData;
	}

	public CustomerDataResponseData getCustomerInfo() {
		return customerInfo;
	}

	public void setCustomerInfo(CustomerDataResponseData customerInfo) {
		this.customerInfo = customerInfo;
	}

	public HomePageResponseData getHomePageData() {
		return homePageData;
	}

	public void setHomePageData(HomePageResponseData homePageData) {
		this.homePageData = homePageData;
	}

}
