package com.evampsaanga.azerfon.models.customerdata;

import com.evampsaanga.azerfon.common.utilities.BaseRequest;

public class CustomerDataRequest extends BaseRequest {
	private String msisdn;

	public String getMsisdn() {
		return msisdn;
	}

	public void setMsisdn(String msisdn) {
		this.msisdn = msisdn;
	}

}
