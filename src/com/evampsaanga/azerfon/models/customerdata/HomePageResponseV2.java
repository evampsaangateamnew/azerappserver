package com.evampsaanga.azerfon.models.customerdata;

import com.evampsaanga.azerfon.common.utilities.BaseResponse;

public class HomePageResponseV2 extends BaseResponse {

	private HomePageResponseDataV2 data;

	public HomePageResponseDataV2 getData() {
		return data;
	}

	public void setData(HomePageResponseDataV2 data) {
		this.data = data;
	}

}