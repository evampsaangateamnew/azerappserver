
package com.evampsaanga.azerfon.models.msisdninvoice;

import org.apache.commons.lang.builder.ToStringBuilder;

import com.evampsaanga.azerfon.common.utilities.BaseResponse;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)

public class MSISDNDetailsResponse extends BaseResponse{

    @JsonProperty("data")
    private MSISDNDetail data;
    
    @JsonProperty("data")
    public MSISDNDetail getData() {
		return data;
	}
    
    @JsonProperty("data")
	public void setData(MSISDNDetail data) {
		this.data = data;
	}

	@Override
    public String toString() {
        return new ToStringBuilder(this).append("data", data).toString();
    }

}
