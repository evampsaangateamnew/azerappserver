/**
 * 
 */
package com.evampsaanga.azerfon.models.msisdninvoice;

import com.evampsaanga.azerfon.common.utilities.BaseResponse;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

/**
 * @author HamzaFarooque
 *
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class MSISDNSummaryResponse extends BaseResponse{

	@JsonProperty("data")
	private SummaryData data = null;

	public SummaryData getData() {
		return data;
	}

	public void setData(SummaryData data) {
		this.data = data;
	}

}
