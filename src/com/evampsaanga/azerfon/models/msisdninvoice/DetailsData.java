/**
 * 
 */
package com.evampsaanga.azerfon.models.msisdninvoice;

/**
 * @author HamzaFarooque
 *
 */
public class DetailsData {
	
	String Label;
	String Amount;
	
	public String getLabel() {
		return Label;
	}
	public void setLabel(String label) {
		Label = label;
	}
	public String getAmount() {
		return Amount;
	}
	public void setAmount(String amount) {
		Amount = amount;
	}
	
}
