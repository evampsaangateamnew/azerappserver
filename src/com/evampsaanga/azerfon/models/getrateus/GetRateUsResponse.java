package com.evampsaanga.azerfon.models.getrateus;

import com.evampsaanga.azerfon.common.utilities.BaseResponse;

public class GetRateUsResponse extends BaseResponse{

	private GetRateUsResponseData data;

	public GetRateUsResponseData getData() {
		return data;
	}

	public void setData(GetRateUsResponseData data) {
		this.data = data;
	}
	
}
