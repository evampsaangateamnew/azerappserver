package com.evampsaanga.azerfon.models.getrateus;

import com.evampsaanga.azerfon.common.utilities.BaseRequest;

public class GetRateUsRequest extends BaseRequest{

	private String entityId;

	public String getEntityId() {
		return entityId;
	}

	public void setEntityId(String entityId) {
		this.entityId = entityId;
	}
}
