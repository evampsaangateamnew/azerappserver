package com.evampsaanga.azerfon.models.tariffdetails;

import com.evampsaanga.azerfon.models.tariffdetails.postpaid.individual.Individual;

public class BusinessIndividualTariffData {
	int sortOrder;
	String tariffType;
	Individual individual;

	public BusinessIndividualTariffData() {
		individual = new Individual();
	}

	public int getSortOrder() {
		return sortOrder;
	}

	public void setSortOrder(int sortOrder) {
		this.sortOrder = sortOrder;
	}

	public String getTariffType() {
		return tariffType;
	}

	public void setTariffType(String tariffType) {
		this.tariffType = tariffType;
	}

	public Individual getIndividual() {
		return individual;
	}

	public void setIndividual(Individual individual) {
		this.individual = individual;
	}

}
