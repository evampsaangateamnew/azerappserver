package com.evampsaanga.azerfon.models.tariffdetails;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.evampsaanga.azerfon.models.supplementaryofferings.getsupplementaryofferings.commonofferings.SupplementryOfferingsData;
import com.evampsaanga.azerfon.models.supplementaryofferings.getsupplementaryofferings.offerscategoriestab.Internet;

public class TariffResponseList implements Cloneable {
	
	
	List<TariffResponseData> tariffResponseList=new ArrayList<TariffResponseData>();

	public List<TariffResponseData> getTariffResponseList() {
		return tariffResponseList;
	}

	public void setTariffResponseList(List<TariffResponseData> tariffResponseList) {
		this.tariffResponseList = tariffResponseList;
	}

	
	@Override
    public Object clone() throws CloneNotSupportedException
    {
		TariffResponseList dataObj = (TariffResponseList) super.clone();
     
	
		//List<SupplementryOfferingsData> off=dataObj.getOffers();
		
		ArrayList<TariffResponseData> tariffResponseListtClone = new ArrayList<>();
        
        Iterator<TariffResponseData> iterator = dataObj.getTariffResponseList().iterator();
        while(iterator.hasNext()){
        	tariffResponseListtClone.add((TariffResponseData) iterator.next().clone());
        }
           // student.data=(SupplementaryOfferingsResponseData) data.c
        dataObj.tariffResponseList=tariffResponseListtClone;
            return dataObj;
    }

	

}
