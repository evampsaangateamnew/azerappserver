package com.evampsaanga.azerfon.models.tariffdetails;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Header implements Cloneable, Serializable {
	private String id;
	private String offeringId;
	private String currency;
	private String name;
	private String priceLabel;
	private String priceValue;
	private String tariffDescription;
	private Integer sortOrder;
	private String bonusLabel;

	public String getBonusLabel() {
		return bonusLabel;
	}

	public void setBonusLabel(String bonusLabel) {
		this.bonusLabel = bonusLabel;
	}

	public List<Attributes> getAttributes() {
		return attributes;
	}

	public void setAttributes(List<Attributes> attributes) {
		this.attributes = attributes;
	}

	List<Attributes> attributes = new ArrayList<Attributes>();

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPriceLabel() {
		return priceLabel;
	}

	public void setPriceLabel(String priceLabel) {
		this.priceLabel = priceLabel;
	}

	public String getPriceValue() {
		return priceValue;
	}

	public void setPriceValue(String priceValue) {
		this.priceValue = priceValue;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getOfferingId() {
		return offeringId;
	}

	public void setOfferingId(String offeringId) {
		this.offeringId = offeringId;
	}

	public Integer getSortOrder() {
		return sortOrder;
	}

	public void setSortOrder(Integer sortOrder) {
		this.sortOrder = sortOrder;
	}

	public String getTariffDescription() {
		return tariffDescription;
	}

	public void setTariffDescription(String tariffDescription) {
		this.tariffDescription = tariffDescription;
	}
	@Override
    public Object clone() throws CloneNotSupportedException
    {
		Header dataObj = (Header) super.clone();
          return dataObj;
	
    }
}
