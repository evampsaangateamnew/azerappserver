/**
 * 
 */
package com.evampsaanga.azerfon.models.tariffdetails;

import java.util.ArrayList;

import com.evampsaanga.azerfon.common.utilities.BaseResponse;
import com.evampsaanga.azerfon.models.supplementaryofferings.getsupplementaryofferings.SupplementaryOfferingsResponse;
import com.evampsaanga.azerfon.models.supplementaryofferings.getsupplementaryofferings.SupplementaryOfferingsResponseData;

/**
 * @author Evamp & Saanga
 *
 */
public class TariffResponse extends BaseResponse implements Cloneable
{
	TariffResponseList data;

	public TariffResponseList getData() {
		return data;
	}

	public void setData(TariffResponseList data) {
		this.data = data;
	}
	@Override
    public Object clone() throws CloneNotSupportedException
    {
		TariffResponse dataObj = (TariffResponse) super.clone();
 
	dataObj.data=(TariffResponseList) data.clone();
       // student.data=(SupplementaryOfferingsResponseData) data.c
 
        return dataObj;
    }
}
