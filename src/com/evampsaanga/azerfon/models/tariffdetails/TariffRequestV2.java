/**
 * 
 */
package com.evampsaanga.azerfon.models.tariffdetails;

import java.util.ArrayList;

import com.evampsaanga.azerfon.common.utilities.BaseRequest;

/**
 * @author Evamp & Saanga
 *
 */
public class TariffRequestV2 extends BaseRequest {

	ArrayList<String> offeringId;

	public ArrayList<String> getOfferingId() {
		return offeringId;
	}

	public void setOfferingId(ArrayList<String> offeringId) {
		this.offeringId = offeringId;
	}

}
