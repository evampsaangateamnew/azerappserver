package com.evampsaanga.azerfon.models.tariffdetails;

import com.evampsaanga.azerfon.models.tariffdetails.prepaid.cin.HeaderCall;
import com.evampsaanga.azerfon.models.tariffdetails.prepaid.cin.HeaderInternet;
import com.evampsaanga.azerfon.models.tariffdetails.prepaid.klass.packageprice.PackagePriceModel;

public class PackagePrice {
	
	private String packagePriceLabel;
	private HeaderCall call;
	private PackagePriceModel sms;
	private HeaderInternet internet;
	
	private HeaderCall callPayg;
	private PackagePriceModel smsPayg;
	private HeaderInternet internetPayg;

	public PackagePrice() {
		this.call = new HeaderCall();
		this.sms = new PackagePriceModel();
		this.internet = new HeaderInternet();
		this.callPayg = new HeaderCall();
		this.smsPayg = new PackagePriceModel();
		this.internetPayg = new HeaderInternet();
	}
	
	public HeaderCall getCallPayg() {
		return callPayg;
	}

	public void setCallPayg(HeaderCall callPayg) {
		this.callPayg = callPayg;
	}

	public PackagePriceModel getSmsPayg() {
		return smsPayg;
	}

	public void setSmsPayg(PackagePriceModel smsPayg) {
		this.smsPayg = smsPayg;
	}

	public HeaderInternet getInternetPayg() {
		return internetPayg;
	}

	public void setInternetPayg(HeaderInternet internetPayg) {
		this.internetPayg = internetPayg;
	}



	public HeaderCall getCall() {
		return call;
	}

	public void setCall(HeaderCall call) {
		this.call = call;
	}

	public PackagePriceModel getSms() {
		return sms;
	}

	public void setSms(PackagePriceModel sms) {
		this.sms = sms;
	}

	public HeaderInternet getInternet() {
		return internet;
	}

	public void setInternet(HeaderInternet internet) {
		this.internet = internet;
	}

	public String getPackagePriceLabel() {
		return packagePriceLabel;
	}

	public void setPackagePriceLabel(String packagePriceLabel) {
		this.packagePriceLabel = packagePriceLabel;
	}

}
