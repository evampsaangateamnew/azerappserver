/**
 * 
 */
package com.evampsaanga.azerfon.models.tariffdetails;

import java.util.ArrayList;

import com.evampsaanga.azerfon.common.utilities.BaseResponse;

/**
 * @author Evamp & Saanga
 *
 */
public class TariffResponseV2 extends BaseResponse {
	ArrayList<TariffResponseDataV2> data;

	public ArrayList<TariffResponseDataV2> getData() {
		return data;
	}

	public void setData(ArrayList<TariffResponseDataV2> data) {
		this.data = data;
	}

	
}
