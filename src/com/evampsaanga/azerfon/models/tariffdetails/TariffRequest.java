/**
 * 
 */
package com.evampsaanga.azerfon.models.tariffdetails;

import java.util.ArrayList;

import com.evampsaanga.azerfon.common.utilities.BaseRequest;

/**
 * @author Evamp & Saanga
 *
 */
public class TariffRequest extends BaseRequest {

	private String subscriberType;
	private String offeringId;
	private String storeId;
	private String groupIds;
	public String getGroupIds() {
		return groupIds;
	}

	public void setGroupIds(String groupIds) {
		this.groupIds = groupIds;
	}

	private ArrayList<String> specialTariffIds=new ArrayList<String>();

	public ArrayList<String> getSpecialTariffIds() {
		return specialTariffIds;
	}

	public void setSpecialTariffIds(ArrayList<String> specialTariffIds) {
		this.specialTariffIds = specialTariffIds;
	}

	public String getStoreId() {
		return storeId;
	}

	public void setStoreId(String storeId) {
		this.storeId = storeId;
	}

	public String getSubscriberType() {
		return subscriberType;
	}

	public void setSubscriberType(String subscriberType) {
		this.subscriberType = subscriberType;
	}

	public String getOfferingId() {
		return offeringId;
	}

	public void setOfferingId(String offeringId) {
		this.offeringId = offeringId;
	}

	@Override
	public String toString() {
		return "TariffRequest [subscriberType=" + subscriberType + ", offeringId=" + offeringId + ", storeId=" + storeId
				+ "]";
	}

}
