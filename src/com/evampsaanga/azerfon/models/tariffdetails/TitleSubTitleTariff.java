/**
 * 
 */
package com.evampsaanga.azerfon.models.tariffdetails;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Evamp & Saanga
 *
 */
public class TitleSubTitleTariff {
	private String title;
	private String titleIcon;
	private String shortDesc;
	private List<GenericAttributes> attributesList;

	/**
	 * @param label
	 * @param shortDesc
	 * @param attributes
	 */
	public TitleSubTitleTariff() {
		this.title = "";
		this.shortDesc = "";
		this.setAttributesList(new ArrayList<>());
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}
	public String getTitleIcon() {
		return titleIcon;
	}

	public void setTitleIcon(String titleIcon) {
		this.titleIcon = titleIcon;
	}
	public String getShortDesc() {
		return shortDesc;
	}

	public void setShortDesc(String shortDesc) {
		this.shortDesc = shortDesc;
	}

	public List<GenericAttributes> getAttributesList() {
		return attributesList;
	}

	public void setAttributesList(List<GenericAttributes> attributesList) {
		this.attributesList = attributesList;
	}

}
