package com.evampsaanga.azerfon.models.tariffdetails;

import java.io.Serializable;

import com.evampsaanga.azerfon.models.tariffdetails.TariffDetailsSection;

public class Items implements Cloneable,Serializable {
	private Header header;
	private PackagePrice packagePrice;

	private TariffDetailsSection details;
	public String getSubscribable() {
		return subscribable;
	}
	public Items() {
		this.header = new Header();
		this.packagePrice = new PackagePrice();
		
		this.details = new TariffDetailsSection();
		this.subscribable=subscribable;

	}

	public void setSubscribable(String subscribable) {
		this.subscribable = subscribable;
	}

	private String subscribable;




	public Header getHeader() {
		return header;
	}


	public void setHeader(Header header) {
		this.header = header;
	}


	public PackagePrice getPackagePrice() {
		return packagePrice;
	}

	public void setPackagePrice(PackagePrice packagePrice) {
		this.packagePrice = packagePrice;
	}

	public TariffDetailsSection getDetails() {
		return details;
	}

	public void setDetails(TariffDetailsSection details) {
		this.details = details;
	}
	@Override
    public Object clone() throws CloneNotSupportedException
    {
		Items dataObj = (Items) super.clone();
 
	dataObj.header=(Header) header.clone();
       // student.data=(SupplementaryOfferingsResponseData) data.c
 
        return dataObj;
    }
}
