/**
 * 
 */
package com.evampsaanga.azerfon.models.tariffdetails;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.swing.text.AbstractDocument.Content;

import com.evampsaanga.azerfon.models.tariffdetails.postpaid.PostpaidTariff;
import com.evampsaanga.azerfon.models.tariffdetails.prepaid.PrepaidTariff;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

/**
 * @author Evamp & Saanga
 *
 */

public class TariffResponseData implements Cloneable{

	private String groupType;
	@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
	private String isSpecial="false";
	List<Items> items=new ArrayList<Items>();
	public String getIsSpecial() {
		return isSpecial;
	}

	public void setIsSpecial(String isSpecial) {
		this.isSpecial = isSpecial;
	}


	
	public String getGroupType() {
		return groupType;
	}

	public void setGroupType(String groupType) {
		this.groupType = groupType;
	}

	

	public List<Items> getItems() {
		return items;
	}

	public void setItems(List<Items> items) {
		this.items = items;
	}

	@Override
    public Object clone() throws CloneNotSupportedException
    {
		TariffResponseData dataObj = (TariffResponseData) super.clone();
     
	
		//List<SupplementryOfferingsData> off=dataObj.getOffers();
		
		ArrayList<Items> ItemsClone = new ArrayList<>();
        
        Iterator<Items> iterator = dataObj.getItems().iterator();
        while(iterator.hasNext()){
        	ItemsClone.add((Items) iterator.next().clone());
        }
           // student.data=(SupplementaryOfferingsResponseData) data.c
        dataObj.items=ItemsClone;
            return dataObj;
    }
	
}
