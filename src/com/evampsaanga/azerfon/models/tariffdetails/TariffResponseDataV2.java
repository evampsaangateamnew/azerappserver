/**
 * 
 */
package com.evampsaanga.azerfon.models.tariffdetails;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Evamp & Saanga
 *
 */
public class TariffResponseDataV2 {
	
	private String groupType;
	private Header header;
	private PackagePrice packagePrice;

	private TariffDetailsSection details;
	
	public TariffResponseDataV2() {
		this.header = new Header();
		this.packagePrice = new PackagePrice();
		
		this.details = new TariffDetailsSection();

	}

	public void setSubscribable(String subscribable) {
		this.subscribable = subscribable;
	}

	private String subscribable;




	public Header getHeader() {
		return header;
	}


	public void setHeader(Header header) {
		this.header = header;
	}


	public PackagePrice getPackagePrice() {
		return packagePrice;
	}

	public void setPackagePrice(PackagePrice packagePrice) {
		this.packagePrice = packagePrice;
	}

	public TariffDetailsSection getDetails() {
		return details;
	}

	public void setDetails(TariffDetailsSection details) {
		this.details = details;
	}
	public String getGroupType() {
		return groupType;
	}
	public void setGroupType(String groupType) {
		this.groupType = groupType;
	}

}
