/**
 * 
 */
package com.evampsaanga.azerfon.models.tariffdetails.postpaid;

import java.util.ArrayList;
import java.util.List;

import com.evampsaanga.azerfon.models.tariffdetails.postpaid.corporate.Corporate;
import com.evampsaanga.azerfon.models.tariffdetails.postpaid.individual.Individual;
import com.evampsaanga.azerfon.models.tariffdetails.prepaid.klass.Klass;

/**
 * @author Evamp & Saanga
 *
 */
public class PostpaidTariff {

	private List<Corporate> corporate;
	private List<Individual> individual;
	private List<Klass> klassPostpaid;

	public PostpaidTariff() {
		this.corporate = new ArrayList<>();
		this.individual = new ArrayList<>();
		this.klassPostpaid = new ArrayList<>();

	}

	public List<Corporate> getCorporate() {
		return corporate;
	}

	public void setCorporate(List<Corporate> corporate) {
		this.corporate = corporate;
	}

	public List<Individual> getIndividual() {
		return individual;
	}

	public void setIndividual(List<Individual> individual) {
		this.individual = individual;
	}

	public List<Klass> getKlassPostpaid() {
		return klassPostpaid;
	}

	public void setKlassPostpaid(List<Klass> klassPostpaid) {
		this.klassPostpaid = klassPostpaid;
	}

}
