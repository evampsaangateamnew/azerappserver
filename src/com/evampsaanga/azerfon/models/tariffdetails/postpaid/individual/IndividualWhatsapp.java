/**
 * 
 */
package com.evampsaanga.azerfon.models.tariffdetails.postpaid.individual;

/**
 * @author Evamp & Saanga
 *
 */
public class IndividualWhatsapp {
	private String iconName;
	private String title;
	private String titleValue;
	private String metrics;

	public String getIconName() {
		return iconName;
	}

	public void setIconName(String iconName) {
		this.iconName = iconName;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getTitleValue() {
		return titleValue;
	}

	public void setTitleValue(String titleValue) {
		this.titleValue = titleValue;
	}

	public String getMetrics() {
		return metrics;
	}

	public void setMetrics(String metrics) {
		this.metrics = metrics;
	}
}
