/**
 * 
 */
package com.evampsaanga.azerfon.models.tariffdetails.postpaid.individual;

import com.evampsaanga.azerfon.models.tariffdetails.TariffDetailsSection;
import com.evampsaanga.azerfon.models.tariffdetails.postpaid.individual.description.IndividualDescription;

/**
 * @author Evamp & Saanga
 *
 */
public class Individual {
	private IndividualHeader header;
	private TariffDetailsSection details;
	private IndividualDescription description;

	public Individual() {
		this.header = new IndividualHeader();
		this.setDetails(new TariffDetailsSection());
		this.description = new IndividualDescription();
	}

	public IndividualHeader getHeader() {
		return header;
	}

	public void setHeader(IndividualHeader header) {
		this.header = header;
	}

	public IndividualDescription getDescription() {
		return description;
	}

	public void setDescription(IndividualDescription description) {
		this.description = description;
	}

	public TariffDetailsSection getDetails() {
		return details;
	}

	public void setDetails(TariffDetailsSection details) {
		this.details = details;
	}

}
