/**
 * 
 */
package com.evampsaanga.azerfon.models.tariffdetails.postpaid.individual.description;

import com.evampsaanga.azerfon.models.tariffdetails.prepaid.cin.description.Advantages;
import com.evampsaanga.azerfon.models.tariffdetails.prepaid.cin.description.Classification;

/**
 * @author Evamp & Saanga
 *
 */
public class IndividualDescription {

	private String descriptionTitle;
	private Advantages advantages;
	private Classification classification;

	public Advantages getAdvantages() {
		return advantages;
	}

	public void setAdvantages(Advantages advantages) {
		this.advantages = advantages;
	}

	public Classification getClassification() {
		return classification;
	}

	public void setClassification(Classification classification) {
		this.classification = classification;
	}

	public String getDescriptionTitle() {
		return descriptionTitle;
	}

	public void setDescriptionTitle(String descriptionTitle) {
		this.descriptionTitle = descriptionTitle;
	}
}
