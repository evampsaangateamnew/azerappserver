/**
 * 
 */
package com.evampsaanga.azerfon.models.tariffdetails.postpaid.individual.details;

import com.evampsaanga.azerfon.models.tariffdetails.prepaid.klass.details.InternationalPeak;

/**
 * @author Evamp & Saanga
 *
 */
public class IndividualDetails {
	private String detailsLabel;
	private IndividualDestination destination;
	private InternationalPeak internationalOnPeak;
	private InternationalPeak internationalOffPeak;

	public IndividualDetails() {
		this.destination = new IndividualDestination();
		this.internationalOnPeak = new InternationalPeak();
		this.internationalOffPeak = new InternationalPeak();
	}

	public IndividualDestination getDestination() {
		return destination;
	}

	public void setDestination(IndividualDestination destination) {
		this.destination = destination;
	}

	public InternationalPeak getInternationalOnPeak() {
		return internationalOnPeak;
	}

	public void setInternationalOnPeak(InternationalPeak internationalOnPeak) {
		this.internationalOnPeak = internationalOnPeak;
	}

	public InternationalPeak getInternationalOffPeak() {
		return internationalOffPeak;
	}

	public void setInternationalOffPeak(InternationalPeak internationalOffPeak) {
		this.internationalOffPeak = internationalOffPeak;
	}

	public String getDetailsLabel() {
		return detailsLabel;
	}

	public void setDetailsLabel(String detailsLabel) {
		this.detailsLabel = detailsLabel;
	}

}
