/**
 * 
 */
package com.evampsaanga.azerfon.models.tariffdetails.postpaid.corporate.price;

import com.evampsaanga.azerfon.models.tariffdetails.prepaid.cin.HeaderCall;

/**
 * @author Evamp & Saanga
 *
 */
public class CorporatePrice {
	private String priceLabel;
	private HeaderCall call;
	private CorporateSMS sms;
	private CorporateInternet internet;

	public HeaderCall getCall() {
		return call;
	}

	public void setCall(HeaderCall call) {
		this.call = call;
	}

	public CorporateSMS getSms() {
		return sms;
	}

	public void setSms(CorporateSMS sms) {
		this.sms = sms;
	}

	public CorporateInternet getInternet() {
		return internet;
	}

	public void setInternet(CorporateInternet internet) {
		this.internet = internet;
	}

	public String getPriceLabel() {
		return priceLabel;
	}

	public void setPriceLabel(String priceLabel) {
		this.priceLabel = priceLabel;
	}

}
