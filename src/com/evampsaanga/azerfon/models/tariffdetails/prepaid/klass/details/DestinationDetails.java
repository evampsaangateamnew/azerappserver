/**
 * 
 */
package com.evampsaanga.azerfon.models.tariffdetails.prepaid.klass.details;

import java.util.ArrayList;
import java.util.List;

import com.evampsaanga.azerfon.models.tariffdetails.GenericAttributes;

/**
 * @author Evamp & Saanga
 *
 */
public class DestinationDetails {

	private String iconName;
	private String title;
	private String titleValue;
	List<GenericAttributes> attributes;

	public DestinationDetails() {
		this.attributes = new ArrayList<>();
	}

	public String getIconName() {
		return iconName;
	}

	public void setIconName(String iconName) {
		this.iconName = iconName;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getTitleValue() {
		return titleValue;
	}

	public void setTitleValue(String titleValue) {
		this.titleValue = titleValue;
	}

	public List<GenericAttributes> getAttributes() {
		return attributes;
	}

	public void setAttributes(List<GenericAttributes> attributes) {
		this.attributes = attributes;
	}

}
