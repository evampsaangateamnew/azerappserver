/**
 * 
 */
package com.evampsaanga.azerfon.models.tariffdetails.prepaid.klass;

/**
 * @author Evamp & Saanga
 *
 */
public class KlassHeaderAttributes {
	private String iconName;
	private String title;
	private String value;
	private String metrics;

	public String getIconName() {
		return iconName;
	}

	public void setIconName(String iconName) {
		this.iconName = iconName;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getMetrics() {
		return metrics;
	}

	public void setMetrics(String metrics) {
		this.metrics = metrics;
	}

}
