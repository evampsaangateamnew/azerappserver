/**
 * 
 */
package com.evampsaanga.azerfon.models.tariffdetails.prepaid.klass.details;

/**
 * @author Evamp & Saanga
 *
 */
public class InternationalPeak {
	private String iconName;
	private String title;
	private String description;

	public String getIconName() {
		return iconName;
	}

	public void setIconName(String iconName) {
		this.iconName = iconName;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

}
