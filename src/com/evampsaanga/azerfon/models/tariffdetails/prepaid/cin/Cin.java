/**
 * 
 */
package com.evampsaanga.azerfon.models.tariffdetails.prepaid.cin;

import com.evampsaanga.azerfon.models.tariffdetails.TariffDetailsSection;
import com.evampsaanga.azerfon.models.tariffdetails.prepaid.cin.description.Description;

/**
 * @author Evamp & Saanga
 *
 */
public class Cin {

	private CinHeader header;
	private TariffDetailsSection details;
	private Description description;

	public Cin() {
		this.header = new CinHeader();
		this.details = new TariffDetailsSection();
		this.description = new Description();
	}

	public CinHeader getHeader() {
		return header;
	}

	public void setHeader(CinHeader header) {
		this.header = header;
	}

	public TariffDetailsSection getDetails() {
		return details;
	}

	public void setDetails(TariffDetailsSection details) {
		this.details = details;
	}

	public Description getDescription() {
		return description;
	}

	public void setDescription(Description description) {
		this.description = description;
	}

}
