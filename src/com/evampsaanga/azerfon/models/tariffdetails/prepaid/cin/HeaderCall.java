/**
 * 
 */
package com.evampsaanga.azerfon.models.tariffdetails.prepaid.cin;

import java.util.ArrayList;
import java.util.List;

import com.evampsaanga.azerfon.models.tariffdetails.CallAttributes;

/**
 * @author Evamp & Saanga
 *
 */
public class HeaderCall {
	private String iconName;
	private String title;
	private String titleValueLeft;
	private String titleValueRight;
	private String priceTemplate;
	List<CallAttributes> attributes;

	public HeaderCall() {
		attributes = new ArrayList<>();
	}

	public String getIconName() {
		return iconName;
	}

	public void setIconName(String iconName) {
		this.iconName = iconName;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getTitleValueLeft() {
		return titleValueLeft;
	}

	public void setTitleValueLeft(String titleValueLeft) {
		this.titleValueLeft = titleValueLeft;
	}

	public String getTitleValueRight() {
		return titleValueRight;
	}

	public void setTitleValueRight(String titleValueRight) {
		this.titleValueRight = titleValueRight;
	}

	public String getPriceTemplate() {
		return priceTemplate;
	}

	public void setPriceTemplate(String priceTemplate) {
		this.priceTemplate = priceTemplate;
	}

	public List<CallAttributes> getAttributes() {
		return attributes;
	}

	public void setAttributes(List<CallAttributes> attributes) {
		this.attributes = attributes;
	}

}
