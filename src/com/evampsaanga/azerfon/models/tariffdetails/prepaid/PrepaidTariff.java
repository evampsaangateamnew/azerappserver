/**
 * 
 */
package com.evampsaanga.azerfon.models.tariffdetails.prepaid;

import java.util.List;

import com.evampsaanga.azerfon.models.tariffdetails.prepaid.cin.Cin;
import com.evampsaanga.azerfon.models.tariffdetails.prepaid.klass.Klass;

/**
 * @author Evamp & Saanga
 *
 */
public class PrepaidTariff {
	private List<Klass> klass;
	private List<Cin> cin;

	public List<Klass> getKlass() {
		return klass;
	}

	public void setKlass(List<Klass> klass) {
		this.klass = klass;
	}

	public List<Cin> getCin() {
		return cin;
	}

	public void setCin(List<Cin> cin) {
		this.cin = cin;
	}

}
