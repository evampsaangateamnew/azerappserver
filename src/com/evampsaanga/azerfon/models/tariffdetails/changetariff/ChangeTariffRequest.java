/**
 * 
 */
package com.evampsaanga.azerfon.models.tariffdetails.changetariff;

import com.evampsaanga.azerfon.common.utilities.BaseRequest;

/**
 * @author Evamp & Saanga
 *
 */
public class ChangeTariffRequest extends BaseRequest {
	private String offeringId;
	private String tariffName;
	private String actionType;

	public String getOfferingId() {
		return offeringId;
	}

	public void setOfferingId(String offeringId) {
		this.offeringId = offeringId;
	}

	public String getTariffName() {
		return tariffName;
	}

	public void setTariffName(String tariffName) {
		this.tariffName = tariffName;
	}

	@Override
	public String toString() {
		return "ChangeTariffRequest [offeringId=" + offeringId + ", tariffName=" + tariffName + ", getLang()="
				+ getLang() + ", getiP()=" + getiP() + ", getChannel()=" + getChannel() + ", getMsisdn()=" + getMsisdn()
				+ "]";
	}

	public String getActionType() {
		return actionType;
	}

	public void setActionType(String actionType) {
		this.actionType = actionType;
	}

}
