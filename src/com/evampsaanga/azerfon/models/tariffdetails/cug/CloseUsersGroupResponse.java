package com.evampsaanga.azerfon.models.tariffdetails.cug;

import java.util.ArrayList;

public class CloseUsersGroupResponse{
	
	CloseUserGroupData GroupData;
	private ArrayList<CloseUsersGroupData> users;
	private String isLastPage;

	public CloseUserGroupData getGroupData() {
		return GroupData;
	}

	public void setGroupData(CloseUserGroupData groupData) {
		GroupData = groupData;
	}

	public ArrayList<CloseUsersGroupData> getUsers() {
		return users;
	}

	public void setUsers(ArrayList<CloseUsersGroupData> users) {
		this.users = users;
	}

	public String getIsLastPage() {
		return isLastPage;
	}

	public void setIsLastPage(String isLastPage) {
		this.isLastPage = isLastPage;
	}

	
	

}
