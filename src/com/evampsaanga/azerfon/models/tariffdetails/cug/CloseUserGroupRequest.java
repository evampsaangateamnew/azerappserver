package com.evampsaanga.azerfon.models.tariffdetails.cug;

import com.evampsaanga.azerfon.common.utilities.BaseRequest;

public class CloseUserGroupRequest extends BaseRequest{

	
	private String virtualCorpCode;
	private String type;
	private String pageNum;
	//private String accountCode;
	private String limitUsers;
	private String searchUsers;
	private String groupId;

	public String getvirtualCorpCode() {
		return virtualCorpCode;
	}

	public void setvirtualCorpCode(String virtualCorpCode) {
		this.virtualCorpCode = virtualCorpCode;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getPageNum() {
		return pageNum;
	}

	public void setPageNum(String pageNum) {
		this.pageNum = pageNum;
	}

	

	public String getLimitUsers() {
		return limitUsers;
	}

	public void setLimitUsers(String limitUsers) {
		this.limitUsers = limitUsers;
	}

	public String getSearchUsers() {
		return searchUsers;
	}

	public void setSearchUsers(String searchUsers) {
		this.searchUsers = searchUsers;
	}

	public String getGroupId() {
		return groupId;
	}

	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}


	
	

}
