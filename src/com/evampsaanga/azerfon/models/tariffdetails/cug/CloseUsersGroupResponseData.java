package com.evampsaanga.azerfon.models.tariffdetails.cug;

import java.util.ArrayList;

public class CloseUsersGroupResponseData {
	
	private ArrayList<CloseUsersGroupData> usersData;
	private String groupName;
	private String userCount;

	
	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public String getUserCount() {
		return userCount;
	}

	public void setUserCount(String userCount) {
		this.userCount = userCount;
	}

	public ArrayList<CloseUsersGroupData> getUsersData() {
		return usersData;
	}

	public void setUsersData(ArrayList<CloseUsersGroupData> usersData) {
		this.usersData = usersData;
	}

	
	
	
	

}
