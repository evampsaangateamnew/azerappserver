package com.evampsaanga.azerfon.models.tariffdetails.cug;

import java.util.ArrayList;

public class CloseUserGroupData {
	ArrayList<CloseUsersGroupResponseData> usersGroupData;

	public ArrayList<CloseUsersGroupResponseData> getUsersGroupData() {
		return usersGroupData;
	}

	public void setUsersGroupData(ArrayList<CloseUsersGroupResponseData> usersGroupData) {
		this.usersGroupData = usersGroupData;
	}
	
}
