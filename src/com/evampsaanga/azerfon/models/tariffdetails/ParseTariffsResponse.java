package com.evampsaanga.azerfon.models.tariffdetails;

import java.io.IOException;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import com.evampsaanga.azerfon.common.utilities.Constants;
import com.evampsaanga.azerfon.common.utilities.Utilities;
import com.evampsaanga.azerfon.models.supplementaryofferings.getsupplementaryofferings.ParseSupplementaryOfferingsResponse;
import com.evampsaanga.azerfon.models.supplementaryofferings.getsupplementaryofferings.commonofferings.TextWithOutTitle;
import com.evampsaanga.azerfon.models.supplementaryofferings.getsupplementaryofferings.commonofferings.TextWithPoints;
import com.evampsaanga.azerfon.models.supplementaryofferings.getsupplementaryofferings.commonofferings.TextWithTitle;
import com.evampsaanga.azerfon.models.supplementaryofferings.getsupplementaryofferings.commonofferings.details_and_description.DetailsAttributes;
import com.evampsaanga.azerfon.models.supplementaryofferings.getsupplementaryofferings.commonofferings.details_and_description.Price;
import com.evampsaanga.azerfon.models.tariffdetails.postpaid.corporate.HeaderAttribute;
import com.evampsaanga.azerfon.models.tariffdetails.prepaid.cin.HeaderCall;
import com.evampsaanga.azerfon.models.tariffdetails.prepaid.cin.description.Advantages;
import com.evampsaanga.azerfon.models.tariffdetails.prepaid.cin.description.Classification;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;

/**
 * @author Evamp & Saanga
 *
 */
public class ParseTariffsResponse {

	static Logger logger = Logger.getLogger(ParseTariffsResponse.class);

	public static TariffResponseList parseTariffResponseData(String msisdn, String valueFromJSON,
			TariffResponseList resData) throws JSONException, IOException {
		// Complete JSON
		Utilities.printDebugLog(msisdn + "-Parser has received JSON: " + valueFromJSON, logger);

		if (!valueFromJSON.isEmpty()) {

			/*
			 * If subscriber type in request of tariff API is prepaid, below section will be
			 * parsed.
			 */

			JSONArray valueJson = new JSONArray(valueFromJSON);
			if (valueJson.length() > 0) {

				// resData.setPostpaid(null);
				/* JSONObject prepaidObj = new JSONObject(valueFromJSON); */

				Utilities.printDebugLog(msisdn + "-ParserNew has ItemsArrayList Array List: " + valueJson, logger);
				resData = parseKlassData(msisdn, valueJson, resData);

				/*
				 * If subscriber type in request of tariff API is postpaid, below section will
				 * be parsed.
				 */
			} else {
				Utilities.printDebugLog(
						msisdn + "-Parser dont have either CORPORATRE or INDIVIDUAL or KLASS BUSINESS Data.", logger);
			}
		} else {
			Utilities.printDebugLog(msisdn + "-Parser has received EMPTY JSON. " + valueFromJSON, logger);
		}

		return resData;
	}

	// Parse CIN Section
	/*
	 * private static TariffResponseData parseCINData(String msisdn, JSONArray
	 * cinArrayList, TariffResponseData resData) throws JSONException, IOException {
	 * Utilities.printDebugLog(msisdn + "-CIN Array List is going to be parsed." +
	 * cinArrayList, logger); List<Cin> cinDataList = new ArrayList<>(); for (int i
	 * = 0; i < cinArrayList.length(); i++) { Cin cin = new Cin(); GenericAttributes
	 * attribute = new GenericAttributes();
	 * 
	 * As we have different templates for Call in header section and SMS|
	 * Destination therefore we have different call attributes.
	 * 
	 * 
	 * // Header section starts JSONObject header =
	 * cinArrayList.getJSONObject(i).getJSONObject("header");
	 * cin.getHeader().setId(header.getString("id"));
	 * cin.getHeader().setName(header.getString("name"));
	 * cin.getHeader().setBonusTitle(header.getString("tag"));
	 * cin.getHeader().setBonusIconName(header.getString("tagIcon")); cin.getHeader
	 * ().setBonusDescription(header.getString("shortDescription"));
	 * cin.getHeader().setOfferingId(header.getString("offeringId"));
	 * 
	 * 
	 * If tariff does not has any offering id then activation button will be
	 * disabled.
	 * 
	 * 
	 * if (Utilities.isOfferingIdExists(cin.getHeader().getOfferingId())) {
	 * cin.getHeader
	 * ().setSubscribable(cinArrayList.getJSONObject(i).getString("subscribable" ));
	 * } else { cin.getHeader().setSubscribable(Constants.
	 * SUPPLEMENTARY_TARIFF_DEFAULT_DISABLE_BUTTON); }
	 * 
	 * // Parsing CIN Call response in a separate method. cin.getHeader().setCall
	 * (parseHeaderCallSection(header.getJSONObject("Call")));
	 * 
	 * JSONObject headerSmsObj = header.getJSONObject("SMS");
	 * 
	 * cin.getHeader().getSms().setTitle(headerSmsObj.getString("smsLabel")); cin
	 * .getHeader().getSms().setTitleValue(headerSmsObj.getString("smsValue")); //
	 * cin.getHeader().getSms().setIconName(headerSmsObj.getString("smsIcon"));
	 * 
	 * // Temporarily Fix Hardcode icon for all CIN tariffs.
	 * cin.getHeader().getSms().setIconName("countrywideSMS");
	 * 
	 * attribute.setTitle(headerSmsObj.getString("smsCountryWideLabel"));
	 * attribute.setValue(headerSmsObj.getString("smsCountryWideValue")); if
	 * (Utilities.ifNotEmpty(attribute.getTitle(), attribute.getValue()))
	 * cin.getHeader().getSms().getAttributes().add(attribute);
	 * 
	 * attribute = new GenericAttributes();
	 * attribute.setTitle(headerSmsObj.getString("smsInternationalLabel"));
	 * attribute.setValue(headerSmsObj.getString("smsInternationalValue")); if
	 * (Utilities.ifNotEmpty(attribute.getTitle(), attribute.getValue()))
	 * cin.getHeader().getSms().getAttributes().add(attribute);
	 * 
	 * JSONObject headerInternetObj = header.getJSONObject("Internet");
	 * cin.getHeader
	 * ().getInternet().setTitle(headerInternetObj.getString("internetLabel"));
	 * cin.getHeader().getInternet().setTitleValue(headerInternetObj.getString(
	 * "internetValue"));
	 * 
	 * // cin.getHeader().getInternet().setIconName(headerInternetObj.getString(
	 * "internetIcon")); // Temporarily Fix Hardcode icon for all CIN tariffs.
	 * cin.getHeader().getInternet().setIconName("countrywideInternet");
	 * 
	 * cin.getHeader().getInternet().setSubTitle(headerInternetObj.getString(
	 * "internetDownloadAndUploadLabel")); cin.getHeader().getInternet()
	 * .setSubTitleValue
	 * (headerInternetObj.getString("internetDownloadAndUploadValue"));
	 * 
	 * // ------------------ Headers section ends ------------------//
	 * 
	 * // ------------------ Details section starts ------------------ // JSONObject
	 * detailsObj = cinArrayList.getJSONObject(i).getJSONObject("details");
	 * TariffDetailsSection detailSection = new TariffDetailsSection();
	 * detailSection = parseDetailSection(msisdn, detailsObj, detailSection);
	 * cin.setDetails(detailSection); // ------------------ Details section ends
	 * ------------------//
	 * 
	 * // Description sections starts JSONObject descriptionObj =
	 * cinArrayList.getJSONObject(i).getJSONObject("description");
	 * cin.getDescription
	 * ().setDescLabel(descriptionObj.getString("descriptionSectionLabel"));
	 * 
	 * cin.getDescription().setAdvantages(parseDescriptionAdvantages(descriptionObj
	 * ));
	 * 
	 * cin.getDescription().setClassification(parseDescriptionClassification(
	 * descriptionObj));
	 * 
	 * cinDataList.add(cin); // --------- ------ Description section
	 * ends---------------------//
	 * 
	 * } // for loop ends.
	 * 
	 * resData.getPrepaid().setCin(cinDataList);
	 * 
	 * return resData; }
	 */

	// Parse KLASS Section
	static TariffResponseList parseKlassData(String msisdn, JSONArray valueJsonArrayList,
			TariffResponseList resData) throws JSONException, JsonParseException, JsonMappingException, IOException {
		Utilities.printDebugLog(msisdn + "-Updated Main data Array List is going to be parsed." + valueJsonArrayList,
				logger);

		List<TariffResponseData> tariffResponseList = new ArrayList<TariffResponseData>();

		for (int j = 0; j < valueJsonArrayList.length(); j++) {
			TariffResponseData tariffResponseData = new TariffResponseData();
			tariffResponseData.setGroupType(valueJsonArrayList.getJSONObject(j).getString("groupType"));
			JSONArray klassArrayList = valueJsonArrayList.getJSONObject(j).getJSONArray("items");
			Utilities.printDebugLog(msisdn + "-Updated Items Array List in outer loop." + klassArrayList, logger);

			List<Items> klassDataList = new ArrayList<>();
			for (int i = 0; i < klassArrayList.length(); i++) {
				Items klass = new Items();

				List<Attributes> klassHeaderAttributesList = new ArrayList<>();

				// Header section starts
				JSONObject header = klassArrayList.getJSONObject(i).getJSONObject("header");
				klass.getHeader().setId(header.getString("id"));
				klass.getHeader().setName(header.getString("name"));
				klass.getHeader().setPriceLabel(header.getString("mrcLabel"));
				klass.getHeader().setPriceValue(header.getString("mrcValue"));
				klass.getHeader().setCurrency(header.getString("currency"));
				klass.getHeader().setOfferingId(header.getString("offeringId"));
				klass.getHeader().setTariffDescription(header.getString("tariffDescription"));
				klass.getHeader().setBonusLabel(header.getString("bonusLabel"));

				/*
				 * If tariff does not has any offering id then activation button will be
				 * disabled.
				 */

				if (Utilities.isOfferingIdExists(klass.getHeader().getOfferingId())) {
					klass.setSubscribable(klassArrayList.getJSONObject(i).getString("subscribable"));
				} else {
					klass.setSubscribable(Constants.SUPPLEMENTARY_TARIFF_DEFAULT_DISABLE_BUTTON);
				}

				// Header Call Attribute List
				
		
				JSONObject headerCall = header.getJSONObject("Call");
				Attributes klassHeaderAttributes = new Attributes();
			
				if (headerCall.getString("callLabel").equalsIgnoreCase("") && headerCall.getString("callValue").equalsIgnoreCase("")) {
				}
				else{
				 
					klassHeaderAttributes.setTitle(headerCall.getString("callLabel"));
					klassHeaderAttributes.setValue(headerCall.getString("callValue"));
					klassHeaderAttributes.setIconName(headerCall.getString("callIcon"));
					klassHeaderAttributes.setMetrics(headerCall.getString("callMetrics"));
					klassHeaderAttributesList.add(klassHeaderAttributes);
				}

				// Header SMS Attribute List
				JSONObject headerSMS = header.getJSONObject("SMS");
				klassHeaderAttributes = new Attributes();
				
				String smsLables=headerSMS.getString("smsLabel");
				String smsValues=headerSMS.getString("smsValue");
				if (smsLables.equalsIgnoreCase("") && smsValues.equalsIgnoreCase("")) {
				}
				else{
					    
					klassHeaderAttributes.setTitle(headerSMS.getString("smsLabel"));
					klassHeaderAttributes.setValue(headerSMS.getString("smsValue"));
					klassHeaderAttributes.setIconName(headerSMS.getString("smsIcon"));
					klassHeaderAttributes.setMetrics(headerSMS.getString("smsMetrics"));
					klassHeaderAttributesList.add(klassHeaderAttributes);
				}

				// Header Internet Attribute List
				JSONObject headerInternet = header.getJSONObject("Internet");
				klassHeaderAttributes = new Attributes();
                String internetLabels=headerInternet.getString("internetLabel");
                String internetValues=headerInternet.getString("internetValue");
				if (internetLabels.equalsIgnoreCase("") && internetValues.equalsIgnoreCase("")) {
				}
				else{
					Utilities.printDebugLog(msisdn + "-Internet NOT NULL-"+klass.getHeader().getOfferingId(), logger); 
					klassHeaderAttributes.setTitle(headerInternet.getString("internetLabel"));
					klassHeaderAttributes.setValue(headerInternet.getString("internetValue"));
					klassHeaderAttributes.setIconName(headerInternet.getString("internetIcon"));
					klassHeaderAttributes.setMetrics(headerInternet.getString("internetMetrics"));
					klassHeaderAttributesList.add(klassHeaderAttributes);
				}

				// Header Country Wide Attribute List
				JSONObject headerCountryWide = header.getJSONObject("countryWide");
				klassHeaderAttributes = new Attributes();
                 String countryWideLabels=headerCountryWide.getString("countryWideLabel");
                 String countryWideValues=headerCountryWide.getString("countryWideValue");
				if (countryWideLabels.equalsIgnoreCase("") && countryWideValues.equalsIgnoreCase("")) {
				}
				else{
					Utilities.printDebugLog(msisdn + "-CountryWide NOT NULL-"+klass.getHeader().getOfferingId(), logger);
					klassHeaderAttributes.setTitle(headerCountryWide.getString("countryWideLabel"));
					klassHeaderAttributes.setValue(headerCountryWide.getString("countryWideValue"));
					klassHeaderAttributes.setIconName(headerCountryWide.getString("countryWideIcon"));
					klassHeaderAttributes.setMetrics(headerCountryWide.getString("countryWideMetrics"));
					klassHeaderAttributesList.add(klassHeaderAttributes);
				}

				// Header Whatsapp Attribute List
				JSONObject headerWhatsapp = header.getJSONObject("Whatsapp");
				klassHeaderAttributes = new Attributes();
                String whatsAppLabels=headerWhatsapp.getString("whatsappLabel");
                String whatsappValues=headerWhatsapp.getString("whatsappValue");
				if (whatsAppLabels.equalsIgnoreCase("") && whatsappValues.equalsIgnoreCase("")) {
				}
				else{
					klassHeaderAttributes.setTitle(headerWhatsapp.getString("whatsappLabel"));
					klassHeaderAttributes.setValue(headerWhatsapp.getString("whatsappValue"));
					klassHeaderAttributes.setIconName(headerWhatsapp.getString("whatsappIcon"));
					klassHeaderAttributes.setMetrics(headerWhatsapp.getString("whatsappMetrics"));
					klassHeaderAttributesList.add(klassHeaderAttributes);
				}

				// Same structure and keys of bonuses objects parsing in a
				// single
				// method.
				Attributes klassHeaderAttributesBonus = parseBonusesObjects(header.getJSONObject("bonusSix"));
				if (Utilities.ifNotEmpty(klassHeaderAttributesBonus.getTitle(), klassHeaderAttributesBonus.getValue()))
					klassHeaderAttributesList.add(klassHeaderAttributesBonus);

				klassHeaderAttributesBonus = parseBonusesObjects(header.getJSONObject("bonusSeven"));
				if (Utilities.ifNotEmpty(klassHeaderAttributesBonus.getTitle(), klassHeaderAttributesBonus.getValue()))
					klassHeaderAttributesList.add(klassHeaderAttributesBonus);

				klassHeaderAttributesBonus = parseBonusesObjects(header.getJSONObject("bonusEight"));
				if (Utilities.ifNotEmpty(klassHeaderAttributesBonus.getTitle(), klassHeaderAttributesBonus.getValue()))
					klassHeaderAttributesList.add(klassHeaderAttributesBonus);

				klass.getHeader().setAttributes(klassHeaderAttributesList);
				// -------------------- Headers section ends
				// --------------------

				// Package Price Section starts
				JSONObject packagePrice = klassArrayList.getJSONObject(i).getJSONObject("packagePrices");
				logger.info("SMS Icon Pakcage Prices: " + packagePrice.toString());

				klass.getPackagePrice().setPackagePriceLabel(packagePrice.getString("packagePricesSectionLabel"));

				// Package Price Call
				List<GenericAttributes> genericAttributesList = new ArrayList<>();

				// Parsing KLASS CALL response in a separate method.
				klass.getPackagePrice().setCall(parseHeaderCallSection(packagePrice.getJSONObject("Call")));

				// Package Price SMS
				genericAttributesList = new ArrayList<>();
				JSONObject smsObj = packagePrice.getJSONObject("SMS");
				klass.getPackagePrice().getSms().setTitle(smsObj.getString("smsLabel"));
				klass.getPackagePrice().getSms().setTitleValue(smsObj.getString("smsValue"));
				klass.getPackagePrice().getSms().setIconName(smsObj.getString("smsIcon"));
				logger.info("SMS Icon: " + smsObj.getString("smsIcon") + " ID: "
						+ klassArrayList.getJSONObject(i).getJSONObject("header").getString("id"));

				GenericAttributes attribute = new GenericAttributes();

				if (Utilities.ifNotEmpty(smsObj.getString("smsCountryWideLabel"),
						smsObj.getString("smsCountryWideValue"))) {

					attribute.setTitle(smsObj.getString("smsCountryWideLabel"));
					attribute.setValue(smsObj.getString("smsCountryWideValue"));
					genericAttributesList.add(attribute);
				}

				attribute = new GenericAttributes();

				if (Utilities.ifNotEmpty(smsObj.getString("smsInternationalLabel"),
						smsObj.getString("smsInternationalValue"))) {

					attribute.setTitle(smsObj.getString("smsInternationalLabel"));
					attribute.setValue(smsObj.getString("smsInternationalValue"));
					genericAttributesList.add(attribute);
				}

				klass.getPackagePrice().getSms().setAttributes(genericAttributesList);
				if (klass.getPackagePrice().getSms().getTitleValue() == null
						|| klass.getPackagePrice().getSms().getTitle().isEmpty()
						|| klass.getPackagePrice().getSms().getTitleValue().equals("")) {
					klass.getPackagePrice().setSms(null);
				}
				// Package Price Internet

				/*
				 * subTitle : "Ödənmiş" subTitleValue : "0.01" internetDownloadAndUploadLabelA :
				 * "" internetDownloadAndUploadValueA : ""
				 */
				genericAttributesList = new ArrayList<>();
				JSONObject internetObj = packagePrice.getJSONObject("Internet");
				klass.getPackagePrice().getInternet().setTitle(internetObj.getString("internetLabel"));
				klass.getPackagePrice().getInternet().setTitleValue(internetObj.getString("internetValue"));
				klass.getPackagePrice().getInternet().setIconName(internetObj.getString("internetIcon"));

				GenericAttributes attributeint = new GenericAttributes();

				if (Utilities.ifNotEmpty(internetObj.getString("internetDownloadAndUploadLabel"),
						internetObj.getString("internetDownloadAndUploadValue"))) {

					attributeint.setTitle(internetObj.getString("internetDownloadAndUploadLabel"));
					attributeint.setValue(internetObj.getString("internetDownloadAndUploadValue"));
					genericAttributesList.add(attributeint);
				}

				attributeint = new GenericAttributes();

				if (Utilities.ifNotEmpty(internetObj.getString("internetDownloadAndUploadLabelA"),
						internetObj.getString("internetDownloadAndUploadValueA"))) {

					attributeint.setTitle(internetObj.getString("internetDownloadAndUploadLabelA"));
					attributeint.setValue(internetObj.getString("internetDownloadAndUploadValueA"));
					genericAttributesList.add(attributeint);
				}

				klass.getPackagePrice().getInternet().setAttributes(genericAttributesList);
				if (klass.getPackagePrice().getInternet().getTitleValue() == null
						|| klass.getPackagePrice().getInternet().getTitle().isEmpty()
						|| klass.getPackagePrice().getInternet().getTitleValue().equals("")) {
					klass.getPackagePrice().setInternet(null);
				}
				/*
				 * klass.getPackagePrice().getInternet().setSubTitle(internetObj.
				 * getString("internetDownloadAndUploadLabel"));
				 * klass.getPackagePrice().getInternet() .setSubTitleValue(internetObj
				 * .getString("internetDownloadAndUploadValue")); klass.getPackagePrice
				 * ().getInternet().setInternetDownloadAndUploadLabelA
				 * (internetObj.getString("internetDownloadAndUploadLabelA"));
				 * klass.getPackagePrice().getInternet(). setInternetDownloadAndUploadValueA
				 * (internetObj.getString("internetDownloadAndUploadValueA"));
				 */
				// ----- ----------- Package Price Section ends
				// -----------------

				// START OF Parsing Call_Payg response in a separate method.
				klass.getPackagePrice().setCallPayg(parseHeaderCallSection(packagePrice.getJSONObject("Call_Payg")));
				// END OF Parsing Call_Payg response in a separate method.
				// START Payg SMS
				genericAttributesList = new ArrayList<>();
				JSONObject smsPaygObj = packagePrice.getJSONObject("Sms_Payg");
				klass.getPackagePrice().getSmsPayg().setTitle(smsPaygObj.getString("smsLabel"));
				klass.getPackagePrice().getSmsPayg().setTitleValue(smsPaygObj.getString("smsValue"));
				klass.getPackagePrice().getSmsPayg().setIconName(smsPaygObj.getString("smsIcon"));

				attribute = new GenericAttributes();

				if (Utilities.ifNotEmpty(smsPaygObj.getString("smsCountryWideLabel"),
						smsPaygObj.getString("smsCountryWideValue"))) {

					attribute.setTitle(smsPaygObj.getString("smsCountryWideLabel"));
					attribute.setValue(smsPaygObj.getString("smsCountryWideValue"));
					genericAttributesList.add(attribute);
				}

				attribute = new GenericAttributes();

				if (Utilities.ifNotEmpty(smsPaygObj.getString("smsInternationalLabel"),
						smsPaygObj.getString("smsInternationalValue"))) {

					attribute.setTitle(smsPaygObj.getString("smsInternationalLabel"));
					attribute.setValue(smsPaygObj.getString("smsInternationalValue"));
					genericAttributesList.add(attribute);
				}

				klass.getPackagePrice().getSmsPayg().setAttributes(genericAttributesList);
				if (klass.getPackagePrice().getSmsPayg().getTitleValue() == null
						|| klass.getPackagePrice().getSmsPayg().getTitle().isEmpty()
						|| klass.getPackagePrice().getSmsPayg().getTitleValue().equals("")) {
					klass.getPackagePrice().setSmsPayg(null);
				}
				// END Payg SMS
				// Payg Internet
				genericAttributesList = new ArrayList<>();
				JSONObject internetPaygObj = packagePrice.getJSONObject("Internet_Payg");
				klass.getPackagePrice().getInternetPayg().setTitle(internetPaygObj.getString("internetLabel"));
				klass.getPackagePrice().getInternetPayg().setTitleValue(internetPaygObj.getString("internetValue"));
				klass.getPackagePrice().getInternetPayg().setIconName(internetPaygObj.getString("internetIcon"));

				GenericAttributes attributeintpayg = new GenericAttributes();

				if (Utilities.ifNotEmpty(internetObj.getString("internetDownloadAndUploadLabel"),
						internetObj.getString("internetDownloadAndUploadValue"))) {

					attributeintpayg.setTitle(internetObj.getString("internetDownloadAndUploadLabel"));
					attributeintpayg.setValue(internetObj.getString("internetDownloadAndUploadValue"));
					genericAttributesList.add(attribute);
				}

				attributeintpayg = new GenericAttributes();

				if (Utilities.ifNotEmpty(internetObj.getString("internetDownloadAndUploadLabelA"),
						internetObj.getString("internetDownloadAndUploadValueA"))) {

					attributeintpayg.setTitle(internetObj.getString("internetDownloadAndUploadLabelA"));
					attributeintpayg.setValue(internetObj.getString("internetDownloadAndUploadValueA"));
					genericAttributesList.add(attributeintpayg);
				}
				klass.getPackagePrice().getInternetPayg().setAttributes(genericAttributesList);

				if (klass.getPackagePrice().getInternetPayg().getTitleValue() == null
						|| klass.getPackagePrice().getInternetPayg().getTitleValue().isEmpty()
						|| klass.getPackagePrice().getInternetPayg().getTitleValue().equals("")) {
					klass.getPackagePrice().setInternetPayg(null);
				}
				// END OF InternetPayG
				// ---------------- Payg Section ends -----------------

				// ---------------- Details section starts -----------------

				JSONObject detailsObj = klassArrayList.getJSONObject(i).getJSONObject("details");
				TariffDetailsSection detailSection = new TariffDetailsSection();
				klass.setDetails(parseDetailSection(msisdn, detailsObj, detailSection));

				// -------------- Details Section ends -----------------

				klassDataList.add(klass);
			} // end of for loop items Array
			tariffResponseData.setItems(klassDataList);
			tariffResponseList.add(tariffResponseData);
		}
		resData.setTariffResponseList(tariffResponseList);
		return resData;
	}
	
	
	
	static TariffResponseV2 parseKlassDataV2(String msisdn, JSONArray klassArrayList,
			TariffResponseV2 resData) throws JSONException, JsonParseException, JsonMappingException, IOException {
		Utilities.printDebugLog(msisdn + "-Updated B2B parseKlassDataV2 method Main data Array List is going to be parsed." + klassArrayList,
				logger);

//		List<TariffResponseData> tariffResponseList = new ArrayList<TariffResponseData>();
//		TariffResponseV2 tariffResponseV2 =new TariffResponseV2();

		/*
		 * for (int j = 0; j < valueJsonArrayList.length(); j++) { TariffResponseData
		 * tariffResponseData = new TariffResponseData();
		 * tariffResponseData.setGroupType(valueJsonArrayList.getJSONObject(j).getString
		 * ("groupType")); JSONArray klassArrayList =
		 * valueJsonArrayList.getJSONObject(j).getJSONArray("items");
		 * Utilities.printDebugLog(msisdn + "-Updated Items Array List in outer loop." +
		 * klassArrayList, logger);
		 */

			ArrayList<TariffResponseDataV2> klassDataList = new ArrayList<TariffResponseDataV2>();
			for (int i = 0; i < klassArrayList.length(); i++) {
				TariffResponseDataV2 klass = new TariffResponseDataV2();

				List<Attributes> klassHeaderAttributesList = new ArrayList<>();
				
				klass.setGroupType(klassArrayList.getJSONObject(i).getString("groupType"));

				// Header section starts
				
				JSONObject header = klassArrayList.getJSONObject(i).getJSONObject("header");
				klass.getHeader().setId(header.getString("id"));
				klass.getHeader().setName(header.getString("name"));
				klass.getHeader().setPriceLabel(header.getString("mrcLabel"));
				klass.getHeader().setPriceValue(header.getString("mrcValue"));
				klass.getHeader().setCurrency(header.getString("currency"));
				klass.getHeader().setOfferingId(header.getString("offeringId"));
				klass.getHeader().setTariffDescription(header.getString("tariffDescription"));
				klass.getHeader().setBonusLabel(header.getString("bonusLabel"));

				/*
				 * If tariff does not has any offering id then activation button will be
				 * disabled.
				 */

//				if (Utilities.isOfferingIdExists(klass.getHeader().getOfferingId())) {
//					klass.setSubscribable(klassArrayList.getJSONObject(i).getString("subscribable"));
//				} else {
//					klass.setSubscribable(Constants.SUPPLEMENTARY_TARIFF_DEFAULT_DISABLE_BUTTON);
//				}

				// Header Call Attribute List
				
		
				JSONObject headerCall = header.getJSONObject("Call");
				Attributes klassHeaderAttributes = new Attributes();
			
				if (headerCall.getString("callLabel").equalsIgnoreCase("") && headerCall.getString("callValue").equalsIgnoreCase("")) {
				}
				else{
				 
					klassHeaderAttributes.setTitle(headerCall.getString("callLabel"));
					klassHeaderAttributes.setValue(headerCall.getString("callValue"));
					klassHeaderAttributes.setIconName(headerCall.getString("callIcon"));
					klassHeaderAttributes.setMetrics(headerCall.getString("callMetrics"));
					klassHeaderAttributesList.add(klassHeaderAttributes);
				}

				// Header SMS Attribute List
				JSONObject headerSMS = header.getJSONObject("SMS");
				klassHeaderAttributes = new Attributes();
				
				String smsLables=headerSMS.getString("smsLabel");
				String smsValues=headerSMS.getString("smsValue");
				if (smsLables.equalsIgnoreCase("") && smsValues.equalsIgnoreCase("")) {
				}
				else{
					    
					klassHeaderAttributes.setTitle(headerSMS.getString("smsLabel"));
					klassHeaderAttributes.setValue(headerSMS.getString("smsValue"));
					klassHeaderAttributes.setIconName(headerSMS.getString("smsIcon"));
					klassHeaderAttributes.setMetrics(headerSMS.getString("smsMetrics"));
					klassHeaderAttributesList.add(klassHeaderAttributes);
				}

				// Header Internet Attribute List
				JSONObject headerInternet = header.getJSONObject("Internet");
				klassHeaderAttributes = new Attributes();
                String internetLabels=headerInternet.getString("internetLabel");
                String internetValues=headerInternet.getString("internetValue");
				if (internetLabels.equalsIgnoreCase("") && internetValues.equalsIgnoreCase("")) {
				}
				else{
					Utilities.printDebugLog(msisdn + "-Internet NOT NULL-"+klass.getHeader().getOfferingId(), logger); 
					klassHeaderAttributes.setTitle(headerInternet.getString("internetLabel"));
					klassHeaderAttributes.setValue(headerInternet.getString("internetValue"));
					klassHeaderAttributes.setIconName(headerInternet.getString("internetIcon"));
					klassHeaderAttributes.setMetrics(headerInternet.getString("internetMetrics"));
					klassHeaderAttributesList.add(klassHeaderAttributes);
				}

				// Header Country Wide Attribute List
				JSONObject headerCountryWide = header.getJSONObject("countryWide");
				klassHeaderAttributes = new Attributes();
                 String countryWideLabels=headerCountryWide.getString("countryWideLabel");
                 String countryWideValues=headerCountryWide.getString("countryWideValue");
				if (countryWideLabels.equalsIgnoreCase("") && countryWideValues.equalsIgnoreCase("")) {
				}
				else{
					Utilities.printDebugLog(msisdn + "-CountryWide NOT NULL-"+klass.getHeader().getOfferingId(), logger);
					klassHeaderAttributes.setTitle(headerCountryWide.getString("countryWideLabel"));
					klassHeaderAttributes.setValue(headerCountryWide.getString("countryWideValue"));
					klassHeaderAttributes.setIconName(headerCountryWide.getString("countryWideIcon"));
					klassHeaderAttributes.setMetrics(headerCountryWide.getString("countryWideMetrics"));
					klassHeaderAttributesList.add(klassHeaderAttributes);
				}

				// Header Whatsapp Attribute List
				JSONObject headerWhatsapp = header.getJSONObject("Whatsapp");
				klassHeaderAttributes = new Attributes();
                String whatsAppLabels=headerWhatsapp.getString("whatsappLabel");
                String whatsappValues=headerWhatsapp.getString("whatsappValue");
				if (whatsAppLabels.equalsIgnoreCase("") && whatsappValues.equalsIgnoreCase("")) {
				}
				else{
					klassHeaderAttributes.setTitle(headerWhatsapp.getString("whatsappLabel"));
					klassHeaderAttributes.setValue(headerWhatsapp.getString("whatsappValue"));
					klassHeaderAttributes.setIconName(headerWhatsapp.getString("whatsappIcon"));
					klassHeaderAttributes.setMetrics(headerWhatsapp.getString("whatsappMetrics"));
					klassHeaderAttributesList.add(klassHeaderAttributes);
				}

				// Same structure and keys of bonuses objects parsing in a
				// single
				// method.
				Attributes klassHeaderAttributesBonus = parseBonusesObjects(header.getJSONObject("bonusSix"));
				if (Utilities.ifNotEmpty(klassHeaderAttributesBonus.getTitle(), klassHeaderAttributesBonus.getValue()))
					klassHeaderAttributesList.add(klassHeaderAttributesBonus);

				klassHeaderAttributesBonus = parseBonusesObjects(header.getJSONObject("bonusSeven"));
				if (Utilities.ifNotEmpty(klassHeaderAttributesBonus.getTitle(), klassHeaderAttributesBonus.getValue()))
					klassHeaderAttributesList.add(klassHeaderAttributesBonus);

				klassHeaderAttributesBonus = parseBonusesObjects(header.getJSONObject("bonusEight"));
				if (Utilities.ifNotEmpty(klassHeaderAttributesBonus.getTitle(), klassHeaderAttributesBonus.getValue()))
					klassHeaderAttributesList.add(klassHeaderAttributesBonus);

				klass.getHeader().setAttributes(klassHeaderAttributesList);
				// -------------------- Headers section ends
				// --------------------

				// Package Price Section starts
				JSONObject packagePrice = klassArrayList.getJSONObject(i).getJSONObject("packagePrices");
				logger.info("SMS Icon Pakcage Prices: " + packagePrice.toString());

				klass.getPackagePrice().setPackagePriceLabel(packagePrice.getString("packagePricesSectionLabel"));

				// Package Price Call
				List<GenericAttributes> genericAttributesList = new ArrayList<>();

				// Parsing KLASS CALL response in a separate method.
				klass.getPackagePrice().setCall(parseHeaderCallSection(packagePrice.getJSONObject("Call")));

				// Package Price SMS
				genericAttributesList = new ArrayList<>();
				JSONObject smsObj = packagePrice.getJSONObject("SMS");
				klass.getPackagePrice().getSms().setTitle(smsObj.getString("smsLabel"));
				klass.getPackagePrice().getSms().setTitleValue(smsObj.getString("smsValue"));
				klass.getPackagePrice().getSms().setIconName(smsObj.getString("smsIcon"));
				logger.info("SMS Icon: " + smsObj.getString("smsIcon") + " ID: "
						+ klassArrayList.getJSONObject(i).getJSONObject("header").getString("id"));

				GenericAttributes attribute = new GenericAttributes();

				if (Utilities.ifNotEmpty(smsObj.getString("smsCountryWideLabel"),
						smsObj.getString("smsCountryWideValue"))) {

					attribute.setTitle(smsObj.getString("smsCountryWideLabel"));
					attribute.setValue(smsObj.getString("smsCountryWideValue"));
					genericAttributesList.add(attribute);
				}

				attribute = new GenericAttributes();

				if (Utilities.ifNotEmpty(smsObj.getString("smsInternationalLabel"),
						smsObj.getString("smsInternationalValue"))) {

					attribute.setTitle(smsObj.getString("smsInternationalLabel"));
					attribute.setValue(smsObj.getString("smsInternationalValue"));
					genericAttributesList.add(attribute);
				}

				klass.getPackagePrice().getSms().setAttributes(genericAttributesList);
				if (klass.getPackagePrice().getSms().getTitleValue() == null
						|| klass.getPackagePrice().getSms().getTitle().isEmpty()
						|| klass.getPackagePrice().getSms().getTitleValue().equals("")) {
					klass.getPackagePrice().setSms(null);
				}
				// Package Price Internet

				/*
				 * subTitle : "Ödənmiş" subTitleValue : "0.01" internetDownloadAndUploadLabelA :
				 * "" internetDownloadAndUploadValueA : ""
				 */
				genericAttributesList = new ArrayList<>();
				JSONObject internetObj = packagePrice.getJSONObject("Internet");
				klass.getPackagePrice().getInternet().setTitle(internetObj.getString("internetLabel"));
				klass.getPackagePrice().getInternet().setTitleValue(internetObj.getString("internetValue"));
				klass.getPackagePrice().getInternet().setIconName(internetObj.getString("internetIcon"));

				GenericAttributes attributeint = new GenericAttributes();

				if (Utilities.ifNotEmpty(internetObj.getString("internetDownloadAndUploadLabel"),
						internetObj.getString("internetDownloadAndUploadValue"))) {

					attributeint.setTitle(internetObj.getString("internetDownloadAndUploadLabel"));
					attributeint.setValue(internetObj.getString("internetDownloadAndUploadValue"));
					genericAttributesList.add(attributeint);
				}

				attributeint = new GenericAttributes();

				if (Utilities.ifNotEmpty(internetObj.getString("internetDownloadAndUploadLabelA"),
						internetObj.getString("internetDownloadAndUploadValueA"))) {

					attributeint.setTitle(internetObj.getString("internetDownloadAndUploadLabelA"));
					attributeint.setValue(internetObj.getString("internetDownloadAndUploadValueA"));
					genericAttributesList.add(attributeint);
				}

				klass.getPackagePrice().getInternet().setAttributes(genericAttributesList);
				if (klass.getPackagePrice().getInternet().getTitleValue() == null
						|| klass.getPackagePrice().getInternet().getTitle().isEmpty()
						|| klass.getPackagePrice().getInternet().getTitleValue().equals("")) {
					klass.getPackagePrice().setInternet(null);
				}
				/*
				 * klass.getPackagePrice().getInternet().setSubTitle(internetObj.
				 * getString("internetDownloadAndUploadLabel"));
				 * klass.getPackagePrice().getInternet() .setSubTitleValue(internetObj
				 * .getString("internetDownloadAndUploadValue")); klass.getPackagePrice
				 * ().getInternet().setInternetDownloadAndUploadLabelA
				 * (internetObj.getString("internetDownloadAndUploadLabelA"));
				 * klass.getPackagePrice().getInternet(). setInternetDownloadAndUploadValueA
				 * (internetObj.getString("internetDownloadAndUploadValueA"));
				 */
				// ----- ----------- Package Price Section ends
				// -----------------

				// START OF Parsing Call_Payg response in a separate method.
				klass.getPackagePrice().setCallPayg(parseHeaderCallSection(packagePrice.getJSONObject("Call_Payg")));
				// END OF Parsing Call_Payg response in a separate method.
				// START Payg SMS
				genericAttributesList = new ArrayList<>();
				JSONObject smsPaygObj = packagePrice.getJSONObject("Sms_Payg");
				klass.getPackagePrice().getSmsPayg().setTitle(smsPaygObj.getString("smsLabel"));
				klass.getPackagePrice().getSmsPayg().setTitleValue(smsPaygObj.getString("smsValue"));
				klass.getPackagePrice().getSmsPayg().setIconName(smsPaygObj.getString("smsIcon"));

				attribute = new GenericAttributes();

				if (Utilities.ifNotEmpty(smsPaygObj.getString("smsCountryWideLabel"),
						smsPaygObj.getString("smsCountryWideValue"))) {

					attribute.setTitle(smsPaygObj.getString("smsCountryWideLabel"));
					attribute.setValue(smsPaygObj.getString("smsCountryWideValue"));
					genericAttributesList.add(attribute);
				}

				attribute = new GenericAttributes();

				if (Utilities.ifNotEmpty(smsPaygObj.getString("smsInternationalLabel"),
						smsPaygObj.getString("smsInternationalValue"))) {

					attribute.setTitle(smsPaygObj.getString("smsInternationalLabel"));
					attribute.setValue(smsPaygObj.getString("smsInternationalValue"));
					genericAttributesList.add(attribute);
				}

				klass.getPackagePrice().getSmsPayg().setAttributes(genericAttributesList);
				if (klass.getPackagePrice().getSmsPayg().getTitleValue() == null
						|| klass.getPackagePrice().getSmsPayg().getTitle().isEmpty()
						|| klass.getPackagePrice().getSmsPayg().getTitleValue().equals("")) {
					klass.getPackagePrice().setSmsPayg(null);
				}
				// END Payg SMS
				// Payg Internet
				genericAttributesList = new ArrayList<>();
				JSONObject internetPaygObj = packagePrice.getJSONObject("Internet_Payg");
				klass.getPackagePrice().getInternetPayg().setTitle(internetPaygObj.getString("internetLabel"));
				klass.getPackagePrice().getInternetPayg().setTitleValue(internetPaygObj.getString("internetValue"));
				klass.getPackagePrice().getInternetPayg().setIconName(internetPaygObj.getString("internetIcon"));

				GenericAttributes attributeintpayg = new GenericAttributes();

				if (Utilities.ifNotEmpty(internetObj.getString("internetDownloadAndUploadLabel"),
						internetObj.getString("internetDownloadAndUploadValue"))) {

					attributeintpayg.setTitle(internetObj.getString("internetDownloadAndUploadLabel"));
					attributeintpayg.setValue(internetObj.getString("internetDownloadAndUploadValue"));
					genericAttributesList.add(attribute);
				}

				attributeintpayg = new GenericAttributes();

				if (Utilities.ifNotEmpty(internetObj.getString("internetDownloadAndUploadLabelA"),
						internetObj.getString("internetDownloadAndUploadValueA"))) {

					attributeintpayg.setTitle(internetObj.getString("internetDownloadAndUploadLabelA"));
					attributeintpayg.setValue(internetObj.getString("internetDownloadAndUploadValueA"));
					genericAttributesList.add(attributeintpayg);
				}
				klass.getPackagePrice().getInternetPayg().setAttributes(genericAttributesList);

				if (klass.getPackagePrice().getInternetPayg().getTitleValue() == null
						|| klass.getPackagePrice().getInternetPayg().getTitleValue().isEmpty()
						|| klass.getPackagePrice().getInternetPayg().getTitleValue().equals("")) {
					klass.getPackagePrice().setInternetPayg(null);
				}
				// END OF InternetPayG
				// ---------------- Payg Section ends -----------------

				// ---------------- Details section starts -----------------

				JSONObject detailsObj = klassArrayList.getJSONObject(i).getJSONObject("details");
				TariffDetailsSection detailSection = new TariffDetailsSection();
				klass.setDetails(parseDetailSection(msisdn, detailsObj, detailSection));

				// -------------- Details Section ends -----------------

				klassDataList.add(klass);
				
				
				
			//} // end of for loop items Array
//				tariffResponseV2.setData(klassDataList);
//				tariffResponseList.add(klass)
//			tariffResponseList.add(tariffResponseData);
		}  // end of outer loop
//		tariffResponseV2.setData(klassDataList);
//		resData.setTariffResponseList(tariffResponseV2);
			
		resData.setData(klassDataList);
		
		return resData;
	}

	private static Attributes parseBonusesObjects(JSONObject headerObj) throws JSONException {
		Attributes klassHeaderAttributes = new Attributes();
		klassHeaderAttributes.setTitle(headerObj.getString("label"));
		klassHeaderAttributes.setValue(headerObj.getString("value"));
		klassHeaderAttributes.setIconName(headerObj.getString("icon"));
		klassHeaderAttributes.setMetrics(headerObj.getString("metrics"));
		return klassHeaderAttributes;
	}

	private static HeaderAttribute parseBonusesObjectsCorporate(JSONObject headerObj) throws JSONException {
		HeaderAttribute headerAttributes = new HeaderAttribute();
		headerAttributes.setTitle(headerObj.getString("label"));
		headerAttributes.setValue(headerObj.getString("value"));
		headerAttributes.setIconName(headerObj.getString("icon"));
		headerAttributes.setMetrics(headerObj.getString("metrics"));
		return headerAttributes;
	}

	// Tariff Details Section Parsing
	private static TariffDetailsSection parseDetailSection(String msisdn, JSONObject detailsObj,
			TariffDetailsSection detailsSection) throws JSONException, IOException {
		Utilities.printDebugLog(
				msisdn + "Tariff Details Parsing tariff details section with data:" + detailsObj.toString(), logger);
		detailsSection.setDetailLabel(detailsObj.getString("detailSectionLabel"));

		// ---------------- Details- Call Section ----------------

		// It is as same as of price in supplementary offerings.

		Price price = parseDetailsCallAndFragmentObjs(detailsObj.getJSONObject("Call"));
		if (Utilities.isPriceEmpty(price))
			detailsSection.getPrice().add(price);
		price = parseDetailsCallAndFragmentObjs(detailsObj.getJSONObject("fragmentB"));
		if (Utilities.isPriceEmpty(price))
			detailsSection.getPrice().add(price);
		price = parseDetailsCallAndFragmentObjs(detailsObj.getJSONObject("fragmentC"));
		if (Utilities.isPriceEmpty(price))
			detailsSection.getPrice().add(price);

		price = parseDetailsCallAndFragmentObjs(detailsObj.getJSONObject("fragmentD"));
		if (Utilities.isPriceEmpty(price))
			detailsSection.getPrice().add(price);

		// ---------------- Details- Destination Section ----------------
		// It is same as of rounding in supplementary offerings.
		JSONObject detailDestinationObj = detailsObj.getJSONObject("destination");

		String destinationLabels=detailDestinationObj.getString("destinationLabel");
		String destinationValues=detailDestinationObj.getString("destinationValue");
		if(destinationLabels.equalsIgnoreCase("") && destinationValues.equalsIgnoreCase(""))
		{
			
		}
		else{
		detailsSection.getRounding().setTitle(detailDestinationObj.getString("destinationLabel"));
		detailsSection.getRounding().setValue(detailDestinationObj.getString("destinationValue"));
		detailsSection.getRounding().setIconName(detailDestinationObj.getString("destinationIcon"));
		}
		DetailsAttributes attribute = new DetailsAttributes();
		attribute.setTitle(detailDestinationObj.getString("destinationInternationalLabel"));
		attribute.setValue(detailDestinationObj.getString("destinationInternationalValue"));
		if (Utilities.ifNotEmpty(attribute.getTitle(), attribute.getValue()))
			detailsSection.getRounding().getAttributeList().add(attribute);

		attribute = new DetailsAttributes();
		attribute.setTitle(detailDestinationObj.getString("destinationInternetLabel"));
		attribute.setValue(detailDestinationObj.getString("destinationInternetValue"));
		if (Utilities.ifNotEmpty(attribute.getTitle(), attribute.getValue()))
			detailsSection.getRounding().getAttributeList().add(attribute);

		attribute = new DetailsAttributes();
		attribute.setTitle(detailDestinationObj.getString("destinationOffnetLabel"));
		attribute.setValue(detailDestinationObj.getString("destinationOffnetValue"));
		if (Utilities.ifNotEmpty(attribute.getTitle(), attribute.getValue()))
			detailsSection.getRounding().getAttributeList().add(attribute);

		attribute = new DetailsAttributes();
		attribute.setTitle(detailDestinationObj.getString("destinationOnnetLabel"));
		attribute.setValue(detailDestinationObj.getString("destinationOnnetValue"));
		if (Utilities.ifNotEmpty(attribute.getTitle(), attribute.getValue()))
			detailsSection.getRounding().getAttributeList().add(attribute);

		// ----- Details- Title Sub Title And Description Section -----

		JSONObject detailTitleSubTitleObj = detailsObj.getJSONObject("titleSubtitle");
		detailsSection.getTitleSubTitleValueAndDesc().setTitle(detailTitleSubTitleObj.getString("titleLabel"));
		detailsSection.getTitleSubTitleValueAndDesc().setTitleIcon(detailTitleSubTitleObj.getString("titleIcon"));
		if (!detailsSection.getTitleSubTitleValueAndDesc().getTitle().isEmpty()) {

			detailsSection.getTitleSubTitleValueAndDesc()
					.setShortDesc(detailTitleSubTitleObj.getString("shortDescription"));

			GenericAttributes attributeSubTitle = new GenericAttributes();
			attributeSubTitle.setTitle(detailTitleSubTitleObj.getString("subtitle1Label"));
			attributeSubTitle.setValue(detailTitleSubTitleObj.getString("subtitle1Value"));
			if (Utilities.ifNotEmpty(attributeSubTitle.getTitle(), attributeSubTitle.getValue()))
				detailsSection.getTitleSubTitleValueAndDesc().getAttributesList().add(attributeSubTitle);

			attributeSubTitle = new GenericAttributes();
			attributeSubTitle.setTitle(detailTitleSubTitleObj.getString("subtitle2Label"));
			attributeSubTitle.setValue(detailTitleSubTitleObj.getString("subtitle2Value"));
			if (Utilities.ifNotEmpty(attributeSubTitle.getTitle(), attributeSubTitle.getValue()))
				detailsSection.getTitleSubTitleValueAndDesc().getAttributesList().add(attributeSubTitle);

			attributeSubTitle = new GenericAttributes();
			attributeSubTitle.setTitle(detailTitleSubTitleObj.getString("subtitle3Label"));
			attributeSubTitle.setValue(detailTitleSubTitleObj.getString("subtitle3Value"));
			if (Utilities.ifNotEmpty(attributeSubTitle.getTitle(), attributeSubTitle.getValue()))
				detailsSection.getTitleSubTitleValueAndDesc().getAttributesList().add(attributeSubTitle);

			attributeSubTitle = new GenericAttributes();
			attributeSubTitle.setTitle(detailTitleSubTitleObj.getString("subtitle4Label"));
			attributeSubTitle.setValue(detailTitleSubTitleObj.getString("subtitle4Value"));
			if (Utilities.ifNotEmpty(attributeSubTitle.getTitle(), attributeSubTitle.getValue()))
				detailsSection.getTitleSubTitleValueAndDesc().getAttributesList().add(attributeSubTitle);
		} else {
			detailsSection.setTitleSubTitleValueAndDesc(null);
		}
		// ------------------ Details- Advantages Section ------------------
		JSONObject detailAdvantagesObj = detailsObj.getJSONObject("advantages");
		detailsSection = parseAdvantagesSection(detailAdvantagesObj, detailsSection);

		// ------------------ Details- Time Section ------------------
		detailsSection.setTime(ParseSupplementaryOfferingsResponse.prepareTimeObject(detailsObj.getJSONObject("time")));

		// ------------------ Details- Date Section ------------------
		detailsSection.setDate(ParseSupplementaryOfferingsResponse.prepareDateObject(detailsObj.getJSONObject("date")));

		// ------------------ Details- Free Resources Section ------------------
		detailsSection.setFreeResourceValidity(ParseSupplementaryOfferingsResponse
				.prepareFreeResourceValidity(detailsObj.getJSONObject("freeResource")));

		// ------------------ Details- Roaming Section ------------------
		detailsSection.setRoamingDetails(
				ParseSupplementaryOfferingsResponse.parseRoamingData(msisdn, detailsObj.getJSONObject("roaming")));

		return detailsSection;
	}

	private static Price parseDetailsCallAndFragmentObjs(JSONObject detailCallObj) throws JSONException {
		Price priceObj = new Price();
		String callLabels=detailCallObj.getString("callLabel");
		String callValues=detailCallObj.getString("callValue");
		if(callLabels.equalsIgnoreCase("") && callValues.equalsIgnoreCase("") )
		{
			
		}
		else{
		priceObj.setTitle(detailCallObj.getString("callLabel"));
		priceObj.setValue(detailCallObj.getString("callValue"));
		priceObj.setIconName(detailCallObj.getString("callIcon"));
		}
		DetailsAttributes attribute = new DetailsAttributes();
		attribute.setTitle(detailCallObj.getString("callOffnetOutOfRegionLabel"));
		attribute.setValue(detailCallObj.getString("callOffnetOutOfRegionValue"));
		if (Utilities.ifNotEmpty(attribute.getTitle(), attribute.getValue()))
			priceObj.getAttributeList().add(attribute);

		attribute = new DetailsAttributes();
		attribute.setTitle(detailCallObj.getString("callOnnetOutOfRegionLabel"));
		attribute.setValue(detailCallObj.getString("callOnnetOutOfRegionValue"));
		if (Utilities.ifNotEmpty(attribute.getTitle(), attribute.getValue()))
			priceObj.getAttributeList().add(attribute);

		attribute = new DetailsAttributes();
		attribute.setTitle(detailCallObj.getString("titleThree"));
		attribute.setValue(detailCallObj.getString("valueThree"));
		if (Utilities.ifNotEmpty(attribute.getTitle(), attribute.getValue()))
			priceObj.getAttributeList().add(attribute);

		attribute = new DetailsAttributes();
		attribute.setTitle(detailCallObj.getString("titleFour"));
		attribute.setValue(detailCallObj.getString("valueFour"));
		if (Utilities.ifNotEmpty(attribute.getTitle(), attribute.getValue()))
			priceObj.getAttributeList().add(attribute);

		return priceObj;

	}

	public static TariffDetailsSection parseAdvantagesSection(JSONObject advantagesObj,
			TariffDetailsSection detailsSection) throws JSONException, SocketException {
		String textWithTitleLabel = advantagesObj.getString("advantagesLabel");
		String textWithTitleDescription = advantagesObj.getString("description");
		String titleIcon = advantagesObj.getString("advantagesIcon");
		Utilities.printDebugLog("parseAdvantagesSection Description Text-" + textWithTitleDescription, logger);
		Utilities.printDebugLog("parseAdvantagesSection Text title-" + textWithTitleLabel, logger);
		Utilities.printDebugLog("parseAdvantagesSection Description Text icon" + titleIcon, logger);

		Utilities.printDebugLog(Boolean.toString(textWithTitleDescription.contains(Constants.GET_BULLETS_BREAK_KEY)),
				logger);

		if (textWithTitleDescription.contains(Constants.GET_BULLETS_BREAK_KEY)) {
			Utilities.printDebugLog("Process Bullets-", logger);
			/*
			 * As per template samples, below parsing is for bullets( Text with points
			 * templates). In this template we show description in bullet points rather than
			 * a paragraph.
			 */

			TextWithPoints textWithPoints = new TextWithPoints();
			textWithPoints.setTitleIcon(titleIcon);
			textWithPoints.setTitle(textWithTitleLabel);
			textWithPoints.setPointsList(Utilities.processBullets(textWithTitleDescription));

			Utilities.printDebugLog("parseAdvantagesSection Description Text icon POINTS :" + titleIcon, logger);
			detailsSection.setTextWithPoints(textWithPoints);
			detailsSection.setTextWithTitle(null);
			detailsSection.setTextWithOutTitle(null);
		} else if (!textWithTitleLabel.trim().isEmpty()) {
			/*
			 * As per template samples, below parsing is for text with title template. In
			 * this template we show title and description.
			 */
			TextWithTitle textWithTitle = new TextWithTitle();
			textWithTitle.setTitle(textWithTitleLabel);
			textWithTitle.setText(textWithTitleDescription);
			textWithTitle.setTitleIcon(titleIcon);
			detailsSection.setTextWithTitle(textWithTitle);

			detailsSection.setTextWithPoints(null);
			detailsSection.setTextWithOutTitle(null);
		} else if (textWithTitleLabel.trim().isEmpty()
				&& !textWithTitleDescription.contains(Constants.GET_BULLETS_BREAK_KEY)) {
			/*
			 * As per template samples, below parsing is for text without title template. In
			 * this template we only show description.
			 */
			TextWithOutTitle textWithOutTitle = new TextWithOutTitle();
			textWithOutTitle.setDescription(textWithTitleDescription);
			textWithOutTitle.setTitleIcon(titleIcon);
			textWithOutTitle.setTitle(textWithTitleLabel);
			if (!textWithOutTitle.getDescription().isEmpty()) {
				detailsSection.setTextWithOutTitle(textWithOutTitle);
			} else {
				detailsSection.setTextWithOutTitle(null);
			}

			detailsSection.setTextWithTitle(null);
			detailsSection.setTextWithPoints(null);
		}
		return detailsSection;
	}

	public static HeaderCall parseHeaderCallSection(JSONObject headerCallObj) throws JSONException {

		HeaderCall callSectionObj = new HeaderCall();
		CallAttributes callAttributes = new CallAttributes();

		callSectionObj.setPriceTemplate(headerCallObj.getString("priceTemplate"));
		callSectionObj.setTitle(headerCallObj.getString("callLabel"));
		callSectionObj.setTitleValueLeft(headerCallObj.getString("callValueA"));
		callSectionObj.setTitleValueRight(headerCallObj.getString("callValueB"));
		callSectionObj.setIconName(headerCallObj.getString("callIcon"));

		if ((callSectionObj.getTitleValueLeft() == null || callSectionObj.getTitleValueLeft().isEmpty()
				|| callSectionObj.getTitleValueLeft().equals(""))
				&& (callSectionObj.getTitleValueRight() == null || callSectionObj.getTitleValueRight().isEmpty()
						|| callSectionObj.getTitleValueRight().equals(""))) {
			HeaderCall callAttributesempty = new HeaderCall();
			return callAttributesempty;
		}

		if (headerCallObj.getString("onnetLabel") != null && !headerCallObj.getString("onnetLabel").isEmpty()
				|| headerCallObj.getString("onnetValueA") != null && !headerCallObj.getString("onnetValueA").isEmpty()
				|| headerCallObj.getString("onnetValueB") != null
						&& !headerCallObj.getString("onnetValueB").isEmpty()) {

			callAttributes.setTitle(headerCallObj.getString("onnetLabel"));
			callAttributes.setValueLeft(headerCallObj.getString("onnetValueA"));
			callAttributes.setValueRight(headerCallObj.getString("onnetValueB"));

			callSectionObj.getAttributes().add(callAttributes);

		}

		if (headerCallObj.getString("offnetLabel") != null && !headerCallObj.getString("offnetLabel").isEmpty()
				|| headerCallObj.getString("offnetValueA") != null && !headerCallObj.getString("offnetValueA").isEmpty()
				|| headerCallObj.getString("offnetValueB") != null
						&& !headerCallObj.getString("offnetValueB").isEmpty()) {

			callAttributes = new CallAttributes();
			callAttributes.setTitle(headerCallObj.getString("offnetLabel"));
			callAttributes.setValueLeft(headerCallObj.getString("offnetValueA"));
			callAttributes.setValueRight(headerCallObj.getString("offnetValueB"));

			callSectionObj.getAttributes().add(callAttributes);

		}

		return callSectionObj;

	}

	private static Advantages parseDescriptionAdvantages(JSONObject descriptionObj) throws JSONException {

		Advantages advantages = new Advantages();
		advantages.setTitle(descriptionObj.getString("advantagesLabel"));
		advantages.setDescription(descriptionObj.getString("advantagesValue"));
		if (Utilities.ifNotEmpty(advantages.getTitle(), advantages.getDescription()))
			return advantages;
		else
			return null;
	}

	private static Classification parseDescriptionClassification(JSONObject descriptionObj) throws JSONException {

		Classification classification = new Classification();
		classification.setTitle(descriptionObj.getString("clarificationLabel"));
		classification.setDescription(descriptionObj.getString("clarificationValue"));
		if (Utilities.ifNotEmpty(classification.getTitle(), classification.getDescription()))
			return classification;
		else
			return null;
	}
}
