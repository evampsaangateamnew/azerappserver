package com.evampsaanga.azerfon.models.companyinvoice;

import com.evampsaanga.azerfon.common.utilities.BaseResponse;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)

public class CompanyInvoiceResponse extends BaseResponse {

    @JsonProperty("data")
    private InvoiceData data;

	public InvoiceData getData() {
		return data;
	}

	public void setData(InvoiceData data) {
		this.data = data;
	}	
}
