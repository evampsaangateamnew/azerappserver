/**
 * 
 */
package com.evampsaanga.azerfon.models.companyinvoice;

/**
 * @author HamzaFarooque
 *
 */
public class InvoiceModel {

	String label;
	String amount;

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

}
