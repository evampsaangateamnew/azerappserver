/**
 * 
 */
package com.evampsaanga.azerfon.models.generalservices.uploadimage;

import com.evampsaanga.azerfon.common.utilities.BaseResponse;

/**
 * @author Evamp & Saanga
 *
 */
public class UploadImageResponse extends BaseResponse {

	UploadImageResponseData data;

	public UploadImageResponseData getData() {
		return data;
	}

	public void setData(UploadImageResponseData data) {
		this.data = data;
	}
}
