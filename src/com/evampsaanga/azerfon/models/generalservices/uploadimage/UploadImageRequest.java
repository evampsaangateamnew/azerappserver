/**
 * 
 */
package com.evampsaanga.azerfon.models.generalservices.uploadimage;

import com.evampsaanga.azerfon.common.utilities.BaseRequest;

/**
 * @author Evamp & Saanga
 *
 */
public class UploadImageRequest extends BaseRequest {
	private String image;
	private String ext;
	private String actionType;

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public String getExt() {
		return ext;
	}

	public void setExt(String ext) {
		this.ext = ext;
	}

	public String getActionType() {
		return actionType;
	}

	public void setActionType(String actionType) {
		this.actionType = actionType;
	}

	@Override
	public String toString() {
		return "UploadImageRequest [ext=" + ext + ", actionType=" + actionType + "]";
	}

}
