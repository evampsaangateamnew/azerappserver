/**
 * 
 */
package com.evampsaanga.azerfon.models.generalservices.uploadimage;

/**
 * @author Evamp & Saanga
 *
 */
public class UploadImageResponseData {
	private String ImageURL;

	public String getImageURL() {
		return ImageURL;
	}

	public void setImageURL(String imageURL) {
		ImageURL = imageURL;
	}

}
