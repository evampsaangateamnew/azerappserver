package com.evampsaanga.azerfon.models.generalservices.exchangeservice;

import com.evampsaanga.azerfon.common.utilities.BaseResponse;

public class ExchangeServiceResponse extends BaseResponse {
	public ExchangeServiceData getData() {
		return data;
	}

	public void setData(ExchangeServiceData data) {
		this.data = data;
	}

	private ExchangeServiceData data=new ExchangeServiceData();

}
