package com.evampsaanga.azerfon.models.generalservices.exchangeservice;

import com.evampsaanga.azerfon.common.utilities.BaseResponse;

public class ExchangeServicesValuesResponse extends BaseResponse {
	private ExchangeServicesValuesData data=new ExchangeServicesValuesData();

	public ExchangeServicesValuesData getData() {
		return data;
	}

	public void setData(ExchangeServicesValuesData data) {
		this.data = data;
	}

}
