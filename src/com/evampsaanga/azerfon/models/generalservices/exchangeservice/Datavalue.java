
package com.evampsaanga.azerfon.models.generalservices.exchangeservice;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "data",
    "voice",
    "dataUnit",
    "voiceUnit"
})
public class Datavalue {

    @JsonProperty("data")
    private String data;
    @JsonProperty("voice")
    private String voice;
    @JsonProperty("dataUnit")
    private String dataUnit;
    @JsonProperty("voiceUnit")
    private String voiceUnit;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("data")
    public String getData() {
        return data;
    }

    @JsonProperty("data")
    public void setData(String data) {
        this.data = data;
    }

    @JsonProperty("voice")
    public String getVoice() {
        return voice;
    }

    @JsonProperty("voice")
    public void setVoice(String voice) {
        this.voice = voice;
    }

    @JsonProperty("dataUnit")
    public String getDataUnit() {
        return dataUnit;
    }

    @JsonProperty("dataUnit")
    public void setDataUnit(String dataUnit) {
        this.dataUnit = dataUnit;
    }

    @JsonProperty("voiceUnit")
    public String getVoiceUnit() {
        return voiceUnit;
    }

    @JsonProperty("voiceUnit")
    public void setVoiceUnit(String voiceUnit) {
        this.voiceUnit = voiceUnit;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
