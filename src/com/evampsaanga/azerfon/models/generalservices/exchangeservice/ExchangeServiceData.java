package com.evampsaanga.azerfon.models.generalservices.exchangeservice;

public class ExchangeServiceData {

	private String message;

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	@Override
	public String toString() {
		return "ExchangeServiceData [message=" + message + "]";
	}

}
