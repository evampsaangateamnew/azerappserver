
package com.evampsaanga.azerfon.models.generalservices.exchangeservice;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "returnMsg",
    "returnCode",
    "isServiceEnabled",
    "count",
    "voicevalues",
    "datavalues"
})
public class ExchangeServicesValuesEsbResponse {

    @JsonProperty("returnMsg")
    private String returnMsg;
    @JsonProperty("returnCode")
    private String returnCode;
    @JsonProperty("isServiceEnabled")
    private String isServiceEnabled;
    @JsonProperty("count")
    private String count;
    @JsonProperty("voicevalues")
    private List<Voicevalue> voicevalues = null;
    @JsonProperty("datavalues")
    private List<Datavalue> datavalues = null;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("returnMsg")
    public String getReturnMsg() {
        return returnMsg;
    }

    @JsonProperty("returnMsg")
    public void setReturnMsg(String returnMsg) {
        this.returnMsg = returnMsg;
    }

    @JsonProperty("returnCode")
    public String getReturnCode() {
        return returnCode;
    }

    @JsonProperty("returnCode")
    public void setReturnCode(String returnCode) {
        this.returnCode = returnCode;
    }

    @JsonProperty("isServiceEnabled")
    public String getIsServiceEnabled() {
        return isServiceEnabled;
    }

    @JsonProperty("isServiceEnabled")
    public void setIsServiceEnabled(String isServiceEnabled) {
        this.isServiceEnabled = isServiceEnabled;
    }

    @JsonProperty("count")
    public String getCount() {
        return count;
    }

    @JsonProperty("count")
    public void setCount(String count) {
        this.count = count;
    }

    @JsonProperty("voicevalues")
    public List<Voicevalue> getVoicevalues() {
        return voicevalues;
    }

    @JsonProperty("voicevalues")
    public void setVoicevalues(List<Voicevalue> voicevalues) {
        this.voicevalues = voicevalues;
    }

    @JsonProperty("datavalues")
    public List<Datavalue> getDatavalues() {
        return datavalues;
    }

    @JsonProperty("datavalues")
    public void setDatavalues(List<Datavalue> datavalues) {
        this.datavalues = datavalues;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
