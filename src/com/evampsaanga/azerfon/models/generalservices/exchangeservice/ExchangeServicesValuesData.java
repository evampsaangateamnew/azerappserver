package com.evampsaanga.azerfon.models.generalservices.exchangeservice;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ExchangeServicesValuesData {
	
	@JsonProperty("isServiceEnabled")
    private String isServiceEnabled;
    @JsonProperty("count")
    private String count;
    @JsonProperty("voicevalues")
    private List<Voicevalue> voicevalues = null;
    @JsonProperty("datavalues")
    private List<Datavalue> datavalues = null;
	
	 public String getIsServiceEnabled() {
		return isServiceEnabled;
	}
	public void setIsServiceEnabled(String isServiceEnabled) {
		this.isServiceEnabled = isServiceEnabled;
	}
	public String getCount() {
		return count;
	}
	public void setCount(String count) {
		this.count = count;
	}
	public List<Voicevalue> getVoicevalues() {
		return voicevalues;
	}
	public void setVoicevalues(List<Voicevalue> voicevalues) {
		this.voicevalues = voicevalues;
	}
	public List<Datavalue> getDatavalues() {
		return datavalues;
	}
	public void setDatavalues(List<Datavalue> datavalues) {
		this.datavalues = datavalues;
	}
	
	

}
