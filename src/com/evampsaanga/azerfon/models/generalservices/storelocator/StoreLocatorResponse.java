package com.evampsaanga.azerfon.models.generalservices.storelocator;

import com.evampsaanga.azerfon.common.utilities.BaseResponse;

public class StoreLocatorResponse extends BaseResponse {
	private StoreLocatorResponseData data;

	public StoreLocatorResponseData getData() {
		return data;
	}

	public void setData(StoreLocatorResponseData data) {
		this.data = data;
	}

}
