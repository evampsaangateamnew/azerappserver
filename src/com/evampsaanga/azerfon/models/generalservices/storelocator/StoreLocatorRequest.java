package com.evampsaanga.azerfon.models.generalservices.storelocator;

import com.evampsaanga.azerfon.common.utilities.BaseRequest;

public class StoreLocatorRequest extends BaseRequest {

	@Override
	public String toString() {
		return "StoreLocatorRequest [getLang()=" + getLang() + ", getiP()=" + getiP() + ", getChannel()=" + getChannel()
				+ ", getMsisdn()=" + getMsisdn() + "]";
	}

}
