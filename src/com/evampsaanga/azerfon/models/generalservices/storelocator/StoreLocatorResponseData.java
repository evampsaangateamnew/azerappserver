package com.evampsaanga.azerfon.models.generalservices.storelocator;

import java.util.List;

public class StoreLocatorResponseData {
	
	private List<String> type;
	private List<String> city;
	private List<Stores> stores;
	
	public List<String> getType() {
		return type;
	}
	public void setType(List<String> type) {
		this.type = type;
	}
	public List<String> getCity() {
		return city;
	}
	public void setCity(List<String> city) {
		this.city = city;
	}
	public List<Stores> getStores() {
		return stores;
	}
	public void setStores(List<Stores> stores) {
		this.stores = stores;
	}
	
	
	
}
