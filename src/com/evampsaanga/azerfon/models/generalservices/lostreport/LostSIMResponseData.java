/**
 * 
 */
package com.evampsaanga.azerfon.models.generalservices.lostreport;

public class LostSIMResponseData {

	private String message;

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

}
