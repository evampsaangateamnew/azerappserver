/**
 * 
 */
package com.evampsaanga.azerfon.models.generalservices.lostreport;

import com.evampsaanga.azerfon.common.utilities.BaseResponse;

public class ReportLostSIMResponse extends BaseResponse {
	
	private LostSIMResponseData data;

	public LostSIMResponseData getData() {
		return data;
	}

	public void setData(LostSIMResponseData data) {
		this.data = data;
	}
	
}
