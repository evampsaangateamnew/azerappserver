/**
 * 
 */
package com.evampsaanga.azerfon.models.generalservices.lostreport;

import com.evampsaanga.azerfon.common.utilities.BaseRequest;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)

public class ReportLostSIMRequest extends BaseRequest {

	private String reasonCode;
	
	
	
	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonProperty("virtualCorpCode")
	String virtualCorpCode="";
	
	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonProperty("type")
	String type="";
	
	
	
	
	public String getReasonCode() {
		return reasonCode;
	}

	public void setReasonCode(String reasonCode) {
		this.reasonCode = reasonCode;
	}

	@Override
	public String toString() {
		return "ReportLostSIMRequest [reasonCode=" + reasonCode + "]";
	}

	public String getVirtualCorpCode() {
		return virtualCorpCode;
	}

	public void setVirtualCorpCode(String virtualCorpCode) {
		this.virtualCorpCode = virtualCorpCode;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	
	

}
