/**
 * 
 */
package com.evampsaanga.azerfon.models.generalservices.faqs;

import com.evampsaanga.azerfon.common.utilities.BaseRequest;

/**
 * 
 * @author Evamp & Saanga
 *
 */
public class FAQSRequest extends BaseRequest {

	@Override
	public String toString() {
		return "FAQSRequest [getLang()=" + getLang() + ", getiP()=" + getiP() + ", getChannel()=" + getChannel()
				+ ", getMsisdn()=" + getMsisdn() + "]";
	}

}
