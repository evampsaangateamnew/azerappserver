/**
 * 
 */
package com.evampsaanga.azerfon.models.generalservices.sendinternetsettings;

import com.evampsaanga.azerfon.common.utilities.BaseResponse;

/**
 * @author Evamp & Saanga
 *
 */
public class SendInternetSettingsResponse extends BaseResponse {

	private SendInternetSettingsResponseData data;

	public SendInternetSettingsResponseData getData() {
		return data;
	}

	public void setData(SendInternetSettingsResponseData data) {
		this.data = data;
	}

}
