package com.evampsaanga.azerfon.models.generalservices.getroaming;

import com.evampsaanga.azerfon.common.utilities.BaseResponse;

public class GetRoamingResponse extends BaseResponse {
	
	com.evampsaanga.azerfon.models.generalservices.getroaming.Data data=new Data();

	public com.evampsaanga.azerfon.models.generalservices.getroaming.Data getData() {
		return data;
	}

	public void setData(
			com.evampsaanga.azerfon.models.generalservices.getroaming.Data data) {
		this.data = data;
	}
	

}
