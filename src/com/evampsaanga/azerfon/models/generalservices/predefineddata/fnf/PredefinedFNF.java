/**
 * 
 */
package com.evampsaanga.azerfon.models.generalservices.predefineddata.fnf;

/**
 * @author Evamp & Saanga
 *
 */
public class PredefinedFNF {
	private String maxFNFCount;
	private String fnFAllowed;

	public String getMaxFNFCount() {
		return maxFNFCount;
	}

	public void setMaxFNFCount(String maxFNFCount) {
		this.maxFNFCount = maxFNFCount;
	}

	public String getFnFAllowed() {
		return fnFAllowed;
	}

	public void setFnFAllowed(String fnFAllowed) {
		this.fnFAllowed = fnFAllowed;
	}
	
}
