/**
 * 
 */
package com.evampsaanga.azerfon.models.generalservices.predefineddata;

import java.util.HashMap;
import java.util.List;

import com.evampsaanga.azerfon.common.utilities.BaseResponse;
import com.evampsaanga.azerfon.models.generalservices.predefineddata.fnf.PredefinedFNF;
import com.evampsaanga.azerfon.models.generalservices.predefineddata.topup.TOPUP;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * @author Evamp & Saanga
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class PredefinedDataResponse extends BaseResponse {

    private TOPUP topup;
    private PredefinedFNF fnf;
    private Tnc tnc = new Tnc();
    private ExchangeService exchangeService;
    private NarTv narTv;
    private RedirectionLinks redirectionLinks = new RedirectionLinks();
    private String liveChat;
    private List<TariffMigrationPrices> tariffMigrationPrices;
    private List<String> cevirEligibleTariffs;
    private String cevirErrorMessage;
    private HashMap<String, String> goldenPayMessages;

    public String getCevirErrorMessage() {
	return cevirErrorMessage;
    }

    public void setCevirErrorMessage(String cevirErrorMessage) {
	this.cevirErrorMessage = cevirErrorMessage;
    }

    public List<String> getCevirEligibleTariffs() {
	return cevirEligibleTariffs;
    }

    public void setCevirEligibleTariffs(List<String> cevirEligibleTariffs) {
	this.cevirEligibleTariffs = cevirEligibleTariffs;
    }

    public TOPUP getTopup() {
	return topup;
    }

    public void setTopup(TOPUP topup) {
	this.topup = topup;
    }

    public PredefinedFNF getFnf() {
	return fnf;
    }

    public void setFnf(PredefinedFNF fnf) {
	this.fnf = fnf;
    }

    public Tnc getTnc() {
	return tnc;
    }

    public void setTnc(Tnc tnc) {
	this.tnc = tnc;
    }

    public ExchangeService getExchangeService() {
	return exchangeService;
    }

    public void setExchangeService(ExchangeService exchangeService) {
	this.exchangeService = exchangeService;
    }

    public NarTv getNarTv() {
	return narTv;
    }

    public void setNarTv(NarTv narTv) {
	this.narTv = narTv;
    }

    public RedirectionLinks getRedirectionLinks() {
	return redirectionLinks;
    }

    public void setRedirectionLinks(RedirectionLinks redirectionLinks) {
	this.redirectionLinks = redirectionLinks;
    }

    public String getLiveChat() {
	return liveChat;
    }

    public void setLiveChat(String liveChat) {
	this.liveChat = liveChat;
    }

    public List<TariffMigrationPrices> getTariffMigrationPrices() {
	return tariffMigrationPrices;
    }

    public void setTariffMigrationPrices(List<TariffMigrationPrices> tariffMigrationPrices) {
	this.tariffMigrationPrices = tariffMigrationPrices;
    }

    @Override
    public String toString() {
	return "PredefinedDataResponse [topup=" + topup + ", fnf=" + fnf + ", tnc=" + tnc + ", exchangeService="
		+ exchangeService + ", narTv=" + narTv + ", redirectionLinks=" + redirectionLinks + ", liveChat="
		+ liveChat + ", tariffMigrationPrices=" + tariffMigrationPrices + "]";
    }

	public HashMap<String, String> getGoldenPayMessages() {
		return goldenPayMessages;
	}

	public void setGoldenPayMessages(HashMap<String, String> goldenPayMessages) {
		this.goldenPayMessages = goldenPayMessages;
	}

}
