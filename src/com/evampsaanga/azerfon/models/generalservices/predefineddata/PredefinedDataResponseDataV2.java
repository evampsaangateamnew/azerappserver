package com.evampsaanga.azerfon.models.generalservices.predefineddata;

import java.util.List;

import com.evampsaanga.azerfon.models.dashboardservice.DocumentTypesData;
import com.evampsaanga.azerfon.models.dashboardservice.GroupNamesData;
import com.fasterxml.jackson.annotation.JsonInclude;

public class PredefinedDataResponseDataV2 {
	private String content;
	private String firstPopup;
	private String lateOnPopup;
	private String popupTitle;
	private String popupContent;
	private String changeLimitLabel;
	private String groupNamePartPay;
	private String groupNamePayBySubs;
	private List<GroupNamesData> groupName;
	private String changeGroupLabel;
	private List<DocumentTypesData> documentTypes;
	
	private RedirectionLinks redirectionLinks = new RedirectionLinks();
	
	public RedirectionLinks getRedirectionLinks() {
		return redirectionLinks;
	}

	public void setRedirectionLinks(RedirectionLinks redirectionLinks) {
		this.redirectionLinks = redirectionLinks;
	}

	@JsonInclude(JsonInclude.Include.NON_NULL)
	private String liveChat;
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private String userCount;;
	

	public String getUserCount() {
		return userCount;
	}

	public void setUserCount(String userCount) {
		this.userCount = userCount;
	}

	public String getLiveChat() {
		return liveChat;
	}

	public void setLiveChat(String liveChat) {
		this.liveChat = liveChat;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getFirstPopup() {
		return firstPopup;
	}

	public void setFirstPopup(String firstPopup) {
		this.firstPopup = firstPopup;
	}

	public String getLateOnPopup() {
		return lateOnPopup;
	}

	public void setLateOnPopup(String lateOnPopup) {
		this.lateOnPopup = lateOnPopup;
	}

	public String getPopupTitle() {
		return popupTitle;
	}

	public void setPopupTitle(String popupTitle) {
		this.popupTitle = popupTitle;
	}

	public String getPopupContent() {
		return popupContent;
	}

	public void setPopupContent(String popupContent) {
		this.popupContent = popupContent;
	}

	public String getChangeLimitLabel() {
		return changeLimitLabel;
	}

	public void setChangeLimitLabel(String changeLimitLabel) {
		this.changeLimitLabel = changeLimitLabel;
	}

	public String getGroupNamePartPay() {
		return groupNamePartPay;
	}

	public void setGroupNamePartPay(String groupNamePartPay) {
		this.groupNamePartPay = groupNamePartPay;
	}

	public String getGroupNamePayBySubs() {
		return groupNamePayBySubs;
	}

	public void setGroupNamePayBySubs(String groupNamePayBySubs) {
		this.groupNamePayBySubs = groupNamePayBySubs;
	}

	public List<GroupNamesData> getGroupName() {
		return groupName;
	}

	public void setGroupName(List<GroupNamesData> groupName) {
		this.groupName = groupName;
	}

	public String getChangeGroupLabel() {
		return changeGroupLabel;
	}

	public void setChangeGroupLabel(String changeGroupLabel) {
		this.changeGroupLabel = changeGroupLabel;
	}

	public List<DocumentTypesData> getDocumentTypes() {
		return documentTypes;
	}

	public void setDocumentTypes(List<DocumentTypesData> documentTypes) {
		this.documentTypes = documentTypes;
	}

}
