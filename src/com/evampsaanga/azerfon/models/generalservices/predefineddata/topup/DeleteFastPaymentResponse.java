package com.evampsaanga.azerfon.models.generalservices.predefineddata.topup;

import com.evampsaanga.azerfon.common.utilities.BaseResponse;

public class DeleteFastPaymentResponse extends BaseResponse{

	@Override
	public String toString() {
		return "DeleteFastPaymentResponse [toString()=" + super.toString() + "]";
	}
	
}
