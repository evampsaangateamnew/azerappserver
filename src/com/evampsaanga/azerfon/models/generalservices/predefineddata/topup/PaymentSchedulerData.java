package com.evampsaanga.azerfon.models.generalservices.predefineddata.topup;

import org.apache.log4j.Logger;

public class PaymentSchedulerData {

	private String amount;
	private String billingCycle; // 1.daily, 2. weekly, 3. monthly
	private String startDate;
	private String recurrenceNumber;
	private String savedCardId;
	private int id;
	private String msisdn;
	private String nextScheduledDate;
	private String recurrentDay;
	private String cardType;

	Logger logger = Logger.getLogger(PaymentSchedulerData.class);

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getMsisdn() {
		return msisdn;
	}

	public void setMsisdn(String msisdn) {
		this.msisdn = msisdn;
	}

	public String getNextScheduledDate() {
		return nextScheduledDate;
	}

	public void setNextScheduledDate(String nextScheduledDate) {
		this.nextScheduledDate = nextScheduledDate;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getBillingCycle() {
		return billingCycle;
	}

	public void setBillingCycle(String billingCycle) {
		this.billingCycle = billingCycle;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getRecurrenceNumber() {
		return recurrenceNumber;
	}

	public void setRecurrenceNumber(String recurrenceNumber) {
		this.recurrenceNumber = recurrenceNumber;
	}

	public String getSavedCardId() {
		return savedCardId;
	}

	public void setSavedCardId(String savedCardId) {
		this.savedCardId = savedCardId;
	}

	public String getRecurrentDay() {
		return recurrentDay;
	}

	public void setRecurrentDay(String recurrentDay) {
		this.recurrentDay = recurrentDay;
	}

	public String getCardType() {
		return cardType;
	}

	public void setCardType(String cardType) {
		if (cardType != null && cardType.equalsIgnoreCase("v"))
			cardType = "Visa";
		if (cardType != null && cardType.equalsIgnoreCase("m"))
			cardType = "Master";
		this.cardType = cardType;
	}

	@Override
	public String toString() {
		return "PaymentSchedulerData [amount=" + amount + ", billingCycle=" + billingCycle + ", startDate=" + startDate
				+ ", recurrenceNumber=" + recurrenceNumber + ", savedCardId=" + savedCardId + ", id=" + id + ", msisdn="
				+ msisdn + ", nextScheduledDate=" + nextScheduledDate + ", recurrentDay=" + recurrentDay + ", cardType="
				+ cardType + ", logger=" + logger + "]";
	}

	
	
}
