package com.evampsaanga.azerfon.models.generalservices.predefineddata.topup;

import com.evampsaanga.azerfon.common.utilities.BaseRequest;

public class AddPaymentSchedulerRequest extends BaseRequest {

//	private PaymentSchedulerData data;

	private String amount;
	private String billingCycle; // 1.daily, 2. weekly, 3. monthly
	private String startDate;
	private String recurrenceNumber;
	private String recurrenceFrequency;
	private String savedCardId;

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getBillingCycle() {
		return billingCycle;
	}

	public void setBillingCycle(String billingCycle) {
		this.billingCycle = billingCycle;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getRecurrenceNumber() {
		return recurrenceNumber;
	}

	public void setRecurrenceNumber(String recurrenceNumber) {
		this.recurrenceNumber = recurrenceNumber;
	}

	public String getSavedCardId() {
		return savedCardId;
	}

	public void setSavedCardId(String savedCardId) {
		this.savedCardId = savedCardId;
	}

	public String getRecurrenceFrequency() {
		return recurrenceFrequency;
	}

	public void setRecurrenceFrequency(String recurrenceFrequency) {
		this.recurrenceFrequency = recurrenceFrequency;
	}

	@Override
	public String toString() {
		return "AddPaymentSchedulerRequest [amount=" + amount + ", billingCycle=" + billingCycle + ", startDate="
				+ startDate + ", recurrenceNumber=" + recurrenceNumber + ", recurrenceFrequency=" + recurrenceFrequency
				+ ", savedCardId=" + savedCardId + ", toString()=" + super.toString() + "]";
	}
	
	
}
