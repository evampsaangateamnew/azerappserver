package com.evampsaanga.azerfon.models.generalservices.predefineddata.topup;

import com.evampsaanga.azerfon.common.utilities.BaseRequest;

public class PaymentKeyRequest extends BaseRequest {

	private int cardType;
	private String amount;
	private String saved;
	private String topupNumber;

	public int getCardType() {
		return cardType;
	}

	public void setCardType(int cardType) {
		this.cardType = cardType;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getTopupNumber() {
		return topupNumber;
	}

	public String getSaved() {
		return saved;
	}

	public void setSaved(String saved) {
		this.saved = saved;
	}

	public void setTopupNumber(String topupNumber) {
		this.topupNumber = topupNumber;
	}

	@Override
	public String toString() {
		return "PaymentKeyRequest [cardType=" + cardType + ", amount=" + amount + ", isSaved=" + saved
				+ ", topupNumber=" + topupNumber + ", toString()=" + super.toString() + "]";
	}

}
