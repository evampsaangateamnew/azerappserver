package com.evampsaanga.azerfon.models.generalservices.predefineddata.topup;

import com.evampsaanga.azerfon.common.utilities.BaseRequest;

public class DeleteSavedCardRequest extends BaseRequest{
	private String savedCardId;

	
	public String getId() {
		return savedCardId;
	}

	public void setId(String id) {
		this.savedCardId = id;
	}

	@Override
	public String toString() {
		return "DeleteSavedCardRequest [savedCardId=" + savedCardId + ", toString()=" + super.toString() + "]";
	}
}
