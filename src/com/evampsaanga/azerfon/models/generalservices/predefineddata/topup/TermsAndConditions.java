/**
 * 
 */
package com.evampsaanga.azerfon.models.generalservices.predefineddata.topup;

import java.util.List;

/**
 * 
 * @author Evamp & Saanga
 *
 */
public class TermsAndConditions {

	List<TermsAndConditionsData> prepaid;

	public List<TermsAndConditionsData> getPrepaid() {
		return prepaid;
	}

	public void setPrepaid(List<TermsAndConditionsData> prepaid) {
		this.prepaid = prepaid;
	}



}
