package com.evampsaanga.azerfon.models.generalservices.predefineddata.topup;

import com.evampsaanga.azerfon.common.utilities.BaseRequest;

public class DeleteFastPaymentRequest extends BaseRequest{
	private String fastPaymentId;

	
	public String getId() {
		return fastPaymentId;
	}

	public void setId(String id) {
		this.fastPaymentId = id;
	}

	@Override
	public String toString() {
		return "DeleteFastPaymentRequest [fastPaymentId=" + fastPaymentId + ", toString()=" + super.toString() + "]";
	}
	
}
