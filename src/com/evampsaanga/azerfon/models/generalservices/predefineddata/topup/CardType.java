package com.evampsaanga.azerfon.models.generalservices.predefineddata.topup;

public class CardType {
	private int key;
	private String value;
	
	public CardType(int key, String value) {
		this.key = key;
		this.value = value;
	}

	public int getKey() {
		return key;
	}

	public String getValue() {
		return value;
	}

	@Override
	public String toString() {
		return "CardType [key=" + key + ", value=" + value + ", toString()=" + super.toString() + "]";
	}
	
	
}
