/**
 * 
 */
package com.evampsaanga.azerfon.models.generalservices.predefineddata.topup;

/**
 * @author Evamp & Saanga
 *
 *
 */
public class GetLoan {
	SelectAmount selectAmount;

	public SelectAmount getSelectAmount() {
		return selectAmount;
	}

	public void setSelectAmount(SelectAmount selectAmount) {
		this.selectAmount = selectAmount;
	}

}
