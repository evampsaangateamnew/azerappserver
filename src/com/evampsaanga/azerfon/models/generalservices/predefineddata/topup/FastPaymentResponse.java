package com.evampsaanga.azerfon.models.generalservices.predefineddata.topup;

import java.util.List;

import com.evampsaanga.azerfon.common.utilities.BaseResponse;


public class FastPaymentResponse extends BaseResponse {

	
	private FastPaymentResponseData data;

	public FastPaymentResponseData getData() {
		return data;
	}

	public void setData(FastPaymentResponseData data) {
		this.data = data;
	}

	@Override
	public String toString() {
		return "FastPaymentResponse [data=" + data + ", toString()=" + super.toString() + "]";
	}


}
