package com.evampsaanga.azerfon.models.generalservices.predefineddata.topup;

import com.evampsaanga.azerfon.common.utilities.BaseResponse;

public class PaymentKeyResponse extends BaseResponse{

	
	private PaymentKeyResponseData data;

	public PaymentKeyResponseData getData() {
		return data;
	}

	public void setData(PaymentKeyResponseData data) {
		this.data = data;
	}

	@Override
	public String toString() {
		return "PaymentKeyResponse [data=" + data + ", toString()=" + super.toString() + "]";
	}


	
}
