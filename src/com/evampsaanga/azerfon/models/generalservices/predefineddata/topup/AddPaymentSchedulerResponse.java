package com.evampsaanga.azerfon.models.generalservices.predefineddata.topup;

import com.evampsaanga.azerfon.common.utilities.BaseResponse;

public class AddPaymentSchedulerResponse extends BaseResponse{

	@Override
	public String toString() {
		return "AddPaymentSchedulerResponse [toString()=" + super.toString() + "]";
	}

}
