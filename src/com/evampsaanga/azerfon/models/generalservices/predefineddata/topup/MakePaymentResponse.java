package com.evampsaanga.azerfon.models.generalservices.predefineddata.topup;

import com.evampsaanga.azerfon.common.utilities.BaseResponse;


public class MakePaymentResponse extends BaseResponse {

	private MakePaymentResponseData data;

	public MakePaymentResponseData getData() {
		return data;
	}

	public void setData(MakePaymentResponseData data) {
		this.data = data;
	}

	@Override
	public String toString() {
		return "MakePaymentResponse [data=" + data + ", toString()=" + super.toString() + "]";
	}

}
