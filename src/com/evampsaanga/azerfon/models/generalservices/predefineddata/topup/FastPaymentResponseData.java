package com.evampsaanga.azerfon.models.generalservices.predefineddata.topup;

import java.util.List;

public class FastPaymentResponseData {
	
	
	private List<FastPaymentDetails> fastPaymentDetails;

	public List<FastPaymentDetails> getFastPaymentDetails() {
		return fastPaymentDetails;
	}

	public void setFastPaymentDetails(List<FastPaymentDetails> fastPaymentDetails) {
		this.fastPaymentDetails = fastPaymentDetails;
	}

	@Override
	public String toString() {
		return "FastPaymentResponseData [fastPaymentDetails=" + fastPaymentDetails + ", toString()=" + super.toString()
				+ "]";
	}
	
}
