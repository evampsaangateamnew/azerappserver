package com.evampsaanga.azerfon.models.generalservices.predefineddata.topup;

import java.util.List;

public class ScheduledPaymentsResponseData {

	private List<PaymentSchedulerData> data;
	
	public List<PaymentSchedulerData> getData() {
		return data;
	}


	public void setData(List<PaymentSchedulerData> data) {
		this.data = data;
	}


	@Override
	public String toString() {
		return "ScheduledPaymentsResponseData [data=" + data + "]";
	}
	
	
}
