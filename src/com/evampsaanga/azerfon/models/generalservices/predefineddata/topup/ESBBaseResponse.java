package com.evampsaanga.azerfon.models.generalservices.predefineddata.topup;

import com.evampsaanga.azerfon.common.utilities.LogsReport;
import com.fasterxml.jackson.annotation.JsonIgnore;


public class ESBBaseResponse {
    @JsonIgnore
    private String callStatus;
    @JsonIgnore
    private String resultCode;
    @JsonIgnore
    private String resultDesc;
    @JsonIgnore
    private LogsReport logsReport;

    public ESBBaseResponse() {
	logsReport = new LogsReport();
    }

    public String getCallStatus() {
	return callStatus;
    }

    public void setCallStatus(String callStatus) {
	this.callStatus = callStatus;
    }

    public String getResultCode() {
	return resultCode;
    }

    public void setResultCode(String resultCode) {
	this.resultCode = resultCode;
    }

    public String getResultDesc() {
	return resultDesc;
    }

    public void setResultDesc(String resultDesc) {
	this.resultDesc = resultDesc;
    }

    public LogsReport getLogsReport() {
	return logsReport;
    }

    public void setLogsReport(LogsReport logsReport) {
	this.logsReport = logsReport;
    }
}
