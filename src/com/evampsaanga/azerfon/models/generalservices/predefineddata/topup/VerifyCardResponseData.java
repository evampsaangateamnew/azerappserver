package com.evampsaanga.azerfon.models.generalservices.predefineddata.topup;

public class VerifyCardResponseData {

	private String returnCode;

	
	public String getReturnCode() {
		return returnCode;
	}

	public void setReturnCode(String returnCode) {
		this.returnCode = returnCode;
	}

	@Override
	public String toString() {
		return "VerifyCardResponseData [returnCode=" + returnCode + "]";
	}

}
