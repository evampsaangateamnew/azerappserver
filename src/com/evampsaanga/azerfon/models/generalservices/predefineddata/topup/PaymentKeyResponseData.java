package com.evampsaanga.azerfon.models.generalservices.predefineddata.topup;

public class PaymentKeyResponseData {

	private String paymentKey;

	
	public String getPaymentKey() {
		return paymentKey;
	}

	public void setPaymentKey(String paymentKey) {
		this.paymentKey = paymentKey;
	}

	@Override
	public String toString() {
		return "PaymentKeyResponseData [paymentKey=" + paymentKey + "]";
	}

}
