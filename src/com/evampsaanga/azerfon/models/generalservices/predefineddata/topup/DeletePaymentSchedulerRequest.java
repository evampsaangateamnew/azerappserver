package com.evampsaanga.azerfon.models.generalservices.predefineddata.topup;

import com.evampsaanga.azerfon.common.utilities.BaseRequest;

public class DeletePaymentSchedulerRequest extends BaseRequest {
	private String paymentSchedulerId;

	
	public String getId() {
		return paymentSchedulerId;
	}

	public void setId(String paymentSchedulerId) {
		this.paymentSchedulerId = paymentSchedulerId;
	}

	@Override
	public String toString() {
		return "DeletePaymentSchedulerRequest [paymentSchedulerId=" + paymentSchedulerId + ", toString()=" + super.toString() + "]";
	}

}
