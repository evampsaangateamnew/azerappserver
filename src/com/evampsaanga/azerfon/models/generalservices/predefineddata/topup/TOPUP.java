/**
 * 
 */
package com.evampsaanga.azerfon.models.generalservices.predefineddata.topup;

/**
 * @author Evamp & Saanga
 *
 */
public class TOPUP {
	private MoneyTransfer moneyTransfer;
	private GetLoan getLoan;
	
    //Adding Plastic Card Object
    private PlasticCard plasticCard;
    
	public MoneyTransfer getMoneyTransfer() {
		return moneyTransfer;
	}
	public void setMoneyTransfer(MoneyTransfer moneyTransfer) {
		this.moneyTransfer = moneyTransfer;
	}
	public GetLoan getGetLoan() {
		return getLoan;
	}
	public void setGetLoan(GetLoan getLoan) {
		this.getLoan = getLoan;
	}
	@Override
	public String toString() {
		return "TOPUP [moneyTransfer=" + moneyTransfer + ", getLoan=" + getLoan + ", plasticCard=" + plasticCard + "]";
	}
	public PlasticCard getPlasticCard() {
		return plasticCard;
	}
	public void setPlasticCard(PlasticCard plasticCard) {
		this.plasticCard = plasticCard;
	}
	
	

}
