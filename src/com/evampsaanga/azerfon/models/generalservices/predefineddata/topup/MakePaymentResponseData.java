package com.evampsaanga.azerfon.models.generalservices.predefineddata.topup;

public class MakePaymentResponseData {

	
	private String responseData;

	public String getResponseData() {
		return responseData;
	}

	public void setResponseData(String responseData) {
		this.responseData = responseData;
	}

	@Override
	public String toString() {
		return "MakePaymentResponseData [responseData=" + responseData + "]";
	}
	
}
