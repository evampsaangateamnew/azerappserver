package com.evampsaanga.azerfon.models.generalservices.predefineddata.topup;

import com.evampsaanga.azerfon.common.utilities.BaseResponse;

public class DeletePaymentSchedulerResponse extends BaseResponse {

	
	private String returnCode; //response data

	public String getReturnCode() {
		return returnCode;
	}

	public void setReturnCode(String data) {
		this.returnCode = data;
	}

	@Override
	public String toString() {
		return "DeletePaymentSchedulerResponse [returnCode=" + returnCode + ", toString()=" + super.toString() + "]";
	}

}
