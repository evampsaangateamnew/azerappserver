package com.evampsaanga.azerfon.models.generalservices.predefineddata.topup;

import java.util.List;

public class SavedCardResponseData {
	private List<CardDetails> cardDetails;
	private String lastAmount;

	
	public List<CardDetails> getCardDetails() {
		return cardDetails;
	}

	public void setCardDetails(List<CardDetails> cardDetails) {
		this.cardDetails = cardDetails;
	}

	public String getLastAmount() {
		return lastAmount;
	}

	public void setLastAmount(String lastAmount) {
		this.lastAmount = lastAmount;
	}

	@Override
	public String toString() {
		return "SavedCardResponseData [cardDetails=" + cardDetails + ", lastAmount=" + lastAmount + "]";
	}

	

}
