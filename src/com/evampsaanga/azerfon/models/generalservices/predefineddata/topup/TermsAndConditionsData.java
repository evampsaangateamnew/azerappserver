/**
 * 
 */
package com.evampsaanga.azerfon.models.generalservices.predefineddata.topup;

/**
 * @author Evamp & Saanga
 *
 *
 */
public class TermsAndConditionsData {
	private String text;
	private String amount;
	private String currency;

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}
}
