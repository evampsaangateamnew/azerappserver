package com.evampsaanga.azerfon.models.generalservices.predefineddata.topup;

import com.evampsaanga.azerfon.common.utilities.BaseResponse;


public class DeleteSavedCardResponse extends BaseResponse{

	@Override
	public String toString() {
		return "DeleteSavedCardResponse [toString()=" + super.toString() + "]";
	}
	
}
