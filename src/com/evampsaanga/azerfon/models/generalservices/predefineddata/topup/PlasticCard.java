package com.evampsaanga.azerfon.models.generalservices.predefineddata.topup;

import java.util.ArrayList;
import java.util.List;

public class PlasticCard {
	private List<CardType> cardTypes;

	public PlasticCard() {
		this.cardTypes = new ArrayList<>();
	}

	public List<CardType> getCardTypes() {
		return this.cardTypes;
	}

	public void setCardTypes(List<CardType> cardTypes) {
		this.cardTypes = cardTypes;
	}

	@Override
	public String toString() {
		return "PlasticCard [cardTypes=" + cardTypes + ", toString()=" + super.toString() + "]";
	}

}
