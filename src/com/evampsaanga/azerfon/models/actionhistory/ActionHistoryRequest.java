package com.evampsaanga.azerfon.models.actionhistory;

import com.evampsaanga.azerfon.common.utilities.BaseRequest;

public class ActionHistoryRequest extends BaseRequest {
	private String startDate;
	private String endDate;
	private String orderKey;
	

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public String getOrderKey() {
		return orderKey;
	}

	public void setOrderKey(String orderKey) {
		this.orderKey = orderKey;
	}

}
