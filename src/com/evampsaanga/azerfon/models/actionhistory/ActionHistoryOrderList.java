package com.evampsaanga.azerfon.models.actionhistory;

import java.util.ArrayList;

public class ActionHistoryOrderList {
	private ArrayList<ActionHistoryResponseData> orderList;

	public ArrayList<ActionHistoryResponseData> getOrderList() {
		return orderList;
	}

	public void setOrderList(ArrayList<ActionHistoryResponseData> orderList) {
		this.orderList = orderList;
	}
}
