/**
 * 
 */
package com.evampsaanga.azerfon.models.history.getusagedetails;

import com.evampsaanga.azerfon.common.utilities.BaseResponse;

/**
 * @author Evamp & Saanga
 *
 */

public class UsageDetailsResponse extends BaseResponse {
	private UsageDetailsResponseData data;

	public UsageDetailsResponseData getData() {
		return data;
	}

	public void setData(UsageDetailsResponseData data) {
		this.data = data;
	}
}
