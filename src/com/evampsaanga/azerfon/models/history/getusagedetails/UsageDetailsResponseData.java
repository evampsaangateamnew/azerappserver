/**
 * 
 */
package com.evampsaanga.azerfon.models.history.getusagedetails;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * @author Evamp & Saanga
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class UsageDetailsResponseData {
	private List<DetailRecord> records;
	private String pdfPath;
	public List<DetailRecord> getRecords() {
		return records;
	}

	public void setRecords(List<DetailRecord> records) {
		this.records = records;
	}

	public String getPdfPath() {
		return pdfPath;
	}

	public void setPdfPath(String pdfPath) {
		this.pdfPath = pdfPath;
	}
}
