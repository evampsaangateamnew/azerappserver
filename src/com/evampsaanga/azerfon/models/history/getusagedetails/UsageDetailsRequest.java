/**
 * 
 */
package com.evampsaanga.azerfon.models.history.getusagedetails;

import com.evampsaanga.azerfon.common.utilities.BaseRequest;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * @author Evamp & Saanga
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class UsageDetailsRequest extends BaseRequest {

	private String startDate;
	private String endDate;
	private String accountId;
	private String customerId;
	private String isDownloadPdf = null;
	private String isSendMail=null;
	private String mailTo=null;


	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public String getAccountId() {
		return accountId;
	}

	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	@Override
	public String toString() {
		return "UsageDetailsRequest [startDate=" + startDate + ", endDate=" + endDate + ", accountId=" + accountId
				+ ", customerId=" + customerId + "]";
	}

	public String getIsDownloadPdf() {
		return isDownloadPdf;
	}

	public void setIsDownloadPdf(String isDownloadPdf) {
		this.isDownloadPdf = isDownloadPdf;
	}

	public String getIsSendMail() {
		return isSendMail;
	}

	public void setIsSendMail(String isSendMail) {
		this.isSendMail = isSendMail;
	}

	public String getMailTo() {
		return mailTo;
	}

	public void setMailTo(String mailTo) {
		this.mailTo = mailTo;
	}



}
