/**
 * 
 */
package com.evampsaanga.azerfon.models.history.verifypin;

import com.evampsaanga.azerfon.common.utilities.BaseRequest;

/**
 * @author Evamp & Saanga
 *
 */
public class UsageDetailsVerifyPinRequest extends BaseRequest {

	private String accountId;
	private String pin;
	private String customerId;

	public String getAccountId() {
		return accountId;
	}

	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}

	public String getPin() {
		return pin;
	}

	public void setPin(String pin) {
		this.pin = pin;
	}

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	@Override
	public String toString() {
		return "UsageDetailsVerifyPinRequest [accountId=" + accountId + ", pin=" + pin + ", customerId=" + customerId
				+ "]";
	}

}
