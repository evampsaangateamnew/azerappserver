/**
 * 
 */
package com.evampsaanga.azerfon.models.history.getoperationshistory;

import com.evampsaanga.azerfon.common.utilities.BaseResponse;

/**
 * @author Evamp & Saanga
 *
 */

public class OperationsHistoryResponse extends BaseResponse {
	private OperationsHistoryResponseData data;

	public OperationsHistoryResponseData getData() {
		return data;
	}

	public void setData(OperationsHistoryResponseData data) {
		this.data = data;
	}

}
