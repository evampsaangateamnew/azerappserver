/**
 * 
 */
package com.evampsaanga.azerfon.models.history.getoperationshistory;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * @author Evamp & Saanga
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class OperationsHistoryResponseData {
	private List<OperationsHistoryRecords> records;

	public List<OperationsHistoryRecords> getRecords() {
		return records;
	}

	public void setRecords(List<OperationsHistoryRecords> records) {
		this.records = records;
	}
}
