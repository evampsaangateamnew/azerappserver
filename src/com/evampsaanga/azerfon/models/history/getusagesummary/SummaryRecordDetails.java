/**
 * 
 */
package com.evampsaanga.azerfon.models.history.getusagesummary;

import java.util.List;

/**
 * @author Evamp & Saanga
 *
 */
public class SummaryRecordDetails {
	private String name;
	private String totalUsage;
	private String totalCharge;
	private List<SummaryRecord> records;

	public String getTotalUsage() {
		return totalUsage;
	}

	public void setTotalUsage(String totalUsage) {
		this.totalUsage = totalUsage;
	}

	public String getTotalCharge() {
		return totalCharge;
	}

	public void setTotalCharge(String totalCharge) {
		this.totalCharge = totalCharge;
	}

	public List<SummaryRecord> getRecords() {
		return records;
	}

	public void setRecords(List<SummaryRecord> records) {
		this.records = records;
	}

	@Override
	public String toString() {
		return "SummaryRecordDetails [totalUsage=" + totalUsage + ", totalCharge=" + totalCharge + ", records="
				+ records + "]";
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
