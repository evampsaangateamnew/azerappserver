/**
 * 
 */
package com.evampsaanga.azerfon.models.history.getusagesummary;

import com.evampsaanga.azerfon.common.utilities.BaseRequest;

/**
 * @author Evamp & Saanga
 *
 */
public class UsageSummaryRequest extends BaseRequest {

	private String startDate;
	private String endDate;
	private String accountId;
	private String customerId;

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public String getAccountId() {
		return accountId;
	}

	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	@Override
	public String toString() {
		return "UsageSummaryRequest [startDate=" + startDate + ", endDate=" + endDate + ", accountId=" + accountId
				+ ", customerId=" + customerId + "]";
	}

}
