/**
 * 
 */
package com.evampsaanga.azerfon.models.history.getusagesummary;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Evamp & Saanga
 *
 */
public class UsageSummaryResponseData {
	private List<SummaryRecordDetails> summaryList;

	public UsageSummaryResponseData() {
		summaryList = new ArrayList<>();
	}

	public List<SummaryRecordDetails> getSummaryList() {
		return summaryList;
	}

	public void setSummaryList(List<SummaryRecordDetails> summaryList) {
		this.summaryList = summaryList;
	}

}
