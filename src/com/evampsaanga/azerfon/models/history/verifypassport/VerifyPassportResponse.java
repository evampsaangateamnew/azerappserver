/**
 * 
 */
package com.evampsaanga.azerfon.models.history.verifypassport;

import com.evampsaanga.azerfon.common.utilities.BaseResponse;

/**
 * @author Evamp & Saanga
 *
 */
public class VerifyPassportResponse extends BaseResponse {

	@Override
	public String toString() {
		return "VerifyPassportResponse [getCallStatus()=" + getCallStatus() + ", getResultCode()=" + getResultCode()
				+ ", getResultDesc()=" + getResultDesc() + "]";
	}

}
