package com.evampsaanga.azerfon.models.history.verifyaccountdetails;

public class VerifyAccountDetailsResponseData {
	private String pin;

	public String getPin() {
		return pin;
	}

	public void setPin(String pin) {
		this.pin = pin;
	}
}
