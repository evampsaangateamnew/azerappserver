package com.evampsaanga.azerfon.models.retryfailed;

import com.evampsaanga.azerfon.common.utilities.BaseResponse;

public class RetryFailedResponse extends BaseResponse {
	private RetryFailedResponseData data;

	public RetryFailedResponseData getData() {
		return data;
	}

	public void setData(RetryFailedResponseData data) {
		this.data = data;
	}

}
