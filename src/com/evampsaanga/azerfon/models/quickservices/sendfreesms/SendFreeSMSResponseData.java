/**
 * 
 */
package com.evampsaanga.azerfon.models.quickservices.sendfreesms;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * @author Evamp & Saanga
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class SendFreeSMSResponseData {

	private String onNetSMS;
	private String offNetSMS;
	private String smsSent;
	
	private String responseMsg;

	public String getResponseMsg() {
		return responseMsg;
	}

	public void setResponseMsg(String responseMsg) {
		this.responseMsg = responseMsg;
	}

	public String getOnNetSMS() {
		return onNetSMS;
	}

	public void setOnNetSMS(String onNetSMS) {
		this.onNetSMS = onNetSMS;
	}

	public String getOffNetSMS() {
		return offNetSMS;
	}

	public void setOffNetSMS(String offNetSMS) {
		this.offNetSMS = offNetSMS;
	}

	public String getSmsSent() {
		return smsSent;
	}

	public void setSmsSent(String smsSent) {
		this.smsSent = smsSent;
	}

}
