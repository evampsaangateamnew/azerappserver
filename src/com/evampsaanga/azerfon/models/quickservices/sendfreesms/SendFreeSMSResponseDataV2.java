/**
 * 
 */
package com.evampsaanga.azerfon.models.quickservices.sendfreesms;

/**
 * @author Evamp & Saanga
 *
 */
public class SendFreeSMSResponseDataV2 {

	private String responseMsg;

	public String getResponseMsg() {
		return responseMsg;
	}

	public void setResponseMsg(String responseMsg) {
		this.responseMsg = responseMsg;
	}
	

}
