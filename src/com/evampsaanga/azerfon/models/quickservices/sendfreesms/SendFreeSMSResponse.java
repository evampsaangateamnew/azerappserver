/**
 * 
 */
package com.evampsaanga.azerfon.models.quickservices.sendfreesms;

import com.evampsaanga.azerfon.common.utilities.BaseResponse;

/**
 * @author Evamp & Saanga
 *
 */
public class SendFreeSMSResponse extends BaseResponse {

	public SendFreeSMSResponseData data;

	public SendFreeSMSResponseData getData() {
		return data;
	}

	public void setData(SendFreeSMSResponseData data) {
		this.data = data;
	}

}
