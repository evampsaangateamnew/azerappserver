/**
 * 
 */
package com.evampsaanga.azerfon.models.quickservices.sendfreesms;

import com.evampsaanga.azerfon.common.utilities.BaseRequest;

/**
 * @author Evamp & Saanga
 *
 */
public class SendFreeSMSRequest extends BaseRequest {
    private String recieverMsisdn;
    private String textmsg;
    private String msgLang;

    public String getRecieverMsisdn() {
	return recieverMsisdn;
    }

    public void setRecieverMsisdn(String recieverMsisdn) {
	this.recieverMsisdn = recieverMsisdn;
    }

    public String getTextmsg() {
	return textmsg;
    }

    public void setTextmsg(String textmsg) {
	this.textmsg = textmsg;
    }

    public String getMsgLang() {
	return msgLang;
    }

    public void setMsgLang(String msgLang) {
	this.msgLang = msgLang;
    }

    @Override
    public String toString() {
	return "SendFreeSMSRequest [recieverMsisdn=" + recieverMsisdn + ", textmsg=" + textmsg + ", msgLang=" + msgLang
		+ ", getLang()=" + getLang() + ", getiP()=" + getiP() + ", getChannel()=" + getChannel()
		+ ", getMsisdn()=" + getMsisdn() + "]";
    }

}
