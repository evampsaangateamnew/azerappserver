/**
 * 
 */
package com.evampsaanga.azerfon.models.quickservices.sendfreesms;

import com.evampsaanga.azerfon.common.utilities.BaseResponse;

/**
 * @author Evamp & Saanga
 *
 */
public class SendFreeSMSResponseV2 extends BaseResponse {

	public SendFreeSMSResponseDataV2 data;

	public SendFreeSMSResponseDataV2 getData() {
		return data;
	}

	public void setData(SendFreeSMSResponseDataV2 data) {
		this.data = data;
	}

}
