/**
 * 
 */
package com.evampsaanga.azerfon.models.quickservices.getfreesmsstatus;

/**
 * @author Evamp & Saanga
 *
 */
public class GetFreeSMSStatusResponseData {
	private String onNetSMS;
	private String offNetSMS;
	public String getOnNetSMS() {
		return onNetSMS;
	}
	public void setOnNetSMS(String onNetSMS) {
		this.onNetSMS = onNetSMS;
	}
	public String getOffNetSMS() {
		return offNetSMS;
	}
	public void setOffNetSMS(String offNetSMS) {
		this.offNetSMS = offNetSMS;
	}

}
