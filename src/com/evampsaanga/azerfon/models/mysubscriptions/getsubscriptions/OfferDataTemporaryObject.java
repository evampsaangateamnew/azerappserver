/**
 * 
 */
package com.evampsaanga.azerfon.models.mysubscriptions.getsubscriptions;

import com.evampsaanga.azerfon.models.supplementaryofferings.getsupplementaryofferings.SupplementaryOfferingsResponseData;

/**
 * @author Evamp & Saanga
 *
 */

/*
 * Below class is used to to store data temporarily with flag check. This check
 * will indicate that either any new data is stored or this object is still
 * persist with old data
 */
public class OfferDataTemporaryObject {
	boolean isNewOfferAdded;
	SupplementaryOfferingsResponseData resData;

	public boolean isNewOfferAdded() {
		return isNewOfferAdded;
	}

	public void setNewOfferAdded(boolean isNewOfferAdded) {
		this.isNewOfferAdded = isNewOfferAdded;
	}

	public SupplementaryOfferingsResponseData getResData() {
		return resData;
	}

	public void setResData(SupplementaryOfferingsResponseData resData) {
		this.resData = resData;
	}
}
