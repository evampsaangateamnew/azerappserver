/**
 * 
 */
package com.evampsaanga.azerfon.models.mysubscriptions.getsubscriptions;

/**
 * @author Evamp & Saanga
 *
 */
public class HeaderUsage {
    private String activationDate;
    private String iconName;
    private String remainingTitle;
    private String remainingUsage;
    private String renewalDate;
    private String renewalTitle;
    private String totalUsage;
    private String unit;
    private String type;

    public String getActivationDate() {
	return activationDate;
    }

    public void setActivationDate(String activationDate) {
	this.activationDate = activationDate;
    }

    public String getIconName() {
	return iconName;
    }

    public void setIconName(String iconName) {
	this.iconName = iconName;
    }

    public String getRemainingTitle() {
	return remainingTitle;
    }

    public void setRemainingTitle(String remainingTitle) {
	this.remainingTitle = remainingTitle;
    }

    public String getRemainingUsage() {
	return remainingUsage;
    }

    public void setRemainingUsage(String remainingUsage) {
	this.remainingUsage = remainingUsage;
    }

    public String getRenewalDate() {
	return renewalDate;
    }

    public void setRenewalDate(String renewalDate) {
	this.renewalDate = renewalDate;
    }

    public String getRenewalTitle() {
	return renewalTitle;
    }

    public void setRenewalTitle(String renewalTitle) {
	this.renewalTitle = renewalTitle;
    }

    public String getTotalUsage() {
	return totalUsage;
    }

    public void setTotalUsage(String totalUsage) {
	this.totalUsage = totalUsage;
    }

    public String getUnit() {
	return unit;
    }

    public void setUnit(String unit) {
	this.unit = unit;
    }

    public String getType() {
	return type;
    }

    public void setType(String type) {
	this.type = type;
    }
}
