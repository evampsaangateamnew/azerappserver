/**
 * 
 */
package com.evampsaanga.azerfon.models.mysubscriptions.getsubscriptions;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.evampsaanga.azerfon.common.utilities.BaseRequest;

/**
 * @author Evamp & Saanga
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class MySubscriptionsRequest extends BaseRequest {
	List<String> listOfferingIds;
	private String offeringName;
	private String brandName;
	private String individualMsisdn;
	private String groupAccessCode;
	
	public String getOfferingName() {
		return offeringName;
	}

	public void setOfferingName(String offeringName) {
		this.offeringName = offeringName;
	}

	public MySubscriptionsRequest() {
		listOfferingIds = new ArrayList<>();
	}

	public List<String> getListOfferingIds() {
		return listOfferingIds;
	}

	public void setListOfferingIds(List<String> listOfferingIds) {
		this.listOfferingIds = listOfferingIds;
	}

	public String getBrandName() {
		return brandName;
	}

	public void setBrandName(String brandName) {
		this.brandName = brandName;
	}

	public String getIndividualMsisdn() {
		return individualMsisdn;
	}

	public void setIndividualMsisdn(String individualMsisdn) {
		this.individualMsisdn = individualMsisdn;
	}

	public String getGroupAccessCode() {
		return groupAccessCode;
	}

	public void setGroupAccessCode(String groupAccessCode) {
		this.groupAccessCode = groupAccessCode;
	}

	@Override
	public String toString() {
		return "MySubscriptionsRequest [listOfferingIds=" + listOfferingIds + ", offeringName=" + offeringName
				+ ", brandName=" + brandName + "]";
	}

}
