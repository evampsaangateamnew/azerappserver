/**
 * 
 */
package com.evampsaanga.azerfon.models.verifyappversion;

import com.evampsaanga.azerfon.common.utilities.BaseResponse;

/**
 * @author Evamp & Saanga
 *
 */
public class VerifyAppVersionResponse extends BaseResponse {
	private VerifyAppVersionResponseData data;

	public VerifyAppVersionResponseData getData() {
		return data;
	}

	public void setData(VerifyAppVersionResponseData data) {
		this.data = data;
	}
}
