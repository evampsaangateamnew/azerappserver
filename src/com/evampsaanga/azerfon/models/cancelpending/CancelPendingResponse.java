package com.evampsaanga.azerfon.models.cancelpending;

import com.evampsaanga.azerfon.common.utilities.BaseResponse;

public class CancelPendingResponse extends BaseResponse {

	private CancelPendingResponseData data;

	public CancelPendingResponseData getData() {
		return data;
	}

	public void setData(CancelPendingResponseData data) {
		this.data = data;
	}

}
