package com.evampsaanga.azerfon.models.cancelpending;

public class CancelPendingResponseData {

	private String responseMsg;

	public String getResponseMsg() {
		return responseMsg;
	}

	public void setResponseMsg(String responseMsg) {
		this.responseMsg = responseMsg;
	}

}
