/**
 * 
 */
package com.evampsaanga.azerfon.models.nartvservices.getsubscriptions;

import com.evampsaanga.azerfon.common.utilities.BaseResponse;

/**
 * @author Evamp & Saanga
 *
 */
public class GetSubscriptionsResponse extends BaseResponse {

	private GetSubscriptionsResponseData data;

	public GetSubscriptionsResponseData getData() {
		return data;
	}

	public void setData(GetSubscriptionsResponseData data) {
		this.data = data;
	}

}
