/**
 * 
 */
package com.evampsaanga.azerfon.models.nartvservices.getsubscriptions;

import com.evampsaanga.azerfon.common.utilities.BaseRequest;

/**
 * @author Evamp & Saanga
 *
 */
public class GetSubscriptionsRequest extends BaseRequest {
	private String offeringId;

	public String getOfferingId() {
		return offeringId;
	}

	public void setOfferingId(String offeringId) {
		this.offeringId = offeringId;
	}

}
