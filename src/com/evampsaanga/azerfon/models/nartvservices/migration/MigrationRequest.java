/**
 * 
 */
package com.evampsaanga.azerfon.models.nartvservices.migration;

import com.evampsaanga.azerfon.common.utilities.BaseRequest;

/**
 * @author Evamp & Saanga
 *
 */
public class MigrationRequest extends BaseRequest {

	private String subscriberId;
	private String planId;

	public String getSubscriberId() {
		return subscriberId;
	}

	public void setSubscriberId(String subscriberId) {
		this.subscriberId = subscriberId;
	}

	public String getPlanId() {
		return planId;
	}

	public void setPlanId(String planId) {
		this.planId = planId;
	}

	@Override
	public String toString() {
		return "MigrationRequest [subscriberId=" + subscriberId + ", planId=" + planId + "]";
	}

}
