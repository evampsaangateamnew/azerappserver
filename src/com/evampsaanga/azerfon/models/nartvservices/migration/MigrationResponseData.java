/**
 * 
 */
package com.evampsaanga.azerfon.models.nartvservices.migration;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * @author Evamp & Saanga
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class MigrationResponseData {

	private String message;

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	@Override
	public String toString() {
		return "MigrationResponseData [message=" + message + "]";
	}

}
