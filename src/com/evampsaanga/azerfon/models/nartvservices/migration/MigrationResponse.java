/**
 * 
 */
package com.evampsaanga.azerfon.models.nartvservices.migration;

import com.evampsaanga.azerfon.common.utilities.BaseResponse;

/**
 * @author Evamp & Saanga
 *
 */
public class MigrationResponse extends BaseResponse {

	private MigrationResponseData data;

	public MigrationResponseData getData() {
		return data;
	}

	public void setData(MigrationResponseData data) {
		this.data = data;
	}

}
