package com.evampsaanga.azerfon.models.acceptTNC;

import com.evampsaanga.azerfon.common.utilities.BaseResponse;

public class AcceptTnCResponse extends BaseResponse {
	AcceptTnCResponseData data;

	public AcceptTnCResponseData getData() {
		return data;
	}

	public void setData(AcceptTnCResponseData data) {
		this.data = data;
	}

}
