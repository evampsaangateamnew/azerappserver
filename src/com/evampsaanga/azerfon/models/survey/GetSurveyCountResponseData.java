package com.evampsaanga.azerfon.models.survey;

public class GetSurveyCountResponseData {
	private String surveyId;
	private String surveyCount;

	public String getSurveyId() {
		return surveyId;
	}

	public void setSurveyId(String surveyId) {
		this.surveyId = surveyId;
	}

	public String getSurveyCount() {
		return surveyCount;
	}

	public void setSurveyCount(String surveyCount) {
		this.surveyCount = surveyCount;
	}

	@Override
	public String toString() {
		return "GetSurveyCountResponseData [surveyId=" + surveyId + ", surveyCount=" + surveyCount + "]";
	}

}
