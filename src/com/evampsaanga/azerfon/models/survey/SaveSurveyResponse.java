package com.evampsaanga.azerfon.models.survey;

import com.evampsaanga.azerfon.common.utilities.BaseResponse;

public class SaveSurveyResponse extends BaseResponse {

	SaveSurveyResponseData data;

	public SaveSurveyResponseData getData() {
		return data;
	}

	public void setData(SaveSurveyResponseData data) {
		this.data = data;
	}

	@Override
	public String toString() {
		return "SaveSurveyResponse [data=" + data + ", toString()=" + super.toString() + "]";
	}

}