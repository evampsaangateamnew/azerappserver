/**
 * 
 */
package com.evampsaanga.azerfon.models.homepageservices;

import java.util.List;

import com.evampsaanga.azerfon.models.homepageservices.freeresources.FreeResource;

/**
 * @author Evamp & Saanga
 *
 */
public class FreeResources {

	private List<FreeResource> freeResources;

	private List<FreeResource> freeResourcesRoaming;

	public List<FreeResource> getFreeResources() {
		return freeResources;
	}

	public void setFreeResources(List<FreeResource> freeResources) {
		this.freeResources = freeResources;
	}

	public List<FreeResource> getFreeResourcesRoaming() {
		return freeResourcesRoaming;
	}

	public void setFreeResourcesRoaming(List<FreeResource> freeResourcesRoaming) {
		this.freeResourcesRoaming = freeResourcesRoaming;
	}

}
