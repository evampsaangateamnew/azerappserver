/**
 * 
 */
package com.evampsaanga.azerfon.models.homepageservices;

/**
 * @author Evamp & Saanga
 *
 */
public class Credit {

	private String creditTitleLabel;
	private String creditTitleValue;
	private String creditCurrency;
	private String creditDateLabel;
	private String creditDate;
	private String creditInitialDate;
	private String creditLimit;
	private String daysDifferenceTotal;
	private String daysDifferenceCurrent;
	
	

	public String getDaysDifferenceTotal() {
		return daysDifferenceTotal;
	}

	public void setDaysDifferenceTotal(String daysDifferenceTotal) {
		this.daysDifferenceTotal = daysDifferenceTotal;
	}

	public String getDaysDifferenceCurrent() {
		return daysDifferenceCurrent;
	}

	public void setDaysDifferenceCurrent(String daysDifferenceCurrent) {
		this.daysDifferenceCurrent = daysDifferenceCurrent;
	}

	public String getCreditTitleLabel() {
		return creditTitleLabel;
	}

	public void setCreditTitleLabel(String creditTitleLabel) {
		this.creditTitleLabel = creditTitleLabel;
	}

	public String getCreditTitleValue() {
		return creditTitleValue;
	}

	public void setCreditTitleValue(String creditTitleValue) {
		this.creditTitleValue = creditTitleValue;
	}

	public String getCreditCurrency() {
		return creditCurrency;
	}

	public void setCreditCurrency(String creditCurrency) {
		this.creditCurrency = creditCurrency;
	}

	public String getCreditDateLabel() {
		return creditDateLabel;
	}

	public void setCreditDateLabel(String creditDateLabel) {
		this.creditDateLabel = creditDateLabel;
	}

	public String getCreditDate() {
		return creditDate;
	}

	public void setCreditDate(String creditDate) {
		this.creditDate = creditDate;
	}

	public String getCreditLimit() {
		return creditLimit;
	}

	public void setCreditLimit(String creditLimit) {
		this.creditLimit = creditLimit;
	}

	public String getCreditInitialDate() {
		return creditInitialDate;
	}

	public void setCreditInitialDate(String creditInitialDate) {
		this.creditInitialDate = creditInitialDate;
	}

}
