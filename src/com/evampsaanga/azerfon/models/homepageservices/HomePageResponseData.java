/**
 * 
 */
package com.evampsaanga.azerfon.models.homepageservices;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.evampsaanga.azerfon.models.homepageservices.balance.Balance;

/**
 * @author Evamp & Saanga
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class HomePageResponseData {

	private String notificationUnreadCount;
	private Balance balance;
	private Installments installments;
	private MRC mrc;
	private Credit credit;
	private FreeResources freeResources;
	private String status;
	private String offeringNameDisplay;
	


	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getOfferingNameDisplay() {
		return offeringNameDisplay;
	}

	public void setOfferingNameDisplay(String offeringNameDisplay) {
		this.offeringNameDisplay = offeringNameDisplay;
	}

	public Balance getBalance() {
		return balance;
	}

	public void setBalance(Balance balance) {
		this.balance = balance;
	}

	public Installments getInstallments() {
		return installments;
	}

	public void setInstallments(Installments installments) {
		this.installments = installments;
	}

	public MRC getMrc() {
		return mrc;
	}

	public void setMrc(MRC mrc) {
		this.mrc = mrc;
	}

	public Credit getCredit() {
		return credit;
	}

	public void setCredit(Credit credit) {
		this.credit = credit;
	}

	public FreeResources getFreeResources() {
		return freeResources;
	}

	public void setFreeResources(FreeResources freeResources) {
		this.freeResources = freeResources;
	}

	public String getNotificationUnreadCount() {
		return notificationUnreadCount;
	}

	public void setNotificationUnreadCount(String notificationUnreadCount) {
		this.notificationUnreadCount = notificationUnreadCount;
	}

}
