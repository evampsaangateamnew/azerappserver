/**
 * 
 */
package com.evampsaanga.azerfon.models.homepageservices.installments;

/**
 * @author Evamp & Saanga
 *
 */
public class Installment {
	private String name;
	private String amountLabel;
	private String amountValue;
	private String currency;
	private String nextPaymentLabel;
	private String nextPaymentValue;
	private String nextPaymentInitialDate;
	private String remainingAmountLabel;
	private String remainingAmountTotalValue;
	private String remainingAmountCurrentValue;
	private String remainingPeriodLabel;
	private String remainingCurrentPeriod;
	private String remainingTotalPeriod;
	private String remainingPeriodBeginDateLabel;
	private String remainingPeriodBeginDateValue;
	private String remainingPeriodEndDateLabel;
	private String remainingPeriodEndDateValue;
	private String purchaseDateLabel;
	private String purchaseDateValue;
	private String installmentFeeLimit;
	private String installmentStatus;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAmountLabel() {
		return amountLabel;
	}

	public void setAmountLabel(String amountLabel) {
		this.amountLabel = amountLabel;
	}

	public String getAmountValue() {
		return amountValue;
	}

	public void setAmountValue(String amountValue) {
		this.amountValue = amountValue;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public String getNextPaymentLabel() {
		return nextPaymentLabel;
	}

	public void setNextPaymentLabel(String nextPaymentLabel) {
		this.nextPaymentLabel = nextPaymentLabel;
	}

	public String getNextPaymentValue() {
		return nextPaymentValue;
	}

	public void setNextPaymentValue(String nextPaymentValue) {
		this.nextPaymentValue = nextPaymentValue;
	}

	public String getRemainingAmountLabel() {
		return remainingAmountLabel;
	}

	public void setRemainingAmountLabel(String remainingAmountLabel) {
		this.remainingAmountLabel = remainingAmountLabel;
	}

	public String getRemainingAmountTotalValue() {
		return remainingAmountTotalValue;
	}

	public void setRemainingAmountTotalValue(String remainingAmountTotalValue) {
		this.remainingAmountTotalValue = remainingAmountTotalValue;
	}

	public String getRemainingAmountCurrentValue() {
		return remainingAmountCurrentValue;
	}

	public void setRemainingAmountCurrentValue(String remainingAmountCurrentValue) {
		this.remainingAmountCurrentValue = remainingAmountCurrentValue;
	}

	public String getRemainingPeriodLabel() {
		return remainingPeriodLabel;
	}

	public void setRemainingPeriodLabel(String remainingPeriodLabel) {
		this.remainingPeriodLabel = remainingPeriodLabel;
	}

	public String getRemainingCurrentPeriod() {
		return remainingCurrentPeriod;
	}

	public void setRemainingCurrentPeriod(String remainingCurrentPeriod) {
		this.remainingCurrentPeriod = remainingCurrentPeriod;
	}

	public String getRemainingTotalPeriod() {
		return remainingTotalPeriod;
	}

	public void setRemainingTotalPeriod(String remainingTotalPeriod) {
		this.remainingTotalPeriod = remainingTotalPeriod;
	}

	public String getRemainingPeriodBeginDateLabel() {
		return remainingPeriodBeginDateLabel;
	}

	public void setRemainingPeriodBeginDateLabel(String remainingPeriodBeginDateLabel) {
		this.remainingPeriodBeginDateLabel = remainingPeriodBeginDateLabel;
	}

	public String getRemainingPeriodBeginDateValue() {
		return remainingPeriodBeginDateValue;
	}

	public void setRemainingPeriodBeginDateValue(String remainingPeriodBeginDateValue) {
		this.remainingPeriodBeginDateValue = remainingPeriodBeginDateValue;
	}

	public String getRemainingPeriodEndDateLabel() {
		return remainingPeriodEndDateLabel;
	}

	public void setRemainingPeriodEndDateLabel(String remainingPeriodEndDateLabel) {
		this.remainingPeriodEndDateLabel = remainingPeriodEndDateLabel;
	}

	public String getRemainingPeriodEndDateValue() {
		return remainingPeriodEndDateValue;
	}

	public void setRemainingPeriodEndDateValue(String remainingPeriodEndDateValue) {
		this.remainingPeriodEndDateValue = remainingPeriodEndDateValue;
	}

	public String getPurchaseDateLabel() {
		return purchaseDateLabel;
	}

	public void setPurchaseDateLabel(String purchaseDateLabel) {
		this.purchaseDateLabel = purchaseDateLabel;
	}

	public String getPurchaseDateValue() {
		return purchaseDateValue;
	}

	public void setPurchaseDateValue(String purchaseDateValue) {
		this.purchaseDateValue = purchaseDateValue;
	}

	public String getInstallmentFeeLimit() {
		return installmentFeeLimit;
	}

	public void setInstallmentFeeLimit(String installmentFeeLimit) {
		this.installmentFeeLimit = installmentFeeLimit;
	}

	public String getInstallmentStatus() {
		return installmentStatus;
	}

	public void setInstallmentStatus(String installmentStatus) {
		this.installmentStatus = installmentStatus;
	}

	public String getNextPaymentInitialDate() {
		return nextPaymentInitialDate;
	}

	public void setNextPaymentInitialDate(String nextPaymentInitialDate) {
		this.nextPaymentInitialDate = nextPaymentInitialDate;
	}

}
