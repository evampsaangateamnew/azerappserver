/**
 * 
 */
package com.evampsaanga.azerfon.models.homepageservices;

import com.evampsaanga.azerfon.common.utilities.BaseResponse;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * @author Evamp & Saanga
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class HomePageResponse extends BaseResponse {
	private HomePageResponseData data;

	public HomePageResponseData getData() {
		return data;
	}

	public void setData(HomePageResponseData data) {
		this.data = data;
	}

}
