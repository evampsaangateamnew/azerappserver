/**
 * 
 */
package com.evampsaanga.azerfon.models.homepageservices.balance;

/**
 * @author Evamp & Saanga
 *
 */
public class Balance {

	private Prepaid prepaid;
	private Postpaid postpaid;

	public Prepaid getPrepaid() {
		return prepaid;
	}

	public void setPrepaid(Prepaid prepaid) {
		this.prepaid = prepaid;
	}

	public Postpaid getPostpaid() {
		return postpaid;
	}

	public void setPostpaid(Postpaid postpaid) {
		this.postpaid = postpaid;
	}

}
