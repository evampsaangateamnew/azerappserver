/**
 * 
 */
package com.evampsaanga.azerfon.models.homepageservices;

import com.evampsaanga.azerfon.common.utilities.BaseRequest;

/**
 * @author Evamp & Saanga
 *
 */
public class HomePageRequest extends BaseRequest {

	private String customerType;
	private String brandId;
	private String offeringId;

	public String getBrandId() {
		return brandId;
	}

	public void setBrandId(String brandId) {
		this.brandId = brandId;
	}

	

	public String getCustomerType() {
		return customerType;
	}

	public void setCustomerType(String customerType) {
		this.customerType = customerType;
	}

	public String getOfferingId() {
		return offeringId;
	}

	public void setOfferingId(String offeringId) {
		this.offeringId = offeringId;
	}

	@Override
	public String toString() {
		return "HomePageRequest [customerType=" + customerType + ", brandId=" + brandId + ", offeringId="
				+ offeringId + "]";
	}

}
