package com.evampsaanga.azerfon.models.ordermanagementinsert;

import com.evampsaanga.azerfon.common.utilities.BaseRequest;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ChangeLimitInitialMaximumRequest extends BaseRequest {
	
	
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	private String offeringId;
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	private String groupType;
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	private String companyStandardValue;
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	private String msisdnuser;
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	private String type;
	
	public String getOfferingId() {
		return offeringId;
	}

	public void setOfferingId(String offeringId) {
		this.offeringId = offeringId;
	}

	
	   public String getMsisdnuser() {
			return msisdnuser;
		}

		public void setMsisdnuser(String msisdnuser) {
			this.msisdnuser = msisdnuser;
		}
	
	
   public String getCompanyStandardValue() {
		return companyStandardValue;
	}

	public void setCompanyStandardValue(String companyStandardValue) {
		this.companyStandardValue = companyStandardValue;
	}

	public String getGroupType() {
		return groupType;
	}

	public void setGroupType(String groupType) {
		this.groupType = groupType;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	
}
