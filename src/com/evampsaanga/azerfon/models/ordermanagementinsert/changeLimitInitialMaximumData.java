package com.evampsaanga.azerfon.models.ordermanagementinsert;

import java.util.ArrayList;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class changeLimitInitialMaximumData {
	private String initialLimit="";
	private String maxLimit="";
	private ArrayList<String> limitList;
	private String limitLabel;
	private String currentLimit;
	
	public ArrayList<String> getLimitList() {
		return limitList;
	}
	public void setLimitList(ArrayList<String> limitList) {
		this.limitList = limitList;
	}
	public String getLimitLabel() {
		return limitLabel;
	}
	public void setLimitLabel(String limitLabel) {
		this.limitLabel = limitLabel;
	}
	public String getInitialLimit() {
		return initialLimit;
	}
	public void setInitialLimit(String initialLimit) {
		this.initialLimit = initialLimit;
	}
	public String getMaxLimit() {
		return maxLimit;
	}
	public void setMaxLimit(String maxLimit) {
		this.maxLimit = maxLimit;
	}
	public String getCurrentLimit() {
		return currentLimit;
	}
	public void setCurrentLimit(String currentLimit) {
		this.currentLimit = currentLimit;
	}


}
