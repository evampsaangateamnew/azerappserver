package com.evampsaanga.azerfon.models.ordermanagementinsert;

import com.evampsaanga.azerfon.common.utilities.BaseResponse;

public class OrderManagementInsertResponse extends BaseResponse {
	public OrderManagementInsertResponseData data;
	

	public OrderManagementInsertResponseData getData() {
		return data;
	}

	public void setData(OrderManagementInsertResponseData data) {
		this.data = data;
	}
	

}
