package com.evampsaanga.azerfon.models.ordermanagementinsert;

import com.evampsaanga.azerfon.common.utilities.BaseResponse;

public class changeLimitInitialMaximumResponse  extends BaseResponse {
	public changeLimitInitialMaximumData getData() {
		return data;
	}

	public void setData(changeLimitInitialMaximumData data) {
		this.data = data;
	}

	private changeLimitInitialMaximumData data=new changeLimitInitialMaximumData();

}
