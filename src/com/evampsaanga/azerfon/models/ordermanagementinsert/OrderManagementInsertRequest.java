package com.evampsaanga.azerfon.models.ordermanagementinsert;

import java.util.List;

import org.apache.commons.lang.builder.ToStringBuilder;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.evampsaanga.azerfon.common.utilities.BaseRequest;
import com.evampsaanga.azerfon.common.utilities.RecieverMsisdn;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonPropertyOrder({ "actPrice", "msgLang", "orderKey", "recieverMsisdn", "destinationTariff", "tariffPermissions",
		"textmsg", "senderName", "amount", "offeringId", "number", "actionType", "companyValue", "totalLimit",
		"orderId", "totalUsersbyGroup", "accountId", "iccid", "transactionId", "customerId", "contactNumber","specialGroup","accountCode","picType"})
public class OrderManagementInsertRequest extends BaseRequest {

	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonProperty("picType")
	private String picType;
	
	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonProperty("accountCode")
	private String accountCode;
	
	
	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonProperty("specialGroup")
	private String specialGroup;
	
	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonProperty("contactNumber")
	private String contactNumber;

	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonProperty("customerId")
	private String customerId;

	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonProperty("iccid")
	private String iccid;

	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonProperty("transactionId")
	private String transactionId;
	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonProperty("actPrice")
	private String actPrice;

	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonProperty("msgLang")
	private String msgLang;

	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonProperty("orderKey")
	private String orderKey;

	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonProperty("recieverMsisdn")
	private List<RecieverMsisdn> recieverMsisdn = null;

	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonProperty("destinationTariff")
	private String destinationTariff;

	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonProperty("tariffPermissions")
	private String tariffPermissions;

	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonProperty("textmsg")
	private String textmsg;

	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonProperty("senderName")
	private String senderName;

	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonProperty("amount")
	private String amount;

	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonProperty("offeringId")
	private String offeringId;

	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonProperty("number")
	private String number;

	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonProperty("actionType")
	private String actionType;

	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonProperty("companyValue")
	private String companyValue;

	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonProperty("totalLimit")
	private String totalLimit;

	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonProperty("orderId")
	private String orderId;

	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonProperty("totalUsersbyGroup")
	private String totalUsersbyGroup;

	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonProperty("accountId")
	private String accountId;

	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonProperty("virtualCorpCode")
	private String virtualCorpCode;

	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonProperty("companyLevel")
	private String companyLevel;

	public String getAccountId() {
		return accountId;
	}

	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}

	public String getTotalUsersbyGroup() {
		return totalUsersbyGroup;
	}

	public void setTotalUsersbyGroup(String totalUsersbyGroup) {
		this.totalUsersbyGroup = totalUsersbyGroup;
	}

	public String getActPrice() {
		return actPrice;
	}

	@JsonProperty("actPrice")
	public void setActPrice(String actPrice) {
		this.actPrice = actPrice;
	}

	@JsonProperty("msgLang")
	public String getMsgLang() {
		return msgLang;
	}

	@JsonProperty("msgLang")
	public void setMsgLang(String msgLang) {
		this.msgLang = msgLang;
	}

	@JsonProperty("orderKey")
	public String getOrderKey() {
		return orderKey;
	}

	@JsonProperty("orderKey")
	public void setOrderKey(String orderKey) {
		this.orderKey = orderKey;
	}

	@JsonProperty("recieverMsisdn")
	public List<RecieverMsisdn> getRecieverMsisdn() {
		return recieverMsisdn;
	}

	@JsonProperty("recieverMsisdn")
	public void setRecieverMsisdn(List<RecieverMsisdn> recieverMsisdn) {
		this.recieverMsisdn = recieverMsisdn;
	}

	@JsonProperty("destinationTariff")
	public String getDestinationTariff() {
		return destinationTariff;
	}

	@JsonProperty("destinationTariff")
	public void setDestinationTariff(String destinationTariff) {
		this.destinationTariff = destinationTariff;
	}

	@JsonProperty("tariffPermissions")
	public String getTariffPermissions() {
		return tariffPermissions;
	}

	@JsonProperty("tariffPermissions")
	public void setTariffPermissions(String tariffPermissions) {
		this.tariffPermissions = tariffPermissions;
	}

	@JsonProperty("textmsg")
	public String getTextmsg() {
		return textmsg;
	}

	@JsonProperty("textmsg")
	public void setTextmsg(String textmsg) {
		this.textmsg = textmsg;
	}

	@JsonProperty("senderName")
	public String getSenderName() {
		return senderName;
	}

	@JsonProperty("senderName")
	public void setSenderName(String senderName) {
		this.senderName = senderName;
	}

	@JsonProperty("amount")
	public String getAmount() {
		return amount;
	}

	@JsonProperty("amount")
	public void setAmount(String amount) {
		this.amount = amount;
	}

	@JsonProperty("offeringId")
	public String getOfferingId() {
		return offeringId;
	}

	@JsonProperty("offeringId")
	public void setOfferingId(String offeringId) {
		this.offeringId = offeringId;
	}

	@JsonProperty("number")
	public String getNumber() {
		return number;
	}

	@JsonProperty("number")
	public void setNumber(String number) {
		this.number = number;
	}

	@JsonProperty("actionType")
	public String getActionType() {
		return actionType;
	}

	@JsonProperty("actionType")
	public void setActionType(String actionType) {
		this.actionType = actionType;
	}

	@JsonProperty("companyValue")
	public String getCompanyValue() {
		return companyValue;
	}

	@JsonProperty("companyValue")
	public void setCompanyValue(String companyValue) {
		this.companyValue = companyValue;
	}

	@JsonProperty("totalLimit")
	public String getTotalLimit() {
		return totalLimit;
	}

	@JsonProperty("totalLimit")
	public void setTotalLimit(String totalLimit) {
		this.totalLimit = totalLimit;
	}

	@JsonProperty("orderId")
	public String getOrderId() {
		return orderId;
	}

	@JsonProperty("orderId")
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	@JsonProperty("virtualCorpCode")
	public String getVirtualCorpCode() {
		return virtualCorpCode;
	}

	@JsonProperty("virtualCorpCode")
	public void setVirtualCorpCode(String virtualCorpCode) {
		this.virtualCorpCode = virtualCorpCode;
	}

	@JsonProperty("companyLevel")
	public String getCompanyLevel() {
		return companyLevel;
	}

	@JsonProperty("companyLevel")
	public void setCompanyLevel(String companyLevel) {
		this.companyLevel = companyLevel;
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this).append("actPrice", actPrice).append("msgLang", msgLang)
				.append("orderKey", orderKey).append("recieverMsisdn", recieverMsisdn)
				.append("destinationTariff", destinationTariff).append("tariffPermissions", tariffPermissions)
				.append("textmsg", textmsg).append("senderName", senderName).append("amount", amount)
				.append("offeringId", offeringId).append("number", number).append("actionType", actionType)
				.append("companyValue", companyValue).append("totalLimit", totalLimit).append("orderId", orderId)
				.append("virtualCorpCode", virtualCorpCode).append("contactNumber", contactNumber)
				.append("customerId", customerId).append("transactionId", transactionId).append("iccid", iccid)
				.append("companyLevel", companyLevel).append("picType", picType).append("accountCode", accountCode).append("specialGroup", specialGroup).toString();
	}

	public String getIccid() {
		return iccid;
	}

	public void setIccid(String iccid) {
		this.iccid = iccid;
	}

	public String getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public String getContactNumber() {
		return contactNumber;
	}

	public void setContactNumber(String contactNumber) {
		this.contactNumber = contactNumber;
	}

	public String getSpecialGroup() {
		return specialGroup;
	}

	public void setSpecialGroup(String specialGroup) {
		this.specialGroup = specialGroup;
	}

	public String getAccountCode() {
		return accountCode;
	}

	public void setAccountCode(String accountCode) {
		this.accountCode = accountCode;
	}

	public String getPicType() {
		return picType;
	}

	public void setPicType(String picType) {
		this.picType = picType;
	}

}
