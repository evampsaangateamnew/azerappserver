/**
 * 
 */
package com.evampsaanga.azerfon.models.triggers;

import com.evampsaanga.azerfon.common.utilities.BaseRequest;

/**
 * @author Evamp & Saanga
 *
 */
public class TriggersRequest extends BaseRequest {
	private String cacheType;

	public String getCacheType() {
		return cacheType;
	}

	public void setCacheType(String cacheType) {
		this.cacheType = cacheType;
	}

	@Override
	public String toString() {
		return "TriggersRequest [cacheType=" + cacheType + ", getLang()=" + getLang() + ", getiP()=" + getiP()
				+ ", getChannel()=" + getChannel() + ", getMsisdn()=" + getMsisdn() + "]";
	}

}
