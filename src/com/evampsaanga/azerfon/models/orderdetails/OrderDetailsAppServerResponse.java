package com.evampsaanga.azerfon.models.orderdetails;

import java.util.List;

import com.evampsaanga.azerfon.common.utilities.BaseResponse;

public class OrderDetailsAppServerResponse extends BaseResponse {
	

	OrderDetailsData data=null;

	public OrderDetailsData getData() {
		return data;
	}

	public void setData(OrderDetailsData data) {
		this.data = data;
	}
	

}
