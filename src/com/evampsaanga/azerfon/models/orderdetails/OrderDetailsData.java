package com.evampsaanga.azerfon.models.orderdetails;

import java.util.List;

public class OrderDetailsData {
	
	List<OrderDetailsResponse> orderdetails=null;

	public List<OrderDetailsResponse> getOrderdetails() {
		return orderdetails;
	}

	public void setOrderdetails(List<OrderDetailsResponse> orderdetails) {
		this.orderdetails = orderdetails;
	}

}
