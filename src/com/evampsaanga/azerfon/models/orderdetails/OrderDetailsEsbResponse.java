
package com.evampsaanga.azerfon.models.orderdetails;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "returnMsg",
    "returnCode",
    "orderDetailsResponse"
})
public class OrderDetailsEsbResponse {

    @JsonProperty("returnMsg")
    private String returnMsg;
    @JsonProperty("returnCode")
    private String returnCode;
    @JsonProperty("orderDetailsResponse")
    private List<OrderDetailsResponse> orderDetailsResponse = null;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("returnMsg")
    public String getReturnMsg() {
        return returnMsg;
    }

    @JsonProperty("returnMsg")
    public void setReturnMsg(String returnMsg) {
        this.returnMsg = returnMsg;
    }

    @JsonProperty("returnCode")
    public String getReturnCode() {
        return returnCode;
    }

    @JsonProperty("returnCode")
    public void setReturnCode(String returnCode) {
        this.returnCode = returnCode;
    }

    @JsonProperty("orderDetailsResponse")
    public List<OrderDetailsResponse> getOrderDetailsResponse() {
        return orderDetailsResponse;
    }

    @JsonProperty("orderDetailsResponse")
    public void setOrderDetailsResponse(List<OrderDetailsResponse> orderDetailsResponse) {
        this.orderDetailsResponse = orderDetailsResponse;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
