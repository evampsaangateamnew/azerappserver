package com.evampsaanga.azerfon.models.rateus;

public class RateUsResponseData {
	private String responseMsg;

	public String getResponseMsg() {
		return responseMsg;
	}

	public void setResponseMsg(String responseMsg) {
		this.responseMsg = responseMsg;
	}

}
