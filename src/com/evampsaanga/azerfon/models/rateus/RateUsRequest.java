package com.evampsaanga.azerfon.models.rateus;

import com.evampsaanga.azerfon.common.utilities.BaseRequest;

public class RateUsRequest extends BaseRequest {

	private String entityId;

	public String getEntityId() {
		return entityId;
	}

	public void setEntityId(String entityId) {
		this.entityId = entityId;
	}

	@Override
	public String toString() {
		return "RateUsRequest [entityId=" + entityId + "]";
	}

}
