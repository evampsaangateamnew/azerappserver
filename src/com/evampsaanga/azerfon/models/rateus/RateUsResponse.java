package com.evampsaanga.azerfon.models.rateus;

import com.evampsaanga.azerfon.common.utilities.BaseResponse;

public class RateUsResponse extends BaseResponse {
	private RateUsResponseData data;

	public RateUsResponseData getData() {
		return data;
	}

	public void setData(RateUsResponseData data) {
		this.data = data;
	}

	@Override
	public String toString() {
		return "RateUsResponse [data=" + data + "]";
	}

}
