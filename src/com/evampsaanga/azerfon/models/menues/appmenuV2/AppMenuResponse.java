/**
 * 
 */
package com.evampsaanga.azerfon.models.menues.appmenuV2;

import com.evampsaanga.azerfon.common.utilities.BaseResponse;

/**
 * @author Evamp & Saanga
 *
 */
public class AppMenuResponse extends BaseResponse {

	private Data data = new Data();

	public Data getData() {
		return data;
	}

	public void setData(Data data) {
		this.data = data;
	}

	

}
