/**
 * 
 */
package com.evampsaanga.azerfon.models.menues.appmenuV2;

import com.evampsaanga.azerfon.common.utilities.BaseRequest;

/**
 * @author Evamp & Saanga
 *
 */
public class AppMenuRequest extends BaseRequest {

	@Override
	public String toString() {
		return "AppMenuRequest [getLang()=" + getLang() + ", getiP()=" + getiP() + ", getChannel()=" + getChannel()
				+ ", getMsisdn()=" + getMsisdn() + "]";
	}

}
