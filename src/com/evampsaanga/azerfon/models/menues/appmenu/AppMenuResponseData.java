/**
 * 
 */
package com.evampsaanga.azerfon.models.menues.appmenu;

import java.util.List;

/**
 * @author Evamp & Saanga
 *
 */
public class AppMenuResponseData {

	private List<AppMenuListData> appMenues;

	public List<AppMenuListData> getAppMenues() {
		return appMenues;
	}

	public void setAppMenues(List<AppMenuListData> appMenues) {
		this.appMenues = appMenues;
	}
}
