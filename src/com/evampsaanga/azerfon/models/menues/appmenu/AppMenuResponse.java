/**
 * 
 */
package com.evampsaanga.azerfon.models.menues.appmenu;

import com.evampsaanga.azerfon.common.utilities.BaseResponse;

/**
 * @author Evamp & Saanga
 *
 */
public class AppMenuResponse extends BaseResponse {

	private AppMenuResponseData data;

	public AppMenuResponseData getData() {
		return data;
	}

	public void setData(AppMenuResponseData data) {
		this.data = data;
	}

}
