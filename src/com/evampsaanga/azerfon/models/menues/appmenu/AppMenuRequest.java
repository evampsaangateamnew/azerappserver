/**
 * 
 */
package com.evampsaanga.azerfon.models.menues.appmenu;

import com.evampsaanga.azerfon.common.utilities.BaseRequest;

/**
 * @author Evamp & Saanga
 *
 */
public class AppMenuRequest extends BaseRequest {

	private String offeringName;
	public String getOfferingName() {
		return offeringName;
	}
	public void setOfferingName(String offeringName) {
		this.offeringName = offeringName;
	}
	@Override
	public String toString() {
		return "AppMenuRequest [getLang()=" + getLang() + ", getiP()=" + getiP() + ", getChannel()=" + getChannel()
				+ ", getMsisdn()=" + getMsisdn() + "]";
	}

}
