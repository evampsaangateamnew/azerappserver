/**
 * 
 */
package com.evampsaanga.azerfon.models.menues.appmenu;

import java.util.List;

/**
 * @author Evamp & Saanga
 *
 */
public class AppMenuListData {
	private String menuHeader;
	private List<AppMenu> menuList;

	public String getMenuHeader() {
		return menuHeader;
	}

	public void setMenuHeader(String menuHeader) {
		this.menuHeader = menuHeader;
	}

	public List<AppMenu> getMenuList() {
		return menuList;
	}

	public void setMenuList(List<AppMenu> menuList) {
		this.menuList = menuList;
	}

}
