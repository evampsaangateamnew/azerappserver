package com.evampsaanga.azerfon.models.dashboardservice;

import com.evampsaanga.azerfon.common.utilities.BaseResponse;

public class DashboardResponse extends BaseResponse{

	DashboardResponseData data;

	public DashboardResponseData getData() {
		return data;
	}

	public void setData(DashboardResponseData data) {
		this.data = data;
	}

	@Override
	public String toString() {
		return "DashboardResponse [data=" + data + "]";
	}

}
