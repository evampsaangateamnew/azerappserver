package com.evampsaanga.azerfon.models.dashboardservice.queryinvoice;

public class QueryInvoiceData {

	private String invoiceAmount;
	private String invoiceDate;
	private String dueDate;
	private String settleDate;
	private String invoiceDateDisp;
	private String dueDateDisp;
	private String status;
	private String settleDateDisp;
	private String daysDisp;
	private String totalAmountToBePaidLabel;
	private String invoiceLabel;
	private String totalDays;
	private String remainingDays;



	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getInvoiceAmount() {
		return invoiceAmount;
	}

	public void setInvoiceAmount(String invoiceAmount) {
		this.invoiceAmount = invoiceAmount;
	}

	public String getInvoiceDate() {
		return invoiceDate;
	}

	public void setInvoiceDate(String invoiceDate) {
		this.invoiceDate = invoiceDate;
	}

	public String getDueDate() {
		return dueDate;
	}

	public void setDueDate(String dueDate) {
		this.dueDate = dueDate;
	}

	public String getInvoiceDateDisp() {
		return invoiceDateDisp;
	}

	public void setInvoiceDateDisp(String invoiceDateDisp) {
		this.invoiceDateDisp = invoiceDateDisp;
	}

	public String getDueDateDisp() {
		return dueDateDisp;
	}

	public void setDueDateDisp(String dueDateDisp) {
		this.dueDateDisp = dueDateDisp;
	}

	public String getSettleDate() {
		return settleDate;
	}

	public void setSettleDate(String settleDate) {
		this.settleDate = settleDate;
	}

	public String getSettleDateDisp() {
		return settleDateDisp;
	}

	public void setSettleDateDisp(String settleDateDisp) {
		this.settleDateDisp = settleDateDisp;
	}

	public String getTotalAmountToBePaidLabel() {
		return totalAmountToBePaidLabel;
	}

	public void setTotalAmountToBePaidLabel(String totalAmountToBePaidLabel) {
		this.totalAmountToBePaidLabel = totalAmountToBePaidLabel;
	}

	public String getInvoiceLabel() {
		return invoiceLabel;
	}

	public void setInvoiceLabel(String invoiceLabel) {
		this.invoiceLabel = invoiceLabel;
	}

	public String getTotalDays() {
		return totalDays;
	}

	public void setTotalDays(String totalDays) {
		this.totalDays = totalDays;
	}

	public String getRemainingDays() {
		return remainingDays;
	}

	public void setRemainingDays(String remainingDays) {
		this.remainingDays = remainingDays;
	}

	public String getDaysDisp() {
		return daysDisp;
	}

	public void setDaysDisp(String daysDisp) {
		this.daysDisp = daysDisp;
	}

}
