package com.evampsaanga.azerfon.models.dashboardservice.queryinvoice;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


public class QueryInvoiceResponseData {
	private List<QueryInvoiceData> queryInvoiceResponseData;

	public List<QueryInvoiceData> getQueryInvoiceResponseData() {
		return queryInvoiceResponseData;
	}

	public void setQueryInvoiceResponseData(List<QueryInvoiceData> queryInvoiceResponseData) {
		this.queryInvoiceResponseData = queryInvoiceResponseData;
	}

}
