package com.evampsaanga.azerfon.models.dashboardservice;

import com.evampsaanga.azerfon.common.utilities.BaseResponse;

public class DashboardResponseResume extends BaseResponse{

	DashboardResponseDataResume data;

	public DashboardResponseDataResume getData() {
		return data;
	}

	public void setData(DashboardResponseDataResume data) {
		this.data = data;
	}

}
