package com.evampsaanga.azerfon.models.dashboardservice;

public class GroupNamesData {
	String value;
	String key;
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}

}
