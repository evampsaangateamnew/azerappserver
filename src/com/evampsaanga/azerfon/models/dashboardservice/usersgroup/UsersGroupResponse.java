package com.evampsaanga.azerfon.models.dashboardservice.usersgroup;

import java.util.ArrayList;

public class UsersGroupResponse{
	
	UserGroupData GroupData;
	private ArrayList<UsersGroupData> users;
	private String isLastPage;

	public UserGroupData getGroupData() {
		return GroupData;
	}

	public void setGroupData(UserGroupData groupData) {
		GroupData = groupData;
	}

	public ArrayList<UsersGroupData> getUsers() {
		return users;
	}

	public void setUsers(ArrayList<UsersGroupData> users) {
		this.users = users;
	}

	public String getIsLastPage() {
		return isLastPage;
	}

	public void setIsLastPage(String isLastPage) {
		this.isLastPage = isLastPage;
	}

	
	

}
