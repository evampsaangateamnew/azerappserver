package com.evampsaanga.azerfon.models.dashboardservice;

import com.evampsaanga.azerfon.common.utilities.BaseRequest;

public class DashboardRequestResume extends BaseRequest{
	
	private String channel;
	private String iP;
	private String msisdn;
	
	

	public String getMsisdn() {
		return msisdn;
	}

	public void setMsisdn(String msisdn) {
		this.msisdn = msisdn;
	}

	
	public String getChannel() {
		return channel;
	}

	public void setChannel(String channel) {
		this.channel = channel;
	}

	public String getiP() {
		return iP;
	}

	public void setiP(String iP) {
		this.iP = iP;
	}

}
