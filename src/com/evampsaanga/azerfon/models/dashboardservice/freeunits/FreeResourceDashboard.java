package com.evampsaanga.azerfon.models.dashboardservice.freeunits;

public class FreeResourceDashboard {
	

	    private FreeResources[] freeResources;
	    private M2mBalance m2mBalancce;
		public M2mBalance getM2mBalancce() {
			return m2mBalancce;
		}
		public void setM2mBalancce(M2mBalance m2mBalancce) {
			this.m2mBalancce = m2mBalancce;
		}


	    public FreeResources[] getFreeResources ()
	    {
	        return freeResources;
	    }

	    public void setFreeResources (FreeResources[] freeResources)
	    {
	        this.freeResources = freeResources;
	    }


}
