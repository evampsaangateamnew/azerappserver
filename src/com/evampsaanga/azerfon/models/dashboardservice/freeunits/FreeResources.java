package com.evampsaanga.azerfon.models.dashboardservice.freeunits;

public class FreeResources {
	 private String resourceDiscountedText;

	    private String resourceUnitName;

	    private String resourceRemainingUnits;

	    private String resourceInitialUnits;

	    private String resourcesTitleLabel;

	    private String resourceType;

	    private String remainingFormatted;

	    public String getResourceDiscountedText ()
	    {
	        return resourceDiscountedText;
	    }

	    public void setResourceDiscountedText (String resourceDiscountedText)
	    {
	        this.resourceDiscountedText = resourceDiscountedText;
	    }

	    public String getResourceUnitName ()
	    {
	        return resourceUnitName;
	    }

	    public void setResourceUnitName (String resourceUnitName)
	    {
	        this.resourceUnitName = resourceUnitName;
	    }

	    public String getResourceRemainingUnits ()
	    {
	        return resourceRemainingUnits;
	    }

	    public void setResourceRemainingUnits (String resourceRemainingUnits)
	    {
	        this.resourceRemainingUnits = resourceRemainingUnits;
	    }

	    public String getResourceInitialUnits ()
	    {
	        return resourceInitialUnits;
	    }

	    public void setResourceInitialUnits (String resourceInitialUnits)
	    {
	        this.resourceInitialUnits = resourceInitialUnits;
	    }

	    public String getResourcesTitleLabel ()
	    {
	        return resourcesTitleLabel;
	    }

	    public void setResourcesTitleLabel (String resourcesTitleLabel)
	    {
	        this.resourcesTitleLabel = resourcesTitleLabel;
	    }

	    public String getResourceType ()
	    {
	        return resourceType;
	    }

	    public void setResourceType (String resourceType)
	    {
	        this.resourceType = resourceType;
	    }

	    public String getRemainingFormatted ()
	    {
	        return remainingFormatted;
	    }

	    public void setRemainingFormatted (String remainingFormatted)
	    {
	        this.remainingFormatted = remainingFormatted;
	    }

	    @Override
	    public String toString()
	    {
	        return "ClassPojo [resourceDiscountedText = "+resourceDiscountedText+", resourceUnitName = "+resourceUnitName+", resourceRemainingUnits = "+resourceRemainingUnits+", resourceInitialUnits = "+resourceInitialUnits+", resourcesTitleLabel = "+resourcesTitleLabel+", resourceType = "+resourceType+", remainingFormatted = "+remainingFormatted+"]";
	    }

}
