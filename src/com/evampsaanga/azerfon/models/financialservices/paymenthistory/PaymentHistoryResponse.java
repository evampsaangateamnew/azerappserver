/**
 * 
 */
package com.evampsaanga.azerfon.models.financialservices.paymenthistory;

import com.evampsaanga.azerfon.common.utilities.BaseResponse;

/**
 * @author Evamp & Saanga
 *
 */
public class PaymentHistoryResponse extends BaseResponse {
	private PaymentHistoryResponseData data;

	public PaymentHistoryResponseData getData() {
		return data;
	}

	public void setData(PaymentHistoryResponseData data) {
		this.data = data;
	}

}
