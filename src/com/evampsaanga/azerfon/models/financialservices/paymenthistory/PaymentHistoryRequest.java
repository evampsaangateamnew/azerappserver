/**
 * 
 */
package com.evampsaanga.azerfon.models.financialservices.paymenthistory;

import com.evampsaanga.azerfon.common.utilities.BaseRequest;

/**
 * @author Evamp & Saanga
 *
 */
public class PaymentHistoryRequest extends BaseRequest {

	private String startDate;
	private String endDate;

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	@Override
	public String toString() {
		return "PaymentHistoryRequest [startDate=" + startDate + ", endDate=" + endDate + ", getLang()=" + getLang()
				+ ", getiP()=" + getiP() + ", getChannel()=" + getChannel() + ", getMsisdn()=" + getMsisdn() + "]";
	}

}
