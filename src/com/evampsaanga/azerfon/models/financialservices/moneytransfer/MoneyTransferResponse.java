package com.evampsaanga.azerfon.models.financialservices.moneytransfer;

import com.evampsaanga.azerfon.common.utilities.BaseResponse;

public class MoneyTransferResponse extends BaseResponse {
	private MoneyTransferResponseData data;

	public MoneyTransferResponseData getData() {
		return data;
	}

	public void setData(MoneyTransferResponseData data) {
		this.data = data;
	}

}
