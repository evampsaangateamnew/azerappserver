/**
 * 
 */
package com.evampsaanga.azerfon.models.financialservices.requestmoney;

import com.evampsaanga.azerfon.common.utilities.BaseRequest;

/**
 * @author Evamp & Saanga
 *
 */
public class RequestMoneyRequest extends BaseRequest {

	private String friendMsisdn;
	private String amount;

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getFriendMsisdn() {
		return friendMsisdn;
		
	}

	public void setFriendMsisdn(String friendMsisdn) {
		this.friendMsisdn = friendMsisdn;
	}

	@Override
	public String toString() {
		return "RequestMoneyRequest [friendMsisdn=" + friendMsisdn + "]";
	}

}
