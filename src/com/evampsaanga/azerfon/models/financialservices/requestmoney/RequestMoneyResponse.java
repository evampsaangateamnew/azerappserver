/**
 * 
 */
package com.evampsaanga.azerfon.models.financialservices.requestmoney;

import com.evampsaanga.azerfon.common.utilities.BaseResponse;

/**
 * @author Evamp & Saanga
 *
 */
public class RequestMoneyResponse extends BaseResponse {

	private RequestMoneyResponseData data;

	public RequestMoneyResponseData getData() {
		return data;
	}

	public void setData(RequestMoneyResponseData data) {
		this.data = data;
	}

}
