/**
 * 
 */
package com.evampsaanga.azerfon.models.financialservices.getloan;

import com.evampsaanga.azerfon.common.utilities.BaseRequest;

/**
 * @author Evamp & Saanga
 *
 */
public class GetLoanRequest extends BaseRequest {

	private String loanAmount;
	private String brandId;
	private String friendMsisdn;

	public String getLoanAmount() {
		return loanAmount;
	}

	public void setLoanAmount(String loanAmount) {
		this.loanAmount = loanAmount;
	}

	public String getBrandId() {
		return brandId;
	}

	public void setBrandId(String brandId) {
		this.brandId = brandId;
	}

	public String getFriendMsisdn() {
		return friendMsisdn;
	}

	public void setFriendMsisdn(String friendMsisdn) {
		this.friendMsisdn = friendMsisdn;
	}

	@Override
	public String toString() {
		return "GetLoanRequest [loanAmount=" + loanAmount + ", brandId=" + brandId + ", friendMsisdn=" + friendMsisdn
				+ "]";
	}

}
