/**
 * 
 */
package com.evampsaanga.azerfon.models.financialservices.getloan;

import com.evampsaanga.azerfon.common.utilities.BaseResponse;

/**
 * @author Evamp & Saanga
 *
 */
public class GetLoanResponse extends BaseResponse {
	private GetLoanResponseData data;

	public GetLoanResponseData getData() {
		return data;
	}

	public void setData(GetLoanResponseData data) {
		this.data = data;
	}

}
