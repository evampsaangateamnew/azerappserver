/**
 * 
 */
package com.evampsaanga.azerfon.models.financialservices.getloan;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * @author Evamp & Saanga
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class GetLoanResponseData {

	private String newBalance;
	private String newCredit;

	private String message;

	public String getNewBalance() {
		return newBalance;
	}

	public void setNewBalance(String newBalance) {
		this.newBalance = newBalance;
	}

	public String getNewCredit() {
		return newCredit;
	}

	public void setNewCredit(String newCredit) {
		this.newCredit = newCredit;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

}
