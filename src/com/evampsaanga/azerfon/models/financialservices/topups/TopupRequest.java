package com.evampsaanga.azerfon.models.financialservices.topups;

import com.evampsaanga.azerfon.common.utilities.BaseRequest;
import com.evampsaanga.azerfon.common.utilities.Constants;
import com.evampsaanga.azerfon.common.utilities.Utilities;
import com.fasterxml.jackson.annotation.JsonIgnore;

public class TopupRequest extends BaseRequest {

	private String cardPinNumber;
//	@JsonIgnore
	  private String subscriberType;
	  private String topupnum;

	public String getCardPinNumber() {
		return cardPinNumber;
	}

	public void setCardPinNumber(String cardPinNumber) {
		this.cardPinNumber = cardPinNumber;
	}
//	@JsonIgnore
	public String getSubscriberType() {
		return subscriberType;
	}

//	@JsonIgnore
	public void setSubscriberType(String subscriberType) {
		this.subscriberType = subscriberType;
	}

	



	public String getTopupnum() {
		return topupnum;
	}

	public void setTopupnum(String topupnum) {
		this.topupnum = topupnum;
	}

	@Override
	public String toString() {
		return "TopupRequest [cardPinNumber=" + cardPinNumber + ", subscriberType=" + subscriberType + ", topupnum="
				+ topupnum + ", getCardPinNumber()=" + getCardPinNumber() + ", getSubscriberType()="
				+ getSubscriberType() + ", getTopupnum()=" + getTopupnum() + "]";
	}



}
