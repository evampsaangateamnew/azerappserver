package com.evampsaanga.azerfon.models.financialservices.topups;

import com.evampsaanga.azerfon.common.utilities.BaseResponse;

public class TopupResponse extends BaseResponse {
	private TopupResponseData data;

	public TopupResponseData getData() {
		return data;
	}

	public void setData(TopupResponseData data) {
		this.data = data;
	}

}
