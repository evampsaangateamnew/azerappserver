/**
 * 
 */
package com.evampsaanga.azerfon.models.financialservices.loanhistory;

import com.evampsaanga.azerfon.common.utilities.BaseResponse;

/**
 * @author Evamp & Saanga
 *
 */
public class LoanHistoryResponse extends BaseResponse {

	private LoanHistoryResponseData data;

	public LoanHistoryResponseData getData() {
		return data;
	}

	public void setData(LoanHistoryResponseData data) {
		this.data = data;
	}

}
