/**
 * 
 */
package com.evampsaanga.azerfon.models.financialservices.loanhistory;

import java.util.List;

/**
 * @author Evamp & Saanga
 *
 */
public class LoanHistoryResponseData {

	private List<Loan> loan;

	public List<Loan> getLoan() {
		return loan;
	}

	public void setLoan(List<Loan> loan) {
		this.loan = loan;
	}

	@Override
	public String toString() {
		return "LoanHistoryResponseData [loan=" + loan + "]";
	}

}
