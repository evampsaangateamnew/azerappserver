package com.evampsaanga.azerfon.interceptor;

import java.io.File;
import java.io.IOException;

import org.apache.commons.vfs.FileChangeEvent;
import org.apache.commons.vfs.FileListener;
import org.apache.commons.vfs.FileObject;
import org.apache.commons.vfs.FileSystemManager;
import org.apache.commons.vfs.VFS;
import org.apache.commons.vfs.impl.DefaultFileMonitor;

public class ConfigurationsFilesMonitor {

	public static void monitor(File file) throws IOException {
		// source: http://stackoverflow.com/a/7977428/1874604
		// setup monitoring

		FileSystemManager manager = VFS.getManager();
		FileObject fobj = manager.resolveFile(file.getPath());
		DefaultFileMonitor fm = new DefaultFileMonitor(new SimpleFileListener());

		fm.setDelay(0);
		fm.addFile(fobj);
		fm.start();
	}

	// public static void main(String[] args) throws IOException {
	// File file = File.createTempFile("simplefile", ".tmp");
	// monitor(file);
	// file.delete();
	// }

	private static class SimpleFileListener implements FileListener {

		@Override
		public void fileChanged(FileChangeEvent arg0) throws Exception {
			System.out.println("File Name:" + arg0.getFile().getName());
			// ResourceBundleFiles.resetResourceBundleConfig();
			System.out.println("File changed");

		}

		@Override
		public void fileCreated(FileChangeEvent arg0) throws Exception {
			System.out.println("File created");
		}

		@Override
		public void fileDeleted(FileChangeEvent arg0) throws Exception {
			System.out.println("File deleted");
		}
	}
}
