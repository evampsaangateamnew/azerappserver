package com.evampsaanga.azerfon.interceptor;

import java.io.File;
import java.io.IOException;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

import com.evampsaanga.azerfon.common.utilities.Constants;

@WebListener
public class ServerStartupConfigurations implements ServletContextListener {

	@Override
	public void contextInitialized(ServletContextEvent event) {
		System.out.println("WELCOME TO BAKCELL E-CARE");
		try {
			ConfigurationsFilesMonitor.monitor(new File(Constants.GET_BASE_CONFIG_PATH + "Config.properties"));
			ConfigurationsFilesMonitor.monitor(new File(Constants.GET_BASE_CONFIG_PATH + "DBQueries.properties"));
			ConfigurationsFilesMonitor.monitor(new File(Constants.GET_BASE_CONFIG_PATH + "ESBRoutes.properties"));
			ConfigurationsFilesMonitor.monitor(new File(Constants.GET_BASE_CONFIG_PATH + "validations.properties"));

		} catch (IOException e) {

			e.printStackTrace();
		}
	}

	@Override
	public void contextDestroyed(ServletContextEvent event) {
		System.out.println("Good Bye!");
	}
}
