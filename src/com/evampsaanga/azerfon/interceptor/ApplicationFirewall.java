/**
 * 
 */
package com.evampsaanga.azerfon.interceptor;

import java.io.IOException;
import java.net.SocketException;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Enumeration;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import com.evampsaanga.azerfon.common.messagesmappings.GetMessagesMappings;
import com.evampsaanga.azerfon.common.utilities.Constants;
import com.evampsaanga.azerfon.common.utilities.GetConfigurations;
import com.evampsaanga.azerfon.common.utilities.Utilities;

/**
 * @author Evamp & Saanga
 *
 */
public class ApplicationFirewall implements HandlerInterceptor {

    Logger logger = Logger.getLogger(ApplicationFirewall.class);

    @Override
    public void afterCompletion(HttpServletRequest arg0, HttpServletResponse arg1, Object arg2, Exception arg3)
	    throws Exception {
	// TODO Auto-generated method stub

    }

    @Override
    public void postHandle(HttpServletRequest arg0, HttpServletResponse arg1, Object arg2, ModelAndView arg3)
	    throws Exception {
	// TODO Auto-generated method stub

    }

    @Override
    public boolean preHandle(HttpServletRequest arg0, HttpServletResponse arg1, Object arg2) throws Exception {

	String lang = arg0.getHeader("lang");
	/*
	 * By force, turning flag status to "TRUE" means by passing all token
	 * validations.
	 * 
	 */

	// Boolean flag = true;
	Boolean flag = true;
	if (flag)
	    return flag;

	if (GetConfigurations.getConfigurationFromCache("isServerUnderMaintenance").trim().equalsIgnoreCase("true")) {
	    Utilities.printDebugLog("-----------------Server is currently under gone for maintenance-----------------",
		    logger);
	    arg1 = prepareResponseObject(arg1, Constants.SERVER_UNDER_MAINTENANCE,
		    GetMessagesMappings.getMessageFromResourceBundle("serverUnderMaintenance", lang));
	    return false;
	} else {
	    printRequestHeaders(arg0);
	    String token = arg0.getHeader("token");

	    Utilities.printDebugLog(arg0.getHeader("msisdn") + "SECURITY INTERCEPTOR RECEIVED TOKEN-" + token, logger);
	    Utilities.printDebugLog("Header MSISDN-" + arg0.getHeader("msisdn") + "-Header Device ID-"
		    + arg0.getHeader("deviceID") + "-Request Landed in Handler - Agent: " + arg0.getHeader("UserAgent")
		    + "- Path: " + arg0.getPathInfo() + "- language: " + lang, logger);

	    /*
	     * ------------------- Security Check NOT APPLICABLE
	     * ----------------------------- Below URLs are used internally
	     * ---------------------------- Therefore no security check needed
	     * ------------
	     */
	    if (arg0.getPathInfo().equalsIgnoreCase("/trigger/refreshcache")) {
		/*
		 * Those sub-URLs which do not need to be intercepted. E.g:
		 * Triggers
		 */
		Utilities.printDebugLog(
			"-----------------Security check not applicable for this request-----------------", logger);
		return true;
	    }

	    if (arg0.getHeader("UserAgent").equalsIgnoreCase(Constants.ALLOWABLE_USER_AGENTS.android.toString())
		    || arg0.getHeader("UserAgent").equalsIgnoreCase(Constants.ALLOWABLE_USER_AGENTS.iPhone.toString())
		    || arg0.getHeader("UserAgent")
			    .equalsIgnoreCase(Constants.ALLOWABLE_USER_AGENTS.portal.toString())) {
		Utilities.printDebugLog("-----------------Agent Matched Successfully-----------------", logger);
		if (!arg0.getPathInfo().equalsIgnoreCase("/customerservices/signup")
			&& !arg0.getPathInfo().equalsIgnoreCase("/customerservices/verifyotp")
			&& !arg0.getPathInfo().equalsIgnoreCase("/customerservices/savecustomer")
			&& !arg0.getPathInfo().equalsIgnoreCase("/customerservices/resendpin")
			&& !arg0.getPathInfo().equalsIgnoreCase("/customerservices/authenticateuser")
			&& !arg0.getPathInfo().equalsIgnoreCase("/customerservices/forgotpassword")
			&& !arg0.getPathInfo().equalsIgnoreCase("/generalservices/verifyappversion")
			&& !arg0.getPathInfo().equalsIgnoreCase("/mysubscriptions/getsubscriptionsforportal")) {
		    if (arg0.getHeader("token") != null && !arg0.getHeader("token").isEmpty()) {
			JSONObject tokenObj = Utilities.splitToken(Utilities.decodeString(token));

			if (tokenObj.getString("msisdn").equalsIgnoreCase(arg0.getHeader("msisdn"))
				&& tokenObj.getString("deviceID").equalsIgnoreCase(arg0.getHeader("deviceID"))) {
			    Utilities.printDebugLog("-----------------Token Validated Successfully-----------------",
				    logger);

			    // String tokenTimeFromSession =
			    // AppCache.hashmapSessions
			    // .get(Utilities.getHashmapKeyForSession(tokenObj.getString("msisdn"),
			    // tokenObj.getString("deviceID")));

			    // if (tokenTimeFromSession != null
			    // &&
			    // tokenTimeFromSession.equalsIgnoreCase(tokenObj.getString("timeStamp")))
			    // {
			    //
			    if (isUserAlive(tokenObj.getString("timeStamp"))) {
				Utilities.printDebugLog("-----------------Token Session Alive-----------------",
					logger);
				flag = true;
			    } else {
				Utilities.printDebugLog("-----------------Token Session Expired-----------------",
					logger);
				arg1 = prepareResponseObject(arg1, Constants.SESSION_EXPIRED, GetMessagesMappings
					.getMessageFromResourceBundle("token.session.expired", lang));
			    }
			    // } else {
			    // Utilities.printDebugLog(
			    // "-----------------Token Expired (Session and
			    // token timestamp mismatched) -----------------",
			    // logger);
			    // arg1 = prepareResponseObject(arg1,
			    // Constants.SESSION_EXPIRED, GetMessagesMappings
			    // .getMessageFromResourceBundle("token.session.expired",
			    // lang));
			    // }
			} else {
			    Utilities.printDebugLog(
				    "----------------- Token INVALID( Either msisdn or device ID mismatched) -----------------",
				    logger);
			    Utilities.printDebugLog("----------------- TOKEN MSISDN: " + tokenObj.getString("msisdn"),
				    logger);
			    Utilities.printDebugLog(
				    "----------------- TOKEN DEVICE ID: " + tokenObj.getString("deviceID"), logger);

			    arg1 = prepareResponseObject(arg1, Constants.INVALID_TOKEN,
				    GetMessagesMappings.getMessageFromResourceBundle("token.invalid", lang));
			}

		    } else {
			Utilities.printDebugLog("----------------- TOKEN NOT FOUND -----------------", logger);
			arg1 = prepareResponseObject(arg1, Constants.INVALID_TOKEN,
				GetMessagesMappings.getMessageFromResourceBundle("token.invalid", lang));
		    }
		} else {
		    Utilities.printDebugLog("-----------------Security check not applicable for this  request ("
			    + arg0.getPathInfo() + " )-----------------", logger);
		    flag = true;
		}
	    } else {
		Utilities.printDebugLog("-----------------Request received from unknown agent-----------------",
			logger);
		arg1 = prepareResponseObject(arg1, Constants.UNKNOWN_AGENT,
			GetMessagesMappings.getMessageFromResourceBundle("unknown.agent", lang));

	    }
	    return flag;
	}
    }

    private void printRequestHeaders(HttpServletRequest arg0) throws SocketException {
	Utilities.printTraceLog("*********************************** REQUEST HEADERS ******************************* ",
		logger);
	Enumeration<String> headerNames = arg0.getHeaderNames();
	while (headerNames.hasMoreElements()) {
	    String headerName = headerNames.nextElement();
	    Utilities.printTraceLog(headerName + ":" + arg0.getHeader(headerName), logger);

	}
	Utilities.printTraceLog(
		"*********************************** REQUEST HEADERS ENDED ******************************* ", logger);

    }

    private boolean isUserAlive(String userLastLogin) throws ParseException, SocketException, NumberFormatException, SQLException {

//	Date d1 = null;
//	Date d2 = null;
//	SimpleDateFormat format = new SimpleDateFormat(Constants.DATE_TIME_FORMAT_FOR_TOKEN);
//
//	d1 = format.parse(userLastLogin); // User last login date
//	d2 = new Date();
//
//	long diff = d2.getTime() - d1.getTime(); // in milliseconds
//	// long diffMins = (diff / 1000) / 60;// for minutes.
//	long diffMins = diff / (24 * 60 * 60 * 1000); // for Days.
//	Utilities.printDebugLog("-------------Days Differnece: " + diffMins, logger);
//
//	if (diffMins < Integer.parseInt(GetConfigurations.getConfigurationFromCache("app.live.session.days").trim()))
//	    return true;
//	return false;
    	
    	
    	return true;
    }

    private HttpServletResponse prepareResponseObject(HttpServletResponse arg1, String resultCode, String resultDesc)
	    throws JSONException, IOException {
	JSONObject obj = new JSONObject();
	arg1.setStatus(200);

	obj.put("resultCode", resultCode);
	obj.put("resultDesc", resultDesc);
	obj.put("callStatus", Constants.Call_Status_False);
	arg1.getWriter().write(obj.toString());
	return arg1;
    }

    // public static void main(String[] args) throws ParseException {
    // System.out.println(new ApplicationFirewall().isUserAlive("2017-10-18
    // 19:32:55"));
    // }
}
